#pragma once

#include "macro_struct.hh"

namespace Browndye{
  class Group_Common;

  struct Dummy_Molecule: public Macro_Struct{
    Core_Index im;

    Dummy_Molecule(const JAM_XML_Pull_Parser::Node_Ptr &dnode, const Nonbonded_Parameter_Info &pinfo,
                   const Group_Common&);

  };

}
