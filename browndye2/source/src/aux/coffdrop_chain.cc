// input: pqr bead file, coffdrop parameter file, connectivity file, name,
// [core_name core_file (core_residue chain_residue)...]...
// output: file for chain
// -pqr, -params, -conn, -name, -cores

/* Possibilities with cores and chains:
   
   1) no cores
   2) one core
     a) chain attached to low end
     b) chain attached to high end
     c) both a and b, forming a loop
   3) two cores
   
   Assumptions:
     core residues are numbered in sequence at least where the chain attaches
     chain residues are numbered in sequence
     chain residues are not necessarily sequential with attached core
       residues, but must be > or < commensurate with the core residue ordering
   
*/
 
// includes
  #include <string>
  #include <map>
  #include <set>
  #include <algorithm>
  #include "../xml/jam_xml_pull_parser.hh"
  #include "../xml/node_info.hh"
  #include "../lib/vector.hh"
  #include "../lib/bool_of_string.hh"
  #include "../global/pos.hh"
  #include "../input_output/get_args.hh"
  #include "../global/indices.hh"
  #include "coffdrop.hh"
// end includes

// using decs
  using std::string;
  using std::pair;
  using std::map;
  using std::set;
  using std::cout;
  using std::make_pair;
  using std::make_tuple;
  using std::tuple;
  using std::get;
  using std::tie;
// end using decs

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;
using JP::Parser;
using JP::Node_Ptr;

// Indices
  class Residue_Type_Index_Tag{};
  typedef Index< Residue_Type_Index_Tag> Residue_Type_Index;

  class Bond_Type_Index_Tag{};
  typedef Index< Bond_Type_Index_Tag> Bond_Type_Index;

  class Angle_Type_Index_Tag{};
  typedef Index< Angle_Type_Index_Tag> Angle_Type_Index;

  class Dihedral_Type_Index_Tag{};
  typedef Index< Dihedral_Type_Index_Tag> Dihedral_Type_Index;

  typedef IDiff< Residue_Index_Tag> Residue_Diff;

  class PBond_Type_Index_Tag{};
  typedef Index< PBond_Type_Index_Tag> PBond_Type_Index;
// end Indices

// structures
  const Core_Index cmax( maxi);
  
  //##################################
  // use for actual (proper) bonds
  struct PBond{
    Array< Residue_Type_Index, 2> residues;
    Array< Atom_Type_Index, 2> atoms;
    Array< Core_Index, 2> cores{ cmax,cmax};
    Residue_Diff diff;

    tuple< Array< Residue_Type_Index, 2>, Array< Atom_Type_Index, 2>, Residue_Diff> rep() const{
      return make_tuple( residues, atoms, diff);
    }

    bool operator<( const PBond& other) const{
      return this->rep() < other.rep();
    }
  };

  //##############################################################
  // use for non-bonded pairwise on neighboring and same residue?
  struct Bond{
    Array< Residue_Type_Index, 2> residues;
    Array< Atom_Type_Index, 2> atoms;
    Array< Core_Index, 2> cores{ cmax, cmax};
    Residue_Diff diff;
    Bond_Type_Index type;
    bool actual = false;
    Length length{ NaN};

    tuple< Array< Residue_Type_Index, 2>, Array< Atom_Type_Index, 2>, Residue_Diff, bool> rep() const{
      return make_tuple( residues, atoms, diff, actual);
    }

    bool operator<( const Bond& other) const{
      return this->rep() < other.rep();
    }
  };

  //###########################################
  struct Angle{
    Array< Residue_Type_Index, 3> residues;
    Array< Atom_Type_Index, 3> atoms;
    Array< Core_Index, 3> cores{ cmax,cmax,cmax};
    Array< Residue_Diff, 2> diffs;

    tuple< Array< Residue_Type_Index, 3>, Array< Atom_Type_Index, 3>, Array< Residue_Diff,2> > rep() const{
      return make_tuple( residues, atoms, diffs);
    }

    bool operator<( const Angle& other) const{
      return this->rep() < other.rep();
    }
  };

  //##################################################
  struct Dihedral{
    Array< Residue_Type_Index, 4> residues;
    Array< Atom_Type_Index, 4> atoms;
    Array< Core_Index, 4> cores{ cmax,cmax,cmax,cmax};
    Array< Residue_Diff, 3> diffs;

    tuple< Array< Residue_Type_Index, 4>, Array< Atom_Type_Index, 4>, Array< Residue_Diff,3> > rep() const{
      return make_tuple( residues, atoms, diffs);
    }

    bool operator<( const Dihedral& other) const{
      return this->rep() < other.rep();
    }
  };
  
  //#############################
  struct Core_Junction{
    Core_Index core;
    std::string name;
    Vector< std::pair< Residue_Index, Residue_Index> > joints;
    // first is core, second is chain
  };
// end structures

  //#################################################
  // application of function to vector
  // not the most simple way, but I wanted to see if I could convert a std::array to a parameter pack 
  namespace Inner{
    template< size_t...>
    struct seq{};
  
    template< size_t N, size_t ...S>
    struct gens : gens< N-1, N, S...> { };
  
    template< size_t ...S>
    struct gens< 0, S...> {
      typedef seq< 0, S...> type;
    };
  
    template< class F, class Args, size_t ... n>
    bool call_inner_with( F& f, Args args, seq< n...>){
      return f( args[n] ...);
    }
  
    template< class F, class T, size_t n>
    bool call_with( F& f, Array< T, n> args){
      return call_inner_with( f, args, typename gens< n-1>::type());
    }
  }
  
  template< class F, class T, size_t n>
  static void apply_wc( F& f, Vector< Array< T, n> >& patterns){
    for( auto& pat: patterns)
        if (Inner::call_with( f, pat))
          break;
  }
// end app  

//##########################################################################
typedef Vector< Vector< Residue, Residue_Index>, Core_Index> Cores;
typedef Vector< Core_Junction, Core_Index> Core_Junctions;
typedef Vector< map< Residue_Index, std::reference_wrapper< const Residue> >, Core_Index> Core_Residues;
typedef map< Residue_Index, std::reference_wrapper< const Residue> > Residue_Index_Map;

constexpr Residue_Index ir0( 0), ir1(1);
constexpr auto rid = ir1 - ir0;

template< class Key, class Res>
bool index_found( const std::map< Key, Res>& rmap, Key i);

/*
std::pair< Cores, Core_Junctions> cores_of_input( int argc, char* argv[]);

Core_Residues core_residues_of_cores( const Cores& cores);

map< Residue_Index, Residue> chain_residues_of_file( const std::string& res_file);

map< Residue_Index, std::reference_wrapper< const Residue> >
residues_index_map( const Cores&, const map< Residue_Index, Residue>&,
                     const Core_Junctions&, const Core_Residues&);

std::pair<  map< string, Residue_Type_Index>, map< string, Atom_Type_Index> >
residue_and_atom_types( const std::string& param_file);

map< Atom_Index, const Atom*> atom_pointers( const Residue_Index_Map& residues);

std::set< PBond>
constraint_types_of( const std::string&, const Residue_Index_Map&,
  const map< string, Residue_Type_Index>&, const map< string, Atom_Type_Index>&);

map< Bond, Bond_Type_Index> bond_types_of( Parser&);

pair< map< Angle, Angle_Type_Index>, set< Array< Residue_Diff, 2> > > 
angle_types_and_diffs( Parser& parser);

pair< map< Dihedral, Dihedral_Type_Index>, set< Array< Residue_Diff, 3> > >
dihedral_types_and_diffs( Parser& parser);

*/

//#####################
template< class Atom_Types>
Atom_Type_Index atype( const Atom_Types& atom_types, const CAtom& atom){
  return atom_types.at( atom.name);
}

template< class Residue_Types>
auto rtype( const Residue_Types& residue_types,
                   const Residue& res) -> Residue_Type_Index{
  return residue_types.at( res.name);
}  

bool is_chain( const Residue& res){
  return res.core == "*";
}   

constexpr Residue_Type_Index rt0( 0.0);
typedef pair< string, Atom_Index> Atom_Desr;


//##########################################################################################################
class Doer{
public:
  void main( int argc, char* argv[]);
  
  map< string, Residue_Type_Index> residue_types;
  map< string, Atom_Type_Index> atom_types;
  map< Residue_Index, std::reference_wrapper< const Residue> > residues;
  map< Bond, Bond_Type_Index> bond_types;
  Cores cores;
  Core_Junctions core_junctions;
  Core_Residues core_residues;
  map< Residue_Index, Residue> chain_residues;
  map< Atom_Index, const CAtom*> atoms;
  std::set< PBond> constraint_types;  
  map< Angle, Angle_Type_Index> angle_types;
  set< Array< Residue_Diff, 2> > angle_diffs;
  map< Dihedral, Dihedral_Type_Index> dihedral_types;
  
  Vector< tuple< Atom_Desr, Atom_Desr> > bonds;
  Vector< tuple< Atom_Desr, Atom_Desr, Atom_Desr> > angles;
  std::set< tuple< Atom_Index, Atom_Index> > cbonds;
  Vector< tuple< Atom_Desr, Atom_Desr, Atom_Desr, Atom_Desr> > dihedrals; 
  
  map< Atom_Index, std::set< Atom_Index> > ex_chain_pairs;
  map< string, map< Atom_Index, std::set< Atom_Index> > > ex_core_pairs;
  
  bool use_constraints = false;
  
  void setup_cores_of_input( int argc, char* argv[]);
  void setup_core_residues_of_cores();
  void setup_chain_residues_of_file( const std::string& res_file);
  void setup_residues_index_map();
  void setup_atom_pointers();
  void setup_residue_and_atom_types( const std::string&);
  void setup_constraint_types( const std::string&);
  void setup_bond_types( Parser&, Node_Ptr);
  void setup_angle_types_and_diffs( Parser&, Node_Ptr);
  void setup_dihedral_types( Parser&, Node_Ptr);
  
  template< class F>
  void print_bonds_gen( const F&, const std::string&);
  
  void print_bonds();
  void print_constraints();
  void print_angles();
  void generate_constraint_bonds();
  void print_dihedrals();
  void put_ex_pair( Atom_Desr a0, Atom_Desr a1);
  void process_excluded_pairs();
  
  void ask_to_use_constraints( int argc, char* argv[]);  
};

//############################################
void Doer::main( int argc, char* argv[]){

  print_help( argc, argv, "-pqr: pqr xml file, -params: parameter file,"
             " -name: name of chain, -conn: connectivity file,"
             " -cores: core information,"
             " -constraints: use constraints not bonds"
             " -frozen: chain can be frozen"
             " -dt: constant time step size"
             );

  // get file names
  auto res_file = string_arg( argc, argv, "-pqr");
  auto param_file = string_arg( argc, argv, "-params");
  auto name = string_arg( argc, argv, "-name");
  auto const_dt_opt = double_opt_arg( argc, argv, "-dt");
  auto frozen = has_flag( argc, argv, "-frozen");

  // print header   
  cout << "<top>\n";
  cout << "  <name> " << name << " </name>\n";
  cout << "  <atoms> " << res_file << " </atoms>\n";
 
  cout << "  <never_frozen> " << str_of_bool( !frozen)
        << " </never_frozen>\n";
  
  if (const_dt_opt){
    cout << "  <constant_dt> " << const_dt_opt.value() << " </constant_dt>\n";
  }
  
  setup_cores_of_input( argc, argv); 
 
  setup_core_residues_of_cores(); 
  setup_chain_residues_of_file( res_file);       
  setup_residues_index_map();
          
  // get types
    // get atom pointers, residue,atom, and constraint types, residue names
  setup_atom_pointers();
  
  //map< string, Residue_Type_Index> residue_types;
  //map< string, Atom_Type_Index> atom_types;
  setup_residue_and_atom_types( param_file);
      
  auto con_file = string_arg( argc, argv, "-conn");    
  setup_constraint_types( con_file);
 
  ask_to_use_constraints( argc, argv);
   
  // maps and sets of types  
  // gather same and adjacent residue "bonds", angles, and dihedrals
    
  std::ifstream input( param_file);
  Parser parser( input, param_file);
  auto top = parser.top();
  
  Array< std::string, 3> tags{"pairs", "bond_angles", "dihedral_angles"};
  
  auto lbonds = [&]( auto& cnode){
    setup_bond_types( parser, cnode);
  };
  
  auto langles = [&]( auto& cnode){
    setup_angle_types_and_diffs( parser, cnode);
  };
  
  auto ldihedrals = [&]( auto& cnode){
    setup_dihedral_types( parser, cnode);  
  };
  
  apply_to_each_child_with_tag( top, tags, lbonds, langles, ldihedrals);
  
  print_bonds();
  if (use_constraints){
    print_constraints();
  }
  print_angles();
  generate_constraint_bonds();          
  print_dihedrals();
  process_excluded_pairs();
  
  cout << "</top>\n";
}

//####################################################
void Doer::process_excluded_pairs(){
  
  for( auto& item: residues){
    auto& res0 = item.second.get();
    auto i0 = res0.number;
    for( auto& atom0: res0.atoms){
      auto ia0 = atom0.number;
      
      auto process = [&]( auto itr){
        if (itr != residues.cend()){
          auto& res1 = itr->second.get();
          for( auto& atom1: res1.atoms){
            auto ia1 = atom1.number;
            if (ia1 != ia0){
              auto a0 = make_pair( res0.core, ia0);
              auto a1 = make_pair( res1.core, ia1);
              put_ex_pair( a0,a1);
            }
          }
        }
      };
  
      process( residues.find( i0 - Residue_Diff(1)));
      process( residues.find( i0));
      process( residues.find( i0 + Residue_Diff(1)));
    }
  }
  
  for( auto& bond: bonds){
    Atom_Desr a0,a1;
    tie( a0,a1) = bond;
    put_ex_pair( a0, a1);
  }
  
  for( auto& angle: angles){
    Atom_Desr a0,a1,a2;
    tie( a0,a1,a2) = angle;
    put_ex_pair( a0,a1);
    put_ex_pair( a0,a2);
    put_ex_pair( a1,a2);
  }
  
  for( auto& dihedral: dihedrals){
    Atom_Desr a0,a1,a2,a3;
    // six combos
    tie( a0,a1,a2,a3) = dihedral;
    put_ex_pair( a0,a1);
    put_ex_pair( a0,a2);
    put_ex_pair( a0,a3);
    put_ex_pair( a1,a2);
    put_ex_pair( a1,a3);
    put_ex_pair( a2,a3);
  }
  
  cout << "  <excluded_atoms>\n";
  for( auto& item: ex_chain_pairs){
    auto ia = item.first;
    auto& exats = item.second;
    cout << "    <chain_atom>\n";
    cout << "      <atom> " << ia << " </atom>\n";

    cout << "      <ex_atoms> ";
    for( auto ja: exats)
      cout << ja << " ";
    cout << "</ex_atoms>\n";

    cout << "    </chain_atom>\n";
  }
  
  for(auto& item: ex_core_pairs){
    auto& cname = item.first;
    cout << "    <core>\n";
    cout << "      <name> " << cname << " </name>\n";
    auto& amap = item.second;
    for( auto& item: amap){
      auto mai = item.first;
      auto& caset = item.second;
      cout << "      <core_atom>\n";
      cout << "        <atom> " << mai << " </atom>\n";
      cout << "        <ex_atoms> ";
      for( auto cai: caset)
        cout << cai << " ";  
      cout << "</ex_atoms>\n";
      cout << "      </core_atom>\n";
    }
    cout << "    </core>\n";
  }
  
  cout << "  </excluded_atoms>\n";  
}

//############################################################
void Doer::put_ex_pair( Atom_Desr a0, Atom_Desr a1){
  // constants
  Atom_Index i0,i1;
  string core0, core1;
  tie( core0, i0) = a0;
  tie( core1, i1) = a1;
  
  bool is_chain0 = (core0 == "*");
  bool is_chain1 = (core1 == "*");
  
  if (is_chain0 ^ is_chain1){
    Atom_Index ic,im;
    std::string cname;
    if (is_chain0){
      ic = i0;
      im = i1;
      cname = core1;
    }
    else{
      ic = i1;
      im = i0;
      cname = core0;
    }
    
    ex_core_pairs[ cname][im].insert( ic);
  }
  
  else if (is_chain0 && is_chain1){
    Atom_Index j0,j1;
    tie( j0,j1) = i0 > i1 ? make_pair( i0,i1) : make_pair( i1,i0);
    
    ex_chain_pairs[j0].insert( j1);
  }
  
}

//########################################################
void Doer::print_dihedrals(){
  
  cout << "  <dihedrals>\n";
  
  auto& rt = residue_types;
  auto& at = atom_types;
  
  std::multimap< Atom_Index, Atom_Index> atom_nebors;
  for( auto& cbond: cbonds){
    auto at0 = std::get<0>( cbond);
    auto at1 = std::get<1>( cbond);
    atom_nebors.insert( make_pair( at0, at1));
    atom_nebors.insert( make_pair( at1, at0));
  }
  
  map< Atom_Index, Residue_Index> residue_of_atom;
  for( auto& item: residues){
    auto& residue = item.second.get();
    auto& ir = item.first;
    for( auto& atom: residue.atoms)
      residue_of_atom.insert( make_pair( atom.number, ir));
  }
  
  auto rpick = [&]( unsigned int m, Residue_Type_Index irt){
    if (m == 1)
      return irt;
    else
      return Residue_Type_Index( 0);
  };  
  
  // proper dihedrals
  for( auto& item: residues){
    auto& res0 = item.second.get();
    for( auto& atom0: res0.atoms){
      auto i0 = atom0.number;
      auto ir0 = residue_of_atom.at( i0);
      auto it0 = atype( at, atom0); 
      auto irt0 = rtype( rt, res0); 
      auto rang1 = atom_nebors.equal_range( i0);
      for( auto itr1 = rang1.first; itr1 != rang1.second; itr1++){
        auto i1 = itr1->second;
        auto ir1 = residue_of_atom.at( i1);
        auto& atom1 = *atoms.at( i1);
        auto it1 = atype( at, atom1); 
        auto& res1 = residues.at( ir1).get();
        auto irt1 = rtype( rt, res1); 
        auto rang2 = atom_nebors.equal_range( i1);
        for( auto itr2 = rang2.first; itr2 != rang2.second; itr2++){
          auto i2 = itr2->second;
          if (i2 != i0){
            auto ir2 = residue_of_atom.at( i2);
            auto& atom2 = *atoms.at( i2);
            auto it2 = atype( at, atom2); 
            auto& res2 = residues.at( ir2).get();
            auto irt2 = rtype( rt, res2); 
            auto rang3 = atom_nebors.equal_range( i2);
            for( auto itr3 = rang3.first; itr3 != rang3.second; itr3++){
              auto i3 = itr3->second;
              if ((i3 != i0) && (i3 != i1)){
                auto ir3 = residue_of_atom.at( i3);
                auto& atom3 = *atoms.at( i3);
                auto it3 = atype( at, atom3); 
                auto& res3 = residues.at( ir3).get();
                auto irt3 = rtype( rt, res3);
                
                if (is_chain( res0) || is_chain( res1) || is_chain( res2) || is_chain( res3)){ 
                  // diffs
                    auto d1 = ir1 - ir0;
                    auto d2 = ir2 - ir0;
                    auto d3 = ir3 - ir0;
  
                  // for now, pick the one that has the fewest wildcard terms and the lowest dihedral type number
                  Vector< pair< int, Dihedral_Type_Index> > choices;
                  for( unsigned int mask = 0; mask < 16; mask++){
                    Dihedral dihedral;
                    auto& atoms = dihedral.atoms;
                      atoms[0] = it0;
                      atoms[1] = it1;
                      atoms[2] = it2;
                      atoms[3] = it3;
                      
                    // masks  
                      unsigned int m0 = (mask >> 0) & 1;
                      unsigned int m1 = (mask >> 1) & 1;
                      unsigned int m2 = (mask >> 2) & 1;
                      unsigned int m3 = (mask >> 3) & 1;
  
                    auto& residues = dihedral.residues;
                      residues[0] = rpick( m0, irt0);
                      residues[1] = rpick( m1, irt1);
                      residues[2] = rpick( m2, irt2);
                      residues[3] = rpick( m3, irt3);
  
                    auto& diffs = dihedral.diffs;
                      diffs[0] = d1;
                      diffs[1] = d2;
                      diffs[2] = d3;
  
                    auto itr = dihedral_types.find( dihedral);
                    if (itr != dihedral_types.end()){
                      auto idh = itr->second;
                      auto sig = 4 - (m0 + m1 + m2 + m3);
                      choices.push_back( make_pair( sig, idh));
                    }
                  }
  
                  if (!choices.empty()){
                    auto iter = std::min_element( choices.begin(), choices.end());
                    auto idh = iter->second;
                    // output  
                      cout << "    <dihedral>\n";
                      cout << "      <atoms> " << i0 << " " << i1 << " " << i2 << " " << i3 << " </atoms>\n";
                      
                      if ( !is_chain( res0) || !is_chain( res1) || !is_chain( res2) || !is_chain( res3))
                        cout << "      <cores> " << res0.core << " " << res1.core << " " << res2.core << " " << res3.core << " </cores>\n";
                      
                      cout << "      <type> " << idh << " </type>\n";
                      cout << "    </dihedral>\n";
                      
                    auto desr0 = make_pair( res0.core, i0);
                    auto desr1 = make_pair( res1.core, i1);
                    auto desr2 = make_pair( res2.core, i2);
                    auto desr3 = make_pair( res3.core, i3);
                    dihedrals.push_back( make_tuple( desr0,desr1,desr2,desr3));
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  // improper dihedrals
  for( auto& item: residues){
    auto& res0 = item.second.get();
    for( auto& atom0: res0.atoms){
      auto i0 = atom0.number;
      auto ir0 = residue_of_atom.at( i0);
      auto it0 = atype( at, atom0); 
      auto irt0 = rtype( rt, res0); 
      auto rang = atom_nebors.equal_range( i0);
      for( auto itr1 = rang.first; itr1 != rang.second; itr1++){
        auto i1 = itr1->second;
        auto ir1 = residue_of_atom.at( i1);
        auto& atom1 = *atoms.at( i1);
        auto it1 = atype( at, atom1); 
        auto& res1 = residues.at( ir1).get();
        auto irt1 = rtype( rt, res1); 
        for( auto itr2 = rang.first; itr2 != rang.second; itr2++){
          auto i2 = itr2->second;
          if (i2 != i1){
            auto ir2 = residue_of_atom.at( i2);
            auto& atom2 = *atoms.at( i2);
            auto it2 = atype( at, atom2); 
            auto& res2 = residues.at( ir2).get();
            auto irt2 = rtype( rt, res2); 
            for( auto itr3 = rang.first; itr3 != rang.second; itr3++){
              auto i3 = itr3->second;
              if ((i3 != i1) && (i3 != i2)){
                auto ir3 = residue_of_atom.at( i3);
                auto& atom3 = *atoms.at( i3);
                auto it3 = atype( at, atom3); 
                auto& res3 = residues.at( ir3).get();
                auto irt3 = rtype( rt, res3);
                
                if (is_chain( res0) || is_chain( res1) || is_chain( res2) || is_chain( res3)){ 
                
                  // diffs
                    auto d0 = ir0 - ir1;
                    auto d2 = ir2 - ir1;
                    auto d3 = ir3 - ir1;
  
                  // for now, pick the one that has the fewest wildcard terms and the lowest dihedral type number
                  Vector< pair< int, Dihedral_Type_Index> > choices;
                  for( unsigned int mask = 0; mask < 16; mask++){
                    Dihedral dihedral;
                    auto& atoms = dihedral.atoms;
                      atoms[0] = it1;
                      atoms[1] = it2;
                      atoms[2] = it0;
                      atoms[3] = it3;
  
                    // masks  
                      unsigned int m0 = (mask >> 0) & 1;
                      unsigned int m1 = (mask >> 1) & 1;
                      unsigned int m2 = (mask >> 2) & 1;
                      unsigned int m3 = (mask >> 3) & 1;
  
                    auto& residues = dihedral.residues;
                      residues[0] = rpick( m1, irt1);
                      residues[1] = rpick( m2, irt2);
                      residues[2] = rpick( m0, irt0);
                      residues[3] = rpick( m3, irt3);
  
                    auto& diffs = dihedral.diffs;
                      diffs[0] = d2;
                      diffs[1] = d0;
                      diffs[2] = d3;
  
                    auto itr = dihedral_types.find( dihedral);
                    if (itr != dihedral_types.end()){
                      auto idh = itr->second;
                      auto sig = 4 - (m0 + m1 + m2 + m3);
                      choices.push_back( make_pair( sig, idh));
                    }
                  }
  
                  if (!choices.empty()){
                    auto iter = std::min_element( choices.begin(), choices.end());
                    auto idh = iter->second;
                    // output
                      cout << "    <dihedral>\n";
                      cout << "      <atoms> " << i1 << " " << i2 << " " << i0 << " " << i3 << " </atoms>\n";
                      
                      if ( !is_chain( res0) || !is_chain( res1) || !is_chain( res2) || !is_chain( res3))
                         cout << "      <cores> " << res1.core << " " << res2.core << " " << res0.core << " " << res3.core << " </cores>\n";
                       
                      cout << "      <type> " << idh << " </type>\n";
                      cout << "    </dihedral>\n";
                        
                    auto desr0 = make_pair( res0.core, i0);
                    auto desr1 = make_pair( res1.core, i1);
                    auto desr2 = make_pair( res2.core, i2);
                    auto desr3 = make_pair( res3.core, i3);
                    dihedrals.push_back( make_tuple( desr0,desr1,desr2,desr3));                                                    
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  cout << "  </dihedrals>\n";
  
}

//#####################################################################
// confusing terminology; these are used only to establish connectivity
void Doer::generate_constraint_bonds(){

  auto& rt = residue_types;
  auto& at = atom_types;

  for( auto& item: residues){
    auto ir = item.first;
    auto& residue = item.second.get();
  
    Vector< const Residue*> nebs;
    {
      auto itr = residues.find( ir - Residue_Diff(1));
      if (itr != residues.end())
        nebs.push_back( &(itr->second.get()));
    }
    nebs.push_back( &residue);
    {
      auto itr = residues.find( ir + Residue_Diff(1));
      if (itr != residues.end())
        nebs.push_back( &(itr->second.get()));
    }
  
    auto nn = nebs.size();
    for( auto ir0: range( nn)){
      auto& res0 = *(nebs[ir0]);
      auto irt0 = rtype( rt, res0); 
      for( auto ir1: range( nn)){
        auto& res1 = *(nebs[ir1]);
        auto irt1 = rtype( rt, res1); 
        Residue_Diff d( ir1 - ir0);
        for( auto& atom0: res0.atoms){
          auto att0 = atype( at, atom0); 
          for( auto& atom1: res1.atoms){
            if (&atom0 != &atom1){
              auto att1 = atype( at, atom1); 
              PBond bond;
              bond.atoms[0] = att0;
              bond.atoms[1] = att1;
              bond.residues[0] = irt0;
              bond.residues[1] = irt1;
              bond.diff = d;
            
              auto itr = constraint_types.find( bond);
              if (itr != constraint_types.end()){
                auto at0 = atom0.number;
                auto at1 = atom1.number;
                cbonds.insert( make_tuple( at0, at1));
              }
            }
          }
        }
      }
    }
  }
}

//###########################################################################
void Doer::print_angles(){
  auto& rt = residue_types;
  auto& at = atom_types;
  
  cout << "  <angles>\n";
  for( auto& item: residues){
  
    auto& res0 = item.second.get();
    auto i0 = item.first;
  
    for( auto& diff: angle_diffs){
      auto d1 = diff[0];
      auto i1 = i0 + d1;
      auto itr1 = residues.find( i1);
      if (itr1 != residues.end()){
        auto d2 = diff[1];
        auto i2 = i0 + d2;
        auto itr2 = residues.find( i2);
        if (itr2 != residues.end()){
          auto& res1 = itr1->second.get();
          auto& res2 = itr2->second.get();
          
          if (is_chain( res0) || is_chain( res1) || is_chain( res2)){
            for( auto& atom0: res0.atoms) for( auto& atom1: res1.atoms) for( auto& atom2: res2.atoms){
              Angle angle;
              auto& atoms = angle.atoms;
              auto& residues = angle.residues;
  
              auto r0 = rtype( rt, res0);
              auto r1 = rtype( rt, res1);
              auto r2 = rtype( rt, res2);
  
              auto proc = [&]( auto r0, auto r1, auto r2){
                // atom and res types
                  atoms[0] = atype( at, atom0);
                  atoms[1] = atype( at, atom1);
                  atoms[2] = atype( at, atom2);
                  residues[0] = r0;
                  residues[1] = r1;
                  residues[2] = r2;
                angle.diffs[0] = d1;
                angle.diffs[1] = d2;
                auto itr = angle_types.find( angle);
                bool found = (itr != angle_types.end());
                if (found){
                  auto i0 = atom0.number;
                  auto i1 = atom1.number;
                  auto i2 = atom2.number;
                  cout << "    <angle>\n";
                  cout << "      <atoms> " << i0 << " " << i1 << " " << i2 << " </atoms>\n";
                  
                  if (!is_chain( res0) || !is_chain( res1) || !is_chain( res2))
                    cout << "      <cores> " << res0.core << " " << res1.core << " " << res2.core << " </cores>\n";
                  
                  cout << "      <type> " << itr->second << " </type>\n";
                  cout << "    </angle>\n";
                  
                  auto desr0 = make_pair( res0.core, i0);
                  auto desr1 = make_pair( res1.core, i1);
                  auto desr2 = make_pair( res2.core, i2);                      
                  angles.push_back( make_tuple(desr0,desr1,desr2));
                }
                return found;
              };
  
              Vector< Array< Residue_Type_Index,3> > patterns{
                {r0,r1,r2},{r0,r1,rt0},{r0,rt0,r2},{rt0,r1,r2},{r0,rt0,rt0},{rt0,r1,rt0},{rt0,rt0,r2},{rt0,rt0,rt0}
              };
              apply_wc( proc, patterns);
            }
          }
        }
      }
    }
  }
  cout << "  </angles>\n";
}

//###########################################################################
void Doer::print_bonds(){
  
  auto f = [&]( Atom_Index i0, Atom_Index i1, const Residue& res0, const Residue& res1, const Bond& bond){
    
    if (!(bond.actual && use_constraints)){
    
      if (!bond.actual)
        cout << "    <bond improper=\"true\">\n";
      else
        cout << "    <bond>\n";
      
      cout << "      <atoms> " << i0 << " " << i1 << " </atoms>\n";
      
      if (!is_chain( res0) || !is_chain( res1))
        cout << "      <cores> " << res0.core << " " << res1.core << " </cores>\n";
      
      cout << "      <type> " << bond.type << " </type>\n";
      cout << "    </bond>\n";
    }
  };
  
  print_bonds_gen( f, "bonds");
}

//###########################################################################
// lots of duplication from print_bonds; clean up someday
void Doer::print_constraints(){
    
  auto f = [&]( Atom_Index i0, Atom_Index i1, const Residue& res0, const Residue& res1, const Bond& bond){
    if (use_constraints && bond.actual){
      cout << "    <length_constraint>\n";
      
      cout << "      <atoms> " << i0 << " " << i1 << " </atoms>\n";
    
      if (!is_chain( res0) || !is_chain( res1))
        cout << "      <cores> " << res0.core << " " << res1.core << " </cores>\n";
      
      cout << "      <length> " << bond.length << " </length>\n";
      cout << "    </length_constraint>\n";
    }
  };
  
  print_bonds_gen( f, "length_constraints");
}

//###########################################################################
template< class F>
void Doer::print_bonds_gen( const F& f, const std::string& name){ 
  auto& rt = residue_types;
  auto& at = atom_types;
  
  cout << "  <" << name << ">\n";
  for( auto& item: residues){
  
    auto process = [&]( Residue_Diff diff, const Residue& res0, const Residue& res1){
      for( auto& atom0: res0.atoms) for( auto& atom1: res1.atoms){
        auto r0 = rtype( rt, res0);
        auto r1 = rtype( rt, res1);
  
        auto proc = [&]( Residue_Type_Index r0, Residue_Type_Index r1){
  
          auto inner = [&]( bool is_actual){
            Bond bond;
            bond.atoms[0] = atype( at, atom0);
            bond.atoms[1] = atype( at, atom1);
            bond.residues[0] = r0;
            bond.residues[1] = r1;
            bond.diff = diff;
            bond.actual = is_actual;
            // get bond type  
            auto itr = bond_types.find( bond);
            auto found = (itr != bond_types.end());
            auto btype = itr->second;
            
            if (found){
              bond.type = btype;
              bond.length = itr->first.length;
              auto i0 = atom0.number;
              auto i1 = atom1.number;
              f( i0,i1,res0,res1,bond);
              
              auto desr0 = make_pair( res0.core, i0);
              auto desr1 = make_pair( res1.core, i1);
              bonds.push_back( make_tuple( desr0,desr1));
            }
            return found;
          };
  
          return (inner( true) || inner( false));
        };
  
        Vector< Array< Residue_Type_Index,2> > patterns{ {r0,r1},{r0,rt0},{rt0,r1},{rt0,rt0}};
        apply_wc( proc, patterns);
      }
    };
  
    auto i = item.first;
    auto& res0 = item.second.get();
    auto ip = i + (Residue_Index(1) - Residue_Index(0));
    auto itr = residues.find( ip);
    if (itr != residues.cend()){
      auto& res1 = itr->second.get();
      
      if (is_chain( res0) || is_chain( res1))
        process( Residue_Diff(1), res0, res1);
    }
  
    if (is_chain( res0))
      process( Residue_Diff(0), res0, res0);
  }
  cout << "  </" << name << ">\n";
}


//##########################################################################
void Doer::setup_dihedral_types( Parser& parser, Node_Ptr node){
  
  // is it needed?
  set< Array< Residue_Diff, 3> > dihedral_diffs;
  
  auto get_dihedral_info = [&]( Node_Ptr node){
    node->complete();
    auto orders = checked_array_from_node< Residue_Index, 4>( node, "orders");
    auto type = checked_value_from_node< Dihedral_Type_Index>( node, "index");
    Dihedral dihedral;
    dihedral.atoms = checked_array_from_node< Atom_Type_Index,4>( node, "atoms");
    dihedral.residues = checked_array_from_node< Residue_Type_Index,4>( node, "residues");
    dihedral.diffs[0] = orders[1] - orders[0];
    dihedral.diffs[1] = orders[2] - orders[0];
    dihedral.diffs[2] = orders[3] - orders[0];
    dihedral_types.insert( make_pair( dihedral, type));
    dihedral_diffs.insert( dihedral.diffs);
  };

  auto ldiheds = [&]( auto& dnode){ 
    parser.apply_to_nodes_of_tag( dnode, "potential", get_dihedral_info);
  };
  
  Array< std::string, 1> tag{ "potentials"};
  apply_to_each_child_with_tag( node, tag, ldiheds);
}

//####################################################################
void Doer::setup_angle_types_and_diffs( Parser& parser, Node_Ptr node){
     
  auto get_angle_info = [&]( Node_Ptr node){
    node->complete();
    auto orders = checked_array_from_node< Residue_Index, 3>( node, "orders");
    auto type = checked_value_from_node< Angle_Type_Index>( node, "index");
    Angle angle;
    angle.atoms = checked_array_from_node< Atom_Type_Index,3>( node, "atoms");
    angle.residues = checked_array_from_node< Residue_Type_Index,3>( node, "residues");
    angle.diffs[0] = orders[1] - orders[0];
    angle.diffs[1] = orders[2] - orders[0];
    angle_types.insert( make_pair( angle, type));
    angle_diffs.insert( angle.diffs);
  };
  
  auto langles = [&]( auto& dnode){
    parser.apply_to_nodes_of_tag( dnode, "potential", get_angle_info);
  };
   
  Array< std::string, 1> tag{ "potentials"};
  apply_to_each_child_with_tag( node, tag, langles);
}

//#############################################################
void Doer::setup_bond_types( Parser& parser, Node_Ptr node){
    
  auto get_pair_info = [&]( Node_Ptr node){
    
    node->complete();
    auto orders = checked_array_from_node< Residue_Index, 2>( node, "orders");
        
    if (orders[0] != ir0 || orders[1] != ir0){
      auto type = checked_value_from_node< Bond_Type_Index>( node, "index");
      Bond bond;
      bond.atoms = checked_array_from_node< Atom_Type_Index,2>( node, "atoms");
      bond.residues = checked_array_from_node< Residue_Type_Index,2>( node, "residues");
      bond.diff = orders[1] - orders[0];
  
      auto& attrs = node->attributes();
      auto opt = attrs.value( "bond");
      if (opt && opt.value() == "true"){
        bond.actual = true;
        bond.length = checked_value_from_node< Length>( node, "eq_length");
      }
      bond_types.insert( make_pair( bond, type));
    }
  };
  
  auto lpairs = [&]( auto& dnode){    
    parser.apply_to_nodes_of_tag( dnode, "potential", get_pair_info);
  };
  
  Array< std::string, 1> tag{ "potentials"};
  apply_to_each_child_with_tag( node, tag, lpairs);
}

//##################################################################################
void Doer::setup_constraint_types( const std::string& con_file){

  std::set< string> res_names;
  for( auto& item: residues){
    res_names.insert( item.second.get().name);
  }
    
  XML_Info xinfo( con_file);
  auto top = xinfo.top;
  auto bnodes = top->children_of_tag( "bond");
  for( auto bnode: bnodes){
    auto names = checked_array_from_node< string, 2>( bnode, "residues");
    Vector< pair< string,string> > rpairs;
    if (names[0] == "XXX"){
      if (names[1] == "XXX"){
        for( auto& rname0: res_names)
          for( auto& rname1: res_names)
            rpairs.emplace_back( rname0, rname1);
      }
      else{
        auto& rname1 = names[1];
        for( auto& rname0: res_names)
          rpairs.emplace_back( rname0, rname1);
      }
    }
    else if (names[1] == "XXX"){
      auto& rname0 = names[0];
      for( auto& rname1: res_names)
        rpairs.emplace_back( rname0, rname1);
    }
    else{
      rpairs.emplace_back( names[0], names[1]);
    }

    // set up constraint types
    auto atoms = checked_array_from_node< string, 2>( bnode, "atoms");
    auto at0 = atom_types.at( atoms[0]);
    auto at1 = atom_types.at( atoms[1]);
    auto orders = checked_array_from_node< Residue_Index, 2>( bnode, "orders");
    for( auto& rpair: rpairs){
      PBond cons;
      cons.atoms[0] = at0;
      cons.atoms[1] = at1;
      cons.diff = orders[1] - orders[0];
      cons.residues[0] = residue_types.at( rpair.first);
      cons.residues[1] = residue_types.at( rpair.second);
      constraint_types.insert( cons);
    }
  }
}

//##############################################################################
void Doer::setup_residue_and_atom_types( const std::string& param_file){
  
  std::ifstream input( param_file);
  Parser parser( input, param_file);

  // get nodes
  auto top = parser.top();
  
  Array< std::string, 1> tag{ "types"};
  
  auto ltypes = [&]( auto& tnode){
    tnode->complete();  
    auto anode = tnode->child( "atoms");
    auto rnode = tnode->child( "residues");
  
    auto type_map = [&]( auto dum_index, Node_Ptr node){
      typedef decltype( dum_index) IType;
      auto tnodes = node->children_of_tag( "type");
      map< string, IType> res;
      for( auto tnode: tnodes){
        auto name = checked_value_from_node< string>( tnode, "name");
        auto index = checked_value_from_node< IType>( tnode, "index");
        res.insert( make_pair( name, index));
      }
      return res;
    };
  
    residue_types = type_map( Residue_Type_Index(), rnode);
    atom_types = type_map( Atom_Type_Index(), anode);
  };
  
  auto success = apply_to_each_child_with_tag( top, tag, ltypes);
  if (!success){
    top->perror( "no types");
  }
}

//#################################################################################
void Doer::setup_atom_pointers(){

  for( auto& item: residues){
    auto& residue = item.second.get();
    for( auto& atom: residue.atoms)
      atoms.insert( make_pair( atom.number, &atom));
  }
}

//##############################################################################
void Doer::setup_residues_index_map(){
  
  //Residue_Index_Map residues;
  
  // constants
  const Core_Index ci0( 0), ci1( 1), ci2( 2);
  const auto nc = cores.size();
  const std::string rmsg( "cannot have residue number");
  
  // inserter functions
  
    auto insert_residue = [&]( const Residue& res){
      residues.insert( make_pair( res.number, std::cref( res)));      
    };
  
    auto insert_chain_reses = [&](){
      for( auto& item: chain_residues)
        insert_residue( item.second);
    };
    
    // include enough core residues to form a dihedral
    auto insert_core_below = [&]( auto& core_res, Residue_Index m){
      for( int i = -2; i <= 0; i++){
         auto ir = m + i*rid;
         if (index_found( core_res, ir)){
           insert_residue( core_res.at(ir).get());
         }
      }      
    };
    
    auto insert_core_above = [&]( auto& core_res, Residue_Index m){
      for( int i = 0; i <= 2; i++){
         auto ir = m + i*rid;
         if (index_found( core_res, ir))
           insert_residue( core_res.at(ir).get());
      }      
    };
  // end  
  
  if (cores.empty())
    insert_chain_reses();
  
  else if (nc == ci1){ // one core
    auto& cj = core_junctions[ ci0];
    auto& joints = cj.joints;
    auto ncj = joints.size();
    
    if (ncj == 1){
      // chain attaches at one place
      
      Residue_Index m,c;
      tie( m,c) = joints[ 0];
      
      auto& core_res = core_residues[ci0];
      
      if (!index_found( core_res, m))
         error( "residue", m, "not found in core");
        
      if (!index_found( chain_residues, c))
        error( "residue", c, "not found in chain");
      
      bool hfound = index_found( chain_residues, c+rid);
      bool lfound = index_found( chain_residues, c-rid);
      
      if (hfound){
        // low end of chain attaches
        if (lfound)
          error( rmsg, c-rid, "in chain");
        if (index_found( core_res, m+rid))
          error( rmsg, m+rid, "in core");
        
        insert_core_below( core_res, m);
        insert_chain_reses();
      }
      else if (lfound){
        // high end of chain attaches
        if (hfound)
          error( rmsg, c+rid, "in chain");
        if (index_found( core_res, m-rid))
          error( rmsg, m-rid, "in core");
        
        insert_chain_reses();
        insert_core_above( core_res, m);
      }
      else{
        // one-residue chain
        bool chfound = index_found( core_res, c+rid);
        bool clfound = index_found( core_res, c-rid);
        if (chfound){
          // core residues ascend
          if (clfound)
            error( rmsg, c+rid, "in core");
          
          insert_chain_reses();
          insert_core_above( core_res, m);
        }
        else if (clfound){
          // core residues descend
          if (chfound)
            error( rmsg, c-rid, "in core");
          
          insert_core_below( core_res, m);
          insert_chain_reses();
          
        }
        else{
          error( "need more than one residue on either chain or core");
        }
      }
      
    }
    else if (ncj == 2){
      // chain attaches at two places
      // constants
        auto& core_res = core_residues[ci0];
        Residue_Index m0i,c0i,m1i,c1i;
        tie( m0i,c0i) = joints[ 0];
        tie( m1i,c1i) = joints[ 1];
        
        Residue_Index m0,c0,m1,c1;
        if (c0i < c1i){
          c0 = c0i;
          c1 = c1i;
          m0 = m0i;
          m1 = m1i;
        }
        else{
          c1 = c0i;
          c0 = c1i;
          m1 = m0i;
          m0 = m1i;        
        }
      // end
        
      // error check
        if (index_found( chain_residues, c0-rid))
          error( rmsg, c0-rid, "in chain");
        if (index_found( chain_residues, c1+rid))
          error( rmsg, c1+rid, "in chain");
        if (index_found( core_res, m0+rid))
          error( rmsg, m0+rid, "in core ", core_res.at(m0).get().core);
        if (index_found( core_res, m1-rid))
          error( rmsg, m1-rid, "in core ", core_res.at(m1).get().core);
      // end
      
      // insertions
        insert_core_below( core_res, m0);
        insert_chain_reses();
        insert_core_above( core_res, m1);
    }
    else{
      error( "number of core-chain refs cannot be", ncj);
    }
  }
  else if (nc == ci2){ // two cores
    // constants
      auto& cj0 = core_junctions[ ci0];
      auto& cj1 = core_junctions[ ci1];
      auto& joints0 = cj0.joints;
      auto& joints1 = cj1.joints;
      if (joints0.size() != 1 || joints1.size() != 1)
        error( "wrong number of core-chain residue index pairs");
      Residue_Index m0i,c0i,m1i,c1i;
      tie( m0i,c0i) = joints0[ 0];
      tie( m1i,c1i) = joints1[ 0];
         
    // end
    
    // error checks
      if (!index_found( core_residues[ci0], m0i))
         error( "residue", m0i, "not found in first core");
      if (!index_found( core_residues[ci1], m1i))
        error( "residue", m1i, "not found in second core");
      if (!index_found( chain_residues, c0i))
        error( "residue", c0i, "not found in chain");
      if (!index_found( chain_residues, c1i))
        error( "residue", c1i, "not found in chain");
    // end
    
    // constants
      const bool ascending = (c0i < c1i);
      Residue_Index m0,c0,m1,c1;
      if (ascending){
        c0 = c0i;
        c1 = c1i;
        m0 = m0i;
        m1 = m1i;
      }
      else{
        c1 = c0i;
        c0 = c1i;
        m1 = m0i;
        m0 = m1i;        
      }
      
      auto& core_res0 = ascending ? core_residues[ci0] : core_residues[ci1];
      auto& core_res1 = ascending ? core_residues[ci1] : core_residues[ci0];
    // end
    
    
    // error checks
      if (index_found( chain_residues, c0-rid))
        error( rmsg, c0-rid, "in chain");
      if (index_found( chain_residues, c1+rid))
        error( rmsg, c1+rid, "in chain");
      if (index_found( core_res0, m0+rid))
        error( rmsg, m0+rid, "in core ", core_res0.at(m0).get().core);
      if (index_found( core_res1, m1-rid))
        error( rmsg, m1-rid, "in core ", core_res1.at(m1).get().core);
    // end
    
    // insertions
      insert_core_below( core_res0, m0);
      insert_chain_reses();
      insert_core_above( core_res1, m1);
    }
  
  
  //return residues;
}

//##########################################################################################
void Doer::setup_chain_residues_of_file( const std::string& res_file){
  
  //map< Residue_Index, Residue> chain_residues; 
  
  auto res_array = residues_of_file( res_file);
  for( auto& res: res_array){
    res.core = "*";
    chain_residues.insert( make_pair( res.number, std::move(res)));
  }
  // check for contiguous numbering
  Residue_Index lo( maxi), hi( 0);
  for( auto& item: chain_residues){
    auto ir = item.first;
    lo = min( ir, lo);
    hi = max( ir, hi);
  }
  
  for( auto ir: range( lo, hi+rid))
    if (!index_found( chain_residues, ir))
      error( "missing chain residue", ir);
    
  //return chain_residues;  
}

//###############################################################
template< class Key, class Res>
bool index_found( const std::map< Key, Res>& rmap, Key i){
  return rmap.find( i) != rmap.end();
}

//##########################################################################################
void Doer::setup_core_residues_of_cores() {
  
  //Core_Residues core_residues;
  for( auto& cvec: cores){
    core_residues.emplace_back();
    auto& cmap = core_residues.back();    
    for( auto& res: cvec){
      cmap.insert( make_pair( res.number, std::cref(res)));
    }
  }
  
  //return core_residues;
}

//###############################################################################################
void Doer::setup_cores_of_input( int argc, char* argv[]){

  //Vector< Vector< Residue, Residue_Index>, Core_Index> cores;
  //Vector< Core_Junction, Core_Index> core_junctions;
  {
    enum class Mode {Core_Name, Core_File, Core_Atom, Chain_Atom};
    auto mode = Mode::Core_Name;
    auto core_info = string_args( argc, argv, "-cores");
    std::string cname;
    Core_Index im( 0);
    for( auto& token: core_info){
       
      auto nt = token.size();
      if (mode == Mode::Core_Name){
        cname = token;
        mode = Mode::Core_File;
      }
      else if (mode == Mode::Core_File){
        if (nt > 3 && token.substr( nt-3, 3) == "xml"){
          cores.emplace_back( residues_of_file( token));
          
          auto& core = cores[im];
          for( auto& res: core){
            res.core = cname;
          }
          
          core_junctions.emplace_back();
          auto& cj = core_junctions.back();
          cj.core = cores.size();
          cj.name = cname;
          mode = Mode::Core_Atom;
        }
        else{
          error( "not a file name", token);
        }
        ++im;
      }
      else if (mode == Mode::Core_Atom){
        std::stringstream ss;
        ss << token;
        int i;
        ss >> i;
        if (ss.fail()){ // not an int, must be next core name
          cname = token;
          mode = Mode::Core_File;
        }
        else{
          auto& joints = core_junctions.back().joints;
          joints.emplace_back();
          joints.back().first = Residue_Index(i);
          mode = Mode::Chain_Atom;
        }
      }
      else{ // mode == Mode::Chain_Atom
        try{
          Residue_Index i( stoi( token));
          auto& joints = core_junctions.back().joints;
          joints.back().second = Residue_Index(i);
          mode = Mode::Core_Atom;
        }
        catch( std::invalid_argument& exc){
          error( "invalid atom number", token);
        }
      }
    }
  }
}

//#############################################################
void Doer::ask_to_use_constraints( int argc, char* argv[]){
  
  use_constraints = has_flag( argc, argv, "-constraints");
}  

}

int main( int argc, char* argv[]){
  Browndye::Doer doer;
  doer.main( argc, argv);
}
