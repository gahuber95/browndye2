#pragma once

#include "../../lib/units.hh"
#include "../../lib/vector.hh"
#include "../../xml/node_info.hh"

namespace Browndye{

enum class Energy_Units{ Default, Kcal_per_moles};

class Parameter_Info{
public:
  
  // convert kcal/mol to kT @ T = 298 K
  // k_b = 0.0019872041 kcal/(mol K), so factor = 1/(k_b*298)
  static
  constexpr double energy_conv_factor = 1.688656287;
  
  Energy_Units energy_units = Energy_Units::Default;

  void set_energy_units( const JAM_XML_Pull_Parser::Node_Ptr& node);

  Vector< Energy> converted_energy( Vector< Energy>& potentials) const;
  
  template< class T>
  T converted_energy( T) const;
};
  
//######################################################## 
template< class T>
T Parameter_Info::converted_energy( T v) const{
  if (energy_units == Energy_Units::Kcal_per_moles){
    return energy_conv_factor*v;
  }
  else{
    return v;
  }
}
 
}
  
