#pragma once
/*
 * physical_constants.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */


#include "../lib/units.hh"

namespace Browndye{
  
const Viscosity water_viscosity( 0.243);
const Permittivity vacuum_permittivity( 0.000142);
}

