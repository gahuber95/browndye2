#include "trajectory_outputter.hh"

// Implements Trajectory_Outputter class

namespace Browndye{

const char endL = '\n';

// constructor
Trajectory_Outputter::Trajectory_Outputter( const System_Common& system,
                                             const std::string& out_file,
                                             const std::string& index_out_file,
                                             size_t n_states_per_block):
  out( out_file), index_out(index_out_file){
  
  if (!out.is_open()){
    error( "Trajectory_Outputter: output file", out_file, "not open");
  }
  
  nstates = n_states_per_block;
  
  states.resize( nstates);
  
  size_t nelements = 0; // later add useful things like dt
  nelements += 1; // for dt (previously time)
  for( auto& group: system.groups){
    nelements += (3 + 4)*(group.cores.size()/Core_Index(1));
    for( auto& chain: group.chains){
      nelements += 3*(chain.atoms.size()/Atom_Index(1));
    }
  }
  if (system.do_staged){
    ++nelements;
  }
  
  state_output.resize( nstates*nelements);
  
  out << "<trajectories>" << endL;

  auto model_type = force_field_rep( system.force_field);
  
  out << "  <force_model_type> " << model_type << "</force_model_type>" << endL;
  out << "  <n_states_per_record> " << nstates << " </n_states_per_record>" << endL;

  out << "  <core_centers>" << endL;
  for( auto& group: system.groups){
    for( auto& core: group.cores){
      auto pos = core.hydro_center;
      out << "    <center> " << pos[0] << " " << pos[1] << " " << pos[2]
          << " </center>" << endL;
    }
  }
  out << "  </core_centers>" << endL;
  
  out << "  <atom_files>" << endL;
  for( auto& group: system.groups){
    out << "    <group>" << endL;
    if (!group.cores.empty())
      for( auto& core: group.cores)
        out << "      <core> " << core.atoms_file << " </core>" << endL;
      
    if (!group.chains.empty()){
      for( auto& chain: group.chains){
        if (!chain.atoms.empty()){
          out << "      <chain> " << chain.chain_file << " </chain>" << endL;
        }
      }
    }
      
    out << "    </group>" << endL;
  }
  out << "  </atom_files>" << endL;
  
  out << "  <fields>" << endL;
  out << "    <field> dt 1 </field>\n";
  for( auto& group: system.groups){
    if (!group.cores.empty()){
      for( auto im: range( group.cores.size())){
        (void)im;
        out << "    <field> core " << (3+4) << " </field>" << endL;
      }
    }
        
    for( auto& chain: group.chains){
      if (!chain.atoms.empty()){
        out << "    <field> chain " << 3*(chain.atoms.size()/Atom_Index(1))
            << " </field>" << endL;
      }
    }
    if (system.do_staged){
      out << "    <field> bin 1 </field>\n";
    }
  }
  out << "  </fields>" << endL;

  itraj = 0;
  isubtraj = 0;
  istate = 0;
  last_pos = out.tellp();
  out << "</trajectories>" << endL; 

  index_out << "<trajectories>" << endL;
  last_index_pos = index_out.tellp();
  index_out << "</trajectories>" << std::endl;
}

//############################################
void Trajectory_Outputter::end_trajectories(){}

void print_space( std::ostream& out, int n){
  for (int i = 0; i < n; i++)
    out << " ";
}

//############################################
void print_fate( System_Fate fate, int ns, std::ostream& out){
  print_space( out, ns);
  if (fate == System_Fate::Final_Rxn)
    out << "<fate> reacted </fate>" << endL;
  else if (fate == System_Fate::Escaped)
    out << "<fate> escaped </fate>" << endL;
  else if (fate == System_Fate::Stuck)
    out << "<fate> stuck </fate>" << endL;
  else
    out << "<fate> in_progress </fate>" << endL;
}

//############################################
void Trajectory_Outputter::end_trajectory( System_Fate fate, const String& rxn, 
                                           const String& rxn_state, Length min_rxn_coord){
  ++itraj;

  out.seekp( last_pos);
  print_fate( fate, 4, out);
  out << "    <rxn_state> " << rxn_state << " </rxn_state>" << endL;
  if (fate == System_Fate::Final_Rxn)
    out << "    <rxn> " << rxn << " </rxn>" << endL;
  out << "    <min_rxn_coord> " << fvalue( min_rxn_coord) << "</min_rxn_coord>" << endL;
  out << "  </trajectory>" << endL;
  last_pos = out.tellp();
  out << "</trajectories>" << endL;

  index_out.seekp( last_index_pos);
  index_out << "  </trajectory>" << endL;
  last_index_pos = index_out.tellp();
  index_out << "</trajectories>" << endL;
  
  out.flush();
}

//############################################
void Trajectory_Outputter::print_end_subtrajectory( System_Fate fate, 
                                                    const String& rxn,
                                                    const String& rxn_state,
                                                    std::ostream& sout){
  print_fate( fate, 6, sout);
  sout << "      <rxn_state_end> " << rxn_state << " </rxn_state_end>" << endL;
  
  if (not ((fate == System_Fate::Escaped) || (fate == System_Fate::Stuck)))
    sout << "      <rxn> " << rxn << " </rxn>" << endL; 
}
 
//############################################ 
void Trajectory_Outputter::end_subtrajectory( System_Fate fate, 
                                              const String& rxn,  
                                              const String& rxn_state){
  ++isubtraj;
  write_states();
  out.seekp( last_pos);
  print_end_subtrajectory( fate, rxn, rxn_state, out);

  out << "    </subtrajectory>" << endL;   

  last_pos = out.tellp();
  out << "  </trajectory>" << endL;
  out << "</trajectories>" << endL;
  
  index_out.seekp( last_index_pos);
  print_end_subtrajectory( fate, rxn, rxn_state, index_out);
  index_out << "    </subtrajectory>" << endL;
  last_index_pos = index_out.tellp();
  index_out << "  </trajectory>" << endL;
  index_out << "</trajectories>" << endL;
}

//############################################
void Trajectory_Outputter::start_trajectory(){
  
  istate = 0;
  isubtraj = 0;
  out.seekp( last_pos);
  traj_start = out.tellp();
  out << "  <trajectory>" << endL;
  out << "    <n_traj> " << itraj << " </n_traj>" << endL;
  last_pos = out.tellp();

  index_out.seekp( last_index_pos);
  index_out << "  <trajectory>" << endL;
  index_out << "    <n_traj> " << itraj << " </n_traj>" << endL;
  index_out << "    <start> " << traj_start << " </start>" << endL;
  last_index_pos = index_out.tellp();
}

//############################################
void Trajectory_Outputter::start_subtrajectory( const String& rxn_state){

  out.seekp( last_pos);
  subtraj_start = out.tellp();
  out << "    <subtrajectory>" << endL;
  out << "      <n_subtraj> " << isubtraj << " </n_subtraj>" << endL;
  out << "      <rxn_state> " << rxn_state.c_str() << " </rxn_state>" << endL;
  last_pos = out.tellp();
  out << "    </subtrajectory>" << endL;
  out << "  </trajectory>" << endL;
  out << "</trajectories>" << std::endl;

  index_out.seekp( last_index_pos);
  index_out << "    <subtrajectory>" << endL;
  index_out << "      <n_subtraj> " << isubtraj << " </n_subtraj>" << endL;
  index_out << "      <start> " << subtraj_start << " </start>" << endL;
  index_out << "      <rxn_state> " << rxn_state << " </rxn_state>" << endL;
  last_index_pos  = index_out.tellp();
  index_out << "    </subtrajectory>" << endL;
  index_out << "  </trajectory>" << endL;
  index_out << "</trajectories>" << std::endl;
}

//############################################
void Trajectory_Outputter::write_states(){

  if (istate > 0){
    size_t ks = 0;
    for (size_t kstate=0; kstate < istate; ++kstate){
      const State& state = states[ kstate];
      
      state_output[ks] = state.dt/Time(1.0);
      ks += 1;
      for( auto& group: state.groups){
        for( auto& core: group.cores){
          auto trans = core.trans;
          for( auto k: range( 3))
            state_output[ ks+k] = trans[k]/Length(1.0);
          ks += 3;
          
          auto quat = core.quat;
          for( auto k: range( 4))
            state_output[ ks+k] = quat[k];
          ks += 4;
        }
        
        for( auto& chain: group.chains){
          for( auto pos: chain){
            for (auto k: range(3)){
              state_output[ ks+k] = pos[k]/Length(1.0);
            }
            ks += 3;
          }
        }
      }
      if (state.ibin){
        auto ib = state.ibin.value();
        state_output[ks] = (double)(ib/Bin_Index(1));
        ++ks;
      }
    }
    auto str = string_of_floats( state_output, ks);
    out.seekp( last_pos);
    
    if (istate < nstates){
      out << "      <s>" << endL;
      out << "        <n> " << istate << " </n>" << endL;
      out << "<data>" << str << "</data>" << endL;
      out << "      </s>" << endL;
    }
    else{
      out << "<s>" << str << "</s>" << endL;
    }
    
    last_pos = out.tellp();
    out << "    </subtrajectory>" << endL;
    out << "  </trajectory>" << endL;
    out << "</trajectories>" << std::endl;
  }
}

//############################################
void Trajectory_Outputter::
output_state( const System_State& sys){
  
  State& state = states[ istate];
  
  auto ng = sys.groups.size();
  state.dt = sys.t_since_output;
  state.groups.resize( ng);
  for( auto gi: range( ng)){
    auto& sgroup = state.groups[gi];
    auto& group = sys.groups[gi];
    
    auto nm = group.core_states.size();
    sgroup.cores.resize( nm);
    for( auto mi: range( nm)){
      auto& score = sgroup.cores[mi];
      auto& core = group.core_states[mi];
      score.trans = core.translation();
      auto rot = core.rotation();
      score.quat = quat_of_mat( rot);
    }
    
    auto nc = group.chain_states.size();
    sgroup.chains.resize( nc);
    for( auto ci: range( nc)){
      auto& schain = sgroup.chains[ci];
      auto& chain = group.chain_states[ci];
      auto na = chain.atoms.size();
      schain.resize( na);
      for( auto ai: range( na)){
        schain[ai] = chain.atoms[ai].pos;
      }
    }
  }
  if (sys.common().do_staged){
    state.ibin = sys.ibin.value();
  }
  
  ++istate;
  if (istate == nstates){
    write_states();
    istate = 0;
  }
}

}

