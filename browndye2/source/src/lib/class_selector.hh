#pragma once

#include <type_traits>

namespace Browndye{

template< bool tf, class T0, class T1>
class Chooser{
public:
  typedef typename std::conditional< tf,T0,T1>::type Res;
  
  static
  Res& value([[maybe_unused]] T0& t0, Res& t1){
    return t1;
  }

  static
  Res& value([[maybe_unused]] const T0& t0, Res& t1){
    return t1;
  }

  static
  const Res& value([[maybe_unused]] T0& t0, const Res& t1){
    return t1;
  }

  static
  const Res& value([[maybe_unused]] const T0& t0, const Res& t1){
    return t1;
  }
  
  static
  Res& value( Res& t0, [[maybe_unused]] T1& t1){
    return t0;
  } 
  
  static
  const Res& value( const Res& t0, [[maybe_unused]] T1& t1){
    return t0;
  }

  static
  Res& value( Res& t0, [[maybe_unused]] const T1& t1){
    return t0;
  }

  static
  const Res& value( const Res& t0, [[maybe_unused]] const T1& t1){
    return t0;
  }

};

template< bool tf, class T0, class T1>
typename std::conditional< tf,T0,T1>::type& 
choice( T0& t0, T1& t1){

  return Chooser< tf,T0,T1>::value( t0,t1); 
}

template< bool tf, class T0, class T1>
typename std::conditional< tf,const T0,T1>::type&
choice( const T0& t0, T1& t1){

  return Chooser< tf,T0,T1>::value( t0,t1);
}

template< bool tf, class T0, class T1>
typename std::conditional< tf,T0, const T1>::type&
choice( T0& t0, const T1& t1){

  return Chooser< tf,T0,T1>::value( t0,t1);
}

template< bool tf, class T0, class T1>
typename std::conditional< tf,const T0, const T1>::type&
choice( const T0& t0, const T1& t1){

  return Chooser< tf,T0,T1>::value( t0,t1);
}

//class Nothing{};

}
