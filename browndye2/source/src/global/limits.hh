#pragma once

#include <limits>

namespace Browndye{
//constexpr double infinity = std::numeric_limits< double>::infinity();
constexpr double large = std::numeric_limits< double>::max();
constexpr double neg_large = std::numeric_limits< double>::lowest();
constexpr double NaN = std::numeric_limits< double>::quiet_NaN();

constexpr std::size_t maxi = std::numeric_limits< std::size_t>::max();
}
