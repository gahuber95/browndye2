#pragma once

/*
 * system_common.hh
 *
 *  Created on: Aug 17, 2016
 *      Author: root
 */

#include <mutex>
#include "../global/physical_constants.hh"
#include "../structures/solvent.hh"
#include "../motion/time_step_parameters.hh"
#include "../group/group_common.hh"
#include "../pathways/pathways.hh"
#include "../pathways/rxn_interface.hh"
#include "../forces/other/nonbonded_parameter_info.hh"
#include "../forces/other/bonded_parameter_info.hh"
#include "../motion/outer_propagator.hh"
#include "../forces/force_field_type.hh"
#include "../group/test_tag.hh"
#include "../forces/electrostatic/single_grid.hh"

namespace Browndye{

/*
class Density_I{
public:
  typedef Energy Potential;
  typedef double Charge;
  typedef double Permittivity;
  typedef Force Potential_Gradient;
};
*/

class System_State;

class System_Common{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
    
  System_Common() = delete;
  explicit System_Common( Node_Ptr);
  System_Common( const System_Common&) = delete;
  System_Common& operator=( const System_Common&) = delete;
  System_Common( System_Common&&) = delete;
  System_Common& operator=( System_Common&&) = delete;
  
  explicit System_Common(Test_Tag);

  Force_Field_Type force_field = Force_Field_Type::Molecular_Mechanics;

  [[nodiscard]] Viscosity viscosity() const;
  void output( std::ofstream&) const;

  [[nodiscard]] bool has_pathway() const;
  [[nodiscard]] Rxn_Index n_reactions() const;
  [[nodiscard]] std::string reaction_name( Rxn_Index) const;
  [[nodiscard]] size_t n_reactions_from( State_Index) const;
  [[nodiscard]] Rxn_Index reaction_from( State_Index, size_t) const;
  [[nodiscard]] Length reaction_coordinate( const System_State&, Rxn_Index) const;
  [[nodiscard]] std::tuple< Length, Diffusivity, Length>
    reaction_coord_n_diffusivity( const System_State&, Rxn_Index) const;
  
  [[nodiscard]] std::pair< bool, Rxn_Index>
    next_completed_reaction( State_Index, const System_State&) const;
    
  [[nodiscard]] State_Index state_after_reaction( Rxn_Index) const;
  [[nodiscard]] State_Index first_state() const;
  [[nodiscard]] std::string state_name( State_Index) const;
  //void set_do_staged();
  //bool doing_staged() const;

  // data
  Solvent solvent;
  Time_Step_Parameters ts_params;
  Length b_rad = Length( NAN), q_rad = Length( NAN);
  bool start_at_site = false;
  
  std::unique_ptr< Nonbonded_Parameter_Info> pinfo;
  std::unique_ptr< Bonded_Parameter_Info> bpinfo;

  //std::mutex mutex;

  Vector< Group_Common, Group_Index> groups;
  
  std::unique_ptr< Single_Grid< double> > density_field;
  
   // for communicating with pathway 
  struct Mol_Rxn_Ref{
    Group_Index ig;
    class Nothing{};
    
    std::variant< Nothing, Core_Index, Chain_Index, Dummy_Index> core_or_chain;
    
    [[nodiscard]] bool is_core() const{
      return std::holds_alternative< Core_Index>( core_or_chain);
    }
    
    [[nodiscard]] bool is_chain() const{
      return std::holds_alternative< Chain_Index>( core_or_chain);
    }
    
    [[nodiscard]] bool is_dummy() const {
      return std::holds_alternative< Dummy_Index>( core_or_chain);
    }
    
    [[nodiscard]] Core_Index core_index() const{
      return std::get< Core_Index>( core_or_chain);
    }
    
    [[nodiscard]] Chain_Index chain_index() const{
      return std::get< Chain_Index>( core_or_chain);
    }
    
    [[nodiscard]] Dummy_Index dummy_index() const {
      return std::get< Dummy_Index>( core_or_chain);
    }
    
    Mol_Rxn_Ref( Group_Index, Core_Index);
    Mol_Rxn_Ref( Group_Index, Chain_Index);
    Mol_Rxn_Ref( Group_Index, Dummy_Index);
    Mol_Rxn_Ref();
  };
  
  struct Restraint{
    Array< std::pair< Mol_Rxn_Ref, Atom_Index>, 2> markers;
    Length length{ NAN};
  };
  
  Vector< Restraint, Restraint_Index> restraints;
  Vector< Mol_Rxn_Ref, Cor_Cha_Index> mol_rxn_refs;
  
  std::unique_ptr< Outer_Propagator> outer_propagator;
  
  bool building_bins = false;
  bool has_hi = true;
  size_t n_steps_per_hi_update = 1;
  bool no_nam = false;
  bool do_staged = false;
  
  [[nodiscard]] Vector< std::string, Rxn_Index> rxn_names() const;
  
private:
  std::unique_ptr< Pathways::Pathway< Rxn_Interface> > pathway;
  
  [[nodiscard]] Length computed_bradius() const;
  void setup_atom_lone_types( Node_Ptr);
  void setup_force_fields( const Node_Ptr&);
  [[nodiscard]] std::unique_ptr< Outer_Propagator> new_outer_propagator() const;
  void setup_reaction_pathway( Node_Ptr);
  void setup_groups( const Node_Ptr&);
  void setup_reaction_type_and_radii( const Node_Ptr&);
  void check_for_starting_at_site( const Node_Ptr&);
  [[nodiscard]] static std::unique_ptr< Single_Grid< double> > new_density_field( Node_Ptr) ;
  void setup_solvent( const Node_Ptr&);
  void check_for_lone_frozen() const;
  [[nodiscard]] static std::pair< bool, size_t> has_hydrodynamic_interactions_with_n_update( const Node_Ptr& top) ;
  
  typedef Pathways::Rxn_Pair< Rxn_Interface> Rxn_Pair;
  
  [[nodiscard]] Diffusivity reaction_diffusivity( const System_State&, Rxn_Pair) const;
  [[nodiscard]] Pos core_atom_pos( const System_State&, Mol_Rxn_Ref, Atom_Index) const;
  [[nodiscard]] static Pos chain_atom_pos( const System_State&, Mol_Rxn_Ref, Atom_Index) ;
  [[nodiscard]] Diffusivity core_trans_diff( const Core_Common&) const;
  [[nodiscard]] Inv_Time core_rot_diff( const Core_Common&) const;
  
  [[nodiscard]] std::tuple< Diffusivity, Inv_Time, Pos>
  diffs_n_center( const System_State&, Mol_Rxn_Ref) const;
  
  [[nodiscard]] Diffusivity chain_atom_diffusivity( const System_State&,
                                      Mol_Rxn_Ref, Atom_Index) const;

  [[nodiscard]] Diffusivity core_chain_atoms_diffu( const System_State&,
                                     Mol_Rxn_Ref, Atom_Index,
                                      Mol_Rxn_Ref, Atom_Index) const;
  
  [[nodiscard]] Diffusivity core_core_atoms_diffu( const System_State&,
        Mol_Rxn_Ref, Atom_Index, Mol_Rxn_Ref, Atom_Index) const;
  
  void setup_restraints( const Node_Ptr&);
  [[nodiscard]] Atom_Index atom_number( Group_Index ig, const std::string& cmname,
                            bool is_core, Atom_Index ia0) const;
  void put_marker( Node_Ptr rnode, int i, Restraint& restraint) const;
  [[nodiscard]] std::pair< Mol_Rxn_Ref, Atom_Index>
    new_marker( bool is_core, Group_Index ig, Atom_Index ia0,
                   const std::string& name) const;
  [[nodiscard]] bool has_no_nam( Node_Ptr) const;
};

inline
Viscosity System_Common::viscosity() const{
  return solvent.viscosity();
}

inline
bool System_Common::has_pathway() const{
  return (bool)pathway;
}

inline
std::string System_Common::reaction_name( Rxn_Index irxn) const{
  if (pathway)
    return pathway->reaction_name( irxn);
  else
    return "reaction";
}

inline
size_t System_Common::n_reactions_from( State_Index is) const{
  if (pathway)
    return pathway->n_reactions_from( is);
  else
    return 0;
}

inline
Rxn_Index System_Common::reaction_from( State_Index is, size_t k) const{
  if (pathway)
    return pathway->reaction_from( is, k);
  else
    return Rxn_Index(0);
}

inline
State_Index System_Common::state_after_reaction( Rxn_Index ri) const{
  if (pathway)
    return pathway->state_after_reaction( ri);
  else
    return State_Index(0);
}

inline
State_Index System_Common::first_state() const{
  if (pathway)
    return pathway->first_state();
  else
    return State_Index(0);
}

inline
std::string System_Common::state_name( State_Index si) const{
  if (pathway)
    return pathway->state_name( si);
  else
    return "state";
}


// needed?
/*
inline
void System_Common::set_do_staged(){
  do_staged = true;
}

inline
bool System_Common::doing_staged() const{
  return do_staged;
}
*/
}

