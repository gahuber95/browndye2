#include "../global/pos.hh"

// Interface
// typedef System;
// For one or more internally defined pairs of classes Ia, Ib:
//   typedef Ref, Subsystem
//   Pos Ia::position( System&, Ia::Subsystem&, Ia::Ref);
//   template< class Ia, class Ib> Energy epsilon( System&, Ia::Subsystem&, Ib::Subsystem&, Ia::Ref, Ib::Ref);
//   template< class Ia, class Ib> Length sigma( System&, Ia::Subsystem&, Ib::Subsystem&, Ia::Ref, Ib::Ref);
//   template< class Ia, class Ib> void add_force( System&, Ia::Subsystem&, Ib::Subsystem&, Ia::Ref, Ib::Ref, F3);

//  template Pair_Applier< F> App; F is the object add_lj below, operator( System&) applies F to all the pairs

namespace Browndye::LJ_Force_Computer{

  template<class I>
  class Computer {
  public:
    typedef typename I::System System;

    static
    void add_forces( System &);

  private:
    template<class Ia, class Ib>
    class add_lj {
    public:
      typedef typename Ia::Ref Refa;
      typedef typename Ib::Ref Refb;
      typedef typename Ia::Subsystem Subsys_A;
      typedef typename Ib::Subsystem Subsys_B;

      static
      void doit( System &sys, Subsys_A&, Subsys_B&, Refa, Refb, double factor=1.0);
    };

    template< template< class,class> class F>
    static
    void apply_to_pairs( System& sys);
  };

  //#######################################################################
  template< class I> template< class Ia, class Ib>
  void Computer<I>::add_lj<Ia,Ib>::doit( System& sys, Subsys_A& suba, Subsys_B& subb, Refa ia, Refb ib, double factor){

    auto posa = I:: template position< Ia>( sys, suba, ia);
    auto posb = I:: template position< Ib>( sys, subb, ib);
    auto dpos = posb - posa;
    Length r = norm( dpos);

    // V = eps*( (sig/r)^12 - (sig/r)^6)
    auto sig = I:: template sigma< Ia, Ib>( sys, suba, subb, ia, ib);
    auto eps = I:: template epsilon< Ia, Ib>( sys, suba, subb, ia, ib);

    auto sr = sig/r;
    auto sr2 = sq( sr);
    auto sr4 = sq( sr2);
    auto sr6 = sr2*sr4;
    auto sr12 = sr6*sr6;

    auto For = factor*eps*(12.0*sr12 - 6.0*sr6)/(r*r);

    I:: template add_force< Ia, Ib>( sys, suba, subb, posa, posb, ia,ib, For*dpos);
  }

  //#######################################################################
  template< class I> template< template< class,class> class F>
  void Computer<I>::apply_to_pairs( System& sys){

    typedef typename I:: template Pair_Applier< F> App;
    App::doit( sys);
  }

  //#######################################################################
  template< class I>
  void Computer<I>::add_forces( System& sys) {

    apply_to_pairs< add_lj>( sys);
  }

}
