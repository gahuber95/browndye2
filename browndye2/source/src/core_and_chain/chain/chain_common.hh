#pragma once

#include <map>
#include "../../lib/vector.hh"
#include "../../structures/atom.hh"
#include "../../generic_algs/collision_detector.hh"
#include "chain_collision_interface.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../structures/bonded_structures.hh"
#include "../../forces/other/nonbonded_parameter_info.hh"
#include "../../forces/other/bonded_parameter_info.hh"
#include "../../structures/solvent.hh"
#include "../macro_struct.hh"

/*
Chain atoms are entered using a PQRXML node for each chain.

*/

// in all cases, a Core_Index of maxi refers to the chain itself

namespace Browndye{

class Group_Common;
class System_Common;
class Chain_State;
class Group_State;

class Bond_Index_Tag{};
typedef Index< Bond_Index_Tag> Bond_Index;

class Angle_Index_Tag{};
typedef Index< Angle_Index_Tag> Angle_Index;

class Dihedral_Index_Tag{};
typedef Index< Dihedral_Index_Tag> Dihedral_Index;

class On_Chain_Tag{};

typedef std::variant< Core_Index, On_Chain_Tag> Opt_Core_Index;

template< size_t _n>
struct BStruct{
  static constexpr size_t n = _n;
  Array< Atom_Index, n> atomis;
  Array< Opt_Core_Index, n> coris;
  //Array< bool, n> on_chain;
  
  [[nodiscard]] bool on_core( size_t i) const{
    return std::holds_alternative< Core_Index>( coris[i]);
  }

  /*
  [[nodiscard]] bool all_on_chain() const{
    bool res = true;
    for( auto i: range(n)){
      res &= !on_core( i);
    }
    return res;
  }
  */

  BStruct(){
    for( auto i: range( n)){
      coris[i] = On_Chain_Tag();
    }
  }
};

//############################################################
struct Length_Constraint_Atoms: public BStruct< 2>{  
  Length length = Length( NaN);
  //static const bool is_constraint = true;
};

struct Coplanar_Constraint_Atoms: public BStruct< 4>{
  //static const bool is_constraint = true;
};

struct Bond: public BStruct<2>{
  typedef Bond_Index Index;
  typedef Length Value;
  constexpr static size_t n = 2;
  //static const bool is_constraint = false;
    
  Bond_Type_Index type{ maxi}; 
};

struct Angle: public BStruct<3>{
  typedef Angle_Index Index;
  typedef double Value;
  constexpr static size_t n = 3;
  //static const bool is_constraint = false;

  Angle_Type_Index type{ maxi};
};

struct Dihedral: public BStruct<4>{
  typedef Dihedral_Index Index;
  typedef double Value;
  constexpr static size_t n = 4;
  //static const bool is_constraint = false;
  
  Dihedral_Type_Index type{ maxi};  
};

//######################################################
struct Chain_Common: public Macro_Struct{
  typedef Browndye::Bond Bond;
  typedef Browndye::Angle Angle;
  typedef Browndye::Dihedral Dihedral;
  
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
  typedef JAM_XML_Pull_Parser::Parser Parser;
  
  Chain_Common( const Nonbonded_Parameter_Info&,
               const Bonded_Parameter_Info&, const Group_Common&,
               const Node_Ptr&, Chain_Index, Group_Index);

  Chain_Common(Browndye::Test_Tag, Group_Index ig, const Vector<Browndye::Atom, Browndye::Atom_Index> &);

  Chain_Common() = delete;
  [[nodiscard]] Charge charge() const;
  
  static
  Node_Ptr chain_node( const std::string& file);
  
  Chain_Index number;
  Vector< Length_Constraint_Atoms, Constraint_Index> length_constraints;
  // Chain_Length_Constraint_Atoms 
  Vector< Coplanar_Constraint_Atoms, Constraint_Index> coplanar_constraints;
  // Chain_Coplanar_Constraint_Atoms
  bool is_coreless = true;
  
  Vector< Bond, Bond_Index> bonds;
  Vector< Angle, Angle_Index> angles;
  Vector< Dihedral, Dihedral_Index> dihedrals;

  Vector< Vector< Atom_Index>, Atom_Index> excluded_atoms, one_four_atoms, coulomb_excluded_atoms;
  
  // maps core index to (map of each core atom to excluded chain atoms)
  typedef std::map< Core_Index, std::map< Atom_Index, Vector< Atom_Index> > > Ex_Cor_Map;
  Ex_Cor_Map excluded_core_atoms, one_four_core_atoms, coulomb_excluded_core_atoms;

  struct Core_Interactor{
    Group_Index ig{ maxi};
    Core_Index im{ maxi};
    Atom_Index ia{ maxi}; // core atom
    Length rin{ NaN}, rout{ NaN};
  };

  struct Dummy_Interactor{
    Group_Index ig{ maxi};
    Dummy_Index id{ maxi};
    Atom_Index ia{ maxi}; // core atom
    Length rin{ NaN}, rout{ NaN};
  };

  struct Chain_Interactor{
    Group_Index ig{ maxi};
    Chain_Index ic{ maxi};
    Length rin{ NaN}, rout{ NaN};
    bool touching = false;
  };

  // if chain is near any of these, it will not be frozen
  Vector< Core_Interactor> core_interactors;
  Vector< Dummy_Interactor> dummy_interactors;
  Vector< Chain_Interactor> chain_interactors;
  
  bool never_frozen = true;

  // rigidly stuck to molecule
  // any core atom that takes part in a constraint, bond, angle, or dihedral
  std::map< Core_Index, Vector< Ind_Atom_Index> > bound_atoms; 
  Vector< Core_Index, Ind_Core_Index> constrained_cores; // Chain_Cores
  Vector< Atom_Index, Ind_Atom_Index> constrained_atoms; // Constrained_Chain_Atoms
  std::map< Core_Index, Vector< Ind_Atom_Index> > chain_core_atoms; // Chain_Core_Atoms
  
  std::string name;
  std::string chain_file;
  std::string atoms_file;
  
  //void renumber_core_atoms( const Group_Common&);
  void setup_interactors( const System_Common& sys);
  
  [[nodiscard]] Time natural_time_step( const Solvent&, const Bonded_Parameter_Info&, const Group_Common&, const Group_State&, const Chain_State&) const;
  
  Transform home_tform;
  
  std::optional< Time> const_dt;
  
private:  
  void get_excluded_atoms_from_node_gen( Vector< Vector< Atom_Index>, Atom_Index>&, Ex_Cor_Map& ex_core_atoms,
                                         const Group_Common&, const Node_Ptr&) const;
  //void determine_excluded_atoms();
  void determine_constrained_atoms();
  //void renumber_atoms( std::map< Atom_Index, Vector< Atom_Index> >&, std::map< Atom_Index, Vector< Atom_Index> >&);

  struct Core_Interactor_Info{
    std::string gname, mname;
    Atom_Index ia = Atom_Index{ maxi};
    Length rin{ NaN}, rout{ NaN};
  };
  
  struct Chain_Interactor_Info{
    std::string gname, cname;
    Length rin{ NaN}, rout{ NaN};
    bool touching = false;
  };
  
  Vector< Core_Interactor_Info> temp_core_interactors, temp_dummy_interactors;
  Vector< Chain_Interactor_Info> temp_chain_interactors;
  
  template< class FF_Bonded_Parameter_Info>
  Time natural_time_step_gen( const Solvent&, const FF_Bonded_Parameter_Info&, const Group_Common&, const Group_State&, const Chain_State&) const;
  
  void get_temp_interactors( const Node_Ptr&);
  
  template< class Strukt>
  Strukt gen_structure_from_node( const Group_Common& group, const Node_Ptr& node) const;
  
  template< class Strukt, class Type_Index>
  Strukt structure_from_node( const std::map< std::string, Type_Index>& dict,
                             const Group_Common& group, const Node_Ptr& node);

  [[nodiscard]] Length_Constraint_Atoms
  length_constraint_from_node( const Group_Common&, const Node_Ptr&) const;

  [[nodiscard]] Coplanar_Constraint_Atoms
  coplanar_constraint_from_node( const Group_Common&, const Node_Ptr& node) const;

  static void reorder_excluded_atoms( Vector< Vector< Atom_Index>, Atom_Index>&) ;
  void compute_eta_max(); // for ABSINTH
  //void compute_eta_max_old(); // for ABSINTH
  
  void put_core_interactors( const System_Common&);
  void put_dummy_interactors( const System_Common&);
};

//#######################################################
inline
const Bond& bstrukt( const Chain_Common& chain, const Bond*, Bond_Index i){
  
  return chain.bonds[i];
}

inline
const Angle& bstrukt( const Chain_Common& chain, const Angle*, Angle_Index i){
  return chain.angles[i];
}

inline
const Dihedral& bstrukt( const Chain_Common& chain, const Dihedral*, Dihedral_Index i){
  return chain.dihedrals[i];
}

}

