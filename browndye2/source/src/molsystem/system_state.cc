#include <cassert>
#include "system_state.hh"
#include "system_thread.hh"
//#include "../hydrodynamics/generic_mobility.hh"
#include "system_mobility_interface.hh"
#include "snas_interface.hh"
#include "../forces/forces.hh"
#include "../motion/constraint_tolerance.hh"
#include "../simulation/simulator.hh"
#include "../forces/absinth/absinth_interface.hh"
//#include "../forces/absinth/absinth.hh"
#include "../forces/other/mm_bonded_parameter_info.hh"

namespace Browndye{

using ::std::move;
using ::std::min;
using ::std::max;
using ::std::get;

//#################################################
// constructor
System_State::System_State( System_Thread& _thread, Sys_Copy_Index _number):
  number( _number),
  thread_ref{ _thread}{
  
  auto& gthreads = thread().groups;
  auto ng = gthreads.size();
  groups.reserve( ng);
  for( auto ig: range( ng)){
    auto& gcommon = common().groups[ig];
    auto& gthread = thread().groups[ig];
    groups.emplace_back( gcommon, gthread);
  }
 
  bool frozen_changed;
  update_geometry( frozen_changed);

  if (common().force_field == Force_Field_Type::Absinth && !frozen_changed){
    update_eta0();
  }

  update_mobility_geometry();
  
  if (common().building_bins){
    set_initial_state_for_bb();
  }
}

//#######################################
void System_State::set_initial_state(){
   
  if (restraint_violated()){
    error( "restraint violated at the start");
  }
  
  if (!has_hi()){
    consistency.hydrodynamics = true;
  }

  make_inconsistent_from_shift();
  
  auto ng = groups.size();
  if (ng == Group_Index(2)){
    if (common().start_at_site){
      place_at_starting_site();
    }
    else{
      place_on_sphere( common().b_rad);
    }
  }
  else{
    place_at_starting_site();
  }

  center_group0();
  //cout << "after center group0" << endl;

  make_start_state_consistent(); 
  n_bd_steps = 0;
}

//############################################
void System_State::push_tets_down(){
  for( auto& group: groups){
    group.push_tets_down();
  }
}

//############################################
void System_State::place_at_starting_site(){

  for( auto& group: groups){
    group.reset_to_start( common());
  }
}

//#################################################
void System_State::place_on_sphere( Length b){
  
  if (groups.size() != Group_Index(2)){
    error( __FILE__, __LINE__, "must have two groups");
  }
  
  center_group0();
  place_group1_on_sphere( b);
}

//#####################################################
void System_State::center_group0(){
  
  Group_Index g0(0);
  auto center0 = group_center( g0);
  auto offset0 = -center0;
  Pure_Translation tform( offset0);
   
  for( auto& group: groups){
    group.transform( tform);
  }

  make_inconsistent_from_shift();
}

//#######################################################
void System_State::place_group1_on_sphere( Length b){
  
  const Group_Index g1( 1);  
  
  auto center1 = group_center( g1);
  Pure_Translation back1( -center1);
  
  auto& rng = thread().rng;
  auto rotmat1 = random_rotation< SNAS_I>( rng);
  Transform rot1( rotmat1, Vec3< Length>( Zeroi{}));
  
  auto u = random_unit_vector< SNAS_I>( rng);
  Pure_Translation forward1( b*u);
  
  auto tform = forward1*rot1*back1;
  groups[g1].transform( tform);
  
  make_inconsistent_from_shift();
}
//###############################
void System_State::swap(){
  
  auto& gthreads = thread().groups;
  auto ng = groups.size();
  for( auto ig: range( ng)){
    auto& group = groups[ig];
    auto& gthread = gthreads[ig];
    group.swap( gthread.current);
  }
}

//###############################
void System_State::save(){

  //test_force_balance( 6);
  check_all_consistency();
  
  auto& gthreads = thread().groups;
  auto ng = groups.size();
  for( auto ig: range( ng)){
    auto& group = groups[ig];
    auto& gthread = gthreads[ig];
    group.save( gthread.current);
    gthread.current.has_state = true;
  }
}

//###############################
void System_State::restore(){
  //cout << "restore" << endl;
  auto& gthreads = thread().groups;
  auto ng = groups.size();
  for( auto ig: range( ng)){
    auto& group = groups[ig];
    auto& gthread = gthreads[ig];
    if (!gthread.current.has_state){
      error( __FILE__, __LINE__, "no state");
    }
    group.restore( gthread.current);    
  }
  
  consistency.force = true;
  consistency.hydrodynamics = true;
  consistency.constraints = true;
  consistency.geometry = true;
  //test_force_balance( 5);
}

//####################################################
void System_State::compute_tet_forces(){
  
  for( auto& group: groups){
    group.push_forces_to_tets();
  }
}

//####################################################
void System_State::make_start_state_consistent(){
  
  bool frozen_changed;
  update_geometry( frozen_changed);
  compute_forces( *this);

  //cout << "test force balance a" << endl;
  //test_force_balance( 1);

  n_full_steps = 0;
  minimum_rxn_coord = Length( large);
  set_initial_reaction_state();
  
  last_dt = Time( large);
  fate = System_Fate::In_Action;
  time = Time( 0.0);
  if (frozen_changed){
    update_mobility_geometry();
  }
  if (has_hi()){
    update_mobility_quantities();
  }
    
  for( auto& group: groups){
    
    { // combine with section from bd_step
      auto info = group.cons_info_and_doer;
      auto& cinfo = info->cinfo;  
      auto& constrainer = info->doer;
      cinfo.initial_satisfaction = true;
      constrainer->satisfy_no_init( cinfo, constraint_solve_tol);
      cinfo.initial_satisfaction = false; 
    }
  }
  push_tets_down();
  compute_forces( *this);
  //cout << "test force balance b" << endl;
  //test_force_balance( 3);
  check_all_consistency();
}

//##################################################
void System_State::set_initial_reaction_state(){
  current_rxn_state = common().first_state();
}

//#######################################################
Time System_State::time_step_guess(){
  
  check_all_consistency();
  
  auto dt = max_time_step();
  //cout << "dt here a " << dt << endl;
  
  auto& sys_common = common();
  auto& solvent = sys_common.solvent;
  auto& bpinfo = *(sys_common.bpinfo);
  for( auto& group: groups){
    auto& gcommon = group.common();    
    auto& chains = gcommon.chains;
    auto& chain_states = group.chain_states;
    for( auto ic: range( chains.size())){
      auto& chain = chain_states[ic];
      if (!(chain.frozen || chain.atoms.empty())){
        dt = min( dt, chains[ic].natural_time_step( solvent, bpinfo, gcommon, group, chain));
        //cout << "dt ic " << ic << ' ' << dt << endl;
      }
    }
  }

  //cout << "guess dt " << dt << endl;
  return dt;
}

//#########################################################
void System_State::update_chain_freezings( Vector< bool, Group_Index>& frozen_changed){
  
  frozen_changed.fill( false);
  auto circum_spheres = all_circumspheres();
  for( auto ig: range( groups.size())){
    bool fc;
    update_group_chain_freezings( circum_spheres, ig, fc);
    frozen_changed[ig] = fc;
  }
}

//#####################################################################################
void System_State::update_group_chain_freezings( const Circum_Spheres& circum_spheres,
						 Group_Index ig, bool& frozen_changed){

  auto& group_statei = groups[ig];
  auto& chain_states = group_statei.chain_states;
  auto nc = chain_states.size();
  Vector< bool, Chain_Index> new_frozen( nc, false);
  auto& gcommon = group_statei.common();
  auto& chains = gcommon.chains;
 
  for( auto ic: range( nc)){
    auto& chain = chains[ic];
    if (!chains[ic].never_frozen){
      bool nfrozen = new_frozen[ ic];
      check_core_interactors( circum_spheres, ig, ic, chain.core_interactors,  nfrozen);
      check_core_interactors( circum_spheres, ig, ic, chain.dummy_interactors,  nfrozen);
      if( nfrozen){
        check_chain_interactors( circum_spheres, ig, ic, nfrozen);
      }
      new_frozen[ ic] = nfrozen;
    }
  }

  frozen_changed = false;
  if (!std::equal( new_frozen.begin(), new_frozen.end(), chain_states.begin(),
                 [&]( auto frozen, auto& chain){ return frozen == chain.frozen;})){
    
    frozen_changed = true;
    consistency.hydrodynamics = false;
    for( auto ic: range( nc)){
      bool newf = new_frozen[ic];
      chain_states[ic].frozen = newf;
    }    
  }
  if (frozen_changed){
    //cout << "frozen changed" << endl;
  }
}

//###############################################################################
template< class Interactor>
void System_State::check_core_interactors( const Circum_Spheres& circum_spheres,
					   Group_Index jg, Chain_Index jc, const Vector< Interactor>& interactors, bool& new_frozen){
    
  new_frozen = true;  
  auto& group_statej = groups[jg];
  auto& chain_states = group_statej.chain_states;
  auto& group_commons = common().groups;
  auto& chainj = chain_states[jc];
  auto& csphere = circum_spheres[jg][jc];
  auto r = csphere.radius;
  auto chain_pos = csphere.pos;
  
  //auto& coreins = chains[jc].*cd_int_ptr;
  for( auto& corein: interactors){
    auto ig = corein.ig;

    auto ia = corein.ia;
    auto& group_statei = groups[ig];
    
    auto site_pos = [&]{
      if constexpr (std::is_same_v< Interactor, Chain_Common::Core_Interactor>){
        auto im = corein.im;
        auto& core_statei = group_statei.core_states[im];
        auto& corei = group_commons[ig].cores[im];
        auto& atoms = corei.atoms;
        return core_statei.transformed( atoms[ia].pos);
      }
      else{
        auto id = corein.id;
        auto& dummyi = group_commons[ig].dummies[id];
        auto im = dummyi.im;
        auto& core_statei = group_statei.core_states[im];
        auto& atom = dummyi.atoms[ia];
        return core_statei.transformed( atom.pos);
      }
    }();
    
    auto d = distance( chain_pos, site_pos);
    auto clearance = d - r;
    if (chainj.frozen){
      if (clearance < corein.rin){
        for( auto& atom: chainj.atoms){ 
          auto ad = distance( site_pos, atom.pos);
          if (ad < corein.rin){
            new_frozen = false;
            break;
          }
        }
      }
    }
    else{  // not frozen
      bool nf = true;
      for( auto& atom: chainj.atoms){
        if (distance( site_pos, atom.pos) < corein.rout){
          nf = false;
          break;
        }
      }
      if (!nf){
        new_frozen = false;
      }
    }
  }
 
}

//########################################################################################
void System_State::check_chain_interactors( const Circum_Spheres& circum_spheres,
                    					    Group_Index ig, Chain_Index ic,
                                             bool& new_frozen){

  new_frozen = true;
  auto& group_state = groups[ig];
  auto& chain_states = group_state.chain_states;
  auto& gcommon = group_state.common();
  auto& chains = gcommon.chains;

  auto& chaini = chain_states[ic];
  auto& chainins = chains[ic].chain_interactors;
  auto& cspherei = circum_spheres[ig][ic];
  auto posi = cspherei.pos;
  auto ri = cspherei.radius;
  
  for( auto& chainin: chainins){
    auto jg = chainin.ig;
    auto jc = chainin.ic;
    
    auto& cspherej = circum_spheres[jg][jc];
    Pos posj = cspherej.pos;
    Length rj = cspherej.radius;
    
    auto d = distance( posi, posj);
    auto clearance = d - (ri + rj);
   
    if (chainin.touching){
      if (chaini.frozen){
        if (clearance < Length( 0.0)){
          new_frozen = false;
        }
      }
      else{
        if (d < 1.1*(ri + rj)){
          new_frozen = false;
        } 
      }
    }
    else{ // no touching option
      
      if (chaini.frozen){
        if (clearance < chainin.rin){
          new_frozen = false;
        }
      }
      else{ 
        if (clearance < chainin.rout){
          new_frozen = false;
        }
      }
    }    
  }
}

//###################################################################
auto System_State::all_circumspheres() const -> Circum_Spheres{
  
  Circum_Spheres circum_spheres;
  circum_spheres.reserve( groups.size());
  
  for( auto& group: groups){
    auto& chain_states = group.chain_states;
    auto& gcommon = group.common();
    auto& chains = gcommon.chains;
    auto nc = chain_states.size();
    Vector< Circum_Sphere, Chain_Index> sub_spheres( nc);
    for( auto ci: range( nc)){
      auto& chain = chain_states[ci];
      auto& chain_com = chains[ci];
      if (!chain_com.never_frozen){
        sub_spheres[ci] = chain_circumsphere( chain);
      }
    }
    circum_spheres.push_back( std::move( sub_spheres));
  }
  return circum_spheres;
}

//#######################################################
class Circum_Spheres_Interface{
public:
  typedef const Chain_State Points;
  typedef Vector< Atom_Index, Atom_Index> Point_Refs;
  typedef Pos Position;
  typedef Atom_Index Index;
  typedef ::Length Length;
  
  static
  void initialize( Points& chain, Point_Refs& refs){
    auto& atoms = chain.atoms;
    refs.reserve( atoms.size());
    for( auto i: range( atoms.size())){
      refs.push_back(i);
    }
  }
  
  static
  Index size( Points& chain){
    return chain.atoms.size();
  }
  
  static
  void get_position( Points& chain, const Point_Refs& refs,
                     Index i, Pos& pos){
    pos = chain.atoms[ refs[i]].pos;
  }
  
  static
  void swap( Points&, Point_Refs& refs, Index i, Index j){
    std::swap( refs[i], refs[j]);
  }
  
  static
  Length radius( Points& chain, const Point_Refs& refs, Index i){
    return chain.common().atoms[refs[i]].radius;
  }
  
  template< typename ... Args>
  static
   void error( Args ... args){
     Browndye::error( args...);
   }
};

//###################################################################################
auto System_State::chain_circumsphere( const Chain_State& chain) -> Circum_Sphere{
  
  Circum_Sphere sph{};
  
  if (!chain.atoms.empty()){
    std::tuple< Pos, Length> res{};
    typedef Circum_Spheres_Interface IFace;
    Circumspheres::
      get_smallest_enclosing_sphere_of_spheres< IFace>(
  						     chain, std::get<0>( res), std::get<1>( res));
    
    sph.pos = get<0>( res);
    sph.radius = get<1>( res);
  }
  else{
    sph.pos = Pos( Zeroi{});
    sph.radius = Length( 0.0);
  }
    
  return sph;
}

//#####################################################################
void System_State::copy_from( const System_State& other){
  
  thread_ref = other.thread_ref;
  current_rxn_state = other.current_rxn_state;
  n_full_steps = other.n_full_steps;
  
  time = other.time;
  number = other.number;
  last_rxn = other.last_rxn;
  groups = other.groups.copy();
  last_dt = other.last_dt;
  minimum_rxn_coord = other.minimum_rxn_coord;
  fate = other.fate;
  consistency = other.consistency;
  
  if (other.my_mob_info_and_doer){
    my_mob_info_and_doer =
      std::make_unique< MID>( other.my_mob_info_and_doer->copy());
    mob_info_and_doer = my_mob_info_and_doer.get();
  }
  else{
    mob_info_and_doer = other.mob_info_and_doer;
  }
  
  mob_info = other.mob_info;
  wprocess = std::make_unique< WProc>( other.wprocess->copy());
}

//#########################################################
// copy constructor
System_State::System_State( const System_State& other):
  thread_ref( other.thread_ref){
  
  copy_from( other);
}

System_State& System_State::operator=( const System_State& other){
  
  copy_from( other);
  return *this;
}

//#############################################################
Length System_State::rxn_coord() const{
  
  assert( consistency.constraints); 
  
  Length mc( large);
  
  auto& com = common();
  if (com.has_pathway()){
    auto nrxns = com.n_reactions_from( current_rxn_state);
    for( auto i: range( nrxns)){
      auto irxn = com.reaction_from( current_rxn_state, i);
      Length coord = com.reaction_coordinate( *this, irxn);
      if (coord < mc){
        mc = coord;
      }
    }
  }
  return mc;
}

//#############################################################
std::tuple< Length, Diffusivity, Length>
System_State::rxn_coord_n_diffu() const{
  
  assert( consistency.constraints);
  
  Length mc( large);
  Diffusivity D;
  Length reqr{ 0.0};
  
  auto& com = common();
  if (com.has_pathway()){
    auto nrxns = com.n_reactions_from( current_rxn_state);
    for( auto i: range( nrxns)){
      auto irxn = com.reaction_from( current_rxn_state, i);
      auto [coord,D0, rr] = com.reaction_coord_n_diffusivity( *this, irxn);
      if (coord < mc){
        mc = coord;
        D = D0;
        reqr = rr;
      }
    }
  }
  return std::make_tuple( mc, D, reqr);
}

//###############################################
void System_State::set_initial_state_for_bb(){
  
  place_on_sphere( common().q_rad);
  make_start_state_consistent();
}

//################################
void System_State::recenter(){
  auto cen = groups[ Group_Index(0)].center();
  Pure_Translation tform( -cen);
  for( auto& group: groups){
    group.transform( tform);
  }
}

//###################################################
const System_Common& System_State::common() const{
  return thread().common();
}

//######################################################################
void System_State::update_mobility_quantities(){
  
  assert( consistency.geometry);
  assert( mob_info_and_doer);
  
  Mob_Info_And_State mias{ *this, *mob_info};
      mob_info_and_doer->doer.update(mias);
  
  consistency.hydrodynamics = true;
}

//######################################################
void System_State::update_mobility_geometry(){
   
  typedef Generic_Mobility_Full::Doer< System_Mobility_Interface> SDoer; 
   
  assert( consistency.geometry);
  
  auto key = mobility_key();
  mob_info = &(thread().mob_info_of_key( *this, key));
  
  if (has_hi()){
    auto nsphu = common().n_steps_per_hi_update;
    if (nsphu > 1){
      Mob_Info_And_State mias{ *this, *mob_info};
      SDoer doer{ mias};
         
      my_mob_info_and_doer =
              std::make_unique< MID>( *mob_info, std::move( doer));
      mob_info_and_doer = my_mob_info_and_doer.get();
    }
    else{
      mob_info_and_doer = &(thread().hydro_doer_of_key( *this, key));
    }
  }
   
  consistency.hydrodynamics = true;
}

//######################################################
std::vector< bool> System_State::mobility_key() const{
  
  std::vector< bool> key;
  for( auto& group: groups){
    auto& chains = group.chain_states;
    for( auto& chain: chains){
      key.push_back( chain.frozen);
    }
  }
  return key;
}

//#################################
bool System_State::has_hi() const{
  return common().has_hi;
}

//################################
void System_State::update_geometry( bool& frozen_changed){

  frozen_changed = false;
  auto ng = groups.size();
  Vector< bool, Group_Index> gfrozen_changed( ng);
  update_chain_freezings( gfrozen_changed);
  for( auto ig: range( ng)){
    auto& group = groups[ig];
    if (gfrozen_changed[ig]){
      frozen_changed = true;
      auto& gthread = thread().groups[ ig];
      group.setup_beads_and_constraints( gthread);
    }
  }
    
  if (frozen_changed){
    if (common().force_field == Force_Field_Type::Absinth){
      update_eta0();
    }

    compute_forces( *this);
    //test_force_balance( 2);
  }
    
  consistency.geometry = true;
  consistency.force = true;
}

//#####################################
void System_State::make_inconsistent_from_shift(){
  
  consistency.force = false;
  consistency.geometry = false;
  if (has_hi()){
    consistency.hydrodynamics = false;
  }
}

//#################################################
void System_State::check_all_consistency() const{
  
  if (!consistency.all()){
    error( "system not consistent");
  }
}

//######################################################
Time System_State::minimum_dt() const{
  
  return Time{ 2.1*std::numeric_limits< double>::min()};
}

//##############################################################
std::optional< Time> System_State::constant_dt() const{
  
  auto ctopt = common().ts_params.const_dt_opt;
  
  if (ctopt){
    return ctopt;
  }
  else{
    bool has_cct = false;
    Time min_dt{ large};
    for( auto& group: groups){
      for( auto& chain: group.chain_states){
        auto cdt = chain.constant_dt();
        if (!chain.frozen){
          if (cdt){
            auto dt = cdt.value();
            min_dt = std::min( min_dt, dt);
            has_cct = true;
          }
        }
      }
    }
    
    if (has_cct){
      return std::optional< Time>{ min_dt};
    }
    else{
      return std::nullopt;
    }
  }
  
}

//##############################################################
void System_State::rethread( Simulator& sim, Thread_Index ith){
    
  thread_ref = sim.thread( ith);
  auto ng = groups.size();
  auto& thd = thread();
  for( auto ig: range( ng)){
    groups[ig].rethread( thd.groups[ ig]);
  }
  
  auto key = mobility_key();
  mob_info = &(thread().mob_info_of_key( *this, key));
  if (has_hi() && common().n_steps_per_hi_update == 1){
    mob_info_and_doer = &(thread().hydro_doer_of_key( *this, key));
  }
}

//################################################################
Diffusivity System_State::pair_radial_diffusivity() const{
  
  if (groups.size() != Group_Index(2)){
    error( "need two groups for staged simulation");
  }
  if (common().has_hi){
    error( "do not use hydrodynamic interactions for staged simulation");
  }
  
  auto diff = [&]( size_t i){
    return groups[ Group_Index{i}].diffusivity();
  };
  
  return diff(0) + diff(1);
} 

void System_State::update_eta0() {

  auto& com = common();
  auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< Force_Field_Type::Absinth>&>( *(com.pinfo));
  auto& bpinfo = dynamic_cast< const MM_Bonded_Parameter_Info&>( *(com.bpinfo));
  Parameters< MM_Nonbonded_Parameter_Info< Force_Field_Type::Absinth>, MM_Bonded_Parameter_Info> params( com.solvent, pinfo, bpinfo);
  auto& g0_threads = thread().groups.front().core_threads;
  auto sys = g0_threads.empty() ? Absinth_Iface< true>::System( *this, params) : Absinth_Iface_In::Sys( *this, params, g0_threads.front().ab_atoms);
  Absinth::Computer< Absinth_Iface< false> >::compute_eta( sys);

  for( auto& group: groups){
    for( auto& core: group.core_states){
      for( auto& atom: core.thread().ab_atoms){
        atom.set_eta0( atom.eta());
      }
    }
    for( auto& chain: group.chain_states){
      for( auto& atom: chain.atoms){
        atom.set_eta0( atom.eta());
      }
    }
  }
}

#ifdef DEBUG
System_State::~System_State(){
  
  this->mob_info = nullptr;
  this->mob_info_and_doer = nullptr;
  this->my_mob_info_and_doer.reset();
  this->wprocess.reset();
}
#endif

}
