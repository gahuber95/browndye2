#include "single_traj_simulator.hh"
#include "../input_output/trajectory_outputter.hh"

namespace Browndye{
  typedef std::string String;
  
  String uitoa( size_t i){
    std::ostringstream stm;
    stm << i;
    String res( stm.str());
    return res;
  }
  
  //#########################################################################################################
  void Single_Traj_Simulator::end_trajectory_output( const System_State& state, const Traj_Out_Ptr& outputter) {
    
    if (outputter){
      auto& common = state.common();
      
      const String reaction_name = (state.fate == System_Fate::Final_Rxn) ?  
        common.reaction_name( state.last_rxn) :
        String( " ");
  
      outputter->end_subtrajectory( state.fate, 
                                    reaction_name,
                                    common.state_name( state.current_rxn_state));
  
      outputter->end_trajectory( state.fate,
                                reaction_name,
                                common.state_name( state.current_rxn_state),
                                state.rxn_coord());
    }
  }
  
  //##########################################################################################################
  void Single_Traj_Simulator::start_trajectory_output( const System_State& state, const Traj_Out_Ptr& outputter) {
    if (outputter){
      auto& common = state.common();
      auto istate = state.current_rxn_state;
      outputter->start_trajectory();
      auto name = common.state_name( istate);
      outputter->start_subtrajectory( name);
    }
  }
  
  //###################################################################################################
  std::unique_ptr< Trajectory_Outputter>
  Single_Traj_Simulator::new_trajectory_outputter( const System_State& state){
   
    auto& thread = state.thread();
    auto& common = thread.common();
   
    std::unique_ptr< Trajectory_Outputter> outputter;
   
    if (!(traj_file_name.empty())){
      std::string number( uitoa( thread.number/Thread_Index(1)));
      std::string suffix( ".xml");
      std::string itext( ".index");
      
      auto ftf_name = traj_file_name + number + suffix;
      auto ftif_name = traj_file_name + number + itext + suffix;
      
      auto nopb = n_output_states_per_block;  
      outputter = std::make_unique< Trajectory_Outputter>( common, ftf_name,
                                                  ftif_name, nopb);
    }
     
    return outputter;
  }
  
  //##################################################################################
  namespace JP = JAM_XML_Pull_Parser;
  
  // constructor
  Single_Traj_Simulator::Single_Traj_Simulator( JP::Node_Ptr node): Simulator( node){
  
    n_trajs = checked_value_from_node< size_t>( node, "n_trajectories");
    
    auto get_val = [&]( const std::string& tag, auto& val){
      typedef std::decay_t< decltype(val)> T;
      auto valo = value_from_node<T>( node, tag);
      if (valo){
        val = *valo;
      }
    };
    
    get_val( "n_steps_per_output", n_steps_per_output);
    get_val( "n_trajectories_per_output", n_trajs_per_output);
    get_val( "max_n_steps", max_n_steps);
    get_val( "trajectory_file", traj_file_name);
    get_val( "n_output_states_per_block", n_output_states_per_block);  
      
    std::string dors;
    get_val( "print_rng_state", dors);
    //do_output_rng_state = (dors == std::string("yes"));
  
    for( auto it: range( mthreads.size())){    
      Sys_Copy_Index icopy{ it/Thread_Index(1)};
      
      auto& mthread = mthreads[it];
      auto new_state = std::make_unique< System_State>( mthread, icopy);
      states.insert( std::make_pair( icopy, std::move( new_state)));
    }
  }
  
}
