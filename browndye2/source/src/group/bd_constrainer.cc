#include "../generic_algs/chain_constraints.hh"
#include "group_constraint_interface.hh"
#include "group_state.hh"

#include "bd_constrainer.hh"

namespace Browndye{
  
  Constrainer::Constrainer( const System& sys){
    //doer = std::make_unique< Doer>( sys);
    doer = new Doer( sys);
  }
  
  Constrainer::~Constrainer(){
    delete doer;
  }
  
  Constrainer::Constrainer( Constrainer&& other){
    doer = other.doer;
    other.doer = nullptr;
  }
  
  Constrainer& Constrainer::operator=( Constrainer&& other){
    doer = other.doer;
    other.doer = nullptr;
    return *this;
  }
    
  double Constrainer::sys_rms_violation( const System& sys){
    return doer->sys_rms_violation( sys);
  }
  
  void Constrainer::satisfy_no_init( System& sys, double tol){
    //cout << "before satisfy no init" << endl;
    doer->satisfy_no_init( sys, tol);
    //cout << "after satisfy no init" << endl;
  }
  
  void Constrainer::satisfy( System& sys, double tol, size_t& nsteps){

    /* already done
    auto comof = [&](){
      Array< Length2, 3> sum( Zeroi{});
      Length rsum{ 0.0};
      Group_State& group = *(sys.state);
      for( auto& tet: group.tets){
        auto trad = tet.bead_radius();
        for( auto& pos: tet.poses){
          sum += trad*pos;
          rsum += trad;
        }
      }
      for( auto& chain: group.chain_states){
        auto& chainc = chain.common();
        auto na = chain.atoms.size();
        for( auto ia: range( na)){
          auto pos = chain.atoms[ia].pos;
          auto rad = chainc.atoms[ia].radius;
          rsum += rad;
          sum += rad*pos;
        }
      }

      return sum/rsum;
    };
    */

    //auto comb = comof();
    doer->satisfy_simple( sys, tol, nsteps);
    //cout << "nsteps " << nsteps << endl;
    //auto come = comof();
    //auto dcom = come - comb;

    // try shifting back
    /*
    Group_State& group = *(sys.state);
    for( auto& tet: group.tets){
      for( auto& pos: tet.poses){
        pos -= dcom;
      }
    }
    for( auto& chain: group.chain_states){
      auto na = chain.atoms.size();
      for( auto ia: range( na)){
        auto& pos = chain.atoms[ia].pos;
        pos -= dcom;
      }
    }
    group.push_tets_down();
     */
  }

  [[maybe_unused]] void Constrainer::satisfy_simple( System& sys, double tol, size_t& nsteps){
    doer->satisfy_simple( sys, tol, nsteps);
  }
}
