#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Implements Mover_Base class.

On several threads, this code will execute a function on 
a copy of an object on each thread.
This code runs only one object per thread.  See multithread_mover.hh
for code that can run arbitrary numbers of objects per thread.

The Interface class has the following types and static functions:

Exception - the type of exception to be thrown if needed.
Objects_Ref - reference to a container of objects
Object_Ref - reference to an object

template< class Func>
void apply_to_object_refs( Func& f, Objects_Ref objects) -
  applies function object "f" to each object in "objects".
  "Func" has overloaded void ()( Object_Ref) operator.

unsigned int size( Objects_Ref) - number of objects

void do_move( Objects_Ref objects, Object object) - moves "object"

void set_null( Objects_Ref& objects) - sets reference to null (no object)

bool is_null( Objects_Ref& objects) - returns "true" if reference is null.

*/

#include <string>
#include <future>
#include "../lib/vector.hh"
#include "barrier.hh"
#include "../global/error_msg.hh"

namespace Browndye{

namespace Multithread{

enum class Task_Result {Normal, Error, Stopped};



class Halt_Exception: public std::exception{
public:
  virtual ~Halt_Exception(){};
};

template< class I>
class Mover_Base{
public:
  typedef typename I::Objects_Ref Objects_Ref;
  typedef typename I::Object_Ref Object_Ref;
  typedef typename I::Exception Exception;
  
  void set_objects( Objects_Ref);
  void do_moves();
  int n_threads() const;
  
  void check_for_interruption() const{
    if (has_error)
      throw( Halt_Exception());
  }
  
private:
  struct TRes{
    Task_Result res = Task_Result::Normal;
    Exception exception{""};
  };
  
  Objects_Ref objects_ref;
  Vector< Object_Ref> objects;
  Vector< std::future< TRes> > tasks;
  bool has_error = false;
  
  
};

template< class I>
int Mover_Base<I>::n_threads() const{
  return I::size( objects_ref);
}

template< class I>
void Mover_Base<I>::set_objects( Objects_Ref refs_in){
  
  objects_ref = refs_in;
  objects.reserve( n_threads());
  
  auto get_ref = [&]( Object_Ref obj){
    objects.push_back( obj);
  };
  
  I::apply_to_object_refs( get_ref, refs_in);
}

template< class I>
void Mover_Base<I>::do_moves(){
  
  auto doit = [&]( Objects_Ref lobjects, size_t ith, Object_Ref obj) -> TRes{
    try{
      I::do_move( *this, lobjects, ith, obj);
      Exception re("");
      return TRes{ Task_Result::Normal, re};
    }
    catch( Halt_Exception& exc){
      return TRes{ Task_Result::Stopped, Exception{""}};
    }
    catch( Exception& exc){
      has_error = true;
      return TRes{ Task_Result::Error, exc};
    }
  };
  
  tasks.reserve( n_threads()-1);
  for( size_t ith: range( 1, objects.size())){
    auto oref = objects[ith];
    tasks.push_back( std::async( std::launch::async,
                                doit, objects_ref, ith, oref));
  }
  
  doit( objects_ref, 0, objects[0]);
  
  TRes tres;
  
  for( auto& task: tasks){
    task.wait();
    TRes tres0 = task.get();
    if (tres0.res == Task_Result::Error){
      tres = tres0;
    }
  }
  
  if (tres.res == Task_Result::Error){
    throw tres.exception;
  }
 
}
}


}


