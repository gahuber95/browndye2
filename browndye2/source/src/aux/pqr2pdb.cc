#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <iomanip>

using std::cout;
using std::setw;

int main(){

  char buffer[1000];
  while( !std::cin.eof()){
    std::cin.getline( buffer, 1000);
    std::string line( buffer);
    
    auto label = line.substr(0,4);
    if (label == "ATOM"){
      
      char sfields[5][100];
      double x,y,z;
      
      sscanf( line.c_str(), "%s %s %s %s %s %lf %lf %lf",
	      sfields[0], sfields[1], sfields[2],
	      sfields[3], sfields[4],  
	      &x, &y, &z);
      
      auto sw = [&](int w){
        cout << setw(w);
      };
      
      cout << "ATOM  ";
      sw( 5); cout << sfields[1]; // atom number
      cout << ' ';
      sw( 4); cout << sfields[2]; // atom name
      cout << ' ';
      
      std::string frname( sfields[3]);
      std::string rname = frname.length() == 4 ? frname.substr( 1,3) : frname;
      
      sw( 3); cout << rname; // residue name
      cout << " A"; // chain name
      sw( 4); cout << sfields[4]; // residue number

      
      cout.precision(3);
      cout << std::fixed;
      sw( 12); cout << x;
      sw( 8);  cout << y;
      sw( 8);  cout << z;
      
      cout.precision(2);
      sw( 6); cout << 1.0; // occupancy
      sw( 6); cout << 0.0; // temperature factor
      cout << "          ";
      sw( 2); cout << sfields[2][0];
      cout << '\n'; 
    }
  }
  cout << "TER\n";
  cout << "END\n";
  
}

