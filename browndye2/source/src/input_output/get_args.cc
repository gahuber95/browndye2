/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Extracts arguments from the command-line input to a program,
using flags to denote the arguments.
*/ 

#include <string.h>
#include "../global/limits.hh"
#include "../global/error_msg.hh"
#include "../lib/vector.hh"

namespace Browndye{

//##############################################################
double double_arg( int argc, char* argv[], const char* flag, double defalt = NAN){
  double res = defalt;
  for( int i = 1; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      char* str = argv[i+1];
      char* pend;
      res = strtod( str, &pend);
      if (pend == str)
        error( "double_arg: not a floating point number at ", flag);
      else 
        break;
    }
  }
  return res;
}

    [[maybe_unused]] std::optional< double>
double_opt_arg( int argc, char* argv[], const char* flag){
  std::optional< double> res;
  for( int i = 1; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      char* str = argv[i+1];
      char* pend;
      res = strtod( str, &pend);
      if (pend == str)
        error( "double_arg: not a floating point number at ", flag);
      else 
        break;
    }
  }
  return res;
}

//##############################################################
    [[maybe_unused]] int int_arg( int argc, char* argv[], const char* flag){
  int res = (int)maxi;
  for( int i = 1; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      char* str = argv[i+1];
      char* pend;
      res = strtol( str, &pend, 10);
      if (pend == str)
        error( "int_arg: not an integer at ", flag);
      else {
        break;
      }
    }    
  }
  return res;
}

//##############################################################
using std::string;

std::optional< string> string_arg_opt( int argc, char* argv[], const string& flag){
  
  for( int i = 1; i < argc-1; i++){
    string arg( argv[i]);  
    if (flag == arg){
      return std::string( argv[i+1]);
    }  
  }
  return std::nullopt;
}

//##############################################################
string string_arg( int argc, char* argv[], const string& flag){
  
  string res;
  for( int i = 1; i < argc-1; i++){
    string arg( argv[i]);  
    if (flag == arg){
      res = argv[i+1];
      break;
    }  
  }
  return res;
}

    [[maybe_unused]] bool has_flag( int argc, char* argv[], const string& flag){
  bool res = false;
  for( int i = 1; i < argc; i++){
    string arg( argv[i]);
    if (flag == arg){
      res = true;
      break;
    }  
  }
  return res;  
}

    [[maybe_unused]] Vector< string> string_args( int argc, char* argv[], const string& flag){
  
  Vector< string> res;
  for( int i = 1; i < argc; i++){
    string argi( argv[i]);  
    if (flag == argi){
      for( int j = i+1; j < argc; j++){
        auto& argj = argv[j];
        if (argj[0] == '-')
          break;
        else
          res.push_back( argj);
      }
      break;
    }  
  }
  
  return res;  
}

    [[maybe_unused]] void print_help( int argc, char* argv[], const std::string& msg){
  if (argc == 2){
    if (strcmp( argv[1], "-help") == 0 || strcmp( argv[1], "--help") == 0){
      std::cout << msg << "\n";
      exit(0);
    }
  }
}

    [[maybe_unused]] string lone_arg( int argc, char* argv[]){
  string res;
  for( int i = 1; i < argc; i++){
    string arg( argv[i]), parg( argv[i-1]);
    if (!(arg[0] == '-' || parg[0] == '-')){
      res = arg;
    }
  }
  return res;
}

}

