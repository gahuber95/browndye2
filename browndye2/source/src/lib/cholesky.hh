#pragma once

/*
 * cholesky.hh
 *
 *  Created on: Apr 14, 2015
 *      Author: root
 */


/*
Required Interface:

typedefs:
Matrix, Dec_Matrix, Matrix_Value, Dec_Matrix_Value, Index;

static functions:
Index size( const Matrix&);
Index size( const Dec_Matrix&);

Matrix_Value element( const Matrix&, Index, Index);
Matrix_Value& element_ref( Matrix&, Index, Index);
Dec_Matrix_Value element( const Dec_Matrix&, Index, Index);
Dec_Matrix_Value& element_ref( Dec_Matrix&, Index, Index);

templated type and functions:
Vector_Type< T>::Result - gives a standalone vector container of T; must be
applicable to both contained types of the vector arguments of solve()

template< class T>
T element( const I::Vector_Type<T>::Result&, Index);

template< class T>
T& element_ref( I::Vector_Type<T>::Result&, Index);

following two functions must be defined for vector arguments to solve()
template< class V>
T element( const V&, Index)

template< class V>
T& element_ref( V&, Index)

void error( const std::string&);

 */

#include "skyline_cholesky.hh"

namespace Browndye{

namespace Cholesky{
  template< class I>
  class Interface: public I{
  public:
    typedef typename I::Index Index;
    //typedef decltype( Index(1) - Index(0)) DIndex;

    template< class M>
    static
    Index bandwidth( [[maybe_unused]] const M& mat, Index i){
      return i;
    }

    template< class V>
    class Value_Type{
    public:
      typedef decltype( I::element( V(), Index( 0u))) Result;
    };
    
    template< class U>
    static
    U transpose( U u){
      return I::transpose(u);
    }
    
    template< class U>
    static
    bool is_negative( const U& u){
      return I::is_negative( u);
    }
    
    template< class U>
    static
    bool is_zero( const U& u){
      return I::is_zero( u);
    }

  };

  template< typename I>
  void decompose( const typename I::Matrix& mat,
                   typename I::Dec_Matrix& dmat, bool& success) {
    Skyline_Cholesky::decompose< Interface<I> >( mat, dmat, success);
  }

  template< typename I, typename B_Vector, typename X_Vector>
  void solve( const typename I::Dec_Matrix& dmat, const B_Vector& b, X_Vector& x){
    Skyline_Cholesky::solve< Interface<I> >( dmat, b, x);
  }

  //template< typename I>
  //void get_inverse( const typename I::Matrix& mat, typename I::Inv_Matrix& imat);

}}

