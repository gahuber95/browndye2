#include "macro_struct.hh"

namespace Browndye{
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
  
  // assume top is completed
  Macro_Struct::Macro_Struct( const JP::Node_Ptr& top, const Nonbonded_Parameter_Info& pinfo, Group_Index _ig){
    
    name = checked_value_from_node< string>( top, "name");
    atoms_file = checked_value_from_node< string>( top, "atoms");
    ig = _ig;
    atoms = pinfo.atoms_from_file( atoms_file);
    setup_map();

  }
  //####################################################################
  // for testing
  Macro_Struct::Macro_Struct( Test_Tag, const Vector< Atom, Atom_Index> &_atoms) {
    atoms = _atoms.copy();
    setup_map();
  }

  //#########################################################
  void Macro_Struct::setup_map() {
    auto na = atoms.size();
    for( auto ia: range(na)){
      auto& atom = atoms[ia];
      auto ja = atom.number;
      auto isnew = atom_imap.insert_or_assign( ja, ia).second;
      if (!isnew){
        error( "duplicate atom number", ja, " in ", name);
      }
    }
  }
  
}
