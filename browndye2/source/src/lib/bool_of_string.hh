#pragma once

#include <string>
namespace Browndye{
  bool bool_of_str( const std::string& tf);
  
  std::string str_of_bool( bool);
}

