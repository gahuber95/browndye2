#include <list>
#include <algorithm>
#include "../global/limits.hh"
#include "../global/indices.hh"
#include "../generic_algs/remove_duplicates.hh"
#include "../generic_algs/remove_if.hh"
#include "bead_constrainer_setupper.hh"
#include "group_constraint_interface.hh"
#include "../core_and_chain/chain/chain_state.hh"
#include "../core_and_chain/molcore/core_state.hh"
#include "group_state.hh"

namespace Browndye{

    //##############################################################
    template< class T, class I>
    bool have_common( const Vector< T,I>& va, const Vector< T,I>& vb){

      for( auto a: va){
        for( auto b: vb){
          if (a == b){
            return true;
          }
        }
      }
      return false;
    }

    //#######################################################
    void Bead_Constrainer_Setupper::add_chains_of_cores(){

      auto& bcc = comm.bound_cores_of_chains;
      related_chains.resize( nc);
      for( auto ic: range( nc)){
        if (is_frozen( ic)){
          auto& bcci = bcc[ic];
          for( auto jc: range( ic, nc)){
            if (is_frozen( jc)){
              auto& bccj = bcc[jc];
              if (have_common( bcci, bccj)){
                related_chains[ic].push_back( jc);
                related_chains[jc].push_back( ic);
              }
            }
          }
          remove_duplicates( related_chains[ic]);
        }
      }
    }

    //########################################################
    void Bead_Constrainer_Setupper::add_to_related_chains(){

      bool done = true;
      do{
        for( auto ic: range( nc)){
          auto& relci = related_chains[ic];
          for( auto jc: relci){
            auto& relcj = related_chains[jc];

            std::list< Chain_Index> extra_rels;
            std::set_difference( relcj.begin(), relcj.end(), relci.begin(), relci.end(), std::back_inserter( extra_rels));

            if (!extra_rels.empty()){
              done = false;

              for( auto ixc: extra_rels){
                relci.push_back( ixc);
              }
            }

            for( auto kc: related_chains[jc]){
              related_chains[ic].push_back( kc);
            }
          }
          remove_duplicates( relci);
        }
      }
      while( !done);
    }

//#############################################################
    void Bead_Constrainer_Setupper::remove_empty_related_chains(){

      auto is_empty = [&]( const auto& rels){
          return rels.empty();
      };

      remove_if_true( related_chains, is_empty);
    }
//##############################################################
    void Bead_Constrainer_Setupper::remove_dups_of_related_chains(){

      auto rcb = related_chains.begin();
      auto rce = related_chains.end();
      typedef Vector< Chain_Index, Ind_Chain_Index> CIndices;

      auto cicomp_eq = [](const CIndices& cs0, const CIndices& cs1){
          if (cs0.empty() && cs1.empty()){
            return true;
          }
          else{
            return cs0.front() == cs1.front();
          }
      };

      auto cicomp_lt = [](const CIndices& cs0, const CIndices& cs1){
          if (cs0.empty()){
            if (cs1.empty()){
              return false;
            }
            else{
              return true;
            }
          }
          else{
            if (cs1.empty()){
              return false;
            }
            else{
              return cs0.front() < cs1.front();
            }
          }
      };

      std::sort( rcb, rce, cicomp_lt);
      auto new_rce =
              std::unique( rcb, rce, cicomp_eq);

      related_chains.erase( new_rce, rce);

      remove_empty_related_chains();
    }
//########################################################
    void Bead_Constrainer_Setupper::setup_related_chains(){

      add_chains_of_cores();
      add_to_related_chains();
      remove_dups_of_related_chains();
    }

//#########################################################
    void Bead_Constrainer_Setupper::setup_min_chain_of_tet(){

      min_chain_of_tet =
              tet_chains.mapped( [&]( const Vector< Chain_Index, Ind_Chain_Index>& chains){
                  Chain_Index min_ic( maxi);
                  for( auto ic: chains){
                    min_ic = min( min_ic, ic);
                  }
                  return min_ic;
              });
    }
    void Bead_Constrainer_Setupper::setup_min_core_of_tet(){

      min_core_of_tet =
              tet_cores.mapped( [&]( const Vector< Core_Index, Ind_Core_Index>& cores){
                  Core_Index min_im( maxi);
                  for( auto im: cores){
                    min_im = min( min_im, im);
                  }
                  return min_im;
              });
    }

//############################################################
    void Bead_Constrainer_Setupper::setup_coreless_chain(){

      auto lnc = chain_states.size();
      for( auto ic: range( lnc)){
        auto& lstate = chain_states[ic];
        if (lstate.frozen){
          auto& lcomm = lstate.common();
          if (lcomm.is_coreless){
            coreless_chain = ic;
          }
        }
      }
    }

    //##################################################
    void Bead_Constrainer_Setupper::setup_free_cores(){

      // cores attached to no frozen chains
      auto& bcc = comm.bound_cores_of_chains;

      Vector< bool, Core_Index> core_free( nm, true);
      for( auto ic: range( nc)){
        if (is_frozen( ic)){
          for( auto im: bcc[ic]){
            core_free[im] = false;
          }
        }
      }

      for( auto im: range( nm)){
        if (core_free[im]){
          free_cores.push_back( im);
        }
      }
    }
//########################################################
    void Bead_Constrainer_Setupper::label_with_tets(){
      for( auto& chain: chain_states){
        chain.itet = std::nullopt;
      }
      for( auto& core: core_states){
        core.itet = std::nullopt;
      }

      for( auto it: range( tets.size())){
        auto& tetf = *(tets[it].fixed);
        for( auto ic: tetf.chain_indices){
          chain_states[ic].itet = it;
        }
        for( auto im: tetf.core_indices){
          core_states[im].itet = it;
        }
      }
    }

    //#######################################################
    void Bead_Constrainer_Setupper::setup_tets(){

      const Chain_Index c1( 1);
      auto nrc = related_chains.size();
      auto nfc = free_cores.size();

      nt = [&](){
          auto ncl = coreless_chain ? 1 : 0;
          return Tet_Index( nrc/c1 + nfc + ncl);
      }();

      tet_chains.resize( nt);

      for( auto ic: range( nrc)){
        Tet_Index it( ic/c1);
        tet_chains[it] = std::move( related_chains[ic]);
      }

      if (coreless_chain){
        tet_chains.back().push_back( coreless_chain.value());
      }

      tet_cores.resize( nt);

      auto& bcc = comm.bound_cores_of_chains;

      for( auto ic: range(nrc)){
        Tet_Index it( ic/c1);
        auto& tc = tet_cores[it];
        for( auto icl: tet_chains[it]){
          if (is_frozen( icl)){
            for( auto im: bcc[icl]){
              tc.push_back( im);
            }
          }
        }
        remove_duplicates( tc);
      }

      for( auto itd: range( nfc)){
        Tet_Index it( itd + nrc/c1);
        tet_cores[it].push_back( free_cores[ itd]);
      }

      tets.clear();
      for( auto it: range(nt)){
        tets.emplace_back( state, tet_cores[it], tet_chains[it]);
      }

      label_with_tets();
    }
    //############################################################
    template< class T, class Idx, class F>
    void sortf( Vector< T, Idx>& vec, F f){

      std::sort( vec.begin(), vec.end(), [&]( auto xa, auto xb) -> bool{
          return f(xa) < f(xb);
      });
    }

    //###################################################################
    void Bead_Constrainer_Setupper::setup_divs( Constraints_Info& cons_info){

      auto& divs = cons_info.divs;
      for( auto it: range(nt)){
        divs.push_back( it);
      }

      auto has_native = [&]( Chain_Index ic){
          auto& chain = comm.chains[ic];
          return !chain.atoms.empty();
      };

      for( auto ic: range(nc)){
        if (!is_frozen(ic) && has_native( ic)){
          divs.push_back( ic);
        }
      }

      auto div_order = [&]( Chain_Or_Tet_Index ict){

          auto for_chain = [&]( Chain_Index ic){
              return comm.chain_orders[ ic];
          };

          auto for_tet = [&]( Tet_Index it){
              auto im = min_core_of_tet[ it];
              return comm.core_orders[ im];
          };

          return std::visit( overload( for_chain, for_tet), ict);
      };

      sortf( divs, div_order);
    }
//###################################################################
    const Bead0_Index b0( 0);

    Cor_Cha_Index
    Bead_Constrainer_Setupper::order_index( const Constraints_Info& cons_info,
                                            Bead0_Index ib) const{

      auto mdiff = cons_info.mdiff;
      auto tdiff = cons_info.tdiff;

      if (ib < b0 + mdiff){
        auto& b = comm.all_chain_beads[ib];
        auto ic = b.is;
        return comm.chain_orders[ ic];
      }
      else if (ib < b0 + tdiff){
        auto& b = comm.all_core_beads[ ib - mdiff];
        auto im = b.is;
        return comm.core_orders[ im];
      }
      else{
        auto& b = comm.all_tet_beads[ ib - tdiff];
        auto it = b.is;
        auto& tet = tets[it];
        if (tet.fixed->core_indices.empty()){
          auto ic = min_chain_of_tet[ it];
          return comm.chain_orders[ ic];
        }
        else{
          auto im = min_core_of_tet[ it];
          return comm.core_orders[ im];
        }
      }
    }
//#####################################################################
    void Bead_Constrainer_Setupper::setup_bead_diffs( Constraints_Info& cons_info){

      const auto nacb = comm.all_chain_beads.size() - b0;
      const auto namb = comm.all_core_beads.size() - b0;

      const auto mdiff = nacb;
      const auto tdiff = nacb + namb;
      cons_info.mdiff = mdiff;
      cons_info.tdiff = tdiff;
    }

//##########################################################
    void Bead_Constrainer_Setupper::setup_chain_beads( Constraints_Info& cons_info){

      auto& all_chain_beads = comm.all_chain_beads;
      auto& beads = cons_info.beads;

      for( auto ib: range( all_chain_beads.size())){
        auto bead = all_chain_beads[ib];
        auto ic = bead.is;
        if (!is_frozen(ic)){
          beads.push_back( ib);
        }
      }
    }
//################################################################
    void Bead_Constrainer_Setupper::setup_core_beads( Constraints_Info& cons_info){

      auto& all_core_beads = comm.all_core_beads;
      auto& beads = cons_info.beads;

      for( auto ib: range( all_core_beads.size())){
        auto bead = all_core_beads[ib];
        auto im = bead.is;
        auto iia = bead.ia;
        auto& bound_atoms = comm.cores[im].bound_atoms;
        auto& ainfo = bound_atoms[iia];
        auto& cinfos = ainfo.chain_infos;
        for( auto& cinfo: cinfos){
          auto ic = cinfo.ic;
          if (!is_frozen(ic)){
            beads.push_back( ib + cons_info.mdiff);
            core_beads.push_back( ib);
            break;
          }
        }
      }
    }
//#################################################################
    void Bead_Constrainer_Setupper::setup_tet_beads( Constraints_Info& cons_info){

      auto& beads = cons_info.beads;

      for (auto it: range(nt)){
        for( auto ia: range( Atom_Index(ntet))){
          Bead0_Index ibr{ ntet*(it/Tet_Index(1)) + ia/Atom_Index(1)};
          auto ib = ibr + cons_info.tdiff;
          beads.push_back( ib);
        }
      }
    }
//##########################################################
    void Bead_Constrainer_Setupper::setup_bead_indices( Constraints_Info& cons_info){

      auto& all_chain_beads = comm.all_chain_beads;
      auto& all_core_beads = comm.all_core_beads;
      auto& all_tet_beads = comm.all_tet_beads;
      auto& bead_indices = cons_info.bead_indices;
      auto& beads = cons_info.beads;

      auto nbi = all_chain_beads.size() + (all_core_beads.size() - b0)
                 + (all_tet_beads.size() - b0);
      bead_indices.resize( nbi);
      bead_indices.fill( Bead_Index{ maxi});

      for( auto ib: range( beads.size())){
        auto ib0 = beads[ib];
        bead_indices[ ib0] = ib;
      }
    }

//#############################################################
    void Bead_Constrainer_Setupper::setup_beads( Constraints_Info& cons_info){

      setup_bead_diffs( cons_info);
      setup_chain_beads( cons_info);
      setup_core_beads( cons_info);
      setup_tet_beads( cons_info);

      auto oindex = [&]( Bead0_Index ib){
          return order_index( cons_info, ib);
      };

      sortf( cons_info.beads, oindex);

      setup_bead_indices( cons_info);
    }
//###################################################################
    Bead0_Index Bead_Constrainer_Setupper::tet_bead_index_of( Constraints_Info& cons_info, Tet_Index it, Atom_Index ia){
      Bead0_Index ibtp{ ntet*(it/Tet_Index(1)) + ia/Atom_Index(1)};
      return ibtp + cons_info.tdiff;
    }
//###################################################################
    void Bead_Constrainer_Setupper::setup_tet_core_constraints( Constraints_Info& cons_info){

      auto& tet_core_constraints = cons_info.tet_core_constraints;

      // improve scaling someday if needed
      for( auto it: range( nt)){
        auto& tet = tets[it];
        //Vector< Core_Bead> mbeads_of_tet;
        auto& mindices = tet.fixed->core_indices;
        for( auto im: mindices){
          for( auto ibmp: core_beads){
            auto mbead = comm.all_core_beads[ibmp];
            if (mbead.is == im){
              auto ibm = ibmp + cons_info.mdiff;
              auto iia = mbead.ia;
              auto& core_state = core_states[im];
              auto& core_comm = core_state.common();
              auto ia = core_comm.bound_atoms[ iia].ia;

              auto pos0 = core_comm.atoms[ia].pos;
              auto mpos = core_state.transformed( pos0);

              auto tlinks = Group_State::best_links( tet.poses, mpos);
              for( auto ja: tlinks){
                auto ibt = tet_bead_index_of( cons_info, it, ja);
                auto tpos = tet.fixed->poses0[ja];
                auto r = distance( tpos, mpos);
                auto& lcons = tet_core_constraints.emplace_back();
                lcons.beads[0] = ibt;
                lcons.beads[1] = ibm;
                lcons.length = r;
              }
            }
          }
        }
      }
    }
//###########################################################
    void Bead_Constrainer_Setupper::setup_tet_tet_constraints( Constraints_Info& cons_info){

      auto& tet_tet_constraints = cons_info.tet_tet_constraints;
      for( auto it: range( nt)){
        for( auto ia: range( Atom_Index(ntet))){
          auto ib = tet_bead_index_of( cons_info, it, ia);
          for( auto ja: range( ia)){
            auto jb = tet_bead_index_of( cons_info, it, ja);
            auto& lcons = tet_tet_constraints.emplace_back();
            lcons.beads[0] = ib;
            lcons.beads[1] = jb;
            lcons.length = tets[it].fixed->length;
          }
        }
      }
    }
//##############################################################
    template< class Con>
    bool Bead_Constrainer_Setupper::has_unfrozen_chain( const Constraints_Info& cons_info,
                                                        const Con& cons, int k){

      auto ib = cons.beads[k];
      if (ib < (b0 + cons_info.mdiff)){
        auto bead = comm.all_chain_beads[ ib];
        auto ic = bead.is;
        return !chain_states[ic].frozen;
      }
      else{
        return false;
      }
    }
//##################################################################
    const Constraint0_Index c0( 0);

    void Bead_Constrainer_Setupper::setup_length_constraints( Constraints_Info& cons_info){

      auto& constraints = cons_info.constraints;
      auto& all_length_constraints = comm.all_length_constraints;

      for( auto ico: range( all_length_constraints.size())){
        auto& con = all_length_constraints[ ico];
        if (has_unfrozen_chain( cons_info, con, 0) ||
            has_unfrozen_chain( cons_info, con, 1)){
          constraints.push_back( ico);
        }
      }

      const auto ncl = all_length_constraints.size() - c0;
      const auto cop_diff = ncl;
      cons_info.cop_diff = cop_diff;
    }
//#################################################################
    void Bead_Constrainer_Setupper::setup_coplanar_constraints( Constraints_Info& cons_info){

      auto& constraints = cons_info.constraints;
      auto& all_coplanar_constraints = comm.all_coplanar_constraints;

      auto cop_diff = cons_info.cop_diff;

      for( auto ico: range( all_coplanar_constraints.size())){
        auto& con = all_coplanar_constraints[ ico];

        bool has_unfrozen = false;
        for( auto k: range(4)){
          if (has_unfrozen_chain( cons_info, con, k)){
            has_unfrozen = true;
            break;
          }
        }

        if (has_unfrozen){
          constraints.push_back( ico + cop_diff);
        }
      }

      const auto ncc = all_coplanar_constraints.size() - c0;
      auto tm_diff = cop_diff + ncc;
      cons_info.tm_diff = tm_diff;
    }
//######################################################################
    void Bead_Constrainer_Setupper::settle_tet_constraints( Constraints_Info& cons_info){

      const auto& tet_core_constraints = cons_info.tet_core_constraints;
      const auto& tet_tet_constraints = cons_info.tet_tet_constraints;

      auto& constraints = cons_info.constraints;
      auto tm_diff = cons_info.tm_diff;

      for( auto ico: range( tet_core_constraints.size())){
        constraints.push_back( ico + tm_diff);
      }

      const auto ntc = tet_core_constraints.size() - c0;
      auto tt_diff = tm_diff + ntc;
      cons_info.tt_diff = tt_diff;

      for( auto ico: range( tet_tet_constraints.size())){
        constraints.push_back( ico + tt_diff);
      }
    }
//##################################################################
    void Bead_Constrainer_Setupper::sort_constraints( Constraints_Info& cons_info){

      auto cop_diff = cons_info.cop_diff;
      auto tm_diff = cons_info.tm_diff;
      auto tt_diff = cons_info.tt_diff;

      auto& tet_core_constraints = cons_info.tet_core_constraints;
      auto& tet_tet_constraints = cons_info.tet_tet_constraints;
      auto& constraints = cons_info.constraints;
      auto& all_length_constraints = comm.all_length_constraints;
      auto& all_coplanar_constraints = comm.all_coplanar_constraints;

      auto con_order_index = [&]( Constraint0_Index ico){

          auto min_bead_order = [&]( const auto& con){
              const auto nb = con.beads.size();
              Cor_Cha_Index oi{ maxi};
              for( auto k: range( nb)){
                auto ib = con.beads[k];
                oi = min( oi, order_index( cons_info, ib));
              }
              return oi;
          };

          if (ico < c0 + cop_diff){
            auto& con = all_length_constraints[ ico];
            return min_bead_order( con);
          }
          else if (ico < c0 + tm_diff){
            auto& con = all_coplanar_constraints[ ico - cop_diff];
            return min_bead_order( con);
          }
          else if (ico < c0 + tt_diff){
            auto& con = tet_core_constraints[ ico - tm_diff];
            return min_bead_order( con);
          }
          else{
            auto& con = tet_tet_constraints[ ico - tt_diff];
            return min_bead_order( con);
          }
      };

      sortf( constraints, con_order_index);
    }
//##############################################################
    void Bead_Constrainer_Setupper::setup_constraints( Constraints_Info& cons_info){

      setup_tet_core_constraints( cons_info);
      setup_tet_tet_constraints( cons_info);
      setup_length_constraints( cons_info);
      setup_coplanar_constraints( cons_info);
      settle_tet_constraints( cons_info);
      sort_constraints( cons_info);
    }

//#############################################################
    bool Bead_Constrainer_Setupper::is_frozen( Chain_Index ic) const{
      return chain_states[ic].frozen;
    }
//###########################
// constructor
    Bead_Constrainer_Setupper::
    Bead_Constrainer_Setupper( Group_Thread& _thread, Group_State& _state):
            state(_state), chain_states( state.chain_states),
            core_states( state.core_states), tets( state.tets),
            thread(_thread), comm(state.common()){

      nc = chain_states.size();
      nm = core_states.size();
    }


}