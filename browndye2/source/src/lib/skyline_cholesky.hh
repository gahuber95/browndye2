#pragma once

/*
Required Interface:

typedefs:
Matrix, Dec_Matrix, Index, Matrix_Value, Dec_Matrix_Value;

static functions:
Index size( const Matrix&);
Index size( const Dec_Matrix&);

//Index must have Index semantics as in ivector

// bandwidth of diagonal matrix is 0, tridiagonal 1
Index bandwidth( const Matrix&, Index);
Index bandwidth( const Dec_Matrix&, Index);

Matrix_Value element( const Matrix&, Index, Index);
Matrix_Value& element_ref( Matrix&, Index, Index);
Dec_Matrix_Value element( const Dec_Matrix&, Index, Index);
Dec_Matrix_Value& element_ref( Dec_Matrix&, Index, Index);

Matrix_Value transpose( Matrix_Value);
Dec_Matrix_Value transpose( Dec_Matrix_Value);

templated type and functions:
Vector_Type< T>::Result - gives vector containing type T
Value_Type< V>::Result - gives contained value of vector V; must be
  applicable to both vector arguments of solve()

template< class T>
T element( const I::Vector_Type<T>::Result&, Index);

template< class T>
T& element_ref( I::Vector_Type<T>::Result&, Index);

following two functions must be defined for vector arguments to solve()
template< class V>
T element( const V&, Index)

template< class V>
T& element_ref( V&, Index)

void error( const char*);

 */

#include <cmath>
#include "vector.hh"

namespace Browndye{

namespace Skyline_Cholesky{
  
  using std::max;
  using std::min;

  template< class I, class Mat, class Value>
  class MWrapper{
  public:
    typedef typename I::Index Index;

    MWrapper( Mat& mat):mat(mat){}

    Value operator()( int i, int j) const{
      return I::element( mat, Index(i), Index(j));
    }

    Value& operator()( int i, int j){
      return I::element_ref( mat, Index(i), Index(j));
    }
    
    int bandwidth( int i) const{
      return I::bandwidth( mat, Index(i))/Index(1);
    }

  private:
    
    Mat& mat;
  };


  template< class I, class Mat, class Value>
  class CMWrapper{
  public:
    typedef typename I::Index Index;

    CMWrapper( const Mat& mat):mat(mat){}

    Value operator()( int i, int j) const{
      return I::element( mat, Index(i),Index(j));
    }
    
    int bandwidth( int i) const{
      return I::bandwidth( mat, Index(i))/Index(1);
    }

  private:
    const Mat& mat;
  };

  template< class I, class Vec, class T>
  class VWrapper{
  public:
    typedef typename I::Index Index;

    VWrapper( Vec& vec):vec(vec){}

    T operator[]( int i) const{
      return I::element( vec, Index(i));
    }

    T& operator[]( int i){
      return I::element_ref( vec, Index(i));
    }
    
  private:
    Vec& vec;
  };

  template< class I, class Vec, class T>
  class CVWrapper{
  public:
    typedef typename I::Index Index;

    CVWrapper( const Vec& vec):vec(vec){}

    T operator[]( int i) const{
      return I::element( vec, Index(i));
    }
    
  private:
    const Vec& vec;
  };

  template< class I>
  void decompose( const typename I::Matrix& _mat, 
		  typename I::Dec_Matrix& _dmat, bool& success) {
      
    typedef typename I::Index Index;
    typedef typename I::Matrix_Value Value;
    typedef typename I::Dec_Matrix_Value Dec_Value;
    typedef typename I::Matrix Matrix;
    typedef typename I::Dec_Matrix Dec_Matrix;

    CMWrapper< I, Matrix, Value> mat( _mat);
    MWrapper< I, Dec_Matrix, Dec_Value> dmat( _dmat);

    success = true;    
    const int n = I::size( _mat)/Index(1);
    for (int i = 0; i < n; i++){
      auto bwi = dmat.bandwidth( i);
      for (int j = max( 0, i-bwi); j < i; j++){
        auto bwj = dmat.bandwidth( j);
        auto sum = mat( i,j); 
    	for (int k = max( 0, max( i-bwi, j-bwj)); k < j; k++){
          sum -= dmat(i,k)*I::transpose( dmat(j,k));
    	}
    	auto djj = dmat(j,j);
    	if (I::is_zero( djj)){
    	  success = false;
    	  return;
    	}
      	dmat(i,j) = sum/I::transpose( djj);
      }
      
      auto sum = mat( i,i);
      for ( int k = max( i-bwi, 0); k < i; k++){
    	sum = sum - dmat(i,k)*I::transpose( dmat(i,k)); //sq<I>( dmat(i,k));
      }

      if (I::is_negative( sum) || (sum != sum)){
        success = false;
        return;
      }
      dmat(i,i) = sqrt( sum);
    }
  }
  
  //############################################################
  template< class I, class B_Vector, class X_Vector>
  void solve( const typename I::Dec_Matrix& _dmat, 
	      const B_Vector& _b, X_Vector& _x){
    
    typedef typename I::Index Index;
    typedef typename I::Dec_Matrix_Value S;
    typedef typename I::Dec_Matrix Dec_Matrix;
    typedef typename I::template Value_Type< B_Vector>::Result B;
    typedef typename I::template Value_Type< X_Vector>::Result X;

    typedef decltype( B()/S()) Y;

    const int n = I::size( _dmat)/Index(1);
    Vector< Y> y( n);

    CMWrapper< I, Dec_Matrix, S> dmat( _dmat);
    CVWrapper< I, B_Vector, B> b(_b);
    VWrapper< I, X_Vector, X> x(_x);
    
    // forward substitution
    for (int i = 0; i < n; i++){
      int bw = dmat.bandwidth( i);
      auto sum = b[i]; //I::template zero< B>();
      for (int k = max( 0, i-bw); k < i; k++){
    	sum -= dmat(i,k)*y[k];
      }
      y[i] = sum/dmat(i,i);
    }

    // back substitution
    Vector< int> upper_widths( n, 0);
    for( auto i: range( n)){
      auto bw = dmat.bandwidth( i);
      for( auto jj: range( bw+1)){
        auto j = i - jj;
        upper_widths[j] = max( upper_widths[j], jj);
      }
    }
    
    const int nm1 =  n - 1;
    for( int i: range(n)){
      auto ic = nm1 - i;
      auto sum = y[ic]; 
      
      auto bw = upper_widths[ic];
      for( int k: range( 1, bw+1)){
        auto jc = ic+k;
        if (k <= dmat.bandwidth( jc)){
          sum -= I::transpose( dmat( jc,ic))*x[jc];
        }
      }
      x[ic] = sum/I::transpose( dmat(ic,ic));
    }
  }

}
}

//*******************************************************************
/*
#include <stdio.h>
#include <cmath>
#include <stdlib.h>
#include "skyline-cholesky.hh"
//#include "ivector.hh"

struct Matrix{

  void checki( int i) const{
    if (i < 0 || 2 < i){
      fprintf( stderr, "out of range %d\n", i);
      xexit(1);
    }
  }

  double operator()( int i, int j) const{
    checki( i);
    checki( j);
    if (abs(i-j) > 1){
      fprintf( stderr, "off matrix band %d %d\n",i,j);
      xexit(1);
    }
      
    return a[i][j];
  }

  double& operator()( int i, int j){
    checki( i);
    checki( j);
    if (abs(i-j) > 1){
      fprintf( stderr, "off matrix band %d %d\n",i,j);
      xexit(1);
    }

    return a[i][j];
  }

  Matrix(){
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
	a[i][j] = NAN;
  }

  unsigned int size() const{ return 3;}

  unsigned int bandwidth() const{ return 1;}

  double a[3][3];
};

class I{
public:
  typedef ::Matrix Matrix;
  typedef ::Matrix Dec_Matrix;
  typedef unsigned int Index;
  typedef double Matrix_Value;
  typedef double Dec_Matrix_Value;
  typedef unsigned int uint;

  static
  uint size( const Matrix& m){
    return m.size();
  }

  static
  uint bandwidth( const Matrix& m, uint i){
    return m.bandwidth();
  }

  static
  double element( const Matrix& m, uint i, uint j){
    return m(i,j);
  }

  static
  double& element_ref( Matrix& m, uint i, uint j){
    return m(i,j);
  }

  template< class T>
  class Vector_Type{
  public:
    typedef Vector< double> Result;
  };

  template< class V>
  class Value_Type{
  public:
    typedef double Result;
  };

  static
  void resize( Vector< double>& v, uint n){
    v.resize( n);
  }

  static
  double element( const Vector< double>& v, uint i){
    return v[i];
  }

  static
  double& element_ref( Vector< double>& v, uint i){
    return v[i];
  }

  static
  void error( const char* msg){
    fprintf( stderr, "%s\n", msg);
    xexit(1);
  }

};

int main(){
  Matrix a;
  a(0,0) = 2.0;
  a(0,1) = 1.0;

  a(1,0) = 1.0;
  a(1,1) = 3.0; 
  a(1,2) = 0.2; 

  a(2,1) = 0.2; 
  a(2,2) = 4.0; 

  Matrix aa = a;

  Vector< double> b( 3);
  b[0] = 1.0;
  b[1] = 20.0;
  b[2] = 3.0;


  Skyline_Cholesky::decompose<I>( aa, aa);

  Vector< double> x(3);  
  Skyline_Cholesky::solve<I>( aa, b, x);

  Vector< double> bb( 3);

  bb[0] = a(0,0)*x[0] + a(0,1)*x[1];
  bb[1] = a(1,0)*x[0] + a(1,1)*x[1] + a(1,2)*x[2];
  bb[2] = a(2,1)*x[1] + a(2,2)*x[2];

  printf( "%g %g %g\n", bb[0], bb[1], bb[2]);
}

*/

