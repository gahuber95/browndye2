#include <stack>
#include <type_traits>
#include <tuple>
#include <functional>

namespace Browndye{

namespace Combinators{
  
  template< class T>
  class is_tuple{
  public:
    static constexpr bool value = false;
  };

  template< class ... A>
  class is_tuple< std::tuple< A...> >{
  public:
    static constexpr bool value = true;
  };
  
}

//##################################################
template< class Fif, class Felse, class ... A>
auto primrec0(  Fif&& fif, Felse&& felse, A&& ... args){

  auto a = std::make_tuple( args...);  
  while(true){
    auto tf = std::apply( fif, a);
    if (tf){
      return a;
    }
    else{
      if constexpr (Combinators::is_tuple< decltype(a)>::value){
	  a = std::apply( felse, a);
	}
      else{
	a = std::apply( felse, std::make_tuple( a));
      }
    }
  }
}

//##################################################
template< class Fif, class Felse, class ... A>
auto primrec(  Fif&& fif, Felse&& felse, A&& ... args){

  auto a = std::make_tuple( args...);  
  while(true){
    auto [tf,b] = std::apply( fif, a);
    if (tf){
      return b;
    }
    else{
      auto ab = std::tuple_cat( a, std::make_tuple(b));
      a = std::apply( felse, ab);
    }
  }  
}

//##################################################
template< class Fif, class Fbase,
	  class Felse1, class Felse2, class A>
auto linrec0(  Fif&& fif, Fbase&& fbase,
		 Felse1&& felse1, Felse2&& felse2, A&& arg){

  std::stack< std::decay_t<A> > a_stack;
  std::invoke_result_t< Felse1, A> c;
  
  a_stack.push( std::forward<A>(arg));
  bool unwind = false;
  while(!unwind){
    auto& a = a_stack.top();
    auto tf = fif( a);
    if (tf){
      c = fbase( a);
      a_stack.pop();
      unwind = true;
    }
    else{
      a_stack.push( felse1( a));
    }  
  }
  
  while( !a_stack.empty()){
    auto& a = a_stack.top();
    c = felse2( a, c);
    a_stack.pop();
  }

  return c;
}

//##################################################
template< class Fif, class Fbase,
	  class Felse1, class Felse2, class A>
auto linrec1(  Fif&& fif, Fbase&& fbase,
		 Felse1&& felse1, Felse2&& felse2, A&& arg){


  typedef std::invoke_result_t< Felse1, A> C;

  typedef std::invoke_result_t< Fif, A> bB;
  typedef std::tuple_element_t< 0, bB> B;
  
  std::stack< std::decay_t<A> > a_stack;
  std::stack< std::decay_t<B> > b_stack;
  C c;
  
  a_stack.push( std::forward<A>(arg));
  bool unwind = false;
  while(!unwind){
    auto& a = a_stack.top();
    auto [tf,b] = fif( a);
    b_stack.push( b);
    if (tf){
      c = fbase( a,b);
      a_stack.pop();
      b_stack.pop();
      unwind = true;
    }
    else{
      a_stack.push( felse1( a,b));
    }  
  }
  
  while( !a_stack.empty()){
    auto& a = a_stack.top();
    auto& b = b_stack.top();
    c = felse2( a, b, c);
    a_stack.pop();
    b_stack.pop();
  }

  return c;
}

//#####################################################################
template <class F>
struct Y_Combinator {
  F f; // the lambda will be stored here

  // a forwarding operator():
  template <class... Args>
  decltype(auto) operator()(Args&&... args) const {
    // we pass ourselves to f, then the arguments.
    return f(std::ref(*this), std::forward<Args>(args)...);
  }
  
  Y_Combinator( F&& _f): f( std::forward<F>(_f)){}
};

// helper function that deduces the type of the lambda:
template <class F>
Y_Combinator<std::decay_t<F>> make_y_combinator(F&& f) {
    return {std::forward<F>(f)};
}

// Thanks to Barry (https://stackoverflow.com/users/2069064/barry) in
// https://stackoverflow.com/questions/2067988/recursive-lambda-functions-in-c11

}
