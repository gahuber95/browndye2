#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  Error-handling code.  There is a templated and overloaded "error" function that
  takes any number of printable arguments, concatenates them into a message, and
  throws an exception. 

*/

#include <iostream>
#include <sstream>
#include <stdexcept>
#include "debug.hh"

namespace Browndye{
  
  inline
  std::string error_msg_inner( std::stringstream& sst){
    return sst.str();
  }

  template< typename T, typename ... Args>
  std::string
  error_msg_inner( std::stringstream& sst, const T& s, Args ... args){
    sst << s << " ";
    return error_msg_inner( sst, args...);
  }

  template< typename ... Args>
  std::string error_msg( Args ... args){
    std::stringstream sst;
    return error_msg_inner( sst, args...);
  }
  
class Jam_Exception: public std::runtime_error{
public:
  explicit Jam_Exception( const std::string& msg): std::runtime_error( msg){}
 
  template< typename ... Args>
  explicit Jam_Exception( Args ... args): std::runtime_error( error_msg( args...)){}
};

template< typename ... Args>
void error( Args ... args){
  
  if constexpr (debug || bounds_check){
    std::cerr << error_msg( args...) << '\n';
    abort();
  }
  else{
    throw Jam_Exception( args...);
  }
}

}

//extern bool global_show;
//constexpr int thread_show = 1;
