#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#include <map>
#include <list>
#include <algorithm>
#include "../../lib/big_matrix.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../lib/units.hh"
#include "../../lib/array.hh"
#include "../../global/limits.hh"
#include "../../xml/node_info.hh"
#include "../../global/indices.hh"
#include "nonbonded_parameter_info.hh"
#include "../../structures/atom.hh"
#include "../../group/test_tag.hh"
#include "../force_field_type.hh"
#include "../../lib/sq.hh"
#include "../absinth/absinth.hh"

/*
This code reads in data for the near-field interactions.

This implements the MM_Nonbonded_Parameter_Info class, which reads in its data
from an XML file.  The MM_Nonbonded_Parameter_Info object, in turn, is used 
in the function 

template< class Interface>
void get_atoms_from_file( const MM_Nonbonded_Parameter_Info* pinfo, 
                          const std::string& file, 
                          typename Interface::Atoms& atoms,
                          bool use_param_charges,
                          bool use_param_radii);

which reads in a container of spheres from a file. The Interface
class has the following types and static functions:

Interface:

typedef Atoms;
void put_atom( Atoms atoms, Type_Index, Atom_Index num, Vec3< Length> pos, Charge, Length radius, Length int_radius);


Handling Lennard-Jones interactions
Each atom type may or may not have a set of LJ parameters
Each pair of atom types may or may not have a set of LJ parameters
Each atom has a radius (might be unused)

If either atom has a type maxi, use radii.  Otherwise, use lookup table.

*/

// need to finish unit test test9

namespace Browndye {


  class MM_Nonbonded_Parameter_Info_Base : public Nonbonded_Parameter_Info {

  public:
    typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

    MM_Nonbonded_Parameter_Info_Base(Force_Field_Type, Vector<JAM_XML_Pull_Parser::Node_Ptr> &);

    explicit MM_Nonbonded_Parameter_Info_Base(Force_Field_Type);

    MM_Nonbonded_Parameter_Info_Base(const MM_Nonbonded_Parameter_Info_Base &pinfo) = delete;

    MM_Nonbonded_Parameter_Info_Base(MM_Nonbonded_Parameter_Info_Base &&pinfo) = default;

    MM_Nonbonded_Parameter_Info_Base &operator=(const MM_Nonbonded_Parameter_Info_Base &pinfo) = delete;

    MM_Nonbonded_Parameter_Info_Base &operator=(MM_Nonbonded_Parameter_Info_Base &&pinfo) = default;

    ~MM_Nonbonded_Parameter_Info_Base() override;

    void establish_interaction_radii(Vector<Atom, Atom_Index> &) const override;

    //[[nodiscard]] Atom_Type_Index n_parameters() const;

    typedef decltype(Length6() * Length6()) Length12;

    typedef decltype(Energy() * Length12()) A_Coef;
    typedef decltype(Energy() * Length6()) B_Coef;

    static
    std::pair<A_Coef, B_Coef> mixed(A_Coef A0, B_Coef B0,
                                    A_Coef A1, B_Coef B1);

    Force_Field_Type fft = Force_Field_Type::None;
    double one_four_factor = 0.0;

    // defaults from https://pubs.acs.org/doi/pdf/10.1021/ja809567k
    // Gabdoulline and Wade, JACS, 2009, p. 9232 (3rd page)
    struct Hydrophobic_Params {
      Length a{3.1}, b{4.35};
      double c{0.5};
      Surface_Tension beta{-energy_conv_factor * 0.025}; // will be negative
      decltype(Force() / Length2()) factor; // beta*c/(b - a)

      void compute_factor() {
        factor = beta * c / (b - a);
      }

      Hydrophobic_Params() {
        compute_factor();
      }
    };

    Hydrophobic_Params hp_params; // make sure they are input!;

    Energy epsilon(Atom_Type_Index) const;

    Length sigma(Atom_Type_Index) const;

    // final expression is multiplied by kT = 1 in BrownDye units
    struct LJ_Parameters {
      A_Coef A = A_Coef(NaN);
      B_Coef B = B_Coef(NaN);

      bool operator<(LJ_Parameters other) const {

        auto pairo = [](LJ_Parameters ab) {
          return std::make_pair(ab.A, ab.B);
        };

        return pairo(*this) < pairo(other);
      }
    };

  protected:
    friend class Back_Door;

    //class TAtom_ITag;
    //typedef Index< TAtom_ITag> TAtom_Index;

    static constexpr double hs_lj_a_factor() { return 0.9345; } //0.89541;}
    [[nodiscard]] Force sasa_force(Length r, const Atom &, const Atom &) const;

    void get_hydrophobic_params(const JAM_XML_Pull_Parser::Node_Ptr &);

    // leave zero type as blank
    Big_Matrix<std::optional<LJ_Parameters>, Atom_Type_Index, Atom_Type_Index> lj_params;

    // used eventually in absinth model
    Vector<Energy, Atom_Type_Index> epsilons; // initialize!
    Vector<Length, Atom_Type_Index> sigmas; // initialize!

    // more functions
    static
    std::pair<A_Coef, B_Coef> AB_of_sigeps(Energy epsilon, Length sigma);

    template<class T>
    static
    T max(T t0, T t1) {
      return std::max(t0, t1);
    }
    
    virtual bool uses_absinth() const = 0;

    template< class T>
    using pair2 = std::pair< T,T>;

    void input_params( Vector< Node_Ptr>& top_nodes, Vector< LJ_Parameters, Atom_Type_Index>& single_params,
                                                           std::map< pair2<Atom_Type_Index>, LJ_Parameters>& pair_params, std::map< pair2< std::string>, Atom_Type_Index>& pre_dict);

    static void get_pair_parameters( Node_Ptr top_node, std::map< pair2< std::string>, Atom_Type_Index>& pre_dict,
                                                  std::map< pair2<Atom_Type_Index>, LJ_Parameters>& pair_params);

    void get_absinth_params( Vector< Node_Ptr>& top_nodes);
    void check_lj_pairs() const;
  };

//#############################################################################
  template<Force_Field_Type FFT>
  class MM_Nonbonded_Parameter_Info : public MM_Nonbonded_Parameter_Info_Base {
  public:
    using MM_Nonbonded_Parameter_Info_Base::MM_Nonbonded_Parameter_Info_Base;

    ~MM_Nonbonded_Parameter_Info() override;

    [[nodiscard]]
    F3 force( Pos r01, Length r, const Atom &, const Atom &) const;

    [[nodiscard]]
    Energy potential(Length r01, const Atom &, const Atom &) const;
    
    bool uses_absinth() const override;
  };

  //#########################################
  template< Force_Field_Type FFT>
  bool MM_Nonbonded_Parameter_Info< FFT>::uses_absinth() const{
    
    return FFT == Force_Field_Type::Absinth;
  }

  //#############################################################################
  template< Force_Field_Type FFT>
  MM_Nonbonded_Parameter_Info< FFT>::~MM_Nonbonded_Parameter_Info() = default;

//###################################################################
  template< Force_Field_Type FFT>
  F3 MM_Nonbonded_Parameter_Info< FFT>::force(Vec3<Length> r01, Length r,
                                                const Atom &atom0, const Atom &atom1) const {

    auto pow6 = [](auto ar) -> auto {
      auto ar2 = ar * ar;
      auto ar4 = ar2 * ar2;
      return ar2 * ar4;
    };

    const Energy unit_kT{1.0};

    Vec3<Force> F;
    constexpr Atom_Type_Index amax{maxi};

    auto radius0 = atom0.radius;
    auto radius1 = atom1.radius;
    auto it0 = atom0.type;
    auto it1 = atom1.type;

    auto repul = [&]() {
      auto a = hs_lj_a_factor() * (radius0 + radius1);
      auto ar6 = pow6(a / r);
      Energy ar12 = unit_kT * ar6 * ar6;
      auto fox = 12.0 * ar12 / (r * r);
      return fox * r01;
    };

    auto lj_force = [&]( auto& paramso){
      auto params = *paramso;

      auto A = params.A;
      auto B = params.B;
      auto ri6 = pow6(1.0/r);
      auto ri12 = ri6*ri6;
      auto fox = (6.0/(r*r))*( 2.0*A*ri12 - B*ri6);
      return fox * r01;
    };

    if constexpr (FFT == Force_Field_Type::Absinth){
      auto &paramso = lj_params(it0, it1);
      F = lj_force( paramso);
    }
    else {
      if (it0 == amax || it1 == amax) {
        F = repul();
      }
      else {
        auto &paramso = lj_params(it0, it1);

        if (paramso) {
          F = lj_force(paramso);
        } else {
          F = repul();
        }
      }

      if constexpr (FFT == Force_Field_Type::Molecular_Mechanics_HP) {
        auto Fh = [&] {
          auto u = r01 / r;
          auto saf = sasa_force(r, atom0, atom1);
          return saf * u;
        }();
        F += Fh;
      }
    }

    return F;
  }

//###################################################################
// correct, update and optimize
// check carefully, once in use!
  template< Force_Field_Type FFT>
  Energy MM_Nonbonded_Parameter_Info< FFT>::potential(Length r, const Atom &atom0, const Atom &atom1) const {

    auto pow6 = [](auto ar) -> auto {
      auto ar2 = ar * ar;
      auto ar4 = ar2 * ar2;
      return ar2 * ar4;
    };

    const Energy unit_kT{1.0};

    Energy V;
    constexpr Atom_Type_Index amax{maxi};

    auto radius0 = atom0.radius;
    auto radius1 = atom1.radius;
    auto repul = [&]() {

      auto a = hs_lj_a_factor() * (radius0 + radius1);
      auto ar6 = pow6(a / r);
      return unit_kT * ar6 * ar6;
    };

    auto it0 = atom0.type;
    auto it1 = atom1.type;
    if (it0 == amax || it1 == amax) {
      V = repul();
    } else {
      auto &paramso = lj_params(it0, it1);

      if (paramso) {
        auto params = paramso.value();

        auto A = params.A;
        auto B = params.B;
        auto ri6 = pow6(1.0 / r);
        auto ri12 = ri6 * ri6;
        auto Vox = 2.0 * A * ri12 - B * ri6; // check!
        V = Vox;
      } else {
        V = repul();
      }
    }
    return V;
  }

}
