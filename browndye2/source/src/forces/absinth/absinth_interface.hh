#pragma once
#include "../gen_interface.hh"
#include "absinth.hh"

namespace Browndye{

  namespace Absinth_Iface_In{

    struct Sys{
      System_State& state;
      Energy energy{ 0.0};

      typedef Parameters< MM_Nonbonded_Parameter_Info< Force_Field_Type::Absinth>, MM_Bonded_Parameter_Info> Params;
      const Params& params;
      Vector< Absinth_Atom, Atom_Index >* ab_atoms = nullptr;

      Sys( System_State& _state, const Params& _params, Vector< Absinth_Atom, Atom_Index >& aa): state(_state), params(_params), ab_atoms( &aa){};
      Sys( System_State& _state, const Params& _params): state(_state), params(_params){};

      [[nodiscard]]
      double one_four_factor() const{
        return params.pinfo.one_four_factor;
      }

      [[nodiscard]] Length debye_length() const {
        return params.solvent.debye_length;
      }
    };

    inline
    const Atom& atom_prot( const Sys&, const Chain_Col_Interface::Container& cont, Atom_Index ia){
      return cont.atoms[ia];
    }

    inline
    const Atom& atom_prot( const Sys&,  const Small_Col_Interface::Container& cont, Atom_Index ia){
      return cont.common.atoms[ia];
    }

    inline
    const Atom& atom_prot( const Sys&,  const Large_Col_Interface< false>::Container& atoms, Atom_Index ia){
      return atoms[ia];
    }

    inline
    Chain_State_Atom& atom_of( Sys&, Chain_Col_Interface::Container& cont, Atom_Index ia){
      return cont.chain.atoms[ia];
    }

    inline
    const Chain_State_Atom& atom_of( const Sys&, const Chain_Col_Interface::Container& cont, Atom_Index ia){
      return cont.chain.atoms[ia];
    }

    inline
    Absinth_Atom& atom_of( Sys& sys, Large_Col_Interface< false>::Container& cont, Atom_Index ia){
      return (*(sys.ab_atoms))[ia];
    }

    inline
    const Absinth_Atom& atom_of( const Sys& sys, const Large_Col_Interface< false>::Container& cont, Atom_Index ia){
      return (*(sys.ab_atoms))[ia];
    }

    inline
    Absinth_Atom& atom_of( Sys& sys, Pre_Core_Thread& cont, Atom_Index ia){
      return cont.ab_atoms[ia];
    }

    inline
    const Absinth_Atom& atom_of( const Sys& sys, const Pre_Core_Thread& cont, Atom_Index ia){
      return cont.ab_atoms[ia];
    }

    template< class Cont>
    void subtract_from_etaa( Sys& sys, Cont& cont, Atom_Index ia, double sub_eta){
      auto& atom = atom_of( sys, cont, ia);
      auto eta = atom.eta();
      atom.set_eta( eta - sub_eta);
    }

    template< class Cont>
    void add_to_dEdetaa( Sys& sys, Cont& cont, Atom_Index ia, Energy dE){
      auto& atom = atom_of( sys, cont, ia);
      auto deta = atom.dEdeta();
      atom.set_dEdeta( deta + dE);
    }

    template< class Cont>
    void put_polar_va( Sys& sys, Cont& cont, Atom_Index ia, double v){
      auto& atom = atom_of( sys, cont, ia);
      atom.set_polar_v( v);
    }

    template< class Cont>
    void put_polar_dvdetaa( Sys& sys, Cont& cont, Atom_Index ia, double dvdeta){
      auto& atom = atom_of( sys, cont, ia);
      atom.set_polar_dvdeta( dvdeta);
    }

    template< class Cont>
    Energy dEdetaa( const Sys& sys, const Cont& cont, Atom_Index ia){

      return atom_of( sys, cont, ia).dEdeta();
    }

    template< class Cont>
    double etaa( const Sys& sys, const Cont& cont, Atom_Index ia){

      return atom_of( sys, cont, ia).eta();
    }

    template< class Cont>
    double polar_va( const Sys& sys, const Cont& cont, Atom_Index ia){

      return atom_of( sys, cont, ia).polar_v();
    }

    template< class Cont>
    double polar_dvdetaa( const Sys& sys, const Cont& cont, Atom_Index ia){

      return atom_of( sys, cont, ia).polar_dvdeta();
    }

    template< class Is_temp_eta, class Cont>
    double eta_maxa( const Sys& sys, const Cont& cont, Atom_Index ia){

      if constexpr (std::is_same_v< Is_temp_eta, std::true_type>) {
        return atom_prot(sys, cont, ia).eta_max();
      }
      else{
        return atom_of( sys, cont, ia).eta0();
      }
    }

    template< class Cont>
    Energy surface_energya( const Sys& sys, const Cont& cont, Atom_Index ia){

      return atom_prot( sys, cont, ia).surface_energy();
    }

    template< class Cont>
    Charge chargea( const Sys& sys, const Cont& cont, Atom_Index ia){
      return atom_prot( sys, cont, ia).charge;
    }

    template< class I>
    Pos positiona(typename I::Transform tform, typename I::Container& cont, Atom_Index i){
      return I::transformed_position( cont, tform, i);
    }

    template< class I>
    Length radiusa( const typename I::Container& cont, Atom_Index i){
      return I::physical_radius( cont, i);
    }

    // any evaluation involving a core is truncated
    template< template <class, class, bool> class F>
    class TWrapper{
    public:
      template< class Ia, class Ib>
      class Wrapped {
      public:
        template< class SubA, class SubB>
        static void doit( Sys& sys, SubA& suba, SubB& subb, Atom_Index ia, Atom_Index ib, double){
          constexpr bool is_cora = !SubA::is_chain;
          constexpr bool is_corb = !SubB::is_chain;
          constexpr bool trunc = is_cora || is_corb;
          F< Ia, Ib, trunc>::doit( sys, suba, subb, ia, ib);
        }
      };
    };

    // rather strange, letting the subsystem and the interface be the same
    // Ia is usually Large_Col, Small_Col, Chain_Col _Interface
    template< class Ia, bool temp_eta>
    struct Outer_Loop: public Ia {
      typedef Ia Iface;
      typedef Outer_Loop< Ia, temp_eta> Subsystem;
      typedef typename Ia::Container Container;
      typedef typename Ia::Refs Refs;
      typedef typename Ia::Ref Ref;
      typedef Ref Atom_Ref;
      typedef typename Ia::Transform Transform;

      Transform tforma;
      Pos cena;
      Container &cona;
      Refs& refsa;

      F3 force = F3( Zeroi{});
      T3 torque = T3( Zeroi{});

      explicit Outer_Loop( Sys&, Transform _tforma, Container &_cona, Refs& _refsa, Pos, Length) :
              tforma(_tforma), cona(_cona), refsa( _refsa) {
        cena = tforma.translation();
      }

      static
      Pos position( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return positiona<Ia>( oloop.tforma, oloop.cona, i);
      }

      static
      Charge charge( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return chargea( sys, oloop.cona, i);
      }

      static
      Length radius( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Ref i){
        return radiusa<Ia>( oloop.cona, i);
      }

      static
      void subtract_from_eta( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Ref i, double sub_eta){
        subtract_from_etaa( sys, oloop.cona, i, sub_eta);
      }

      static
      void add_to_dEdeta( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Ref i, Energy dE){
        add_to_dEdetaa( sys, oloop.cona, i, dE);
      }

      static
      Energy dEdeta( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return dEdetaa( sys, oloop.cona, i);
      }

      static
      double eta( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return etaa( sys, oloop.cona, i);
      }

      static
      double polar_v( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return polar_va( sys, oloop.cona, i);
      }

      static
      double polar_dvdeta( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return polar_dvdetaa( sys, oloop.cona, i);
      }

      static
      double eta_max( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i) {
        if constexpr (temp_eta) {
          return eta_maxa< std::true_type>(sys, oloop.cona, i);
        }
        else{
          return eta_maxa< std::false_type>(sys, oloop.cona, i);
        }
      }

      static
      Energy surface_energy( const Sys& sys, const Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i){
        return surface_energya( sys, oloop.cona, i);
      }

      static
      void put_polar_v( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i, double v){
        put_polar_va( sys, oloop.cona, i, v);
      }

      static
      void put_polar_dvdeta( Sys& sys, Outer_Loop< Ia, temp_eta>& oloop, Atom_Index i, double dvdeta){
        put_polar_dvdetaa( sys, oloop.cona, i, dvdeta);
      }

    }; // end Outer_Loop

    // Ia is usually Small_Col, Chain_Col _Interface
    template< class Ib, bool temp_eta>
    struct Inner_Loop: public Ib {
      typedef Inner_Loop< Ib, temp_eta> Subsystem;
      typedef typename Ib::Container Container;
      typedef typename Ib::Transform Transform;
      typedef typename Ib::Refs Refs;
      typedef typename Ib::Ref Ref;
      typedef Ref Atom_Ref;

      Container &conb;
      Transform tformb;
      F3 force = F3( Zeroi{});
      Pos posa;

      template< class Ia>
      Inner_Loop( Sys& sys, Transform _tformb, Container & _conb, Refs&, Pos, Length, Outer_Loop<Ia, temp_eta>& loopa, typename Ia::Ref ia) :
        conb(_conb), tformb(_tformb){
        posa = Ia::position( loopa.cona, ia);
      }

      static
      Pos position( Sys sys, Inner_Loop< Ib, temp_eta>& iloop, Atom_Ref i){
        return positiona<Ib>(iloop.tformb, iloop.conb, i);
      }

      static
      Charge charge( Sys& sys, Inner_Loop< Ib, temp_eta>& iloop, Atom_Index i){
        return chargea( sys, iloop.conb, i);
      }

      static
      Length radius( Sys sys, Inner_Loop< Ib, temp_eta>& iloop, Atom_Ref i){
        return radiusa<Ib>( iloop.conb, i);
      }

      static
      void subtract_from_eta( Sys& sys, Inner_Loop< Ib, temp_eta>& iloop, Atom_Ref i, double sub_eta){
        subtract_from_etaa( sys, iloop.conb, i, sub_eta);
      }

      static
      void add_to_dEdeta( Sys& sys, Inner_Loop< Ib, temp_eta>& iloop, Atom_Ref i, Energy dE){
        add_to_dEdetaa( sys, iloop.conb, i, dE);
      }

      static
      Energy dEdeta( const Sys& sys, const Inner_Loop< Ib, temp_eta>& iloop, Atom_Index i){
        return dEdetaa( sys, iloop.conb, i);
      }

      static
      double polar_v( const Sys& sys, const Inner_Loop< Ib, temp_eta>& iloop, Atom_Index i){
        return polar_va( sys, iloop.conb, i);
      }

      static
      double polar_dvdeta( const Sys& sys, const Inner_Loop< Ib, temp_eta>& iloop, Atom_Index i){
        return polar_dvdetaa( sys, iloop.conb, i);
      }
    };

    // check sign on force!
    template< class Ia, class Ib, bool temp_eta>
    void add_force( Sys& sys, Outer_Loop< Ia, temp_eta> &loopa, Inner_Loop< Ib, temp_eta>& loopb, Pos, Pos, typename Ia::Ref ia, typename Ib::Ref ib,  F3 f) {

      if constexpr (Ia::is_chain){
        loopa.cona.catoms[ia].force -= f;
      }
      else {
        loopa.force -= f;
        auto r = loopa.position( sys, loopa, ia) - loopa.cena;
        loopa.torque -= cross( r, f);
      }

      if constexpr (Ib::is_chain){
        loopb.conb.catoms[ib].force += f; // check sign!
      }
      else{
        loopb.force += f;
      }
    }

  } // end namespace Absinth_Iface_In


  //##################################################################################################
  //namespace AII = Absinth_Iface_In;

  class Absinth_Iface_Base{
  public:
    typedef Absinth_Iface_In::Sys System;
  };

  template< bool temp_eta>
  class Absinth_Iface: public Absinth_Iface_Base{
  public:
    static
    void reset_etas( System&);

    static
    Length debye_length( const System& sys){
      return sys.debye_length();
    }

    static
    void add_to_energy( System& sys, Energy dE){
      sys.energy += dE;
    }

    template< class Ia> using Outer_LoopG = Absinth_Iface_In::Outer_Loop< Ia, temp_eta>;
    template< class Ib> using Inner_LoopG = Absinth_Iface_In::Inner_Loop< Ib, temp_eta>;

    template< template <class, class> class F>
    using Pair_Applier = Pair_Applier_Gen< Absinth_Iface, Outer_LoopG, Inner_LoopG, F, &Chain_Common::excluded_atoms, &Chain_Common::excluded_core_atoms>;

    template< template< class> class F>
    class Atom_Applier {
    public:
      static void doit( System &);
    };

    template< template <class, class, bool> class F>
    using Charge_Pair_Applier = Pair_Applier_Gen< Absinth_Iface, Outer_LoopG, Inner_LoopG,
      Absinth_Iface_In::TWrapper<F>:: template Wrapped,
      &Chain_Common::coulomb_excluded_atoms, &Chain_Common::coulomb_excluded_core_atoms>;

    template< class Ia, class Ib>
    static
    void add_force( System& sys, typename Ia::Subsystem& suba, typename Ib::Subsystem subb, Pos posa, Pos posb, typename Ia::Ref ia, typename Ib::Ref ib, F3 f){
      Absinth_Iface_In::add_force( sys, suba, subb, posa, posb, ia, ib, f);
    }
  };

  template< bool temp_eta>
  template< template< class> class F>
  void Absinth_Iface< temp_eta>::Atom_Applier<F>::doit( System& sys){

    auto& state = sys.state;
    auto& groups = state.groups;
    auto ng = groups.size();
    for( auto ig: range( ng)){
      auto& group = groups[ig];

      // chains
      auto& chains = group.chain_states;
      auto nc = chains.size();
      for( auto ic: range( nc)){
        auto& chain = chains[ic];
        auto& cont = chain.collision_info->container;
        Vector< Atom_Index, Atom_Index> refs;
        typedef Absinth_Iface_In::Outer_Loop< Chain_Col_Interface, temp_eta> Loop;
        Loop oloop( sys, Blank_Transform{}, cont, refs, Pos{}, Length{});

        auto& atoms = chain.atoms;
        auto na = atoms.size();
        for( auto ia: range( na)){
          F< Loop>::doit( sys, oloop, ia);
        }
      }

      auto& cores = group.core_states;
      // zero core
      {
        if (!cores.empty() && ig == Group_Index(0)){
          auto& core = cores.front();
          auto& cont = core.common().atoms;
          Vector< Atom_Index, Ind_Atom_Index> refs;
          typedef Absinth_Iface_In::Outer_Loop< Large_Col_Interface< false>, temp_eta> Loop;
          Loop oloop( sys, Blank_Transform{}, cont, refs, Pos{}, Length{});
        }
      }

      // cores
      auto nm = cores.size();
      for( auto ic: range( Core_Index(1), nm)){
        typedef Absinth_Iface_In::Outer_Loop< Small_Col_Interface, temp_eta> Loop;
        auto& core = cores[ic];
        Pre_Core_Thread& cont = core.thread();
        Vector< Atom_Index, Ind_Atom_Index> refs;
        Loop oloop( sys, core.transform(), cont, refs, Pos{}, Length{});

        auto& atoms = core.atoms();
        auto na = atoms.size();
        for( auto ia: range( na)){
          F< Loop>::doit( sys, oloop, ia);
        }
      }
    }
  }

  //##########################################################################
  template< bool temp_eta>
  void Absinth_Iface< temp_eta>::reset_etas( System& sys){

    auto set_eta = [&]( auto& atom, auto& atom_state){

      auto etam = [&] { ;
        if constexpr (temp_eta) {
          return atom_state.eta0();
        }
        else {
          return atom.eta_max();
        }
      }();

      atom_state.set_eta( etam);
      atom_state.set_dEdeta( Energy{ 0.0});
    };

    System_State& state = sys.state;
    auto& groups = state.groups;
    for( auto& group: groups){

      auto& chains = group.chain_states;
      for( auto& chain: chains){
        auto& cchain = chain.common();
        auto& atom_states = chain.atoms;
        auto& atoms = cchain.atoms;

        auto na = atoms.size();
        for( auto ia: range( na)){
          auto& atom_state = atom_states[ia];
          auto& atom = atoms[ia];
          set_eta( atom, atom_state);
        }
      }

      auto& gthr = group.thread();
      for( auto& core: gthr.core_threads){
        auto& corec = core.common;
        auto na = core.ab_atoms.size();
        for( auto ia: range( na)){
          auto& atom_state = core.ab_atoms[ia];
          auto& atom = corec.atoms[ia];
          set_eta( atom, atom_state);
        }
      }
    }
  }
}
