#include <ctype.h>
#include "str_stack.hh"

// Implements "stack of strings"

using std::move;

namespace Browndye{
namespace JAM_XML_Parser{

bool isspaces( const String& str){
  const char* chars = str.c_str();
  size_t n = str.size();
  for (size_t i = 0; i<n; i++){
    if (!isspace( chars[i]))
      return false;
  }
  return true;
}

void add_string( Str_Stack& str_stack, String str){

  str_stack.push( str);

  bool done = false;
  while (!done){
    String top0 = move( str_stack.top());
    str_stack.pop();
    if (isspaces( top0))
      top0 = String(" ");

    if (str_stack.empty()){
      str_stack.push( move( top0));
      done = true; 
    }
    else {
      if (2*top0.size() >= str_stack.top().size()){
	auto top1 = move( str_stack.top());
        str_stack.pop();
        const String top01 =  top1 + top0;
        str_stack.push( move( top01));
      }
      else {
        str_stack.push( move( top0));
        done = true;
      }
    } 
  }
}        

/***********************************/
const String string_from_stack( Str_Stack& str_stack){
  String res("");
  while (!str_stack.empty()){
    const String next = move( str_stack.top());

    str_stack.pop();
    res = next + res;
  }

  return res;
}
}}
