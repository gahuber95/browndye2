#pragma once

/*
 * time_step_parameters.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#include <fstream>
#include "../lib/units.hh"
#include "../global/limits.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"

namespace Browndye{

class System_Common;

struct Time_Step_Parameters{
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  Time dt_min_core{ 0.0}, dtr_min_core{ 0.0};
  Time dt_min_chain{ 0.0}, dtr_min_chain{ 0.0};  
  std::optional< Time> const_dt_opt;

  Time_Step_Parameters( Node_Ptr& node);
  Time_Step_Parameters() = default;
  
  void output( std::ofstream&) const;
};
}

