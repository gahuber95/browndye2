(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Used internally.
Applies "f" to all XML nodes labeled "point" in "buffer". 
The tags "x", "y", "z", 
"radius", and "type" are used to feed the corresponding labeled arguments
to "f". The tag used to extract the argument "charge" is the "qtag" argument
to "apply_to_points".
*)

type ftype = (x: float -> y: float -> z: float -> radius: float -> charge: float -> ptype: string -> unit);;

(* f x y z r q type *)
let apply_to_points ~qtag ~buffer:buf ~(f:ftype) = 
  try
    let ftags = ["x"; "y"; "z"; "radius"; qtag]
    and stags = ["type"]
    in
      
    let pre_f int_res flt_res str_res = 
      
      let foundf tag =
	try
	  List.assoc tag flt_res
	with Not_found ->
	  nan
	  
      and founds tag =
	try
	  List.assoc tag str_res
	with Not_found ->
	  ""
      in
	
      let x = foundf "x"
      and y = foundf "y"
      and z = foundf "z"
      and r = foundf "radius"
      and q = foundf qtag
      and ptype = founds "type" in
	f ~x: x ~y: y ~z: z ~radius: r ~charge: q ~ptype: ptype
	  
    in
      Object_parser.apply_to_objects "point" buf [] ftags stags pre_f

  with ex ->
    Printf.fprintf stderr "error in point parser\n";
    raise ex
;;
       


      
