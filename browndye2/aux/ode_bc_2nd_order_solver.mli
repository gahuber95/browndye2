(*
Used internally.
Solves the 1D 2nd-order boundary value problem using finite differences.

  cxx*pxx + cx*px + c1*p + c0 = 0
 
  on [0, 1]
 
with two Diriclet boundary conditions.

solve_dd cxx cx c1 c0 bc0 bc1 p 

The number of evenly-spaced points is determined from the size of "p",
and the two end points are included in the solution.  The boundary
values are given by "bc0" and "bc2", and the coefficients are given
by functions of x.
*)
 
val solve_dd :
  (float -> float) ->
  (float -> float) ->
  (float -> float) ->
  (float -> float) ->
  float -> float -> float array -> unit
