#pragma once

#include "array.hh"
#include "vector.hh"

namespace Browndye{

namespace Linalg{

template< class T, size_t n>
class Cholesky_I{
public:
  typedef decltype( sqrt( T())) ST;
  typedef SMat< T,n,n> Matrix;
  typedef SMat< ST, n,n> Dec_Matrix;
  typedef T Matrix_Value;
  typedef ST Dec_Matrix_Value;
  
  template< class U>
  static
  size_t size( const SMat< U, n,n>& A){
    return A.size();
  }

  template< class U>
  static
  U element( const SMat< U, n,n>& A, size_t i, size_t j){
    return A[i][j];
  }

  template< class U>
  static
  U& element_ref( SMat< U, n,n>& A, size_t i, size_t j){
    return A[i][j];
  }

  template< class U>
  class Vector_Type{
  public:
    typedef Array< U, n> Result;
  };
   
  typedef size_t Index;
  
  template< class U>
  static
  void resize( Array< U, n>&, size_t){}

  template< class U>
  static
  U element( const Array< U, n>& A, size_t i){
    return A[i];
  }

  template< class U>
  static
  U& element_ref( Array< U, n>& A, size_t i){
    return A[i];
  }

  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }

  template< class U>
  static
  U transpose( U u){
    return u;
  }
  
  template< class U>
  static
  bool is_negative( U u){
    return u < U{ 0.0};
  }

};

  //###############################################
template< class T>
class Cholesky_LI{
public:
  typedef decltype( sqrt( T())) ST;

  template< class TT>
  using VMatrix = Vector< Vector< TT> >;
  
  typedef VMatrix< T> Matrix;
  typedef VMatrix< ST> Dec_Matrix;
  typedef T Matrix_Value;
  typedef ST Dec_Matrix_Value;
  
  template< class U>
  static
  size_t size( const VMatrix< U>& A){
    return A.size();
  }

  template< class U>
  static
  U element( const VMatrix< U>& A, size_t i, size_t j){
    return A[i][j];
  }

  template< class U>
  static
  U& element_ref( VMatrix< U>& A, size_t i, size_t j){
    return A[i][j];
  }

  template< class U>
  class Vector_Type{
  public:
    typedef Vector< U> Result;
  };
   
  typedef size_t Index;
  
  template< class U>
  static
  void resize( Vector< U>& A, size_t n){
    A.resize( n);
  }

  template< class U>
  static
  U element( const Vector< U>& A, size_t i){
    return A[i];
  }

  template< class U>
  static
  U& element_ref( Vector< U>& A, size_t i){
    return A[i];
  }

  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }

  template< class U>
  static
  U transpose( U u){
    return u;
  }
  
  template< class U>
  static
  bool is_negative( U u){
    return u < U{ 0.0};
  }

  template< class U>
  static
  bool is_zero( U u){
    return u == U{ 0.0};
  }
};

  
}}
