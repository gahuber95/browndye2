#include "system_mobility_interface.hh"
#include "../molsystem/system_state.hh"

namespace Browndye{
  
Sys_Mob_Div_Index System_Mobility_Interface::n_divisions( const System& sys){
  return sys.mobility_info.divisions.size();
}


//############################################################################################
template< class FTet, class FChain>
auto System_Mobility_Interface::bead_quantity( FTet& ftet0, FChain& fchain0,
                                              const System& sys, Div_Index imc, Atom_Index ia){
  
 
  auto& state = sys.state;
  auto [ig, id] = sys.mobility_info.divisions[ imc];
    
  auto& group = state.groups[ ig];
  assert( group.cons_info_and_doer);
  
  auto itc = group.cons_info_and_doer->cinfo.divs[ id];
  
  typedef decltype( ftet0( group, Tet_Index(), ia)) Quantity;
   
  auto ftet = [&]( Tet_Index it) -> Quantity{
    return ftet0( group, it, ia);
  };
  
  auto fchain = [&]( Chain_Index ic) -> Quantity{
    return fchain0( group, ic, ia);
  };
  
  return std::visit( overload( ftet, fchain), itc);
}
   
//########################################################################## 
Length System_Mobility_Interface::bead_radius( const System& sys,
                                              Div_Index mci, Atom_Index bi){
  
  auto radius_tet = [&]( const Group_State& group, Tet_Index ti, Atom_Index ai) -> Length{
    return group.tets[ti].fixed->radius;
  };
  
  auto radius_chain = [&]( const Group_State& group, Chain_Index ci, Atom_Index ai) -> Length{
    return group.chain_states[ci].common().atoms[ ai].radius;
  };
  
  return bead_quantity( radius_tet, radius_chain, sys, mci, bi);
}

//####################################################################
Pos System_Mobility_Interface::bead_center( const System& sys,
                                             Div_Index mci, Atom_Index bi){
  
  auto pos_tet = [&]( const Group_State& group, Tet_Index ti, Atom_Index ai) -> Pos{
    return group.tets[ti].poses[ ai];
  };
  
  auto pos_chain = [&]( const Group_State& group, Chain_Index ci, Atom_Index ai) -> Pos{
    return group.chain_states[ci].atoms[ai].pos;
  };
  
  return bead_quantity( pos_tet, pos_chain, sys, mci, bi);
}

//#############################################################################################
auto System_Mobility_Interface::n_beads( const System& sys, Sys_Mob_Div_Index mci) -> Atom_Index{
    
  auto num_tet = [&]( const Group_State& group, Tet_Index, Atom_Index) -> Atom_Index{
    return Atom_Index(4);
  };
  
  auto num_chain = [&]( const Group_State& group, Chain_Index ci, Atom_Index) -> Atom_Index{
    return Atom_Index( group.chain_states[ci].atoms.size()/Atom_Index(1));
  };
  
  return bead_quantity( num_tet, num_chain, sys, mci, Atom_Index(1));
}
  
//###########################################################################
Pos System_Mobility_Interface::div_center( const System& sys, Sys_Mob_Div_Index mci){
  
  auto center_tet = [&]( const Group_State& group, Tet_Index ti, Atom_Index) -> Pos{
    
    auto& poses = group.tets[ti].poses;
    Pos sum( Zeroi{});
    for( auto pos: poses){
      sum += pos;
    }
    return 0.25*sum;
  };
  
  auto center_chain = [&]( const Group_State& group, Chain_Index ci, Atom_Index) -> Pos{ 
    
    auto& cstate = group.chain_states[ ci];
    auto& satoms = cstate.atoms;
    auto& atoms = cstate.common().atoms;
    auto na = atoms.size();
    
    Vec3< Length2> sum( Zeroi{});
    Length sumr( 0.0);
    for( auto ia: range( na)){
      auto r = atoms[ia].radius;
      sum += r*satoms[ia].pos;
      sumr += r;
    }

    return (1.0/sumr)*sum;
  };
  
  return bead_quantity( center_tet, center_chain, sys, mci, Atom_Index(1));
}

//#####################################################################################################
void System_Mobility_Interface::get_centers( const System& sys, Vector< Pos, Sys_Mob_Div_Index>& poses){
  
  auto mdn = poses.size();
  for( auto mdi: range( mdn)){
    poses[mdi] = div_center( sys, mdi);
  }
}
  
//###########################################################################################
void System_Mobility_Interface::get_mobilities( const System& sys, Vector< Vector< Inv_Length, Atom_Index>,
                                               Sys_Mob_Div_Index>& mobs){
  
  auto mdn = mobs.size();
  for( auto mdi: range( mdn)){
    auto nb = n_beads( sys, mdi);
    auto& bmobs = mobs[mdi];
    for( auto ib: range( nb)){
      bmobs[ib] = 1.0/bead_radius( sys, mdi, ib);
    }
  }  
}

//#####################################################################################
void System_Mobility_Interface::add_quantity( const System& sys, System_State& state,
                                               Sys_Mob_Div_Index mci, Atom_Index ia,
                                                 Pos dx){
    
  auto [ig, id] = sys.mobility_info.divisions[ mci];
  
  auto& group = state.groups[ ig];
  auto tci = group.cons_info_and_doer->cinfo.divs[ id];
  
  auto ftet = [&]( Tet_Index it) -> Pos&{
    auto& tet = group.tets[it];
    return tet.poses[ia];
  };
  
  auto fchain = [&]( Chain_Index ic) -> Pos&{
    return group.chain_states[ic].atoms[ia].pos;
  };
  
  auto& pos = std::visit( overload( ftet, fchain), tci);  
  pos += dx;
}

//#######################################################################
Vec3< Length2> System_Mobility_Interface::quantity( const System& sys, Force_Tag,
                                                 Sys_Mob_Div_Index mci, Atom_Index bi){
  
  const auto factor = sys.force_factor;
  
  typedef Vec3< Length2> LF3;
  
  auto force_tet = [&]( const Group_State& group, Tet_Index ti, Atom_Index ai) -> LF3{
    return factor*(group.tets[ti].forces[ai]);
  };
  
  auto force_chain = [&]( const Group_State& group, Chain_Index ci, Atom_Index ai) -> LF3{
    return factor*( group.chain_states[ci].atoms[ai].force);
  };
  
  return bead_quantity( force_tet, force_chain, sys, mci, bi);  
}

//#############################################################
Vec3< Length1p5> System_Mobility_Interface::quantity( const System& sys,
                                                     const Wiener_Vector& ws,
                                                   Sys_Mob_Div_Index imc, Atom_Index ia){
  
  const auto factor = sys.mobility_info.wiener_factor;
  auto [ig,id] = sys.mobility_info.divisions[ imc];
  return factor*ws[ig][id].fine_dW[ia];  
}

//#############################################################
Vec3< Length1p5>
System_Mobility_Interface::div_quantity( const System& sys,
                                         const Wiener_Vector& ws,
                                         Sys_Mob_Div_Index imc){
  
  const auto factor = sys.mobility_info.wiener_factor;
  auto [ig,id] = sys.mobility_info.divisions[ imc];
  return factor*ws[ig][id].coarse_dW;  
}

//###################################################
// constructor
Mobility_Info::Mobility_Info( const System_State& state):

  wiener_factor( new_wiener_factor( state)){
  
  auto ng = state.groups.size();
 
  for( auto gi: range( ng)){
    auto& group = state.groups[gi];
    auto& divs = group.cons_info_and_doer->cinfo.divs;
    
    auto nd = divs.size();
    for( auto id: range(nd)){
      
      auto div = divs[id];
      
      bool add = true;
      if (std::holds_alternative< Chain_Index>( div)){
        auto ic = std::get< Chain_Index>( div);
        if (group.chain_states[ic].atoms.empty()){
          add = false;
        }
      }
      
      if (add){
        divisions.push_back( std::make_pair( gi, id));
      }
    }
  }  
}

//##########################################################
auto Mobility_Info::new_wiener_factor( const System_State& state) -> WF{
  
  const auto& solvent = state.common().solvent;
  auto dfactor = solvent.diffusivity_factor();
  return sqrt( 2.0*dfactor);
}
 
//###############################################
auto Mob_Info_And_State::
  new_force_factor( const System_State& state, Time dt) -> FF{
  
  auto mu = state.common().viscosity();
  return dt/mu;
}
  
 
}
