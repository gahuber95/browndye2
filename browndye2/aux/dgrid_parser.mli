(* used internally. Parses data from the "distance grid" output of
 "grid_distances".
*)

type data3d =
    (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array3.t

type t = {
  data: data3d; (* grid of distances *)
  spacing: float*float*float;    (* spacing between grid points *)
  corner: Vec3.t;                (* position of lowest grid point *)
}


(* grid information from input buffer *)
val grid: Scanf.Scanning.scanbuf -> t

