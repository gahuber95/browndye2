#pragma once

#include <map>
#include <algorithm>
#include "../../global/indices.hh"
#include "../../lib/vector.hh"
#include "../../lib/array.hh"
#include "../../lib/units.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../global/limits.hh"
#include "../../structures/atom.hh"
#include "parameter_info.hh"

namespace Browndye{

class Nonbonded_Parameter_Info: public Parameter_Info{
public:
  [[nodiscard]] std::optional< Atom_Type_Index> index( const std::string& residue, const std::string& atom) const;
  
  typedef Vector< Atom, Atom_Index> Atoms; 
  [[nodiscard]] Atoms atoms_from_file( const std::string& file) const;

  virtual ~Nonbonded_Parameter_Info();

protected:
  typedef const std::string cstring;
  typedef std::pair< cstring, const cstring> Key;
  typedef std::map< Key, Atom_Type_Index> Dict;
  Dict dict; // (residue, atom) to atom-type
  typedef std::pair< cstring, std::variant< cstring, Atom_Index> > SE_Key;
  std::map< SE_Key, Energy> surface_energies; // for ABSINTH
    
  virtual void establish_interaction_radii( Vector< Atom, Atom_Index>&) const = 0;
  
  Atoms atoms_from_xml_gen( JAM_XML_Pull_Parser::Parser&) const;
};

}
