#pragma once

/*
 * rxn_interface.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "../global/indices.hh"
#include "../lib/units.hh"
#include "../lib/vector.hh"
#include "../structures/atom.hh"

// Interface for "pathways.hh"

namespace Browndye{

class System_State;
class System_Common;

class Rxn_Interface{
public:
  typedef Browndye::System_State Molecule_Set;
  typedef Browndye::System_Common Molecule_Info;
  typedef ::Length Length;
  
  typedef Browndye::Atom_Index Atom_Index;
  typedef Browndye::Cor_Cha_Index Mol_Index;
  typedef Browndye::State_Index State_Index;
  typedef Browndye::Rxn_Index Rxn_Index;

  static
  Length distance( const System_State& mpair, Cor_Cha_Index im0, Atom_Index ia0, Cor_Cha_Index im1, Atom_Index ia1);
  
  static
  Cor_Cha_Index molecule_index( const System_Common&, const Vector< std::string>&);

};
}

