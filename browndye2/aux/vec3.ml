(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Used internally.
Data structure used for 3-D vectors.
*)

type t = {
  mutable x: float;
  mutable y: float;
  mutable z: float;
}


let v3 x y z = {
  x = x;
  y = y;
  z = z;
}

let vscaled c a = 
  {
    x = c*.a.x;
    y = c*.a.y;
    z = c*.a.z;
  }

 
let vdiff a b =  
  {
    x = a.x -. b.x;
    y = a.y -. b.y;
    z = a.z -. b.z;
  }


let vsum a b =  
  {
    x = a.x +. b.x;
    y = a.y +. b.y;
    z = a.z +. b.z;
  }


let vnorm a = 
  sqrt (a.x*.a.x +. a.y*.a.y +. a.z*.a.z)

let distance a b = vnorm (vdiff a b)

let sq x = x*.x

let distance2 a b = 
  (sq (a.x -. b.x)) +. (sq (a.y -. b.y)) +. (sq (a.z -. b.z)) 

let normed_diff a b = 
  let diff = vdiff a b in
  let nrm = vnorm diff in
    vscaled (1.0/.nrm) diff


let normed a = 
  let nrm = vnorm a in
    vscaled (1.0/.nrm) a


let dot a b = 
    a.x*.b.x +. a.y*.b.y +. a.z*.b.z

	   
let cross a b = {
  x = (a.y*.b.z -. a.z*.b.y);
  y = (a.z*.b.x -. a.x*.b.z);
  z = (a.x*.b.y -. a.y*.b.x);
}

