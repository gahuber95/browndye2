#include <set>
#include "xml/jam_xml_pull_parser.hh"
#include "xml/node_info.hh""

// Atoms are in an order
// An atom can be constrained by only atoms that precede it
// An atom can be constrained by no more than 3 atoms
// An atom can be constrained by no more than 2 atoms in the same flat

namespace{
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
  using std::cout;
  using std::endl;
  using std::map;
  using std::set;
  using std::pair;
  using std::move;
  using std::make_pair;
  
struct NString{
  string data;
  bool is_next = false;
  
  bool operator<( const NString& other) const{
    return cmp() < other.cmp();
  }
  
  std::tuple< size_t, bool> cmp() const {
    return std::make_tuple( std::hash< string>{}( data), is_next);
  }
  
};
  
//#######################################################
/*
string rep( const NString& s){
   if (s.is_next){
     return s.data + '+';
   }
   else{
     return s.data;
   }
 }
*/  
  
template< class T>
using smap = map< NString, T>;
  
typedef smap< set< NString> > Neb_Map;    
  
struct LConstraint{
  Array< NString, 2> data;
  bool is_improper = false;
  
  bool operator<( const LConstraint& other) const{
     return cmp() < other.cmp();
   }
   
  std::tuple< size_t, size_t, bool> cmp() const {
    auto h0 = std::hash< string>{}( data[0].data);
    auto h1 = std::hash< string>{}( data[1].data);
    
    return std::make_tuple( h0,h1,is_improper);
  }
  
};
  
struct Template_Info{
  string residue;
  Vector< NString> atoms;
  Neb_Map excluded;
  set< LConstraint> length_constraints;
  set< Array< NString, 4> > coplanar_constraints;
}; 

template< class T>
Vector< T> vector_of_list( const std::list< T>& lst){
  
  Vector< T> res;
  for( auto& t: lst){
    res.push_back( t);
  }
  return res;
}

template< class T>
Vector< T> vector_of_moved_list( std::list< T>&& lst){
  
  Vector< T> res;
  for( auto& t: lst){
    res.push_back( std::move(t));
  }
  return res;
}

//###############################################
/*
void print( const Template_Info& tinfo){
  
  cout << "residue " << tinfo.residue << endl;
  cout << "coplanar\n";
  for( auto& cops: tinfo.coplanar_constraints){
    for( auto& cop: cops){
      cout << cop.data << ' ';
    }
    cout << endl;
  }
  cout << endl;
  
  cout << "length\n";
  for( auto& lens: tinfo.length_constraints){
    for( auto& len: lens){
      cout << len.data << ' ';
    }
    cout << '\n';
  }
  cout << endl;
  
  cout << "excluded\n";
  for( auto& item: tinfo.excluded){
    cout << item.first.data << "; ";
    auto& nebs = item.second;
    for( auto& neb: nebs){
      cout << neb.data << ' ';
    }
    cout << endl;
  }
}
*/

//#######################################################################################  
smap< size_t> atom_orders_of( JP::Node_Ptr rnode, const Vector< NString>& atoms){
  
  smap< size_t> atom_orders;
  size_t ia = 0;
  for( auto& atom: atoms){
    //atom_orders.insert( std::make_pair( atom, ia));
    atom_orders[atom] = ia;
    ++ia;
  }
  return atom_orders;
}
  
//#####################################################
NString nstring_of( const string& name){
  if (name.back() == '+'){
    auto n = name.size();
    return NString{ name.substr( 0, n-1), true};
  }
  else{
    return NString{ name, false};
  }
}

//#######################################################################################
Neb_Map nebs_of( const Vector< JP::Node_Ptr>& anodes){
  
  Neb_Map nebs0;
  for( auto anode: anodes){
    auto atom = nstring_of( checked_value_from_node< string>( anode, "name"));
    auto nebs = checked_vector_from_node< string>( anode, "nebs");
    auto nnebs = nebs.mapped( nstring_of);
    for( auto& neb: nnebs){
      nebs0[ atom].insert( neb);
    }
  }
  return nebs0;
}
 //#########################################################################################################
  template< class Key, class Val>
  std::optional< Val> found( const map< Key, Val>& dict, const Key& x){
    
    auto itr = dict.find( x);
    if (itr == dict.end()){
      return std::nullopt;
    }
    else{
      return itr->second;
    }
  }
  
  
  //#######################################################################################
  // near nebs in both directions
  Neb_Map both_nebs( const smap< size_t>& orders, const Neb_Map& nebs0){
    
    Neb_Map nnebs;
    for( auto& item: orders){
      auto& atom = item.first;
      auto nebso = found( nebs0, atom);
      if (nebso){
        auto& nebs = nebso.value();
        for( auto& neb: nebs){
          nnebs[atom].insert( neb);
          nnebs[neb].insert( atom);
        }
      }
    }
    return nnebs;
  }
  
 
  //#######################################################################
  Neb_Map far_nebs( const smap< size_t>& orders, const Neb_Map& nebs0){
    
    auto nnebs = both_nebs( orders, nebs0);
    
    Neb_Map afnebs;
    for( auto& item: nebs0){
      auto& atom = item.first;
      auto ia = orders.at( atom);
      auto& nebs = item.second;
      for( auto& neb: nebs){
        if (orders.at( neb) < ia){
          afnebs[ atom].insert( neb);
        }
        auto xnebso = found( nnebs, neb);
        if (xnebso){
          auto& xnebs = xnebso.value();
          for( auto& xneb: xnebs){
            auto ic = orders.at( xneb);
            if (ic < ia){
              afnebs[ atom].insert( xneb);
            } 
          }            
        }
      }
    }
    
    return afnebs;
  }
  
  //############################################################################
  Vector< Array< NString, 4> > coplanar_constraints_of( const set< NString>& flat){
  
    auto n = flat.size();
    Vector< NString> flatv;
    flatv.reserve( n);
    for( auto& name: flat){
      flatv.push_back( name);
    }
    
    Vector< Array< NString, 4> > coplanar_constraints;
    for( auto i: range( 3, n)){
      auto& coplanar = coplanar_constraints.emplace_back();
      size_t j = 0;
      for( auto k: range(i-3, i+1)){
        coplanar[j] = flatv[k];
        ++j;
      }
    }
    
    return coplanar_constraints;
  }
  
  //##################################################################
  template< class T>
  bool inside( const set< T>& stuff, const T& t){
    return stuff.find( t) != stuff.end();
  }
    
  //#####################################################
  Vector< NString> atoms_of( JP::Node_Ptr rnode){
    
    auto anodes = vector_of_moved_list( rnode->children_of_tag( "atom"));
    
    auto atoms1 = anodes.mapped( [&]( JP::Node_Ptr anode){
      auto name = checked_value_from_node< string>( anode, "name");
      return nstring_of( name);
    });
    
    auto atom0 = checked_value_from_node< string>( rnode, "first");
     
    return initialized_vector( atoms1.size() + 1,
                                    [&]( size_t i){
      if (i == 0){
        return nstring_of( atom0);
      }
      else{
        return atoms1[i-1];
      }
    });
  }
  
  //####################################################################
  Vector< set< NString> > flat_sets_of( JP::Node_Ptr rnode){
  
    auto fnodes = vector_of_moved_list( rnode->children_of_tag( "flat"));
    return fnodes.mapped( [&]( JP::Node_Ptr fnode){
      auto names = vector_from_node< string>( fnode);
      auto nnames = names.mapped( nstring_of);
      std::set< NString> fset;
      fset.insert( nnames.begin(), nnames.end());
      return fset;
    });
  }
  
  //####################################################################
  struct Atom_Info{
     NString name;
     int order;
     std::optional< int> iflat;
   };
  
  smap< Atom_Info> atom_infos( const Vector< NString>& atoms, const smap< size_t>& orders,
                                      const Vector< set< NString> >& flat_sets){
    smap< Atom_Info> ainfos;
    for( auto& atom: atoms){
      Atom_Info ainfo; 
      ainfo.name = atom;
      ainfo.order = orders.at( atom);
      auto nf = flat_sets.size();
      for( auto i: range( nf)){
        auto& flat_set = flat_sets[i];
        if (inside( flat_set, atom)){
          ainfo.iflat = i;
          break;
        }
      }
      ainfos[ atom] = ainfo;
    }
    
    return ainfos;
  }

  //##############################################################################  
  Neb_Map nored_nebs_of( const smap< Atom_Info>& ainfos, const Neb_Map& fnebs){
    
    smap< int> ncons, nfcons;
       
    for( auto ainfo: ainfos){
      auto& atom = ainfo.first;
      ncons[atom] = 0;
      nfcons[atom] = 0;
    }
    
    Neb_Map nored_nebs;
    for( auto& item: fnebs){
      auto& atom = item.first;
      auto iflato = ainfos.at( atom).iflat;
      auto& nebs = item.second;
      for( auto& neb: nebs){
        if (ncons[ atom] == 3){
          break;
        }
        else{
          bool add = true;
          auto jflato = ainfos.at( neb).iflat;
          if (iflato && jflato){
            auto iflat = iflato.value();
            auto jflat = jflato.value();
            if (iflat == jflat){
              if (nfcons[atom] == 2){
                add = false;
              }
              else{
                ++nfcons[ atom];
              }
            }  
          }
          
          if (add){
            nored_nebs[ atom].insert( neb);
            ++ncons[ atom];
          }
        }
      }
    }
    return nored_nebs;
  }
  

  //##############################################################################
  set< LConstraint> length_constraints_of( const Neb_Map& nnebs, const Neb_Map& fnebs){
  
    set< LConstraint> length_constraints;
    for( auto& item: fnebs){
      auto& atom = item.first;
      auto& nebs = item.second;
      for( auto& neb: nebs){
        if (!(atom.is_next && neb.is_next)){
          LConstraint lcons;
          lcons.data = Array< NString, 2>{ atom, neb};
          
          if (!inside( nnebs.at( atom), neb)){
            lcons.is_improper = true;
          }
          
          length_constraints.insert( lcons);
        }
      }
    }
    return length_constraints;
  }
  
  //###############################################################
  typedef set< Array< NString, 4> > Coplanar_Constraints;
  
  Coplanar_Constraints all_coplanar_constraints_of( const Vector< set< NString> >& flat_sets){
       
    return flat_sets.folded_left(
    [&]( const Coplanar_Constraints& res, const set< NString>& flat){
      auto sub = coplanar_constraints_of( flat);
      Coplanar_Constraints new_res;
      new_res.insert( res.begin(), res.end());
      new_res.insert( sub.begin(), sub.end());
      return new_res;
    },
    Coplanar_Constraints{});
  }
  
  //########################################################################
  Template_Info constraints( JP::Node_Ptr rnode){

    auto rname = checked_value_from_node< string>( rnode, "name");

    auto anodes = vector_of_moved_list( rnode->children_of_tag( "atom"));  
    auto atoms = atoms_of( rnode);
    auto orders = atom_orders_of( rnode, atoms);  
    auto nebs = nebs_of( anodes);
    auto fnebs = far_nebs( orders, nebs);
              
    auto flat_sets = flat_sets_of( rnode);
            
    auto ainfos = atom_infos( atoms, orders, flat_sets);
    //auto nored_nebs = nored_nebs_of( ainfos, fnebs);
        
    auto length_cons = length_constraints_of( nebs, fnebs);
    auto coplanar_cons = all_coplanar_constraints_of( flat_sets);
    
    Template_Info tinfo;
    tinfo.residue = rname;
    tinfo.atoms = std::move( atoms);
    tinfo.length_constraints = std::move( length_cons);
    tinfo.coplanar_constraints = std::move( coplanar_cons);
    tinfo.excluded = std::move( fnebs);
    
    return tinfo;
  }
  
  
  //#########################################################
  map< string, Template_Info> constraint_dict(){
    
    string file( "template.xml");
    std::ifstream input( file);
    JP::Parser parser( input, file);
    auto top = parser.top();
    top->complete();
    
    map< string, Template_Info> dict;
    auto rnodes = top->children_of_tag( "residue");
    for( auto rnode: rnodes){
      auto name = checked_value_from_node< string>( rnode, "name");
      dict[ name] = constraints( rnode);
    }
    return dict;
  }
  
  
  //#########################################################################
  struct Residue{
    string residue;
    size_t ir;
  };
  
  struct Atom{
    string residue, name;
    size_t ir, ia;
    Array< double, 3> pos;
  };
  
  
  pair< Vector< Residue>, Vector< Atom> > atoms_reses_of_file( string file){
    
    //MM_Nonbonded_Parameter_Info info;
    //return info.atoms_from_file( file);
    
    std::ifstream input( file);
    JP::Parser parser( input, file);
    
    auto top = parser.top();
    top->complete();
    
    auto rnodes = top->children_of_tag( "residue");
    Vector< Atom> atoms;
    Vector< Residue> reses;
    for( auto rnode: rnodes){
      auto rname = checked_value_from_node< string>( rnode, "name");
      auto ir = checked_value_from_node< size_t>( rnode, "number");
      Residue resi;
      resi.residue = rname;
      resi.ir = ir;
      reses.push_back( resi);
      
      auto anodes = rnode->children_of_tag( "atom");
      for( auto anode: anodes){
        auto aname = checked_value_from_node< string>( anode, "name");
        auto ia = checked_value_from_node< size_t>( anode, "number");
        auto x = checked_value_from_node< double>( anode, "x");
        auto y = checked_value_from_node< double>( anode, "y");
        auto z = checked_value_from_node< double>( anode, "z");
        Array< double, 3> pos{ x,y,z};
        
        Atom atom;
        atom.residue = rname;
        atom.name = aname;
        atom.ir = ir;
        atom.ia = ia;
        atom.pos = pos;
        atoms.push_back( atom);
      }
    }
    
    return make_pair( move( reses), move( atoms));
  }
  
  //##########################################################################
  struct Atom_Ref{
    string atom;
    size_t ir;
    
    auto tupled() const{
      return make_tuple( std::cref( atom), ir);
    }
    
    bool operator<( const Atom_Ref& other) const{
      auto tp = tupled();
      auto otp = other.tupled();
      return tp < otp;
    }
  };
  
  void main0(){
    string file( "template.xml");
    std::ifstream input( file);
    JP::Parser parser( input, file);
    auto top = parser.top();
    top->complete();
    
    smap< Vector< Array< string, 2> > > dict;
    smap< Neb_Map > edict;
    auto rnodes = top->children_of_tag( "residue");
    for( auto rnode: rnodes){
      //auto name = checked_value_from_node< string>( rnode, "name");
      auto tinfo = constraints( rnode);
      cout << endl;
      //print( tinfo);
    }
  }
  
  //#################################################
  /*
  auto trunced( const string& name) -> string{
    if (name.back() == '+'){
      auto n = name.size();
      return name.substr( 0, n-1);
    }
    else{
      return name;
    }
  } 
  */
  //#################################################
  auto atom_ref_of( const NString& aname, size_t ir) -> Atom_Ref {
    
    Atom_Ref ref;
    ref.atom = aname.data;
    ref.ir = ir + (aname.is_next ? 1 : 0);
    return ref;
  }
  
  
  struct LCon{
    size_t i0, i1;
    double r;
    bool is_improper = false;
    
    bool operator<( const LCon& other) const{
      return r < other.r;
    }
    
    bool operator==( const LCon& other) const {
      return (i0 == other.i0) && (i1 == other.i1) && (r == other.r);
    }
    
  };
  
  Vector< set< size_t> > actual_flats( const Vector< Array< size_t, 4> >& cconstraints);
  
  //#################################################################################################
  void reduce_lconstraints( const Vector< set< size_t> >& aflats, size_t ia0, Vector< LCon>& lcons){
    // get down to three per atom, shortest constraints preferred
    if (lcons.size() > 3){
      auto lb = lcons.begin();
      auto le = lcons.end();
      std::sort( lb, le);
      lcons.erase( lb+3, le);
    }
      
    // get down to two per atom in same flat
    if (lcons.size() > 2){
      for( auto& aflat: aflats){
        if (inside( aflat, ia0)){
          
          Vector< LCon> lfcons;
          for( auto& lcon: lcons){
            auto ia1 = lcon.i1;
            if (inside( aflat, ia1)){
              lfcons.push_back( lcon);
            }
          }
          if (lfcons.size() > 2){
            auto& lfcon = lfcons.front();
            auto itr = std::find( lcons.begin(), lcons.end(), lfcon);
            if (itr == lcons.end()){
              error( "not get here");
            }
            lcons.erase( itr);
            break;
          }
        }
      }
    }
  }
 
  //#################################################################################################
  void print_lconstraints( const Vector< Atom>& atoms, const Atom& atom0, const map< size_t, size_t>& atom_dict, const Vector< LCon>& lcons){
    for( auto& lcon: lcons){
      auto ia0 = lcon.i0;
      auto ia1 = lcon.i1;
      auto r = lcon.r;
      auto pos0 = atom0.pos;
      auto ir0 = atom0.ir;
      
      auto ka1 = atom_dict.at( ia1);
      auto& atom1 = atoms[ka1];
      auto pos1 = atom1.pos;
      auto& aname0 = atom0.name;
      auto& aname1 = atom1.name;
      auto ir1 = atom1.ir;
      
      cout << "    <length_constraint";
      if (lcon.is_improper){
        cout << " improper=\"true\" ";
      }
      cout << ">\n";
      cout << "      <atoms> " << ia0 << ' ' << ia1 << " </atoms>\n";
      cout << "      <length> " << r << " </length>\n";
      cout << "      <c> " << atom0.residue << ' ' << ir0 << ' ' << aname0 << ' ' << ir1 << ' ' << aname1 << " </c>" << endl;
      cout << "      <pos0> " << pos0 << " </pos0>\n";
      cout << "      <pos1> " << pos1 << " </pos1>\n";              
      cout << "    </length_constraint>\n";
    }
  }
  
  //#################################################################################################
  void output_length_constraints( const Vector< Atom>& atoms, const map< string, Template_Info>& dict,
                                   const map< Atom_Ref, size_t>& atom_ref_dict, const Vector< Array< size_t, 4> >& cop_cons){
    
    map< size_t, size_t> atom_dict;
    for( auto ka: range( atoms.size())){
      auto ia = atoms[ka].ia;
      atom_dict[ ia] = ka;
    }
    
    auto aflats = actual_flats( cop_cons);
    
    cout << "  <length_constraints>\n";   
    for( auto& atom0: atoms){
      auto pos0 = atom0.pos;
      auto& itemp = dict.at( atom0.residue);
      auto& constraints = itemp.length_constraints; 
  
      auto ia0 = atom0.ia;
      auto ir0 = atom0.ir;
      
      Vector< LCon> lcons;
      
      for( auto& con: constraints){
        auto& aname0 = con.data[0];
        if (aname0.data == atom0.name){            
          auto& aname1 = con.data[1];
          auto ir = ir0 - (aname0.is_next ? 1 : 0);
          auto ref1 = atom_ref_of( aname1, ir);
          auto ia1o = found( atom_ref_dict, ref1);
          if (ia1o){
            auto ia1 = ia1o.value();
            auto ka1 = atom_dict.at( ia1);
            auto& atom1 = atoms[ka1];
            auto pos1 = atom1.pos;
            auto r = distance( pos1, pos0);
            LCon lcon{ ia0, ia1, r};
            lcon.is_improper = con.is_improper;
            lcons.push_back( lcon);
          }
        }
      }
      
      reduce_lconstraints( aflats, ia0, lcons);
      print_lconstraints( atoms, atom0, atom_dict, lcons);
    }
    cout << "  </length_constraints>\n";
    
  }
  
  //#######################################################
  bool shared_atom( const set< size_t>& ca, Array< size_t, 4> cb){
    
    bool res = false;
    int ns = 0;
    for( auto ib: range( 4)){
      if (inside( ca, cb[ib])){
        ++ns;
        if (ns == 2){
          res = true;
          break;
        }
      }
    }
    
    return res;
  }
  
  //###################################################################################
  Vector< set< size_t> > actual_flats( const Vector< Array< size_t, 4> >& cconstraints){
    
    Vector< set< size_t> > res;
    auto n = cconstraints.size();
    Vector< bool> gotten( n, false);
    
    for( auto i: range(n)){
      if (!gotten[i]){
        set< size_t> flat;
        auto& consi = cconstraints[i];
        for( auto ia: consi){
          flat.insert( ia);
        }
        
        bool changed;
        do{
          changed = false;  
        
          for( auto j: range(i+1,n)){
            auto consj = cconstraints[j];
            if (!gotten[j]){
              if (shared_atom( flat, consj)){
                for( auto ja: consj){
                  flat.insert( ja);
                }
                changed = true;
                gotten[j] = true;
              }
            }
          }
        }
        while( changed);
        
        res.push_back( move( flat));
      }
    }
    
    return res;
  }
  
  //#######################################################
  Vector< Array< size_t, 4> > actual_coplanar_constraints( const Vector< Atom>& atoms, const map< string, Template_Info>& dict,
                                   const map< Atom_Ref, size_t>& atom_ref_dict){
    
    map< size_t, size_t> atom_dict;
    for( auto ka: range( atoms.size())){
      auto ia = atoms[ka].ia;
      atom_dict[ ia] = ka;
    }
  
    Vector< Array< size_t, 4> > res;  
    
    //cout << "  <coplanar_constraints>\n";   
    for( auto& atom: atoms){
      auto& itemp = dict.at( atom.residue);
      auto& constraints = itemp.coplanar_constraints; 
  
      auto ir0 = atom.ir;
      for( auto& con: constraints){
        auto& aname0 = con[0];
        if (aname0.data == atom.name){           
        
          auto ir = ir0 - (aname0.is_next ? 1 : 0);
          
          auto atom_number = [&]( const NString& aname) -> std::optional< size_t>{
            auto ref = atom_ref_of( aname, ir);
            return found( atom_ref_dict, ref);
          };
          
          auto ia0 = atom_number( aname0);
          auto ia1 = atom_number( con[1]);
          auto ia2 = atom_number( con[2]);
          auto ia3 = atom_number( con[3]);
          
          if (ia0 && ia1 && ia2 && ia3){
            /*
            cout << "    <coplanar_constraint>\n";
            cout << "      <atoms> " << ia0.value() << ' ' << ia1.value() << ' ' << ia2.value() << ' ' << ia3.value() << " </atoms>\n";
            cout << "      <c> " << ir << ' ' << rep( aname0)
                  << ' ' << rep( con[1]) << ' ' << rep( con[2]) << ' ' << rep( con[3]) << " </c>" << endl;
            cout << "    </coplanar_constraint>\n";
            */
            res.emplace_back( Array< size_t, 4>{ ia0.value(), ia1.value(), ia2.value(), ia3.value()});
          }
        }
      }
    }
    //cout << "  </coplanar_constraints>\n";
    return res;
  }
  //#############################################################################
  void print_coplanar_constraints( const Vector< Array< size_t, 4> >& cconstraints){
    
    cout << "  <coplanar_constraints>\n"; 
    for( auto& con: cconstraints){
      cout << "    <coplanar_constraint>\n";
      cout << "      <atoms> ";
      for( auto k: range( 4)){
        cout << con[k] << ' ';
      }
      cout << " </atoms>\n";    
      cout << "    </coplanar_constraint>\n";
    }
    cout << "  </coplanar_constraints>\n"; 
  }
  
  //#############################################################################
  map< size_t, set< size_t> > excluded_atoms_of( const Vector< Atom>& atoms, const map< string, Template_Info>& dict,
                               const map< Atom_Ref, size_t>& atom_dict){
     
    map< size_t, set< size_t> > excluded_atoms;
    
    for( auto& atom0: atoms){
      auto ir = atom0.ir;
      auto& tinfo = dict.at( atom0.residue);
      auto& exsr = tinfo.excluded;
      auto aname0 = NString{ atom0.name, false};
      auto aname0p = NString{ atom0.name, true};
      
      auto process = [&]( const NString& aname0){
        auto exs0o = found( exsr, aname0);
        if (exs0o){
          auto& exs0 = exs0o.value();
          Atom_Ref ref0;
          ref0.atom = aname0.data;
          ref0.ir = ir + (aname0.is_next ? 1 : 0);
          
          auto ia0o = found( atom_dict, ref0);
          if (ia0o){
            auto ia0 = ia0o.value();
          
            for( auto& aname1: exs0){
              auto dir1 = aname1.is_next ? 1 : 0;
              
              Atom_Ref ref1;
              ref1.atom = aname1.data;
              ref1.ir = ir + dir1;
              auto ia1 = atom_dict.at( ref1);
              
              excluded_atoms[ia0].insert( ia1);
            }
          }
        }
      };
      
      process( aname0);
      process( aname0p);
    }
    return excluded_atoms;
  }

  //#########################################################################
  void output_excluded_atoms( const map< size_t, set< size_t> >& excluded_atoms){
    
    cout << "  <excluded_atoms>\n";
    for( auto& item: excluded_atoms){
      auto ia = item.first;
      cout << "    <chain_atom>\n";
      cout << "      <atom> " << ia << " </atom>\n";
      auto& nebs = item.second;
      cout << "      <ex_atoms> ";
      for( auto neb: nebs){
        cout << neb << ' ';
      }
      cout << "</ex_atoms>\n";
      cout << "    </chain_atom>\n";
    }
    
    cout << "  </excluded_atoms>\n";
  };
  
  
  //################################################################3
  void main1(){
    
    auto dict = constraint_dict();
    auto [residues,atoms] = atoms_reses_of_file( "ploop337_atoms.xml");
    map< Atom_Ref, size_t> atom_dict;
    for( auto& atom: atoms){
      Atom_Ref ref;
      ref.atom = atom.name;
      ref.ir = atom.ir;
      atom_dict[ ref] = atom.ia;
    }
         
    cout << "<top>\n";
    auto cop_constraints = actual_coplanar_constraints( atoms, dict, atom_dict);
    
    output_length_constraints( atoms, dict, atom_dict, cop_constraints);
    print_coplanar_constraints( cop_constraints);
    
    auto excluded_atoms = excluded_atoms_of( atoms, dict, atom_dict);
    output_excluded_atoms( excluded_atoms);
    
    cout << "</top>\n";   
  }
}

//########################################3
int main(){
  main1();
}
