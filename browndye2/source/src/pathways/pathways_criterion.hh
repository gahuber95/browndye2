#pragma once

/*
w * pathway_criterion.hh
 *
 *  Created on: Oct 15, 2015
 *      Author: root
 */

#include <memory>
#include <map>
#include "../lib/vector.hh"
#include "pathways_rxn_pair.hh"
#include "atom_remap.hh"

namespace Browndye{

namespace Pathways{

  class Cr_Tag{};
  typedef Index< Cr_Tag> Crit_Index;

  template< class I>
  class Criterion{
  public:
    typedef typename I::Mol_Index Mol_Index;
    typedef typename I::Atom_Index Atom_Index;
    typedef typename I::Length Length;
    typedef typename I::Molecule_Info Molecule_Info;
    
    Crit_Index n_needed{ maxi};
    Vector< Rxn_Pair<I>, Crit_Index> pairs;
    Vector< std::unique_ptr< Criterion>, Crit_Index> criteria;

    Criterion( const Molecule_Info&, JP::Node_Ptr, Mol_Index, Mol_Index);
    Criterion() = default;
    Criterion( const Criterion&) = delete;
    Criterion& operator=( const Criterion&) = delete;
    Criterion( Criterion&&) = default;
    Criterion& operator=( Criterion&&) = default;
    ~Criterion() = default;

    void remap( Mol_Index, std::map< Atom_Index, Atom_Index>& imap);
  };
  
  // constructor
  template< class I>
  Criterion<I>::Criterion( const Molecule_Info& minfo, JP::Node_Ptr node, Mol_Index imol0, Mol_Index imol1){
    auto pnodes = node->children_of_tag( "pair");
    auto cnodes = node->children_of_tag( "criterion");

    Mol_Index im0{ imol0}, im1{ imol1};
    
    auto msnode = node->child( "molecules");
    if (msnode){
      auto mnode0 = checked_child( msnode, "molecule0");
      auto mnode1 = checked_child( msnode, "molecule1");
      
      auto names0 = vector_from_node< std::string>( mnode0);
      auto names1 = vector_from_node< std::string>( mnode1);
      
      im0 = I::molecule_index( minfo, names0);
      im1 = I::molecule_index( minfo, names1);
    }
    
    if (im0 == Mol_Index( maxi) || im1 == Mol_Index( maxi)){
      error( "molecules must defined in reaction criteria", node->file_name(), node->line_number());
    }
    for( auto& pnode: pnodes){
      pairs.emplace_back( pnode, im0, im1);
    }

    for( auto& cnode: cnodes){
      criteria.emplace_back( std::make_unique< Criterion>( minfo, cnode, im0, im1));
    }

    n_needed = checked_value_from_node< Crit_Index>( node, "n_needed");
  }
  
  
  template< class I>
  void Criterion<I>::remap( Mol_Index mi, std::map< Atom_Index, Atom_Index>& imap){
  
    for( auto& pair: pairs){
      if (pair.mol0 == mi)
        pair.atom0 = anumber( imap, pair.atom0);
      if (pair.mol1 == mi)
        pair.atom1 = anumber( imap, pair.atom1);
    }
  
    for( auto& ptr: criteria)
      ptr->remap( mi, imap);
  }

}
}
