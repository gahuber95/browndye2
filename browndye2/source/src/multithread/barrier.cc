/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include "barrier.hh"

/*
Implements a multithreaded barrier.  The object is initialized
with argument "n". When a process calls "wait", it is held up
until "n" processes have called wait. Then, they are all released.
*/
namespace Browndye{

void Barrier::initialize( unsigned int nthreads){
  count = nthreads;
  cycle = false;
  threshhold = nthreads;
}

void Barrier::release(){
  cycle = !cycle;
  count = threshhold;
  cond.notify_all();
}

void Barrier::wait(){
  std::unique_lock< std::mutex> lock( mutex);

  bool old_cycle = cycle;
  --count;
  if (count == 0){
    release();
  }
  else{
    while (old_cycle == cycle){
      cond.wait( lock);
    }
  }
}
}