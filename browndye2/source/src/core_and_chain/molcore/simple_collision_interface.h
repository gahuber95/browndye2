#pragma once
//
// Created by ghuber on 9/19/23.
//
#include "../../lib/vector.hh"
#include "../../structures/atom.hh"
#include "../../transforms/transform.hh"
#include "partition_contents_ext.hh"
#include "../collision_interface.hh"
#include "core_common.hh"

// used only for initializing eta_max in cores
namespace Browndye{

  class Simple_Col_Interface{
  public:

    static constexpr bool is_changeable = true;
    static constexpr bool is_chain = false;

    typedef Pos Position;
    typedef Ind_Atom_Index Index;
    typedef ::Length Length;

    typedef Vector< Atom, Atom_Index> Container;

    static
    void does_have_near_interaction( Container&){};

    typedef Atom_Index Ref;
    typedef Vector< Atom_Index, Ind_Atom_Index> Refs;
    typedef Blank_Transform Transform;
    typedef Ind_Atom_Index size_type;

    static
    void copy_references( Container& atoms, Refs& arefs){
      auto n = converted< Ind_Atom_Index >( atoms.size());
      arefs.resize( n);
      for( auto i: range( n)) {
        arefs[i] = converted< Atom_Index>( i);
      }
    }

    static
    void copy_references([[maybe_unused]] const Container& cont, const Refs& arefs0,
                         Refs& arefs1){

      auto n = arefs0.size();
      arefs1.resize( n);
      for( auto i: range( n)) {
        arefs1[i] = arefs0[i];
      }
    }

    static
    size_type size( const Container& atoms){
      return converted< Ind_Atom_Index>( atoms.size());
    }

    static
    size_type size( const Container&, const Refs& refs){
      return refs.size();
    }

    static
    Pos position( const Container& atoms, const Refs& refs, size_type i){
      return atoms[refs[i]].pos;
    }

    static
    Pos transformed_position( Pos pos, Blank_Transform){
      return pos;
    }

    static
    Length radius( const Container& atoms, const Refs& refs, size_type i){
      return atoms[refs[i]].interaction_radius;
    }

    template< class F>
    static
    void partition_contents( const Container& cont, const Refs& refs, const F& f,
                             Refs& refs0, Refs& refs1){
      partition_contents_ext< Simple_Col_Interface>( cont, refs, f, refs0, refs1);
    }

    static
    void empty_container( Container&, Refs& refs){
      refs.clear();
    }

    static
    void swap( Container&, Refs& refs, size_type i, size_type j){
      auto ref = refs[i];
      refs[i] = refs[j];
      refs[j] = ref;
    }

    static
    void clear_transform( Container& cont, Refs& refs){}



    static
    Pos transformed_position( Container& atoms, Blank_Transform, Atom_Index i){

      return atoms[i].pos;
    }

    static
    Length physical_radius( const Container& atoms, Atom_Index i){
      return atoms[i].radius;
    }

    /*
    static
    Length2 sasa( const Container& cont, Atom_Index i){
      auto& atoms = cont.common.atoms;
      return atoms[i].sasa();
    }
     */

    static
    Pos position( const Container& atoms, Atom_Index i){
      return atoms[i].pos;
    }

    /*
    static
    Charge charge([[maybe_unused]] const Container& cont, [[maybe_unused]] Atom_Index i){
      error( __FILE__, __LINE__, "should not get here");
      return Charge( 0.0);
    }
    */

  };
}

