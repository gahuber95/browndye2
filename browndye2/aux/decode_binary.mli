(* Used internally. MIME Base64 encoding of floats. The floats are treated as
32-bit IEEE floats, even on a 64-bit machine *)

val string_of_floats: float array -> bytes
val floats_of_string: string -> float array
