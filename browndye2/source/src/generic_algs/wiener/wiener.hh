#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
The Wiener::Process class implements the back-stepping Wiener process
as described in the paper.

Does not have the sqrt(2) factor in it like before; it strictly represents
the dW in Brownian dynamics.

The Interface class has the following types and static functions:

Vector (must contain sqrt(time), copyable and movable
Gaussian
Time

void set_half( const Vector&, Vector&);
void set_difference( const Vector&, const Vector&, Vector&);
//void set_gaussian( Gaussian&, const Sqrt_Time& scale, Vector&);
void add_gaussian( Gaussian&, const Sqrt_Time& scale, Vector&);

 */

#include <cassert>
#include <stack>
#include <vector>
#include <functional>
#include "../../global/debug.hh"
#include "../../global/limits.hh"
#include "../../global/error_msg.hh"



namespace Browndye::Wiener{

template< class I>
class Process{
public:
  typedef typename I::Vector Vector;
  typedef typename I::Gaussian Gaussian;
  typedef typename I::Time Time;
  //typedef decltype( Time()) Sqrt_Time;

  void split( Gaussian&);  
  void step_forward();
  
  Time time_step() const;
  Time time() const;
  [[nodiscard]] bool at_end() const;
  const Vector& dW() const;
  
  Process() = delete;
  // expect initial Wiener process step to be in w and consistent with dt
  Process( Vector&& w, Time dt);
  Process( const Process&) = delete;
  Process& operator=( const Process&) = delete;
  Process( Process&&) = default;
  Process& operator=( Process&&) = default;
  Process copy() const;
  [[nodiscard]] size_t size() const;

private:
  class Copy_Tag{};
  explicit Process( Copy_Tag){}
  
  struct W_Info{
  
    explicit W_Info( Vector&& _w): w(std::move(_w)){} 
    explicit W_Info( const Vector& _w): w( I::copy(_w)){};
    
    W_Info() = delete;
    W_Info( const W_Info& other): w( I::copy( other.w)), dt( other.dt){}
    
    W_Info& operator=( const W_Info& other){
      w = I::copy( other.w);
      dt = other.dt;
      return *this;
    }
    
    W_Info( W_Info&&) = default;
    W_Info& operator=( W_Info&&) = default;
  
    Vector w;
    Time dt = Time( NaN);
  };
  
  std::stack< W_Info, std::vector< W_Info> > ws;
  Time t{ 0.0};
  
  void check_empty() const; 
};

template< class I>
void Process< I>::check_empty() const{
  if (debug){
    if (ws.empty()){
      error( "Wiener::Process: out of time steps"); 
    }
  }
}

template< class I>
size_t Process< I>::size() const{
  return ws.size();
}

// constructor
template< class I>
Process< I>::Process( Vector&& init_w, Time dt){
  
  ws.emplace( std::move( init_w));
  ws.top().dt = dt;
}

template< class I>
void Process< I>::split( Gaussian& gaussian){
  
  const auto& old_cur = ws.top();
  
  auto new_cur_w = I::copy( old_cur.w);
  auto next_w = I::copy( old_cur.w);
  W_Info new_cur( std::move( new_cur_w)), next( std::move( next_w));
  
  I::set_half( old_cur.w, new_cur.w);
  
  new_cur.dt = old_cur.dt/2.0;
  auto s = sqrt( new_cur.dt/2.0);
  I::add_gaussian( gaussian, s, new_cur.w);
  
  next.dt = new_cur.dt;
  I::set_difference( old_cur.w, new_cur.w, next.w);
  
  ws.pop();
  ws.push( std::move( next));
  ws.push( std::move( new_cur));
}
//#############################################
template< class I>
Process< I> Process< I>::copy() const{
  
  Process< I> other{ Copy_Tag()};
  other.t = t;
  other.ws = ws;
  return other;
}

//#############################################
template< class I>
void Process<I>::step_forward(){
  check_empty();
  
  t += ws.top().dt;
  ws.pop();
}

template< class I>
typename I::Time Process< I>::time_step() const{
  check_empty();
  
  return ws.top().dt;
}

template< class I>
typename I::Time Process< I>::time() const{
  return t;
}

template< class I>
bool Process< I>::at_end() const{
  return ws.empty();
}

template< class I>
const typename I::Vector& 
Process< I>::dW() const{
  check_empty();
  
  return ws.top().w;    
}
  
 
}

