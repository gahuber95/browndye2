#pragma once

// Simple typedef for XML parsers

#include <expat.h>
#include <string>

namespace Browndye{
namespace JAM_XML_Parser{
  typedef std::string String;
}}

