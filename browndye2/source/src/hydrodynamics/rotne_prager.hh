#pragma once

#include "../global/pi.hh"
#include "../lib/array.hh"
#include "../lib/sq.hh"

// answers are scaled by viscosity

namespace Browndye{

namespace Rotne_Prager{
 
  template< class Length>
  auto single_t_mobility( Length a) -> decltype(1.0/a){
    return 1.0/(pi6*a);
  }

  template< class Length>
  auto single_r_mobility( Length a) -> decltype(1.0/(a*a*a)){
    return 1.0/(8.0*pi*a*a*a);
  }

  // from PJ Zuk et al. J. Fluid Mech. (2014), vol. 741, R5, doi:10.1017/jfm.2013.668
  template< class Length>
  auto overlap_tt_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (16.0*r3*(a0 + a1) - sq( sq(a0 - a1) + 3.0*r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  template< class Length>
  auto overlap_tt_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (3.0*sq( sq(a0 - a1) - r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  /*
  template< class Length>
  auto tt_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }

  template< class Length>
  auto tt_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }
  */

  template< class Length>
  auto overlap_rr_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (16.0*r3*(a0 + a1) - sq( sq(a0 - a1) + 3.0*r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  template< class Length>
  auto overlap_rr_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (3.0*sq( sq(a0 - a1) - r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  /*
  template< class Length>
  auto sep_rr_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }

  template< class Length>
  auto sep_rr_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }
  */

  template< class Length>
  auto overlap_tr_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (16.0*r3*(a0 + a1) - sq( sq(a0 - a1) + 3.0*r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  template< class Length>
  auto overlap_tr_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){
    auto r2 = r*r;
    auto r3 = r2*r;
    return (3.0*sq( sq(a0 - a1) - r2))/(6.0*32.0*pi*a0*a1*r3);
  }

  /*
  template< class Length>
  auto sep_tr_uu_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }

  template< class Length>
  auto sep_tr_I_term( Length a0, Length a1, Length r) -> decltype(1.0/r){

  }
  */

  template< class Length>
  auto zz_mobility( Length a0, Length a1, Length r) -> decltype(1.0/(r)){
    if (r > a0 + a1){
      auto asq = 0.5*(a0*a0 + a1*a1);
      return (1.5 - (asq/(r*r)))/(pi6*r);
    }
    else
      return (overlap_tt_I_term( a0,a1,r) + overlap_tt_uu_term( a1,a1,r));
  }

  template< class Length>
  decltype( 1.0/(Length()*Length()*Length()))
    single_rotational_mobility( Length a){
    return 1.0/(8.0*pi*a*a*a);
  }

  template< class Length>
  auto xx_mobility( Length a0, Length a1, Length r) -> decltype(1.0/r){
    if (r > a0 + a1){
      auto asq = 0.5*(a0*a0 + a1*a1);
      return (0.75 + 0.5*( asq/(r*r)))/(pi6*r);
    }
    else
      return overlap_tt_I_term( a0,a1,r);
  }

  template< class Length>
  auto no_hi_mobility( Length a0, Length a1) -> decltype(1.0/a0){

    return (1.0/a0 + 1.0/a1)/pi6;
  }

  template< class Length>
  auto parallel_mobility( Length a0, Length a1, Length r) -> decltype(1.0/r){

    return no_hi_mobility( a0,a1) - 2.0*zz_mobility( a0,a1,r);
  }

  template< class Length>
  auto perpendicular_mobility( Length a0, Length a1, Length r) -> decltype(1.0/r){

    return no_hi_mobility( a0,a1) - 2.0*xx_mobility( a0,a1,r);
  }

  // not complete, still need rt components for complete overlap
  template< class Length, class Inv_Length, class Inv_Length2, class Inv_Length3>
  void get_components( Length ai, Length aj, Length r, Inv_Length& tt_I, Inv_Length& tt_uu,
                       Inv_Length3& rr_I, Inv_Length3& rr_uu, Inv_Length2& rt_eps, Inv_Length2& tr_eps){

    auto ai2 = ai*ai;
    auto aj2 = aj*aj;
    auto r2 = r*r;
    auto r3 = r2*r;

    if (r > ai + aj){
      {// tt
        auto den = 8.0*pi*r;
        auto a2or2 = (ai2 + aj2)/r2;
        tt_I = (1.0 + a2or2/3.0)/den;
        tt_uu = (1.0 - a2or2)/den;
      }
      { // rr
        auto den = 16.0*pi*r3;
        rr_I = -1.0/den;
        rr_uu = 3.0/den;
      }
      { // rt and tr
        rt_eps = tr_eps = 1.0/(8.0*pi*r2);
      }
    }
    else if (r > fabs( ai - aj)){ // r < a0 + a1
      { // tt
        auto aij = ai*aj;
        auto am2 = sq( ai - aj);
        auto den = 6.0*32.0*pi*aij*r3;
        tt_I = (16.0*r3*(ai + aj) - sq( am2 + 3.0*r2))/den;
        tt_uu = (3.0*sq( am2 - r2))/den;
      }
      { // rr
        auto r4 = r*r3;
        auto r6 = r2*r4;
        auto ai3 = ai2*ai;
        auto aj3 = aj2*aj;
        auto a4a = ai2 + 4.0*ai*aj + aj2;
        auto A = (5.0*r6 - 27.0*r4*(ai2 + aj2) + 32.0*r3*(ai3 + aj3) -
                  9.0*r2*sq(ai2 - aj2) - sq(sq(ai - aj))*a4a)/(64.0*r3);
        auto B = (3.0*sq(sq(ai - aj) - r2)*(a4a - r2))/(64.0*r3);
        auto den = 8.0*pi*ai3*aj3;
        rr_I = A/den;
        rr_uu = B/den;
      }
      { // rt and tr
        auto f = [&]( Length ai, Length aj) -> Inv_Length2 {
          auto ai2 = ai*ai;
          auto aj2 = aj*aj;
          return (sq(ai - aj + r)*(aj2 + 2.0*aj*(ai + r) - 3.0*sq(ai - r)))/(8.0*16.0*pi*ai2*ai*aj*r2);
        };
        rt_eps = f( ai,aj);
        tr_eps = f( aj,ai);
      }
    }
    else{ // r <= fabs( a0 - a1)
      auto a = max( ai,aj);
      tt_I = 1.0/(pi6*a);
      tt_uu = Inv_Length( 0.0);
      
      rr_I = 1.0/(8.0*pi*a*a*a);
      rr_uu = Inv_Length3( 0.0);
      // add rt components later
    }
  }

  template< class Length, class Inv_Length>
  void get_trans_components( Length ai, Length aj, Length r, Inv_Length& tt_I, Inv_Length& tt_uu){

    auto ai2 = ai*ai;
    auto aj2 = aj*aj;
    auto r2 = r*r;
    auto r3 = r2*r;

    if (r > ai + aj){
      {// tt
        auto den = 8.0*pi*r;
        auto a2or2 = (ai2 + aj2)/r2;
        tt_I = (1.0 + a2or2/3.0)/den;
        tt_uu = (1.0 - a2or2)/den;
      }
    }
    else if (r > fabs(ai - aj)){ // r < a0 + a1
      { // tt
        auto aij = ai*aj;
        auto am2 = sq( ai - aj);
        auto den = 6.0*32.0*pi*aij*r3;
        tt_I = (16.0*r3*(ai + aj) - sq( am2 + 3.0*r2))/den;
        tt_uu = (3.0*sq( am2 - r2))/den;
      }
    }
    else{ // r <= fabs( ai - aj)
      auto a = ::std::max( ai,aj);
      tt_I = 1.0/(pi6*a);
      tt_uu = Inv_Length( 0.0);
    }
  }

  using std::get;

  // rij is j - i (opposite of Zuk paper)
  // relates motion of i due to force/torque on j
  // [v_i ] = [ mtt mtr][F_j]
  // [om_i]   [ mrt mrr][T_j]
  //
  template< class Length, class Pos, class Mob_tt, class Mob_tr, class Mob_rr>
  auto matrices( Length ai, Length aj, const Pos& rij) ->  std::tuple< Mob_tt, Mob_tr, Mob_tr, Mob_rr>{

    std::tuple< Mob_tt, Mob_tr, Mob_tr, Mob_rr> res{};
    auto& mtt = get<0>( res);
    auto& mrt = get<1>( res);
    auto& mtr = get<2>( res);
    auto& mrr = get<3>( res);

    //auto r = norm( rij);
    Vec3< double> u = normed( rij);

    Mat3< double> uu;
    for( int i = 0; i < 3; i++)
      for( int j = 0; j < 3; j++)
        uu[i][j] = u[i]*u[j];

    typedef decltype(1.0/Length()) Inv_Length;
    typedef decltype( sq(Inv_Length())) Inv_Length2;
    typedef decltype( Inv_Length()*Inv_Length2()) Inv_Length3;

    Inv_Length tt_I{}, tt_uu{};
    Inv_Length3 rr_I{}, rr_uu{};
    Inv_Length2 rt_eps{}, tr_eps{};

    get_components( ai, aj, norm( rij), tt_I, tt_uu, rr_I, rr_uu, rt_eps, tr_eps);

    auto I = id_matrix< 3>();

    mtt = tt_I*I + tt_uu*uu;
    mrr = rr_I*I + rr_uu*uu;

    Mat3< double> eps_u{ Zeroi{}};
    eps_u[0][1] = -u[2];
    eps_u[1][2] = -u[0];
    eps_u[2][0] = -u[1];
    eps_u[1][0] = u[2];
    eps_u[2][1] = u[0];
    eps_u[0][2] = u[1];

    mrt = rt_eps*eps_u;
    mtr = tr_eps*eps_u;

    return res;
  }


  template< class Length, class Pos>
  auto trans_matrix( Length ai, Length aj, const Pos& rij) -> auto{

    //auto r = norm( rij);
    Vec3< double> u = normed( rij);

    Mat3< double> uu;
    for( int i = 0; i < 3; i++)
      for( int j = 0; j < 3; j++)
        uu[i][j] = u[i]*u[j];

    typedef decltype(1.0/Length()) Inv_Length;

    Inv_Length tt_I{}, tt_uu{};
    get_trans_components( ai, aj, norm( rij), tt_I, tt_uu);

    auto I = id_matrix< 3>();

    auto mtt = tt_I*I + tt_uu*uu;
    return mtt;
  }
}}

