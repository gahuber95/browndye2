#pragma once
/*
 * chain_state_atom.hh
 *
 *  Created on: Jul 19, 2016
 *      Author: root
 */


#include "../../global/pos.hh"
#include "../absinth_atom.hh"

namespace Browndye{

enum class Exclusion_Status { None, Excluded, One_Four};

struct Chain_State_Atom: public Absinth_Atom{
  Pos pos;
  Vec3< Force> force;
  Exclusion_Status estatus = Exclusion_Status::None; // someday put on thread, along with absinth intermediate variables
};

}


