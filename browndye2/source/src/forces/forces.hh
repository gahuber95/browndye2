#pragma once

#include "../structures/solvent.hh"
#include "../molsystem/system_state.hh"

namespace Browndye{

// assumes that core 0 of group 0 is stationary
void compute_forces( System_State& system);

}
