#pragma once

#include <string>
#include "vector.hh"

namespace Browndye{

Vector< std::string> split( const std::string& str, char sp = ' ');

std::string trimmed( const std::string& str);

}

