#pragma once

#include "../hydrodynamics/generic_mobility_full.hh"
#include "bead_chain_mobility_interface.hh"

namespace Browndye{

struct Resistance_Matrices{
  Mat3< Length> A;
  //Mat3< Length2> B;
  Mat3< Length3> C;
  Pos hc;
};

template< class GS>    
Resistance_Matrices  
resistance_matrices_hi( const Bead_Info< GS>&);

template< class GS>    
Resistance_Matrices
resistance_matrices_nohi( const Bead_Info< GS>&);

template< class GS>
Pos hydrodynamic_center( const Bead_Info< GS>&);  
}

namespace Browndye{
 
template< class GS> 
using HDoer = Generic_Mobility_Full::Doer< Bead_Chain_Mobility_I< GS> >; 
 
// approximate
template< class GS>
Pos hydrodynamic_center( const Bead_Info< GS>& binfo){
  
  auto& state = binfo.state;
  Vec3< Length2> sum( Zeroi{});
  Length asum( 0);
  
  for( auto ic: binfo.chain_indices){
    auto& chain_state = state.chain_states[ic];
    auto& chain = chain_state.common();
    auto& atoms = chain_state.atoms;
    auto& catoms = chain.atoms;
    auto na = atoms.size();
    for( auto ia: range(na)){
      auto pos = atoms[ia].pos;
      auto a = catoms[ia].radius;
      sum += a*pos;
      asum += a;
    }
  }
  
  for( auto& stet: binfo.stets){
    auto a = stet.radius;
    for( auto pos: stet.poses){
      sum += a*pos;
      asum += a;
    }
  }
  
  return sum/asum; 
}
 
//######################################################### 
// may want more generic version, like before 
// taken about hydro center from doer
template< class GS>
Resistance_Matrices
resistance_matrices_hi( const Bead_Info< GS>& binfo){
  
  //Pos hc = hydrodynamic_center( binfo);
  Tet_Bead_Info tbinfo( binfo);

  HDoer< GS> doer(  tbinfo);
  //cout << "before update" << endl;
  doer.update( tbinfo);
  //cout << "before resistance mat and com" << endl;
  auto [A,C,com] = doer.resistance_matrices_and_com( tbinfo);
  //cout << "after resistance mat and com" << endl;

  Resistance_Matrices rm;
  rm.A = A;
  rm.C = C;
  rm.hc = com;
  return rm;
}

//######################################################
template< class F, class GS>
void process_beads( F& f, const Bead_Info< GS>& binfo){
  
  for( auto& stet: binfo.stets){
    for( auto& pos: stet.poses){
      f( pos, stet.radius);
    }
  }
  
  auto& state = binfo.state;
  for( auto& ci: binfo.chain_indices){
    auto& atom_states = state.chain_states[ci].atoms;
    auto& atoms = state.chain_states[ci].common().atoms;
    auto na = atom_states.size();
    
    for( auto i: range(na)){
      f( atom_states[i].pos, atoms[i].radius);
    }
  }
}

//#############################################################
template< class GS>
Resistance_Matrices
resistance_matrices_nohi( const Bead_Info< GS>& beads){
  
  Resistance_Matrices res;
  auto& A = res.A;
  //auto& B = res.B;
  auto& C = res.C;
  auto& hc = res.hc;
  
  //B = Mat3< Length2>( Zeroi{});
  C = Mat3< Length3>( Zeroi{});
  
  Length Aval( 0.0);
  
  hc = hydrodynamic_center( beads);
  
  auto f = [&]( Pos pos, Length radius){
    auto ap6 = pi6*radius;
    Aval += ap6;
    auto r = pos - hc;
    C += ap6*(norm2(r)*id_matrix<3>() - outer( r,r));
  };
  
  process_beads( f, beads);
  A = Aval*id_matrix<3>();
  return res;
}

}
