#include "stager.hh"
#include "../molsystem/system_state.hh"
#include "../lib/bisect.hh"

namespace{
  using namespace Browndye;
  const Group_Index g0{0}, g1{1};
  const Core_Index m0{0};
}

namespace Browndye{

// later, input this
//const Vector< double> distances0{ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,
//  10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0};

const Length bwidth0{ 1.0};
const Length bwidthR{ 10.0};

// test it
Vector< Length, Bin_Index> partitions( Length Rb){
  
  if (bwidth0 + bwidthR > Rb){
    error( "need smaller bins at each end");
  }
  
  Length min_dist( large);
  double best_alpha = NaN;
  Bin_Index nbins{ maxi};
  for( size_t nb: range( 2, 10000)){
    
    const double eps = 1.0e-4;
    
    auto series = [&]( double alpha){
      auto a = alpha + eps;
      return (1.0 - pow( a, nb))/(1.0 - a);
    };
    
    auto f = [&]( double alpha){
      return series( alpha) - Rb/bwidth0;
    };
    
    auto hia = Bisect::upper_bound( f, 1.0);
    auto alpha = Bisect::solution( f, 1.0, hia, eps);
    
    auto d = fabs( bwidth0*pow( alpha, nb-1) - bwidthR);
    if (d < min_dist){
      min_dist = d;
      best_alpha = alpha;
    }
    else{
      nbins = Bin_Index( nb-1);
      break;
    }
  }
  
  Vector< Length, Bin_Index> parts( inced( nbins));
  auto factor = 1.0;
  parts.front() = Length{ 0.0};
  for( auto i: range( Bin_Index(1), nbins)){
    parts[i] = parts[deced(i)] + bwidth0*factor;
    factor *= best_alpha;
  }
  parts.back() = Rb;
  return parts;
}

//#######################################################
// constructor
Stager::Stager( const System_Common& csys){
  
  auto Rb = csys.b_rad;
  
  distances = partitions( Rb);
  auto nb = distances.size();  
  flux_sums.resize( nb, Array< Inv_Length, 2>( Zeroi{}));
  counts.resize( deced(nb), 0);
  times.resize( deced(nb), Time{0});
  n_crosses.resize( nb, Array< size_t,2>( Zeroi{}));
}

//######################################################################################
// later, include HI
Diffusivity Stager::site_diffusivity( const System_State& state) const{
   
  auto& group1 = state.groups[ g1];
  return group1.diffusivity();
}

//##########################################################################
Inv_Length Stager::site_force( const System_State& state, Vec3< double> u01) const{
  
  auto& group1 = state.groups[ g1];  
  auto F1 = group1.scaled_force();
  return dot( F1, u01);  
}

//##################################################################
std::optional< Bin_Index> Stager::bin_of( Length r) const {
  
  auto bgn = distances.begin();
  auto end = distances.end();
  auto itr = std::upper_bound( bgn, end, r);
  if (itr == end){
    return std::nullopt;
  }
  else{
    return Bin_Index( size_t( distance( bgn, itr) - 1));
  }   
}

//#################################################################
// refactor
std::pair< bool, Bin_Index> Stager::add_data_reject( const System_State& state){
  
  auto& group0 = state.groups[ g0];
  auto& group1 = state.groups[ g1];
  
  auto dt = state.last_dt;
  auto D = site_diffusivity( state);
  auto cen1 = group1.center();
  auto cen0 = group0.center();
  
  //Core_Index closest_im;
  //Site_Index closest_ist;
  Length2 min_d2{ large};
  Pos min_r01;
  auto& cores0 = group0.core_states;
  for( auto im: range( cores0.size())){
    auto& core = cores0[im];
    auto tform0 = core.transform();
    auto& ccore = core.common();
    auto& sites = ccore.sites;
    for( auto ist: range( sites.size())){
      auto spos0 = sites[ist];
      auto spos = tform0.transformed( spos0);
      auto r01 = cen1 - spos;
      auto d2 = norm2( r01);
      if (d2 < min_d2){
        min_d2 = d2;
        //closest_im = im;
        //closest_ist = ist;
        min_r01 = r01;
      }
    }
  }
  
  auto rsite2 = norm2( min_r01);
  auto rsite = sqrt( rsite2);
  
  auto rcen01 = cen1 - cen0;
  auto rcen = norm( rcen01);
  
  auto Rb = state.common().b_rad;
  auto rho = (2.0*Rb/pi)*atan( rsite/(Rb - rcen));
  
  auto bcen = Rb - rcen;
  auto drho_drsite = (2.0*Rb/pi)/(rsite2/bcen + bcen);
  auto drho_drcen = drho_drsite*rsite/bcen;
  
  auto drhodx1 = (drho_drsite/rsite)*min_r01 + (drho_drcen/rcen)*rcen01;
  
  auto F1 = group1.scaled_force();
  
  auto F = dot( F1, normed( drhodx1));
  
  auto cterm = [&]( int dir) -> Inv_Length{
    auto Ddt = D*dt;
    auto ddir = (double)dir;
    auto Favg = 0.5*(last_F + F);
    return 1.0/(sqrt( Ddt/pi) + ddir*Ddt*Favg);
  };
   
  Bin_Index ib{ maxi};
  bool reject;
  auto go_forward = [&]( bool gf, int dir){
    if (gf){
      last_F = F;
      last_rho = rho;
      reject = false;
      auto lib = state.ibin.value();
      auto dib = Bin_Index( 1) - Bin_Index( 0);
      ib = lib + dir*dib;
    }
    else{
      reject = true;
    }
  };
  
  if (last_rho){ // not first time
    auto last_rhov = last_rho.value();
    
    auto ib0o = bin_of( last_rhov);
    if (!ib0o){
      error( __FILE__, __LINE__, "not get here");
    }
    auto ib0 = ib0o.value();
    ++(counts[ib0]);
    times[ib0] += dt; // perhaps use accumulator
    
    auto ib1o = bin_of( rho);
    if (!ib1o){ // crossed outermost
      auto ib0p = inced( ib0);
      ++(n_crosses[ib0p][1]);
      flux_sums[ib0p][1] += cterm( 1);
      reject = true;
    }
    else{
      auto ib1 = ib1o.value();
      auto cdiff = counts[ib1] <= counts[ib0];
      
      if (ib1 > ib0){ // forward
        auto ib0p = inced( ib0);
        flux_sums[ib0p][1] += cterm( 1);
        ++(n_crosses[ib0p][1]);
        
        go_forward( cdiff, 1);
      }
      else if (ib1 < ib0){ // backwards
        flux_sums[ib0][0] += cterm( -1);
        ++(n_crosses[ib0][0]);
        
        go_forward( cdiff, -1);
      }
      else{ // same bin
        reject = false;
      }
    }
  }
  else{ // first time
    ib = bin_of( rho).value();
    go_forward( true, 0);
  }
  
  return std::make_pair( reject, ib);
}

//################################################
Stager Stager::copy() const{
  
  Stager res( Init_Tag{});
  res.counts = counts.copy();
  res.distances = distances.copy();
  res.flux_sums = flux_sums.copy();
  res.n_crosses = n_crosses.copy();
  res.times = times.copy();
  return res;
}

//################################################
Stager added( const Stager& s0, const Stager& s1){
  
  auto addv = [&]( auto& v0, auto& v1){
    auto n = v0.size();
    for( auto i: range( n)){
      v1[i] += v0[i];
    }
  };
  
  Stager res = s1.copy();
  addv( s0.counts, res.counts);
  addv( s0.flux_sums, res.flux_sums);
  addv( s0.n_crosses, res.n_crosses);
  addv( s0.times, res.times);
  
  return res;
} 

}
