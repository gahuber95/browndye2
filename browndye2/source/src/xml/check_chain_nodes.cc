#include "check_bdtop_nodes.hh"

namespace Browndye{
  void check_chain_input( JAM_XML_Pull_Parser::Node_Ptr node){
    
    Tag pattern{ "root", "name", "atoms", "never_frozen", "constant_dt",
            
            tag( "bonds", tag( "bond", "atoms","type","cores")),
            tag( "angles", tag( "angle", "atoms", "type", "cores")),
            tag( "dihedrals", tag( "dihedral", "atoms", "type", "cores")),
            
            tag( "length_constraints",
              tag( "length_constraint", "atoms", "length", "cores")),
            tag( "coplanar_constraints",
              tag( "coplanar_constraint", "atoms", "cores")),
            
            tag( "excluded_atoms",
                tag("chain_atom", "atom", "ex_atoms"),
                tag( "core", "name",
                    tag("core_atom", "atom", "ex_atoms"))
                ),
            
            tag( "excluded_coulomb_atoms",
                tag("chain_atom", "atom", "ex_atoms"),
                tag( "core", "name",
                    tag("core_atom", "atom", "ex_atoms"))
                ),
            
            tag( "one_four_atoms",
              tag("chain_atom", "atom", "ex_atoms"),
              tag( "core", "name",
                  tag("core_atom", "atom", "ex_atoms"))
              ),
            
            tag( "core_interactors",
                tag( "interactor", "group", "core", "atom",
                    "inner_distance", "outer_distance")),

            tag( "dummy_interactors",
              tag( "interactor", "group", "core", "atom",
                   "inner_distance", "outer_distance")),

            tag( "chain_interactors",
                 tag( "interactor", "group", "chain",
                     "inner_distance", "outer_distance", "touching")),
            
            tag( "copy", "parent", "translation", "rotation",
                tag("equivalent_cores", "match")
                )
          };
    
    pattern.check_node( node);
  }
}
