#pragma once

#include "../lib/array.hh"

namespace Browndye{

// relative to origin
/*
template< class Length, class Length2, class Length3>
auto mobility_matrices_and_com( Mat3< Length> A, Mat3< Length2> B, Mat3< Length3> C){
  
  using std::get;
  
  typedef decltype( 1.0/Length()) Inv_Length;
  typedef decltype( 1.0/Length2()) Inv_Length2;
  typedef decltype( 1.0/Length3()) Inv_Length3;
  typedef Vec3< Length> Pos;
  
  std::tuple< Mat3< Inv_Length>, Mat3< Inv_Length2>, Mat3< Inv_Length3>, Pos> res;  
  auto& a = get<0>( res);
  auto& b = get<1>( res);
  auto& c = get<2>( res);
  auto& r = get<3>( res);
  
  auto Cinv = inverse( C);
  auto BtCB = transpose( B)*Cinv*B;
  a = inverse( A - BtCB);
  b = -Cinv*(B*a);
  auto Ainv = inverse( A);
  auto BABt = B*Ainv*transpose( B);
  c = inverse( C - BABt);
  Vec3< Inv_Length2> eb{ b[1][2] - b[2][1], b[2][0] - b[0][2], b[0][1] - b[1][0]};
  // p 114 Kim and Karrila, Microhydrodynamics
  auto trc = trace( c);
  auto cc = c - trc*id_matrix<3>();
  r = -inverse(cc)*eb; // kludge  
  
  return res;
}
 */
}
