#pragma once

#include "../lib/vector.hh"
#include "../lib/array.hh"
#include "../lib/units.hh"
#include "../global/indices.hh"

namespace Browndye{
  
  struct Coarse_And_Fine{
    Vec3< Sqrt_Time> coarse_dW;
    Vector< Vec3< Sqrt_Time>, Atom_Index> fine_dW;
    
    // copy constructor
    Coarse_And_Fine( const Coarse_And_Fine& other){
      coarse_dW = other.coarse_dW;
      fine_dW = other.fine_dW.copy();
    }
    
    Coarse_And_Fine() = default;
    Coarse_And_Fine( Coarse_And_Fine&&) = default;
    Coarse_And_Fine& operator=( Coarse_And_Fine&&) = default;
  };
  
  typedef Vector< Vector< Coarse_And_Fine, Mob_Div_Index>, Group_Index> Wiener_Vector;
}
