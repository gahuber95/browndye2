#include "../xml/node_info.hh"
#include "../input_output/get_args.hh"

// still need to test

namespace{
  using namespace Browndye;
  using std::cout;
  using std::string;
  namespace JP = JAM_XML_Pull_Parser;
  
  typedef  std::map< std::pair< string, string>, double> CMap;
  
  void do_charge_residues( CMap& cmap, JP::Node_Ptr rnode){
    
    auto anodes = rnode->children_of_tag( "atom");
    auto rname = checked_value_from_node< string>( rnode, "name");
    for( auto anode: anodes){
      auto aname = checked_value_from_node< string>( anode, "name");
      auto q = checked_value_from_node< double>( anode, "charge");
      auto key = std::make_pair( rname, aname);
      cmap[ key] = q;
    }
  }
  
  //#############################################
  CMap charge_map( const string& cfile){
    
    CMap res;
    auto dor = [&]( JP::Node_Ptr rnode){
      do_charge_residues( res, rnode);
    };
    
    std::ifstream input( cfile);
    JP::Parser parser( input, cfile);
    auto top = parser.top();
    parser.apply_to_nodes_of_tag( top, "residue", dor);
    return res;
  }
  
  //#############################################
  void echo( JP::Node_Ptr node, size_t ns, const string& tag){
    
    for( auto i: range(ns)){
      (void)i;
      cout << ' ';
    }
    
    auto text = checked_value_from_node< string>( node, tag);
    cout << "<" << tag << "> " << text << " </" << tag << ">\n";
  }
  
  //#############################################
  void do_residue( const CMap& cmap, JP::Node_Ptr rnode){
    
    cout << "  <residue>\n";
    
    echo( rnode, 4, "name");
    echo( rnode, 4, "number");
     
    auto rname = checked_value_from_node< string>( rnode, "name");
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      auto aname = checked_value_from_node< string>( anode, "name");
      
      cout << "    <atom>\n";
      echo( anode, 6, "name");
      echo( anode, 6, "number");
      
      echo( anode, 6, "x");
      echo( anode, 6, "y");
      echo( anode, 6, "z");
     
      auto q = [&]{
        auto key = std::make_pair( rname, aname);
        auto itr = cmap.find( key);
        if (itr == cmap.end()){
          error( "sub_charges: charge not found for", rname, aname);
        }
        return itr->second;
      }();
      cout << "      <charge> " << q << " </charge>\n";
      
      echo( anode, 6, "radius");
      
      cout << "    </atom>\n";
    }
    cout << "  </residue>\n";
  }
  
  //#############################################
  void main0( int argc, char* argv[]){
    
    if (argc == 2 && argv[1] == string( "-help")){
      cout << "sub_charges -c charges.xml substitutes charges from charges.xml to pqrxml file from standard input "
      "and sends to standard output\n";
    }
    else{
      auto cfile = string_arg( argc, argv, "-c");
      auto cmap = charge_map( cfile);
      
      auto dor = [&]( JP::Node_Ptr rnode){
        do_residue( cmap, rnode);
      };
      
      JP::Parser parser( std::cin);
      auto top = parser.top();
      cout << "<roottag>\n";
      parser.apply_to_nodes_of_tag( top, "residue", dor);
      cout << "</roottag>\n";
    }
  } 
}

int main( int argc, char* argv[]){
  
  main0( argc, argv);
}
