#pragma once

namespace Browndye{
  
  template< class T>
  auto sq( T t){
    return t*t;
  }
  
  template< class T>
  auto cube( T t){
    return t*sq( t);
  }
}