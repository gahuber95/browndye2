#include <cassert>
#include "../molsystem/system_state.hh"
#include "../forces/forces.hh"
#include "constraint_tolerance.hh"
#include "wiener_interface.hh"
#include "../forces/other/cd_bonded_parameter_info.hh"

//###############################################
namespace{

  constexpr bool diff_test = false;
  constexpr bool rot_test = false;

  using namespace Browndye;
  
  using std::min;
  using std::max;
  using std::make_tuple;
  using std::get;
  using std::tuple;
    
  bool has_cores( const System_Common& fcommon);
  bool cm_length_constraint_ok( const Group_Thread&, const Group_State&, Constraint_Index);
  bool coplanar_constraint_ok( const Group_State&, Constraint_Index);
  void check_length_constraint_blowup( const Group_Thread&, const Group_State&,
                                  Constraint_Index);
  void check_coplanar_constraint_blowup( const Group_State& group,
                                            Constraint_Index ic);
}

namespace Browndye{
  
  class Rxn_Tester{
  public:
    System_State& state;
    Time dt, dt_min, dtr_min;
    Length old_rxn_coord, rxn_coord;
    Diffusivity D;
    Length req_distance;
    bool const_dt;
    
    Rxn_Tester( System_State& _state, Time _dt, Time _dt_min, Time _dtr_min,
                 Length _old_rxn_coord, Length _rxn_coord,
                 Diffusivity Din, Length _req_distance, bool hcdt):
      state(_state), dt(_dt), dt_min(_dt_min), dtr_min(_dtr_min),
      old_rxn_coord( _old_rxn_coord), rxn_coord( _rxn_coord), D(Din),
      req_distance( _req_distance), const_dt( hcdt)
      {}
    
    [[nodiscard]] bool reaction_bstep() const;
    [[nodiscard]] bool force_bstep() const;
  };

//######################################################################
constexpr double back_angle = 20.0;

void System_State::check_for_blowup() const{
  
  auto check = [&]( const std::string& msg, auto ia, auto ib, auto ic, auto x){
    typedef decltype( x) X;
    
    auto xval = x/X(1.0);
    if (!(xval == xval) || std::isinf( xval)){
      auto it = thread().number;
      error( "System blew up", msg, ia, ib, ic, " thread ", it);
    }
  };
  
  for( auto ig: range( groups.size())){
    auto& group = groups[ig];
    for( auto it: range( group.tets.size())){
      auto& tet = group.tets[it];
      for(auto  k: range( tet.poses.size())){
        auto pos = tet.poses[k];
        for( auto x: pos){
          check( "tet", ig,it, k, x);  
        }
      }
    }
    for( auto ic: range( group.chain_states.size())){
      auto& chain = group.chain_states[ic];
      for( auto ia: range( chain.atoms.size())){
        auto& atom = chain.atoms[ia];
        for( auto x: atom.pos){
          check( "chain", ig, ic, ia, x);
        }
      }
    }
  }
}

//#############################################################
// debug
[[maybe_unused]] double System_State::constraint_error() const{
  auto& group = groups[ Group_Index(1)];
  auto info = (group.cons_info_and_doer);
  info->cinfo.initial_satisfaction = true;
  auto res = info->doer->sys_rms_violation( info->cinfo);
  info->cinfo.initial_satisfaction = false;
  return res;
}

[[maybe_unused]] double System_State::saved_constraint_error() const{
  auto& group = groups[ Group_Index(1)];
  auto info = (group.cons_info_and_doer);
  auto res = info->doer->sys_rms_violation( info->cinfo);
  return res;
}

//##############################################################
auto System_State::group_forces_torques() const -> FT_Info {

  FT_Info info;

  F3 total_force(Zeroi{});
  T3 total_torque(Zeroi{});

  auto ng = groups.size();
  auto &forces = info.forces;
  forces.resize(ng);
  forces.fill(F3(Zeroi{}));

  auto &torques = info.torques;
  torques.resize(ng);
  torques.fill(T3(Zeroi{}));

  info.max_force = Force( 0.0);
  info.max_torque = Torque( 0.0);

  for (auto ig: range(ng)) {
    auto &group = groups[ig];
    for (auto &tet: group.tets) {
      for (auto k: range(Atom_Index(4))) {
        auto F = tet.forces[k];
        auto pos = tet.poses[k];
        auto T = cross(pos, F);
        total_force += F;
        total_torque += T;
        forces[ig] += F;
        torques[ig] += T;
        info.max_force = std::max( info.max_force, norm( F));
        info.max_torque = std::max( info.max_torque, norm( T));
      }
    }
    for (auto &chain: group.chain_states) {
      if (!chain.frozen) {
        for (auto &atom: chain.atoms) {
          auto F = atom.force;
          auto pos = atom.pos;
          auto T = cross(pos, F);
          total_force += F;
          forces[ig] += F;
          total_torque += T;
          torques[ig] += T;
          info.max_force = std::max( info.max_force, norm( F));
          info.max_torque = std::max( info.max_torque, norm( T));
        }
      }
    }
  }
  info.total_force = total_force;
  info.total_torque = total_torque;

  return info;
}
//###############################################################################
// for debug
[[maybe_unused]]
void System_State::test_force_balance( int tag) const{
  
  auto info = group_forces_torques();
  auto& forces = info.forces;
  auto& torques = info.torques;
  auto total_force = info.total_force;
  auto total_torque = info.total_torque;

  auto fnorms = forces.mapped( [&](F3 f){ return norm( f);});
  auto tnorms = torques.mapped( [&]( T3 t){ return norm( t);});

  for( auto k: range(3)){
    check_nan_always( total_force[k]);
    check_nan_always( total_torque[k]);
  }

  auto tf = norm( total_force);
  auto tt = norm( total_torque);

  if (tf > 1.0e-6*info.max_force || tt > 1.0e-6*info.max_torque){
    std::cout << "total force " << total_force << std::endl;
    std::cout << "total torque " << total_torque << std::endl;
    for( auto& group: groups){
      
      std::cout << "group\n";
      for( auto& tet: group.tets){
        std::cout << "tet\n";
        for( auto k: range(Atom_Index(4))){
          auto F = tet.forces[k];
          std::cout << k << ' ' << F << std::endl;
        }
      }
      for( auto& chain: group.chain_states){
        if( !chain.frozen){
          std::cout << "chain\n";
          for( auto& atom: chain.atoms){
            auto F = atom.force;
            std::cout << F << std::endl; 
          }
        }
      }
    }

    auto it = thread().number;
    error( "unbalanced force", it, tag);
  }
}

//############################################################
void
System_State::do_bd_step( Time dt, const Wiener_Vector& ws, BD_Step_Result& step_result) {

  //if (n_bd_steps % 10 == 0) {
    //std::cout << "bd step " <<   n_bd_steps << ' ' << dt << ' ' << rxn_coord() << std::endl;
    //if (n_bd_steps%1000 == 0){
    //  std::cout.flush();
    //}
  //}

  auto& solvent = common().solvent;
  bool has_const_dt{ constant_dt()};  
   
  assert( dt > Time( 0.0));
  ++n_bd_steps;
  assert( consistency.all());  
        
  fate = System_Fate::In_Action;
  bool& must_backstep = step_result.must_backstep;
  must_backstep = false;
  step_result.is_done = false;
  step_result.rxn_completed = false;
  
  // needed? yes
  for( auto& group: groups){
    auto viol = group.constraint_error();
    if (viol > constraint_solve_tol){
      auto info = group.cons_info_and_doer;
      auto& cinfo = info->cinfo;  
      auto& constrainer = info->doer;
      cinfo.initial_satisfaction = true;
      constrainer->satisfy_no_init( cinfo, constraint_solve_tol);
      cinfo.initial_satisfaction = false;
    }
    compute_forces( *this);
  }
    
  save();
    
  auto old_rxn_coord = rxn_coord();
  
  step_beads_forward( solvent, ws, dt, must_backstep);
 

  check_for_blowup();  
  
  auto [dt_min, dtr_min] = time_step_minimum_bounds();
  const bool large_dt = (dt > dt_min);
  
  must_backstep = must_backstep && large_dt;
  
  // extra safety check for stability  
  if (has_const_dt){
    bool did_split;
    split_const_dt( dt, step_result, did_split);
    if (did_split){
      return;
    }
  }
              
  if (!must_backstep){
    auto conr_backstep = backstep_due_to_constraints();
    must_backstep = conr_backstep && large_dt;
  }
    
  if (!must_backstep){
    satisfy_constraints();

    auto [rc,D,rr] = rxn_coord_n_diffu();
    Rxn_Tester tester{ *this, dt, dt_min, dtr_min,
                        old_rxn_coord, rc,D,rr, has_const_dt};
    compute_forces( *this);
    must_backstep = tester.force_bstep() && large_dt;
  
    if (!must_backstep){
      test_and_update_reactions( tester.rxn_coord, step_result);
      if (!step_result.rxn_completed){
        must_backstep = tester.reaction_bstep();
      }
    }
  }
  
  if (has_const_dt){ // perhaps redundant
    must_backstep = false;
  }
  
  if (restraint_violated()){
    if (!must_backstep){
      restore();
    }
  }
}

//##################################################################
void check_constraints( const Group_State& group, bool& did){
  
  const double constraint_tol = 0.2;
  
  did = false;
  auto& constraints = group.cons_info_and_doer->cinfo.constraints;
  for( auto ic: range( constraints.size())){
    auto iic = constraints[ic];
    
    auto ctype = group.constraint_type( iic);
    if (ctype == Constraint_Type::Chain_Core){
      auto r_act = group.constraint_length( ic);
      auto [ib0,ib1] = group.lconstraint_beads( ic);
      auto pos0 = group.bead_position( ib0);
      auto pos1 = group.bead_position( ib1);
      auto r = distance( pos0, pos1);
      
      if (fabs( r - r_act)/r_act > constraint_tol){
        did = true;
        break;
      }
    }
    if (did){
      break;
    }
  }
}

//#########################################################################
// for now, just check COFFDROP chain-chain bonds       

void System_State::check_bonds( const Group_State& group, bool& did) const{
  
  auto& bpinfop = common().bpinfo;
  if (bpinfop->ff_type() == Force_Field_Type::Spline){
    auto bpinfo = dynamic_cast< CD_Bonded_Parameter_Info*>( bpinfop.get());
    auto& bond_types = bpinfo->bond_types;
  
    size_t ibond = 0;  
    for( auto& chain: group.chain_states){
      if (!chain.frozen){
        auto& bonds = chain.common().bonds;
        for( auto& bond: bonds){
          auto& atomis = bond.atomis;
          auto ia0 = atomis[0];
          auto ia1 = atomis[1];
          auto& coris = bond.coris;
          auto ic0o = coris[0];
          auto ic1o = coris[1];
          
          auto is_chain0 = std::holds_alternative< On_Chain_Tag>( ic0o);
          auto is_chain1 = std::holds_alternative< On_Chain_Tag>( ic1o);
          
          if (is_chain0 && is_chain1){
            auto& atoms = chain.atoms;
            auto& atom0 = atoms[ia0];
            auto& atom1 = atoms[ia1];
            auto pos0 = atom0.pos;
            auto pos1 = atom1.pos;
            auto r = distance( pos1, pos0);
            auto it = bond.type;
            auto& bond_type = bond_types[it];
            auto max_r = bond_type.Vs->high_bound();
            if (r > max_r){
              did = true;
              break;
            }
          }
          else if (is_chain0 && !is_chain1){
            auto& atoms = chain.atoms;
            auto& atom0 = atoms[ia0];
            auto pos0 = atom0.pos;
            
            auto im1 = std::get< Core_Index>( ic1o);
            auto& core1 = group.core_states[im1];
            auto pos1t = core1.atoms()[ia1].pos;
            auto pos1 = core1.transformed( pos1t);
             
            auto r = distance( pos1, pos0);
            auto it = bond.type;
            auto& bond_type = bond_types[it];
            auto max_r = bond_type.Vs->high_bound();
            if (r > max_r){
              did = true;
              break;
            }
          }
          else if (is_chain1 && !is_chain0){
            auto& atoms = chain.atoms;
            auto& atom1 = atoms[ia1];
            auto pos1 = atom1.pos;
            
            auto im0 = std::get< Core_Index>( ic0o);
            auto& core0 = group.core_states[im0];
            auto pos0t = core0.atoms()[ia0].pos;
            auto pos0 = core0.transformed( pos0t);
             
            auto r = distance( pos1, pos0);
            auto it = bond.type;
            auto& bond_type = bond_types[it];
            auto max_r = bond_type.Vs->high_bound();
            if (r > max_r){
              did = true;
              break;
            }
          }
          ++ibond;
        }
      }
      if (did){
        break;
      }
    }
  } 
}

//#########################################################################
void System_State::do_double_step( Time dt, BD_Step_Result& step_result){
  
  restore();
  auto hdt = 0.5*dt;
  auto hws = Wiener_Interface::wiener_vector( *this);
  
  Wiener_Interface::set_gaussian( *this, sqrt(hdt), hws);
  --n_bd_steps;
  do_bd_step( hdt, hws, step_result);
    
  if( !(step_result.is_done || step_result.rxn_completed)){  
    Wiener_Interface::set_gaussian( *this, sqrt(hdt), hws);
    --n_bd_steps;
    do_bd_step( hdt, hws, step_result);
  }
  
}

//#################################################################################
void System_State::split_const_dt( Time dt,
                                  BD_Step_Result& step_result, bool& did){
  
  did = false;
 
  auto ng = groups.size();
  for( auto ig: range( ng)){
    auto& group = groups[ig];    
    check_constraints( group, did);
    
    if (did){
      break;
    }
    else{
      check_bonds( group, did);
      if (did){
        break;
      }
    }
  }
  
  if (did){ 
    do_double_step( dt , step_result);
  }
}

//##################################################################
/*
void System_State::split_for_broken_bond( Time dt, BD_Step_Result& step_result){
  
  restore();
  auto hdt = 0.5*dt;
  auto hws = Wiener_Interface::wiener_vector( *this);
  
  Wiener_Interface::set_gaussian( *this, sqrt(hdt), hws);
  do_bd_step( hdt, hws, step_result);
  
  if( !(step_result.is_done || step_result.rxn_completed)){  
    Wiener_Interface::set_gaussian( *this, sqrt(hdt), hws);
    do_bd_step( hdt, hws, step_result);
  }
}
*/
//#########################################################
bool System_State::restraint_violated() const{
  
  typedef System_Common::Mol_Rxn_Ref Mol_Rxn_Ref;
  
  for( auto& restraint: common().restraints){
    
    auto [rref0,ai0] = restraint.markers[0];
    auto [rref1,ai1] = restraint.markers[1];
    
    auto pos = [&]( Mol_Rxn_Ref rref, Atom_Index ia){
      auto ig = rref.ig;
      auto& group = groups[ig];
      if (rref.is_core()){
        auto im = std::get< Core_Index>( rref.core_or_chain);
        auto& core = group.core_states[ im];
        auto& atom = core.atoms()[ia];
        return core.transformed( atom.pos);
      }
      else{
        auto ic = std::get< Chain_Index>( rref.core_or_chain);
        auto& chain = group.chain_states[ ic];
        auto& atom = chain.atoms[ia];
        return atom.pos;
      }
    };
    
    auto pos0 = pos( rref0, ai0);
    auto pos1 = pos( rref1, ai1);
    auto d = restraint.length;
      
    if (distance( pos0, pos1) > d){
      return true;
    }
  }
  return false;
}

//#########################################################  
bool Rxn_Tester::reaction_bstep() const{
  
  if (const_dt){
    return false;
  }
  
  else if (state.common().has_pathway()){
  
    auto adt_min = 0.5*sq( 0.0001*req_distance)/D;
    
    if (dt > std::max( dtr_min, adt_min)){
      auto sdev = sqrt(2.0*D*dt);
      return (12.0*sdev > old_rxn_coord); // 3.0
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }
}

//###################################################
bool Rxn_Tester::force_bstep() const{
  
  if (const_dt){
    return false;
  }
  else{
    const auto& solvent = state.common().solvent;
    return (dt > dt_min && state.backstep_due_to_force( solvent, dt));
  }
}
  
//##############################################################
std::pair< Time, Time> System_State::time_step_minimum_bounds() const{
  
  auto all_frozen = are_all_frozen();
  
  auto& fcommon = common();  
  auto& ts_params = fcommon.ts_params;
  auto with_cores = has_cores( fcommon);
  
  auto use_core_dt = with_cores && all_frozen;
  
  auto dt_min =
    use_core_dt ?
       ts_params.dt_min_core : ts_params.dt_min_chain; 
    
  auto dtr_min =
    use_core_dt ?
      ts_params.dtr_min_core : ts_params.dtr_min_chain;
      
  auto final_dt = minimum_dt();    
      
  return std::make_pair( std::max( final_dt, dt_min),
                        std::max( final_dt, dtr_min));
}

//##############################################################
void System_State::test_and_update_reactions( Length rxn_coord, 
                                 BD_Step_Result& step_result){
 
  auto& fcommon = common();  
  if (fcommon.has_pathway()){
    
    minimum_rxn_coord = min( minimum_rxn_coord, rxn_coord);
    
    Rxn_Index irxn;
    auto& completed = step_result.rxn_completed;
    std::tie( completed, irxn) = fcommon.next_completed_reaction( current_rxn_state, *this);
    if (completed){
      auto istate = fcommon.state_after_reaction( irxn);
      last_rxn = irxn;
      current_rxn_state = istate;
      if (fcommon.n_reactions_from( istate) == 0){
        fate = System_Fate::Final_Rxn;
        step_result.is_done = true;
      }
    }
  }  
}

//##############################################################
bool System_State::are_all_frozen() const{

  bool all_frozen = true;
  for( auto& group: groups){
    if (group.has_unfrozen_chain()){
      all_frozen = false;
      break;
    }
  }
  return all_frozen;
}

//##############################################
struct Angles_Sys{
  Browndye_RNG& gen;
  Time total_t;
  Vec3< double> phi_det;
  Inv_Time Dr;
  
  Array< double, 4> quat{ 1.0, 0.0, 0.0, 0.0};
};

//#######################################
class WIface{
public:
  typedef Vec3< Sqrt_Time> W_Vector;
  typedef ::Time Time;
  typedef Angles_Sys System;

  static  
  void set_half( const W_Vector&, W_Vector&);
  
  static
  void set_difference( const W_Vector&, const W_Vector&, W_Vector&);
  
  static
  void add_gaussian( System&, const Sqrt_Time& scale, W_Vector&);
  
  // first bool denotes whether done, second denotes need to backstep
  static
  std::tuple< bool,bool> advance( System&, Time t, Time dt, const W_Vector&);
  
  static
  void step_back( System&, Time t, Time dt){}
  
  static
  W_Vector copy( const W_Vector& wv){
    return wv;
  }
};

//###########################################################
void WIface::set_half( const W_Vector& a, W_Vector& c){
  
  c = 0.5*a;
}

//###########################################################
void WIface::set_difference( const W_Vector& a, const W_Vector& b,
                        W_Vector& c){
  
  c = a - b;
}

//###########################################################
void WIface::add_gaussian( System& sys, const Sqrt_Time& scale,
                            W_Vector& v){
  
  for( auto i: range(3)){
    v[i] += scale*sys.gen.gaussian();
  }
}

//###########################################################
std::tuple< bool,bool>
WIface::advance( Angles_Sys& sys, Time t, Time dt, const W_Vector& dw){
        
  auto dphi_det = (dt/sys.total_t)*sys.phi_det;
  auto dphi_w = sqrt( 2.0*sys.Dr)*dw;
  auto dphi = dphi_w + dphi_det;

  const size_t thresh_deg = 5;  
  const double thresh = thresh_deg*angle_conv_factor;
  
  if (norm( dphi) > thresh){
    return make_tuple( false, true);
  }
  else{
    auto dquat = quat_of_dom( dphi);
    sys.quat = quat_product( dquat, sys.quat);
    return make_tuple( false, false);
  }
}

//##############################################################
Vec3< double> angle_of_dxs( const Tet< Group_State>& tet, Pos cen,
                               const V4< Pos>& xs){
  
  const auto a4 = Atom_Index( ntet);
  Vec3< Length2> an_mom( Zeroi{});
  for( auto ia: range( a4)){
    auto deltx = xs[ia];
    auto r = tet.poses[ ia] - cen;
    an_mom += cross( r, deltx);
  }
 
  auto imoment = [&]{
    auto d = tet.fixed->length;
    auto cd = d/sqrt(3.0);
    return 3.0*cd*cd;
  }();  
  
  return an_mom/imoment;
}

//###############################################################
Inv_Time System_State::rotational_diffusivity( const Tet< Group_State>& tet) const{
  
  auto rmob = tet.rotational_mobility();
  auto& solvent = common().solvent;
  return solvent.diffusivity_factor()*rmob;
}

//############################################################
void tform_tet_poses( Pos cen, Pos trans, Array< double, 4> quat,
                     Tet< Group_State>& tet){
  
  auto rot = mat_of_quat( quat);
   
  auto rot_tform = Transform( rot, Pos( Zeroi{}));
  auto tform = Pure_Translation( cen + trans)*rot_tform*Pure_Translation( -cen);
   
  for( auto& pos: tet.poses){
    pos = tform.transformed( pos);
  }
}

//##############################################
/*
Pos removed_tet_translations( Tet< Group_State>& tet){
  
  //auto transd = centroid( tet.dposes);
  //auto transw = centroid( tet.wdposes);
  
  const auto a4 = Atom_Index( ntet);
  for( auto ia: range(a4)){
    //tet.dposes[ia] -= transd;
    //tet.wdposes[ia] -= transw;
  }
  
  return Pos( Zeroi{});//transd + transw;
} 
*/
//##############################################################
void check_tet_blowup( Tet< Group_State>& tet, bool& backstep){
  
  auto new_pos = [&]( Atom_Index i){
    return tet.poses[i];
  };
  
  auto L = tet.fixed->length;
  for( auto i: range( Atom_Index(ntet))){
    auto posi = new_pos(i);
    for( auto j: range( i)){
      auto posj = new_pos(j);
      auto r = distance( posi, posj);
      if (r > 10.0*L){
        //error( "core tetrahedron blew up; need smaller minimum dt");
        backstep = true;
      }
    }
  }
}

//################################################################
/*
void System_State::renorm_tet( Time delta_t, bool has_const_dt,
                              Tet< Group_State>& tet,
                                bool& backstep){
  
  backstep = false;
  check_tet_blowup( tet, backstep);
  if (backstep){
    return;
  }
  else{
    auto trans = removed_tet_translations( tet);
    auto cen = centroid( tet.poses);
    
    auto Dr = rotational_diffusivity( tet);
    auto phid = angle_of_dxs( tet, cen, tet.dposes);
    auto phiw = angle_of_dxs( tet, cen, tet.wdposes);
    auto dw0 = (1.0/sqrt(2.0*Dr))*phiw;
     
    const auto back_phi = back_angle*angle_conv_factor;
    if (!has_const_dt && max( norm( phid), norm( phiw)) > back_phi){
      backstep = true;
    }
    
    auto& rng = thread().rng;
    Angles_Sys asys{ rng, delta_t, phid, Dr};
    Wiener_Loop::do_one_full_step< WIface>( asys, delta_t, std::move( dw0));
     
    tform_tet_poses( cen, trans, asys.quat, tet);
  }
}
*/
//##############################################
/*
void System_State::renorm_tets( Time dt, bool& backstep){
    
  bool has_const_dt{ constant_dt()};
  
  backstep = false;
  for( auto& group: groups){
    for( auto& tet: group.tets){
      renorm_tet( dt, has_const_dt, tet, backstep);
      if (backstep){
        break;
      }
    }
  }
}
*/
//##############################################
/*
void System_State::zero_tets_dposes(){
  
  for( auto& group: groups){
    for( auto& tet: group.tets){
      auto& dposes = tet.dposes;
      for( auto& dpos: dposes){
        dpos = Pos( Zeroi{});
      }
     
      auto& wdposes = tet.wdposes;
      for( auto& wdpos: wdposes){
        wdpos = Pos( Zeroi{});
      }
    }
  }
}
*/
//####################################################
void System_State::get_sole_pos( Pos& pos) const{
  auto& group = groups.back();
  
  if (group.core_states.empty()){
    pos = group.chain_states.back().atoms.back().pos;
  }
  else{
    pos = group.core_states.back().translation();
  }
}

//##################################################################
void System_State::get_sole_tet_axis( Array< double,3>& axis) const{
  
  if (groups.size() != Group_Index(1)){
    error( "need exactly one group");
  }
  
  auto& group = groups.back();
  if (group.core_states.size() != Core_Index(1)){
    error( "need exactly one core");
  }
  
  if (!group.chain_states.empty()){
    error( "cannot have any chains");
  }
  
  auto& tet = group.tets.back();
  auto tform = tet.transform_of();
  auto& poses = tet.fixed->poses0;
  
  auto tposes = poses.mapped( [&](Pos pos){
    return tform.transformed( pos);
  });
    
  auto apos = [&]( size_t i){
    return tposes[ Atom_Index(i)];
  };
      
  auto cen = (1.0/3.0)*(apos(1) + apos(2) + apos(3));
  axis = normed( apos(0) - cen);
}

//########################################################
void System_State::step_beads_forward( const Solvent& solvent,
                        const Wiener_Vector& ws, Time dt,
                        bool& backstep){
  
  backstep = false;
  auto& mob_computer = mob_info_and_doer->doer;
  
  // test (someday turn into unit test)
  Pos pos0;
  if constexpr(diff_test){
    get_sole_pos( pos0);
  }
  Vec3< double> axis0;
  if constexpr( rot_test){
    get_sole_tet_axis( axis0);
  }
    
  if (has_hi()){
    Mob_Info_And_State minfo_st{ *this, *mob_info};
    minfo_st.set_time_step( *this, dt);
    
    typedef System_Mobility_Interface::Force_Tag Force_Tag;
    
    //zero_tets_dposes();
    mob_computer.add_product_with_mobility( minfo_st, Force_Tag{}, *this);
    minfo_st.add_stochastic_terms = true;

    mob_computer.add_stochastic_propagation( minfo_st, ws, *this);
  }
  else{
    step_beads_forward_no_hi( solvent, ws, dt);
  }
  
  // instead of tet renormalization
  /*
  for( auto& group: groups){
    for( auto& tet: group.tets){
      for( auto ia: range( Atom_Index{ntet})){
        auto& pos = tet.poses[ia];
        pos += (tet.dposes[ia] + tet.wdposes[ia]); 
      }
    }
  }
  */
  auto [min_dt,min_dtr] = time_step_minimum_bounds();
  check_constraints_blowup( min( min_dt, min_dtr), dt); 
   
  //renorm_tets( dt, backstep);
  //push_tets_down();
    
  // test
  if constexpr(diff_test){
    Pos pos1;
    get_sole_pos( pos1);
    auto dpos = pos1 - pos0;      
    std::cout << dt << ' ' << dpos << std::endl;      
  }
  if constexpr( rot_test){
    Vec3< double> axis1;
    get_sole_tet_axis( axis1);
    auto cthe = dot( axis0, axis1);
    std::cout << dt << ' ' << 1 - cthe << std::endl;
  }
   
  if (!backstep){
    consistency.constraints = false;
    consistency.force = false;
  }
}

//############################################################################
template< class Advance>
void advance_tet_beads( const Advance& advance, Group_State& group, 
                         const Vector< Vec3< Sqrt_Time>, Atom_Index>& wsd,
                         Tet_Index it){
   
  auto& tet = group.tets[it];
  for( auto ia: range( Atom_Index( ntet))){
    auto& f = tet.forces[ia];
    auto a = tet.fixed->radius;
    auto wsdb = wsd[ia];
    Pos dpos, wdpos;
    advance( dpos, wdpos, f, a, wsdb);
    tet.poses[ia] += (dpos + wdpos);

  }
}

//#######################################################################
template< class Advance>
void advance_chain_beads( const Advance& advance, Group_State& group,
                         const Vector< Vec3< Sqrt_Time>, Atom_Index>& wsd,
                          Chain_Index ic
                         ){
  
  auto& chain = group.chain_states[ic];
  auto& atoms = chain.atoms;
  auto na = atoms.size();
  for( auto ia: range( na)){
    auto& f = atoms[ia].force;
    auto a = chain.common().atoms[ia].radius;
    auto wsdb = wsd[ia];
    Pos dpos, wdpos;
    advance( dpos, wdpos, f, a, wsdb);
    auto& pos = atoms[ia].pos;
    pos += (dpos + wdpos);
  }
}

//####################################################################
void System_State::step_beads_forward_no_hi( const Solvent& solvent,
                                const Wiener_Vector& ws, Time dt){
  
  const auto mu = solvent.viscosity();
  const auto kT = solvent.kT;
    
  auto& divisions = mob_info->divisions;
  auto nd = divisions.size();
   
  for( auto isd: range(nd)){
    auto indices = divisions[ isd];
    auto ig = indices.first;
    auto id = indices.second;
  
    auto& wsg = ws[ig];
    auto& wsd = wsg[id].fine_dW;
    auto& group = groups[ig];
    auto& mob_divs = group.cons_info_and_doer->cinfo.divs;
    auto& div = mob_divs[ id];
    
    auto adv_bead = [&]( Pos& dpos, Pos& wdpos, const F3 f,
                         Length a, const auto& wsdb){
      
      auto mob = 1.0/(pi6*mu*a);
      dpos = mob*f*dt; 
      wdpos = sqrt( 2.0*kT*mob)*wsdb;
    };
        
    auto comp_tet = [&]( Tet_Index it) -> void{
      advance_tet_beads( adv_bead, group, wsd, it);
    };
    
    auto comp_chain = [&]( Chain_Index ic) -> void{
      advance_chain_beads( adv_bead, group, wsd, ic);
    };
    
    std::visit( overload( comp_chain, comp_tet), div);
  }
}

//######################################################################
void System_State::satisfy_constraints(){
   
  for( auto& group: groups){
    group.satisfy_constraints();
  }
   
  consistency.constraints = true;
}

//####################################################################
Pos bead_pos( const Group_State& group, Bead_Index ib){
  
  return group.bead_position( ib);
}

//############################################################
Pos old_bead_pos( const Group_Thread& gthread, const Group_State& group, Bead_Index ib){
  
  return group.old_bead_position( gthread, ib);
}

//####################################################################
void System_State::check_constraints_blowup( Time min_dt, Time dt) const{
  
  if (dt <= min_dt){
  
    auto& gthreads = thread().groups;
    auto ng = groups.size();
    for( auto ig: range( ng)){
    //for( auto& group: groups){ 
      auto& group = groups[ig];
      auto& gthread = gthreads[ig];
      auto& constraints = group.cons_info_and_doer->cinfo.constraints;
      for( auto ic: range( constraints.size())){
        auto iic = constraints[ic];
        
        auto ctype = group.constraint_type( iic);
        if (ctype == Constraint_Type::Chain_Core){
          check_length_constraint_blowup( gthread, group, ic);
        }
        else if (ctype == Constraint_Type::Coplanar){
          check_coplanar_constraint_blowup( group, ic);
        }
      }   
    }
  }
}
//###################################################
bool System_State::backstep_due_to_constraints() const{  
    
  bool has_const_dt{ constant_dt()};  
    
  if (has_const_dt){
    return false;
  }
  else{
    auto& gthreads = thread().groups;
    auto ng = groups.size();
    for( auto ig: range( ng)){
    //for( auto& group: groups){ 
      auto& group = groups[ig];
      auto& gthread = gthreads[ig];
      auto& constraints = group.cons_info_and_doer->cinfo.constraints;
      for( auto ic: range( constraints.size())){
        auto iic = constraints[ic];
        
        auto ctype = group.constraint_type( iic);
        if (ctype == Constraint_Type::Chain_Core){
          return !cm_length_constraint_ok( gthread, group, ic);
        }
        else if (ctype == Constraint_Type::Coplanar){
          return !coplanar_constraint_ok( group, ic);
        }
      }   
    }
    return false; 
  }
}

//###################################################
/*
bool are_backstep_close( Length L, Length r){
  return (::fabs( (r-L)/L) < constraint_backstep_tol);
}
*/
//################################################################
class Force_Change_Measures{
public:
  void accumulate_measures( const System_State& state);
  bool is_timestep_too_large( const Solvent&, Time dt);
  
private:
  Length2 dx2_sum{ 0.0};
  Force aDdxdF_sum; 
  
  void add_measure( Length, Pos, Pos, F3, F3, Force&);
  void accumulate_chain_measures( const Group_Thread&, const Group_State&, Force&);
  void accumulate_tet_measures( const Group_Thread&, const Group_State&, Force&);
};

//######################################################################################
bool System_State::backstep_due_to_force( const Solvent& solvent, Time dt) const{
  
  Force_Change_Measures force_change_measures;
  force_change_measures.accumulate_measures( *this);
  return force_change_measures.is_timestep_too_large( solvent, dt);
}

//###################################################################################
bool Force_Change_Measures::is_timestep_too_large( const Solvent& solvent,
                                                    Time dt){
  
  auto mu = solvent.viscosity();
  constexpr double alpha = 0.02;
   
  if (aDdxdF_sum <= decltype( aDdxdF_sum)(0.0)){
    return false;
  }
  else{
    auto det = fabs( pi6*mu*dx2_sum/aDdxdF_sum);  
    return (dt > alpha*det);
  }  
}

//############################################################################
void Force_Change_Measures::add_measure( Length a, Pos old_pos, Pos pos,
                                        F3 old_F, F3 F, Force& DdxdF_sum){
  
  auto ainv = 1.0/a;
  auto dx = pos - old_pos;
  dx2_sum += norm2( dx);
  DdxdF_sum += ainv*dot( F - old_F, dx);
}

//############################################################
void Force_Change_Measures::accumulate_measures( const System_State& state){
  
  Force DdxdF_sum( 0.0);
  auto& groups = state.groups;
  auto& gthreads = state.thread().groups;
  auto ng = groups.size();
  for( auto ig: range(ng)){
    auto& group = groups[ig];
    auto& gthread = gthreads[ig];
    accumulate_chain_measures( gthread, group, DdxdF_sum);
    accumulate_tet_measures( gthread, group, DdxdF_sum);  
  }      
  aDdxdF_sum = fabs( DdxdF_sum);    
}

//#########################################################################
void Force_Change_Measures::
  accumulate_tet_measures( const Group_Thread& gthread,
                          const Group_State& group, Force& DdxdF_sum){

  auto& saved = gthread.current;
  if (!saved.has_state){
    error( __FILE__, __LINE__, "no state");
  }
  
  auto& tets = group.tets;
  auto& old_tets = saved.tet_states;
  auto nt = tets.size();
  for( auto it: range(nt)){
    auto& tet = tets[it];
    auto& old_tet = old_tets[it];
    
    // forces with old positions; new positions
    Tet< Group_State>::Small_Tet_With_Forces tet0;
    {
      Vec3< Force> F( Zeroi{});
      for( auto& f: tet.forces){
        F += f;
      }
      
      Vec3< Torque> T( Zeroi{});
      auto cen = centroid( tet.poses);
      for( auto ia: range( Atom_Index( ntet))){
        auto& f = tet.forces[ia];
        auto pos = tet.poses[ia];
        auto t = cross( pos - cen, f);
        T += t;
      }
      
      tet0.poses = tet.poses;
      tet0.forces = tet_vertex_forces( tet.poses, F, T);
    }
    
    auto a = tet.fixed->radius;    
    for( auto ia: range( Atom_Index( ntet))){
      auto pos = tet0.poses[ia];
      auto F = tet0.forces[ia];
      auto old_pos = old_tet.poses[ia];
      auto old_F = old_tet.forces[ia];  
      add_measure( a, old_pos, pos, old_F, F, DdxdF_sum);
    }
  }    
}

//#####################################################################
void Force_Change_Measures::
  accumulate_chain_measures( const Group_Thread& gthread,
                            const Group_State& group,
                            Force& DdxdF_sum){
  
  auto& saved = gthread.current;
    
  Chain_Index ic( 0);
  for( auto& chain: group.chain_states){
    if (!chain.frozen){
      auto& old_chain = saved.chain_states[ic];
      ++ic;
      auto& atoms = chain.atoms;
      auto na = atoms.size();
      auto& old_atoms = old_chain.atoms;
      auto& common_atoms = chain.chain_ref.get().atoms;
      for( auto ia: range(na)){
        auto a = common_atoms[ia].radius;
        auto& atom = atoms[ia];
        auto& old_atom = old_atoms[ia];
        auto pos = atom.pos;
        auto old_pos = old_atom.pos;
        auto F = atom.force;
        auto old_F = old_atom.force;
        add_measure( a, old_pos, pos, old_F, F, DdxdF_sum);
      }
    }
  }
}

//####################################################################
/*
bool System_State::tet_constraints_not_far_off() const{
  
  for( auto& group: groups){
    auto& tets = group.tets;
    for( auto ti: range( tets.size())){
      auto& tet = tets[ti];
      if (!tet.constraints_not_far_off()){
        return false;
      }
    }
  }
  return true;  
}
*/

}

namespace{
  //##################################################
  bool has_cores( const System_Common& fcommon){
    bool res = false;
    for( auto& group: fcommon.groups){
      if (group.cores.size() > Core_Index(0)){
        res = true;
        break;
      }
    }
    return res;
  }
  
  //####################################################################
  bool is_angle_ok( const Group_Thread& gthread, const Group_State& group,
                          Bead_Index ib0, Bead_Index ib1){
    
    auto pos0 = bead_pos( group, ib0);
    auto opos0 = old_bead_pos( gthread, group, ib0);
    auto pos1 = bead_pos( group, ib1);
    auto opos1 = old_bead_pos( gthread, group, ib1);
    
    auto u = normed( pos1 - pos0);
    auto ou = normed( opos1 - opos0);
    
    auto angle = acos( dot( u, ou));
    constexpr double angle0 = angle_conv_factor*back_angle;
    
    return angle < angle0;
  }
  
  //##############################################################
  bool cm_length_constraint_ok( const Group_Thread& gthread,
                                 const Group_State& group,
                                Constraint_Index ic){
    
    auto [ib0,ib1] = group.lconstraint_beads( ic);
    return is_angle_ok( gthread, group, ib0, ib1);
  }
  
  void check_length_constraint_blowup( const Group_Thread& gthread,
                                  const Group_State& group,
                                 Constraint_Index ic){
     
     auto [ib0,ib1] = group.lconstraint_beads( ic);     
     auto pos0 = bead_pos( group, ib0);
     auto pos1 = bead_pos( group, ib1);
     auto r = distance( pos0, pos1);
     auto L = group.constraint_length( ic);
     if (r > 10.0*L){
       error( "length constraint blew up, need smaller dt");
     }
   }
  
  //####################################################################
  double coplanar_violation( const Group_State& group,
                               Constraint_Index ic){
    
    Array< Bead_Index, 4> beads = group.cconstraint_beads( ic);
    
    auto poses = beads.mapped( [&]( Bead_Index ib){
      return group.bead_position( ib);
    });
      
    // estimate distance from plane
    // pretend it is equilateral triangle with fourth point in center
    Length sumL( 0.0);
    for( auto i: range(4)){
      for( auto j: range(i)){
        sumL += distance( poses[i], poses[j]);
      }
    }
      
    auto pos0 = poses[0];  
    auto d1 = poses[1] - pos0;
    auto d2 = poses[2] - pos0;
    auto d3 = poses[3] - pos0;
    auto volume = fabs( dot( cross( d1,d2),d3))/6.0;
    auto h = sumL/(6 + 2*::sqrt(3.0)); // half triangle side
    auto H = volume/(::sqrt(3.0)*h*h); // height above plane
    return H/(2.0*h);
  }
  
  //########################################################
  void check_coplanar_constraint_blowup( const Group_State& group,
                                      Constraint_Index ic){
    
    if (coplanar_violation( group, ic) > 10.0){
      error( "coplanar constraint blew up");
    }
  }
  
  //########################################################
  bool coplanar_constraint_ok( const Group_State& group,
                                Constraint_Index ic){
  
    return coplanar_violation( group, ic) < constraint_backstep_tol;
  }
  
}
