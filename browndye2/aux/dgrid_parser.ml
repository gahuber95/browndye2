(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* used internally. Parses data from the "distance grid" output of
 "grid_distances". Visible function is "grid" (see header file)

*)

open Bigarray;;

type data3d =
    (float, float32_elt, c_layout)
    Array3.t

type t = {
  data: data3d; (* grid of distances *)
  spacing: float*float*float;    (* spacing between grid points *)
  corner: Vec3.t;                (* position of lowest grid point *)
}

module JP = Jam_xml_ml_pull_parser

let some_of opt = 
  match opt with
    | Some obj -> obj
    | None -> raise (Failure "some_of: nothing there")

let grid_from_parser parser = 

  let nx,ny,nz =
    let nnode = some_of (JP.node_of_tag parser "n") in
      JP.complete_current_node parser;
      let str = JP.node_stream nnode in 
      let nx = JP.int_from_stream str in
      let ny = JP.int_from_stream str in
      let nz = JP.int_from_stream str in
	nx,ny,nz
  in

  let spacing = 
    let snode = some_of (JP.node_of_tag parser "spacing") in
      JP.complete_current_node parser;
      let str = JP.node_stream snode in 
      let sx = JP.float_from_stream str in
      let sy = JP.float_from_stream str in
      let sz = JP.float_from_stream str in
	sx,sy,sz
  in
  let corner = 
    let cnode = some_of (JP.node_of_tag parser "corner") in
      JP.complete_current_node parser;
      let str = JP.node_stream cnode in 
      let cx = JP.float_from_stream str in
      let cy = JP.float_from_stream str in
      let cz = JP.float_from_stream str in
	Vec3.v3 cx cy cz
  in
    
    ignore (JP.node_of_tag parser "data");
    
    let data = Array3.create float32 C_layout nx ny nz 
(*
      Array.init nx
	(fun ix ->
	  Array.init ny 
	    (fun iy ->
	      Array.init nz (fun iz -> nan)
	    )
	)
 *)
    in
    let ix,iy,iz = ref 0, ref 0, ref 0 in
    let incs () = 
      let inc i = i := !i + 1 in
      let zero i = i := 0 in
	inc iz;
	if !iz = nz then (
	  zero iz;
	  inc iy;
	  if !iy = ny then (
	    zero iy;
	    inc ix;
	    if !ix = nx then
	      zero ix;
	  )
	)
    in


      JP.apply_to_nodes_of_tag parser "d"
	  (fun dnode ->
	    JP.complete_current_node parser;

	    let str = JP.node_stream dnode in
	    let get_d () = 
	      let d = JP.float_from_stream str in
		data.{!ix, !iy, !iz} <- d;
		incs()
	    in
	    let attr = JP.node_attributes dnode in
	    let n = 
	      if (List.length attr) > 0 then (
		let nstr = List.assoc "n" attr in 
		  if nstr = "1" then 1 
		  else if nstr = "2" then 2 
		  else if nstr = "3" then 3 
		  else raise (Failure "d tag has number other than 1 thru 3")
	      )
	      else 3
		
	    in
	      for i = 1 to n do
		get_d();
	      done;
	  )
      ;

      if not (!ix = 0 && !iy = 0 && !iz = 0) then
	raise (Failure "not enough data for array");    
      
      {
	data = data;
	spacing = spacing;
	corner = corner;
      }
	
let grid buf = 
  try
    let parser = JP.new_parser buf in
      grid_from_parser parser 

  with ex -> (
    Printf.fprintf stderr "Error parsing distance grid\n";
    raise ex
  )



;;

	
	      




