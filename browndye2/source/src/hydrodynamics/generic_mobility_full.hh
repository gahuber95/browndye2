#pragma once

#include <numeric>
#include <tuple>
#include <random>
#include "../lib/vector.hh"
#include "../lib/array.hh"
#include "../lib/range.hh"
#include "../lib/linalg.hh"
#include "rotne_prager.hh"
#include "../lib/inverse3x3.hh"
#include "../lib/solve3.hh"
#include "mobility_matrices.hh"
#include "mat3_division.hh"
#include "../lib/rotations.hh"

// different system can be used from that given to the constructor

namespace Browndye{
  
namespace Generic_Mobility_Full{

using std::move;
using std::get;

class Atom_Tag{};
//class Div_Tag{};

//#################################################
template< class I>
struct Doer{
  typedef typename I::Div_Index DIndex; // division
  typedef typename I::Bead_Index AIndex;
  typedef typename I::System System;
  typedef typename I::Length Length;
  typedef typename I::Time Time;
  
  typedef decltype( 1.0/Length()) Mob;
  typedef decltype( sqrt( Mob())) Sqrt_Mob;
  //typedef decltype( 1.0/Sqrt_Mob()) Inv_Sqrt_Mob;
  //typedef decltype( 1.0/Mob()) Inv_Mob;
  typedef Vec3< Length> Pos;
  typedef decltype( Length()*Length()) Len2;
  typedef decltype( Len2()*Length()) Len3;
  typedef decltype( 1.0/Length()) Inv_Len;
  
  Doer() = delete;
  Doer( const Doer&) = delete;
  Doer& operator=( const Doer&) = delete;
  Doer( Doer&&) = default;
  Doer& operator=( Doer&&) = default;
  Doer( const System&);

  class Blank{};

  [[maybe_unused]] Doer( Blank){};

  Doer copy() const;

  template< class F_Vec, class V_Vec>
  void
  add_product_with_mobility( const System& system, const F_Vec& fvec, V_Vec& vvec) const;
  
  template< class W_Info, class D_Vec>
  void add_stochastic_propagation( const System&, const W_Info&, D_Vec&) const;
    
  std::tuple< Mat3< Length>, Mat3< Len3>, Vec3< Length> >
  resistance_matrices_and_com( const System&) const;   
    
  void update(const System &);
    
  //void test_center( std::mt19937_64& gen) const;
    
//#########################################################################  
private:
  void resize( const System&);
  
  template< class T, class Idx>
  using GMatrix = Vector< Vector< Mat3<T>, Idx>, Idx>;
  
  template< class T, class Idx>
  using GVector = Vector< Vec3< T>, Idx>;
  
  // data
  class MBead_Tag{};
  typedef Index< MBead_Tag> BIndex;
  
  GMatrix< Mob, BIndex> coarse_M; // assume full storage
  GMatrix< Sqrt_Mob, BIndex> coarse_L;
    
  // private functions
  void compute_coarse_M( const System&);
  
  template< class T>
  static
  void resize_coarse( const System& system, GMatrix< T, BIndex>& M);
  
  template< class F_Info, class V_Info>
  void add_coarse_velocities( const System& system, const F_Info& finfo, V_Info& vinfo) const;
   
  template< class T, class Idx>
  static
  void do_cholesky_decomp( const GMatrix< T, Idx>&,
                              GMatrix< decltype( sqrt(T())), Idx>&);
  
  //template< class Lt, class Vt, class Idx>
  //static
  //auto solution( const GMatrix< Lt, Idx>& L,
  //              const GVector< Vt, Idx>& v);
  
  template< class M, class Index, class F, class V>
  static
  auto get_sym_product( const Vector< Vector< M, Index>, Index>&,
                         const Vector< F, Index>&,
                           Vector< V, Index>&); 
   
  template< class M, class V_Info,
            class Index, class D>
  void get_lower_and_diag_product( const System&,
                                     const GMatrix< M, Index>& m,
                                     const V_Info& vinfo,
                                     GVector< D, Index>& d) const;
  
  //static
  //auto centers_for_test( AIndex na, Length radius, Length side_len);
  
  //static
  //void rotate_and_shift( std::mt19937_64& gen, Pos pos, Vector< Pos, AIndex>& centers);
  
  //static
  //auto random_position( std::mt19937_64& gen, Length side_len);
  
  //static
  //auto rotne_prager_matrix( Length a0, Length a1, Pos cen0, Pos cen1);
  
  //static
  //auto mob_center( const Vector< Vector< Mat3< Sqrt_Mob>, AIndex>, AIndex>& L,
   //                 const Vector< Pos, AIndex>& centers, Length radius);
  
  static
  auto single_trans_matrix( Length a);
  
  class Copy_Tag{};
  Doer( const Doer& other, Copy_Tag);
  
  // used for Wiener process
  template< class Info>
  auto cf_quantity( Atom_Tag, const System& system,
                     const Info& info, DIndex id, AIndex ia) const{
    return I::quantity( system, info, id, ia);
  }

  /*
  template< class Info>
  auto cf_quantity(  Div_Tag, const System& system,
                     const Info& info, DIndex, DIndex id) const{
    return I::div_quantity( system, info, id);
  }
  */

  Vec3< Length> whole_center( const System&) const;
  Mat3< Len3> coarse_C_matrix( const System&, Vec3< Length> center) const;
  Mat3< Length> A_matrix() const;
  //Mat3< Len3> fine_C_matrix( const System&) const;
  
  template< class F>
  void do_loop( const System&, F&& f) const;
  
  template< class Foa, class Fi, class Fob>
  void do_double_loop( const System&, Foa&&, Fi&&, Fob&&) const;
};

//#####################################################
template< class I>
template< class F>
void Doer<I>::do_loop( const System& sys, F&& f) const{
  
  auto nd = I::n_divisions( sys);
  BIndex ib{ 0};
  for( auto id: range( nd)){
    auto na = I::n_beads( sys, id);
    for( auto ia: range( na)){
      f( id, ia, ib);
      ++ib;
    }
  }
}

//#####################################################
template< class I>
template< class Foa, class Fi, class Fob>
void Doer<I>::do_double_loop( const System& sys,
                               Foa&& foa, Fi&& fi, Fob&& fob) const{
  
  auto nd = I::n_divisions( sys);
  BIndex ib{ 0};
  for( auto id: range( nd)){
    auto nai = I::n_beads( sys, id);
    //cout << "id nai " << id << ' ' << nai << endl;
    for( auto ia: range( nai)){
      auto resi = foa( id, ia, ib);
      
      BIndex jb{ 0};
      for( auto jd: range( inced(id))){
        auto naj = (jd == id) ? inced( ia) : I::n_beads( sys, jd);
        for( auto ja: range( naj)){
          resi = fi( resi, jd,ja,jb);
          ++jb;
        }
      }
      
      fob( resi);
      ++ib;
    }
  }
}

//#########################################################################
// constructor
template< class I>
Doer<I>::Doer( const Doer& other, Copy_Tag){
  
  coarse_L = other.coarse_L.copy();
  coarse_M = other.coarse_M.copy();
}

template< class I>
Doer<I> Doer<I>::copy() const{
  return Doer( *this, Copy_Tag());
}



//#########################################################################
template< class I>
template< class F_Info, class V_Info>
void
Doer<I>::add_product_with_mobility( const System& system,
                                       const F_Info& finfo, V_Info& vinfo) const{
  
  add_coarse_velocities( system, finfo, vinfo);
}

//##########################################################################
// ensure that W_Info has the sqrt(2) factor already in it!
// (change later)
template< class I>
template< class W_Info, class D_Vec>
void Doer<I>::add_stochastic_propagation( const System& sys, const W_Info& winfo, D_Vec& dvec) const{
   
  //auto nd = I::n_divisions( sys);
 
  auto nb = coarse_M.size();      
  GVector< Length, BIndex> dx( nb);  
  get_lower_and_diag_product( sys, coarse_L, winfo, dx);  
  
  do_loop( sys, [&]( DIndex id, AIndex ia, BIndex ib){
    auto dxd = dx[ib];
    I::add_quantity( sys, dvec, id, ia, dxd);
  });

}

//##########################################################
template< class Mt, class Idx>
class I_Mat_Cholesky{
public:
  typedef decltype( sqrt( Mt())) Lt;
     
  template< class T>
  using GMatrix = Vector< Vector< Mat3<T>, Idx>, Idx>;
  
  typedef GMatrix< Mt> Matrix;
  typedef GMatrix< Lt> Dec_Matrix;
  typedef Mat3<Mt> Matrix_Value;
  typedef Mat3<Lt> Dec_Matrix_Value;
  typedef Idx Index;
  
  template< class T>
  static
  auto element( const GMatrix< T>& M, Idx i, Idx j){
    return M[i][j];
  }
  
  template< class T>
  static
  auto& element_ref( GMatrix< T>& M, Index i, Index j){
    return M[i][j];
  }
  
  template< class T>
  static
  Mat3< T> transpose( Mat3< T> m){
    return Browndye::transpose( m);
  }
  
  template< class T>
  static
  Index size( const GMatrix< T>& M){
    return M.size();
  }
    
  template< class T>
  class Vector_Type{
  public:
    typedef Vector< Vec3< T>, Idx> Result;
  };
   
  template< class T>
  static
  Vec3<T> element( const Vector< Vec3<T>, Idx>& v, Index i){
    return v[i];
  }
  
  template< class T>
  static
  Vec3<T>& element_ref( Vector< Vec3<T>, Idx>& v, Index i){
    return v[i];
  }
  
  template< class MV>
  static
  MV zero(){
    return MV( Zeroi{});
  }
  
  template< class T>
  static
  auto determinant( Mat3< T> M){
    auto a = M[0][0];
    auto b = M[0][1];
    auto c = M[0][2];
    auto d = M[1][0];
    auto e = M[1][1];
    auto f = M[1][2];
    auto g = M[2][0];
    auto h = M[2][1];
    auto i = M[2][2];
    
    return a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h;
  }
  
  template< class T>
  static
  bool is_negative( Mat3<T> M){
    typedef decltype( T()*T()*T()) T3;
    auto det = determinant( M);  
    return det < T3( 0.0);
  }
  
  template< class T>
  static
  bool is_zero( Mat3<T> M){
    typedef decltype( T()*T()*T()) T3;
    auto det = determinant( M);  
    return det == T3( 0.0);
  }
  
  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }
};

//###################################################################
template< class I>
auto Doer<I>::whole_center( const System& sys) const -> Vec3< Length> {
 
  auto nd = I::n_divisions( sys);
  Vec3< Len2> c_sum( Zeroi{});
  Length a_sum{ 0.0};
  for( auto id: range( nd)){
    auto na = I::n_beads( sys, id);
    for( auto ia: range( na)){
      auto a = I::bead_radius( sys, id, ia);
      auto cen = I::bead_center( sys, id, ia);
      a_sum += a;
      c_sum += a*cen;
    }
  }
  return (1.0/a_sum)*c_sum;  
}


//###############################################################
template< class I>
auto Doer<I>::coarse_C_matrix( const System& sys,
                                Vec3< Length> center) const -> Mat3< Len3>{
  
  typedef decltype( 1.0/Len2{}) Vel;
  typedef decltype( 1.0/Len3()) Om;  
  
  auto nb = coarse_L.size();
  GVector< Vel, BIndex> vs( nb);
  GVector< Inv_Len, BIndex> cforce( nb);

  Mat3< Len3> C;     

  for( int k: range(3)){
    
    do_loop( sys, [&]( DIndex id, AIndex ia, BIndex ib){
        Vec3< Om> omega( Zeroi{});
        omega[k] = Om( 1.0);
        auto bcen = I::bead_center( sys, id, ia);
        vs[ib] = cross( omega, (bcen - center));
      }
    );
    
    Cholesky::solve< I_Mat_Cholesky< Mob, BIndex>>( coarse_L, vs, cforce);
    
    Vec3< double> Tsum( Zeroi{});
   
    do_loop( sys, [&]( DIndex id, AIndex ia, BIndex ib){
      auto f = cforce[ib];
      auto bcen = I::bead_center( sys, id, ia);
      auto d = bcen - center;
      Tsum += cross( d, f);
    });
    
    C[k] = (1.0/Om(1.0))*Tsum;
  }
  return C;
}

//##########################################################
template< class I>
auto Doer<I>::A_matrix() const -> Mat3< Length>{
  
  auto nb = coarse_L.size();
  GVector< Inv_Len, BIndex> cforce( nb);
  
  typedef decltype( 1.0/Len2{}) Vel;
  Mat3< Length> A;
  GVector< Vel, BIndex> vs( nb);
  for( int k: range(3)){
    for( auto& v: vs){
      v = Vec3< Vel>( Zeroi{});
      v[k] = Vel( 1.0);
    }
    
    Cholesky::solve< I_Mat_Cholesky< Mob, BIndex>>( coarse_L, vs, cforce);
    
    Vec3< Inv_Len> Fsum( Zeroi{});
    for( auto& f: cforce){
      Fsum += f;
    }
    A[k] = (1.0/Vel(1.0))*Fsum;
  }
  return A;
}
//###################################################################
template< class I>
auto Doer<I>::resistance_matrices_and_com( const System& sys) const ->
  std::tuple< Mat3< Length>, Mat3< Len3>, Vec3< Length> >{
    
  // scaled units: F -> 1/L, v -> L^-2  
  auto A = A_matrix();  
  auto center = whole_center( sys);  
  auto C = coarse_C_matrix( sys, center);    
  return std::make_tuple( A, C, center);    
}


//#########################################
// constructor
template< class I>
Doer<I>::Doer( const System& system){

  resize( system);
}

//###############################################################
template< class I>
void Doer<I>::compute_coarse_M( const System& sys){
  //cout << "compute coarse M" << endl;
  /*
  auto nd = I::n_divisions( system);
  BIndex ib{ 0};
  for( auto id: range( nd)){
    auto nai = I::n_beads( system, id);

    for( auto ia: range( nai)){
      
      auto ceni = I::bead_center( system, id, ia);
      auto ai = I::bead_radius( system, id, ia);
      
      BIndex jb{ 0};
      for( auto jd: range( inced( id))){
        auto naj = (jd == id) ? inced( ia) : I::n_beads( system, jd);       
      
        for( auto ja: range( naj)){

          if (jb > ib){ // debug
            error( __FILE__, __LINE__, "not get here");
          }

          if (ib == jb){
            coarse_M[ib][jb] = single_trans_matrix( ai);
          }
          else{
            auto aj = I::bead_radius( system, jd, ja);
            auto cenj = I::bead_center( system, jd, ja);
            auto d = cenj - ceni;
            
            auto M = Rotne_Prager::trans_matrix( ai, aj, d);
            coarse_M[ib][jb] = M;
          }
          ++jb;
        }
      }
      ++ib;
    }
  }
  */
  
  auto foa = [&]( DIndex id, AIndex ia, BIndex ib){
    auto ceni = I::bead_center( sys, id, ia);
    auto ai = I::bead_radius( sys, id, ia);
    return std::make_tuple( ib, ceni, ai);
  };
  
  auto fi = [&]( auto data, DIndex jd, AIndex ja, BIndex jb){
    
    auto [ib,ceni,ai] = data;
    
    if (ib == jb){
       coarse_M[ib][jb] = single_trans_matrix( ai);
    }
    else{
      auto aj = I::bead_radius( sys, jd, ja);
      auto cenj = I::bead_center( sys, jd, ja);
      auto d = cenj - ceni;
      
      auto M = Rotne_Prager::trans_matrix( ai, aj, d);
      //typedef std::decay_t< decltype( coarse_M[ib][jb])> Mt;
      //Mt M( Zeroi{});
      coarse_M[ib][jb] = M;
    }
    
    return data;
  };

  auto fob = [&]( auto& dum){};

  do_double_loop( sys, foa, fi, fob);
  //cout << "end compute coarse M" << endl;
}


//#################################################################
template< class I>
    void
Doer<I>::update(const System &sys) {
  
  compute_coarse_M( sys);  
  do_cholesky_decomp( coarse_M, coarse_L);  
}

//#########################################################
/*
template< class I>
auto Doer<I>::centers_for_test( AIndex na, Length radius, Length side_len){
  
  Vector< Pos, AIndex> centers( na);
      
  AIndex i( 0);
  for( auto kx: range( 2))
    for( auto ky: range( 2))
      for( auto kz: range( 2)){
        auto x = (0.5 - (double)kx)*side_len;
        auto y = (0.5 - (double)ky)*side_len;
        auto z = (0.5 - (double)kz)*side_len;
        auto& cen = centers[i];
        cen[0] = x;
        cen[1] = y;
        cen[2] = z;
        ++i;
      }
   
  return centers;
}

//#############################################################################
template< class I>
auto Doer<I>::random_position( std::mt19937_64& gen, Length side_len){
  
  auto blen = 5.0;
  std::uniform_real_distribution< double> uni( -blen, blen);
  Pos pos;
  for( auto k: range(3))
     pos[k] = side_len*( uni( gen));
  return pos;
}

//########################################################################
class Rot_Interface{
public:
  typedef std::mt19937_64 Random_Number_Generator;
  
  static
  double gaussian( std::mt19937_64& gen){
    std::normal_distribution< double> gauss;
    return gauss( gen);
  }
};

//############################################################################################
template< class I>
void Doer<I>::rotate_and_shift( std::mt19937_64& gen, Pos pos, Vector< Pos, AIndex>& centers){
 
  auto rot = random_rotation< Rot_Interface>( gen);
  for( auto& center: centers)
    center = rot*center;
    
  for( auto& center: centers)
    center += pos;
}
*/

//#############################################################################
template< class I>
template< class F_Info, class V_Info>
void Doer<I>::add_coarse_velocities( const System& sys,
                                       const F_Info& finfo, V_Info& vinfo) const{
  
  //auto nd = I::n_divisions( sys);
  typedef decltype( I::quantity( sys, finfo, I::n_divisions( sys), AIndex{})) Ft;
  
  auto nb = coarse_M.size();
  Vector< Ft, BIndex> Fs( nb);
  
  do_loop( sys, [&]( DIndex id, AIndex ia, BIndex ib){
    Fs[ib] = I::quantity( sys, finfo, id, ia);
  });
  
  
  typedef decltype( coarse_M[ BIndex{}][ BIndex{}]) Mt;
  typedef decltype( Mt{}*Ft{}) Vt;
  Vector< Vt, BIndex> vs( nb);
  
  get_sym_product( coarse_M, Fs, vs);
  
  do_loop( sys, [&]( DIndex id, AIndex ia, BIndex ib){
    auto v = vs[ib];
    I::add_quantity( sys, vinfo, id, ia, v);
  });
  
}

//########################################################################
template< class I>
template< class M, class Index, class F, class V>
auto Doer<I>::get_sym_product( const Vector< Vector< M, Index>, Index>& m,
               const Vector< F, Index>& f, Vector< V, Index>& v){
 
  auto n = m.size();
  for( auto i: range( n)){
    V sum( Zeroi{});
    for( auto j: range( i)){  
      sum += m[i][j]*f[j];
    }
    for( auto j: range( i, n)){
      sum += transpose( m[j][i])*f[j];
    }
    v[i] = sum;
  }
}

//########################################################################
template< class I>
template< class M, class V_Info, class Index, class D>
 void Doer<I>::get_lower_and_diag_product( 
                                    const System& sys,      
                                    const GMatrix< M, Index>& m,
                                    const V_Info& vinfo,
                                    GVector< D, Index>& d) const{  
  
  /*
  auto nd = I::n_divisions( system);
  BIndex ib{ 0};
  for( auto id: range( nd)){
    auto nai = I::n_beads( system, id);
    for( auto ia: range( nai)){
      Vec3< D> sum( Zeroi{});
      BIndex jb{ 0};
      for( auto jd: range( inced( id))){
        auto naj =  (jd == id) ? inced( ia) : I::n_beads( system, jd);
        for( auto ja: range( naj)){
          auto subv = cf_quantity( Atom_Tag{}, system, vinfo, jd, ja);      
          sum += m[ib][jb]*subv;
          ++jb;
        }
      }
      d[ib] = sum;
      ++ib;
    }
  }
  */
  
  auto foa = [&]( DIndex id, AIndex ia, BIndex ib){
    Vec3< D> sum( Zeroi{});
    return std::make_tuple( ib, sum);
  };
  
  auto fi = [&]( auto data, DIndex jd, AIndex ja, BIndex jb){
    auto [ib,sum] = data;
    auto subv = cf_quantity( Atom_Tag{}, sys, vinfo, jd, ja);      
    auto new_sum = sum + m[ib][jb]*subv;
    return std::make_tuple( ib, new_sum);
  };
  
  auto fob = [&]( auto data){
    auto [ib,sum] = data;
    d[ib] = sum;
  };
  
  do_double_loop( sys, foa, fi, fob);
}

//####################################################################
// L is cholesky decomp of actual matrix

/*
template< class I>
template< class Lt, class Bt, class Idx>
auto Doer<I>::solution( const GMatrix< Lt, Idx>& L,
                       const GVector< Bt, Idx>& b){

  auto n = b.size();

  typedef decltype( Lt()*Lt()) Mt;
  typedef decltype( Bt()/Mt()) Xt;
  
  GVector< Xt, Idx> x( n);
  
  Cholesky::solve< I_Mat_Cholesky< Mt, Idx> >( L, b, x);
  return x;
}
*/
//####################################################################
template< class I>
void Doer< I>::resize( const typename I::System& system){
    
  resize_coarse( system, coarse_M);
  resize_coarse( system, coarse_L);
}

//###############################################################################
template< class I>
template< class T>
void Doer< I>::resize_coarse( const System& system, GMatrix< T, BIndex>& M){
  
  auto nb = [&]{
    BIndex ib{ 0};
    auto nd = I::n_divisions( system);
    for( auto id: range( nd)){
      (void)id;
      auto na = I::n_beads( system, id);
      ib += BIndex( na/AIndex(1)) - BIndex{0};
    }
    return ib;
  }();
  

  M.resize( nb);
  for( auto ib: range( nb)){
    M[ib].resize( inced( ib));
  }
}

//################################################################
template< class I>
auto Doer<I>::single_trans_matrix( Length a){
  
  if (a > Length{0.0}){
    return Rotne_Prager::single_t_mobility( a)*id_matrix<3>();
  }
  else{
    return Inv_Len( large)*id_matrix< 3>();
  }
}

//##################################################################
template< class I>
template< class T, class Idx>
void Doer<I>::do_cholesky_decomp( const GMatrix< T, Idx>& M,
                            GMatrix< decltype( sqrt(T())), Idx>& L){

  bool success;    
  Cholesky::decompose< I_Mat_Cholesky< T, Idx> >(M, L, success);
  if (!success){
    error( "Cholesky failed", __LINE__);
  }
}
}
}


