
#include <algorithm>
#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
#include "../input_output/get_args.hh"
#include "../lib/vector.hh"
#include "../global/pos.hh"
#include "../lib/slist.hh"
#include "coffdrop.hh"

using std::string;
using std::pair;
using std::cout;
using std::map;
using std::plus;
using std::get;

namespace Browndye{

struct Bead_Info{
  string bead, residue;
  SList< string> atoms;

  typedef std::tuple< const string&, const string&,
		      const SList< string>&> Rep;

  Rep rep() const{
    return std::make_tuple( bead, residue, atoms);
  }

  bool operator==( const Bead_Info& other) const{
    return rep() == other.rep();
  }
};

struct LResidue{
  string name;
  Residue_Index number;
  SList< CAtom> atoms;

  typedef std::tuple< const string&, 
		      Residue_Index, SList< CAtom> > Rep;

  Rep rep() const{
    return std::make_tuple( name, number, atoms);
  }

  bool operator==( const LResidue& other) const{
    return rep() == other.rep();
  }
};

typedef map< string, SList< Bead_Info> > Bead_Atom_Map;
typedef SList< LResidue > LResidues;

typedef map< pair< string, string>, Charge> Charge_Map;

//############################################
void print_pos( Array< Length, 3> pos){
  cout << "      <x> " << pos[0] << " </x>\n";
  cout << "      <y> " << pos[1] << " </y>\n";
  cout << "      <z> " << pos[2] << " </z>\n";  
}

void main0( int argc, char* argv[]){
  
  double radius;
  
  //##################################
  auto bead_atom_map = []( const string& map_file){
  
    // residue, (bead, (atoms...))...
    Bead_Atom_Map dict; 
    XML_Info xinfo( map_file);
    auto res_nodes = xinfo.top->children_of_tag( "residue");
    for( auto res_node: res_nodes){
      auto rname = checked_value_from_node< string>( res_node, "name");
      auto bead_nodes = res_node->children_of_tag( "bead");
      SList< Bead_Info> binfos;
      for( auto bead_node: bead_nodes){
        Bead_Info binfo;
        
        binfo.bead = checked_value_from_node< string>( bead_node, "name");
        binfo.atoms =
          slist_from_vector(
            checked_vector_from_node< string>( bead_node, "atoms"));
        binfo.residue = rname;
        
        binfos = appended( binfo, binfos);
      }
      dict[ rname] = move( binfos);
    }
    
    return dict;  
  };
  
  //##################################
  auto joined_residues = []( const LResidue& res0, const LResidue& res1){
    LResidue res;
    res.name = res1.name;
    res.number = res1.number;
    res.atoms = concatenated( res0.atoms, res1.atoms);
    return res;
  };
  
  //##################################
  auto with_atom_removed = []( const string& aname, SList< CAtom> atoms){
    return filtered(
      [&]( const CAtom& atom){return !(atom.name == aname);}, atoms);
  };
  
  //#####################################
  auto with_atoms_removed = [&]( SList< string> anames, SList< CAtom> atoms){
    
    auto watr = [&]( SList< CAtom> atoms, const string& aname){
      return with_atom_removed( aname, atoms);
    };
    
    return folded( watr, anames, atoms);
  };
  
  //#####################################
  auto with_atoms_removed2 = [&]( const string& name0, const string& name1, SList< CAtom> atoms){
    return with_atoms_removed( slist( name0, name1), atoms);
  };
  
  //##################################
  auto uncapped_N = [&]( LResidues cresidues){
    
    auto& res0 = head( cresidues);
    if (res0.name == "ACE"){
      auto tcres = tail( cresidues);
      auto& res1 = head( tcres);
      auto res01a = joined_residues( res0, res1);
      auto res01 = res01a;
      res01.atoms = with_atom_removed( "N", res01a.atoms);
      
      auto ttcres = tail( tcres);
      return appended( res01, ttcres);
    }
    else
      return cresidues;
  };
  
  //##################################
  auto uncapped_C = [&]( LResidues rcresidues){
    
    auto cresidues = reversed( rcresidues);
    
    auto& res0 = head( cresidues);
    if (res0.name == "NME"){
      auto tcres = tail( cresidues);
      auto& res1 = head( tcres);
      auto res01a = joined_residues( res0, res1);
      auto res01 = res01a;
      res01.atoms = with_atom_removed( "O", res01a.atoms);
      
      auto ttcres = tail( tcres);
      return reversed( appended( res01, ttcres));
    }
    else
      return rcresidues;
  };
  
  //##################################
  auto uncapped = [&]( LResidues residues){
    return uncapped_C( uncapped_N( residues));
  };

  //##################################
  auto lresidues = []( Vector< Residue, Residue_Index>&& reses){
    auto lreses0 = slist_from_vector( move(reses));
    
    auto r_to_lr = []( const Residue& res){
      LResidue lres;
      lres.name = res.name;
      lres.number = res.number;
      lres.atoms = slist_from_vector( res.atoms);
      return lres;
    };
    
    return mapped( r_to_lr, lreses0);
    
  };

  //############################################################################
  auto charges_map = []( const string& charge_file) -> Charge_Map{
    
    Charge_Map charges;
    XML_Info xinfo( charge_file);
    auto charge_nodes = xinfo.top->children_of_tag( "charge");
    for( auto charge_node: charge_nodes){
      auto residue = checked_value_from_node< string>( charge_node, "residue");
      auto atom = checked_value_from_node< string>( charge_node, "atom");
      auto q = checked_value_from_node< Charge>( charge_node, "value");
      auto key = make_pair( residue, atom);
      charges.insert( make_pair( key, q));
    }
    return charges;
  };

  //#################################################################
  auto has_atom = []( const string& aname, SList< CAtom> atoms) -> bool{
    
    auto has_name = [&]( const CAtom& atom) -> bool{
      return atom.name == aname;
    };
    
    return contains( has_name, atoms);
  };
  
  //##################################
  auto adjusted_bead_N_residue = [&]( const LResidue& res0){
    auto res = res0;
    auto atoms1 = with_atoms_removed2( "OC", "CC", res.atoms);
    res.atoms = has_atom( "CN", atoms1) ? with_atom_removed( "NN", atoms1) : atoms1;
    return res;
  };  

  //##################################
  auto adjusted_bead_C_residue = [&]( const LResidue& res0){
    auto res = res0;
    auto atoms1 = with_atoms_removed2( "NN", "CN", res.atoms);
    res.atoms = has_atom( "CC", atoms1) ? with_atom_removed( "OC", atoms1): atoms1;
    return res;
  };
  
  //##############################################
  auto with_adjusted_caps = [&]( LResidues reses){
    auto new_n = adjusted_bead_N_residue( head( reses));
    auto rreses = reversed( reses);
    auto new_c = adjusted_bead_C_residue( head( rreses));
    
    auto new_rreses = appended( new_c, tail( rreses));
    
    return appended( new_n, tail( reversed( new_rreses)));    
  };
  
  //####################################################
  auto with_nonterminals_removed = [&]( LResidues reses){
    
    auto nend = head( reses);
    auto rreses = reversed( tail( reses));
    auto cend = head( rreses);
    auto rmiddle = tail( rreses);
    
    auto removed = [&]( const LResidue& res0){
      auto res = res0;
      res.atoms = with_atoms_removed2( "OC", "NN", res0.atoms);
      return res;
    };
    
    auto cleaned_rmiddle = mapped( removed, rmiddle);
    return appended( nend, reversed( appended( cend, cleaned_rmiddle)));
  };
  
  
  //##################################################################################################
  auto residues_with_bead_numbers = [&]( LResidues residues){
    
    using namespace std::placeholders;
    
    auto bead_sizes = mapped( []( const LResidue& residue){ return size( residue.atoms);},
                             residues);
    
    auto bead_starts = reversed( tail( reversed( appended( 0, accumulated( plus<>(), bead_sizes)))));
    
    auto res_bead_nums = 
      mapped(
           []( auto binfo){
             auto [ist, n] = binfo;
             return mapped( bind( plus<int>(), ist, _1), iota(n));
           },
           zipped( bead_starts, bead_sizes));
    
    auto beads_with_nums = []( SList< CAtom> beads, SList< int> nums){
      return mapped( []( auto bn){
        auto [bead,num] = bn;
        auto beadn = bead;
        beadn.number = Atom_Index(num);
        return beadn;
      }, zipped( beads, nums));
    };
    
    return mapped(
      [&]( auto& resnums){
        auto& [residue0,nums] = resnums;
        auto residue = residue0;
        residue.atoms = beads_with_nums( residue.atoms, nums);
        return residue;
      },
      zipped( residues, res_bead_nums));
    
  };
  
  //##################################################################################################
  
  auto bead_residues = [&]( LResidues areses, const Bead_Atom_Map& ba_map, const Charge_Map& charges) -> LResidues{
    
    //##################################################
    auto bead_residue = [&]( LResidue ares) -> LResidue{
      
      auto rname = ares.name;
      const auto& bead_infos =
        [&]{
            try{
               return ba_map.at( rname);
             }
             catch( std::out_of_range& exc){
               error( "residue name ", rname, "not defined");
               return slist< Bead_Info>();
             }
          }();
      
      //###############################################################
      auto ats_of_binfo = [&]( const Bead_Info& binfo) -> SList< CAtom>{
        
        auto hasat = [&]( const CAtom& aatom) -> bool{
          return contains( [&]( const string& bname){ return bname == aatom.name;}, binfo.atoms);
        };
        
        return filtered( hasat, ares.atoms);
      };
      
      auto atoms_by_bead = mapped( ats_of_binfo, bead_infos);
      
      auto [nz_bead_infos, nz_atoms_by_bead] = 
        unzipped( filtered( [](auto ba){
          return !is_empty( get<1>( ba));},
          zipped( bead_infos, atoms_by_bead)));
        
      
      //###################################################
      auto bead_position = []( SList< CAtom> atoms) -> Pos{
        
        auto pos_sum = []( Pos pos, const CAtom& atom1){
          return pos + atom1.pos;
        };
        
        return folded( pos_sum, atoms, Pos( Zeroi{}))/((double)size( atoms));
      };
      
      auto bpositions = mapped( bead_position, nz_atoms_by_bead);
      
      //#######################################################
      auto bead_from_info_pos = [&]( const auto& info) -> CAtom{
        auto [binfo, pos] = info;
        CAtom bead;
        bead.name = binfo.bead;
        auto ckey = make_pair( binfo.residue, binfo.bead);
        
        auto citer = charges.find( ckey);
        bead.charge = (citer == charges.end()) ? Charge(0.0) : citer->second;
        
        bead.pos = pos;
        return bead;
      };
      
      auto bres = ares;
      bres.atoms = mapped( bead_from_info_pos,  zipped( nz_bead_infos, bpositions));
      return bres;
    };
    
    return residues_with_bead_numbers(
              with_nonterminals_removed(                         
                with_adjusted_caps( mapped( bead_residue, areses))));
  };
  
  //##############################################################
  auto print_residue_header = [&]( const LResidue& residue){
    cout << "  <residue>\n";
    cout << "    <name> " << residue.name << " </name>\n";
    cout << "    <number> " << residue.number << " </number>\n"; 
  };
  
  //###########################################
  auto print_bead = [&]( const CAtom& bead){
    cout << "    <atom>\n";
    cout << "      <name> " << bead.name << " </name>\n";
    cout << "      <number> " << bead.number << " </number>\n";
    cout << "      <charge> " << bead.charge << " </charge>\n";
    print_pos( bead.pos);
    cout << "      <radius> " << radius << " </radius>\n";
    cout << "    </atom>\n";
  };
  
  //#################################################################
  auto print = [&]( LResidues residues){
            
    cout << "<top>\n";
    for( auto& residue: residues){
      print_residue_header( residue);  
      iterate( print_bead, residue.atoms);   
      cout << "  </residue>\n";
    }
    cout << "</top>\n"; 
  };
  
  //##################################
  auto top = [&](){
    string help( "outputs COFFDROP beads.\n -map: mapping file from atoms to beads\n -pqr: file of original atoms in XML format\n -charges: COFFDROP charge file\n -radius: bead radius in A (default is 3.43 from original model)");
 
    print_help( argc, argv, help);
  
    auto map_file = string_arg( argc, argv, "-map");
    auto atom_file = string_arg( argc, argv, "-pqr");
    auto charge_file = string_arg( argc, argv, "-charges");
  
    radius = double_arg( argc, argv, "-radius"); // COFFDROP paper has it as 3.43 A
    if (radius != radius)
      radius = 3.43;
  
    auto ba_map = bead_atom_map( map_file); 
    auto residues = uncapped( lresidues( residues_of_file( atom_file)));
    
    auto charges = charges_map( charge_file);  
    auto bead_reses = bead_residues( residues, ba_map, charges);
    
    print( bead_reses);
  };
  
  //#################################
  top();
  
}}
 
int main(int argc, char **argv){
  
  Browndye::main0( argc, argv);
}
 
