#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute
   See the file COPYRIGHT for copying permission
*/



#include <cmath>
#include "../lib/vector.hh"
#include "../global/limits.hh"

namespace Browndye {

  namespace Conjugate_Gradient_Minimize {

    // golden section search
    template<class Func, class X>
    auto one_d_minimum(Func &&f, X scale, X tol, X x) -> X {

      const auto xm = x;
      const auto fm = f(xm);
      auto best_f = fm;
      auto best_x = xm;
      
      auto improve = [&]( auto f, auto x){
        if (f < best_f){
          best_f = f;
          best_x = x;
        }
      };
      
      auto x1 = x - scale;
      auto f1 = f(x1);
      improve( f1, x1);
      
      auto x4 = x + scale;
      auto f4 = f(x4);
      improve( f4, x4);

      while (fm >= f1) {
        x1 -= scale;
        f1 = f(x1);
        improve( f1, x1);
      }
      while (fm >= f4) {
        x4 += scale;
        f4 = f(x4);
        improve( f4, x4);
      }

      const auto phi = (1 + sqrt(5.0)) / 2;
      const auto r = phi - 1; // 0.61
      const auto c = 1 - r; // 0.39

      auto x2 = r * x1 + c * x4;
      auto x3 = r * x4 + c * x1;
      auto f2 = f(x2);
      auto f3 = f(x3);



      auto res = [&]{
        
        decltype( f2) min_f{ large};
        while (x4 - x1 > tol || min_f > best_f) {
          min_f = std::min(std::min(std::min(f1, f2), f3), f4);
          if (min_f == f1 || min_f == f2) {
            improve( f1,x1);
            improve( f2,x2);
            
            x4 = x3;
            f4 = f3;
            x3 = x2;
            f3 = f2;
            x2 = r * x3 + c * x1;
            f2 = f(x2);
          }
          else {
            improve( f3,x3);
            improve( f4,x4);
            
            x1 = x2;
            f1 = f2;
            x2 = x3;
            f2 = f3;
            x3 = r * x2 + c * x4;
            f3 = f(x3);
          }
        }
  
        if (f2 < f3) {
          //cout << "linem " << f2 << endl;
          return x2;
        }
        else{
          //cout << "linem " << f3 << endl;
          return x3;
        }
      }();
    
    
      return res;
    }


    //#################################################################################################
    // assume objective function is dimensionless
    // uses Fletcher-Reeves method
    template<class I>
    void minimize( typename I::Func &f, typename I::X scale, typename I::X tol, Vector<typename I::X> &x) {

      typedef typename I::X X;
      typedef double F;
      typedef decltype(F() / X()) Grad;
      typedef decltype(X() / Grad()) A;
      auto n = x.size();
      Vector<Grad> grad(n), prev_grad(n);
      Vector<X> next_x(n);
      Vector<Grad> s(n);

      auto fline = [&](A a) {
        for (auto i: range(n)) {
          next_x[i] = x[i] + a*s[i];
        }
        return I::value(f, next_x);
      };

      I::get_gradient(f, x, grad);
      auto gden = norm2(grad);
      for (auto i: range(n)) {
        s[i] = -grad[i];
      }

      auto g = sqrt( gden);

      auto aof = [&]() { 
        return one_d_minimum( fline, scale/g, tol/g, A{0.0}); 
      };

      auto a = aof();

      for (auto i: range(n)) {
        x[i] += a * s[i];
      }

      //size_t iter = 0; // debug
      while (true) {
        //std::cout << "iter " << iter << " " << I::value( f, x) << std::endl;
        //++iter;

        prev_grad.copy_from( grad);
        I::get_gradient(f, x, grad);

        typedef decltype( Grad()*Grad()) G2;
        G2 gnum{ 0.0};
        for( auto i : range(n)){ // Polak-Ribiere
          gnum += grad[i]*(grad[i] - prev_grad[i]);
        }
        if (gnum < G2(0.0)){
          gnum = G2( 0.0);
        }

        auto beta = gnum / gden;

        for (auto i: range(n)) {
          s[i] = beta * s[i] - grad[i];
        }

        gden = norm2( grad);
        g = sqrt( gden);

        a = aof();

        for (auto i: range(n)) {
          x[i] += a * s[i];
        }

        if (g * scale * scale < tol) {
          break;
        }
      }
    }
  }
}
