#include <iostream>
#include <sstream>
#include "../lib/vector.hh"


using namespace Browndye;

double sq( double a){
  return a*a;
}

//#################################################
struct Info{
  Vector< double> autoc;
  double mean, sdev;
};

Info autocor( const Vector< double>& xs){
  
  auto n = xs.size();
  
  auto sum = xs.folded_left( []( double sum, double x){
    return sum + x;
  }, 0.0);
  
  auto mean = sum/n;
  
  auto sum2 = xs.folded_left( [&]( double sum, double x){
    return sum + sq( x - mean);
  }, 0.0);
    
  auto sig2 = sum2/n;
  
  Info info;
  info.mean = mean;
  info.sdev = sqrt( sig2);
  
  auto& res = info.autoc;
  res.resize( n/2);
  for( auto i: range( n/2)){
    double sum = 0.0;
    for( auto j: range( n-i)){
      sum += (xs[j] - mean)*(xs[j+i] - mean);
    }
    auto ac = sum/(n-i);
    res[i] = ac/sig2;
  }
   
  return info; 
}

//##########################################
int main( int argc, char* argv[]){
  
   size_t nc = 1;
   
   if (argc == 2){
    std::string arg = argv[1];
    if (arg == "-help" || arg == "--help"){
      std::cout <<
        "reads in text file of two or more columns (time and quantities); "
        "outputs autocorrelation as function of time. Works only "
        "for evenly spaced time steps. Also outputs mean and standard "
        "deviation at the top of the output. An optional argument gives "
        "the column (other than time); default is 1\n";
      return 0;
    }
    else{
      auto n = atoi( argv[1]);
      nc = n;
    }
  }
  else if (argc > 2){
    std::cerr << "too many arguments\n";
    return 1;
  }
  
  Vector< double> xs, ts;
  
  while (true){
    double t,x;
       
    std::string line;
    std::getline( std::cin, line);
    std::stringstream sstr( line);
    
    sstr >> t;   
       
    for( auto i: range( 0,nc)){
      (void)i;
      sstr >> x;
    }
    
    if (std::cin.eof()){
      break;
    }
    else{
      ts.push_back( t);
      xs.push_back( x);
    }
  }
  
  auto info = autocor( xs);
  auto& ac = info.autoc;
  auto na = ac.size();
  std::cout << "# mean " << info.mean
            << " sdev " << info.sdev << std::endl;
  for( auto i: range( na)){
    std::cout << ts[i] << ' ' << ac[i] << std::endl;
  }
}

