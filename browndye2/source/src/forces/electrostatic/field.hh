#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
The Field class contains nested grids (Single_Grid) 
and the multipole field (Multipole_Field::Field) beyond the outermost
grid. It is initialized with the names of the data files, and is
used to get the potential and its gradient at a point.

It is templated with a user-defined Interface class, which has
the following: Potential, Charge, Permittivity, Pot_Grad

This allows it to be used for the desolvation fields as well as
electric fields.

*/

#include <algorithm>
#include <memory>
#include <iostream>
#include "../../global/error_msg.hh"
#include "single_grid.hh"
#include "charge_field.hh"
#include "sfield_ptr_tree.hh"
#include "sfield_interface.hh"
#include "../../global/pi.hh"
#include "../../xml/jam_xml_pull_parser.hh"

namespace Browndye{

namespace Field{

template< class I>
class Field{
public:
  typedef typename I::Potential Potential;
  typedef Single_Grid< Potential> SGrid;
  typedef typename I::Charge Charge;
  typedef typename I::Permittivity Permittivity;
  typedef typename I::Potential_Gradient Pot_Grad;
  typedef decltype( Pot_Grad()*Pot_Grad()) EE;
  typedef decltype( EE()/Length()) EE_Grad;

  typedef typename SField_Ptr::Tree< SField_Interface<I> >::Hint Hint;

  Field() = delete;
  // Names of files.
  Field( const std::string& mpole, const Vector< std::string>& sfiles, Vec3< Length> offset);

  // with no multipole field
  Field( const Vector< std::string>& sfiles, Vec3< Length> offset);

  Vec3< Pot_Grad>
  gradient( Pos pos, Hint&) const;
  
  Potential potential( Pos pos, Hint&) const;

  Vec3< EE_Grad> EE_gradient( Pos, Hint&) const;

  Charge charge() const;
  bool has_solvent_info() const;
  Hint first_hint() const;
  void get_corners( Vec3< Length>&, Vec3< Length>&) const;
  bool has_been_checked() const;

  template< class F>
  void apply_to_grids( F&& f);

private:  
  typedef Charge_Field MField;

  std::unique_ptr< MField> mfield;
  SField_Ptr::Tree< SField_Interface<I> > sfields;
  bool already_checked = false;
};


}

// Implementation of the Field class

namespace Field{

namespace JP = JAM_XML_Pull_Parser;

template< class I>
bool Field<I>::has_been_checked() const{
  return already_checked;  
}

template< class I>
template< class F>
void Field<I>::apply_to_grids( F&& f){
  
  sfields.apply( f);
}

  //**************************************************************
/*
This class allows the Field< I>::potential to be compiled
with UNITS turned on, avoiding a units type clash between the
electrostatic multipole class and the desolvation field.
*/
template< class Potential, class Pot_Grad>
class Selector{
public:
  static
  Potential potential( const Charge_Field* mfield,
                      const Vec3< Length>& pos){
    return Potential( 0.0);
  }

  static
  Vec3< Pot_Grad> gradient( const Charge_Field* mfield, Pos pos){
    
    return Vec3< Pot_Grad>( Zeroi{});
  }
};

template<>
class Selector< EPotential, EPotential_Gradient>{
public:
  static
  EPotential potential( const Charge_Field* mfield,
                        const Vec3< Length>& pos){
    if (mfield)
      return mfield->potential( pos);
    else
      return EPotential( 0.0);
  }

  static
  Vec3< EPotential_Gradient>  gradient( const Charge_Field* mfield, Pos pos){
    if (mfield)
      return mfield->gradient( pos);
    else{ // no mpoles
      return Vec3< EPotential_Gradient>( Zeroi{});
    }
  }
};
  //**************************************************************
template< class I>
typename I::Potential
Field< I>::potential( Pos pos, Hint& hint) const{

  Potential v{};
  const SGrid* sfield = sfields.lowest_found( v, pos, hint);

  if (sfield != nullptr)
    return v;
  else
    return Selector< Potential, Pot_Grad>::
      potential( mfield.get(), pos);
}

//##############################################
template< class I>
auto Field< I>::
gradient( Pos pos, Hint& hint) const -> Vec3< Pot_Grad>{

  Vec3< Pot_Grad> grad;
  const SGrid* sfield = sfields.lowest_found( grad, pos, hint);

  if (sfield == nullptr){
    grad =  Selector< Potential, Pot_Grad>::gradient( mfield.get(), pos);
  } 
  
  return grad;    
}

//##############################################
template< class I>
auto Field< I>::
EE_gradient( Pos pos, Hint& hint) const -> Vec3< EE_Grad>{

  Vec3< EE_Grad> grad( Zeroi{});
  sfields.lowest_found( grad, pos, hint);
      
  return grad;
}

//####################################################
  // automatically ordered the correct way re nesting
  // constructor
template< class I>
Field< I>::Field( const Vector< std::string>& files, Pos offset){

  if (files.empty()){
    error( "no files for electric field");
  }

  auto nf = files.size();
  for( size_t i = 0; i<nf; i++){
    const std::string& file = files[i];
    auto grid = std::make_unique< SGrid>( file.c_str(), offset);
    sfields.insert( std::move( grid));
  }

  sfields.check_consistency();
}

//#######################################################
// constructor
template< class I>
Field< I>::Field( const std::string& mpole, const Vector< std::string>& files, Pos offset): Field( files, offset){

  mfield = std::make_unique< MField>( mpole, offset);
}

template< class I>
bool Field< I>::has_solvent_info() const {
  return bool(mfield);
}

template< class I>
typename I::Charge
Field< I>::charge() const{
  return mfield->eff_charge();
}

template< class I>
typename Field< I>::Hint Field< I>::first_hint() const{
  return sfields.first_hint();
}

template< class I>
void Field<I>::get_corners( Vec3< Length>& low, Vec3< Length>& high) const{
  const SGrid& top = sfields.top_item();
  
  low = top.low_corner();
  high = top.high_corner();
}

}}


