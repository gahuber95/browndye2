#pragma once
#include <type_traits>
#include <utility>

namespace Browndye{

/*
template <class ...Fs>
struct overload : Fs... {
  template <class ...Ts>
  overload(Ts&& ...ts) : Fs{ std::forward<Ts>(ts)}...
  {} 

  using Fs::operator()...;
};

template <class ...Ts>
overload(Ts&&...) -> overload< std::remove_reference_t<Ts>...>;
*/

template <class ...Fs>
struct overload : Fs... {
  template <class ...Ts>
  overload(const Ts& ...ts) : Fs{ ts}...
  {} 

  using Fs::operator()...;
};

template <class ...Ts>
overload(Ts&&...) -> overload< std::remove_reference_t<Ts>...>;

 
/* 
//#############################################################
// kludge for g++
template< class F0, class F1>
struct overload2: F0, F1{
  
  overload2( const F0& f0, const F1& f1): F0{f0}, F1{f1}{}
  
  using F0::operator();
  using F1::operator();
};
 
//#############################################################
// kludge for g++
template< class F0, class F1, class F2>
struct overload3: F0, F1, F2{
  
  overload3( const F0& f0, const F1& f1, const F2& f2): F0{f0}, F1{f1}, F2{f2}{}
  
  using F0::operator();
  using F1::operator();
  using F2::operator();
}; 
 
//#############################################################
// kludge for g++
template< class F0, class F1, class F2, class F3>
struct overload4: F0, F1, F2, F3{
  
  overload4( const F0& f0, const F1& f1, const F2& f2, const F3& f3): F0{f0}, F1{f1}, F2{f2}, F3{f3}{}
  
  using F0::operator();
  using F1::operator();
  using F2::operator();
  using F3::operator();
};
*/ 
 
 
}