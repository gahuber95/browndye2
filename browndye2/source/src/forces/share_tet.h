//
// Created by ghuber on 10/11/23.
//
#pragma once

namespace Browndye {

  template<class State0, class State1>
  bool share_tet(const State0 &state0, const State1 &state1) {

    auto itet0 = state0.itet;
    auto itet1 = state1.itet;
    if (itet0 && itet1) {

      auto ig0 = state0.group_number();
      auto ig1 = state1.group_number();
      return (ig0 == ig1 ) && (itet0.value() == itet1.value());
    }
    else {
      return false;
    }
  }

}