#pragma once

/*
 * simulation.hh
 *
 *  Created on: Sep 29, 2015
 *      Author: root
 */

#include <fstream>
#include <memory>
#include "../xml/jam_xml_pull_parser.hh"

namespace Browndye{

class Simulation{
public:

  explicit Simulation( const std::string& file);

  JAM_XML_Pull_Parser::Node_Ptr top_node = nullptr;

private:
  std::ifstream input;
  std::unique_ptr< JAM_XML_Pull_Parser::Parser> parser;
};

}

