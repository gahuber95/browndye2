#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
NAM_Simulator reads in the data, carries out a one-trajectory-at-a-time
simulation, and outputs data.  Also takes care of the multithreading. 
(NAM stands for Northrup-Allison-McCammon, even though their
original algorithm is not used here.
*/



#include <mutex>
#include "single_traj_simulator.hh"
#include "nam_multi_interface.hh"

namespace Browndye{

class Trajectory_Outputter;

class NAM_Simulator: public Single_Traj_Simulator{
public:
  explicit NAM_Simulator( JAM_XML_Pull_Parser::Node_Ptr);
  NAM_Simulator() = delete;
  NAM_Simulator( const NAM_Simulator&) = delete;
  NAM_Simulator( NAM_Simulator&&) = delete;
  NAM_Simulator& operator=( const NAM_Simulator&) = delete;
  NAM_Simulator& operator=( NAM_Simulator&&) = delete;

  //void initialize( const std::string& file);

  //void set_number_of_trajectories( size_t);
  void run() override;
  
private:
  //friend class NAM_Multi_Interface;
  typedef std::unique_ptr< Trajectory_Outputter> Traj_Out_Ptr;

  void run_individual( System_State&);
  void run_trajectory( System_State&, const Traj_Out_Ptr&);
  //Traj_Out_Ptr new_trajectory_outputter( const System_State&, const std::string&);
  void output_results( const System_State&);
  //void start_trajectory_output( const System_State& state, const Traj_Out_Ptr&) const;
  //void end_trajectory_output( const System_State& state, const Traj_Out_Ptr&) const;
  //void print_state( const System_State&, 
  //                std::ofstream&, bool state_change ) const;
  void run_thread( System_State* state);
  void check_stuck( System_State& state, bool& finished);
  void check_done( System_State& state, bool& finished);
  void change_of_state( System_State& state,
      const std::unique_ptr< Trajectory_Outputter>& outputter);
  
  //size_t n_trajs = 0;
  size_t n_stuck = 0;
  size_t n_escaped = 0;
  //size_t max_n_steps = maxi;
  
  size_t n_trajs_completed = 0;
  size_t n_trajs_started = 0;
  Vector< size_t, Rxn_Index> completed_rxns;  
  std::string min_rxn_coord_name;
  
  Vector< Length> min_rxn_coords;
};

//**************************************************
/*
template< class Func>
void NAM_Multi_Interface::apply_to_object_refs( Func& f, NAM_Simulator* sim){
  
  for( auto& system: sim->systems)
    f( &system);  
} 

inline
size_t NAM_Multi_Interface::size( NAM_Simulator* sim){
  return sim->systems.size();
}
*/
}

/* 

Input:

information for Molecule_Pair

number of trajectories
number of steps per output
maximum number of steps
output file
optional random number seed

Output:

number reacted vs. number of trajectories

*/

