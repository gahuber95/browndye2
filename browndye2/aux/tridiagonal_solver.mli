(* 
Used internally. Solves a tridiagonal system. The matrix is
contained in the arrays "a", "b", and "c" as shown below, 
the rhs is in "d", and the answer is placed in "x".
Arrays "b", "c", and "d" are destroyed, and a.(0) is unused.
 
b0 c0             x0     d0
a1 b1 c1          x1     d1
   a2 b2 c2       x2     d2
      ....
       an-1 bn-1  xn-1   dn-1
*)
 
val solve :
  a: float array ->
  b: float array ->
  c: float array -> d: float array -> x: float array -> unit
