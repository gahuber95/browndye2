#include "parameter_info.hh"

namespace Browndye{
    
Vector< Energy>
Parameter_Info::converted_energy( Vector< Energy>& potentials) const{
 
 if (energy_units == Energy_Units::Kcal_per_moles){
   return potentials.mapped( [&](Energy v){return energy_conv_factor*v;});
  }
 else{
   return potentials.copy();
 }
}

//#########################################################################
void Parameter_Info::set_energy_units( const JAM_XML_Pull_Parser::Node_Ptr& node){
 
  node->complete();
  auto unit_name = value_from_node< std::string>( node);
  if (unit_name == "kcal_per_mol"){
    energy_units = Energy_Units::Kcal_per_moles;
  }
  else if (unit_name == "default"){
    energy_units = Energy_Units::Default;
  }
  else{
    node->perror( "undefined energy units");
  }
}

}