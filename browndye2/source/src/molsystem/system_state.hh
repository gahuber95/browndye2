#pragma once

#include <memory>
#include "../global/physical_constants.hh"
#include "../group/group_state.hh"
#include "system_fate.hh"
#include "system_thread.hh"
#include "../generic_algs/wiener/wiener_loop.hh"
#include "../motion/wiener_interface.hh"


namespace Browndye{

class System_Mobility_Interface;
//class System_Mobility_Info;
class System_Common;
class Simulator;
class Rxn_Tester;
class Wiener_Interface;

class System_State{
public:
  
  struct BD_Step_Result{
    bool is_done, must_backstep, rxn_completed;
  };
  
  void do_large_step( Thread_Index);
  void do_bd_step( Time dt,
                    const Wiener_Vector& ws,
                    BD_Step_Result& step_result);
  
  System_State() = delete;
  System_State( const System_State&);
  System_State& operator=( const System_State&);
  System_State( System_State&&) = default;
  System_State& operator=( System_State&&) = default;
  
  // positions are as in common
  System_State( System_Thread&, Sys_Copy_Index);
  void set_initial_state();
  void set_initial_state_for_bb();
  
  void recenter();
  
  System_Thread& thread();
  [[nodiscard]] const System_Thread& thread() const;
  [[nodiscard]] const System_Common& common() const;
 
  [[nodiscard]] Length rxn_coord() const;
  [[nodiscard]] std::tuple< Length, Diffusivity, Length> rxn_coord_n_diffu() const;
  Time time_step_guess();
  
  //Length inter_group_distance( Group_Index, Group_Index);
  
  void update_geometry( bool& frozen_changed);
  void update_mobility_geometry();
  void update_mobility_quantities();
  [[nodiscard]] bool has_hi() const;
  void make_inconsistent_from_shift();
  
  void save();
  void restore();
  void swap();
  void rethread( Simulator&, Thread_Index);
  
  [[nodiscard]] bool constraints_consistent() const;
  void set_force_consistency_true();
  void check_all_consistency() const;
  void make_start_state_consistent();
  void push_tets_down();
  
  [[nodiscard]] Time minimum_dt() const;
  [[nodiscard]] std::optional< Time> constant_dt() const;

  [[maybe_unused]] [[nodiscard]] double constraint_error() const; // debug
  [[maybe_unused]] [[nodiscard]] double saved_constraint_error() const;
  void compute_tet_forces();

  [[nodiscard]] Diffusivity pair_radial_diffusivity() const; // for staged_simulation

  #ifdef DEBUG
  ~System_State();
  #endif
  
  // data
  State_Index current_rxn_state{ maxi};
  Rxn_Index last_rxn{ maxi};
  Length minimum_rxn_coord;
 
  size_t n_bd_steps{ 0}, n_full_steps{ 0};

  Time time{ 0.0};
  Sys_Copy_Index number{ maxi};
  Time last_dt{ NaN};
  System_Fate fate = System_Fate::Undefined;
  
  Vector< Group_State, Group_Index> groups;
    
  typedef System_Thread::Mob_Info_And_Doer MID;
  const Mobility_Info* mob_info = nullptr;   
  MID* mob_info_and_doer = nullptr;
  std::unique_ptr< MID> my_mob_info_and_doer; // if updates are staggered
  //bool mob_geometry_changed = true;
  
  typedef Wiener_Loop::WProcess< Wiener_Interface> WProc;
  std::unique_ptr< WProc> wprocess;
  
  // for weighted ensemble
  double wt{ NaN};
  bool had_reaction = false;
  Time t_since_output = Time( 0.0);
  std::optional< Bin_Index> ibin;  
    
  //bool has_const_dt() const;
  [[maybe_unused]] void test_force_balance( int tag) const; // private

protected:
  std::reference_wrapper< System_Thread> thread_ref;
  
  void set_initial_reaction_state();

  
private:
  // consistent relative to positions
   struct Consistency{
     bool force = false;
     bool geometry = false;
     bool constraints = true;
     bool hydrodynamics = false;
     
     [[nodiscard]] bool all() const;
   };
     
  friend class Rxn_Tester;
  typedef Group_State::Circum_Sphere Circum_Sphere;
  typedef Group_State::Circum_Spheres Circum_Spheres;
  
  
  void copy_from( const System_State&);
  
  static
  Circum_Sphere chain_circumsphere( const Chain_State& cstate);
  
  void update_group_chain_freezings( const Circum_Spheres& circum_spheres,
                                    Group_Index, bool& frozen_changed);
  
  
  //typedef Vector< Vector< Atom_Index>, Atom_Index> Chain_Common::* Excl_Ptr;
  //typedef Vector< typename Chain_Common::Core_Interactor> Chain_Common::* CD_Int_Ptr;
  
  template< class Interactor>
  void check_core_interactors( const Circum_Spheres&, Group_Index, Chain_Index,
                                 const Vector< Interactor>&, bool& new_frozen);
  void check_chain_interactors( const Circum_Spheres&, Group_Index, Chain_Index,
                                bool& new_frozen);
  [[nodiscard]] Circum_Spheres all_circumspheres() const;
  void center_group0();
  void place_group1_on_sphere( Length);
  
  void update_chain_freezings( Vector< bool, Group_Index>& frozen_changed);
  void place_at_starting_site();
  void place_on_sphere( Length);
  [[nodiscard]] Pos group_center( Group_Index gi) const;
  [[nodiscard]] std::vector< bool> mobility_key() const;
  
  // BD step, functions in motion/do_bd_step.cc
  void step_beads_forward( const Solvent& solvent, 
                      const Wiener_Vector& ws, Time dt,
                          bool& backstep);
  
  void satisfy_constraints();
  [[nodiscard]] bool backstep_due_to_constraints() const;
  void check_constraints_blowup( Time min_dt, Time dt) const;
  [[nodiscard]] bool backstep_due_to_force( const Solvent& solvent, Time dt) const;
  
  void test_and_update_reactions( Length rxnc, BD_Step_Result& step_result);
  [[nodiscard]] std::pair< Time, Time> time_step_minimum_bounds() const;
  [[nodiscard]] bool are_all_frozen() const;
  //void test_for_rxn_and_force_backstep_and_update( Time dt, Length old_rxn_coord,
  //                                                  BD_Step_Result& step_result);
  
  void step_beads_forward_no_hi( const Solvent& solvent, const Wiener_Vector& ws,
                                  Time dt);
  
  [[nodiscard]] Inv_Time rotational_diffusivity( const Tet< Group_State>& tet) const;
  void renorm_tet( Time delta_t, bool has_const_dt,
                    Tet< Group_State>& tet, bool& backstep);
  void renorm_tets( Time dt, bool& backstep);
  void zero_tets_dposes();
  //bool tet_constraints_not_far_off() const; // perhaps not needed
  
  // Full step, functions in motion/do_full_step.cc
  void do_bimolecular_case();
  void transform_group1( Transform);
  void do_building_bins_case( Pos);
  void do_nam_case( Pos);
  void update_mobility_if_needed( bool); 
  [[nodiscard]] Time max_time_within_group( Group_Index ig) const;
  [[nodiscard]] Time max_time_between_groups_below( Group_Index ig) const;
  [[nodiscard]] Time max_time_step() const;
  void step_const_dt( Time dt);
  void step_variable_dt();
  [[nodiscard]] bool restraint_violated() const;
  void split_const_dt( Time dt, BD_Step_Result&, bool& did);
  void get_sole_pos( Pos&) const;
  void get_sole_tet_axis( Array< double, 3>&) const;
  void check_for_blowup() const;
  //void split_for_broken_bond( Time dt, BD_Step_Result&);
  void check_bonds( const Group_State& group, bool& did) const;
  void do_double_step( Time dt, BD_Step_Result& step_result);
  void update_eta0();

  struct FT_Info{
    Vector< F3, Group_Index> forces;
    Vector< T3, Group_Index> torques;
    F3 total_force;
    T3 total_torque;
    Force max_force;
    Torque max_torque;
  };

  [[nodiscard]] FT_Info group_forces_torques() const;

      // data
  Consistency consistency;
  
};

//#################################################
inline
System_Thread& System_State::thread(){
  return thread_ref.get();
}

inline
const System_Thread& System_State::thread() const{
  return thread_ref.get();
}

inline
Pos System_State::group_center( Group_Index gi) const{
  return groups[ gi].center();
}

inline
bool System_State::Consistency::all() const{
  
  return force && constraints && hydrodynamics && geometry;
}

inline
bool System_State::constraints_consistent() const{
  return consistency.constraints;
}

inline
void System_State::set_force_consistency_true(){
  consistency.force = true;
}

/*
inline
bool System_State::has_const_dt() const{
  
  return common().ts_params.const_dt_opt.has_value();
}  
*/

}


