//
// Created by ghuber on 11/4/23.
//
#pragma once
#include <map>
#include "../../lib/vector.hh"

namespace Browndye{

  template< class T, class Idx>
  std::map< Idx, Idx> redundant_index_families( const Vector< Vector< T, Idx>, Idx>& params){

    auto tlt = [&]( Idx i0, Idx i1) -> bool {
      auto &row0 = params[i0];
      auto &row1 = params[i1];
      return lexicographical_compare( row0.begin(), row0.end(), row1.begin(), row1.end());
    };

    auto nt = params.size();
    Vector< Idx, Idx> indices( nt);
    for( auto i: range( nt)){
      indices[i] = i;
    }

    sort( indices.begin(), indices.end(), tlt);

    // maps index to its lowest equivalent
    std::map< Idx, Idx> index_families;
    Idx itf(0);
    if (nt > Idx(0)) {
      index_families.insert( std::make_pair( indices[itf], indices[itf]));
    }

    const auto dt1 = Idx(1) - Idx(0);
    for( auto i: range( Idx(1), nt)){
      if (tlt( indices[i - dt1], indices[i])) {
        itf = i; // new run of equivalent indices
      }
      index_families.insert( std::make_pair( indices[i], indices[itf]));
    }

    return index_families;
  }
}