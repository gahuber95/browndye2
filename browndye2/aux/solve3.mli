(*
Used internally.

Both functions are applied to 3x3 matrices.

"solve3" solves the equation matrix*x_array = b_array

"eigenvectors" returns the eigenvectors and corresponding eigenvalues
of "matrix".

*)

val solve3: matrix: float array array -> b_array: float array -> 
  x_array: float array -> unit

val eigenvectors: matrix: (float array array) -> 
  ((float array)*(float array)*(float array)) * (float*float*float)

