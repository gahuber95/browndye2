#pragma once

#include "gen_interface.hh"
#include "lj_force_computer.hh"

namespace Browndye{

  namespace LJ {

    struct Sys {
      System_State &state;
      const MM_Nonbonded_Parameter_Info< Force_Field_Type::Molecular_Mechanics> &pinfo; // need to generalize someday

      Sys(System_State &_state, const MM_Nonbonded_Parameter_Info< Force_Field_Type::Molecular_Mechanics> &_pinfo) : state(_state), pinfo(_pinfo) {}

      double one_four_factor() const{
        return pinfo.one_four_factor;
      }
    };

    typedef const Vector< Atom, Atom_Index> CAtoms;

    typedef Sys System_State;

    template< class Ia>
    struct Outer_Loop: public Ia {
      typedef Outer_Loop< Ia> Subsystem;
      typedef Ia Iface;
      typedef typename Ia::Container Container;
      typedef typename Ia::Refs Refs;
      typedef typename Ia::Ref Ref;
      typedef typename Ia::Transform Transform;
      
      Transform tforma;
      Container &cona;
      Refs& refsa;
      
      F3 force = F3( Zeroi{});
      T3 torque = T3( Zeroi{});

      explicit Outer_Loop( Sys&, Transform _tforma, Container &_cona, Refs& _refsa, Pos, Length) :
              tforma(_tforma), refsa( _refsa), cona(_cona) {}
    };

    template<class Ib>
    struct Inner_Loop: public Ib {
      //typedef Ib Ibb;
      typedef Inner_Loop< Ib> Subsystem;
      typedef typename Ib::Container Container;
      typedef typename Ib::Transform Transform;
      typedef typename Ib::Refs Refs;
      typedef typename Ib::Ref Ref;

      Container &conb;
      Transform tformb;
      Energy epsilon;
      Length sigma;
      F3 force = F3( Zeroi{});
      Pos posa;
      //Pos cena;

      //typename Ib::Inner_Loop loopb( sys, tformj, conj, refsj, posj, radj, loopa, ia);

      template< class Ia>
      Inner_Loop( Sys& sys, Transform _tformb, Container & _conb, Refs&, Pos, Length, Outer_Loop<Ia>& loopa, typename Ia::Ref ia) : tformb(_tformb), conb(_conb) {
        auto ita = Ia::atom_type( loopa.cona, ia);
        sigma = sys.pinfo.sigma( ita);
        epsilon = sys.pinfo.epsilon( ita);
        posa = Ia::position( loopa.cona, ia);
      }
    };

//################################################################################
    template< class I>
    Pos position( Sys&, Inner_Loop< I>& loopb, typename I::Ref ib){
      return I::transformed_position( loopb.conb, loopb.tformb, ib);
    }

    template< class I>
    Pos position( Sys&, Outer_Loop< I>& loopa, typename I::Ref ia){
      return I::transformed_position( loopa.cona, loopa.tforma, ia);
    }

    template< class Ia, class Ib>
    Energy epsilon( Sys& sys, Outer_Loop< Ia>& oloop, Inner_Loop< Ib>& iloop, typename Ia::Ref, typename Ib::Ref ib){

      auto ea = iloop.epsilon;
      auto itb = Ib::atom_type( iloop.conb, ib);
      auto eb = sys.pinfo.epsilon( itb);
      return sqrt(ea * eb);
    }

    template< class Ia, class Ib>
    Length sigma( Sys& sys, Outer_Loop< Ia>& oloop, Inner_Loop< Ib>& iloop, typename Ia::Ref, typename Ib::Ref ib){

      auto sa = iloop.sigma;
      auto itb = Ib::atom_type( iloop.conb, ib);
      auto sb = sys.pinfo.sigma(itb);
      return sa + sb; // check
    }

    // check sign on force!
    template< class Ia, class Ib>
    void add_force( Sys&, Outer_Loop< Ia> &loopa, Inner_Loop< Ib>& loopb, Pos, Pos, typename Ia::Ref ia, typename Ib::Ref ib,  F3 f) {

      if constexpr (Ia::is_chain){
        loopa.cona.catoms[ia].force -= f;
      }
      else {
        loopa.force += f;
      }

      if constexpr (Ib::is_chain){
        loopb.conb.catoms[ib].force += f; // check sign!
      }
    }
  } // end namespace LJ


  //##################################################################################################
  class LJ_Iface{
  public:
    typedef LJ::Sys System;

    template< class Ia, class Ib>
    static
    Energy epsilon( System& sys, typename Ia::Subsystem& suba, typename Ib::Subsystem subb, typename Ia::Ref ia, typename Ib::Ref ib){
      return LJ::epsilon( sys, suba, subb, ia, ib);
    }

    template< class Ia, class Ib>
    static
    Length sigma( System& sys, typename Ia::Subsystem& suba, typename Ib::Subsystem subb, typename Ia::Ref ia, typename Ib::Ref ib){
      return LJ::sigma( sys, suba, subb, ia, ib);
    }

    template< class I>
    static
    Pos position( System& sys, typename I::Subsystem& sub, typename I::Ref i){
      return LJ::position( sys, sub, i);
    }

    template< class Ia, class Ib>
    static
    void add_force( System& sys, typename Ia::Subsystem& suba, typename Ib::Subsystem subb, Pos posa, Pos posb, typename Ia::Ref ia, typename Ib::Ref ib, F3 f){
      LJ::add_force( sys, suba, subb, posa, posb, ia, ib, f);
    }

    template< template <class, class> class F>
    using Pair_Applier = Pair_Applier_Gen< LJ_Iface, LJ::Outer_Loop, LJ::Inner_Loop, F, &Chain_Common::excluded_atoms, &Chain_Common::excluded_core_atoms>;

  };
}
