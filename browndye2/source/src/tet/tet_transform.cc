#include "../transforms/transform.hh"
#include "../lib/linalg.hh"
#include "../lib/rotations.hh"
#include "../transforms/pure_translation.hh"
#include "centroid.hh"
#include "v4.hh"

// perhaps not needed later
//######################################
// best fit between two tetrahedra

// Closed-form solution of absolute orientation using unit quaternions
// Berthold K. P. Horn
// Journal of the Optical Society of America A Vol. 4, Issue 4, pp. 629-642 (1987)
// https://doi.org/10.1364/JOSAA.4.000629 


namespace Browndye{
  
  // refactor to use weighted alignment transform
  Transform tform_of_tet( const V4< Pos>& tet0,
                           const V4< Pos>& tet1){
     
    auto cen0 = centroid( tet0);
    auto cen1 = centroid( tet1);
  
    Mat3< Length2> M( Zeroi{});
    
    for( auto i: range(4)){
      auto ai = Atom_Index(i);
      M += outer( tet0[ai] - cen0, tet1[ai] - cen1);
    }
    
    constexpr size_t X = 0, Y = 1, Z = 2;
    
    SMat< Length2, 4,4> N;
    N[0][0] = M[X][X] + M[Y][Y] + M[Z][Z];
    N[1][0] = M[Y][Z] - M[Z][Y];
    N[2][0] = M[Z][X] - M[X][Z];
    N[3][0] = M[X][Y] - M[Y][X];
    
    N[1][1] = M[X][X] - M[Y][Y] - M[Z][Z];
    N[2][1] = M[X][Y] + M[Y][X];
    N[3][1] = M[Z][X] + M[X][Z];
    
    N[2][2] = -M[X][X] + M[Y][Y] - M[Z][Z];
    N[3][2] =  M[Y][Z] + M[Z][Y];
    
    N[3][3] = -M[X][X] - M[Y][Y] + M[Z][Z];

    for( auto ir: range(0,4)){
      for( auto ic: range( ir+1,4)){
        N[ir][ic] = N[ic][ir];
      }
    }
          
    auto evecs = Linalg::eigensystem( N).first;
        
    auto quat = evecs[3]; 
    auto rot = mat_of_quat( quat);
    
    Pure_Translation trans0( -cen0), trans1( cen1);
    Transform tformr( rot, Pos( Zeroi{}));
    
    return trans1*tformr*trans0;
  }
  
 
}

//###################################################
// turn into unit test
/*
namespace{
  class I{
  public:
    typedef std::mt19937_64 Random_Number_Generator;
    
    static
    double gaussian( std::mt19937_64& gen){
      std::normal_distribution< double> gauss;
      return gauss( gen);
    }
  };
  
  void main0(){
    using namespace Browndye;
    
    std::mt19937_64 gen;
      
      
    const Length L0{ 0.0}, L1{ 1.0};
    
    Pos p0{ L0,L0,L0}, p1{L1,L0,L0}, p2{L0,L1,L0}, p3{L0,L0,L1};
      
    gen.discard( 14);
    
    auto rot = random_rotation< I>( gen);
    std::xout << "rot\n";
    for( auto row: rot){
      std::xout << row << '\n';
    }
    std::xout << '\n';
    
    std::uniform_real_distribution< double> uni;
    Pos rpos{ L1*uni(gen), L1*uni( gen), L1*uni( gen)};
    
    auto q0 = rot*p0 + rpos;
    auto q1 = rot*p1 + rpos;
    auto q2 = rot*p2 + rpos;
    auto q3 = rot*p3 + rpos;
    
    Array< Pos, 4> tet0{ p0,p1,p2,p3}, tet1{ q0,q1,q2,q3};
    
    auto tform = tform_of_tet( tet0, tet1);
    auto rotc = tform.rotation();
    
    std::xout << "new rot\n";
    for( auto row: rotc){
      std::xout << row << '\n';
    }
    std::xout << '\n';
    
    xout << "old new q0\n";
    xout << q2 << '\n';
    xout << tform.transformed( p2) << '\n';
    
  }
}

int main(){
  main0();
}
*/
