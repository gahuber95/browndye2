#pragma once

#include <memory>
#include <string>
#include "../../lib/vector.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../xml/node_info.hh"
#include "../../forces/electrostatic/field.hh"
#include "../../xml/get_strings.hh"

namespace Browndye{

template< class I>
void initialize_field( const JAM_XML_Pull_Parser::Node_Ptr& enode,
                       std::shared_ptr< Field::Field< I> >& field_ptr, Pos offset){
  
  if (enode != nullptr){
    auto mpnode = enode->child( "multipole_field");
    
    typedef std::string String;

    // fields listed in no particular order
    auto grid_files = strings_from_node( enode, "grid");
    
    typedef Field::Field< I> LField;
    if (mpnode){
      auto mpfile = value_from_node< String>( mpnode);
      field_ptr = std::make_unique< LField>( mpfile, grid_files, offset);
    }
    else {
      field_ptr = std::make_unique< LField>( grid_files, offset);
    }
  }
}
}

