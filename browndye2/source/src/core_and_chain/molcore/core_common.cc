#include <fstream>
#include <list>
#include "core_common.hh"
#include "../../molsystem/system_common.hh"
#include "initialize_field.hh"
#include "../../lib/bool_of_string.hh"
#include "../../forces/absinth/absinth.hh"
#include "../sphere_overlap.hh"
#include "../home_transform.hh"
#include "simple_collision_interface.h"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;
typedef std::string String;
using std::min;
using std::max;
using std::make_unique;

//################################################################################
// constructor
Core_Common::Core_Common( const Nonbonded_Parameter_Info& pinfo, const JP::Node_Ptr& node, const System_Common& _system, Group_Index gi, Core_Index _number):
  Macro_Struct( node, pinfo, gi), number(_number), system(_system){
  
  name = checked_value_from_node< String>( node, "name");
  
  auto conode = node->child( "copy");
  if (conode){
    setup_from_parent( conode);    
  }
  else{
    auto hfile = checked_value_from_node< String>( node, "hydro_params");
    std::tie( h_radius, hydro_center) = hydro_params( hfile);
        
    bool pre_checked;
    setup_fields( node, pre_checked);
    
    set_charge_chebybox_from_node( node);
    set_desolvation_chebybox_from_node( node);
        
    setup_atoms( pinfo);
    
    if (v_field && !pre_checked){
      check_mpole_consistency();
    }
    
    if (system.force_field == Force_Field_Type::Absinth &&
        (gi == Group_Index(0)) && (number == Core_Index(0))){
      setup_eta_max();
      hollow_out_grids();
      is_stationary = true;
    }
  }
}


//#########################################################
void Core_Common::hollow_out_grids(){
  
  const auto eps_p = 4.0*vacuum_permittivity; // should not be hard-coded
  const auto eps_s = vacuum_permittivity*system.solvent.dielectric;
  const Length srad{ 1.4}; // should not be hard-coded
  const auto rw = Absinth::rw;
  
  constexpr int X = 0, Y = 1, Z = 2;
  
  auto f = [&]( Single_Grid< EPotential>& grid){
    auto lo = grid.low_corner();
    auto hi = grid.high_corner();
    auto ns = grid.dimensions();
    auto nx = ns[X];
    auto ny = ns[Y];
    auto nz = ns[Z];
    
    auto hx = (hi[X] - lo[X])/((double)(nx-1));
    auto hy = (hi[Y] - lo[Y])/((double)(ny-1));
    auto hz = (hi[Z] - lo[Z])/((double)(nz-1));
    
    for( auto& atom: atoms){
      auto q = atom.charge;
      auto a = atom.radius;
      auto R = a + srad;
      auto Rw = atom.radius + rw;
      auto V0 = (q/(pi4*R))*(1.0/eps_s - 1.0/eps_p);
      auto pos = atom.pos;
      
      auto arw = Rw + 0.5*rw;
      auto lox = max( lo[X], pos[X] - arw);
      auto hix = min( hi[X], pos[X] + arw);
      auto loy = max( lo[Y], pos[Y] - arw);
      auto hiy = min( hi[Y], pos[Y] + arw);
      auto loz = max( lo[Z], pos[Z] - arw);
      auto hiz = min( hi[Z], pos[Z] + arw);

      size_t ixlo = (lox - lo[X])/hx;
      size_t ixhi = (hi[X] - hix)/hx + 1;
      
      size_t iylo = (loy - lo[Y])/hy;
      size_t iyhi = (hi[Y] - hiy)/hy + 1;

      size_t izlo = (loz - lo[Z])/hz;
      size_t izhi = (hi[Z] - hiz)/hz + 1;
      
      for( auto ix: range( ixlo, ixhi)){
        auto x = lo[X] + hx*(double)ix;
        for( auto iy: range( iylo, iyhi)){
          auto y = lo[Y] + hy*(double)iy;
          for( auto iz: range( izlo, izhi)){
            auto z = lo[Z] + hz*(double)iz;  
            Pos gpos{ x,y,z};
            auto r = distance( pos, gpos);
            double factor = [&]{
              if (r < a + rw){
                return 1.0;
              }
              else if (r < a + 1.5*rw){
                return 0.5*(1.0 + cos((pi2/rw)*(r - (a + rw))));
              }
              else{
                return 0.0;
              }
            }();
            
            typedef typename Units::Short_Version< EPotential>::Res SPotential;
            
            SPotential V = factor*( (r < R) ? V0 + q/(pi4*eps_p*r) : q/(pi4*eps_s*r));
            grid.add_to_value( ix,iy,iz, -V);
          }
        }
      }
      
    }
  };
  
  v_field->apply_to_grids( f);
  
}

//#########################################################
void Core_Common::setup_eta_max(){
  
  for( auto& atom: atoms){
    atom.set_eta_max( 1.0);
  }

  typedef Collision_Detector::Structure< Simple_Col_Interface> MCollision_Structure;

  MCollision_Structure cols( atoms, max_per_cell);
  
  typedef Vector< Atom, Atom_Index> Container;
  typedef Atom_Index Ref;
  typedef Vector< Ref, Ind_Atom_Index> Refs;
  
  auto volumes = atoms.mapped( [&](const Atom& atom){ return atom.volume;});
  for( auto& atom: atoms){
    atom.volume = Length3{ 0.0};
  }
  
  auto h = Absinth::rw;
  
  auto add_overlaps = [&]( Atom& atomi, Atom& atomj){
    auto d = distance( atomi.pos, atomj.pos);
    auto ri = atomi.radius;
    auto rj = atomj.radius;
    auto olap = sphere_overlap( ri, rj, d);
    auto olapi = sphere_overlap( ri+h, rj, d);
    auto olapj = sphere_overlap( ri, rj+h, d);
    atomi.volume += olapi - olap;
    atomj.volume += olapj - olap;
  };
  
  auto fb = [&]( Blank_Transform, Container& atoms, Refs& refs){
    
    for( auto i: refs){
      auto& atomi = atoms[i];
      for( auto j: range(i)){
        auto& atomj = atoms[j];
        add_overlaps( atomi, atomj);
      }
    }
  };
  
  auto f = [&]( Blank_Transform, Container&, Refs& refs0, Pos center0, Length radius0,
               Blank_Transform, Container& atoms, Refs& refs1, Pos center1, Length radius1){
    
    for( auto i0: refs0){
      auto& atom0 = atoms[i0];
      for( auto i1: refs1){
        if (&refs1 != &refs0 || i0 != i1){
          auto& atom1 = atoms[i1];
          add_overlaps( atom0, atom1);
        }
      }
    }
  };
  
  Collision_Detector::compute_self_interaction( f, fb, cols, Blank_Transform{});
   
  for( auto& atom: atoms){
    auto r = atom.radius;
    auto Vmax = (pi4/3)*(cube(r + h) - cube(r));
    auto olap = atom.volume;
    atom.set_eta_max(  1.0 - olap/Vmax);
  }
  
  auto nat = atoms.size();
  for( auto ia: range( nat)){
    atoms[ia].volume = volumes[ia];
  }
}

//#########################################################
void Core_Common::setup_fields( const Node_Ptr& node, bool& pre_checked){
  
  JP::Node_Ptr dnode = node->child( "desolvation_field");
  if (dnode){
    initialize_field( dnode, born_field, -hydro_center);
    born_hint0 = born_field->first_hint();
  }  
    
  pre_checked = false;  
  auto enode = node->child( "electric_field");
  if (enode){
    auto cvalopt = enode->attributes().value( "checked");
    if (cvalopt && bool_of_str( cvalopt.value())){
      pre_checked = true;
    }
    
    initialize_field( enode, v_field, -hydro_center);
    v_hint0 = v_field->first_hint();
  }
}

//#############################################################################
void Core_Common::setup_atoms( const Nonbonded_Parameter_Info& pinfo){
  
  atoms = pinfo.atoms_from_file( atoms_file);
  for( auto& atom: atoms){
    atom.pos -= hydro_center;
  }
  if (number == Core_Index(0) && ig == Group_Index(0)){
    collision_structure = make_unique< Collision_Structure>( atoms, max_per_cell);
  }
  
  /*
  Vector< Atom_Index, Atom_Index> anumbers;
  anumbers.reserve( atoms.size());
  for( auto& atom: atoms){
    anumbers.push_back( atom.number);
  }
  typedef std::map< Atom_Index, Atom_Index> AMap;
  atom_imap = std::make_shared< AMap>( atom_number_map( anumbers));
  */
}

//######################################################
void Core_Common::setup_from_parent( Node_Ptr conode){
  
  find_copy_parent( conode);
  
  if (copy_parent){
    auto& parent = *copy_parent;
  
    h_radius = parent.h_radius;
    
    born_field = parent.born_field;
    if (born_field){
      born_hint0 = parent.born_hint0;
    }
    v_field = parent.v_field;
    if (v_field){
      v_hint0 = parent.v_hint0;
    }
    q_blob = parent.q_blob;
    q2_blob = parent.q2_blob;
    
    atoms = parent.atoms.copy();
    
    atom_imap = parent.atom_imap;
    copy_tform = home_transform( conode);
    //atoms_file = parent.atoms_file;
  }
  else{
    conode->perror( "core", name, "is a copy and must have a parent");  
  }
}

//###################################################################
void Core_Common::find_copy_parent( const Node_Ptr &conode){
  
  auto oname = checked_value_from_node< std::string>( conode, "parent");
  copy_parent = nullptr;
   for( auto& group: system.groups){
     for( auto& core: group.cores){
       if (core.name == oname){
         copy_parent= &core;
         break;
       }
     }
   }  
}

//##################################################################
std::pair< Length, Pos> Core_Common::hydro_params( const std::string& file) const{
  std::ifstream input( file);
  if (!input.is_open())
    error( "hydro file", file, "for", name,"could not be opened");

  JP::Parser parser( input, file);
  auto top_node = parser.top();
  top_node->complete();
  
  auto h_rad = checked_value_from_node< Length>( top_node, "radius");
  auto h_cen = checked_array_from_node< Length, 3>( top_node, "center");
  
  return make_pair( h_rad, h_cen);
}

//##########################################################################
void Core_Common::set_desolvation_chebybox_from_node( const Node_Ptr &node){
  
  auto cbox_file = value_from_node< std::string>( node, "eff_charges_squared");
  if (cbox_file){
    q2_blob = make_unique<Charged_Blob::Blob< 4, Q2_Blob_Interface> >( *cbox_file, -hydro_center);
  }
}

//####################################################################
void Core_Common::set_charge_chebybox_from_node(const Node_Ptr &node){
  
  auto cbox_file = value_from_node< std::string>( node, "eff_charges");
  if (cbox_file){
    q_blob = make_unique< Charged_Blob::Blob< 4, Q_Blob_Interface> >( *cbox_file, -hydro_center);
  }
}

//#######################################################
template< class T>
inline
T min4( const T& a, const T& b, const T& c, const T& d){
  return min( min( a,b), min( c,d));
}

//####################################################
void Core_Common::check_mpole_consistency() const{
  
  Vec3< Length> lowc, highc;
  v_field->get_corners( lowc, highc);

  auto dmin = Length( large);
  for( auto& atom: atoms){
    Length r = atom.radius;
    auto& pos = atom.pos;
    Length dxl = pos[0] - lowc[0] - r;
    Length dxh = highc[0] - pos[0] - r;
    Length dyl = pos[1] - lowc[1] - r;
    Length dyh = highc[1] - pos[1] - r;
    Length dzl = pos[2] - lowc[2] - r;
    Length dzh = highc[2] - pos[2] - r;
    dmin = min( min( min( min4( dxl,dxh,dyl,dyh), dzl), dzh), dmin);    
  }
  
  auto center = 0.5*(lowc + highc);
  auto dmax = Length( 0.0);
  for( auto& atom: atoms){
    dmax = max( dmax, distance( center, atom.pos));
  }
  Length eff_radius = dmax;
  auto debye = system.solvent.debye_length;

  Length req_dist = min( 3.0*debye, 3.0*eff_radius);

  if ( dmin < req_dist){
    Length shortfall = req_dist - dmin;
    error( name, "outermost grid is too small: needs to be ", 
           fvalue( shortfall), " length units bigger");
  }
}

//####################################
Charge Core_Common::charge() const{
  
  Charge q( 0.0);
  if( v_field && v_field->has_solvent_info()){
    q = v_field->charge();
  }
  else if (q_blob){
    q = q_blob->total_charge();
  }
  return q;
}

//############################################################
bool Core_Common::are_same( const Core_Common& other) const{
  
  if (copy_parent){
    if (other.copy_parent){
      return other.copy_parent == copy_parent;
    }
    else{
      return copy_parent == &other;
    }
  }
  else{
    return (this == other.copy_parent);
  }
}

//#############################################
bool Core_Common::has_copy_children() const{
  
  for( auto& group: system.groups){
    for( auto& core: group.cores){
      if (core.copy_parent == this){
        return true;
      }
    }
  }
  return false;
}

//###############################################
Transform Core_Common::home_tform() const{
  
  if (copy_parent){
    auto ptform = copy_parent->home_tform();
    return ptform*copy_tform;
  }
  else{
    return { id_matrix< 3>(), hydro_center};
  }
}

// constructor
Core_Common::Core_Common( Test_Tag tag,  System_Common &_sys, const Vector<Atom, Atom_Index> &atoms, Core_Index num) :
  Macro_Struct( tag, atoms), number( num), system(_sys) {}

}




