#pragma once

namespace Browndye{

constexpr double constraint_backstep_tol = 0.25;
constexpr double constraint_solve_tol = 1.0e-3;

}

