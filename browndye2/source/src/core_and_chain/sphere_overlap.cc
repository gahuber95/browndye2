#include "../lib/units.hh"
#include "../forces/absinth/absinth.hh"


namespace Browndye{
  
  Length3 sphere_overlap( Length Ra, Length Rb, Length r){
    
    class None0{};
    
    class None{
    public:
      typedef int System;
      //typedef None0 Flexible_Interface;
      //typedef None0 Rigid_Interface;
    };
    
    return Absinth::Computer< None>::sphere_overlap_nod( Ra, Rb, r);
  }
  
}
