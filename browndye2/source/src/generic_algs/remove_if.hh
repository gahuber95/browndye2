#pragma once
#include "../lib/vector.hh"

namespace Browndye{
  
  template< class T, class I, class F>
  void remove_if_true( Vector< T, I>& v, F&& f){
    
    auto b = v.begin();
    auto e = v.end();
    
    int nrem = 0;
    for( auto itr = b; itr < e - nrem; itr++){
      if (f(*itr)){
        auto jtr = e - (nrem + 1);
        if (jtr > itr){
          *itr = std::move( *jtr);
          ++nrem;
          --itr;
        }
        else if (jtr == itr){
          ++nrem;
        }
      }
    }
    
    v.erase( e - nrem, e);
  }
  
  
  
  
}