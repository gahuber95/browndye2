/*
 * molecule_system_common.cc
 *
 *  Created on: Oct 20, 2016
 *      Author: root
 */

#include <tuple>
#include <utility>
#include "system_common.hh"
#include "../group/group_thread.hh"
#include "../input_output/outtag.hh"
#include "../lib/bool_of_string.hh"
#include "../motion/qb_factor.hh"
#include "../forces/other/mm_nonbonded_parameter_info.hh"
#include "../forces/other/mm_bonded_parameter_info.hh"
#include "../forces/other/cd_nonbonded_parameter_info.hh"
#include "../forces/other/cd_bonded_parameter_info.hh"
#include "system_state.hh"

namespace{
  using std::max;
  using std::make_unique;
  using std::tie;
  using std::string;
  using std::ifstream;
  using std::make_pair;
}

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

const Group_Index g0( 0), g1( 1), g2( 2);

//#######################################################################
System_Common::Mol_Rxn_Ref::Mol_Rxn_Ref( Group_Index _ig, Core_Index im):
  core_or_chain( im){
  ig = _ig;
}

//#########################################################################
System_Common::Mol_Rxn_Ref::Mol_Rxn_Ref( Group_Index _ig, Chain_Index ic):
  core_or_chain( ic){
  ig = _ig;
}

//#########################################################################
System_Common::Mol_Rxn_Ref::Mol_Rxn_Ref( Group_Index _ig, Dummy_Index id):
  core_or_chain( id){
  ig = _ig;
}

System_Common::Mol_Rxn_Ref::Mol_Rxn_Ref(){
  
  core_or_chain = Nothing{};
}

//###########################################################
void System_Common::setup_force_fields( const Node_Ptr& top){

  auto ff_type_nameo = value_from_node< string>( top, "force_field");
  if (ff_type_nameo) {
    force_field = force_field_of_rep(ff_type_nameo.value());
  }

  typedef Force_Field_Type FF;
  auto pfnodes = top->children_of_tag( "parameters");
    
  auto opened = [&]( const string& pfile) -> auto{
    auto res =  ifstream( pfile);
    if (!res.is_open()){
      error( "force field parameter file", pfile, "could not be opened");
    }
    return res;
  };

  auto ff = force_field;
  if (ff == FF::Molecular_Mechanics || ff == FF::Molecular_Mechanics_HP || ff == FF::Absinth){
   
    if (pfnodes.empty()){
      if (ff == FF::Molecular_Mechanics_HP) {
        pinfo = make_unique<MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics_HP> >(ff);
      }
      else if (ff == FF::Absinth){
        error( "ABSINTH force field needs parameters");
      }
      else {
        pinfo = make_unique< MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics> >(ff);
      }

      bpinfo = make_unique< MM_Bonded_Parameter_Info>( force_field);
    }
    else{
      Vector< JP::Node_Ptr> pnodes;
      for( const auto& pfnode: pfnodes){
        auto pfile = value_from_node< string>( pfnode);
        auto pnode = JP::node_from_file( pfile);
        pnodes.push_back( pnode); 
      }

      if (ff == FF::Molecular_Mechanics_HP) {
        pinfo = make_unique<MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics_HP> >(ff, pnodes);
      }
      else if (ff == FF::Absinth){
        pinfo = make_unique< MM_Nonbonded_Parameter_Info< FF::Absinth> >( ff, pnodes);
      }
      else {
        pinfo = make_unique< MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics> >(ff, pnodes);
      }

      bpinfo = make_unique< MM_Bonded_Parameter_Info>( force_field, pnodes);
    }
  }
  
  else if (force_field == FF::Spline){
    if (pfnodes.empty()){
      error( "parameter file needed for spline force field");
    }
    
    auto pfnode = pfnodes.back();
    auto pfile = value_from_node< string>( pfnode);     
  
    {
      auto input = opened( pfile);      
      JP::Parser parser( input, pfile);
      pinfo = make_unique< CD_Nonbonded_Parameter_Info>( parser);
    }
    {
      auto input = opened( pfile);        
      JP::Parser parser( input, pfile);      
      bpinfo = make_unique< CD_Bonded_Parameter_Info>( parser);
    }
  }
  else{
    error( "force field not defined");
  }
    
}

//#####################################################
std::unique_ptr< Outer_Propagator> System_Common::new_outer_propagator() const{
  
  auto ng = groups.size();
  if (ng == g2){
    auto& group0 = groups[ g0];
    auto& group1 = groups[ g1];
    
    auto D_factor = solvent.diffusivity_factor();
    
    auto& mob_info0 = group0.mobility_info;
    auto Dtrans0 = D_factor*mob_info0.trans_mobility;
    auto Drot0 = D_factor*mob_info0.rot_mobility;
    auto q0 = group0.charge;
    
    auto& mob_info1 = group1.mobility_info;
    auto Dtrans1 = D_factor*mob_info1.trans_mobility;
    auto Drot1 = D_factor*mob_info1.rot_mobility;
    auto q1 = group1.charge;
    
    OP_Group_Info gi0{ q0, Dtrans0, Drot0}, gi1{ q1, Dtrans1, Drot1};
    
    //auto q_rad = qb_factor*b_rad;
    
    return std::make_unique< Outer_Propagator>( b_rad, q_rad, has_hi, solvent, gi0, gi1);
  }
  else{
    return { nullptr};
  }
}

//##############################################################
void System_Common::setup_reaction_pathway( Node_Ptr top){
  
  auto rxn_res = value_from_node< string>( std::move(top), "reaction_file");
  if (rxn_res){
    auto& rxn_file = *rxn_res;  
        
    for( auto& group: groups){
      auto gi = group.number;
      for( auto& core: group.cores){
        Mol_Rxn_Ref rxn_ref( gi, core.number);
        mol_rxn_refs.push_back( rxn_ref);
      }
      for( auto& chain: group.chains){
        Mol_Rxn_Ref rxn_ref( gi, chain.number);
        mol_rxn_refs.push_back( rxn_ref);
      }
      auto nd = group.dummies.size();
      for( auto id: range( nd)){
        Mol_Rxn_Ref rxn_ref( gi, id);
        mol_rxn_refs.push_back( rxn_ref);
      }
    }
    
    typedef Pathways::Pathway< Rxn_Interface> Path;
    pathway = std::make_unique< Path>( *this, rxn_file);
    
    // renumber atoms in pathway
    Cor_Cha_Index mci( 0);
    for( auto& group: groups){
      for( auto& core: group.cores){
        pathway->remap_atoms( mci, core.atom_imap);
        ++mci;
      }
      for( auto& chain: group.chains){
        pathway->remap_atoms( mci, chain.atom_imap);
        ++mci;
      }
      for( auto& dummy: group.dummies){
        pathway->remap_atoms( mci, dummy.atom_imap);
        ++mci;
      }
    }
  } 
}

//##################################################
void System_Common::setup_groups( const Node_Ptr& top){

  //cout << "setup groups" << endl;
  auto gnodes = top->children_of_tag( "group");
  
  Group_Index gi(0);
  groups.reserve( Group_Index( gnodes.size()));
  for( auto& gnode: gnodes){
    groups.emplace_back( *this, gnode, gi);
    ++gi;
  }
  
  for( auto& group: groups){
    for( auto& chain: group.chains){
      chain.setup_interactors( *this);
    }
  }
   //cout << "end setup groups" << endl;
  }

//######################################################
void System_Common::setup_reaction_type_and_radii( const Node_Ptr& top){
  
  if (!no_nam){
    if (groups.size() == Group_Index(2)){
      auto b_rad_res = value_from_node< Length>( top, "b_radius");
      if (b_rad_res){
        b_rad = b_rad_res.value();
      }
      else{    
        b_rad = computed_bradius();
      }
      q_rad = qb_factor*b_rad;
    }
  }
   
  check_for_starting_at_site( top);
}

//##############################################################
void System_Common::check_for_starting_at_site( const Node_Ptr& top){

  bool og{ (groups.size() == g1)};
  start_at_site = og;
  
  auto sas_status = value_from_node< string>( top, "start_at_site");
  if (sas_status){
    auto res = sas_status.value();
    auto sas = bool_of_str( res);
    if (!sas && og){
      top->perror( "if only one group, then can only start at site");
    }
    start_at_site = bool_of_str( res);     
  }
}


//##################################################################
std::unique_ptr< Single_Grid< double> >
System_Common::new_density_field( Node_Ptr top) {
  
  auto density_file_res = value_from_node< string>( std::move(top), "density_field");
  if (density_file_res){
    auto& file = *density_file_res;
    return make_unique<  Single_Grid< double> >( file, Pos(Zeroi()));
  }
  else{
    return {};
  }
}

//##################################################
void System_Common::setup_solvent( const Node_Ptr& top){
  auto snode = top->child( "solvent_file");
  if (snode){
    auto fname = value_from_node< string>( snode);
    solvent = Solvent( fname);
  } 
}

//######################################################################
std::pair< bool, size_t> System_Common::has_hydrodynamic_interactions_with_n_update( const Node_Ptr& top) {
  
  bool has_hi_res = true;
  auto hi_status = value_from_node< string>( top, "hydrodynamic_interactions");
  if (hi_status){
    has_hi_res = bool_of_str( *hi_status);
  }
  size_t nu = 1;
  if (has_hi_res){
    auto nu_opt = value_from_node< size_t>(top, "n_steps_between_hi_updates");
    if (nu_opt){
      nu = nu_opt.value();
    }
  }
  
  return make_pair( has_hi_res, nu);
}

//#################################################
void System_Common::check_for_lone_frozen() const{
  
  if (groups.size() == Group_Index(1)){
    auto& group = groups.back();
    auto& cores = group.cores;
    auto& chains = group.chains;
    auto are_nums = [&]( size_t im, size_t ic){
      return cores.size() == Core_Index( im) &&
            chains.size() == Chain_Index( ic);
    };
    
    if (are_nums( 1,0)){
      error( "should not have just one core and nothing else");
    }
    if (are_nums( 0,1)){
      if (!chains.back().never_frozen){
        error( "should not have just one frozen chain");
      }
    }
  }
}

//#################################################
// constructor
System_Common::System_Common( JP::Node_Ptr top){

  //cout << "System Common construct" << endl;
  setup_solvent( top);
  setup_force_fields( top);
  
  std::tie( has_hi, n_steps_per_hi_update) =
    has_hydrodynamic_interactions_with_n_update( top);
  
  setup_groups( top);
  check_for_lone_frozen();
 
  ts_params = Time_Step_Parameters( top);
 
  setup_atom_lone_types( top);
  density_field = new_density_field( top);
   
  setup_reaction_type_and_radii( top);
  setup_reaction_pathway( top);
  outer_propagator = new_outer_propagator();
  
  setup_restraints( top);
  no_nam = has_no_nam( top);
  //cout << "System Common construct end" << endl;
}
//###############################################
bool System_Common::has_no_nam( Node_Ptr top) const{
  
  if (groups.size() != Group_Index(2)){
    return true;
  }
  else if (!has_pathway()){
    return true;
  }
  else{
    auto nnopt = value_from_node< string>( std::move(top), "no_nam");
    if (nnopt){
      auto nn = nnopt.value();
      return bool_of_str( nn);
    }
    else{
      return false;
    }
  }
}

//###################################################################################
template< class CM, class Idx>
Idx with_found_name( const Vector< CM, Idx>& cms,
                            const std::string& name,
                          const std::string& err_msg){
  
  auto cmend = cms.end();
  auto cmbeg = cms.begin();
  auto itr = std::find_if( cmbeg, cmend, [&]( const CM& cm){
    return (cm.name == name);
  });
  if (itr == cmend){
    error( err_msg, name);
  }
  return Idx{ size_t( itr - cmbeg)};
} 

//#######################################################
typedef std::map< Atom_Index, Atom_Index> AIMap;

template< class CM>
const AIMap& cm_imap( const CM& cm){
  
  /*
  if constexpr (std::is_same_v< std::decay_t< decltype( cm)>, Core_Common>){
    return *(cm.atom_imap);
  }
  else{
    return cm.atom_imap;
  }
  */
  return cm.atom_imap;
}

//####################################################################
Atom_Index System_Common::atom_number( Group_Index ig, const std::string& cmname,
                                      bool is_core, Atom_Index ia0) const{
       
  auto& group = groups[ig];
  
  auto anum = [&]( const auto& cms){
    auto err_msg = "not found in group " + group.name;
    auto icm = with_found_name( cms, cmname, err_msg);
    auto& cm = cms[icm];  
      
    auto& imap = cm_imap( cm);
    auto aitr = imap.find( ia0);
    if (aitr == imap.end()){
      error( "atom", ia0, "not found in", cmname);
    }
    return aitr->second;
  };
  
  if (is_core){
    return anum( group.cores);
  }
  else{ // is chain
    return anum( group.chains);
  }
}

//#######################################################
auto System_Common::new_marker( bool is_core, Group_Index ig,
              Atom_Index ia0, const std::string& name) const ->
                  std::pair<Mol_Rxn_Ref, Atom_Index>{
                    
  auto& group = groups[ig];
  const std::string m0 = is_core ? "core" : "chain";
  auto emsg = m0 + " name not found";
  
  if (is_core){
    auto im = with_found_name( group.cores, name, emsg); 
    auto ia = atom_number( ig, name, true, ia0);
    return std::make_pair( Mol_Rxn_Ref( ig, im), ia);
  }
  else{
    auto ic = with_found_name( group.chains, name, emsg); 
    auto ia = atom_number( ig, name, false, ia0);
    return std::make_pair( Mol_Rxn_Ref( ig, ic), ia);    
  }
}

//#############################################################
void System_Common::put_marker( Node_Ptr rnode, int i,
                                   Restraint& restraint) const{
  auto c = std::to_string( i);
  auto atom_tag = "atom" + c;
  auto group_tag = "group" + c;
  auto core_tag = "core" + c;
  auto chain_tag = "chain" + c;
  
  auto group_name = checked_value_from_node< string>( rnode, group_tag);
  const std::string ermsg = "group name not found";
  auto ig = with_found_name( groups, group_name, ermsg);
  auto ia0 = checked_value_from_node< Atom_Index>( rnode, atom_tag);
  auto core_name_opt = value_from_node< string>( rnode, core_tag);
  auto chain_name_opt = value_from_node< string>( rnode, chain_tag);
     
  restraint.markers[i] = [&]{
    if (core_name_opt){ // is core
      auto& name = core_name_opt.value();
      return new_marker( true, ig, ia0, name);
    }
    else{  
      if (!chain_name_opt){
        rnode->perror( "must have a chain or a core", i);
      }
      // is chain
      auto& name = chain_name_opt.value();
      return new_marker( false, ig, ia0, name);
    }
    
  }();
}
   
//##############################################################
void System_Common::setup_restraints( const Node_Ptr& node){
  
  auto rnode0 = node->child( "restraints");
  if (rnode0){
    auto rnodes = rnode0->children_of_tag( "restraint");
    for( const auto& rnode: rnodes){
      Restraint restraint;
      restraint.length = checked_value_from_node< Length>( rnode, "length");
            
      put_marker( rnode, 0, restraint);
      put_marker( rnode, 1, restraint);
      
      restraints.push_back( restraint);
    }
  }
}

//#########################################################
void System_Common::output( std::ofstream &outp) const{
  
  auto ot = [&]( const string& tag, auto t){
    outtag( outp, tag, t, 4);
  };
  
  solvent.output( outp);
    
  ot( "hydrodynamic_interactions", has_hi);
    
  ts_params.output( outp);

  outp << "  <molecule_info>\n";
  if (groups.size() == Group_Index(2)){
    ot( "b_radius", b_rad);
    ot( "b_reaction_rate", outer_propagator->relative_rate( b_rad));
  }      
  ot( "n_reactions", n_reactions());
 
  outp << "  </molecule_info>\n";  
}

//####################################################################
Length System_Common::computed_bradius() const{
  
  if (groups.size() != g2){
    error( "System_Common::computed_bradius: must have exactly 2 groups");
  }
  
  Length smooth0, aextent0, smooth1, aextent1;
  
  tie( smooth0, aextent0) = groups[ g0].field_and_atom_limits( solvent);
  tie( smooth1, aextent1) = groups[ g1].field_and_atom_limits( solvent);
   
  auto rcol = aextent0 + aextent1;
  auto relec0 = smooth0 + aextent1;
  auto relec1 = smooth1 + aextent0;
  
  return max( rcol, max( relec0, relec1));
}

//####################################################################
void System_Common::setup_atom_lone_types( JP::Node_Ptr top){
  
  auto aw_file_res = value_from_node< string>( std::move(top), "atom_weights");
  if (aw_file_res){
    auto aw_file = *aw_file_res;
    
    ifstream aw_input( aw_file);
    JP::Parser aw_parser( aw_input, aw_file);
    
    auto atop_node = aw_parser.top();
    atop_node->complete();
    auto anodes = atop_node->children_of_tag( "atom");
    
    std::map< string, double> atom_weights;
    for( auto& anode: anodes){
      auto atype = checked_value_from_node< string>( anode, "type");
      auto weight = checked_value_from_node< double>( anode, "weight");
      atom_weights.insert( make_pair( atype, weight));
    }
    
    auto setup = [&]( const string& fname,
                     Vector< Atom, Atom_Index>& atoms){
      ifstream input( fname);
      JP::Parser parser( input, fname);
      auto top = parser.top();
      
      Atom_Index ia( 0);
      auto process_residue = [&]( const JP::Node_Ptr& rnode) -> void{
        auto anodes = rnode->children_of_tag( "atom");
        for( const auto& anode: anodes){
          auto type = checked_value_from_node< string>( anode, "name");
          auto itr = atom_weights.find( type);
          if (itr == atom_weights.end()){
            rnode->perror( "atom type ", type, " not found");
          }
          else{
            atoms[ia].mass_weight = Energy( itr->second);
          }
          ++ia;
        }
      };
      
      parser.apply_to_nodes_of_tag( top, "residue", process_residue);
    };
    
    for( auto& group: groups){
      for( auto& core: group.cores){
        setup( core.atoms_file, core.atoms);
      }
      for( auto& chain: group.chains){
        setup( chain.atoms_file, chain.atoms);
      }
    }
  }
}

//############################################
Rxn_Index System_Common::n_reactions() const{
  
  if (pathway){
    return pathway->n_reactions();
  }
  else{
    return Rxn_Index(0);
  }
}

//########################################################################################
std::pair< bool, Rxn_Index>
System_Common::next_completed_reaction( State_Index si, const System_State& state) const{
  
  if (pathway){
    return pathway->next_completed_reaction( si, state);
  }
  else{
    return make_pair( false, Rxn_Index(0));
  }
}

//###################################################################
Length System_Common::reaction_coordinate( const System_State& state,
                                            Rxn_Index ir) const{
  if (pathway){
    return pathway->reaction_coordinate( state, ir).distance;
  }
  else{
    return Length( large);
  }
}

//#######################################################
Pos System_Common::core_atom_pos( const System_State& state,
                                 Mol_Rxn_Ref rxn_ref, Atom_Index ia) const{
  
  auto ig = rxn_ref.ig;
  auto& group_state = state.groups[ig];

  if (rxn_ref.is_core()){
    auto im = rxn_ref.core_index();
    auto& core = group_state.core_states[im];
    auto& core_common = core.common();
    auto& atoms = core_common.atoms;
    auto& atom = atoms[ia];
    return core.transformed( atom.pos);
  }
  else{ // dummy atom
    auto di = rxn_ref.dummy_index();
    auto& group_com = groups[ig];
    auto& dummy = group_com.dummies[di];
    auto& atom = dummy.atoms[ia];
    auto im = dummy.im;
    auto& core = group_state.core_states[im];
    return core.transformed( atom.pos);
  }
}

//#############################################################
Pos System_Common::chain_atom_pos( const System_State& state,
            Mol_Rxn_Ref rxn_ref, Atom_Index ia) {
  
  auto ig = rxn_ref.ig;
  auto ic = rxn_ref.chain_index();
  auto& cstate = state.groups[ig].chain_states[ic];
  return cstate.atoms[ia].pos;
}

//#############################################################
Diffusivity System_Common::core_trans_diff( const Core_Common& core) const{
  
  auto dfactor = solvent.diffusivity_factor();
  auto a = core.h_radius;
  return dfactor/(a*pi6);
}
 
 //#############################################################
Inv_Time System_Common::core_rot_diff( const Core_Common& core) const{
   
  auto dfactor = solvent.diffusivity_factor();
  auto a = core.h_radius;
  return dfactor/(a*a*a*pi8);
}

//#############################################################
std::tuple< Diffusivity, Inv_Time, Pos>
System_Common::diffs_n_center( const System_State& state,
                                Mol_Rxn_Ref mrref) const{
  auto ig = mrref.ig;
  auto im = [&]{
    if (mrref.is_core()){
      return mrref.core_index();
    }
    else{
      auto id = mrref.dummy_index();
      auto ig = mrref.ig;
      auto& group = groups[ig];
      auto& dummy = group.dummies[id];
      return dummy.im;
    }
  }();
  
  auto& core = state.groups[ig].core_states[ im];
  auto& core_com = core.common();
  auto Dt = core_trans_diff( core_com);
  auto Dr = core_rot_diff( core_com);
  auto cen = core.translation();
  return make_tuple( Dt, Dr, cen);
}

//#################################################################
Diffusivity
System_Common::chain_atom_diffusivity( const System_State& state,
                                    Mol_Rxn_Ref mrref, Atom_Index ia) const{
  auto ig = mrref.ig;
  auto ic = mrref.chain_index();
  auto& chain1 = state.groups[ig].chain_states[ ic];
  auto& chain1_com = chain1.common();
  auto a1 = chain1_com.atoms[ia].radius;
  auto dfactor = solvent.diffusivity_factor();
  return dfactor/(a1*pi6);
}

//####################################################################
Diffusivity rot_to_trans( Vec3< double> udpos, Pos rpos, Inv_Time Dr){
  return Dr*norm( cross( rpos, cross( udpos, rpos)));
}

//####################################################################
Diffusivity System_Common::core_chain_atoms_diffu( const System_State& state, Mol_Rxn_Ref mrref0, Atom_Index ia0,
                      Mol_Rxn_Ref mrref1, Atom_Index ia1) const{
   
  auto pos0 = core_atom_pos( state, mrref0, ia0);
  auto [Dt0,Dr0,cen0] = diffs_n_center( state, mrref0);
                    
  auto pos1 = chain_atom_pos( state, mrref1, ia1);
  auto Dt1 = chain_atom_diffusivity( state, mrref1, ia1);
  
  auto udpos = normed( pos1 - pos0);
  auto dcen = pos1 - cen0;
  auto rpos0 = pos0 - cen0;
  
  auto Dt = dot( udpos, normed( dcen))*( Dt0 + Dt1);          
  auto Dtr0 = rot_to_trans( udpos, rpos0, Dr0);
  
  return Dt + Dtr0;
}

//######################################################################
Diffusivity System_Common::core_core_atoms_diffu( const System_State& state,
                                   Mol_Rxn_Ref mrref0, Atom_Index ia0,
                                   Mol_Rxn_Ref mrref1, Atom_Index ia1) const{
  
  auto pos0 = core_atom_pos( state, mrref0, ia0);
  auto [Dt0,Dr0,cen0] = diffs_n_center( state, mrref0);
  
  auto pos1 = core_atom_pos( state, mrref1, ia1);
  auto [Dt1,Dr1,cen1] = diffs_n_center( state, mrref1);
          
  auto udpos = normed( pos1 - pos0);
  auto dcen = cen1 - cen0;
  auto rpos0 = pos0 - cen0;
  auto rpos1 = pos1 - cen1;
  
  auto Dt = dot( udpos, normed( dcen))*( Dt0 + Dt1);
  
  auto Dtr0 = rot_to_trans( udpos, rpos0, Dr0);
  auto Dtr1 = rot_to_trans( udpos, rpos1, Dr1);
  
  return Dt + Dtr0 + Dtr1;
}

//#############################################################

Diffusivity
System_Common::reaction_diffusivity( const System_State& state,
                                      Rxn_Pair rpair) const{
  
  auto cm0 = rpair.mol0;
  auto& mrref0 = mol_rxn_refs[ cm0];
  auto ia0 = rpair.atom0;
  
  auto cm1 = rpair.mol1;
  auto& mrref1 = mol_rxn_refs[ cm1];
  auto ia1 = rpair.atom1;
    
  auto isco0 = mrref0.is_core() || mrref0.is_dummy();
  auto isco1 = mrref1.is_core() || mrref1.is_dummy();
   
  if (isco0 && isco1){
    return core_core_atoms_diffu( state, mrref0, ia0, mrref1, ia1);
  }
  
  else if (!isco0 && !isco1){
    auto Dt0 = chain_atom_diffusivity( state, mrref0, ia0);
    auto Dt1 = chain_atom_diffusivity( state, mrref1, ia1); 
    return Dt0 + Dt1;
  }
  else if (isco0 && !isco1){
    return core_chain_atoms_diffu( state, mrref0, ia0, mrref1, ia1);
  }
  else{ // !isco0 && isco1
    return core_chain_atoms_diffu( state, mrref1, ia1, mrref0, ia0);
  }
}

//####################################################################
// tacked on the required distance at the end, a cludge
std::tuple< Length, Diffusivity, Length>
System_Common::reaction_coord_n_diffusivity( const System_State& state,
                                              Rxn_Index ir) const{
  
  if (pathway){
    auto rpair = pathway->reaction_coordinate( state, ir);
    auto D = reaction_diffusivity( state, rpair);
    return std::make_tuple( rpair.distance, D, rpair.req_distance);  
  }
  
  else{
    return std::make_tuple( Length( large), Diffusivity( NaN), Length( NaN));
  }
}

//#################################################
Vector< std::string, Rxn_Index> System_Common::rxn_names() const{
  
  Vector< std::string, Rxn_Index> res;
  auto nr = pathway->n_reactions();
  for( auto ir: range( nr)){
    res.push_back( pathway->reaction_name( ir));
  }
  return res;
}

//###################################################################################
  System_Common::System_Common( Test_Tag){}


}
