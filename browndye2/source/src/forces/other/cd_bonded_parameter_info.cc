#include <fstream>
#include "../../global/pi.hh"
#include "cd_bonded_parameter_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

Vector< double> angles_of_node( const JP::Node_Ptr& node){
  
  auto sdata = vector_from_node< double>( node);
  for( auto& x: sdata){
    x *= angle_conv_factor;
  }
  return sdata;
}

//################################################################
void CD_Bonded_Parameter_Info::set_pairs( Parser& parser, const Node_Ptr& node){
  
  auto get_poten = [&]( const JP::Node_Ptr& node){
    node->complete();
    auto orders = checked_array_from_node< int, 2>( node, "orders");
    if (orders[0] != 0 || orders[1] != 0){      
      bond_types.emplace_back( *this, node);
    }
  };
  
  Array< std::string, 2> tags{ "distance", "potentials"};
  
  auto ldistance = [&]( Node_Ptr& cnode){
    cnode->complete();
    distances = array_from_node< Length, 2>( cnode);
  };
  
  auto lpotentials = [&]( Node_Ptr& cnode){
    parser.apply_to_nodes_of_tag( cnode, "potential", get_poten);
  };
  
  auto res = apply_to_each_child_with_tag( node, tags, ldistance, lpotentials);
  if (!res){
    node->perror( "not all required tags present");
  }
}

//##################################################################
void CD_Bonded_Parameter_Info::set_angles( Parser& parser, const Node_Ptr& node){
  
  auto get_bond_angle = [&]( const JP::Node_Ptr& node) -> void{
    angle_types.emplace_back( *this, node);     
  };
  
  auto langle = [&]( Node_Ptr& cnode){
    bond_angle_values = angles_of_node( cnode);
  };
  
  auto lpotentials = [&]( Node_Ptr& cnode){
    parser.apply_to_nodes_of_tag( cnode, "potential", get_bond_angle);
  };
  
  Array< std::string,2> tags{"angle", "potentials"};
  
  auto res = apply_to_each_child_with_tag( node, tags, langle, lpotentials);
  if (!res){
    node->perror( "not all required tags present");
  }
}

//#############################################################
void CD_Bonded_Parameter_Info::set_dihedrals( Parser& parser, const Node_Ptr& node){
  
  auto get_dihedral_angle = [&]( const JP::Node_Ptr& node) -> void{
    dihedral_types.emplace_back( *this, node);
  };
  
  Array< std::string,2> tags{"angle", "potentials"};
   
  auto langle = [&]( const Node_Ptr& cnode){
    dihedral_angle_values = angles_of_node( cnode);
  };
  auto lpotential = [&]( const Node_Ptr& cnode){
    parser.apply_to_nodes_of_tag( cnode, "potential", get_dihedral_angle);
  };
  
  auto res = apply_to_each_child_with_tag( node, tags, langle, lpotential);
  if (!res){
    node->perror( "not all required tags present");
  }
}

//##################################################################
CD_Bonded_Parameter_Info::CD_Bonded_Parameter_Info( Parser& parser){
  
  Array< std::string, 4> tags{ "units", "pairs", "bond_angles", "dihedral_angles"};
  
  auto top_node = parser.top();
  
  auto lenergy = [&]( const Node_Ptr& cnode){
    set_energy_units( cnode);
  };
  auto lpairs = [&]( const Node_Ptr& cnode){
    set_pairs( parser, cnode);
  };
  auto langles = [&]( const Node_Ptr& cnode){
    set_angles( parser, cnode);
  };
  auto ldihedrals = [&]( const Node_Ptr& cnode){
    set_dihedrals( parser, cnode);
  };
  
  apply_to_each_child_with_tag( top_node, tags,
                               lenergy, lpairs, langles, ldihedrals);
  
}
}

/*
int main(){
  CD_Bonded_Parameter_Info info( "bull.xml");
  std::out << info.angle_types.size() << " " << info.dihedral_types.size() << "\n";
  
}
*/