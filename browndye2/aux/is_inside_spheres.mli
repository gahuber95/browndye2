(*
Used internally.  This module is used to construct tree data structures
of spheres and bounding triangles, and to use those structures to determine 
whether a point lies inside the assemblage of spheres.

*)

type sphere = {
  spos : Vec3.t;
  radius : float;
  mutable number : int;
  mutable active: bool;
  actual_number: int;
}

type 'a branch = {
  bpos : Vec3.t;
  bradius : float;
  children : 'a tree * 'a tree;
}
and 'a leaf = {
  lpos : Vec3.t;
  lradius : float;
  child : 'a;
}
and 'a tree = Leaf of 'a leaf | Branch of 'a branch | Empty

type sphere_container = {
  mutable tree : sphere tree;
  low_corner : Vec3.t;
  high_corner : Vec3.t;
  spheres : sphere array;
  mutable zlist: sphere list;
}

val is_inside_spheres :
  (int * int * int) tree ->
  sphere array -> Vec3.t -> Vec3.t -> bool

module Spheres :
  sig
    val circumcircle :
      Vec3.t -> Vec3.t -> Vec3.t -> Vec3.t * float
    val smallest_enclosing_sphere :
      Vec3.t array -> Vec3.t * float
  end

val search_tree :
  ('a -> Vec3.t) -> ('a -> float) -> 'a array -> 'a tree

val partitioned_spheres :
  sphere array -> sphere_container

val fold_tree_within_distance :
  ('b -> bool) -> 
  Vec3.t -> float -> ('b -> 'c) -> ('c -> 'c -> 'c) -> 'b tree -> 'c option
  
val smallest_enclosing_sphere_of_spheres : sphere array -> Vec3.t * float

val nearest_neighbors : ('a -> Vec3.t) -> 'a array -> ('a, 'a) Hashtbl.t
  
val update_sphere_container: sphere_container -> unit

