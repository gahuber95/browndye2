#pragma once

#include "../global/pos.hh"

namespace Browndye{
  
  template< size_t n, class Index>
  Pos centroid( const Array< Pos,n, Index>& poses){
    
    Pos sum( Zeroi{});
    for( auto k: range( Index(n))){
      sum += poses[k];
    }
    return (1.0/n)*sum;
  }
  
  template< class V, class W>
  Pos weighted_centroid( const V& poses, const W& wts){
    
    typedef typename W::value_type Wt;
    typedef decltype( Wt()*Pos{}) WPos;
    
    Wt wsum{ 0.0};
    WPos sum( Zeroi{});
    auto n = poses.size();
    for( auto k: range( n)){
      auto pos = poses[k];
      auto wt = wts[k];
      sum += wt*pos;
      wsum += wt;
    }
    return (1.0/wsum)*sum;   
  }
  
}
