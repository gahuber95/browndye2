#pragma once

#include <array>
#include <optional>
#include <functional>
#include "default_values.hh"

/*
Used to define a small array whose length is fixed and
known at compile-time.  It is used instead of ordinary
C arrays for two reasons. First, bounds checking can be
turned on by compiling with the DEBUG flag. Second,
default values, like NaN, are automatically put in 
when the DEBUG flag is set; this helps debugging.

Useful classes:

template< class Type, unsigned int n> SVec;

specialized to 3-D
template< class T> Vec3; 

Matrix
template< class Type, unsigned nr, unsigned int nc> SMat;


 */

#include "range.hh"
#include "../global/limits.hh"
#include "../global/error_msg.hh"
#include "default_values.hh"
#include "index.hh"
#include "index_selector.hh"
#include "../global/debug.hh"

namespace Browndye{

class Zeroi{};
class NaNi{};


template< class T, size_t n, class Idx = size_t>
class Array: public std::array< T, n>{

public:
  typedef std::array< T,n> Parent;
  typedef typename Parent::reference reference;
  typedef typename Parent::const_reference const_reference;
  typedef Idx index_type;

  Parent& parent(){
    return static_cast< Parent&>( *this);
  }

  Parent parent() const{
    return static_cast< Parent>( *this);
  }
  
  //using Parent::Parent;

  reference operator[]( Idx i){
    check_bounds( i);
    return Parent::operator[]( value(i));
  }

  const_reference operator[]( Idx i) const{
    check_bounds( i);
    return Parent::operator[]( value(i));
  }
  
  Array(){
    if constexpr (debug){
      typedef JAM_Vector::Default_Value< T> DV;
      for( auto& x: *this)
        x = DV::value();
    }
  }

  explicit Array( Zeroi);
  explicit Array( NaNi);

  Array( std::array< T,n>);

  Array( const Array& other) = default;
  Array( Array&& other) = default;
  Array& operator=( const Array& v) = default;
  Array& operator=( Array&& v) = default;
  Array& operator=( std::array< T,n>);
  
  template< class U>
  Array& operator=( Array< U, n, Idx> other);
  
  Array( std::initializer_list< T> init){
    Idx i{ 0};
    for( auto x: init){
      (*this)[i] = x;
      ++i;
    }
  }
  
  //Array( std::initializer_list< T> init): std::array<T,n>( init){}

  Idx size() const{
    return Idx(n);
  }

  static
  constexpr size_t isize(){
    return n;
  }

  reference at( Idx i){
    check_bounds( i);
    return Parent::at(value(i));
  }

  const_reference at( Idx i) const{
    check_bounds( i);
    return Parent::at(value(i));
  }
  
  Array& operator+=( const Array& other){
    for( auto i: range(n))
      (*this)[i] += other[i];
    return *this;
  }

  Array& operator-=( const Array& other){
    for( auto i: range(n))
      (*this)[i] -= other[i];
    return *this;
  }

  Array operator-() const;

  void fill( const T& t){
    Parent::fill( t);
    //for( auto i: range(n))
      //(*this)[i] = t;
  }


  template< class F>
  auto mapped( F&& f) const{
    Array< std::result_of_t<F( T)>, n, Idx> res;
    for( auto k: range( Idx(n))){
      res[k] = f( (*this)[ k]);
    }
    return res;
  }
    
  template< class F, class U>
  auto folded_left( F&&f, U&& u) const{
    U res = u;
    for( auto i: range(n))
      res = f( res, (*this)[i]);
    return res;
  }

 template<class F>
 std::optional< std::pair< size_t, std::reference_wrapper< const T> > >
 found( F&& f) const;
  
 #ifdef DEBUG
 ~Array(){
   for( auto i: range(Idx{n})){
     (*this)[i] = JAM_Vector::Default_Value< T>::value();
   }
 }
 #endif

  // make sure size_t versions are not visible when index is not an integer type
  static constexpr bool is_int = std::is_integral< Idx>::value;
  typedef typename Index_Selector::IBlocker< is_int>::type CType;

  reference operator[]( CType) = delete;
  const_reference operator[]( CType) const = delete;

  reference at( CType) = delete;
  const_reference at( CType) const = delete;

private:
  void check_bounds( Idx i) const{
    if (bounds_check){
      if (value(i) >= n){
        error( "Array: out of bounds: ", i, " ", this->size());
      }
    }
  }

  static
  size_t value( Idx i){
    return Index_NS::value( i);
  }


};


namespace Array_Details {
  // Thanks to https://en.cppreference.com/w/cpp/experimental/make_array
  template<class> struct is_ref_wrapper : std::false_type {};
  template<class T> struct is_ref_wrapper<std::reference_wrapper<T>> : std::true_type {};
 
  template<class T>
  using not_ref_wrapper = std::negation<is_ref_wrapper<std::decay_t<T>>>;
 
  template <class D, class...> struct return_type_helper { using type = D; };
  template <class... Types>
  struct return_type_helper<void, Types...> : std::common_type<Types...> {
      static_assert(std::conjunction_v<not_ref_wrapper<Types>...>,
                    "Types cannot contain reference_wrappers when D is void");
  };
 
  template <class D, class... Types>
  using return_type = Array<typename return_type_helper<D, Types...>::type,
                                 sizeof...(Types)>;
}
 
template < class D = void, class... Types>
constexpr Array_Details::return_type<D, Types...> make_array(Types&&... t) {
  return {std::forward<Types>(t)... };
}


template< class S, class T, size_t n, class Idx>
S& operator<<( S& s, const Array< T,n,Idx>& v){
  for( auto i: range( Idx{n}))
    s << v[i] << " ";
  return s;
}

template< class S, class T, size_t n, class Idx>
S& operator>>( S& s, Array< T,n,Idx>& v){
  for( auto i: range( Idx{n}))
    s >> v[i];
  return s;
}

template< class T>
class Zero_Type{
public:
  static
  T value(){
    return T{0.0};
  }
};

template<>
class Zero_Type< int>{
public:
  static
  int value(){
    return 0;
  }
};

template<>
class Zero_Type< size_t>{
public:
  static
  size_t value(){
    return 0;
  }
};



template< class T, size_t n, class Idx>
class Zero_Type< Array< T,n,Idx> >{
public:
  static
  Array< T,n,Idx> value(){
    return Array< T,n,Idx>( Zeroi{});
  }
};

template< class T, size_t nr, size_t nc, class Idxr, class Idxc>
class Matrix;

template< class T, size_t nr, size_t nc, class Idxr, class Idxc>
class Zero_Type< Matrix< T,nr,nc, Idxr, Idxc> >{
public:
  static
  Matrix< T,nr,nc, Idxr,Idxc> value(){
    return  Matrix< T,nr,nc, Idxr,Idxc>( Zeroi{});
  }
};


template< class T>
class NaN_Type{
public:
  static
  T value(){
    return T{NaN};
  }
};

template< class T, size_t n, class Idx>
class NaN_Type< Array< T,n,Idx> >{
public:
  static
  Array< T,n,Idx> value(){
    return  Array< T,n,Idx>( NaNi{});
  }
};



// constructor
template< class T, size_t n, class Idx>
Array< T,n,Idx>::Array( Zeroi){
  fill( Zero_Type< T>::value());
}

template< class T, size_t n, class Idx>
Array< T,n,Idx>::Array( NaNi){
  fill( NaN_Type< T>::value());
}

template< class T, size_t n, class Idx>
Array< T,n,Idx>::Array( std::array< T,n> other){
  std::copy( other.begin(), other.end(), this->begin());
}

template< class T, size_t n, class Idx>
auto Array< T,n,Idx>::operator=( std::array< T,n> other) -> Array&{
  std::copy( other.begin(), other.end(), this->begin());
  return *this;
}

template< class T, size_t n, class Idx>
template< class U>
auto Array< T,n,Idx>::operator=( Array< U, n, Idx> other) -> Array&{
  
  std::copy( other.begin(), other.end(), this->begin());
  return *this;
}


template< class T, size_t n0, size_t n1>
Array< T, n0+n1> concated( Array< T,n0> a0, Array< T,n1> a1){

  Array< T, n0+n1> a{};
  for( auto i: range( n0)){
    a[i] = a0[i];
  }
  for( auto i: range( n1)){
    a[ n0+i] = a1[i];
  }
  return a;
}

//***********************************************
template< class T>
using Vec3 = Array< T, 3>;

template< class T, size_t nr, size_t nc,
	  class Idxr=size_t, class Idxc=size_t>
class Matrix: public Array< Array< T, nc, Idxc>, nr, Idxr>{
public:
  typedef Array< Array< T, nc, Idxc>, nr, Idxr> Super;
  typedef Super Supert;
  using Super::Super;

  explicit Matrix( const Supert& s): Super(s){}

  Matrix(): Super(){
    if constexpr( debug){
      typedef JAM_Vector::Default_Value< T>  DV;
        for( auto& arr: *this)
          for( auto& x: arr)
            x = DV::value();
    }
  }

  explicit Matrix( Zeroi){
    for( auto& row: (*this))
      row = Array< T,nc,Idxc>( Zeroi{});
  }

  Matrix( const Matrix& other) = default;
  Matrix( Matrix&& other) = default;
  Matrix& operator=( const Matrix& v) = default;
  Matrix& operator=( Matrix&& v) = default;

  const Supert& super() const{
    return static_cast< const Supert&>(*this);
  }

  Matrix& operator+=( const Matrix& other){
    for( auto i: range( nr))
      (*this)[i] += other[i];
    return *this;
  }

  Matrix& operator-=( const Matrix& other){
    for( auto i: range( nr))
      (*this)[i] -= other[i];
    return *this;
  }

  Matrix operator-() const;
};

template< class T, size_t nr, size_t nc>
using SMat = Matrix< T, nr,nc, size_t, size_t>;

namespace Arrays{

  template< class T, size_t n, size_t i>
  void filldm( SMat< T,n,n>&){
    static_assert( i == n, "wrong number of args");
  }
  
  template< class T, size_t n, size_t i, class T0, class ...Ts>
  void filldm( SMat< T,n,n>& a, T0 t, Ts... rest){
    a[i][i] = t;
    filldm< T,n,i+1, Ts...>( a, rest...);
  }

  /*
  template< class>
  class FunWrap;

  template< class F, class Arg>
  class FunWrap< std::function< F(Arg)> >{
  public:
    typedef Arg Arg0;
  };
  */
}

template< size_t n, class T, class ...Ts>
SMat< T, n,n> diag_matrix( T t, Ts ... ts){

  SMat< T,n,n> a{ Zeroi{}};
  Arrays::filldm< T,n,0,Ts...>( a, t, ts...);
  return a;
}

template< class T>
using Mat3 = SMat< T, 3,3>;


//*****************************************************
template< class Ta, class Tb, size_t n, class Idx>
Array< decltype(Ta() + Tb()),n,Idx> 
operator+( Array< Ta,n,Idx> a, Array< Tb,n,Idx> b){
  
  Array< decltype( Ta() + Tb()),n,Idx> c{};
  for( auto i: range(n))
    c[i] = a[i] + b[i];
  return c;
}
 
template< class Ta, class Tb, size_t n, class Idx>
Array< decltype(Ta() - Tb()), n,Idx> 
operator-( Array< Ta,n,Idx> a, Array< Tb,n,Idx> b){
  
  Array< decltype(Ta() - Tb()), n,Idx> c{};
  for( auto i: range(n))
    c[i] = a[i] - b[i];
  return c;
}

template< class T, size_t n, class Idx>
Array< T, n, Idx>
Array< T,n,Idx>::operator-() const{
  return -1.0*(*this);
}

template< class Ta, class Tb, size_t n, class Idx>
Array< decltype(Ta()*Tb()), n, Idx> 
operator*( Ta a, Array< Tb,n,Idx> b){

  Array< decltype(Ta()*Tb()), n, Idx> c{};
  for( auto i: range(n))
    c[i] = a*b[i];
  return c;
}

template< class Ta, class Tb, size_t n, class Idx>
Array< decltype(Ta()*Tb()), n, Idx> 
operator*( Array< Ta,n,Idx> a, Tb b){

  Array< decltype(Ta()*Tb()), n, Idx> c{};
  for( auto i: range(n))
    c[i] = a[i]*b;
  return c;
}

template< class Ta, class Tb, size_t n, class Idx>
Array< decltype(Ta()/Tb()), n, Idx> 
operator/( Array< Ta,n,Idx> a, Tb b){

  Array< decltype(Ta()/Tb()), n, Idx> c{};
  for( auto i: range(n))
    c[i] = a[i]/b;
  return c;
}

template< class Ta, class Tb, size_t nr, size_t nc, class Idxr, class Idxc>
Matrix< decltype( Ta()*Tb()), nr,nc, Idxr, Idxc>
operator*( Ta a, Matrix< Tb, nr,nc, Idxr, Idxc> b){

  typedef decltype( Ta()*Tb()) Tab;
  Matrix< Tab, nr,nc, Idxr, Idxc> c{};
  for( auto ir: range(nr)){
    for( auto ic: range( nc))
      c[ir][ic] = a*b[ir][ic];
  }
  return c;
}

template< class Ta, class Tb, size_t nr, size_t nc, class Idxr, class Idxc>
Matrix< decltype( Tb()*Ta()), nr,nc, Idxr, Idxc>
operator*( Matrix< Tb, nr,nc, Idxr, Idxc> b, Ta a){

  typedef decltype( Tb()*Ta()) Tab;
  Matrix< Tab, nr,nc, Idxr, Idxc> c{};
  for( auto ir: range(nr)){
    for( auto ic: range( nc))
      c[ir][ic] = b[ir][ic]*a;
  }
  return c;
}

template< class Ta, class Tb, size_t nr, size_t nc, class Idxr, class Idxc>
Matrix< decltype( Tb()/Ta()), nr,nc, Idxr, Idxc>
operator/( Matrix< Tb, nr,nc, Idxr, Idxc> b, Ta a){

  typedef decltype( Tb()/Ta()) Tab;
  Matrix< Tab, nr,nc, Idxr, Idxc> c{};    
  for( auto ir: range(nr)){
    for( auto ic: range( nc))    
      c[ir][ic] = b[ir][ic]/a;
  }
  return c;
}

template< class Ta, size_t na, class Idxa, class Tb, size_t nb, class Idxb>
Array< decltype( Ta()*Tb()), na, Idxa>
operator*( Matrix< Ta, na, nb, Idxa, Idxb> a, Array< Tb, nb, Idxb> b){

  typedef decltype( Ta()*Tb()) Tab;
  Array< Tab, na, Idxa> c{};
  for( auto ia: range( na)){  
    auto row = a[ia];
    auto sum = Tab( 0.0);
    for( auto ib: range( nb))
      sum += row[ib]*b[ib];
    c[ia] = sum;
  }
  return c;
}   

template< class Ta, size_t na, class Idxa, size_t nc, class Idxc,
          class Tb, size_t nb, class Idxb>
Matrix< decltype(Ta()*Tb()), na,nb, Idxa, Idxb>
operator*(  Matrix< Ta, na,nc, Idxa, Idxc> a,
            Matrix< Tb, nc,nb, Idxc, Idxb> b){

  typedef decltype( Ta()*Tb()) Tab;
  Matrix< Tab, na,nb, Idxa, Idxb> c{};
  for( auto ia: range( na)){  
    auto rowa = a[ia];
    for( auto ib: range( nb)){
      auto sum = Tab( 0.0);
      for( auto ic: range( nc))
        sum += rowa[ic]*b[ic][ib];
      c[ia][ib] = sum;
    }
  }
  return c;
}

template< class Ta, class Tb, size_t nr, size_t nc, class Idxr, class Idxc>
Matrix< decltype(Ta() + Tb()),nr,nc,Idxr,Idxc> 
operator+( Matrix< Ta,nr,nc,Idxr,Idxc> a, 
           Matrix< Tb,nr,nc,Idxr,Idxc> b){

  typedef decltype( Ta() + Tb()) Tab;
  typedef Matrix< Tab,nr,nc,Idxr,Idxc> Res;
  return Res( a.super() + b.super());
}

template< class Ta, class Tb, size_t nr, size_t nc, class Idxr, class Idxc>
Matrix< decltype(Ta() - Tb()),nr,nc,Idxr,Idxc> 
operator-( Matrix< Ta,nr,nc,Idxr,Idxc> a, 
           Matrix< Tb,nr,nc,Idxr,Idxc> b){

  typedef decltype( Ta() - Tb()) Tab;
  typedef Matrix< Tab,nr,nc,Idxr,Idxc> Res;
  return Res( a.super() - b.super());
}

template< class T, size_t nr, size_t nc, class Idxr,class Idxc>
Matrix< T, nr,nc, Idxr,Idxc>
Matrix< T, nr,nc, Idxr,Idxc>::operator-() const{
  return -1.0*(*this);
}

template< class Ta, class Tb, size_t n, class Idx>
decltype( Ta()*Tb()) dot( Array< Ta, n, Idx> a, Array< Tb, n, Idx> b){

  decltype( Ta()*Tb()) sum( 0.0);
  for( auto i: range( n))
    sum += a[i]*b[i];
  return sum;
}

// make more generic and efficient
template< class Ta, class Tb, size_t na, size_t nb>
Matrix< decltype( Ta()*Tb()), na, nb, size_t, size_t> outer( Array< Ta, na> a, Array< Tb, nb> b){
  
  Matrix< decltype( Ta()*Tb()), na, nb, size_t, size_t> res;
  for( auto ia: range( na))
    for( auto ib: range( nb))
        res[ia][ib] = a[ia]*b[ib];
  return res;
}


template< class Ta, class Tb, class Idx>
Array< decltype( Ta()*Tb()), 3u, Idx>
cross( Array< Ta, 3u, Idx> a, Array< Tb, 3u, Idx> b){

  Array< decltype( Ta()*Tb()), 3u, Idx> c{};
  auto i0 = Idx(0);
  auto i1 = Idx(1);
  auto i2 = Idx(2);

  c[i0] = a[i1]*b[i2] - a[i2]*b[i1];
  c[i1] = a[i2]*b[i0] - a[i0]*b[i2];
  c[i2] = a[i0]*b[i1] - a[i1]*b[i0];

  return c;
}

template< class Ta, class Tb>
Mat3< decltype( Ta()*Tb())>
cross( Mat3< Ta> A, Vec3< Tb> b){

  Mat3< decltype(Ta()*Tb())> res{};
  for( auto i: range(3))
    res[i] = cross( A[i], b);

  return res;
}

template< class Ta, class Tb>
Mat3< decltype( Ta()*Tb())>
cross( Vec3< Tb> b,  Mat3< Ta> A){

  return -cross( transpose(A),b);
}

template< class T, size_t n, class Idx>
decltype(T()*T()) distance2( Array<T,n,Idx> a, Array< T,n,Idx> b){
  
  decltype( T()*T()) sum( 0.0);
  for( auto i: range( n)){
    auto d = a[i] - b[i];    
    sum += d*d;
  }
  return sum;
}

template< class T, size_t n, class Idx>
T distance( Array<T,n,Idx> a, Array< T,n,Idx> b){
  
  return sqrt( distance2( a,b));
}

/*
template< size_t n, class Idx>
double distance( Array<double,n,Idx> a, Array< double,n,Idx> b){
  
  return std::ssqrt( distance2( a,b));
}
*/

template< class T, size_t n, class Idx>
decltype(T()*T()) norm2( Array< T, n, Idx> a){
  decltype( T()*T()) sum( 0.0);
  for( auto i: range( n))
    sum += a[i]*a[i];
  
  return sum;
}

template< class T, size_t n, class Idx>
T norm( Array< T, n, Idx> a){
  return sqrt( norm2( a));
}

/*
template< size_t n, class Idx>
double norm( Array< double, n, Idx> a){
  return std::ssqrt( norm2( a));
}
*/


template< class T, size_t n, class Idx>
Array< double, n, Idx> normed( Array< T, n, Idx> a){
  auto r = norm( a);
  return a/r;
}

template< class T, size_t n, class Idx>
void set_zero( Array< T, n, Idx>& a){
  a.fill( T(0.0));
}

template< size_t n>
SMat< double, n,n> id_matrix(){
  SMat< double, n,n> a{ Zeroi{}};
  for( auto i: range(n))
    a[i][i] = 1.0;
  return a;
}

template< size_t n>
SMat< double, n,n> zero_matrix(){
  SMat< double, n,n> a{ Zeroi{}};
  return a;
}

template< class T, size_t nr, size_t nc>
SMat< T,nc,nr> transpose( const SMat< T, nr,nc>& a){

  SMat< T,nc,nr> b{};
  for( auto ir: range( nr))
    for( auto ic: range( nc))      
      b[ic][ir] = a[ir][ic];
  return b;
}

template< class T, size_t n>
T trace( const SMat< T,n,n>& a){

  T sum{ 0.0};
  for( auto i: range( n))
    sum += a[i][i];
  return sum;
}

template< class S, class T, size_t nr, size_t nc>
void print( S& str, SMat< T, nr,nc> A){
  for( auto ir: range( nr)){
    for( auto ic: range( nc))
      str << A[ir][ic] << " ";
    str << "\n";
  }
  str << "\n";
}

template< class T, size_t n>
Array< T,n> aconverted( Array< T,n> a){
  return a;
}

template< size_t n>
Array< std::string, n> aconverted( Array< const char*, n> a){
  Array< std::string, n> res;
  for( size_t i = 0; i < n; i++)
    res[i] = a[i];
  return res;
}

template< class T0>
auto new_array( T0 t0){
  return aconverted( Array< T0,1>{ t0});
}

template< class T0>
auto new_array(){
  return aconverted( Array<T0,0>{});
}

template< class T0, class ...Ts>
auto new_array( T0 t0, Ts... ts){
  constexpr size_t n = sizeof...(ts);
  auto tail = new_array( ts...);
  decltype( aconverted( Array< T0, n+1>())) res;
  res[0] = t0;
  for( size_t i = 0; i < n; i++)
    res[i+1] = tail[i];
  return res;
}


  template< class T, size_t n, class Idx>
  template<class F>
  std::optional< std::pair< size_t, std::reference_wrapper< const T> > >
  Array< T, n, Idx>::found( F&& f) const{
    
    for( auto i: range( n)){
      const T& t = (*this)[i];
      if (f(t)){
    	return std::make_pair( i, std::cref(t)); 
      }
    }
    return std::nullopt; 
  }

  template< size_t n, class F>
  auto initialized_array( F&& f) -> Array< decltype(f(size_t())), n>{

    typedef decltype(f(size_t())) T;
    Array< T,n> res;
    for( auto i: range( res.size())){
      res[i] = f(i);
    }
    return res;
  }

  /*
  template< size_t n, class F>
  auto initialized_array( F f){

    std::function ff( f);
    typedef decltype( ff) FF;
    //typedef typename Arrays::FunWrap< FF>::Arg0 Idx;
    typedef typename FF::argument_type Idx; // change for C++20
    
    //typedef std::result_of_t< F(Idx)> T;
    typedef typename FF::result_type T;
    Array< T, n, Idx> res;
    for( auto i: range( n)){
      res[i] = f(i);
    }
    return res;
  }
  */  
}

