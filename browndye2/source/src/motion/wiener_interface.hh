#pragma once

#include "../global/browndye_rng.hh"
#include "wiener_vector.hh"
#include "../molsystem/system_state.hh"
#include "../molsystem/system_thread.hh"

namespace Browndye{

class Wiener_Interface{
public:
  typedef ::Time Time;

  typedef Wiener_Vector W_Vector;
  typedef Browndye_RNG Gaussian;
  typedef System_State System;
  
  typedef Vec3< Sqrt_Time> DW;
 
  static
  void set_difference( const W_Vector& a, const W_Vector& b, W_Vector& c);

  static
  W_Vector difference( const W_Vector& a, const W_Vector& b){
    W_Vector c{ a.copy()};
    set_difference( a,b,c);
    return c;
  }

  static
  void set_half( const W_Vector& a, W_Vector& b);
   
  static 
  void add_gaussian( System& state, Sqrt_Time sdev, W_Vector& w);

  static 
  void set_gaussian( System& state, Sqrt_Time sdev, W_Vector& w);
  
  static
  void set_zero( W_Vector& w);
  
  static
  Gaussian& gaussian( System& state);
  
  static
  W_Vector wiener_vector( System& state);
 
  static
  std::pair< bool,bool> advance( System& system, [[maybe_unused]] Time t, Time dt,
                                const W_Vector& ws);
   
  static  
  void step_back( System& state, [[maybe_unused]] Time t, [[maybe_unused]] Time dt);

  static
  W_Vector copy( const W_Vector& v){
    return v.copy();
  }

};


}

