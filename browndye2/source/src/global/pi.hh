#pragma once

namespace Browndye{
  constexpr double pi = 3.141592653589793238;
  constexpr double pi2 = 2*pi;
  constexpr double pi6 = 6*pi;
  constexpr double pi4 = 4*pi;
  constexpr double pi8 = 8*pi;
  
  constexpr double angle_conv_factor = pi/180.0;
}
