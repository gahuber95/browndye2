#pragma once

#include "../lib/array.hh"
#include "../lib/units.hh"
#include "../global/indices.hh"
#include "../global/pos.hh"
#include "v4.hh"

namespace Browndye{

// may need to change algorithm if ntet != 4
V4< F3> tet_vertex_forces( const V4< Pos>& rs, F3 F, T3 T);

}

