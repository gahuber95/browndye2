#pragma once

#include "../global/browndye_rng.hh"
#include "system_common.hh"
#include "../group/group_thread.hh"
#include "system_mobility_interface.hh"
#include "../group/saved_group_state.hh"

namespace Browndye{


class System_Thread{
public:
  System_Thread( const System_Common&, Thread_Index number, const Vector< uint32_t>& );
  System_Thread() = delete;
  System_Thread( const System_Thread&) = delete;
  System_Thread& operator=( const System_Thread&) = delete;
  System_Thread( System_Thread&&) = default;
  System_Thread& operator=( System_Thread&&) = default;
  ~System_Thread() = default;

  const System_Common& common() const{
    return common_ref.get();
  }

  Browndye_RNG rng;
  Vector< Group_Thread, Group_Index> groups;
  Thread_Index number{ maxi};
  
  //Vector< Saved_Group_State, Group_Index> saved;
  //State_Index saved_rxn_state{ maxi};
  //Rxn_Index saved_last_rxn{ maxi};

  struct Mob_Info_And_Doer{
    const Mobility_Info* mob_info = nullptr;
    typedef Generic_Mobility_Full::Doer< System_Mobility_Interface> Doer;
    Doer doer;
    
    Mob_Info_And_Doer( const Mobility_Info& mi, Doer&& dr):
      doer( std::move( dr)){
      mob_info = &mi;
    }
      
    Mob_Info_And_Doer copy() const{
      return Mob_Info_And_Doer{ *mob_info, doer.copy()};
    }  
  };
  
  const Mobility_Info& mob_info_of_key( const System_State&,
                                       std::vector< bool>);
  Mob_Info_And_Doer& hydro_doer_of_key( System_State&,
                                       std::vector< bool>);
  
  std::map< std::vector< bool>, Mobility_Info> mob_infos;
  std::map< std::vector< bool>, Mob_Info_And_Doer> hydro_doers;
  
  Vector< bool, Atom_Index> excluded, one_four;
  
private:
  ::std::reference_wrapper< const System_Common> common_ref;
};
}

