let error = 0.01;;

let atom_file_ref = ref "";;
let grid_file_ref = ref "";;
let debye_ref = ref infinity;;
let fine_file_ref = ref "";;
let coarse_file_ref = ref "";;

Arg.parse [("-atoms", Arg.Set_string atom_file_ref, "pqrxml file");
           ("-grid", Arg.Set_string grid_file_ref, "grid dx file");
           ("-debye", Arg.Set_float debye_ref, "Debye length (default infinity)");
           ("-fine", Arg.Set_string fine_file_ref, "fine-grained file");
           ("-coarse", Arg.Set_string coarse_file_ref, "coarse-grained file");
          ]
            (fun arg -> raise (Failure "not anonymous arguments"))
            "Splits grid into a coarse outer grid and fine inner grid\n"
;;

let atom_file = !atom_file_ref;;
let grid_file = !grid_file_ref;;
let debye = !debye_ref;;
let fine_file = !fine_file_ref;;
let coarse_file = !coarse_file_ref;;

let xmin = ref infinity;;
let xmax = ref neg_infinity;;
let ymin = ref infinity;;
let ymax = ref neg_infinity;;
let zmin = ref infinity;;
let zmax = ref neg_infinity;;

let atom_buf = Scanf.Scanning.from_file atom_file;;

Atom_parser.apply_to_atoms ~buffer: atom_buf
  ~f: (fun ~rname:rnam ~rnumber:rnum ~aname:anam ~anumber:anum 
    ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
    if x +. r > !xmax then
      xmax := x +. r;
    if x -. r < !xmin then
      xmin := x -. r;

    if y +. r > !ymax then
      ymax := y +. r;
    if y -. r < !ymin then
      ymin := y -. r;

    if z +. r > !zmax then
      zmax := z +. r;
    if z -.r < !zmin then
      zmin := z -. r;
  )
;;

let xlo = !xmin;;
let xhi = !xmax;;
let ylo = !ymin;;
let yhi = !ymax;;
let zlo = !zmin;;
let zhi = !zmax;;

let pr = Printf.printf;;

let grid_buf = Scanf.Scanning.from_file grid_file;;
let ginfo = Grid_parser.grid_info grid_buf;; 

module GP = Grid_parser;;
module V3 = Vec3;;

let xglo,yglo,zglo = 
  let v = ginfo.GP.low in 
  v.V3.x, v.V3.y, v.V3.z
;;

let hx,hy,hz = ginfo.GP.h;;
let nx,ny,nz = ginfo.GP.n;;

let xghi = xglo +. (float (nx-1))*.hx;;
let yghi = yglo +. (float (ny-1))*.hy;;
let zghi = zglo +. (float (nz-1))*.hz;;

let hmax = max (max hx hy) hz;;
let h2 = 2.0*.hmax;;

let dist = Grid_error.min_distance ~error:error ~debye:debye ~width:h2;;
  
let xnlo = xlo -. dist;;
let ynlo = ylo -. dist;;
let znlo = zlo -. dist;;

let xnhi = xhi +. dist;;
let ynhi = yhi +. dist;;
let znhi = zhi +. dist;;

let fpr = Printf.fprintf;;

let print_footer chan = 
  fpr chan
    "attribute \"dep\" string \"positions\"\n\
     object \"regular positions regular connections\" class field\n\
     component \"positions\" value 1\n\
     component \"connections\" value 2\n\
     component \"data\" value 3\n"
;;

let output_coarse () = 

  let shalf n = if n mod 2 == 0 then n/2 else n/2 + 1 in

  let nnx = shalf nx
  and nny = shalf ny
  and nnz = shalf nz in

  let in_buf = Scanf.Scanning.from_file grid_file in
  let oc = open_out coarse_file in
  fpr oc "object 1 class gridpositions counts %d %d %d\n" nnx nny nnz;
  fpr oc "origin %g %g %g\n" xglo yglo zglo;
  fpr oc "delta %g 0.0 0.0\n" (2.0*.hx);
  fpr oc "delta 0.0 %g 0.0\n" (2.0*.hy);
  fpr oc "delta 0.0 0.0 %g\n" (2.0*.hz);
  fpr oc "object 2 class gridconnections counts %d %d %d\n" nnx nny nnz;
  fpr oc "object 3 class array type double rank 0 items %d data follows\n" (nnx*nny*nnz);
  
  let irow = 
    Grid_parser.apply_to_potential 
      in_buf
      (fun info -> 0) 
      (fun irow ix iy iz v ->
       
       if (ix mod 2 == 0 && iy mod 2 == 0 && iz mod 2 == 0) then (
         fpr oc "%g " v;
         if irow == 2 then (
           fpr oc "\n";
           0
         )
         else
           irow + 1
       )
       else
         irow
      )
  in
  
  if not (irow = 0) then
    fpr oc "\n";
  
  print_footer oc;
  close_out oc
;;

let output_fine () = 
  let ixlo = int_of_float ((xnlo -. xglo)/.hx)
  and iylo = int_of_float ((ynlo -. yglo)/.hy)
  and izlo = int_of_float ((znlo -. zglo)/.hz) in

  let ixhi = nx - (int_of_float ((xghi -. xnhi)/.hx))
  and iyhi = ny - (int_of_float ((yghi -. ynhi)/.hy))
  and izhi = nz - (int_of_float ((zghi -. znhi)/.hz)) in
 
  let nnx = ixhi - ixlo + 1
  and nny = iyhi - iylo + 1
  and nnz = izhi - izlo + 1 in
 
  let xolo = xglo +. (float ixlo)*.hx
  and yolo = yglo +. (float iylo)*.hy
  and zolo = zglo +. (float izlo)*.hz in
                      
  let in_buf = Scanf.Scanning.from_file grid_file in
  let oc = open_out fine_file in
  fpr oc "object 1 class gridpositions counts %d %d %d\n" nnx nny nnz;
  fpr oc "origin %g %g %g\n" xolo yolo zolo;
  fpr oc "delta %g 0.0 0.0\n" hx;
  fpr oc "delta 0.0 %g 0.0\n" hy;
  fpr oc "delta 0.0 0.0 %g\n" hz;
  fpr oc "object 2 class gridconnections counts %d %d %d\n" nnx nny nnz;
  fpr oc "object 3 class array type double rank 0 items %d data follows\n" (nnx*nny*nnz);

  let irow = 
    Grid_parser.apply_to_potential 
      in_buf
      (fun info -> 0) 
      (fun irow ix iy iz v ->
        
       if ((ixlo <= ix && ix <= ixhi) && (iylo <= iy && iy <= iyhi) && 
             (izlo <= iz && iz <= izhi)) then (
         fpr oc "%g " v;
         if irow = 2 then (
           fpr oc "\n";
           0
         )
         else
           (irow + 1)
       )
       else
         irow
      )
  in
  if not (irow = 0) then 
    fpr oc "\n";
  
  print_footer oc;
  close_out oc
;;

output_fine();;
output_coarse();; 

(*
pr "distance %g\n" dist;;
pr "outer %g %g %g, %g %g %g\n" xglo yglo zglo xghi yghi zghi;;
pr "inner %g %g %g, %g %g %g\n" xnlo ynlo znlo xnhi ynhi znhi;;
pr "outer volume %g\n" ((xghi -. xglo)*.(yghi -. yglo)*.(zghi -. zglo));
pr "inner volume %g\n" ((xnhi -. xnlo)*.(ynhi -. ynlo)*.(znhi -. znlo));
 *)
