/*
 * time_step_parameters.cc

 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#include "time_step_parameters.hh"
#include "../input_output/outtag.hh"

namespace Browndye{

using std::tie;

//constructor
Time_Step_Parameters::Time_Step_Parameters( Node_Ptr& node){
  
  auto tnode = node->child( "time_step_tolerances");
  if (tnode){
    const_dt_opt = value_from_node< Time>( tnode, "constant_dt");
    if (!const_dt_opt){
      get_value_from_node( tnode, "minimum_core_dt", dt_min_core);
      get_value_from_node( tnode, "minimum_chain_dt", dt_min_chain);
    
      dtr_min_core = dt_min_core;
      get_value_from_node( tnode, "minimum_core_reaction_dt", dtr_min_core);
      dtr_min_chain = dt_min_chain;
      get_value_from_node( tnode, "minimum_chain_reaction_dt", dtr_min_chain);    
    }
  }
}

void Time_Step_Parameters::output( std::ofstream& outp) const{
  
  auto ot = [&]( const std::string& tag, auto t){
    outtag( outp, tag, t, 4);
  };
  
  outp << "  <time_step_tolerances>\n";
  ot( "minimum_core_dt", dt_min_core);
  ot( "minimum_core_rxn_dt", dtr_min_core); 
  ot( "minimum_chain_dt", dt_min_chain);
  ot( "minimum_chain_rxn_dt", dtr_min_chain);   
  //ot( "force", ftol);
  //ot( "reaction", rtol);
  outp << "  </time_step_tolerances>\n";
}

}