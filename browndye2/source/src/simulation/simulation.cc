/*
 * simulation.cc
 *
 *  Created on: Sep 29, 2015
 *      Author: root
 */

#include "simulation.hh"

namespace Browndye{

//  constructor
Simulation::Simulation( const std::string& file):
   input( file){

  if (!input.is_open()){
    error( "simulation input file", file, "could not be opened");
  }

  parser = std::make_unique< JAM_XML_Pull_Parser::Parser>( input, file);
  top_node = parser->top();
  top_node->complete();
}

}
