#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Parser_1F is a class used for parsing XML.  It is parameterized by
a user-defined State class, and contains two user-defined 
functions: one called upon the opening of an XML node,
and one called upon the end of an XML node as the parsing proceeds.
Wraps Expat.
*/

#include <map>
#include <stack>
#include <fstream>
#include <sstream>
#include <memory>
#include <expat.h>
#include "../global/error_msg.hh"
#include "attrs.hh"
#include "str_stack.hh"
#include "../lib/vector.hh"

namespace Browndye{
namespace JAM_XML_Parser{

constexpr unsigned int BUFFER_SIZE_1F = 10000;

//********************************************************************
template< class State>
class Parser_1F{

public:
  // called at the beginning of an XML node.
  // "tag" is the name of the XML tag, and "attrs" contains the
  // attributes.
  typedef
  void (*Start_Fun)( State& state, const String& tag, const Attrs& attrs);
  
  // called at the end of an XML node. The contents of the node can
  // be read from "cinfo".
  typedef
  void (*End_Fun)( State& state, const String& tag, Vector< String>&&);
  
  //**********
  Parser_1F() = delete;
  Parser_1F( const Parser_1F&) = delete;
  Parser_1F& operator=( const Parser_1F&) = delete;
  Parser_1F( Parser_1F&&) = delete;
  Parser_1F& operator=( Parser_1F&&) = delete;
  
  Parser_1F( State&, Start_Fun, End_Fun, const std::string& file);
  ~Parser_1F();

  // parse all
  void parse( std::istream& stm);
  void parse_some( std::istream& stm, bool& done);

  int current_line_number() const;
  int current_column_number() const;

  const std::string& file_name() const;

  //**********************
private: 
  typedef std::stack< String> Str_Stack;

  //**********
  static void start( void* data, const XML_Char* _tag, 
                     const XML_Char** _attr);
  static void end( void* data, const XML_Char* _tag);
  static void do_chars( void* data, const XML_Char* chars, int len);

  void tag_start( const String& tag, const Attrs&); 
  void tag_end( const String& tag);

  //*********** 
  State& state;

  Start_Fun start_fun;
  End_Fun end_fun;

  XML_Parser xml_parser;
  std::stack< Str_Stack> str_stacks;
  char buffer[ BUFFER_SIZE_1F];
  
  std::string fname;
};

/***********************************************************************/
void add_string( Str_Stack& str_stack, String str);

const String string_from_stack( Str_Stack& str_stack);

/***********************************/
template< class State>
const std::string& Parser_1F< State>::file_name() const{
  return fname;
}

template< class State>
void Parser_1F< State>::parse_some( std::istream& stm, bool &done){

  stm.read( buffer, BUFFER_SIZE_1F);
  int len = stm.gcount();
  if (stm.fail() && !stm.eof()){
    int line = XML_GetCurrentLineNumber( xml_parser);
    error( "Read error at line", line, "in file", file_name());
  }
  done = stm.eof();
  
  if (!XML_Parse( xml_parser, buffer, len, done)) {
    error( "Parse error at line", 
           //XML_GetCurrentLineNumber( xml_parser),
           current_line_number(),
           ":",
           XML_ErrorString( XML_GetErrorCode( xml_parser)),
          "in file", file_name()
          );
  }
}

/***********************************/
template< class State>
void Parser_1F< State>::parse( std::istream& stm){
  bool done;
  for (;;){
    parse_some( stm, done);
    if (done)
      break;   
  }
}


/***********************************/
// constructor
template< class State>
Parser_1F< State>::Parser_1F( State& _state, Start_Fun sfun, End_Fun efun, const std::string& _fname): 
  state(_state), start_fun( sfun), end_fun( efun), fname(_fname){

  xml_parser = XML_ParserCreate( NULL);
  XML_SetElementHandler( xml_parser, start, end);
  XML_SetCharacterDataHandler( xml_parser, do_chars);
  XML_SetUserData( xml_parser, (void*)this);
}

/***********************************/
template< class State>
Parser_1F< State>::~Parser_1F(){

  XML_ParserFree( xml_parser);
}

/***********************************/
template< class State>
void Parser_1F< State>::tag_start( const String& tag, const Attrs& attrs){
  (*start_fun)( state, tag.c_str(), attrs);
  str_stacks.push( Str_Stack());
} 

/***********************************/
template< class State>
void Parser_1F< State>::start( void *data, const char *_tag, const char **_attr){

  Parser_1F< State>& parser = *((Parser_1F< State>*)data);
  String tag(_tag);
  Attrs attr;
  for( unsigned int i=0; _attr[i] != nullptr; i += 2){
    const String key( _attr[i]);
    const String ref( _attr[i+1]);
    attr.insert( key, ref);
  }

  parser.tag_start( tag, attr);
}

/***********************************/
template< class State>
void Parser_1F< State>::tag_end( const String& tag){
  
  Str_Stack& str_stack = str_stacks.top();
  const String res = string_from_stack( str_stack);
  str_stacks.pop();

  std::stringstream ss( res);
  Vector< String> svec;

  while( !ss.eof()){
    String s;
    ss >> s;
    if (s.size() > 0)
      svec.push_back( s);
  }

  (*end_fun)( state, tag, std::move( svec));
} 

/***********************************/
template< class State>
void Parser_1F< State>::end( void *data, const char *_tag){
  Parser_1F< State>& parser = *((Parser_1F< State>*)data);
  String tag(_tag);
  parser.tag_end( tag);
}

/***********************************/
template< class State>
void Parser_1F< State>::do_chars( void* data, const XML_Char* chars, int len){

  Parser_1F< State>& parser = *((Parser_1F< State>*)data);
  String str( chars, len);
  add_string( parser.str_stacks.top(), str);
}

template< class State>
int Parser_1F< State>::current_line_number() const{
  return XML_GetCurrentLineNumber( xml_parser);
}  

template< class State>
int Parser_1F< State>::current_column_number() const{
  return XML_GetCurrentColumnNumber( xml_parser);
}  

}}


