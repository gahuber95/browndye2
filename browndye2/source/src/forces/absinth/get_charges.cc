#include <fstream>
#include <map>
#include "../../xml/node_info.hh"
#include "../../lib/split.hh"

namespace{

using namespace Browndye;
using std::string;
using std::cout;

std::map< string, string> aa_names{ {"Alanine", "ALA"},{"Glycine","GLY"}, {"Valine","VAL"},{"Leucine","LEU"},{"Isoleucine","ILE"},
  {"Serine","SER"},{"Threonine","THR"},{"Cysteine","CYS"},{"Proline","PRO"},{"Phenylalanine","PHE"},{"Tyrosine","TYR"},{"Tryptophan","TRP"},
  {"Histidine (HD)", "HID"},{"Histidine (HE)", "HIE"},{"Aspartic Acid(-)","ASP"},{"Asparagine","ASN"},{"Glutamic Acid(-)","GLU"},
  {"Glutamine","GLN"},{"Methionine","MET"},{"Lysine","LYS"},{"Arginine","ARG"},{"N-Terminal Glycine(+)","NGLY"},
  {"N-Terminal Proline(+)", "NPRO"}, {"C-Terminal Glycine(-)", "CGLY"}, {"C-Terminal Proline(-)", "CPRO"}, {"N-Terminal Residue(+)","NTERM"},
  {"C-Terminal Residue(-)","CTERM"}, {"Aspartic Acid", "ASH"}, {"Glutamic Acid", "GLH"}, {"Histidine (+)", "HIP"}, {"Cystine (-SS-)", "CYX"}
  };


Vector< string> stitched_quotes( const Vector< string>& line){
  
  Vector< string> res;
  bool on_quote = false;
  string qtok;
  for( auto& tok: line){
    auto nt = tok.size();
    if (!on_quote){
      if (tok[0] == '"'){
        if (tok.back() == '"'){
          res.push_back( tok.substr( 1,nt-2));
        }
        else{
          on_quote = true;
          qtok += tok.substr( 1, nt-1) + " ";
        }
      }
      else{
        res.push_back( tok);
      }
    }
    else{ // on quote
      if (tok.back() == '"'){
        qtok += tok.substr( 0, nt-1);
        on_quote = false;
        res.push_back( qtok);
        qtok.clear();
      }
      else{
        qtok += tok;
      }
    }
  }
  return res;
}

//##########################################################
namespace JP = JAM_XML_Pull_Parser;

std::map< string, Vector< string> > aas_of_res(){
  
  string file( "lj_parameters.xml");
  std::ifstream input( file);
  JP::Parser parser( input, file);
  
  auto top = parser.top();
  top->complete();
  
  std::map< string, Vector< string> > res;
  auto rnodes = top->children_of_tag( "residue");
  for( auto rnode: rnodes){
    auto rname = checked_value_from_node< string>( rnode, "name");
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      auto aname = checked_value_from_node< string>( anode, "name");
      res[rname].push_back( aname);
    }
  }
  return res;
}

//##########################################################
// n-term: add H2, H3 as well as H
// c-term: add O as well as OXT
typedef std::map< std::pair< string, string>, double> Charge_Map;

Charge_Map nterminal_map( const Charge_Map& charge_premap, const Charge_Map& charge_map){
  
  Charge_Map ntcharge_premap;
  
  // copy derived ones
  for( auto& entry: charge_map){
    auto& aa = entry.first;
    auto& res = aa.first;
   
    if (res.size() == 3){
      auto& atom = aa.second;
      auto q = entry.second;
      auto nres = "N" + res;
      auto naa = std::make_pair( nres, atom);
      ntcharge_premap[ naa] = q;
    }
  }
  
  auto natsn = [&]{
    std::map< string, double> res;
    for( auto& entry: charge_premap){
      auto& aa = entry.first;
      auto& resi = aa.first;
      if (resi == "NTERM"){
        auto& atom = aa.second;
        auto q = entry.second;
        res[atom] = q;
        if (atom == "H"){
          res["H2"] = q;
          res["H3"] = q;
        }
      }
    }
    return res;
  }();
  
  std::map< std::pair< string, string>, double> ntcharge_map;
  for( auto& entry: natsn){
    auto& atom = entry.first;
    auto q = entry.second;
    for( auto& arentry: ntcharge_premap){
      auto& aa = arentry.first;
      auto& res = aa.first;
      auto key = std::make_pair( res, atom);
      ntcharge_map[ key] = q;
    }
  }
  
  for( auto& entry: ntcharge_premap){
    auto& aa = entry.first;
    auto q = entry.second;
    ntcharge_map.try_emplace( aa, q);
  }
  
  // add in real ones (GLY, PRO)
  for( auto& entry: charge_map){
    auto& aa = entry.first;
    auto& res = aa.first;
   
    if (res.size() == 4 && res[0] == 'N'){
      auto& atom = aa.second;
      auto q = entry.second;
      auto naa = std::make_pair( res, atom);
      ntcharge_map[ naa] = q;
      if (atom == "H"){
        auto key1 = std::make_pair( res, "H2");
        ntcharge_map[ key1] = q;
        auto key2 = std::make_pair( res, "H3");
        ntcharge_map[ key2] = q;        
      }
    }
  }
  
  return ntcharge_map;
}

//#############################################################
Charge_Map cterminal_map( const Charge_Map& charge_premap, const Charge_Map& charge_map){
  
  Charge_Map ctcharge_premap;
  
  // copy derived ones
  for( auto& entry: charge_map){
    auto& aa = entry.first;
    auto& res = aa.first;
   
    if (res.size() == 3){
      auto& atom = aa.second;
      auto q = entry.second;
      auto nres = "C" + res;
      auto naa = std::make_pair( nres, atom);
      ctcharge_premap[ naa] = q;
    }
  }
  
  auto natsc = [&]{
    std::map< string, double> res;
    for( auto& entry: charge_premap){
      auto& aa = entry.first;
      auto& resi = aa.first;
      if (resi == "CTERM"){
        auto& atom = aa.second;
        auto q = entry.second;
        res[atom] = q;
        if (atom == "OXT"){
          res["O"] = q;
        }
      }
    }
    return res;
  }();
  
  std::map< std::pair< string, string>, double> ctcharge_map;
  for( auto& entry: natsc){
    auto& atom = entry.first;
    auto q = entry.second;
    for( auto& arentry: ctcharge_premap){
      auto& aa = arentry.first;
      auto& res = aa.first;
      auto key = std::make_pair( res, atom);
      ctcharge_map[ key] = q;
    }
  }
  
  for( auto& entry: ctcharge_premap){
    auto& aa = entry.first;
    auto q = entry.second;
    ctcharge_map.try_emplace( aa, q);
  }
  
  // add in real ones (GLY, PRO)
  for( auto& entry: charge_map){
    auto& aa = entry.first;
    auto& res = aa.first;
   
    if (res.size() == 4 && res[0] == 'C'){
      auto& atom = aa.second;
      auto q = entry.second;
      auto naa = std::make_pair( res, atom);
      ctcharge_map[ naa] = q;
      if (atom == "OXT"){
        auto key1 = std::make_pair( res, "O");
        ctcharge_map[ key1] = q;
      }
    }
  }
  return ctcharge_map;
}

//##############################################################
void main0(){
  std::ifstream input( "oplsaal.prm");
  string line;
  
  std::map< int, double> charges;

  Charge_Map charge_premap;
  
  while(!input.eof()){
    std::getline(input, line);
    auto toks = split( line);
    if (toks[0] == "charge"){
      auto ic = atoi( toks[1].c_str());
      auto q = atof( toks.back().c_str());
      charges[ic] = q;
    }
    else if (toks[0] == "biotype"){
      auto qtoks = stitched_quotes( toks);
      auto itr = aa_names.find( qtoks[3]);
      if (itr != aa_names.end()){
        auto rname = itr->second;
        auto qid = atoi( qtoks[5].c_str());
        auto q = charges.at( qid);
        auto aname = [&]{
          auto& aa = qtoks[2];
          return (aa == "HN") ? "H": aa;
        }();
        
        charge_premap[ std::make_pair( rname, aname)] = q;
      }
    }
  }
  
  
  auto aas = aas_of_res();
  
  Charge_Map charge_map;
  for( auto& entry: charge_premap){
    auto& aa = entry.first;
    auto& res = aa.first;
    auto& atom = aa.second;
    auto q = entry.second;
    auto na = atom.size();
    auto itr = aas.find( res);
    if (itr != aas.end()){
      auto& full_atoms = itr->second;
      for( auto& fatom: full_atoms){
        if (fatom.size() >= na){
          if (fatom.substr( 0, na) == atom){
            charge_map[ std::make_pair( res, fatom)] = q;
          }
        } 
      }
    }
  }
  
  // overwrite exact matches
  for( auto& entry: charge_premap){
    auto& aa = entry.first;
    auto& res = aa.first;
    auto& atom = aa.second;
    auto q = entry.second;
    auto itr = aas.find( res);
    if (itr != aas.end()){
      auto& full_atoms = itr->second;
      for( auto& fatom: full_atoms){
        if (fatom == atom){
          charge_map[ std::make_pair( res, fatom)] = q;
        } 
      }
    }
  }
  
  auto ntcharge_map = nterminal_map( charge_premap, charge_map); 
  auto ctcharge_map = cterminal_map( charge_premap, charge_map);  
  std::map< string, std::map< string, double> > final_map;
  
  auto put_final = [&]( const Charge_Map& cmap){
    for( auto& entry: cmap){
      auto& aa = entry.first;
      auto& res = aa.first;
      auto& atom = aa.second;
      auto q = entry.second;
      auto& rmap = final_map[res];
      rmap[atom] = q;
    }
  };
  
  put_final( charge_map);
  put_final( ntcharge_map);
  put_final( ctcharge_map);
  
  cout << "<top>\n";
  for( auto& entry: final_map){
    auto res = entry.first;
    cout << "  <residue>\n";
    cout << "    <name> " << res << " </name>\n";
    auto& cmap = entry.second;
    for( auto& aentry: cmap){
      auto& atom = aentry.first;
      auto q = aentry.second;
      cout << "    <atom>\n";
      cout << "      <name> " << atom << " </name>\n";
      cout << "      <charge> " << q << " </charge>\n";
      cout << "    </atom>\n";     
    }
    cout << "  </residue>\n";
  }
  
  cout << "</top>\n";
}
}

int main(){
  main0();
}
