#pragma once

#include "../core_and_chain/chain/chain_state.hh"
#include "../core_and_chain/molcore/core_state.hh"
#include "../tet/tet.hh"
#include "group_thread.hh"
#include "saved_group_state.hh"
#include "../hydrodynamics/generic_mobility_full.hh"

namespace Browndye{

class Group_Common;
//class GM_Iface_With_Tets;

/* important states

whether constraints are current
whether constraint satisfier is current
whether forces and torques are current on molecules and chains

tets:
whether tet transforms are updated
whether tets are current with frozen chains
whether tets have current vertex forces

chain_states:
whether chain hi scale factors are updated
whether chain bounding spheres are updated
 */

struct Saved_Group_State;
//struct GM_System_With_Tets;
class System_Common;

enum class Constraint_Type{
  Chain_Core, Coplanar, Tet_Core, Tet_Tet
};

class Group_State{
public:
  //typedef Saved_Group_State Saved_State;
  typedef Browndye::Chain_State Chain_State;
  
  Group_State() = delete; 
  Group_State( const Group_State&);
  Group_State& operator=( const Group_State&);
  Group_State( Group_State&&) = default;
  Group_State& operator=( Group_State&&) = default;

  Group_State( const Group_Common&, Group_Thread&, bool for_mob = false);
  void reset_to_start( const System_Common&);
   
  [[nodiscard]] const Group_Common& common() const;
  Group_Thread& thread();
  [[nodiscard]] const Group_Thread& thread() const;
  void rethread( Group_Thread&);
  
  void shift( Pos);
  
  template< class TForm>
  void transform( TForm);
  
  [[nodiscard]] Group_Index number() const;
  [[nodiscard]] bool has_hi() const;
  [[nodiscard]] Pos center() const;
  [[nodiscard]] Diffusivity diffusivity() const;
  //Inv_Time rotational_diffusivity() const;
  [[nodiscard]] Vec3< Inv_Length> scaled_force() const;
  //[[nodiscard]] Vec3< double> scaled_torque() const;

  void save( Saved_Group_State& ) const;
  void restore( const Saved_Group_State&);
  void swap( Saved_Group_State& );

  [[nodiscard]] std::vector< bool> constraint_index() const;
  //Time tet_natural_time_step( const Solvent&) const;
  //void satisfy_tet_constraints( bool& backstep);
  void satisfy_constraints();
  void push_forces_to_tets();
  void push_tets_down();
  //void push_tet_core_tforms_down();
 
  //const Saved_Group_State& current( const Group_Thread&);

  [[nodiscard]] const Solvent& solvent() const;
  //Length chain_constraint_length( Chain_Index, Constraint_Index) const;

  [[nodiscard]] bool has_unfrozen_chain() const;

  // move back to System_State
  struct Circum_Sphere{
    Pos pos;
    Length radius;
  };
  typedef Vector< Vector< Circum_Sphere, Chain_Index>, Group_Index> Circum_Spheres;
  

  //Time minimum_dt() const;
    
  #ifdef DEBUG
  ~Group_State(){
    cons_info_and_doer = nullptr;
  }
  #endif

  // data
  Vector< Core_State, Core_Index> core_states;
  Vector< Chain_State, Chain_Index> chain_states;
  Vector< Tet< Group_State>, Tet_Index> tets;

  typedef Group_Thread::CID CID;
  CID* cons_info_and_doer = nullptr;
  //bool frozen_changed = false;
    
  // more functions (private?)
  [[nodiscard]] double constraint_error() const; // debug
  //double saved_constraint_error() const; // debug
  void correct_position( const Vector< Pos, Bead_Index>&);
  
  void setup_beads_and_constraints( Group_Thread&);  
  [[nodiscard]] Length bead_radius( Bead_Index) const;
  [[nodiscard]] bool is_core_bead( Bead_Index) const;
  Pos& bead_position( Bead_Index);
  [[nodiscard]] Pos saved_bead_position( Bead_Index) const;
  [[nodiscard]] Pos bead_position( Bead_Index) const;
  [[nodiscard]] Pos old_bead_position( const Group_Thread&, Bead_Index) const;
  [[nodiscard]] std::pair< Bead_Index, Bead_Index>
    lconstraint_beads( Constraint_Index) const;
  [[nodiscard]] std::array< Bead_Index, 4> cconstraint_beads( Constraint_Index) const;
  [[nodiscard]] Length constraint_length( Constraint_Index) const;
  [[nodiscard]] bool is_coplanar_constraint( Constraint_Index) const;
  [[nodiscard]] bool chain_has_no_native( Chain_Index) const;
  
  [[nodiscard]] Constraint_Type constraint_type( Constraint0_Index) const;
  
  static
  Array< Atom_Index, 3>
  best_links( const Array< Pos, ntet, Atom_Index>& verts, Pos pos);
  
  void setup_cons_info_and_doer( Group_Thread&);
  
private:
  // data
  std::reference_wrapper< Group_Thread> thread_ref;  
  std::reference_wrapper< const Group_Common> common_ref;
  
  // functions
  void copy_from( const Group_State& other);
    
  //Tet< Group_State> next_tet( Vector< bool, Core_Index>&, Vector< bool, Chain_Index>&, Chain_Index);
  //void update_bound_core_atoms( std::vector< bool>);
   
  //Vector< Bead_Index, Tet_Index> setup_core_and_tet_beads( Constraints_Info&, const Vector< Bead_Index, Chain_Index>&,
   //                               const Vector< Vector< Atom_Index>, Core_Index>&) const;
  
  [[nodiscard]] const Length_Constraint& lconstraint( Constraint_Index) const;
};

//#############################################
template< class TForm>
void Group_State::transform( TForm tform){

  for( auto& chain: chain_states){
    if (!chain.frozen){
      chain.transform( tform);
    }
  }
  
  for( auto& tet: tets){
  
    auto tposes = tet.poses.mapped( [&]( Pos pos){
      return tform.transformed( pos);
    });
    tet.poses = tposes;
    
    tet.push_transforms_down();
  }
}

//###############################################
inline
const Solvent& Group_State::solvent() const{
  return common().system.solvent;
}

//###################################################
/*
inline
Length Group_State::chain_constraint_length( Chain_Index ic,
                                              Constraint_Index igc) const{
  
  return common().chains[ic].length_constraints[igc].length;
}
*/
//##########################################################
inline
bool Group_State::chain_has_no_native( Chain_Index ic) const{
  return common().chains[ic].atoms.empty();
}

inline
const Group_Common& Group_State::common() const{
  return common_ref.get();
}

inline
const Group_Thread& Group_State::thread() const{
  return thread_ref.get();
}

inline
Group_Thread& Group_State::thread(){
  return thread_ref.get();
}

}
