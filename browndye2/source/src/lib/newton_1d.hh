#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/



/*
Finds the zero of a 1-D function using the Newton-Raphson method.

The Interface class has the following types and static functions:

Info - type of object carrying the function information
X - type of function argument
F - type of function result
DFDX - type of function derivative

F f( Info&, X x) - return function at "x"
void get_f_and_dfdx( Info&, X x, F& result, DFDX& dresult_dx) - 
  get function value and derivative at "x"

*/

#include <cmath>
#include <tuple>
#include "../global/error_msg.hh"

namespace Browndye{

  /*
    "low" and "high" are bounds, "mid" is a first guess, "ftol" is 
    allowed deviation from zero, xtol is tolerance on interval.
  */

template< class Fun, class FunDer, class X>
std::invoke_result_t< Fun, X>
newton_raphson_solution( Fun&&, FunDer&& funder, X low,
                          X mid, X high,
                          X xtol,
                          std::invoke_result_t< Fun,X> ftol,
                          bool and_or){
  
  typedef std::invoke_result_t< Fun, X> F;
  
  X x = mid;
  X xlow = low;
  X xhigh = high;

  auto flow = func( xlow);
  auto fhigh = func( xhigh);

  typedef decltype( F()*F()) F2;
  if (flow*fhigh > F2(0.0)){
    error( "Newton_Raphson solution: zero is not bounded");
  }

  while (true){
    auto [f,dfdx] = funder( x);

    if (and_or){
      if (fabs(f) < ftol && (xhigh - xlow < xtol)){
        break;
      }
    }
    else{
      if (fabs(f) < ftol || (xhigh - xlow < xtol)){
        break;
      }
    }

    x = x - f/dfdx;
    
    if (x < xlow || xhigh < x){
      x = 0.5*( xlow + xhigh);
    }
    
    std::tie( f,dfdx) = funder( x);
        
    if (flow*f < F2( 0.0)){
      xhigh = x;
      fhigh = f;
    }
    else{
      xlow = x;
      flow = f;
    }
  }  
  return x;
}
 
}

