#pragma once

#include <string>
#include <map>
#include <sstream>
#include "files.hh"

#include <random>
#include <iomanip>
#include "../lib/array.hh"
#include "../xml/node_info.hh"

namespace Browndye{
namespace Orchestrator{

class Source;

using std::string;

class Source_From_File;
// accepts all inputs in generic form
// result is file name
// results of inputs are substituted into the command string
// update criteria: one or more inputs are more recent
// does not need command or sources

template< class Func, class ... Inputs>
class Source_From_Data;
// accepts Source_From_XML_File, other Source_From_Data
// result is output of Func

template< class Result>
class Source_From_XML_File;
// accepts optional input of Source_From_File
// result is Result from XML element
// results of inputs are substituted into the command string
// update criteria: input is more recent
//   or value has changed from that in .orchestrator file

//################################################
Source_From_File
new_source_from_file( const string& file);

Source_From_File
new_source_from_file( const string& file, const string& command,
                     std::initializer_list< Source>);

Source_From_File
new_source_from_file( const string& file, const string& command,
                       Vector< Source>&);

//template< class Func, class ...Inputs>
//Source_From_Data< Func, Inputs...> new_source_to_data( Func f, Inputs... inputs);

template< class Result>
Source_From_XML_File< Result>
new_source_from_xml_file( const string& file,
                         std::initializer_list< string> tags);


template< class Result>
Source_From_XML_File< Result>
new_source_from_xml_file( const string& file,
                           std::initializer_list< string> tags,
                           const string& command,
                           std::initializer_list< Source> inputs
                         );

//############################################################################
using std::make_shared;
using std::shared_ptr;
using std::tuple;
using std::stringstream;

// the following can be replaced if C++17 filesystem is not available
//namespace FS = std::experimental::filesystem;

//##############################################################################
template < std::size_t... Ns , typename... Ts >
auto tail_impl( std::index_sequence<Ns...> , tuple<Ts...> t ){
  return  std::make_tuple( std::get<Ns+1u>(t)... );
}

template < typename... Ts >
auto tail( tuple<Ts...> t ){
  return tail_impl( std::make_index_sequence<sizeof...(Ts) - 1u>() , t );
}

//##############################################################################
template< class T>
string string_rep( const T& t){
  stringstream sstr;
  sstr << t;
  return sstr.str();
}

template< class T>
string subbed_string_inner( int low, const string& command, T input0){
  
  auto index_rep = "$" + std::to_string( low);
  auto poso = command.find( index_rep);
  if (poso == string::npos){
    return command;
  }
  else{
    auto pos = poso;
    auto input_rep = string_rep( input0);
  
    auto new_command = command;
    new_command.replace( pos, index_rep.size(), input_rep);
    return new_command;
  }
}

string subbed_string( const string& command0, const Vector< string>& input);
  

//############################
inline
string subbed_string( int low, const string& command, tuple<> input){
  return command;
}

//############################
template< typename Input0, typename ... Inputs>
string subbed_string( int low, const string& command, tuple< Input0, Inputs...> input){
  auto input0 = std::get<0>( input);
  auto new_command = subbed_string_inner( low, command, input0);
  return subbed_string( low+1, new_command, tail( input)); 
}

//############################
template< typename ... Inputs>
string subbed_string( const string& command, tuple< Inputs...> input){
  return subbed_string( 0, command, input);
}

void subbed_string_test();

//##########################################################################
inline
tuple<> vresults( tuple<>){
  return std::make_tuple();
}

//############################
template< class Input_Ptr0, class ... Input_Ptrs>
auto vresults( tuple< Input_Ptr0, Input_Ptrs...> inputs){
  auto& input0 = std::get<0>( inputs);
  auto rest = tail( inputs);
  return tuple_cat( std::make_tuple( input0->vresult()), vresults( rest));
}

//############################
class OKludge{};

/*
template< class Input_Ptr>
inline
File_Time most_recent_time( tuple< Input_Ptr> inputs){
  auto& input = *(std::get<0>( inputs));
  return input.time();
}
*/

template< class ... T>
tuple< OKludge, T...> klutup( tuple< T...> input){
  return std::tuple_cat( std::make_tuple( OKludge{}), input);
}

inline
File_Time most_recent_time( tuple<OKludge>){
  
  return std::numeric_limits< File_Time>::lowest();
}

//############################
template< class Input_Ptr0, class ... Input_Ptrs>
File_Time most_recent_time( tuple< OKludge, Input_Ptr0, Input_Ptrs...> inputs){
  
  auto& input0 = *(std::get<1>( inputs));
  auto t0 = input0.time();
  auto tail0 = tail( tail( inputs));
  auto tr = most_recent_time(klutup(tail0));
  return std::max( t0, tr);
}

//################################################################################
inline
void updatet( tuple<>){}

template< class Input_Ptr0, class ... Input_Ptrs>
void updatet( tuple< Input_Ptr0, Input_Ptrs...> inputs){
  
  auto& input0 = *(std::get<0>( inputs));
  input0.update();
  updatet( tail( inputs));
}

void run_command( const string& command, const string& output_file);

//##############################################################################
class Innards_Gen{
public:
  virtual void update() = 0;
  virtual string result() const = 0;
  virtual File_Time time() const = 0;
  virtual ~Innards_Gen() = 0;
  virtual std::optional< string> output_file() const;
};

inline
std::optional< string> Innards_Gen::output_file() const{
  return std::nullopt;
}

class Source{
public:
  void update();
  string result() const;
  File_Time time() const;
  
  Source( Source_From_File&);
  Source( Source_From_File&&);
  
  template< class T>
  Source( Source_From_XML_File< T>&);
  
  template< class Func, class ... Inputs>
  Source( Source_From_Data< Func, Inputs...>&);
  
  std::optional< string> output_file() const;
  
private:
  std::shared_ptr< Innards_Gen> innards;
};

// constructor
template< class T>
Source::Source( Source_From_XML_File< T>& source){
  innards = source.innards;
}

// constructor
template< class Func, class ... Inputs>
Source::Source( Source_From_Data< Func, Inputs...>& data_source){
  innards = data_source.innards;
}

inline
std::optional< string> Source::output_file() const{
  return innards->output_file();
}

File_Time most_recent_time( const Vector< Source>& inputs);
  
void updatet( Vector< Source>& inputs);
  
Vector< string> results( const Vector< Source>& inputs);

// Inputs are other sources
// command has items of $0, $1, etc which are substituted by file names or data
class Source_From_File{
public:
  Source_From_File( const string& out_file, const string& command,
                   Vector< Source>& inputs);
  
  Source_From_File( const string& outfile);
  Source_From_File() = delete;
  
  void update();
  string result() const;
  const string& output_file() const;

  struct Innards: public Innards_Gen{
    string out_file, command;
    Vector< Source> inputs;
    
    void update() override;
    string result() const override;
    File_Time time() const override;
    ~Innards();
    std::optional< string> output_file() const override;
  };   
  
  shared_ptr< Innards> innards;
};

//############################
inline
std::optional< string> Source_From_File::Innards::output_file() const{
  return out_file;
}

inline
const string& Source_From_File::output_file() const{
  return innards->out_file;
}

inline
string Source_From_File::Innards::result() const{
  return out_file;
}

//############################
template< class T>
Vector< T> vector_from_ilist( std::initializer_list< T> lst){
  
  Vector<T> res;
  for( auto t: lst)
    res.push_back( t);
  return res;
}

//############################################################################
// Input can be source
template< class Func, class ... Inputs>
class Source_From_Data{
public:
  Source_From_Data( Func f, Inputs ... inputs);
  Source_From_Data() = delete;
        
  struct Innards: public Innards_Gen{
    Func f;
    tuple< shared_ptr< typename Inputs::Innards>... > inputs;
    
    typedef decltype( f( typename Inputs::Result()...)) Result;
    
    Innards( Func _f):f(_f){}
    Result vresult() const;
    string result() const override;
    File_Time time() const override;
    void update() override;
  };
  shared_ptr< Innards> innards;
};

//######################################
template< class Func, class ...Inputs>
Source_From_Data< Func, Inputs...> new_source_to_data( Func f, Inputs... inputs){
  return Source_From_Data< Func, Inputs...>( f, inputs...);
}

//######################################
// constructor
template< class Func, class ...Inputs>
Source_From_Data< Func, Inputs...>::Source_From_Data( Func f, Inputs ... inputs){
  
  innards = make_shared< Innards>( f);
  innards->inputs = std::make_tuple( inputs.innards...);
}

//######################################
template< class Func, class ...Inputs>
void Source_From_Data< Func, Inputs...>::Innards::update(){
  
  updatet( inputs);
}

//######################################
template< class F, class Args, size_t ... n>
auto call_inner_with( F&& f, Args args, std::index_sequence< n...>){
  
  return f( std::get<n>(args) ...);
}

//######################################
template< class F, class ...Inputs>
auto call_with( F&& f, tuple<Inputs...> args){
  
  constexpr auto n = sizeof...(Inputs);
  return call_inner_with( f, args, std::make_index_sequence< n>());
}

//######################################
template< class Func, class ... Inputs>
File_Time Source_From_Data< Func, Inputs...>::Innards::time() const{
  return most_recent_time( klutup( inputs));
}

//######################################
template< class Func, class ...Inputs>
auto Source_From_Data< Func, Inputs...>::Innards::vresult() const -> Result{
  return call_with( f, vresults( inputs));
}

template< class Func, class ...Inputs>
string Source_From_Data< Func, Inputs...>::Innards::result() const{
  return string_rep( vresult());
}

//#####################################################################################
namespace JP = JAM_XML_Pull_Parser;

template< class T>
struct Value_Info_From_XML{
  bool changed = false, found_in_ofile = false;
  File_Time time;
  T value;
};

//##################################
template< class Result_t>
class Source_From_XML_File{
public:
  Source_From_XML_File( const string& file, const Vector< string>& tags);
  Source_From_XML_File( const string& file, const Vector< string>& tags, Source_From_File&);

  typedef Result_t Result;  
  Result result() const;
  
  struct Innards: public Innards_Gen{
    string file;
    shared_ptr< Source_From_File::Innards> file_source;
    
    Vector< string> tags;
    Result_t value;
    bool changed = false;
    bool found_in_ofile = false;
    File_Time ftime;
    
    typedef Result_t Result;
    
    Innards( const string& file, const Vector< string>& tags);
    Innards( const string& file, const Vector< string>& tags,
             Source_From_File&);    
    ~Innards();
    
    Result_t vresult() const;
    string result() const override;
    File_Time time() const override;
    void update() override;
    
  private:
    Value_Info_From_XML< Result> value_info_from_xml() const;
    void output_new_xml_file() const;
    void output_new_item_after_tags( Result new_value, stringstream&) const;
    void output_old_item_after_tags( JP::Node_Ptr, stringstream&) const;
    void output_items( JP::Node_Ptr, Result new_value, stringstream&) const;
    void get_item_info_from_node( JP::Node_Ptr, Value_Info_From_XML< Result>&) const;
    void initialize( const Vector< string>& _tags);
  };
  
  shared_ptr< Innards> innards;
};

//######################################
template< class V>
void output_tags( const V& tags, stringstream& output);

inline
string oname(){
  return string( ".orchestrator");
}

//#######################################
template< class Result_t>
Result_t Source_From_XML_File< Result_t>::result() const{
  return innards->value;
}

//#######################################
// constructor
template< class Result_t>
Source_From_XML_File< Result_t>::Innards::Innards( const string& _file,
                                                  const Vector< string>& _tags){

  file = _file;
  initialize( _tags);
}

template< class Result_t>
Source_From_XML_File< Result_t>::Innards::Innards( const string& _file,
                                                  const Vector< string>& _tags, Source_From_File& _file_source):
  Innards( _file, tags){

  file_source = _file_source.innards;

}
//#######################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::initialize( const Vector< string>& _tags){
  
  tags = _tags.copy();
  auto vinfo = value_info_from_xml();
  changed = vinfo.changed;
  value = vinfo.value;
  found_in_ofile = vinfo.found_in_ofile;
  if (found_in_ofile)
    ftime = vinfo.time;
}

//#####################################
template< class Result_t>
Source_From_XML_File< Result_t>::Innards::~Innards(){

  if (changed){
    output_new_xml_file();
  }
}

//####################################
template< class T>
Source_From_XML_File< T>
new_source_from_xml_file( const string& file,
                         std::initializer_list< string> tags){
    
  auto vtags = vector_from_ilist( tags);  
  return Source_From_XML_File< T>( file, vtags);
}

//####################################
// new_source_from_xml_file( const string& file, const Vector< string>& tags);

template< class T>
Source_From_XML_File< T>
new_source_from_xml_file( const string& file, std::initializer_list< string> tags, Source_From_File& fsource){
  
  auto vtags = vector_from_ilist( tags);  
  return Source_From_XML_File< T>( file, vtags, fsource);
}

//####################################
// constructor
template< class Result_t>
Source_From_XML_File< Result_t>::Source_From_XML_File( const string& file,
                                 const Vector< string>& tags){
 
  innards = make_shared< Innards>( file, tags);
}

//####################################
// constructor
template< class Result_t>
Source_From_XML_File< Result_t>::Source_From_XML_File( const string& file,
                                   const Vector< string>& tags, Source_From_File& fsource){
  
  innards = make_shared< Innards>( file, tags, fsource);
}

//###################################
template< class Result_t>
Result_t Source_From_XML_File< Result_t>::Innards::vresult() const{
  
  std::ifstream input( file);
  JP::Parser parser( input, file);
  auto node = parser.top();
  node->complete();
    
  for( auto& tag: tags){
    node = node->child( tag);
    if (!node){
      error( "tag", tag, "in file", file, "not found");
    }
  }
  
  return value_from_node< Result_t>( node);
}

template< class Result_t>
string Source_From_XML_File< Result_t>::Innards::result() const{
  return string_rep( vresult());
}

//########################################
struct XML_File_Info{
  std::ifstream input;
  std::unique_ptr< JP::Parser> parser;
  JP::Node_Ptr top;
  
  XML_File_Info();
};

//########################################
template< class Result_t>
File_Time Source_From_XML_File< Result_t>::Innards::time() const{
  
  if (changed)
    return file_time( file);
  else
    return ftime;  
}

//########################################
template< class Result_t>
Value_Info_From_XML< Result_t>
Source_From_XML_File< Result_t>::Innards::value_info_from_xml() const{
  
  Value_Info_From_XML< Result_t> info;
  info.value = vresult();
  
  if (file_exists( oname())){
    XML_File_Info finfo;
    auto top = finfo.top;
    
    auto inodes = top->children_of_tag( "item");
    for( auto inode: inodes) 
      get_item_info_from_node( inode, info);
    
    if (!info.found_in_ofile)
      info.changed = true;
  }
  else
    info.changed = true;
  
  return info;
}

//#######################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::
get_item_info_from_node( JP::Node_Ptr inode, Value_Info_From_XML< Result_t>& info) const{

  auto ifile = checked_value_from_node< string>( inode, "file");
  if (ifile == file){
    auto itags = checked_vector_from_node< string>( inode, "tags");
    if (itags == tags){
      auto ivalue = checked_value_from_node< Result_t>( inode, "value");
      info.found_in_ofile = true;
      info.time = checked_value_from_node< File_Time>( inode, "time");
      if (info.value != ivalue){
        info.changed = true;
      }
    }
  }
}

//######################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::output_new_xml_file() const{

  stringstream output;
  output << "<top>\n";
  
  if (file_exists( oname())){
    XML_File_Info finfo;
    auto top = finfo.top;
    output_items( top, value, output);
  }
   
  if( !found_in_ofile){
    output << "  <item>\n";
    output << "    <file> " << file << " </file>\n";
    output_tags( tags, output); 
    output_new_item_after_tags( value, output); 
    output << "  </item>\n";
  }
  
  output << "</top>\n"; // << std::ends;
  
  std::ofstream foutput( oname());
  foutput << output.str();
}

//#####################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::
  output_items( JP::Node_Ptr top, Result_t new_value, stringstream& output) const{
  
  auto inodes = top->children_of_tag( "item");
  for( auto inode: inodes){
    output << "  <item>\n";

    auto ifile = checked_value_from_node< string>( inode, "file");
    output << "    <file> " << ifile << " </file>\n";
    
    auto itags = checked_vector_from_node< string>( inode, "tags");
    output_tags( itags, output);
    
    if (ifile == file && itags == tags)
      output_new_item_after_tags( new_value, output);
    else
      output_old_item_after_tags( inode, output);
    
    output << "  </item>\n";
  } 
}

//#####################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::
  output_new_item_after_tags( Result_t new_value, stringstream& output) const{
 
  output << "    <value> " << new_value << " </value>\n";
  output << "    <time> " << file_time( file) << " </time>\n"; 
}

//####################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::
  output_old_item_after_tags( JP::Node_Ptr inode, stringstream& output) const{
  
  auto value = checked_value_from_node< string>( inode, "value");
  output << "    <value> " << value << " </value>\n";
  auto vtime = checked_value_from_node< File_Time>( inode, "time");
  output << "    <time> " << vtime << " </time>\n"; // kludge for case of 128-bit time that has no <<
}

//####################################
template< class V>
void output_tags( const V& tags, stringstream& output){
  output << "    <tags> ";
  for( auto& tag: tags)
    output << tag << " ";
  output << "</tags>\n";
}

//###################################
template< class Result_t>
void Source_From_XML_File< Result_t>::Innards::update(){
  if (file_source)
    file_source->update();
}


}
}

