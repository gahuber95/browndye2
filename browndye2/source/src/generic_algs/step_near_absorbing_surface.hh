#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/* Resolves motion of particle moving near absorbing surface at 0.
Computes 1) whether particle is absorbed in
next time step, and 2) if not, its new radial position.

Assume that kT term is part of force, so F has units of 1/Length

Interface:

typedef ... Random_Number_Generator
double gaussian( RNG&) - gaussian with zero mean and unit variance
double uniform( RNG&) - uniform between 0 and 1

*/

#include <cmath>
#include <memory>
#include "../global/error_msg.hh"
#include "../global/pi.hh"
#include "../lib/inv_erf.hh"
#include "../lib/sq.hh"

namespace Browndye{

template< class I, class Length, class Force, class Diffusivity>
std::tuple< bool, Length, decltype( Length()*Length()/Diffusivity())>
step_near_absorbing_surface( typename I::Random_Number_Generator& rng, Length x0, Force F, Diffusivity D){
  
  typedef decltype( Length()*Length()/Diffusivity()) Time;
  auto b = -F;
  Time t = x0*x0/D;
  auto tau = t*D;    

  auto st = sqrt( tau);
  auto st2 = 2.0*st;
  auto bt = b*tau;
  auto erfmt = erf( (x0 - bt)/st2);
  auto erfpt = erf( (x0 + bt)/st2);
  double psurv = 0.5*( exp(b*x0)*(erfpt - 1.0) + erfmt + 1.0);

  bool survives = I::uniform( rng) < psurv;
  Length new_x;
  Time time;
  if (survives){
    Length x;
    bool found;
    // rejection method; use prob dist of no-flux particle versus actual distr
    auto E = erf( (x0 - bt)/st2);
    do{
      // generate no-flux particle
      /*
      do {
        auto w = I::gaussian( rng);
        x = x0 - b*tau + sqrt( 2.0)*st*w;
      }
      while ( x < Length(0.0));
      */
      
      auto pc = I::uniform( rng);
      
      auto iearg = pc*(E + 1.0) - E;
      if (fabs( iearg) >= 1.0)
        error( "arg to inv erf out of range", iearg);
      
      x = 2.0*st*inv_erf( iearg) - bt + x0;
      
      auto t4 = 4.0*tau;
      auto p0 = exp(-sq(x - x0 + bt)/t4);
      auto p1 = exp( b*x0 - sq(x + x0 + bt)/t4);
      auto p2 = -2.0*exp(-(sq(x + bt) + x0*(x0 + 2.0*(x - bt)))/t4);
      auto p = p0 + p1 + p2; 
      // divide by sqrt( D*t) to get something propto actual prob dist

      auto pu = p0/(0.5*(erfmt + 1.0));
      
      if (p/pu > 1.0)
        error( "bad prob", p/pu);
      
      found = (I::uniform( rng) < p/pu);
    }
    while (!found);

    new_x = x;
    time = t;
  }

  else{ // !survives
    auto x02 = x0*x0;
    auto b2 = b*b;
    // p(t) propto diff( p,x) at x = 0
    // find t_max where diff( p(t),t) = 0
    // solution to quadratic equation; use Taylor expansion for small b*x0
    auto tau_max = fabs( b*x0) < 0.5 ? 
      x02*(1.0/6.0 - x02*b2/216.0) :  
      (sqrt( b2*x02 + 9.0) - 3.0)/b2;

    typedef decltype( tau) Tau;
    typedef decltype( 1.0/tau) Inv_Tau;
    auto pt = [&]( Tau tau) -> Inv_Tau {
      return x0*exp(-sq(x0 - b*tau)/(4.0*tau))/(2.0*sqrt(pi*tau*sq(tau)));
    };

    // rejection method
    auto pmax = pt( tau_max);
    bool done = false;
    Tau tau_samp;
    do{
      tau_samp = tau*I::uniform( rng);
      auto p_samp = pmax*I::uniform( rng);
      done = p_samp < pt( tau_samp);
    }
    while (!done);

    time = tau_samp/D;
    new_x = Length( 0.0);
  }
  
  return std::make_tuple( survives, new_x, time);
} 
}


