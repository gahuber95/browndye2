#pragma once

#if __has_include( <unistd.h>)
#  define USING_POSIX
#  define FTYPE 1
#elif __has_include( <filesystem>)
# define USING_CPP_FILE
#  define FTYPE 2
#elif __has_include( <experimental/filesystem>)
# define USING_EXP_CPP_FILE
#  define FTYPE 4
#elif __has_include( <boost/filesystem.hpp>)
#  define USING_BOOST
#  define FTYPE 8
#endif

