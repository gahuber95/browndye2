/*
 * rxn_interface.cc
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "rxn_interface.hh"
#include "../molsystem/system_state.hh"
namespace Browndye{

Cor_Cha_Index Rxn_Interface::molecule_index( const System_Common& common, const Vector< std::string>& name){
  
  auto& group_name = name[0];
  auto& unit_name = name[1];
        
  auto has_name = [&]( const System_Common::Mol_Rxn_Ref& rxn_ref) -> bool{
    auto& group = common.groups[ rxn_ref.ig];
    if (group.name == group_name){ 
      if (rxn_ref.is_core()){
        return group.cores[ rxn_ref.core_index()].name == unit_name;
      }
      else if (rxn_ref.is_dummy()){
        return group.dummies[ rxn_ref.dummy_index()].name == unit_name;
      }
      else{
        return group.chains[ rxn_ref.chain_index()].name == unit_name;
      }
    }
    else{
      return false;
    }
  };
  
  auto& mol_rxn_refs = common.mol_rxn_refs;
  auto itr = std::find_if( mol_rxn_refs.begin(), mol_rxn_refs.end(), has_name);
  if (itr == mol_rxn_refs.end())
    error( "molecule name: group", group_name, "unit", unit_name, "from pathway is not found");
  
  return Cor_Cha_Index( itr - mol_rxn_refs.begin());
}

//###########################################################################
Length Rxn_Interface::distance( const System_State& sys, Cor_Cha_Index im0, Atom_Index ia0, Cor_Cha_Index im1, Atom_Index ia1){
  
  auto& common = sys.common();
  
  auto position = [&]( Cor_Cha_Index icm, Atom_Index ia) -> Pos {
    auto& rxn_ref = common.mol_rxn_refs[icm];
    auto ig = rxn_ref.ig;
    if (rxn_ref.is_core()){
      auto mj = rxn_ref.core_index();
      auto& core = sys.groups[ig].core_states[mj];
      auto& core_common = core.common();
      auto& atoms = core_common.atoms;
      auto& atom = atoms[ia];
      return core.transformed( atom.pos);
    }
    else if (rxn_ref.is_chain()){
      auto ic = rxn_ref.chain_index();
      auto& cstate = sys.groups[ig].chain_states[ic];
      return cstate.atoms[ia].pos;
    }
    else{
      auto id = rxn_ref.dummy_index();
      auto& group = sys.common().groups[ig];
      auto& dmol = group.dummies[id];
      //auto iid = dmol.iid;
      auto im = dmol.im;
      auto& core = sys.groups[ig].core_states[im];
      auto& atom = dmol.atoms[ia];
      return core.transformed( atom.pos);
    }
  };
  
  auto pos0 = position( im0, ia0);
  auto pos1 = position( im1, ia1);
  
  return Browndye::distance( pos0, pos1);
}
}


