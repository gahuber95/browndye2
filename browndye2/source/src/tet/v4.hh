#pragma once
#include "../lib/array.hh"
#include "../global/indices.hh"
#include "tet_number.hh"

namespace Browndye{
  
  template< class T>
  using V4 = Array< T, ntet, Atom_Index>;
  
}