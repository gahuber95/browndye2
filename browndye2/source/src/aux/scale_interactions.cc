#include <iomanip>
#include "../xml/jam_xml_1f_parser.hh"
#include "../input_output/get_args.hh"

namespace{
using namespace Browndye;
namespace JP = JAM_XML_Parser;
using std::string;
using std::cout;

//################################
void psp( int level){
  cout << string( 2*level, ' ');
}

//############################
struct State{
  //bool in_changed = true;
  std::stack< bool> in_changed;
  size_t level = 0;
  double scale = 1.0;
  
  State(){
    in_changed.push( true);
  }
};

//###############################################################################
const Vector< string> dtags{ "root", "pairs", "potentials", "potential", "data"};

void node_begin( State& state, const string& tag, const JP::Attrs& attrs){
  
  auto& level = state.level;
  
  bool cic = state.in_changed.top();
  bool no_bond = attrs.dict.empty() || (attrs.dict.at("bond") != "true");
  state.in_changed.push( cic && no_bond && (tag == dtags[level]));
    
  psp( level);
  cout << "<" << tag;
  for( auto& key: attrs.dict){
    auto& name = key.first;
    auto& value = key.second;
    cout << ' ' << name << "=\"" << value << '\"';
  }
  cout << ">\n";
  level += 1;
}

//#################################################################
void print_data( const State& state, const Vector< string>& data){
  auto& level = state.level;
  psp( level);
   
  if (state.in_changed.top() && level == dtags.size()){
    for( auto& x: data){
      auto value = atof( x.c_str());
      auto cscale = (value < 0.0) ? state.scale : 1.0;
      cout << cscale*value << ' ';
    }
  }
  else{
    for( auto& x: data)
      cout << x << ' ';        
  }
  cout << '\n'; 
}

//#######################################################################
void node_end( State& state, const string& tag, Vector< string>&& data){
  
  auto& level = state.level;
  
  if (!data.empty())
    print_data( state, data);
  
  --level;
  psp( level);
  cout << "</" << tag << ">\n";
  
  state.in_changed.pop();
}

//################################################
void main0( int argc, char* argv[]){
  print_help( argc, argv, "-in: input file; -scale: scale factor");
  
  auto scale = double_arg( argc, argv, "-scale");
  auto file = string_arg( argc, argv, "-in");
  std::ifstream input( file.c_str());
 
  State state;
  state.scale = scale;
  JP::Parser_1F< State> parser( state, node_begin, node_end, file);
  parser.parse( input);
}
  
}

//###################################
int main( int argc, char* argv[]){
  main0( argc, argv);
}
