#include <cstring>
#include "../lib/vector.hh"
#include "../lib/float32_t.hh"
#include "encode_binary.hh"

/*
std::string
string_of_floats( const Vector< double>& floats, unsigned int nf);

Returns a MIME (Base64 format) string representation of the array of floats.
This is the inverse of the Ocaml code found in aux/decode_binary.ml
*/

namespace {
using namespace  Browndye;

static Vector< char> rtable;

class RTable_Initializer{
public:
  RTable_Initializer();
};

const uint32_t digit_start = 48;
const uint32_t lowercase_start = 97;
const uint32_t uppercase_start = 65;
const char last_chars[2] = {'+', '/'};
const char pad = '=';

RTable_Initializer::RTable_Initializer(){
  rtable.resize( 64);
  for (uint32_t i = 0; i < 64; i++)
    if (i < 26)
      rtable[i] = i + uppercase_start;
    else if (i < 52) 
      rtable[i] = i - 26 + lowercase_start;
    else if (i < 62) 
      rtable[i] = i - 52 + digit_start;
    else
      rtable[i] = last_chars[ i - 62];
}

[[maybe_unused]] static RTable_Initializer rtable_initializer;

bool is_big_endian(){  
  const uint32_t i = 1;
  return (*(char*)&i) == 0 ;
}

/*
union float_int32{
  Browndye::float32_t f;
  uint32_t i;
};
*/

int32_t uint_of_float32( float32_t f){
 
  int32_t i;
  std::memcpy( &i, &f, sizeof(f));
  return i;
}
}

namespace Browndye{

std::string
string_of_floats( const Vector< double>& floats, unsigned int nf){
    
  uint32_t nb = nf*4;

  typedef unsigned char uchar;
  Vector< uchar> bytes( nb, ' ');
  for (uint32_t iblock = 0; iblock < nf; ++iblock){
    float32_t fval = floats[iblock];
    uint32_t ival = uint_of_float32( fval);
    uint32_t j = iblock*4;
    uint32_t mask = 0xFF;
    uint32_t b0 = mask & ival;
    uint32_t b1 = mask & (ival >> 8);
    uint32_t b2 = mask & (ival >> 16);
    uint32_t b3 = mask & (ival >> 24);

    if (is_big_endian()){
      bytes[j+0] = b3;
      bytes[j+1] = b2;
      bytes[j+2] = b1;
      bytes[j+3] = b0;
    }
    else{
      bytes[j+0] = b0;
      bytes[j+1] = b1;
      bytes[j+2] = b2;
      bytes[j+3] = b3;
    }    
  }
  
  uint32_t nc = 4*((nb+2)/3);
  std::string res( nc, ' ');
  for( uint32_t iblock = 0; iblock < nb/3; iblock++){
    uchar b0 = bytes[ 3*iblock+0];
    uchar b1 = bytes[ 3*iblock+1];
    uchar b2 = bytes[ 3*iblock+2];

    uchar i0a = b0 >> 2; 
    uchar i0b = b0 & 0x3;
    uchar i1a = b1 >> 4;
    uchar i1b = b1 & 0xF;
    uchar i2a = b2 >> 6;
    uchar i2b = b2 & 0x3F;
    
    uchar c0 = i0a;
    uchar c1 = (i0b << 4) | i1a;
    uchar c2 = (i1b << 2) | i2a;
    uchar c3 = i2b;

    res[ 4*iblock+0] = rtable[ c0];
    res[ 4*iblock+1] = rtable[ c1];
    res[ 4*iblock+2] = rtable[ c2];
    res[ 4*iblock+3] = rtable[ c3];
  }
  if ((nb % 3) == 1){
    uint32_t b0 = bytes[ nb-1];
    uint32_t i0a = b0 >> 2;
    uint32_t i0b = b0 & 0x3;
    
    uint32_t c0 = i0a;
    uint32_t c1 = i0b << 4;
    
    res[ nc-4] = rtable[ c0];
    res[ nc-3] = rtable[ c1];
    res[ nc-2] = pad;
    res[ nc-1] = pad;
  }
  else if ((nb % 3) == 2) {
    uint32_t b0 = bytes[ nb-2]; 
    uint32_t b1 = bytes[ nb-1];
    
    uint32_t i0a = b0 >> 2;
    uint32_t i0b = b0 & 0x3;
    uint32_t i1a = b1 >> 4;
    uint32_t i1b = b1 & 0xF;
    
    uint32_t c0 = i0a;
    uint32_t c1 = (i0b << 4) | i1a;
    uint32_t c2 = i1b << 2;
    
    res[ nc-4] = rtable[ c0];
    res[ nc-3] = rtable[ c1];
    res[ nc-2] = rtable[ c2];
    res[ nc-1] = pad;
  }
 
  return res;
}

}

/************************************************************/
// Test code
/*
int main(){
  typedef std::string String;;

  Vector< double> a(2);
  a[0] = 1.1;
  a[1] = 2.2;

  String rep = string_of_floats( a);

  printf( "%s\n", rep.c_str());
}
*/
