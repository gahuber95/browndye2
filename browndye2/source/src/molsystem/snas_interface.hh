#pragma once
#include "../global/browndye_rng.hh"

namespace Browndye{

class SNAS_I{
public:
  typedef Browndye_RNG Random_Number_Generator;
  
  static
  double gaussian( Browndye_RNG& gen){
    return gen.gaussian();
  }

  static  
  double uniform( Browndye_RNG& gen){
    return gen.uniform();
  }
};

}

