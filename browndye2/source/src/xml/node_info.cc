/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation of "node_info" functions

#include "node_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

JP::Node_Ptr checked_child( JP::Node_Ptr node, const std::string& tag){

  auto res = node->child( tag);
  if (!res)
    node->perror( "node of ", tag, " not found");
  
  return res;
}
}
