#pragma once

#include <string>
#include "../../lib/units.hh"

namespace Browndye{

class V_Field_Interface{
public:
  typedef EPotential Potential;
  typedef ::Charge Charge;
  typedef ::Permittivity Permittivity;
  typedef ::EPotential_Gradient Potential_Gradient;
  typedef std::string String;
};

}

