#pragma once

#include "transform.hh"
#include "blank_transform.hh"

namespace Browndye{

class Pure_Translation{
public:
  Pure_Translation();
  Pure_Translation( Pos);
  
  Pure_Translation( const Pure_Translation&) = default;
  Pure_Translation( Pure_Translation&&) = default;
  Pure_Translation& operator=( const Pure_Translation&) = default;
  Pure_Translation& operator=( Pure_Translation&&) = default;
  
  Pure_Translation operator*( Pure_Translation) const;
  Transform operator*( Transform) const;
  Pure_Translation operator*( Blank_Transform) const;
  
  friend
  Transform operator*( Transform, Pure_Translation);
  
  friend
  Pure_Translation operator*( Blank_Transform, Pure_Translation);
  
  Pos transformed( Pos) const;
  Pos translation() const;
  
private:
  Pos t;
};

// constructor
inline
Pure_Translation::Pure_Translation(){
  Length L0( 0.0);
  t = Pos{ L0, L0, L0};
}

inline
Pos Pure_Translation::translation() const{
  return t;
}

inline
Pure_Translation::Pure_Translation( Pos _t): t(_t){}

inline
Pure_Translation Pure_Translation::operator*( Pure_Translation other) const{
  
  return Pure_Translation( other.t + t);
}

inline
Transform Pure_Translation::operator*( Transform other) const{

  return Transform( other.rotation(), t + other.translation());  
}

inline
Pure_Translation Pure_Translation::operator*( Blank_Transform) const{
  return *this;
}

inline
Transform operator*( Transform A1, Pure_Translation A2){
  return Transform( A1.rotation(), A1.rotation()*A2.t + A1.translation());
}

inline
Pure_Translation operator*( Blank_Transform, Pure_Translation pt){
  return pt;
}

inline
Pos Pure_Translation::transformed( Pos pos) const{
  return pos + t;
}

}

