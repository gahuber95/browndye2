(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(*
Outputs rate constants and 95% confidence intervals. Input is results file from 
"we_simulation". 
*)


module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;

let pi = 3.1415926;;

let out_nam = ref false;;

Arg.parse 
  [("-nam", Arg.Set out_nam, "gives the equivalent output of nam_simulation");
  ]
(fun arg -> raise (Failure "no arguments"))
 "Outputs rate constant and confidence intervals. Input is results file from \"we_simulation\""
;;

 
let rparser = JP.new_parser (Scanf.Scanning.stdin);;

let found_node parser tag = 
  while not (JP.is_current_node_tag parser tag) do
    JP.next parser;
  done;
  JP.current_node parser
;;


let so_node = found_node rparser "solvent";;
JP.complete_current_node rparser;;

let hival = 
  let hi_node = found_node rparser "hydrodynamic_interactions" in
  JP.complete_current_node rparser;
  let stm = JP.node_stream hi_node in
  JP.int_from_stream stm
;;

let ts_node = found_node rparser "time_step_tolerances";;
JP.complete_current_node rparser;;

let mi_node = found_node rparser "molecule_info";;
JP.complete_current_node rparser;;

let kdb = NI.float_of_child mi_node "b_reaction_rate";;

let nd = JP.int_from_stream (JP.node_stream (found_node rparser "n"));;
let nrxns = nd-1;;

let rn_node = found_node rparser "rxn_names";;
JP.complete_current_node rparser;;

let rxn_names =
  let stm = JP.node_stream rn_node in
  Array.init nrxns
    (fun i -> JP.string_from_stream stm)
;;

ignore (found_node rparser "data");;

let all_data = 
  JP.fold_nodes_of_tag rparser "d" 
      (fun res node ->
	let stm = JP.node_stream node in
	let row = Array.init nd (fun i -> 0.0) in
	  for i = 0 to nd-1 do
	    Scanf.bscanf stm " %g" (fun x -> row.(i) <- x)
	  done;
	  row :: res
      )
      []
;;


let total_flux = 
  let n = List.length all_data in
  let sum = 
    List.fold_left 
      (fun sum row -> 
	sum +. (Array.fold_left (+.) 0.0 row)
      ) 
      0.0
      all_data
  in
    sum /. (float n)
;;


let series_mean_n_bounds data = 

  let data = Array.of_list (List.rev data) in    
  let autoc = Fft.autocorrelation data in
  let ns = 1000 in

  Random.self_init();
  
  let n = Array.length data in
  let sq x = x*.x in
  
  let smean a =   
    let sum = Array.fold_left (+.) 0.0 a in
    sum/.(float n)
  in
  
  let ran_autocors = 
    let k = 1 in
    Array.init ns
      (fun i -> 
	let xs = 
	  Array.init n
	    (fun i -> data.( Random.int n)) 
	in
	let mean = smean xs in
	
	let den_sum = 
	  Array.fold_left (fun sum x -> sum +. (sq (x -. mean))) 0.0 xs 
	in
	let num_sum =
	  let sum = ref 0.0 in
	  for i = k to (n-1) do
	    sum := !sum +. (xs.(i) -. mean)*.(xs.(i-k) -. mean)
	  done;
	  !sum
	in
	(float n)*.num_sum /. ((float (n-k))*.den_sum)
      )
  in
  
  Array.sort compare ran_autocors;
  
  let low, high = ran_autocors.( 25*ns/1000), ran_autocors.( 975*ns/1000)
  in
  let cor_len =
    let na = Array.length autoc in
    let finis = ref false in
    let i = ref 0 in
    while not !finis do
      
      if !i = na then (
	Printf.fprintf stderr 
	  "autocorrelation does not die away fast enough\n";
        Printf.fprintf stderr "%g > %g\n" autoc.(na-1) high;
	exit 0;
      );
      
      let ac = autoc.(!i) in
      finis := (low < ac) && (ac < high);
      i := !i + 1;
    done;
    !i
  in
    
  let sample_mean() =
    let ix = ref 0 in
    let p = 1.0/.(float cor_len) in
    let sum = ref 0.0 in
    for i = 0 to n-1 do 
      if i = 0 || ((Random.float 1.0) < p) || (!ix = n-1) then 
	ix := Random.int n
      else 
	ix := !ix + 1;
      
      sum := !sum +. data.(!ix)
    done;
    !sum /. (float n)
  in
  
  let sample_means = 
    Array.init ns (fun i -> sample_mean())
  in
  
  Array.sort compare sample_means;
  
  let lowb, highb = 
    sample_means.( 25*ns/1000), sample_means.( 975*ns/1000)
  in
  (* Printf.printf "betas %g %g\n" lowb highb; *)
  lowb, (smean data), highb
;;


let val_and_bounds = 
  Array.init nd 
    (fun id ->
      let data = List.map (fun row -> row.(id)) all_data in
      series_mean_n_bounds data
    )
;;

let conv_factor = 602000000.0;; (* 1/M s, make it an option later *)
let escaped_low, escaped_mean, escaped_high = val_and_bounds.(nrxns);; 
let nsamp = 10000;;


let pr = Printf.printf;;

(**************************************************************)
let pr_results() =

  let fnval node tag =
    NI.float_of_child node tag
  in

  let prs sp =
    for i = 1 to sp do
      pr " ";
    done;
  in
  
  let ptag sp node tag =
    prs sp;
    pr "<%s> %g </%s>\n" tag (fnval node tag) tag;
  in

  let pitag sp node tag =
    prs sp;
    let ival = NI.int_of_child node tag in
    pr "<%s> %d </%s>\n" tag ival tag;
  in
    
  let so = "solvent" in
  let solp tag =
    ptag 4 so_node tag;
  in
  
  pr "<rates>\n";
  pr "  <%s>\n" so;
  solp "kT";
  solp "debye_length";
  solp "dielectric";
  solp "vacuum_permittivity";
  solp "relative_viscosity";
  pr "  </%s>\n" so;
  let hi = "hydrodynamic_interactions" in
  pr "  <%s> %d </%s>\n" hi hival hi;

  let tst = "time_step_tolerances" in
  let tsp tag =
    ptag 4 ts_node tag;
  in
  pr "  <%s>\n" tst;
  tsp "minimum_core_dt";
  tsp "minimum_core_rxn_dt";
  tsp "minimum_chain_dt";
  tsp "minimum_chain_rxn_dt";
  pr "  </%s>\n" tst;
  
  let mi = "molecule_info" in
  let mip tag =
    ptag 4 mi_node tag;
  in
  pr "  <%s>\n" mi;
  mip "b_radius";
  let brr = "b_reaction_rate" in
  pr "    <%s> %g </%s>\n" brr kdb brr;
  pitag 4 mi_node "n_reactions"; 
  pr "  </%s>\n" mi;

  let ftraj, ntraj =
    let low, mean, high = val_and_bounds.(0) in
    let low_beta = low/.total_flux
    and high_beta = high/.total_flux
    and mean_beta = mean/.total_flux
    in
    let w = high_beta -. low_beta in
    let ftraj = 16.0*.mean_beta*.(1.0 -. mean_beta)/.(w*.w) in
    ftraj, (int_of_float ftraj)
  in
  let nescaped =
     let low, mean, high = val_and_bounds.(nrxns) in
     let mean_beta = mean/.total_flux in
     int_of_float (mean_beta *. ftraj)
  in
  
  pr "  <reactions>\n";
  pr "    <n_trajectories> %d </n_trajectories>\n" ntraj; 
  pr "    <stuck> 0 </stuck>\n";
  pr "    <escaped> %d </escaped>\n" nescaped;
  
  for irxn = 0 to nrxns-1 do
    let low, mean, high = val_and_bounds.(irxn) in
    let mean_beta = mean/.total_flux
    in
    let ns = int_of_float (mean_beta *. ftraj) in
    pr "    <completed>\n";
    pr "      <name> %s </name>\n" rxn_names.(irxn);
    pr "      <n> %d </n>\n" ns;
    pr "    </completed>\n";
  done;
  pr "  </reactions>\n";
  pr "</rates>\n"
    
;;

(**************************************************************)
let pr_rates() = 
  pr "<rates>\n";
  pr "  <!-- 95%% confidence intervals -->\n";

  for irxn = 0 to nrxns-1 do
    let low, mean, high = val_and_bounds.(irxn) in
    
    let low_beta = low/.total_flux
    and high_beta = high/.total_flux
    and mean_beta = mean/.total_flux
    in
    
    let low_rate = conv_factor*.kdb*.low_beta
    and high_rate = conv_factor*.kdb*.high_beta in
    let mean_rate = conv_factor*.kdb*.mean_beta
                  
    in
    pr "  <rate>\n";
    pr "    <number> %d </number>\n" irxn;
    pr "    <rate_constant>\n";
    pr "      <low> %g </low>\n" low_rate;
    pr "      <mean> %g </mean>\n" mean_rate;
    pr "      <high> %g </high>\n" high_rate;
    pr "    </rate_constant>\n";
    pr "    <reaction_probability>\n";
    pr "      <low> %g </low>\n" low_beta;
    pr "      <mean> %g </mean>\n" mean_beta;
    pr "      <high> %g </high>\n" high_beta;
    pr "    </reaction_probability>\n";
    pr "  </rate>\n";
    

  done;

  pr "</rates>\n"
;;

if !out_nam then
  pr_results()
else
  pr_rates()
;;
