#pragma once

#include <tuple>
#include "wiener.hh"

/* Interface

typedef W_Vector;
typedef Time;
typedef System;

typedef decltype( sqrt( Time())) Sqrt_Time;

void set_half( const W_Vector&, Vector&);
void set_difference( const W_Vector&, const W_Vector&, W_Vector&);
void set_gaussian( System&, const Sqrt_Time& scale, W_Vector&);
void add_gaussian( System&, const Sqrt_Time& scale, W_Vector&);
void set_zero( W_Vector&);
W_Vector copy( const W_Vector&);

Gaussian& gaussian( System&);
W_Vector wiener_vector( const System&);

// first bool denotes whether done, second denotes need to backstep
std::tuple< bool,bool> advance( System&, Time t, Time dt, const W_Vector&);

void step_back( System&, Time t, Time dt);

*/


namespace Browndye::Wiener_Loop{

template< class I>
class Wiener_I{
public:
  typedef typename I::W_Vector Vector;
  typedef typename I::System Gaussian;
  typedef typename I::Time Time;
  
  typedef decltype( sqrt( Time())) Sqrt_Time;
   
  static
  void set_half( const Vector& a, Vector& c){
    I::set_half( a, c);
  }
  
  static
  void set_difference( const Vector& a, const Vector& b, Vector& c){
    I::set_difference( a,b,c);
  }
  
  static
  void set_gaussian( Gaussian& g, const Sqrt_Time& scale, Vector& a){
    I::set_gaussian( g,scale,a);
  }
  
  static
  void add_gaussian( Gaussian& g, const Sqrt_Time& scale, Vector& a){
    I::add_gaussian( g, scale, a);
  }

  static
  void set_zero( Vector& a){
    I::set_zero( a);
  }
  
  static
  Vector copy( const Vector& a){
    return I::copy( a);
  }
  
};

//#################################################################
template< class I>
using WProcess = Wiener::Process< Wiener_I< I> >;

// returns final time step
template< class I>
typename I::Time do_one_full_step( typename I::System& system,
                                    typename I::Time dt0,
                                    typename I::W_Vector&& wv){
  
  WProcess< I> process( move( wv), dt0);
  
  typename I::Time dt;  
  do{
    dt = process.time_step();
    auto t = process.time();
    
    bool is_done, must_backstep;
    std::tie( is_done, must_backstep) =
                            I::advance( system, t, dt, process.dW());
    if (!is_done){
      if (must_backstep){
        I::step_back( system, t, dt);
        process.split( system);
      }
      else{
        process.step_forward();
      }
    }
    else{
      break;
    }
  }
  while( !process.at_end());
   
  return dt;
}

//##################################################################
// returns successful time step
template< class I>
typename I::Time do_one_bd_step( typename I::System& system,
                                 WProcess< I>& process){
  
  typename I::Time dt;
  bool must_backstep, is_done;
  do{
    dt = process.time_step();
    auto t = process.time();
    
    std::tie( is_done, must_backstep) =
                          I::advance( system, t, dt, process.dW());
    if (!is_done){
      if (must_backstep){ 
        I::step_back( system, t, dt);
        process.split( system);
      }
      else{
        process.step_forward();
      }
    }
  }
  while( !is_done && must_backstep);
   
  return dt;
}

//##################################################################
// provides initial Wiener step
template< class I>
typename I::Time
do_one_full_step( typename I::System& system, typename I::Time dt0){

  auto wv = I::wiener_vector( system);
  I::set_gaussian( system, sqrt( dt0), wv);
  
  return do_one_full_step<I>( system, dt0, std::move(wv));
}

//################################################################
template< class I>
WProcess< I>
starting_wprocess( typename I::System& system, typename I::Time dt0){
  
  auto wv = I::wiener_vector( system);
  I::set_gaussian( system, sqrt( dt0), wv);
  return WProcess< I>( move( wv), dt0);
}

}


