#pragma once

#include "default_values.hh"
#include "../global/debug.hh"

namespace Browndye{

template< class Tag>
class IDiff;

template< class Tag>
class Index{
public:
  
  explicit constexpr Index( size_t i): data(i){}
  
  constexpr Index(){}
    
  Index( const Index& other) = default;
  Index( Index&& other) = default;
  Index& operator=( const Index& other) = default;
  Index& operator=( Index&& other) = default;
  
  constexpr size_t value() const{
    return data;
  }

  Index operator++(){
    ++data;
    return *this;
  }

  Index operator++( int){
    Index old = *this;
    ++data;
    return old;
  }

  Index operator--(){
    --data;
    return *this;
  }

  Index operator--( int){
    Index old = *this;
    --data;
    return old;
  }

  bool operator==( const Index& other) const{
    return data == other.data;
  }

  bool operator!=( const Index& other) const{
    return data != other.data;
  }

  bool operator<( const Index& other) const{
    return data < other.data;
  }

  bool operator<=( const Index& other) const{
    return data <= other.data;
  }

  bool operator>( const Index& other) const{
    return data > other.data;
  }

  bool operator>=( const Index& other) const{
    return data >= other.data;
  }

  Index operator+=( const IDiff< Tag>& d){
    data += d.data;
    return *this;
  }

  Index operator-=( const IDiff< Tag>& d){
    data -= d.data;
    return *this;
  }

  Index operator/( size_t m) const{
    return Index( data/m);
  }

  size_t operator/( Index m) const{
    return data/m.data;
  }

  size_t operator%( size_t m) const{
    return data % m;
  }

  Index operator%( Index m) const{
    return Index( data % m.data);
  }

  void set_value( size_t i){
    data = i;
  }

  friend
  Index max( Index a, Index b){
    return Index( std::max( a.data, b.data));
  }

  friend
  Index min( Index a, Index b){
    return Index( std::min( a.data, b.data));
  }

  #ifdef DEBUG
    size_t data = JAM_Vector::Default_Value< size_t>::value();
  #else
    size_t data;
  #endif
};

//####################################
template< class Tag>
Index< Tag> inced( Index< Tag> i){
  
  typedef Index< Tag> Me;
  return i + (Me(1) - Me(0));
}

template< class Tag>
Index< Tag> deced( Index< Tag> i){
  
  typedef Index< Tag> Me;
  return i - (Me(1) - Me(0));
}

//####################################################
template< class Tag, class Stream>
Stream& operator<<( Stream& stm, const Index< Tag>& idx){
  stm << idx.value();
  return stm;
}

template< class Tag, class Stream>
Stream& operator>>( Stream& stm, Index< Tag>& idx){
  size_t i;
  stm >> i;
  idx.set_value( i);
  return stm;
}

template< class Tag>
Index< Tag> zero(){
  return Index< Tag>(0);
}

template< class Tag>
class IDiff{
public:
  typedef size_t size_type;
  typedef std::ptrdiff_t difference_type;
  typedef difference_type value_type;
    
  explicit constexpr IDiff( value_type data): data(data){}

  IDiff() = default;
  IDiff( const IDiff& other) = default;
  IDiff( IDiff&& other) = default;
  IDiff& operator=( const IDiff& other) = default;
  IDiff& operator=( IDiff&& other) = default;

        
  IDiff operator++(){
    ++data;
    return *this;
  }
  
  IDiff operator++( int){
    value_type temp = data;
    ++data;
    return IDiff(temp);
  }
  
  IDiff operator--(){
    --data;
    return *this;
  }
  
  IDiff operator--( int){
    value_type temp = data;
    --data;
    return IDiff(temp);
  }
  
  IDiff operator/( size_t m) const{
     return IDiff( data/m);
   }
  
  IDiff operator+=( IDiff< Tag> i){
    data += value(i);
    return *this;
  }
  
  IDiff operator-=( IDiff< Tag> i){
    data -= value(i);
    return *this;
  }
  
  constexpr value_type my_data() const{
    return data;
  }

  constexpr value_type value() const{
    return data;
  }

  friend
  IDiff max( IDiff a, IDiff b){
    return IDiff( a.data < b.data ? b.data : a.data);
  }

  friend
  IDiff min( IDiff a, IDiff b){
    return IDiff( a.data < b.data ? a.data : b.data);
  }

  friend class Index< Tag>;

private:
  value_type data;
};

template< class Tag>
typename IDiff< Tag>::difference_type value( IDiff< Tag> i){
  return i.my_data();
}

template< class Tag>
inline
IDiff< Tag> abs( IDiff< Tag> a){
  typedef typename IDiff< Tag>::difference_type DType;
  DType arg = a.my_data();
  DType res = arg < 0 ? -arg : arg;
  return IDiff< Tag>( res);
}

template< class Tag, class Str>
inline
Str& operator<<( Str& out, IDiff< Tag> i){
  return (out << i.my_data());
}

template< class Tag>
inline
constexpr Index< Tag> operator-( Index< Tag> a, IDiff< Tag> b){
  return Index< Tag>( a.value() - b.value());
}

template< class Tag>
inline
constexpr IDiff< Tag>
operator-( Index< Tag> a, Index< Tag> b){
  return IDiff< Tag>(a.value() - b.value());
}

template< class Tag>
inline
Index< Tag> operator+( Index< Tag> a, IDiff< Tag> b){
  return Index< Tag>( a.value() + b.value());
}

template< class Tag>
inline
Index< Tag> operator+( IDiff< Tag> a, Index< Tag> b){
  return Index< Tag>( a.value() + b.value());
}

template< class Tag>
inline
IDiff< Tag> operator+( IDiff< Tag> a, IDiff< Tag> b){
  return IDiff< Tag>( a.value() + b.value());
}

template< class Tag>
inline
IDiff< Tag> operator*( int a, IDiff< Tag> b){
  return IDiff< Tag>( a*b.value());
}

template< class Tag>
inline
IDiff< Tag> operator*( IDiff< Tag> b, int a){
  return IDiff< Tag>( a*b.value());
}

template< class Tag>
inline
constexpr IDiff< Tag> operator-( IDiff< Tag> a, IDiff< Tag> b){
  return IDiff< Tag>( a.value() - b.value());
}

template< class Tag>
inline
bool operator<( Index< Tag> a, Index< Tag> b){
  return a.value() < b.value();
}

template< class Tag>
inline
bool operator<=( Index< Tag> a, Index< Tag> b){
  return a.value() <= b.value();
}

template< class Tag>
inline
bool operator>( Index< Tag> a, Index< Tag> b){
  return a.value() > b.value();
}

template< class Tag>
inline
bool operator>=( Index< Tag> a, Index< Tag> b){
  return a.value() >= b.value();
}

template< class Tag>
inline
bool operator==( Index< Tag> a, Index< Tag> b){
  return a.value() == b.value();
}

template< class Tag>
inline
bool operator!=( Index< Tag> a, Index< Tag> b){
  return a.value() != b.value();
}

template< class Tag>
inline
bool operator<( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() < b.value();
}

template< class Tag>
inline
bool operator<=( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() <= b.value();
}

template< class Tag>
inline
bool operator>( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() > b.value();
}

template< class Tag>
inline
bool operator>=( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() >= b.value();
}

template< class Tag>
inline
bool operator==( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() == b.value();
}

template< class Tag>
inline
bool operator!=( IDiff< Tag> a, IDiff< Tag> b){
  return a.value() != b.value();
}

//*******************************************************************
namespace Index_NS{
  template< bool s, class I>
  class Selector;

  template< class I>
  class Selector< true, I>{
  public:
    static
    size_t value( I i){
      return size_t(i);
    }
  };

  template< class I>
  class Selector< false, I>{
  public:
    static
    size_t value( I i){
      return i.value();
    }
  };

  template< class I>
  size_t value( I i){
    return Selector< std::is_integral<I>::value, I>::value( i);
  }

}

template< class Index_b, class Ia>
Index_b converted( Index< Ia> i){
  return Index_b{ i/Index< Ia>{1}};
}

template< class I>
IDiff< I> diff( Index< I> i){
  return i - Index<I>{0};
}

}

