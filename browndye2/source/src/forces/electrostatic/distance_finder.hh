#pragma once

#include "../../lib/units.hh"
#include "../../lib/array.hh"

namespace Browndye{

template< class CI>
class Distance_Finder{
public:
  typedef typename CI::Container Container;
  //typedef typename CI::Ref  Ref;
  typedef typename CI::Refs Refs;
  typedef typename CI::Index Index;

  bool operator()( const Container& cont, const Refs& arefs,
                   Length r, const Vec3< Length>& pos) const{

    for( auto i: arefs){
      auto apos = CI::position( cont, i);
      if (distance( apos, pos) < r + CI::physical_radius( cont, i))
        return true;
    }
    return false;
  }
};
}

