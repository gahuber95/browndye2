(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* Used internally. Extracts information from a dx-format file. See
header file for documentation. *)

type data3d = 
    (float, Bigarray.float32_elt, Bigarray.c_layout) Bigarray.Array3.t
;;

type t = {
  n: int*int*int;
  h: float*float*float;
  low: Vec3.t;
  data: data3d option;
};;

let grid_info buffer =
  try
    (try
	while true do
	  Scanf.bscanf buffer "#%s@\n" (fun s -> ());
	done;
      with 
	  Scanf.Scan_failure x -> ()
    );
    
    let nxr, nyr, nzr = ref 0, ref 0, ref 0 in
    let hxr, hyr, hzr = ref 0.0, ref 0.0, ref 0.0 in
    let lowxr, lowyr, lowzr = ref 0.0, ref 0.0, ref 0.0 in
      
      Scanf.bscanf buffer
	"object 1 class gridpositions counts %d %d %d\n"
	(fun nx ny nz ->
	  nxr := nx;
	  nyr := ny;
	  nzr := nz;
	);
      
      Scanf.bscanf buffer "origin %f %f %f\n" 
	(fun x y z -> lowxr := x; lowyr := y; lowzr := z;);  
      
      Scanf.bscanf buffer
	"delta %f %f %f\n" (fun hx dy dz -> hxr := hx);
      Scanf.bscanf buffer
	"delta %f %f %f\n" (fun dx hy dz -> hyr := hy);
      Scanf.bscanf buffer
	"delta %f %f %f\n" (fun dx dy hz -> hzr := hz);

      Scanf.bscanf buffer
	"object 2 class gridconnections counts %d %d %d\n"
	(fun nx ny nz -> ());
      
      Scanf.bscanf buffer
	"object 3 class array type double rank 0 items %d data follows\n"
	(fun n -> ());

      let nx,ny,nz = !nxr, !nyr, !nzr in
      let hx, hy, hz = !hxr, !hyr, !hzr in
      let lowx, lowy, lowz = !lowxr, !lowyr, !lowzr in
	{
	  n = nx,ny,nz;
	  h = hx,hy,hz;
	  low = Vec3.v3 lowx lowy lowz;
	  data = None;
	}
  with ex -> (
    Printf.fprintf stderr "Error in grid_parser\n";
    raise ex;
  )


let new_grid buffer = 
  let egrid = grid_info buffer in

  let nx,ny,nz = egrid.n in

  let data = 
    Bigarray.Array3.create Bigarray.float32 Bigarray.c_layout nx ny nz 
  in
    for ix = 0 to nx-1 do
      for iy = 0 to ny-1 do
	for iz = 0 to nz-1 do
	  let v = 
	    let vr = ref nan in
	      Scanf.bscanf buffer "%f " (fun v -> vr := v);
	      !vr
	  in
	    Bigarray.Array3.set data ix iy iz v;
	done;
      done;
    done;
    {
      egrid with
	data = Some data;
    }      
;;

(* f ix iy iz v lap *)
let apply_to_laplacian buffer finfo f =

  let egrid = grid_info buffer in
 
  let processed_info = finfo egrid in

  let nx,ny,nz = egrid.n in
  let planea = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  and planeb = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  and planec = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  in
  let read_plane plane = 
    for iy = 0 to ny-1 do
      for iz = 0 to nz-1 do
	let v = 
	  let vr = ref nan in
	    Scanf.bscanf buffer "%f " (fun v -> vr := v);
	    !vr
	in
	  Bigarray.Array2.set plane iy iz v;
      done;
    done;
  in
    read_plane planea;
    read_plane planeb;
    read_plane planec;
    
    let planem = ref planea
    and plane0 = ref planeb
    and plane1 = ref planec
    in
    let g p i j = Bigarray.Array2.get p i j in
    let hx,hy,hz = egrid.h in
    let hx2, hy2, hz2 = hx*.hx, hy*.hy, hz*.hz in
    let pinfo_ref = ref processed_info in
      for ix = 1 to nx-2 do
	for iy = 1 to ny-2 do
	  for iz = 1 to nz-2 do
	    let v = (g !plane0 iy iz) in
	    let p0 = 2.0*.v in
	    let lx = ((g !planem iy iz) +. (g !plane1 iy iz) -. p0)/.hx2
	    and ly = ((g !plane0 (iy+1) iz) +. (g !plane0 (iy-1) iz) 
		       -. p0)/.hy2
	    and lz = ((g !plane0 iy (iz+1)) +. (g !plane0 iy (iz-1)) 
		       -. p0)/.hz2
	    in
	    let lap = lx +. ly +. lz in
	      pinfo_ref := (f !pinfo_ref ix iy iz v lap);
		  
	  done;
	done;

	let temp = !planem in
	  planem := !plane0;
	  plane0 := !plane1;
	  plane1 := temp;
	  if ix < nx-2 then
	    read_plane !plane1
	      
      done;
      !pinfo_ref
;;

(* f ix iy iz v *)
let apply_to_potential buffer finfo f =

  let egrid = grid_info buffer in
  let pinfo = ref (finfo egrid) in

  let nx,ny,nz = egrid.n in
  let vr = ref nan in
    for ix = 0 to nx-1 do
      for iy = 0 to ny-1 do
	for iz = 0 to nz-1 do
	  let v = 
	    Scanf.bscanf buffer "%f " (fun v -> vr := v);
	    !vr
	  in
	    pinfo := (f !pinfo ix iy iz v);
	    
	done;
      done;	    
    done;
    !pinfo
;;


(* f t ix iy iz ee *)
let apply_to_ee buffer finfo f =
  let egrid = grid_info buffer in
 
  let processed_info = finfo egrid in

  let nx,ny,nz = egrid.n in
  let planea = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  and planeb = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  and planec = 
    Bigarray.Array2.create Bigarray.float32 Bigarray.c_layout ny nz 
  in
  let read_plane plane = 
    for iy = 0 to ny-1 do
      for iz = 0 to nz-1 do
	let v = 
	  let vr = ref nan in
	    Scanf.bscanf buffer "%f " (fun v -> vr := v);
	    !vr
	in
	  Bigarray.Array2.set plane iy iz v;
      done;
    done;
  in
    read_plane planea;
    read_plane planeb;
    read_plane planec;
    
    let planem = ref planea
    and plane0 = ref planeb
    and plane1 = ref planec
    in
    let g p i j = Bigarray.Array2.get p i j in
    let hx,hy,hz = egrid.h in
    let hx2, hy2, hz2 = 2.0*.hx, 2.0*.hy, 2.0*.hz in
      for ix = 1 to nx-2 do
	for iy = 1 to ny-2 do
	  for iz = 1 to nz-2 do
	    let ex = ((g !plane1 iy iz) -. (g !planem iy iz))/.hx2
	    and ey = ((g !plane0 (iy+1) iz) -. ((g !plane0 (iy-1) iz)))/.hy2
	    and ez = ((g !plane0 iy (iz+1)) -. ((g !plane0 iy (iz-1))))/.hz2
	    in
	    let ee = ex*.ex +. ey*.ey +. ez*.ez in
	      f processed_info ix iy iz ee
	  done;
	done;

	planem := !plane0;
	plane0 := !plane1;	
	if ix < nx-2 then
	  read_plane !plane1	    
      done
;;

(**********************************************************************)
(* test code *)
(*
let buf = Scanf.Scanning.from_file 
  "/net/home/ALUMNI/ghuber/ghuber/ribosome/ribosome/potential-all-270.dx";;

(*
let grid = new_grid buf;;
let data = 
  match grid.data with
    | Some data -> data
    | None -> raise (Failure "")
;;
*)

let sum = ref 0.0;;
let egrid = new_empty_grid buf;;

Printf.printf "got_here\n"; flush stdout;;

let f ix iy iz v lap = 
  sum := !sum +. lap
;;

apply_to_laplacian buf egrid f;;

(*
let sum = ref 0.0;;
let nx,ny,nz = grid.n;;
  for ix = 0 to nx-1 do
    for iy = 0 to ny-1 do
      for iz = 0 to nz-1 do
	sum := !sum +. Bigarray.Array3.get data ix iy iz
      done;
    done;
  done;;
*)

Printf.printf "sum %f\n" !sum;;

*)
