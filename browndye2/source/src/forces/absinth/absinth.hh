#pragma once

// Interface;
// class System;
//
// static functions
// void reset_etas( System&);
// void add_to_energy( System&, Energy);
// template< class F> void apply_to_atoms( System& sys, F&&);
// template< class F> void apply_to_pairs( System& sys, F&&);
//
// typename Atom_Ref;
//
// static functions
// Pos position( Sys&, Atom_Ref);
// Length radius( Sys&, Atom_Ref);
// double eta_max( Sys&, Atom_Ref);
// Energy surface_energy( Sys&, Atom_Ref);
// Charge charge( Sys&, Atom_Ref);
// void add_force( Sys&, Atom_Ref, Vec3< Force>);
// void subtract_from_eta( Sys&, Atom_Ref, double);
// double eta( Sys&, Atom_Ref);
// double eta0_geometric( Sys&, Atom_Ref);
// double eta0_electrostatic( Sys&, Atom_Ref);
// void set_eta_electrostatic( Sys&, Atom_Ref, double);
// double eta_polar( Sys&, Atom_Ref);
// void add_to_dEdeta( Sys&, Atom_Ref, Energy);
// void put_polar_v( Sys&, Atom_Ref, double);
// double polar_v( Sys&, Atom_Ref);
// void put_polar_dvdeta( Sys&, Atom_Ref, double);
// double polar_dvdeta( Sys&, Atom_Ref);
// EPotential other_potential( const Sys&, Atom_Ref);
  
#include <iostream>
#include <random>
#include "../../global/pos.hh"
#include "../../global/pi.hh"
#include "../../lib/autodiff.hh"
#include "../../global/physical_constants.hh"
#include "../../structures/solvent.hh"



namespace Absinth{

using namespace Browndye;
namespace A = Autodiff;

using std::make_pair;
using std::pair;

constexpr Length rw{ 5.0};

template< class I>
class Computer{
public:
 
  typedef typename I::System System;

  static
  void add_forces( System&);

  static
  void compute_eta( System&);
    
  static
  Length3 sphere_overlap_nod( Length R0, Length R1, Length d);
    
private:
  
  /* for future genericization
  typedef typename I::Length Len;
  typedef typename I::Energy Eng;
  typedef decltype( Len()*Len()) Len2;
  typedef decltype( Len2()*Len()) Len3;
  typedef typename I::Atom_Ref AIndex;
  */

  // Table 4
  static constexpr double chi_d = 0.1; 
  static constexpr double tau_d = 0.25;
  static constexpr double tau_s = 0.5;
  static constexpr double chi_s = 0.9;
  static constexpr double eps_w = 78.2;
  static const double inv_sqrt_eps_w;

  template< class D>
  static 
  auto sphere_overlap_mid_gen( Length R0, Length R1, Length d);
  
  static
  pair< Length3 , Length2> sphere_overlap_mid( Length R0, Length R1, Length din);
  
  static
  Length3 sphere_overlap_mid_nod( Length R0, Length R1, Length d);
  
  static
  pair< double, double> v_fun( double eta_max, double tau, double chi, double eta);
  
  template< class Ia, class Ib>
  class add_to_eta{
  public:
    typedef typename Ia::Atom_Ref Atom_Refa;
    typedef typename Ib::Atom_Ref Atom_Refb;
    typedef typename Ia::Subsystem SubA;
    typedef typename Ib::Subsystem SubB;

    static
    void doit( System& sys, SubA&, SubB&,  Atom_Refa i, Atom_Refb j, double);
  };
  
  static
  pair< Length3 , Length2> sphere_overlap( Length R0, Length R1, Length d);
   
  template< class D>
  static
  auto sphere_overlap_gen( Length R0, Length R1, Length d);

  template< class Ia>
  class add_to_dEdeta{
  public:
    typedef typename Ia::Atom_Ref Atom_Ref;
    typedef typename Ia::Subsystem SubA;

    static
    void doit( System& sys, SubA&, Atom_Ref i);
  };
  
  template< class Ia>
  class compute_polar_v{
  public:
    typedef typename Ia::Atom_Ref Atom_Ref;
    typedef typename Ia::Subsystem SubA;

    static
    void doit( System& sys, SubA&, Atom_Ref i);
  };

  template< class Ia, class Ib, bool truncated>
  class add_polar_forces{
  public:
    typedef typename Ia::Atom_Ref Atom_Refa;
    typedef typename Ib::Atom_Ref Atom_Refb;
    typedef typename Ia::Subsystem SubA;
    typedef typename Ib::Subsystem SubB;

    static
    void doit( System& sys, SubA&, SubB&, Atom_Refa i, Atom_Refb j);
  };
  
  template< class Ia, class Ib, class Rc, class Eterm, class VC>
  static
  void do_inner_polar( System& sys,  typename Ia::Subsystem&, typename Ib::Subsystem&, Pos posa, Pos posb, Rc rc, Eterm eterm, VC Vc,
                                     typename Ia::Atom_Ref i, typename Ib::Atom_Ref j);
  
  template< class Ia, class Ib>
  class distribute_dEdeta{
  public:
    typedef typename Ia::Atom_Ref Atom_Refa;
    typedef typename Ib::Atom_Ref Atom_Refb;
    typedef typename Ia::Subsystem SubA;
    typedef typename Ib::Subsystem SubB;

    static
    void doit( System& sys, SubA&, SubB&, Atom_Refa, Atom_Refb, double);
  };
  
  template< class T>
  static auto cube( T x){
    return x*x*x;
  }
  
  template< template<class, class> class F>
  static void apply_to_pairs( System&);

  template< template<class, class, bool> class F>
  static void apply_to_charge_pairs( System&);

  template< template< class> class F>
  static void apply_to_atoms( System&);
};

template< class I>
const double Computer<I>::inv_sqrt_eps_w = 1.0/sqrt( Computer<I>::eps_w);
//#####################################################################
template< class I> template< template< class,class> class F>
void Computer<I>::apply_to_pairs( System& sys){

  typedef typename I:: template Pair_Applier< F> App;
  App::doit( sys);
}

template< class I> template< template< class,class,bool> class F>
void Computer<I>::apply_to_charge_pairs( System& sys){

  typedef typename I:: template Charge_Pair_Applier< F> App;
  App::doit( sys);
}


template< class I> template< template <class> class F>
void Computer<I>::apply_to_atoms( System& sys){

  typedef typename I:: template Atom_Applier< F> App;
  App::doit( sys);
}


//#####################################################################
template< class I>
void Computer<I>::add_forces( System& sys) {
  
  I::reset_etas( sys); // set to eta_max for each atom type
  apply_to_pairs< add_to_eta>( sys);
  
  apply_to_atoms< add_to_dEdeta>( sys);
  apply_to_atoms< compute_polar_v>( sys);
  //apply_to_charge_pairs< add_polar_forces>( sys); debug
  apply_to_pairs< distribute_dEdeta>( sys); // problem lies here also
}

template< class I>
void Computer<I>::compute_eta( System &sys) {

  I::reset_etas( sys); // set to eta_max for each atom type
  apply_to_pairs< add_to_eta>( sys);
}

//#####################################################################
//https://mathworld.wolfram.com/Sphere-SphereIntersection.html
template< class I> template< class D>
auto Computer<I>::sphere_overlap_mid_gen( Length R0, Length R1, Length din) {
  
  auto RR = D:: template constant< 0>( R0 + R1);
  auto RR2 = D:: template constant< 1>( 3.0*( R0*R0 + R1*R1));
  auto RtR = D:: template constant< 2>( 6.0*R0*R1);
  auto piotw = D:: template constant< 3>( pi/12.0);
  auto two = D:: template constant< 4>( 2.0);
  
  auto d = D:: template inputv< 0>( din);
  auto Rrd = RR - d;
  return piotw*Rrd*Rrd*( d + two*RR - (RR2 - RtR)/d);
}

//###################################################
class I_Deriv{
public:
  template< int n, class T>
  static
  auto constant( T x){
    return A::constant< n>( x);
  }
  
  template< int n, class T>
  static
  auto inputv( T x){
    return A::inputv<n>( x);
  }
  
  //typedef decltype( A::inputv<0>( Length{})) Len;
};

//##########################################################
template< class I>
auto Computer<I>::sphere_overlap_mid( Length R0, Length R1, Length din)  -> pair< Length3 , Length2> {
  
  auto V = sphere_overlap_mid_gen< I_Deriv>(R0, R1, din);
  
  auto d = A::inputv<0>( din);
  auto Vv = A::value( V);
  auto dV = A::derivative_struct( V);
  auto dVdd = A::derivative( dV, d);
  
  return make_pair(Vv, dVdd);
}

//##########################################################
class I_Val{
public:
  template< int n, class T>
  static
  auto constant( T x){
    return x;
  }
  
  template< int n, class T>
  static
  auto inputv( T x){
    return x;
  }
    
  //typedef Length Len;
};

//##########################################################
template< class I>
auto Computer<I>::sphere_overlap_mid_nod( Length R0, Length R1, Length d)  -> Length3 {
  
  return sphere_overlap_mid_gen< I_Val>(R0, R1, d);
}

//###################################################
// returns value, derivative wrt d
template< class I> template< class D>
auto Computer<I>::sphere_overlap_gen( Length R0, Length R1, Length d)  {

  const Length2  L2z{ 0.0};

  constexpr bool is_deriv = std::is_same_v< D, I_Deriv>;

  if (d > R0 + R1){
    if constexpr (is_deriv){
      return make_pair( Length3 { 0.0}, L2z);
    }
    else{
      return Length3 { 0.0};
    }
  }
  else{
    auto minR = std::min( R0, R1);
    auto maxR = std::max( R0, R1);
    
    if (d + minR < maxR){
      auto V =  ((4*pi)/3)*minR*minR*minR;
      if constexpr (is_deriv){
        return make_pair( V, L2z);
      }
      else{
        return V;
      }
    }
    else{
      if constexpr (is_deriv){
        return sphere_overlap_mid( R0, R1, d);
      }
      else{
        return sphere_overlap_mid_nod( R0, R1, d);
      }
    }
  }
}

//##########################################################
template< class I> 
auto Computer<I>::sphere_overlap( Length R0, Length R1, Length d)  -> pair< Length3 , Length2>{
  
  return sphere_overlap_gen< I_Deriv>(R0, R1, d);
}

template< class I> 
auto Computer<I>::sphere_overlap_nod( Length R0, Length R1, Length d)  -> Length3 {
  
  return sphere_overlap_gen< I_Val>(R0, R1, d);
}
//################################################################
// eta_k_max - defined for each atom type, precalculate, 0 < eta < 1
const double eta_min = 0.26;
// v_k( eta_k)
// drop group index i for clarity

// v_k, return value, derivative wrt eta_k
template< class I>
pair< double, double>
Computer<I>::v_fun( double eta_max, double tau, double chi, double eta) {
  

  if (eta < eta_min){
    return make_pair( 0.0, 0.0);
  }
  else if (eta > eta_max){
    return make_pair( 1.0, 0.0);
  }
  else{
    const auto d1v = chi*eta_max + (1.0 - chi)*eta_min;
    
    auto d1 = A::constant< 0>( d1v);
    auto one = A::constant< 1>( 1.0);
    auto tau_c = A::constant< 2>( tau);
    auto eta_maxc = A::constant< 3>( eta_max);
    auto eta_minc = A::constant< 4>( eta_min);
    
    auto eterm = [&]( auto eta){
      return one/(one + exp((d1 - eta)/tau_c)); 
    };
    
    auto emxterm = eterm( eta_maxc); 
    auto emnterm = eterm( eta_minc); 
    
    auto d2 = one/( emxterm - emnterm);
    auto d3 = one - d2*emxterm;
    
    auto eta_in = A::inputv< 0>( eta);
    auto e_term = eterm( eta_in);
    auto res = e_term*d2 + d3;
    
    auto vres = A::value( res);
    auto dstru = A::derivative_struct( res);
    auto dres = A::derivative( dstru, eta_in);
          
    return make_pair( vres, dres);
  }
}
//################################################################
// Vk_max - constant function of atom radius
// gam_kL - (overlap of L sphere and k+rw sphere)/(L volume)
// but use gam_kL*(L volume) directly for eta_k

// eta_k: 1 - (sum of overlap volumes)/Vk_max
// Compute and store v_k for each atom
// Compute and store d_energy/d_v for each atom
// Compute d_v/d_r for each pair, r = distance
//    d_v/d_eta * d_eta/d_r
// Force: -(d_energy/d_v)*(d_v/d_r)*(d_r/d_pos)
//################################################################
template< class I> template< class Ia, class Ib>
void Computer<I>::add_to_eta<Ia,Ib>::doit( System& sys, SubA& suba, SubB& subb, Atom_Refa i, Atom_Refb j, double factor){

  if (factor > 0.0) { // this might not be needed
    auto posi = Ia::position(sys, suba, i);
    auto posj = Ib::position(sys, subb, j);
    Length r = distance(posi, posj);
    Length Ri = Ia::radius(sys, suba, i);
    Length Rj = Ib::radius(sys, subb, j);

    const double vol = pi4 / 3.0;
    const auto Vrw = vol * cube(rw);
    Length3 Vi = vol * cube(Ri);
    Length3 Vj = vol * cube(Rj);

    Length3 Vmaxi = Vrw - Vi;
    Length3 Vmaxj = Vrw - Vj;

    Length3 overlapi = sphere_overlap_nod(Ri, Rj + rw, r);
    Length3 overlapj = sphere_overlap_nod(Rj, Ri + rw, r);

    double sub_etai = overlapj / Vmaxi;
    double sub_etaj = overlapi / Vmaxj;

    Ia::subtract_from_eta( sys, suba, i, sub_etai);
    Ib::subtract_from_eta( sys, subb, j, sub_etaj);
  }
}
//############################################################
template< class I> template< class Ia>
void Computer<I>::add_to_dEdeta<Ia>::doit( System& sys, SubA& sub, Atom_Ref i){
  
  auto eta_max = Ia::eta_max( sys, sub, i);
  auto eta = Ia::eta( sys, sub, i);
  auto [v,dvdeta] = v_fun( eta_max, tau_d, chi_d, eta);  
  auto dG = Ia::surface_energy( sys, sub, i);
  I::add_to_energy( sys, dG*v);
  Ia::add_to_dEdeta( sys, sub, i, dG*dvdeta);
}

//############################################################
template< class I> template< class Ia>
void Computer<I>::compute_polar_v<Ia>::doit( System& sys, SubA& suba, Atom_Ref i){
  
  auto eta_max = Ia::eta_max( sys, suba, i);
  auto eta = Ia::eta( sys, suba, i);
  
  auto [v,dvdeta] = v_fun( eta_max, tau_s, chi_s, eta);
  
  Ia::put_polar_v( sys, suba, i, v);
  Ia::put_polar_dvdeta( sys, suba, i, dvdeta);
}

//###################################################################################
// may need to make this a macro to speed things up
template< class I>
template< class Ia, class Ib, class Rc, class Eterm, class VC>
void Computer<I>::do_inner_polar( System& sys, typename Ia::Subsystem& suba, typename Ib::Subsystem& subb,
                                  Pos posa, Pos posb, Rc rc, Eterm eterm, VC Vc,
                                   typename Ia::Atom_Ref i, typename Ib::Atom_Ref j){
  
  auto vi = Ia::polar_v( sys, suba, i);
  auto vj = Ib::polar_v( sys, subb, j);
  
  auto one = A::constant<3>( 1.0);
  
  auto vvi = A::inputv<0>( vi);
  auto vvj = A::inputv<1>( vj);
  auto res = (one - eterm*vvi)*(one - eterm*vvj)*Vc/rc;
  
  Energy E = A::value( res);
  I::add_to_energy( sys, E);
  auto dres = A::derivative_struct( res);
  auto dEdvi = A::derivative( dres, vvi);
  auto dEdvj = A::derivative( dres, vvj);
  auto dEdr = A::derivative( dres, rc);
  
  auto dvdetai = Ia::polar_dvdeta( sys, suba, i);
  auto dEdetai = dEdvi*dvdetai;
  Ia::add_to_dEdeta( sys, suba, i, dEdetai);
  
  auto dvdetaj = Ib::polar_dvdeta( sys, subb, j);
  auto dEdetaj = dEdvj*dvdetaj;
  Ib::add_to_dEdeta( sys, subb, j, dEdetaj);

  auto dpos = posb - posa;
  auto mF = dEdr*(dpos/A::value( rc));

  I:: template add_force< Ia, Ib>( sys, suba, subb, posa, posb, i, j, mF);
}

//#####################################################################################
// include all coulombic forces
template< class I> template< class Ia, class Ib, bool truncd>
void Computer<I>::add_polar_forces<Ia,Ib, truncd>::doit( System& sys, SubA& suba, SubB& subb, Atom_Refa i, Atom_Refb j){
    
  auto posi = Ia::position( sys, suba, i);
  auto posj = Ib::position( sys, subb, j);
  auto dpos = posj - posi;
  Length r = norm( dpos);
  
  auto terms = [&]{
    auto qi = Ia::charge( sys, suba, i);
    auto qj = Ib::charge( sys, subb, j);
    auto Vc = A::constant<0>(qi*qj/(pi4*vacuum_permittivity));
    
    auto rc = A::inputv<2>( r);
    auto epsc = A::constant<1>( inv_sqrt_eps_w);
    auto hkappac = A::constant<2>( 0.5/I::debye_length( sys));
    auto eterm = epsc*exp(-hkappac*rc);
    
    return std::make_tuple( eterm, Vc, rc);
  };
  
  if constexpr (truncd){
    //auto Ra = Ia::radius( sys, i);
    auto Rb = Ib::radius( sys, subb, j);
    // assumes that first template is for flexible?
    if (r < Rb + 1.5*rw){
      auto [eterm, Vc, rc] = terms();
      
      if (r > Rb + rw){
        auto one = A::constant<4>( 1.0);
        auto pi2c = A::constant< 5>( pi2/rw);
        auto half = A::constant< 6>( 0.5);
        auto ph = A::constant< 7>( Rb + rw);
        auto sigd = half*(cos( pi2c*(rc - ph)) + one);
        auto res = sigd*eterm*Vc/rc;
        Energy E = A::value( res);
        I::add_to_energy( sys, E);
        auto dres = A::derivative_struct( res);
        auto dEdr = A::derivative( dres, rc);
        auto mF = dEdr*(dpos/r);
        I:: template add_force< Ia, Ib>( sys, suba, subb, posi, posj, i, j, mF);
      }
      else{
        //auto [eterml, Vcl, rcl] = terms();
        do_inner_polar< Ia, Ib>( sys, suba, subb, posi, posj, rc, eterm, Vc, i, j);
      } 
    }
  }
  else{
    auto qi = Ia::charge( sys, suba, i);
    auto qj = Ib::charge( sys, subb, j);
    auto Vc = A::constant<0>(qi*qj/(pi4*vacuum_permittivity));
    
    auto rc = A::inputv<2>( r);
    auto epsc = A::constant<1>( inv_sqrt_eps_w);
    auto hkappac = A::constant<2>( 0.5/I::debye_length( sys));
    auto eterm = epsc*exp(-hkappac*rc);
    
    do_inner_polar< Ia, Ib>( sys, suba, subb,posi, posj,rc, eterm, Vc, i, j);
  }
}

//#################################################################
/*
template< class I>
void Computer<I>::
  add_rigid_polar_forces( System& sys, typename Rigid_I::Atom_Ref i){
    
  typedef Rigid_I Ia;  
  auto V0 = Ia::other_potential( sys, i);
  
  auto vi = Ia::polar_v( sys, i);
  auto qi = Ia::charge( sys, i);    
  auto Vrc = A::constant<0>(qi*V0);
  
  auto epsc = A::constant<1>( inv_sqrt_eps_w);
  auto one = A::constant<3>( 1.0);
  
  auto vvi = A::inputv<0>( vi);
  
  auto res = (one - epsc*vvi)*Vrc;
  
  Energy E = A::value( res);
  I::add_to_energy( sys, E);
  auto dres = A::derivative_struct( res);
  auto dEdvi = A::derivative( dres, vvi);
  
  auto dvdetai = Ia::polar_dvdeta( sys, i);
  auto dEdetai = dEdvi*dvdetai;
  Ia::add_to_dEdeta( sys, i, dEdetai);
}
*/

//################################################################
template< class I> template< class Ia, class Ib>
void Computer<I>::distribute_dEdeta<Ia,Ib>::doit( System& sys, SubA& suba, SubB& subb, Atom_Refa i, Atom_Refb j, double factor) {

  if (factor > 0.0) { // perhaps not needed
    auto posi = Ia::position(sys, suba, i);
    auto posj = Ib::position(sys, subb, j);
    auto dpos = posj - posi;
    Length r = norm(dpos);
    Length Ri = Ia::radius(sys, suba, i);
    Length Rj = Ib::radius(sys, subb, j);

    const double vol = pi4 / 3.0;
    const auto Vrw = vol * cube(rw);
    auto Vi = vol * cube(Ri);
    auto Vj = vol * cube(Rj);

    auto Vmaxi = Vrw - Vi;
    auto Vmaxj = Vrw - Vj;

    auto dolapi = sphere_overlap(Ri, Rj + rw, r).second;
    auto dolapj = sphere_overlap(Rj, Ri + rw, r).second;

    auto dEdetaj = Ib::dEdeta(sys, subb, j);
    auto dEdri = -(dolapi / Vmaxj) * dEdetaj;
    auto dEdetai = Ia::dEdeta(sys, suba, i);
    auto dEdrj = -(dolapj / Vmaxi) * dEdetai;

    auto dEdr = dEdri + dEdrj;
    auto mF = (dEdr / r) * dpos;
    I:: template add_force< Ia, Ib>( sys, suba, subb, posi, posj, i, j, mF);
  }
}

}
