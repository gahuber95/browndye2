#pragma once

#include <list>
#include "../lib/vector.hh"

namespace Browndye{

  template< class T, class I>
  void remove_duplicates( Vector< T,I>& v){
    sort( v.begin(), v.end());
    auto new_end = unique( v.begin(), v.end());
    v.erase( new_end, v.end());
  }
  
  template< class T, class I, class F>
  [[maybe_unused]] void remove_duplicates( Vector< T,I>& v, F&& f){
    sort( v.begin(), v.end(), f);
    auto new_end = unique( v.begin(), v.end(), f);
    v.erase( new_end, v.end());
  }

  template< class T>
  [[maybe_unused]] void remove_duplicates( std::list< T>& v){
    v.sort();
    auto new_end = unique( v.begin(), v.end());
    v.erase( new_end, v.end());
  }
  
}
