#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  The Pathway class implements the reaction network and is used to
  determine when a reaction takes place.  It also reads in its data
  from an XML node. The interface class I has the
  following types and static functions:

  types: Molecule_Set, Molecule_Info, Mol_Index, Atom_Index, Rxn_Index, State_Index, Length

  Length distance( Molecule_Set&, Mol_Index mol0, Atom_Index atom0, Mol_Index mol1, Atom_Index atom1) -
  distance between two atoms

  Mol_Index molecule_index( const Molecule_Info&, const std::string&) - index of molecule from its name

*/

#include <algorithm>
#include <set>
#include <string>
#include <limits>
#include "../lib/vector.hh"
#include "pathways_reaction.hh"
#include "pathways_state.hh"
#include "../xml/jam_xml_pull_parser.hh"

namespace Browndye{
namespace Pathways{

  namespace JP = JAM_XML_Pull_Parser;

  template< class I>
  class Pathway{

  public:    
    typedef typename I::Molecule_Set Molecule_Set;
    typedef typename I::Molecule_Info Molecule_Info;
    typedef typename I::Mol_Index Mol_Index;
    typedef typename I::Atom_Index Atom_Index;
    typedef typename I::State_Index State_Index;
    typedef typename I::Rxn_Index Rxn_Index;
    typedef typename I::Length Length;

    typedef std::string String;

    State_Index n_states() const;
    Rxn_Index n_reactions() const;

    State_Index state_before_reaction( Rxn_Index irxn) const;
    State_Index state_after_reaction( Rxn_Index irxn) const;

    size_t n_reactions_from( State_Index istate) const;
    Rxn_Index reaction_from( State_Index istate, size_t i) const;

    void remap_atoms( Mol_Index, std::map< Atom_Index, Atom_Index>& imap);
    State_Index first_state() const;
    const String& reaction_name( Rxn_Index) const;
    const String& state_name( State_Index) const;

    Pathway( const Molecule_Info&, const String& file);

    //void initialize_from_node( JP::Node_Ptr node);
  
    Rxn_Pair<I>
    reaction_coordinate( const Molecule_Set& mols, Rxn_Index irxn) const;
    
    bool is_satisfied( Rxn_Index irxn, const Molecule_Set& mols) const;
    
    std::pair< bool, Rxn_Index> next_completed_reaction( State_Index istate, const Molecule_Set& mols) const;
    
    // f( mol, atom0, atom1) - all atom pairs define criterion of irxn
    template< class F>
    void apply_to_pairs( const F& f, Molecule_Set& mol, Rxn_Index irxn) const;
    
    // apply_to_pairs applied to reactions leaving istate
    template< class F>
    void apply_to_pairs_of_successors( const F& f, Molecule_Set& mol,
                                       State_Index istate) const;

    //*******************
  private:
    //data
    Vector< State<I>, State_Index> states;
    Vector< Reaction<I>, Rxn_Index> reactions;

    std::map< String, Rxn_Index> reaction_str2int;
    std::map< String, State_Index> state_str2int;

    String first_state_name;
    State_Index first_state_number_data{ maxi};

    //mutable Vector< Rxn_Pair<I>, Crit_Index> pairs;

    // private functions
    void determine_states();

    bool is_satisfied( const Molecule_Set& mols, const Criterion<I>& criterion) const;

    template< class F>
    void apply_to_pairs( const F& f, Molecule_Set& mol,
                         const Criterion<I>& crit) const;

    Rxn_Pair<I> reaction_coordinate( const Criterion<I>& criterion, 
                                const Molecule_Set& mols) const;

    Rxn_Pair<I> reaction_coordinate( const Reaction<I>& reaction, 
                                const Molecule_Set& mols) const;
  }; 

  /*
    <roottag>
      <first_state> ... </first_state>

      <reactions>    
        <reaction>
        <name> </name>
        <state_to>  </state_to>
        <state_from>  </state_from>

        <criterion>
          <molecules>
            <molecule> group unit </molecule>
            <molecule> group unit </molecule>
          </molecules>
          <n_needed> 3 </n_needed>
          <pair>
            <atoms> i0 i1 </atoms>
            <distance> 2.1 </distance>
          </pair>
          ....

          <criterion> .. </criterion>
          ....
        </criterion>
    
      </reaction>
    ...
    </reactions>

    <states>
      <state>
        <number> 0 </number>
        <rxn_to> 1 </rxn_to>
        ...
        <rxn_from> 2 </rxn_from>
        ...
      </state>
    ...
    </states>
  </roottag>

  */

  //*****************************************************************
  template< class I>
  bool
  Pathway< I>::is_satisfied( const Molecule_Set& mols,
                             const Criterion<I>& criterion) const{

    Crit_Index n_satis( 0);

    for( auto& pair: criterion.pairs){
      if (I::distance( mols, pair.mol0, pair.atom0, pair.mol1, pair.atom1) < pair.req_distance){
        ++n_satis;
        if (n_satis >= criterion.n_needed){
          return true;
        }
      }
    }

    for( auto& ptr: criterion.criteria){
      auto& sub_criterion = *ptr;
      if (is_satisfied( mols, sub_criterion)){
        ++n_satis;
        if (n_satis >= criterion.n_needed){
          return true;
        }
      }
    }

    return false;
  }

  //********************************************************************
  template< class I>
  Rxn_Pair<I> Pathway< I>::reaction_coordinate( const Criterion<I>& criterion,
                                         const Molecule_Set& mols) const{

    auto np = criterion.pairs.size();
    auto nc = criterion.criteria.size();
    //typedef decltype( nc) Crit_Index;

    Crit_Index cr0{ 0}, cr1{ 1};
    
    auto nt = np + (nc - cr0);
    Vector< Rxn_Pair<I>, Crit_Index> pairs;
    pairs.reserve( nt);

    for( auto& cpair: criterion.pairs){
      auto& pair = pairs.emplace_back( cpair);      
      auto& distance = pair.distance;
      distance =  I::distance( mols, pair.mol0, pair.atom0, pair.mol1, pair.atom1)
                        - cpair.req_distance;

      if (distance < Length( 0.0)){
        distance = Length( 0.0);
      }      
    }

    auto& criteria = criterion.criteria;
    for( auto ic: range( nc)){
      auto& sub_criterion = criteria[ic];
      pairs.push_back( reaction_coordinate( *sub_criterion, mols));
    }

    auto nn = criterion.n_needed;

    if (nn > np){
      error( __FILE__, __LINE__, "not get here");
    }
     
    auto pair_indices = initialized_vector( nt, 
      [&]( Crit_Index i){
        return i;
      }
    );
    
    std::partial_sort( pair_indices.begin(),
                        pair_indices.begin() + nn/cr1,
                      pair_indices.end(),
      [&]( Crit_Index i0, Crit_Index i1){
        return pairs[i0].distance < pairs[i1].distance;
      }
    );
    
    auto ip = pair_indices[ nn - (cr1 - cr0)];
    return pairs[ ip];
  }

  //********************************************************************
  template< class I>
  Rxn_Pair<I> Pathway< I>::
  reaction_coordinate( const Reaction<I>& reaction,
                       const Molecule_Set& mols) const{

    return reaction_coordinate( reaction.criterion, mols);
  }

  //********************************************************************
  template< class I>
  Rxn_Pair<I> Pathway< I>::
  reaction_coordinate( const Molecule_Set& mols, Rxn_Index i) const{

    return reaction_coordinate( reactions[i], mols);
  }

  //********************************************************************
  template< class I>
  bool Pathway< I>::
  is_satisfied( Rxn_Index irxn, const Molecule_Set& mols) const{

    return is_satisfied( mols, reactions[irxn].criterion);
  }

  template< class I>
  auto Pathway< I>::next_completed_reaction( State_Index istate,
                                             const Molecule_Set& mols) const -> std::pair< bool, Rxn_Index>{

    bool completed{ false};
    Rxn_Index jrxn{ maxi};
    const State<I>& state = states[ istate];
    for( auto k: range( state.rxns_from.size())){
      auto irxn = state.rxns_from[k];
      if (is_satisfied( irxn, mols)){
        jrxn = irxn;
        completed = true;
        return std::make_pair( completed, jrxn);
      }
    }
  
    return std::make_pair( completed, jrxn);
  }

  //********************************************************************
  template< class I>
  template< class F>
  void Pathway< I>::
  apply_to_pairs( const F& f, Molecule_Set& mol,
                  const Criterion<I>& crit) const{

    for( auto& pair: crit.pairs){
      f( mol, pair.atom0, pair.atom1);
    }
    for( auto& sub_crit: crit.criteria){
      apply_to_pairs( f, mol, sub_crit);
    }
  }

  template< class I>
  template< class F>
  void Pathway< I>::
  apply_to_pairs( const F& f, typename I::Molecule_Set& mol,
                  Rxn_Index irxn) const{
    
    auto& rxn = reactions[irxn];
    apply_to_pairs( f, mol, rxn.criterion);
  }

  //********************************************************************
  template< class I>
  template< class F>
  void Pathway< I>::
  apply_to_pairs_of_successors( const F& f,
                                Molecule_Set& mol,
                                State_Index istate) const{

    auto& state = states[ istate];
    for( auto irxn: state.rxns_from){
      apply_to_pairs( f, mol, irxn);
    }
  }

  //********************************************************************

  template< class I>
  inline
  auto Pathway<I>::state_before_reaction( Rxn_Index irxn) const -> State_Index{
    return reactions[irxn].state_before;
  }

  template< class I>
  inline
  auto Pathway<I>::state_after_reaction( Rxn_Index irxn) const -> State_Index{
    return reactions[irxn].state_after;
  }

  template< class I>
  inline
  auto Pathway<I>::n_reactions() const -> Rxn_Index{
    return reactions.size();
  }

  template< class I>
  inline
  auto Pathway<I>::n_states() const -> State_Index{
    return states.size();
  }

  template< class I>
  inline
  size_t Pathway<I>::n_reactions_from( State_Index istate) const{
    return states[ istate].rxns_from.size();
  }

  template< class I>
  inline
  auto Pathway<I>::reaction_from( State_Index istate, size_t i) const -> Rxn_Index{
    return states[ istate].rxns_from[i];
  }

  template< class I>
  inline
  auto Pathway<I>::reaction_name( Rxn_Index i) const -> const String&{
    return reactions[i].name;
  }

  template< class I>
  inline
  auto Pathway<I>::state_name( State_Index i) const -> const String&{
    return states[i].name;
  }

  // e.g, maps 2 4 6 8 to 0 1 2 3
  template< class I>
  void Pathway<I>::remap_atoms( Mol_Index mi,  std::map< Atom_Index, Atom_Index>& imap){
    
    for( auto& rxn: reactions){
      rxn.criterion.remap( mi, imap);
    }
  }

  template< class I>
  auto Pathway<I>::first_state() const -> State_Index{
    return first_state_number_data;
  }

  /* starts with only reactions and their input string data;
     generates states, and puts integer data into states and reactions.
  */

  template< class I>
  void Pathway<I>::determine_states(){

    std::set< String> state_names;
    for( auto i: range( reactions.size())){
      Reaction<I>& rxn = reactions[i];
      rxn.n = i;
      reaction_str2int[ rxn.name] = i;
      state_names.insert( rxn.state_before_name);
      state_names.insert( rxn.state_after_name);
    }
    state_names.insert( first_state_name);

    states.resize( State_Index( state_names.size()));
    State_Index i{0};
    for( auto& name: state_names){
      states[i].name = name;
      states[i].n = i;
      state_str2int[ name] = i;
      ++i;
    }

    for( auto& rxn: reactions){
      auto isb = state_str2int[ rxn.state_before_name];
      auto isa = state_str2int[ rxn.state_after_name];
      rxn.state_before = isb;
      rxn.state_after = isa;
      states[ isb].n = isb;
      states[ isb].rxns_from.push_back( rxn.n);
      states[ isa].n = isa;
      states[ isa].rxns_to.push_back( rxn.n);
    }

    first_state_number_data = state_str2int[ first_state_name];
  }

  //********************************************************************
  // constructor
  template< class I>
  Pathway<I>::Pathway( const Molecule_Info& minfo, const String& file){

    std::ifstream input( file);
    if (!input.is_open()){
      error( "pathways file ", file, " could not be opened");
    }
    JP::Parser parser( input, file);
    auto node = parser.top();
    node->complete();

    auto rsnode = node->child( "reactions");
    if (rsnode){
      auto rnodes = rsnode->children_of_tag( "reaction");
      for( auto& rnode: rnodes){
        reactions.emplace_back( minfo, rnode);
      }
    }
    
    first_state_name = checked_value_from_node< std::string>( node, "first_state");
    determine_states();
  }

}
}


