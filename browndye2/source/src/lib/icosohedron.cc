#include "../global/pi.hh"
#include "icosohedron.hh"

namespace Browndye{

Vector< Vec3< double> > icosohedron_tri_cens( int order){
  const auto rim_height = 1.0/sqrt( 5.0);
  const auto rim_radius = 2.0/sqrt( 5.0);
  //const auto pi2 = 2.0*pi;
  
  typedef Vec3< double> V3;
  
  V3 top{0.0, 0.0, 1.0};
  auto top_rim =
    initialized_vector( 5,
      [&]( int i){ 
    	auto th = 0.2*pi2*i;
    	auto cth = cos( th);
      	auto sth = sin( th);
      	return V3{ rim_radius*cth, rim_radius*sth, rim_height};
       });
        
  V3 bottom{ 0.0, 0.0, -1.0};
  auto bottom_rim = 
    initialized_vector( 5,
      [&]( int i){
    	auto th = 0.2*pi2*(i + 0.5);
    	auto cth = cos( th);
    	auto sth = sin( th);
  	    return V3{ rim_radius*cth, rim_radius*sth, -rim_height};
     });
    
  	  
  auto top_cap = 
    initialized_vector( 5,
      [&]( int i){  
    	return Vec3< V3>{ top_rim[i], top, top_rim[(i+1) % 5]};
     }
    );
  
  auto bottom_cap = 
    initialized_vector( 5,
      [&]( int i){
                       
    	return Vec3< V3>{ bottom_rim[i], bottom, bottom_rim[(i+1) % 5]};
     }
    );
  
  auto up_belt = 
    initialized_vector( 5,
       [&]( int i){
    	 return Vec3< V3>{ bottom_rim[i], top_rim[(i+1) % 5],
                        	 bottom_rim[(i+1) % 5]}; 
     }
    );
  
  auto down_belt = 
    initialized_vector( 5, 
       [&]( int i){ 
    	 return Vec3< V3>{ top_rim[i], bottom_rim[i], top_rim[(i+1) % 5]};
     }
    );
  
  Vector< Vec3< V3> > tris;
  tris.insert( tris.end(), top_cap.begin(), top_cap.end());
  tris.insert( tris.end(), bottom_cap.begin(), bottom_cap.end());
  tris.insert( tris.end(), up_belt.begin(), up_belt.end());
  tris.insert( tris.end(), down_belt.begin(), down_belt.end());
  
  auto divided = [&]( const Vector< Vec3< V3> >& tris){
    Vector< Vec3< V3> > res;
    res.reserve( 4*tris.size());
    for( auto& tri: tris){
      auto v01 = 0.5*(tri[0] + tri[1]);
      auto v12 = 0.5*(tri[1] + tri[2]);
      auto v20 = 0.5*(tri[2] + tri[0]);
      
      auto add = [&]( V3 v0, V3 v1, V3 v2){
        res.push_back( Vec3< V3>{ v0,v1,v2});
      };
      auto v0 = tri[0];
      auto v1 = tri[1];
      auto v2 = tri[2];
      add( v0, v01, v20);
      add( v1, v12, v01);
      add( v2, v20, v12);
      add( v01, v12, v20);
    }
    return res;
  };
  
  for( int i = 0; i < order; i++)
    tris = divided( tris);
  
  return tris.mapped( [&]( auto tri){ return (tri[0] + tri[1] + tri[2])/3.0;}); 
}

}