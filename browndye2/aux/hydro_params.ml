(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* Computes the hydrodynamic radius using the algorithm of Hansen 
(J. Chem. Phys. 121, 9111 (2004)).  The hydrodynamic radius can be
approximated as the inverse of the average of the inverse length of
the chords that connect one surface point to another.

The input is the grid of 1's and 0's output by "inside_points".

In this implementation, the molecular surface is represented by a 
cubic grid. The areas of intersection within each cube are
computed ahead of time, and used to weight the chord averages during
Monte Carlo averaging.
*)

Arg.parse
  []
  (fun a -> ())
  "Computes hydrodynamic parameters.  Takes output of \"inside_points\" as input"
;;


(* The following code computes the area of intersection within a cube for
every combination of inside (white) and outside (black) vertices.
*)

type vertex = White | Black;;

let edges = [| 
  ((0,0,0), (1,0,0)); ((1,0,0), (1,1,0)); ((1,1,0), (0,1,0)); ((0,1,0), (0,0,0)); 
  ((0,0,1), (1,0,1)); ((1,0,1), (1,1,1)); ((1,1,1), (0,1,1)); ((0,1,1), (0,0,1)); 
  ((0,0,0), (0,0,1)); ((1,0,0), (1,0,1)); ((1,1,0), (1,1,1)); ((0,1,0), (0,1,1)); 
|];;

let fdiags = [|
  ((0,0,0),(1,1,0)); ((1,0,0),(0,1,0)); ((0,0,1),(1,1,1)); ((1,0,1),(0,1,1));
  ((0,0,0),(0,1,1)); ((0,1,0),(0,0,1)); ((1,0,0),(1,1,1)); ((1,1,0),(1,0,1)); 
  ((0,0,0),(1,0,1)); ((1,0,0),(0,0,1)); ((0,1,0),(1,1,1)); ((1,1,0),(0,1,1)); 
|];;

let ldiags = [| ((0,0,0),(1,1,1)); ((1,0,0),(0,1,1)); 
	       ((1,1,0),(0,0,1)); ((0,1,0),(1,0,1)); |];;  

let fingerprint cube = 
  let sum = 
    let psum = ref 0 in
      for ix = 0 to 1 do
	for iy = 0 to 1 do
	  for iz = 0 to 1 do
	    psum := !psum + 
	    (match cube.(ix).(iy).(iz) with
	       | Black -> 0
	       | White -> 1
	    )
	  done;
	done;
      done;
      if !psum > 4 then
	8 - !psum
      else
	!psum
  in

  let value edge = 
    let (ix,iy,iz),(jx,jy,jz) = edge in
      match cube.(ix).(iy).(iz), cube.(jx).(jy).(jz) with
	| White, White -> 0
	| White, Black -> 1
	| Black, Black -> 0
	| Black, White -> 1	    
  in
  let total edges = 
    Array.fold_left
      (fun sum edge ->
	 sum + (value edge)
      )
      0
      edges

  in
  let nedges = total edges
  and nfdiags = total fdiags
  and nldiags = total ldiags
  in
    sum,nedges,nfdiags,nldiags
;;

let initial_cube () = 
  [| [| [| Black; Black;|]; [| Black; Black;|]; |]; 
     [| [| Black; Black;|]; [| Black; Black;|]; |]; |]
;;

let pf c = 
  let n0,n1,n2,n3 = fingerprint c in
    Printf.printf "%d %d %d %d\n" n0 n1 n2 n3
;;

(*
(* 1 *)
let c = initial_cube();;
c.(0).(0).(0) <- White;;
pf c;;

(* area: tri = sqrt(3)/8 *)

(* 2 *)
let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
pf c;;

(* area: rec = sqrt(2)/2 *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(1).(0) <- White;;
pf c;;

(* area: 2*tri *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(1).(1) <- White;;
pf c;;

(* area: 2*tri *)

(* 3 *)
let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(0) <- White;;
pf c;;

(* area: 1/2 + 3*tri *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(1) <- White;;
pf c;;

(* area: rec + tri *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(1).(0) <- White;;
c.(1).(0).(1) <- White;;
pf c;;

(* area: 3*tri *)

(* 4 *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(0) <- White;;
c.(1).(1).(0) <- White;;
pf c;;

(* area: 1 *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(0) <- White;;
c.(1).(1).(1) <- White;;
pf c;;

(* area: 1/2 + 4*tri *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(0) <- White;;
c.(1).(0).(1) <- White;;
pf c;;

(* area: sqrt(2) *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(0).(0) <- White;;
c.(0).(1).(0) <- White;;
c.(0).(0).(1) <- White;;
pf c;;

(* area: 6*tri *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(0).(0).(1) <- White;;
c.(1).(1).(0) <- White;;
c.(1).(1).(1) <- White;;
pf c;;

(* area: 2*rect *)

let c = initial_cube();;
c.(0).(0).(0) <- White;;
c.(1).(1).(0) <- White;;
c.(1).(0).(1) <- White;;
c.(0).(1).(1) <- White;;
pf c;;


(* area: 4*tri *)
*)

let tri = sqrt(3.0)/.8.0;;
let rect = sqrt(2.0)/.2.0;;

let areas = [|
  (sqrt 3.0)/.8.0 ;
  (sqrt 2.0)/.2.0 ;
  2.0*.tri ;
  2.0*.tri ;
  1.0/.2.0 +. 3.0*.tri ;
  rect +. tri ;
  3.0*.tri ;
  1.0 ;
  1.0/.2.0 +. 4.0*.tri ;
  (sqrt 2.0) ;
  6.0*.tri ;
  2.0*.rect ;
  4.0*.tri ;
|]
;;

let sigs = 
[| 
  (1, 3, 3, 1);
  (2, 4, 6, 2);
  (2, 6, 4, 2);
  (2, 6, 6, 0);
  (3, 5, 7, 3);
  (3, 7, 7, 1);
  (3, 9, 3, 3);
  (4, 4, 8, 4);
  (4, 8, 6, 2);
  (4, 6, 8, 2);
  (4, 6, 6, 4);
  (4, 8, 8, 0);
  (4, 12, 0, 4)
|]
;;

let sig_areas = Hashtbl.create 13;;

for i = 0 to 12 do
  Hashtbl.add sig_areas sigs.(i) areas.(i);
done
;;

module IP = Igrid_parser;;

let grid_info = IP.grid true Scanf.Scanning.stdin;;

let hx,hy,hz = grid_info.IP.spacing;;
let grid = grid_info.IP.data;;

let len = Array.length;;

open Bigarray;;

let nx,ny,nz = Array3.dim1 grid, Array3.dim2 grid, Array3.dim3 grid;;
(* len grid, len grid.(0), len grid.(0).(0);; *)

let is_surface ix iy iz = 
  let sum = ref 0 in
    for kx = 0 to 1 do
      for ky = 0 to 1 do
	for kz = 0 to 1 do
	  sum := !sum + grid.{ix+kx,iy+ky,iz+kz};
	done;
      done;
    done;
    not ((!sum = 0) || (!sum = 8))
;;

let sig_cube ix iy iz = 
  let scube = [| [| [| Black; Black |]; [| Black; Black |]; |];  
		 [| [| Black; Black |]; [| Black; Black |]; |]; |]
  in
    for kx = 0 to 1 do
      for ky = 0 to 1 do
	for kz = 0 to 1 do
	  match grid.{ix+kx,iy+ky,iz+kz} with
	    | 1 -> scube.(kx).(ky).(kz) <- White
	    | 0 -> ()
	    | _ -> raise (Failure "hydro_radius: other than 0 or 1 in grid")
	done;
      done;
    done;
    scube
;;

module V = Vec3;;

let area_position ix iy iz = 
  let vpos kx ky kz = 
    V.v3 
      (hx*.(float (ix+kx))) (hy*.(float (iy+ky))) (hz*.(float (iz+kz)))
  in
  let epos jx jy jz kx ky kz = 
    V.vscaled 0.5 (V.vsum (vpos jx jy jz) (vpos kx ky kz))
  in
  let sum, n = 
    Array.fold_left 
      (fun sums edge ->
	 let (jx,jy,jz),(kx,ky,kz) = edge in
	   if (grid.{ix+jx,iy+jy,iz+jz} + 
	      grid.{ix+kx,iy+ky,iz+kz})  = 1 
	   then
	     let sum,n = sums in
	       (V.vsum sum (epos jx jy jz kx ky kz)), (n+1)
	   else 
	     sums
      )
      ((V.v3 0.0 0.0 0.0),0)
      edges
  in
    V.vscaled (1.0/.(float n)) sum
;;
  
let surface_ref = ref [];;

for ix = 0 to nx-2 do
  for iy = 0 to ny-2 do
    for iz = 0 to nz-2 do
      if is_surface ix iy iz then
	let scube = sig_cube ix iy iz in
	let signa = fingerprint scube in
	let area = Hashtbl.find sig_areas signa in
	let pos = area_position ix iy iz in
	  surface_ref := (area,pos) :: !surface_ref 
    done;
  done;
done;;

(* all surface cubes,with intersection areas *)
let surface = Array.of_list !surface_ref;;
let nsu = len surface;;

let nsamp = 1000000;;

Random.init 1111111;;

let psum = ref (V.v3 0.0 0.0 0.0);;
let a0sum = ref 0.0;; 
let asum = ref 0.0;;
let inv_chord_sum = ref 0.0;;
for i = 1 to nsamp do
  let repeat = ref true in
    while !repeat do
      let i0 = Random.int nsu in
      let i1 = Random.int nsu in
	if not (i0 = i1) then (
	  repeat := false;
	  let a0,pos0 = surface.(i0)
	  and a1,pos1 = surface.(i1) in
          (* psum := V.vsum !psum (V.vscaled a0 pos0);
             a0sum := !a0sum +. a0; *)
	  let r = V.distance pos0 pos1 in
	  asum := !asum +. a0*.a1;
	  inv_chord_sum := !inv_chord_sum +. a0*.a1/.r;
	)
    done;
done;;


Array.iter 
  (fun elem -> 
    let a,pos = elem in
    psum := V.vsum !psum (V.vscaled a pos);
    a0sum := !a0sum +. a;
  ) 
  surface
;;

let corner = 
  let x,y,z = grid_info.IP.corner in
  V.v3 x y z
;;

let center = V.vsum corner (V.vscaled (1.0/.(!a0sum)) !psum);; 
let hydro_r = !asum /. !inv_chord_sum;;

let max_dist = 
  Array.fold_left 
    (fun r surf ->
      let a,rpos = surf in
      let pos = V.vsum corner rpos in
      max r (V.distance center pos)
    )
    0.0
    surface
;;
  
let pr = Printf.printf;;
  
pr "<top>\n";;
pr "  <center> %g %g %g </center>\n" (center.V.x) (center.V.y) (center.V.z);;
pr "  <max_distance> %g </max_distance>\n" max_dist;;
pr "  <radius> %g </radius>\n" hydro_r;;   
pr "</top>\n";;  
    


