#pragma once

#include "../../molsystem/system_state.hh"
#include "../../forces/other/mm_nonbonded_parameter_info.hh" 
#include "absinth.hh"
#include "../parameters.hh"
#include "../excluded_flags.hh"

namespace Browndye{
  
  
  //##################################################################
  // will change once multiple chains and cores are implemented
  
  struct Absinth_State{
    System_State& state;
    Core_Thread* core_thread = nullptr;
    
    typedef Parameters< MM_Nonbonded_Parameter_Info, MM_Bonded_Parameter_Info> Params;
    const Params& params;
    
    Absinth_State( System_State& state, Core_Thread*, const Params&);
  };
  
  inline
  Absinth_State::Absinth_State( System_State& _state, Core_Thread* _ct, const Params& _params):
    state(_state), core_thread(_ct), params(_params){
  }
  
  struct ARef{
    Chain_State_Atom& atom;
    const Atom& patom;
    
    ARef( Chain_State_Atom& _atom, const Atom& _patom):
      atom(_atom), patom(_patom){}
  };
  
  
  //################################################
  class Absinth_Flex_Iface{
  public:
    typedef ARef Atom_Ref;
    
    static
    Pos position( const Absinth_State& state, Atom_Ref ia){
      return atom_of( state, ia).pos;
    }
  
    static
    Length radius( const Absinth_State& state, Atom_Ref ia){
      return atom_prot( state, ia).radius;
    }
    
    static
    void subtract_from_eta( Absinth_State& state, Atom_Ref ia, double sub_eta){
      auto& atom = atom_of( state, ia);
      auto eta = atom.eta();
      atom.set_eta( eta - sub_eta);
    }
  
    static
    double eta_max( const Absinth_State& state, Atom_Ref ia){
      return atom_prot( state, ia).eta_max();
    }
  
    static
    double eta( const Absinth_State& state, Atom_Ref ia){
      return atom_of( state, ia).eta();
    }
    
    static
    Energy surface_energy( const Absinth_State& state, Atom_Ref ia){
      return atom_prot( state, ia).surface_energy();
    }
    
    static
    void add_to_dEdeta( Absinth_State& state, Atom_Ref ia, Energy dEde){
      atom_of( state, ia).add_to_dEdeta( dEde);
    }
    
    static
    void put_polar_v( Absinth_State& state, Atom_Ref ia, double v){
      atom_of( state, ia).set_polar_v( v);
    }
    
    static
    void put_polar_dvdeta( Absinth_State& state, Atom_Ref ia, double dvde){
      atom_of( state, ia).set_polar_dvdeta( dvde);
    }
  
    static
    double polar_v( const Absinth_State& state, Atom_Ref ia){
      return atom_of( state, ia).polar_v();
    }
  
    static
    Charge charge( const Absinth_State& state, Atom_Ref ia){
      return atom_prot( state, ia).charge;
    }
  
    static
    double polar_dvdeta( const Absinth_State& state, Atom_Ref ia){
      return atom_of( state, ia).polar_dvdeta();
    }
    
    static
    void add_force( Absinth_State& state, Atom_Ref ia, F3 f){
      atom_of( state, ia).force += f;
    }
  
    static
    Energy dEdeta( const Absinth_State& state, Atom_Ref ia){
      return atom_of( state, ia).dEdeta();
    }
  
  private:
    static
    const Atom& atom_prot([[maybe_unused]] const Absinth_State& state, Atom_Ref aref){
      return aref.patom;
    }

    static    
    const Chain_State_Atom& atom_of([[maybe_unused]] const Absinth_State& sys, const Atom_Ref aref){
      return aref.atom;
    }
    
    static    
    Chain_State_Atom& atom_of([[maybe_unused]] Absinth_State& sys, Atom_Ref aref){
      return aref.atom;
    }
  
  };
  
  //##################################################
  class Absinth_Rigid_Iface{
  public:
    typedef Absinth_Atom& Atom_Ref;
    
    static
    Pos position([[maybe_unused]] const Absinth_State& sys, Atom_Ref atom){
      return atom.matom->pos;
    }
    
    static
    Length radius([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.matom->radius;
    }
    
    static
    void subtract_from_eta([[maybe_unused]] Absinth_State& state, Atom_Ref atom, double sub_eta){
      auto eta = atom.eta();
      atom.set_eta( eta - sub_eta);
    }
    
    static
    double eta_max([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.matom->eta_max();
    }
    
    static
    double eta([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.eta();
    }
    
    static
    Energy surface_energy([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.matom->surface_energy();
    }
    
    static
    void add_to_dEdeta([[maybe_unused]] Absinth_State& state, Atom_Ref atom, Energy dEde){
      atom.add_to_dEdeta( dEde);
    }
    
    static
    void put_polar_v([[maybe_unused]] Absinth_State& state, Atom_Ref atom, double v){
      atom.set_polar_v( v);
    }
    
    static
    void put_polar_dvdeta([[maybe_unused]] Absinth_State& state, Atom_Ref atom, double dvde){
      atom.set_polar_dvdeta( dvde);
    }
    
    static
    double polar_v([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.polar_v();
    }
    
    static
    Charge charge([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.matom->charge;
    }
    
    static
    double polar_dvdeta([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.polar_dvdeta();
    }

    // for zero core of zero group only
    static
    void add_force([[maybe_unused]] Absinth_State& state, Atom_Ref atom, F3 f){
      auto& core = state.state.groups.front().core_states.front();
      auto pos = atom.matom->pos;
      core.force += f;
      core.torque += cross( pos, f);
    }
    
    static
    Energy dEdeta([[maybe_unused]] const Absinth_State& state, Atom_Ref atom){
      return atom.dEdeta();
    }
  };
   
  //################################################## 
  class Absinth_Iface{
  public:
    typedef Absinth_State System;
    typedef Absinth_Flex_Iface Flexible_Interface;
    typedef Absinth_Rigid_Iface Rigid_Interface;
    //typedef typename Chain_Common::Ex_Cor_Map Ex_Cor_Map;

    static
    void reset_etas( Absinth_State&);

    template< template< class> class F>
    class Atom_Applier {
    public:
      static void doit(System &);
    };

    template< template< class, class> class F>
    class Pair_Applier{
    public:
      static void doit( System&);
    };

    // third arg true -> field is truncated
    template< template< class, class, bool> class F>
    class Charge_Pair_Applier{
    public:
      static void doit( System&);
    };

    // void add_to_energy( System&, Energy);
    template< class F> static void apply_to_flexible_atoms( System& sys, F&&);
    
    template< class F> static void apply_to_rigid_atoms([[maybe_unused]] System& sys, F&&){}
    template< class F> static void apply_to_flexible_pairs( System& sys, F&&);
    template< class F> static void apply_to_rigid_pairs([[maybe_unused]] System& sys, F&&){}
    template< class F> static void apply_to_flexible_rigid_pairs( System& sys, F&&);
    
    template< class F> static void apply_to_flexible_charge_pairs( System& sys, F&&);
    template< class F> static void apply_to_rigid_charge_pairs([[maybe_unused]] System& sys, F&&){}
    template< class F> static void apply_to_flexible_rigid_charge_pairs([[maybe_unused]] System& sys, F&&);
    
    static void add_to_energy( System&, Energy){} // change later with more complete system
    
    static Length debye_length( System& state){
      return state.params.solvent.debye_length;
    }
    
    typedef Vector< Vector< Atom_Index>, Atom_Index> Chain_Common::* Excl_Ptr;
    
    template< class F>
    static void apply_to_flexible_pairs_gen( System& sys, F&&, Excl_Ptr, bool include_far);
    
    typedef typename Chain_Common::Ex_Cor_Map Chain_Common::* Ex_Cor_Map_Ptr; 
    
    template< class F>
    static void apply_to_flexible_rigid_pairs_gen( System& sys, F&&, Ex_Cor_Map_Ptr);
    
    template< class F>
    static void apply_to_chain_pair( System& sys, F&&, Chain_State& chaini, Chain_State& chainj, [[maybe_unused]] bool include_far);
  };
  
  //################################################################
  template< class F>
  void Absinth_Iface::apply_to_chain_pair( System& sys, F&& f, Chain_State& chaini, Chain_State& chainj,
                                           [[maybe_unused]] bool include_far){
  
    auto& coli = chaini.collision_info->col_struct;
    auto radi = coli.radius();
    auto posi = coli.position();
    auto& cchaini = chaini.common();
    
    auto include_inter = [&]{
      if (include_far){
        return true;
      }
      else{
        auto& colj = chainj.collision_info->col_struct;
        auto radj = colj.radius();
        auto posj = colj.position();
        auto r = distance( posi, posj);
        auto rr = radi + radj + 2.0*Absinth::rw;
        return r < rr;
      }
    }();
    
    if (include_inter){ 
      auto& cchainj = chainj.common();
      auto& atomsi = chaini.atoms;
      auto nai = atomsi.size();
      auto& atomsj = chainj.atoms;
      auto naj = atomsj.size();
      auto& patomsi = cchaini.atoms;
      auto& patomsj = cchainj.atoms;
      for( auto ia: range( nai)){
        ARef iar{ atomsi[ia], patomsi[ia]};
        for( auto ja: range( naj)){
          ARef jar{ atomsj[ja], patomsj[ja]};
          f( sys, iar, jar);
        }
      }
    }
  }
  
  //##################################################################################
  template< class F>
  void Absinth_Iface::apply_to_flexible_pairs_gen( System& sys, F&& f, Excl_Ptr excatptr, bool include_far){

    System_State& state = sys.state;
    auto& groups = state.groups;
    auto ng = groups.size();
    
    for( auto ig: range( ng)) {
      auto &chainsi = groups[ig].chain_states;
      // chains on different groups
      for (auto jg: range(ig)) {
        auto &chainsj = groups[jg].chain_states;
        for (auto &chaini: chainsi) {
          if (chaini.has_near_interaction) {
            for (auto &chainj: chainsj) {
              if (chainj.has_near_interaction) {
                apply_to_chain_pair(sys, f, chaini, chainj, include_far);
              }
            }
          }
        }
      }
      // chains on same group
      auto nc = chainsi.size();
      for (auto ic: range(nc)) {
        auto &chaini = chainsi[ic];
        if (chaini.has_near_interaction) {
          auto &cchaini = chaini.common();
          auto &atomsi = chaini.atoms;
          auto nai = atomsi.size();
          // atoms on different chains
          for (auto jc: range(ic)) {
            auto &chainj = chainsi[jc];
            if (chainj.has_near_interaction) {
              apply_to_chain_pair(sys, f, chaini, chainj, include_far);
            }
          }

          // atoms on same chain
          const auto &excluded_atoms = chaini.common().*excatptr;
          auto &excluded = state.thread().excluded;
          auto &patomsi = cchaini.atoms;
          for (auto ia: range(nai)) {
            fill_in_excluded_flags(excluded_atoms[ia], excluded);
            ARef iar{atomsi[ia], patomsi[ia]};
            for (auto ja: range(ia)) {
              ARef jar{atomsi[ja], patomsi[ja]};
              if (!excluded[ja]) {
                f(sys, iar, jar);
              }
            }
          }
        }
      }
    }
  }
  
  //##################################################################################
  // debug
  template< class Key, class T>
  std::optional< std::reference_wrapper< const T> > map_at( const std::map< Key,T>& mp, const Key& arg){

    auto itr = mp.find( arg);
    if (itr != mp.end()){
      return std::cref( itr->second);
    }
    else{
      return std::nullopt;
    }
  }

  template< class F>
  void Absinth_Iface::apply_to_flexible_rigid_pairs_gen( System& sys, F&& f,  Ex_Cor_Map_Ptr ex_cor_map){

    if (sys.core_thread){
      auto core_thread = sys.core_thread;
      //auto matom0 = &(sys.core_thread->common.atoms.front());
      auto& excluded = sys.state.thread().excluded;
    
      auto& apatches = core_thread->apatches;
      auto& groups = sys.state.groups;

      for( auto& item: apatches){
        auto& patch = item.second;

        auto& cref = core_thread->patch_chain_info[ patch.ichainb];

        while( true){

          auto ig = cref.ig;
          auto ic = cref.ic;

          auto& chain = groups[ig].chain_states[ic];
          if (chain.has_near_interaction){
            auto& cchain = chain.common();
            auto& excl_map = cchain.*ex_cor_map;
            //auto& excl_mapm0 = excl_map.at( Core_Index(0));
            auto excl_mapm0o = map_at( excl_map, Core_Index(0));
            if (!excl_mapm0o) {
              excluded.fill(false);
            }

            for( auto ja: range( patch.iatomb, patch.iatome)) {

              auto& atom = core_thread->patch_atoms[ja];
              auto matom = atom.matom;
              if (excl_mapm0o){
                auto ia = matom->number;
                auto &excl_ats = excl_mapm0o.value().get().at(ia);
                fill_in_excluded_flags(excl_ats, excluded);
              }

              auto &catoms = chain.atoms;
              auto &ccatoms = cchain.atoms;
              auto nac = catoms.size();
              for (auto iac: range(nac)) {
                if (!excluded[iac]) {
                  ARef aref(catoms[iac], ccatoms[iac]);
                  f( sys, aref, atom);
                }
              }
            }
          }

          if (cref.next){
            auto ir = cref.next.value();
            cref = core_thread->patch_chain_info[ ir];
          }
          else{
            break;
          }
        }
      }
    }
  }
  
  //##############################################################
  template< class F>
  void Absinth_Iface::apply_to_flexible_pairs( System& sys, F&& f){

    apply_to_flexible_pairs_gen( sys, f, &Chain_Common::excluded_atoms, false);
  }
  
  template< class F>
  void Absinth_Iface::apply_to_flexible_charge_pairs( System& sys, F&& f){

    apply_to_flexible_pairs_gen( sys, f, &Chain_Common::coulomb_excluded_atoms, true);
  }
  
  //##############################################################
  template< class F>
  void Absinth_Iface::apply_to_flexible_rigid_pairs( System& sys, F&& f){
    
    apply_to_flexible_rigid_pairs_gen( sys, f, &Chain_Common::excluded_core_atoms);
  }

  template< class F>
  void Absinth_Iface::apply_to_flexible_rigid_charge_pairs( System& sys, F&& f){
    
    apply_to_flexible_rigid_pairs_gen( sys, f, &Chain_Common::coulomb_excluded_core_atoms);
  }

  template< template< class, class>  class F>
  void Absinth_Iface::Pair_Applier<F>::doit(Browndye::Absinth_Iface::System& sys) {

    F< Flexible_Interface, Flexible_Interface> fff;
    apply_to_flexible_pairs( sys, fff);

    F< Rigid_Interface , Rigid_Interface > frr;
    apply_to_rigid_pairs( sys, frr);

    F< Flexible_Interface , Rigid_Interface > ffr;
    apply_to_flexible_rigid_pairs( sys, ffr);
  }

  template< template< class, class, bool>  class F>
  void Absinth_Iface::Charge_Pair_Applier<F>::doit(Browndye::Absinth_Iface::System& sys) {

    F< Flexible_Interface, Flexible_Interface, false> fff;
    apply_to_flexible_charge_pairs( sys, fff);

    F< Rigid_Interface , Rigid_Interface, true> frr;
    apply_to_rigid_charge_pairs( sys, frr);

    F< Flexible_Interface , Rigid_Interface, true> ffr;
    apply_to_flexible_rigid_charge_pairs( sys, ffr);
  }

  //###############################################################################
  template< template< class>  class F>
  void Absinth_Iface::Atom_Applier<F>::doit(Browndye::Absinth_Iface::System& sys) {

    F< Flexible_Interface> ff;
    apply_to_flexible_atoms( sys, ff);

    F< Rigid_Interface> fr;
    apply_to_rigid_atoms( sys, fr);
  }

  template< class F>
  void Absinth_Iface::apply_to_flexible_atoms( System& sys, F&& f) {

    System_State &state = sys.state;
    auto &groups = state.groups;
    for (auto &group: groups) {
      auto &chains = group.chain_states;
      for (auto &chain: chains) {
        if (chain.has_near_interaction) {
          auto &cchain = chain.common();
          auto &atoms = chain.atoms;
          auto &patoms = cchain.atoms;
          auto nai = atoms.size();
          for (auto ia: range(nai)) {
            ARef iar{atoms[ia], patoms[ia]};
            f(sys, iar);
          }
        }
      }
    }
  }
}
