#pragma once

#include "../lib/units.hh"
#include "../global/limits.hh"
#include "../structures/solvent.hh"
#include "../global/pos.hh"
#include "../transforms/transform.hh"
#include "../global/browndye_rng.hh"

namespace Browndye{

struct OP_Group_Info{
  OP_Group_Info() = delete;
  OP_Group_Info( Charge _q, Diffusivity _Dtrans, Inv_Time _Drot):
    q(_q), Dtrans( _Dtrans), Drot( _Drot){}

  Charge q{ NaN};
  Diffusivity Dtrans{ NaN};
  Inv_Time Drot{ NaN};
};

// keeps position and orientation of Group 0 constant, assumes Group 0 center at origin

class Outer_Propagator{
public:
  Outer_Propagator( Length bradius, Length mradius, bool hydro_interact, Solvent, OP_Group_Info, OP_Group_Info);
  
  // true if it returns
  std::pair< bool, Transform> new_state( Browndye_RNG&, Transform) const;
  decltype( Length3()/Time()) relative_rate( Length) const;
  
private:
  bool has_hydro_interact;
  Length debye_length;
  Length V_factor;
  Energy kT;
  decltype( Diffusivity()*Length()) D_factor;
  Length bradius, qradius;
  Length a0, a1;
  Length2 a2;
  Inv_Time Drot0, Drot1;
  Length bradius_cover, qradius_cover;
  double return_prob;
  
  Length ts_boundary( Length radius, bool is_inner) const;
  Length cover( bool is_inner) const;
};

}

