#pragma once

#include "../lib/index.hh"

namespace Browndye{

class Atom_Type_Index_Tag{};
typedef Index< Atom_Type_Index_Tag> Atom_Type_Index;

class Atom_Lone_Type_Index_Tag{};
typedef Index< Atom_Lone_Type_Index_Tag> Atom_Lone_Type_Index;

class Atom_Index_Tag{};
typedef Index< Atom_Index_Tag> Atom_Index;

class Ind_Atom_Index_Tag{};
typedef Index< Ind_Atom_Index_Tag> Ind_Atom_Index;

class Chain_Index_Tag{};
typedef Index< Chain_Index_Tag> Chain_Index;

class Ind_Chain_Index_Tag{};
typedef Index< Ind_Chain_Index_Tag> Ind_Chain_Index;

//class Spring_Index_Tag{};
//typedef Index< Spring_Index_Tag> Spring_Index;

class Core_Index_Tag{};
typedef Index< Core_Index_Tag> Core_Index;

class Ind_Core_Index_Tag{};
typedef Index< Ind_Core_Index_Tag> Ind_Core_Index;

//class AIndex_Index_Tag;
//typedef Index< AIndex_Index_Tag> AIndex_Index;

class Group_Index_Tag;
typedef Index< Group_Index_Tag> Group_Index;

class Cor_Cha_Index_Tag{};
typedef Index< Cor_Cha_Index_Tag> Cor_Cha_Index;

//class Sphere_Index_Tag{};
//typedef Index< Sphere_Index_Tag> Sphere_Index;

class State_ITag{};
class Rxn_ITag{};

typedef Index< State_ITag> State_Index;
typedef Index< Rxn_ITag> Rxn_Index;

class Tet_Index_Tag{};
typedef Index< Tet_Index_Tag> Tet_Index;

class Constraint_Index_Tag{};
typedef Index< Constraint_Index_Tag> Constraint_Index;

class Bead_Index_Tag{};
typedef Index< Bead_Index_Tag> Bead_Index;

class Mob_Div_Index_Tag{};
typedef Index< Mob_Div_Index_Tag> Mob_Div_Index;

class Sys_Mob_Div_Index_Tag{};
typedef Index< Sys_Mob_Div_Index_Tag> Sys_Mob_Div_Index;

class Sys_Copy_Index_Tag{};
typedef Index< Sys_Copy_Index_Tag> Sys_Copy_Index;

class Thread_Index_Tag{};
typedef Index< Thread_Index_Tag> Thread_Index;

class Restraint_Index_Tag{};
typedef Index< Restraint_Index_Tag> Restraint_Index;

//class Site_Index_Tag{};
//typedef Index< Site_Index_Tag> Site_Index;

class Bin_Index_Tag{};
typedef Index< Bin_Index_Tag> Bin_Index;

class Dummy_Index_Tag{};
typedef Index< Dummy_Index_Tag> Dummy_Index;

}
