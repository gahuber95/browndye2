#pragma once
#include <cmath>
#include <utility>
#include <cstdint>
#include <tuple>
#include <iostream>
#include <type_traits>

namespace Autodiff_Innards{

using std::integral_constant;
using std::is_same_v;
using std::tuple;
using std::make_tuple;
using std::make_pair;
using std::max;
using std::cout;
using std::endl;

class None{
public:
public:
  typedef std::integral_constant< uint32_t, 1111111> Hash;
};

//###################################################
template< uint32_t n>
using HCon = integral_constant< uint32_t, n>;

constexpr uint32_t trunc( int i, uint32_t key){
  return (key >> 8*i) & 0xFF;
}

constexpr uint32_t nhash( uint32_t hash0, uint32_t key){
  auto hash1 = hash0 + key;
  auto hash2 = hash1 + (hash1 << 10);
  return hash2 ^ (hash2 >> 6);
}

constexpr auto hash_fun( uint32_t keya, uint32_t keyb){
  
  auto key0 = trunc( 0, keya);
  auto key1 = trunc( 1, keya);
  auto key2 = trunc( 2, keya);
  auto key3 = trunc( 3, keya);

  auto key4 = trunc( 0, keyb);
  auto key5 = trunc( 1, keyb);
  auto key6 = trunc( 2, keyb);
  auto key7 = trunc( 3, keyb);
  
  uint32_t hash0( 0);
  auto hashf0a = nhash( nhash( nhash( nhash( hash0, key0), key1), key2), key3);
  auto hashf0 = nhash( nhash( nhash( nhash( hashf0a, key4), key5), key6), key7);
  
  auto hashf1 = hashf0 + (hashf0 << 3);
  auto hashf2 = hashf1 ^ (hashf1 >> 11);
  auto hashf = hashf2 + (hashf2 << 15);
  
  return hashf;
}

template< uint32_t keya, uint32_t keyb>
auto chash_fun( HCon< keya>, HCon< keyb>){
  
  return HCon< hash_fun( keya, keyb)>{};
}

template< uint32_t keya, uint32_t keyb, uint32_t ...keys>
auto chash_fun( HCon< keya> keyac, HCon< keyb> keybc, HCon< keys>... keysc){
  
  return chash_fun( keyac, chash_fun( keybc, keysc...));
}

template< char k0, char k1, char k2, char k3, char k4, char k5, char k6, char k7>
constexpr  uint32_t char_hash(){
    
  typedef uint32_t ui;
  
  uint32_t h0 = k0 + ((ui)k1 << 8*1) + ((ui)k2 << 8*2) + ((ui)k3 << 8*3);
  uint32_t h1 = k4 + ((ui)k5 << 8*1) + ((ui)k6 << 8*2) + ((ui)k7 << 8*3);
    
    
  return hash_fun( h0, h1);
}

constexpr uint32_t fhash = char_hash< 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n'>();

template< char k0, char k1, char k2, char k3, char k4, char k5, char k6, char k7>
class New_Function_Hash{
public:
  static constexpr uint32_t chash = char_hash< k0,k1,k2,k3,k4,k5,k6,k7>();
  typedef HCon< hash_fun( fhash, chash)> Type;
};

//############################################################
template< class Indexv, class Val>
class Constant{
public:
  //static constexpr int level = 0;
  static constexpr int length = 1;
  typedef Indexv Index;
  static constexpr int n = Index::value;
  
  static constexpr uint32_t chash = char_hash< 'c', 'o', 'n', 's', 't', 'a', 'n', 't'>();
  typedef HCon< hash_fun( n, chash)> Hash;
  
  const Val val;

  Val value() const{
    return val;
  }
    
  Constant() = delete;
  explicit Constant( Val _val): val(_val){}
};

template< int n, class Val>
auto new_constant( Val x){
  
  typedef integral_constant< int, n> Index;
  return Constant< Index, Val>{ x};
}

//####################################################
template< class T>
class Is_Constant{
public:
  static constexpr bool value = false; 
};

template< class I, class V>
class Is_Constant< Constant< I,V> >{
public:
  static constexpr bool value = true;
};

//############################################################
template< class F, class Val, class Der, class Arg>
class Expr1{
public:
  Val val;
  Der deriv;
  Arg arg;
  
  typedef typename Arg::Hash Hash0;
  typedef decltype( chash_fun( typename F::Hash(), Hash0())) Hash;
  typedef F Func;
  
  Val value() const{
    return val;
  }
  
  Expr1( Val _val, Der _deriv, Arg _arg): val(_val), deriv(_deriv), arg(_arg){}
  Expr1() = delete;
};

template< class F, class Arg>
auto new_expression1( Arg arg){
  
  auto [val,deriv] = F::f( arg.value());
  
  typedef decltype( val) Val;
  typedef decltype( deriv) Der;
  
  return Expr1< F, Val, Der, Arg>{ val, deriv, arg};
}

//##########################################
template< class T>
class Is_Expr1{
public:
  static constexpr bool value = false; 
};

template< class F, class Val, class Der, class Arg>
class Is_Expr1< Expr1< F, Val, Der, Arg> >{
public:
  static constexpr bool value = true;
};

//###############################################
template< class F, class Val, class Der, class Arg, class New_Arg>
auto replaced_args_expr1( Expr1< F, Val, Der, Arg> expr,
                          New_Arg new_arg){
  
  auto val = expr.val;
  auto der = expr.deriv;
  return Expr1< F, Val,Der, New_Arg>{val,der, new_arg};
}      

//####################################################
// F is units of top function
template< class Indexv, class Val>
class Input_Var{
public:
  //typedef None Function;
  typedef Indexv Index;
  static constexpr int n = Index::value;
  static constexpr uint32_t chash = char_hash< 'i', 'n', 'p', 'u', 't', 'v', 'a', 'r'>();
  typedef HCon< hash_fun( n, chash)> Hash;
  
  const Val val;  
    
  Input_Var() = delete;  
  explicit Input_Var( Val _val): val(_val){}  
    
  Val value() const{
    return val;
  }
};

template< int n, class Val>
auto new_input( Val x){
  
  typedef integral_constant< int, n> Index;
  return Input_Var< Index, Val>{ x};
}

//####################################################
template< class T>
class Is_Input{
public:
  static constexpr bool value = false; 
};

template< class I, class V>
class Is_Input< Input_Var< I,V> >{
public:
  static constexpr bool value = true;
};

//#############################################
template< class Hashv>
class Hash_Ref{
public:
  typedef Hashv Hash;
};

//####################################################
template< class T>
class Is_Hash_Ref{
public:
  static constexpr bool value = false; 
};

template< class T>
class Is_Hash_Ref< Hash_Ref< T> >{
public:
  static constexpr bool value = true;
};

//######################################################
template< class F, class Val, class Der0, class Der1, class Arg0, class Arg1>
class Expr2{
public:  
  typedef typename Arg0::Hash Hash0;
  typedef typename Arg1::Hash Hash1;
  
  Val val;
  Der0 deriv0;
  Der1 deriv1;
  Arg0 arg0;
  Arg1 arg1;
  
  typedef decltype( chash_fun( typename F::Hash(),
                              chash_fun( Hash0(), Hash1()))) Hash;
       
  Val value() const{
    return val;
  }
  
  Expr2( Val _val, Der0 _deriv0, Der1 _deriv1, Arg0 _arg0, Arg1 _arg1):
    val(_val), deriv0( _deriv0), deriv1( _deriv1), arg0(_arg0), arg1( _arg1){}
    
  Expr2() = delete;
};

template< class F, class Arg0, class Arg1> 
auto new_expression2( Arg0 arg0, Arg1 arg1){
  
  auto [val,der0,der1] = F::f( arg0.value(), arg1.value());
  
  auto new_arg0 = refed_up( arg0, arg1);
  typedef decltype( new_arg0) New_Arg0;
  
  typedef decltype( val) Val;
  typedef decltype( der0) Der0;
  typedef decltype( der1) Der1;
  
  return Expr2< F, Val,Der0,Der1, New_Arg0, Arg1>
      {val,der0,der1, new_arg0, arg1};
}

//##########################################
template< class T>
class Is_Expr2{
public:
  static constexpr bool value = false; 
};

template< class F, class Val, class Der0, class Der1,
            class Arg0, class Arg1>
class Is_Expr2< Expr2< F, Val, Der0, Der1, Arg0, Arg1> >{
public:
  static constexpr bool value = true;
};

//############################################################
template< class F, class Val, class Der0, class Der1, class Arg0,
            class Arg1, class New_Arg0, class New_Arg1>
auto replaced_args_expr2( Expr2< F, Val, Der0,Der1, Arg0, Arg1> expr,
                        New_Arg0 new_arg0, New_Arg1 new_arg1){
  
  auto val = expr.val;
  auto der0 = expr.deriv0;
  auto der1 = expr.deriv1;
  return Expr2< F, Val,Der0,Der1, New_Arg0, New_Arg1>
       {val,der0,der1, new_arg0, new_arg1};
}                        

//##################################################
// returns version of expr0 with references to Hash1
template< class Expr0, class Expr1>
auto refed_up_one( Expr0 expr0, Expr1 expr1){
  
  typedef typename Expr0::Hash Hash0;
  typedef typename Expr1::Hash Hash1;
  
  if constexpr (is_same_v< Hash0, Hash1>){
    if constexpr (Is_Constant< Expr0>::value){
      return expr0;
    }
    else{
      return Hash_Ref< Hash1>{};
    }
  }
  
  else if constexpr (Is_Expr1< Expr0>::value){
    auto new_arg = refed_up_one( expr0.arg, expr1);
    return replaced_args_expr1( expr0, new_arg);
  }
  
  else if constexpr (Is_Expr2< Expr0>::value){
    auto new_arg0 = refed_up_one( expr0.arg0, expr1);
    auto new_arg1 = refed_up_one( expr0.arg1, expr1);
    return replaced_args_expr2( expr0, new_arg0, new_arg1);
  }

  else{ // constant, ref, or input
    return expr0;
  }
}

//###########################################################
// returns version of expr0 with references to terms in expr1
// assume that expr0 and expr1 are already internally refed_up
template< class Expr0, class Expr1>
auto refed_up( Expr0 expr0, Expr1 expr1){
   
  auto expr0a = refed_up_one( expr0, expr1);
  
  if constexpr (Is_Expr1< Expr1>::value){
    return refed_up( expr0a, expr1.arg);
  }
  else if constexpr (Is_Expr2< Expr1>::value){
    auto expr0b = refed_up( expr0a, expr1.arg0);
    return refed_up( expr0b, expr1.arg1);
  }
  else if constexpr (Is_Input< Expr1>::value){
    return expr0a;
  }
  else{ // constant, ref
    return expr0;
  }
}
//##########################################
template< class Expr>
class Wrapper{
public:
  Expr expr;
  Wrapper( Expr _expr): expr(_expr){}
  
  auto value() const{
    return expr.value();
  }
};

template< class F, class Expr>
auto ufunction1( Expr expr){
  
  return new_expression1< F>( expr);
}

template< class F, class Expr0, class Expr1>
auto ufunction2( Expr0 expr0, Expr1 expr1){
  
  return new_expression2< F>( expr0, expr1);
}

template< class T>
Wrapper< T> wrapped( T t){
  return Wrapper<T>( t);
}

template< class F, class Expr>
auto function1( F f, Wrapper< Expr> w){
  
  return wrapped( ufunction1< F>( w.expr));
}

template< class F, class Expr0, class Expr1>
auto function2( F f, Wrapper< Expr0> w0, Wrapper< Expr1> w1){
  
  return wrapped( ufunction2< F>( w0.expr, w1.expr));
}

template< int n, class Expr>
auto derivative( Wrapper< Expr> w){
  
  return derivative< n>( w.expr);
}

template< class Expr>
auto value( Wrapper< Expr> w){
  return w.expr.value();
}

template< int n, class Val>
auto inputv( Val x){
  return wrapped( new_input< n>( x));
}

template< int n, class Val>
auto constant( Val x){
  return wrapped( new_constant< n>( x));
}

//###########################################
class Negative{
public:
  static constexpr uint32_t chash = char_hash< 'n','e','g','a','t','i','v','e'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class X>
  static
  auto f( X x){
    auto mx = -x;
    return make_tuple( mx, -1.0);
  }
};

template< class Expr>
auto operator-( Wrapper< Expr> w){
  
  return function1( Negative(), w);
}

//###########################################
class Sqrt{
public:
  static constexpr uint32_t chash = char_hash< 's', 'q', 'u', 'a', 'r', 'e', 'r', 'o'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class X>
  static
  auto f( X x){
    auto sx = sqrt( x);
    return make_tuple( sx, 0.5/sx);
  }
};

template< class Expr>
auto sqrt( Wrapper< Expr> wexpr){
  
  return function1( Sqrt(), wexpr);
}

//###########################################
class Cbrt{
public:
  static constexpr uint32_t chash = char_hash< 'c', 'u', 'b', 'e', 'r', 'o', 'o', 't'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class X>
  static
  auto f( X x){
    auto cx = std::cbrt( x);
    return make_tuple( cx, 1.0/(3.0*cx*cx));
  }
};

template< class Expr>
auto cbrt( Wrapper< Expr> wexpr){
  
  return function1( Cbrt(), wexpr);
}

//###########################################
class Exp{
public:
  static constexpr uint32_t chash = char_hash< 'e', 'x', 'p', 'o', 'n', 'e', 't', 'i'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto ex = std::exp( x);
    return make_tuple( ex, ex);
  }
};

template< class Expr>
auto exp( Wrapper< Expr> wexpr){
  
  return function1( Exp(), wexpr);
}
//###########################################
class Log{
public:
  static constexpr uint32_t chash = char_hash< 'l', 'o', 'g', 'a', 'r', 'i', 't', 'h'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto ex = std::log( x);
    auto dx = 1.0/x;
    return make_tuple( ex, dx);
  }
};

template< class Expr>
auto log( Wrapper< Expr> wexpr){
  
  return function1( Log(), wexpr);
}

//###########################################
class Sine{
public:
  static constexpr uint32_t chash = char_hash< 's', 'i', 'n', 'e', 't', 'r', 'i', 'g'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto sx = std::sin( x);
    auto cx = std::cos( x);
    return make_tuple( sx, cx);
  }
};

template< class Expr>
auto sin( Wrapper< Expr> wexpr){
  
  return function1( Sine(), wexpr);
}

//###########################################
class Cosine{
public:
  static constexpr uint32_t chash = char_hash< 'c', 'o', 's', 'i', 'n', 'e', 't', 'r'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto cx = cos( x);
    auto sx = -std::sin( x);
    return make_tuple( cx, sx);
  }
};

template< class Expr>
auto cos( Wrapper< Expr> wexpr){
  
  return function1( Cosine(), wexpr);
}

//###########################################
class Tangent{
public:
  static constexpr uint32_t chash = char_hash< 't', 'a', 'n', 'g', 'e', 'n', 't', 't'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto tx = std::tan( x);
    auto cx = std::cos(x);
    auto dx = 1.0/(cx*cx);
    return make_tuple( tx, dx);
  }
};

template< class Expr>
auto tan( Wrapper< Expr> wexpr){
  
  return function1( Tangent(), wexpr);
}

//###########################################
class Arctangent{
public:
  static constexpr uint32_t chash = char_hash< 'a', 'r', 'c', 't', 'a', 'n', 'g', '1'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto tx = std::atan( x);
    auto dx = 1.0/(1.0 + x*x); 
    return make_tuple( tx, dx);
  }
};

template< class Expr>
auto atan( Wrapper< Expr> wexpr){
  
  return function1( Arctangent(), wexpr);
}

//###########################################
class Arcsine{
public:
  static constexpr uint32_t chash = char_hash< 'a', 'r', 'c', 's', 'i', 'n', 'e', 't'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto ax = std::asin( x);
    auto dx = 1.0/std::sqrt(1.0 - x*x); 
    return make_tuple( ax, dx);
  }
};

template< class Expr>
auto asin( Wrapper< Expr> wexpr){
  
  return function1( Arcsine(), wexpr);
}

//###########################################
class Arccosine{
public:
  static constexpr uint32_t chash = char_hash< 'a', 'r', 'c', 'c', 'o', 's', 'i', 'n'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto ax = std::acos( x);
    auto dx = -1.0/std::sqrt(1.0 - x*x); 
    return make_tuple( ax, dx);
  }
};

template< class Expr>
auto acos( Wrapper< Expr> wexpr){
  
  return function1( Arccosine(), wexpr);
}

//###########################################
class Erf{
public:
  static constexpr uint32_t chash = char_hash< 'e', 'r', 'r', 'o', 'r', 'f', 'u', 'n'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  static
  auto f( double x){
    auto ex = std::erf( x);
    const auto pi = 2.0*std::acos( 0.0);
    const auto sqpi = std::sqrt( pi);
    auto dx = (2.0/sqpi)*std::exp( -x*x); 
    return make_tuple( ex, dx);
  }
};

template< class Expr>
auto erf( Wrapper< Expr> wexpr){
  
  return function1( Erf(), wexpr);
}

//###########################################
template< size_t n>
class Power{
public:
  
 static constexpr uint32_t chash = char_hash< 'p', 'o', 'w', 'e', 'r', 'o', 'f', (char)n>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class T>
  static
  auto g( T x){
    if constexpr (n % 2 == 0){
      if constexpr (n == 0){
        return 1.0;
      }
      else{
        auto h = Power<n/2>::g( x);
        return h*h;
      }
    }
    else{
      return x*Power<n-1>::g( x);
    }
  }
  
  template< class T>
  static
  auto f( T x){
    
    if constexpr (n > 0){
      auto pmx = Power< n-1>::g(x);
      auto dx = ((double)n)*pmx;
      auto px = pmx*x;
      return make_tuple( px, dx);
    }
    else{
      return make_tuple( 1.0, 0.0/T(1.0));
    }
  }
  
};

template< size_t n, class Expr>
auto power( Wrapper< Expr> wexpr){
  
  return function1( Power<n>(), wexpr);
}

//###########################################
class Arctangent2{
public:
  static constexpr uint32_t chash = char_hash< 'a', 'r', 'c', 't', 'a', 'n', 'g', '2'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class T>
  static
  auto f( T y, T x){
    const T t1{ 1.0};
    auto ayx = atan2( y/t1, x/t1);
    auto r2 = x*x + y*y;
    auto dx = -y/r2;
    auto dy = x/r2;
    return make_tuple( ayx, dx, dy);
  }
  
};

template< class Expry, class Exprx>
auto atan2( Wrapper< Expry> wexpry, Wrapper< Exprx> wexprx){
  
  return function2( Arctangent2(), wexpry, wexprx);
}

//###########################################
class Hypotenuse{
public:
  static constexpr uint32_t chash = char_hash< 'h', 'y', 'p', 'o', 't', 'e', 'n', 'u'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
  
  template< class T>
  static
  auto f( T y, T x){
    const T t1{ 1.0};
    auto h = std::hypot( x/t1, y/t1);
    auto dx = x/h;
    auto dy = y/h;
    return make_tuple( h, dx, dy);
  }
};

template< class Expry, class Exprx>
auto hypot( Wrapper< Expry> wexpry, Wrapper< Exprx> wexprx){
  
  return function2( Hypotenuse(), wexpry, wexprx);
}

//##############################################
class Sum{
public:
  
  template< class X>
  static
  auto f( X a, X b){
    return make_tuple( a + b, 1.0, 1.0);
  }
  
  static constexpr uint32_t chash = char_hash< 's', 'u', 'm', '_', 'p', 'l', 'u', 's'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
};

template< class Expr0, class Expr1>
auto operator+( Wrapper< Expr0> w0, Wrapper< Expr1> w1){
  
  return function2( Sum(), w0, w1);
}

//##############################################
class Diff{
public:
  
  template< class X>
  static
  auto f( X a, X b){
    return make_tuple( a - b, 1.0, -1.0);
  }
  
  static constexpr uint32_t chash = char_hash< 'd', 'i', 'f', 'f', 'e', 'r', 'e', 'n'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
};

template< class Expr0, class Expr1>
auto operator-( Wrapper< Expr0> w0, Wrapper< Expr1> w1){
  
  return function2( Diff(), w0, w1);
}

//########################################################
class Prod{
public:
  
  template< class A, class B>
  static
  auto f( A a, B b){
    return make_tuple( a*b, b, a);
  }
  
  static constexpr uint32_t chash = char_hash< 'p', 'r', 'o', 'd', 'u', 'c', 't', 't'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
};

template< class Expr0, class Expr1>
auto operator*( Wrapper< Expr0> w0, Wrapper< Expr1> w1){
  
  return function2( Prod(), w0, w1);
}

//########################################################
class Quot{
public:
  
  template< class A, class B>
  static
  auto f( A a, B b){
    return make_tuple( a/b, 1.0/b, -a/(b*b));
  }
  
  static constexpr uint32_t chash = char_hash< 'q', 'u', 'o', 't', 'i', 'e', 'n', 't'>();
  typedef HCon< hash_fun( fhash, chash)> Hash;
};

template< class Expr0, class Expr1>
auto operator/( Wrapper< Expr0> w0, Wrapper< Expr1> w1){
  
  return function2( Quot(), w0, w1);
}

// Derivative stuff
//#####################################################
// replaces Input_Var in processing
template< class Indexv, class Adj>
struct Output_Var{
public:
  typedef Indexv Index;
  Adj adj;
};

template< class Index, class Adj>
auto new_output_var( Adj adj){
  
  return Output_Var< Index, Adj>{adj};
}

//#############################################
template< class T>
class Is_Output{
public:
  static constexpr bool value = false; 
};

template< class I, class A>
class Is_Output< Output_Var< I,A> >{
public:
  static constexpr bool value = true;
};

//##################################
// replaces Expr1 in processing
template< class Arg>
struct Output_Expr1{
  const Arg arg;
};

template< class Arg>
auto new_output_expr1( Arg arg){
  return Output_Expr1< Arg>{ arg};
}

//###################################
// replaces Expr2 in processing
template< class Arg0, class Arg1>
struct Output_Expr2{
  const Arg0 arg0;
  const Arg1 arg1;
};

template< class Arg0, class Arg1>
auto new_output_expr2( Arg0 arg0, Arg1 arg1){
  return Output_Expr2< Arg0, Arg1>{ arg0, arg1};
}

//##########################################
template< class T>
class Is_Output_Expr1{
public:
  static constexpr bool value = false; 
};

template< class Arg>
class Is_Output_Expr1< Output_Expr1< Arg> >{
public:
  static constexpr bool value = true;
};

//##########################################
template< class T>
class Is_Output_Expr2{
public:
  static constexpr bool value = false; 
};

template< class Arg0, class Arg1>
class Is_Output_Expr2< Output_Expr2< Arg0, Arg1> >{
public:
  static constexpr bool value = true;
};

//################################################
template< class Hash, class Adj, class Next>
class Hash_Dict{
public:
  Adj adj;
  Next next;
};

template< class Hash, class Adj, class Next>
auto new_hash_dict( Adj adj, Next next){
  return Hash_Dict< Hash, Adj, Next>{ adj, next};
}

template< class Hash, class Adj>
auto with_added_adjoint( Adj adj, None){
  
  return new_hash_dict< Hash>( adj, None{});
}

template< class Hash, class Adj, class Hash0, class Adj0, class Next>
auto with_added_adjoint( Adj dadj, Hash_Dict< Hash0, Adj0, Next> dict){
  
  if constexpr (is_same_v< Hash, Hash0>){
    auto adj = dict.adj + dadj;
    return new_hash_dict< Hash>( adj, dict.next);
  }
  else{
    auto new_next = with_added_adjoint< Hash>( dadj, dict.next);
    return new_hash_dict< Hash0>( dict.adj, new_next);
  }
}

template< class Hash>
auto hash_value( None){
  return None{};
}

template< class Hash, class Hash0, class Adj0, class Next>
auto hash_value( Hash_Dict< Hash0, Adj0, Next> dict){
  
  if constexpr (is_same_v< Hash, Hash0>){
    return dict.adj;
  }
  else {
    return hash_value< Hash>( dict.next);
  }
}

//###################################################################
// replaces: Input_Var with Output_Var,
// Expr1 with Output_Expr1, Expr2 with Output_Expr2
// Hash_Ref with None, Constant with None

template< class Expr, class Adj, class Dict>
auto processed( Expr expr, Adj adj, Dict dict){
  
  typedef typename Expr::Hash Hash;
  auto dadj = hash_value< Hash>( dict);
  constexpr bool has_dval = !is_same_v< decltype( dadj), None>;
  
  auto adjp = [&]{
    if constexpr (has_dval){
      return adj + dadj;
    }
    else{
      return adj;
    }
  }();
  
  if constexpr (Is_Hash_Ref< Expr>::value){
    (void)adjp;
    //typedef typename Expr::Hash Hash;
    auto new_dict = with_added_adjoint< Hash>( adj, dict);
    return make_pair( None{}, new_dict);
  }
  else if constexpr (Is_Input< Expr>::value){
    typedef typename Expr::Index Index;
    auto nov = new_output_var< Index>( adjp);
    return make_pair( nov, dict);
  }
  else if constexpr (Is_Expr1< Expr>::value){
    auto adj0 = adjp*expr.deriv;
    auto [new_arg, new_dict] = processed( expr.arg, adj0, dict);
    auto noe = new_output_expr1( new_arg);
    return make_pair( noe, new_dict);
  }
  else if constexpr (Is_Expr2< Expr>::value){
    auto adj0 = adjp*expr.deriv0;
    auto [new_arg0, new_dict0] = processed( expr.arg0, adj0, dict);
    
    auto adj1 = adjp*expr.deriv1;
    auto [new_arg1,new_dict1] = processed( expr.arg1, adj1, new_dict0);
    
    auto noe = new_output_expr2( new_arg0, new_arg1);
    return make_pair( noe, new_dict1);
  }
  else{ // should be a constant
    return make_pair( None{}, dict);
  } 
}

//#################################################
template< class T>
auto derivative_struct( Wrapper< T> wexpr){
  
  return processed( wexpr.expr, 1.0, None{}).first;
}

//#################################################
template< class Index, class Expr>
auto cderivative( Expr expr){
  
  if constexpr (Is_Output_Expr1< Expr>::value){
    return cderivative< Index>( expr.arg);
  }
  else if constexpr( Is_Output_Expr2< Expr>::value){
    auto res0 = cderivative< Index>( expr.arg0);
    typedef decltype( res0) Res0;
    auto res1 = cderivative< Index>( expr.arg1);
    if constexpr (!is_same_v< None, Res0>){
      return res0;
    }
    else{
      return res1;
    }
  }
  else if constexpr( Is_Output< Expr>::value){
    typedef typename Expr::Index Index0;
    if constexpr (is_same_v< Index0, Index>){
      return expr.adj;
    }
    else{
      return None{};
    }
  }
  else{
    return None{};
  }
}

template< template <class,class...> class Expr,
          class First, class Index, class Val, class...Rest>
auto derivative( Expr< First, Rest...> expr,
                    Wrapper< Input_Var< Index, Val> >){
  
  auto res = cderivative< Index>( expr);
  typedef decltype( res) Res;
  static_assert( !is_same_v< Res, None>, "no derivative found");
  return res;
}

inline
void ps( size_t n){
  for( size_t i = 0; i < n; i++){
    cout << ' ';
  }
}

//############################################
template< class Expr>
void printn( size_t n, Expr expr){
  ps( n);
  auto n2 = n + 2;
  if constexpr (Is_Expr1< Expr>::value){
    typedef typename Expr::Hash Hash;
    cout << "Expr1 " << Hash::value << endl;
    printn( n2, expr.arg);
  }
  else if constexpr (Is_Output_Expr1< Expr>::value){
    cout << "Output Expr1\n";
    printn( n2, expr.arg);
  }
  else if constexpr (Is_Output_Expr2< Expr>::value){
    cout << "Output Expr2\n";
    printn( n2, expr.arg0);
    printn( n2, expr.arg1);
  }
  
  else if constexpr (Is_Expr2< Expr>::value){
    typedef typename Expr::Hash Hash;
    cout << "Expr2 " << Hash::value << endl;
    printn( n2, expr.arg0);
    printn( n2, expr.arg1);
  }
  else if constexpr (Is_Input< Expr>::value){
    typedef typename Expr::Index Index;
    typedef typename Expr::Hash Hash;
    cout << "Input " << Hash::value << ' ' << Index::value << ' '
          << expr.value() << endl;
  }
  else if constexpr (Is_Constant< Expr>::value){
    typedef typename Expr::Index Index;
    typedef typename Expr::Hash Hash;
    cout << "Constant " << Hash::value << ' ' << Index::value << ' '
          << expr.value() << endl;
  }
  else if constexpr (Is_Hash_Ref< Expr>::value){
    typedef typename Expr::Hash Hash;
    cout << "Hashref " << Hash::value << endl;
  }
  else if constexpr (Is_Output< Expr>::value){
    cout << "Output " << Expr::Index::value << ' ' << expr.adj << endl;
  }
  else{
    static_assert( is_same_v< None, Expr>, "print else");
    cout << "None\n";
  }
}

template< class Expr>
[[maybe_unused]] void print( Wrapper< Expr> x){
  printn( 0, x.expr);
}

template< class Expr>
[[maybe_unused]] void printd( Expr expr){
  printn( 0, expr);
}

}
//######################################################
namespace Autodiff{
  using Autodiff_Innards::inputv;
  using Autodiff_Innards::derivative_struct;
  using Autodiff_Innards::derivative;
  using Autodiff_Innards::value;
  using Autodiff_Innards::constant;
  using Autodiff_Innards::power;
}

namespace Autodiff_Dev{
  using namespace Autodiff;
  
  using Autodiff_Innards::Wrapper;
  using Autodiff_Innards::New_Function_Hash;
  using Autodiff_Innards::function1;
  using Autodiff_Innards::function2;
}



