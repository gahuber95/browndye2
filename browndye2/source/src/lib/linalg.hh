#pragma once

#include <utility>
#include "range.hh"
#include "array.hh"
#include "vector.hh"
#include "cholesky.hh"
#include "smat_cholesky_interface.hh"

namespace Browndye{

namespace Linalg{

Vector< Vector< double> >
inverse_ltri_mat( const Vector< Vector< double> >&);

// eigenvalues in ascending order
template< class T>
std::pair< Vector< Vector< double> >, Vector< T> > 
eigensystem( const Vector< Vector< T> >& A);

//################################################
void eigensystem_inner(  Vector< double>& a, Vector< double>& w);

template< class T, size_t n>
std::pair< SMat< double, n,n>, Array< T, n> >
eigensystem( const SMat< T, n,n> A){

  Vector< double> a(n*n), w(n);

  T T1( 1.0);
  for( auto i: range(n)){
    for( auto j: range(n)){
      a[n*i + j] = A[i][j]/T1;
    }
  }

  eigensystem_inner( a, w);

  Array< T, n> evalues;
  for( auto i: range(n)){
    evalues[i] = T1*w[i];
  }

  SMat< double, n,n> evectors;
  for( auto i: range(n)){
    for( auto j: range(n)){
      evectors[i][j] = a[n*i + j];
    }
  }
  
  return std::make_pair( evectors, evalues);
}

//#############################################################
void get_solution_inner(  Vector< double>& a, Vector< double>& b);

template< class Ta, class Tb, size_t n>
Array< decltype( Tb()/Ta()), n>
solution( const SMat< Ta, n,n> A, const Array< Tb,n> B){

  Vector< double> a(n*n), b(n);
  
  Ta Ta1( 1.0);
  for( auto i: range(n)){
    for( auto j: range(n)){
      a[i + n*j] = A[i][j]/Ta1;
    }
  }
    
  Tb Tb1( 1.0);
  for( auto i: range(n)){
    b[i] = B[i]/Tb1;
  }
  
  get_solution_inner( a, b);
  
  typedef decltype( Tb()/Ta()) Tc;
  Tc Tc1( 1.0);
  
  Array< Tc, n> x;
  for( auto i: range( n)){
    x[i] = b[i]*Tc1;
  }

  return x;
}

//##########################################################
Vector< double> solution( const Vector< Vector< double> >& A,
			  const Vector< double>& b);
  
//#########################################################
template< class T>
std::pair< Vector< Vector< double> >, Vector< T> > 
eigensystem( const Vector< Vector< T> >& A){
  
  auto n = A.size();
  Vector< double> a(n*n), w(n);
  
  for( auto i: range(n)){
    for( auto j: range(n)){
      a[n*i + j] = A[i][j]/T(1);
    }
  }
  
  eigensystem_inner( a, w);
  
  Vector< T> evalues( n);
  for( auto i: range(n)){
    evalues[i] = w[i]*T(1);
  }
  
  Vector< Vector< double> > evectors( n);
  for( auto i: range(n)){
    evectors[i].resize( n);
    for( auto j: range(n)){
      evectors[i][j] = a[n*i + j];
    }
  }
  
  return std::make_pair( std::move( evectors), std::move( evalues)); 
}

  //########################################################
// assumes matrix is symmetric and positive definite
template< class T>
Vector< Vector< decltype( 1.0/T())> >
inverse( const Vector< Vector< T> >& A){
  
  typedef decltype( sqrt( T())) ST;
  typedef decltype( 1.0/T()) TI;

  auto n = A.size();
  
  Vector< Vector< ST> > D( n);
  for( auto& row: D){
    row.resize( n);
  }
  
  bool success;
  Cholesky::decompose< Cholesky_LI< T> >( A, D, success);
  if (!success){
    error( "Cholesky failed");
  }
  
  Vector< Vector< TI> > Ainv( n);
  for( auto& row: Ainv){
    row.resize( n);
  }
    
  Vector< Vector< double> > I(n);
  for( auto i: range(n)){
    auto& row = I[i];
    row.resize( n);
    row.fill( 0.0);
    row[i] = 1.0;
  }
  
  for( auto i: range( n)){
    Cholesky::solve< Cholesky_LI< T> >( D, I[i], Ainv[i]);
  }
    
  return Ainv;
}

}

/*
template< class T, size_t n>
SMat< decltype( 1.0/T()), n,n> inverse( SMat< T,n,n> A){
  typedef decltype( sqrt( T())) ST;
  typedef decltype( 1.0/T()) TI;
  SMat< ST, n,n> D;
  Cholesky::decompose< Cholesky_I< T, n> >( A, D);
  
  SMat< TI, n,n> Ainv;
  auto I = id_matrix< n>();
  
  for( auto i: range( n))
    Cholesky::solve< Cholesky_I< T, n> >( D, I[i], Ainv[i]);
  
  return Ainv;
}
*/

}

/*
int main(){

  Mat3< Mobility> A;
  A[0][0] = Mobility( 1.0);
  A[0][1] = Mobility( 0.5);
  A[0][2] = Mobility( 0.25);
  A[1][0] = Mobility( 0.5);
  A[1][1] = Mobility( 1.0);
  A[1][2] = Mobility( 0.5);
  A[2][0] = Mobility( 0.25);
  A[2][1] = Mobility( 0.5);
  A[2][2] = Mobility( 1.0);


  Vec3< Vec3< double> > evecs;
  Vec3< Mobility> evals;

  std::tie( evecs, evals) = eigensystem( A);

  for( auto i: range(3))
    std::out << evecs[i][0] << " " << evecs[i][1] << " " << evecs[i][2] << "\n";
  std::out << "\n";

  for( auto i: range(3))
    for( auto j: range(3))
      std::out << i << " " << j << " " << dot( evecs[i], evecs[j]) << "\n";
  std::out << "\n";

  for( auto i: range(3))
    std::out << evals[i] << " " << dot(A*evecs[i], evecs[i]) << "\n";
}

int main(){

  Mat3< double> A{{0.0,1.0,1.0},{1.0,0.0,1.0},{1.0,1.0,0.0}};
  
  Vec3< double> b{ 1.0, 2.0, 3.0};

  auto x = solution( A, b);
  std::out << x[0] << " " << x[1] << " " << x[2] << "\n";

  auto Ax = A*x;
  std::out << Ax[0] << " " << Ax[1] << " " << Ax[2] << "\n";
}

*/
