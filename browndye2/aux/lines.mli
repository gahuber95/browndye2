(* 
Used internally.
Used for parsing plain text lines.

readline buf : reads text from buf until a carriage return is reached.

stripped str : returns str but with whitespace at either end stripped away

split str: returns array of strings from splitting str along runs of whitespace

*)

val readline: Scanf.Scanning.scanbuf -> string

val stripped: string -> string

val split: string -> string array

