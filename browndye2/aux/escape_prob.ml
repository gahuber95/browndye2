(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* Used internally.  Computes integral used in determining escape
probabilities.

integral a0 a1 q dL, where a0 and a1 are particle radii, q is
sgn( q0*q1)*sqrt( q0*q1), and dL is Debye length.
*)


(* inner absorbing radius, eps, kT, and D are unity 

Characteristics:

Length: inner absorbing radius = R
Time:   R^2/diffusivity
Energy:  kT
Charge:  sqrt( kT eps R) 

q^2 = q0*q1

*)

let pi = 3.1415926

let integral has_hi a0 a1 q dL = 

  let hydro = if has_hi then 1.0 else 0.0 in 

  let q2 = 
    let qq = q*.q in
    let sgn = if q > 0.0 then 1.0 else -1.0 in
      sgn*.qq
  in

  let a = 1.0/.(1.0/.a0 +. 1.0/.a1) in
  let asq = (a0*.a0 +. a1*.a1)/.2.0 in

  let integrand s =
    if s = 0.0 then
      1.0
    else
      let dterm = exp (-1.0/.(s*.dL)) in
      let v = q2*.dterm*.s/.(4.0*.pi) in
      let diff = 
	let r = 1.0/.s in
	  1.0 -. hydro*.(3.0/.r -. 2.0*.asq/.(r*.r*.r))*.a
      in
	(exp v)/.diff
  in
    
  let tol = 1.0e-10 in
    Romberg.integral integrand 0.0 1.0 tol
      
	

(*******************************************)
(* Test code *)
(*
let q = -1.0;;
let r = 3.0;;

let f q b = 
  let q2 = 
    let qq = q*.q in
    let sgn = if q > 0.0 then 1.0 else -1.0 in
      sgn*.qq/.(4.0*.pi) 
  in
    Printf.printf "qqb %g\n" (q2/.b);
    if q2 = 0.0 then
      b
    else
      (((exp (q2/.b)) -. 1.0)/.q2)
;;


Printf.printf "integral %g real %g\n" 
  (integral 1.0 q infinity r) (f q r)
;;

Printf.printf "simple integral %g\n" 
  (Romberg.integral (fun x -> (exp (-.x))) 0.0 1.0 1.0e-6)
;;
*)
