#pragma once

#include "../../lib/units.hh"
#include "field_for_blob.hh"
#include "../../transforms/transform.hh"
#include "../../lib/array.hh"
#include "distance_finder.hh"

namespace Browndye{

// CInterface refers to collision structure of field
//template< class CInterface, class FInterface>
template< class Interface>
class Blob_Interface{
public:
  typedef typename Interface::Charge Charge;
  typedef typename Interface::Potential Potential;
  //typedef ::Transform Transform;
  //typedef Field_For_Blob< CInterface, FInterface> Field;

  static
  void transform( const Transform& tform, const Vec3< Length>& before, Vec3< Length>& after){
    after = tform.transformed( before);
  }

  template< class U, class Transform>
  static
  void rotate( const Transform& trans, const Vec3< U>& before, Vec3< U>& after){
    after = trans.rotated( before);
  }

  template< class Field>
  static
  Potential value( const Field& field, const Vec3< Length>& pos){
    return field.field.potential( pos, field.hint);
  }

  typedef decltype( Potential()/Length()) Potential_Gradient;

  template< class Field>
  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad){
    grad = field.field.gradient( pos, field.hint);
    
  } 

  template< class Field>
  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos){

    Distance_Finder< typename Field::CInterface> df{};
    return field.collision_structure.is_within( df, r, pos);
  }
};
}

