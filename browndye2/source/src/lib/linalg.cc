#include <cassert>
#include "vector.hh"

// lapack subroutines
extern "C" {
  void dsyev_( char* jobz, char* uplo, int* n, double* a, int* lda, 
              double* w, double* work, int* lwork, int* info);
  
  void dgetrf_( int* m, int* n, double* a, int* lda, int* ipiv, int* info);
  
  void dgetrs_( char* trans, int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb,
		int* info);

  void dtrsm_( char* side, char* uplo, char* transa, char* diag, int* m, int* n, double* alpha,
	       double* A, int* lda, double* B, int* ldb);		

}

//####################################################
namespace Browndye{

namespace Linalg{

Vector< Vector< double> >
inverse_ltri_mat( const Vector< Vector< double> >& A){

  int n = A.size();
  assert( n == (int)(A[0].size()));
  Vector< double> b( n*n, 0.0);
  for( auto i: range(n)){
    b[n*i + i] = 1.0;
  }

  char side = 'L';
  char uplo = 'L';
  char transa = 'N';
  char diag = 'N';
  
  double alpha = 1.0;

  Vector< double> a( n*n);
  for( auto i: range(n)){
    for( auto j: range(n)){
      a[i + n*j] = A[i][j];
    }
  }

  dtrsm_( &side, &uplo, &transa, &diag, &n, &n, &alpha,
	  a.data(), &n, b.data(), &n);

  Vector< Vector< double> > X(n);
  for( auto i: range(n)){
    X[i].resize( n);
    for( auto j: range(i+1)){
      X[i][j] = b[i + n*j];
    }
  }

  return X;
}

  //##############################################################  
void get_solution_inner( Vector< double>& a, Vector< double>& b){
  int n = b.size();
  Vector< int> ipiv( n);
  int info;
  dgetrf_( &n, &n, a.data(), &n, ipiv.data(), &info);
  if (info != 0){
    error( "solution: problems with LU decomposition");
  }
    
  int nrhs = 1;
  char trans = 'N';
  dgetrs_( &trans, &n, &nrhs, a.data(), &n, ipiv.data(), b.data(), &n, &info); 
  
  if (info != 0){
    error( "solution: problems with backsubstitution");
  }
}

//###############################################################
Vector< double> solution( const Vector< Vector< double> >& A,
			  const Vector< double>& b){

  auto n = b.size();
  assert( n == A.size() && n == A[0].size());
  Vector< double> a(n*n), x(n);
  
  for( auto i: range(n)){
    for( auto j: range(n)){
      a[i + n*j] = A[i][j];
    }
  }
  
  for( auto i: range(n)){
    x[i] = b[i];
  }
  
  get_solution_inner( a, x);
  return x;  
}
  
//###############################################################
void eigensystem_inner(  Vector< double>& a, Vector< double>& w){

  int n = w.size();
  char jobz = 'V';
  char uplo = 'U';
  int lda = n;
  int lwork;
  Vector< double> work(1);
  int info;

  lwork = -1;
  dsyev_( &jobz, &uplo, &n, a.data(), &lda, w.data(), work.data(), 
          &lwork, &info); 

  lwork = work[0];
  work.resize( lwork);

  dsyev_( &jobz, &uplo, &n, a.data(), &lda, w.data(), work.data(), 
          &lwork, &info);

  if (info != 0){
    error( "eigensystem: did not work");
  }
}

}}


// convert to unit test
/*


void main1( std::mt19937_64& gen){

  std::uniform_real_distribution< double> uni;
  SMat< double, 3,3> a;

  for( auto i: range(3))
    for( auto j: range(i+1)){
      a[i][j] = a[j][i] = uni( gen);
    }

  auto res = eigensystem( a);

  auto& evalues = res.second;
  auto& evectors = res.first;

  for( auto i: range(3)){
    auto vec = evectors[i];
    
    Array< double, 3> prod;
    for( auto j: range(3)){
      double sum = 0.0;
      for( auto k: range(3))
        sum += a[j][k]*vec[k];
      prod[j] = sum;
    }
    
    auto p0 = dot( vec, vec);
    auto p1 = dot( prod, prod)/sq( evalues[i]);
    auto p2 = dot( prod, vec)/evalues[i];

    const double tol = 1.0e-6;
    auto test_close = [&]( double x){
      if (!(1.0-tol < x && x < 1.0+tol))
        error( "eigensystem: not passed");
    };

    test_close( p0);
    test_close( p1);
    test_close( p2);

  }

}

int main(){
  std::mt19937_64 gen;
  for( int i = 0; i < 1000; i++)
    main1( gen);
}

*/

/*
void main1( std::mt19937_64& gen){
  std::uniform_real_distribution< double> uni;
  
  SMat< double, 3,3> a;
  for( auto i: range(3))
    for( auto j: range(3)){
      a[i][j] = a[j][i] = uni( gen);
    }
  
  Array< double, 3> b;
  for( auto i: range(3))
    b[i] = uni( gen);
  
  auto x = Linalg::solution( a, b);
  
  auto bt = a*x;
  auto db = distance( b, bt);

  if (db > 1.0e-12)
    error( "solve: failed");
}

int main(){
  std::mt19937_64 gen;
  for( int i = 0; i < 1000; i++)
    main1( gen);
}
*/
