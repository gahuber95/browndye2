(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* 
Not called by bd_top.  Generates a list of reaction-defining pairs
from the structure of the bi-molecular complex and a file of 
complementary atoms (for example, hydrogen-bonding pairs.)  
It searches for contacts with a specified search radius.

It takes the following arguments, with tags:

-mol0 : pqrxml file of Molecule 0
-mol1 : pqrxml file of Molecule 1
-ctypes : file of contact types
-dist : search radius (default 4.5)
-nonred : generate non-redundant pairs  

If the -nonred flag is present, then after finding possible pairs, 
the program eliminates redundant pairs. It groups the pairs according to 
connectivity, selects the closest bond in each group, and eliminates all 
other pairs involving the atoms of the closest pair. It then regroups and 
eliminates repeatedly until no more connected groups are left. 

*)

let len = Array.length;;
let pr = Printf.printf;;

let mol0_file_ref = ref "";;
let mol1_file_ref = ref "";;
let ctypes_file_ref = ref "";;
let search_distance_ref = ref 4.5;;
let non_redundant_ref = ref false;;

Arg.parse
  [("-mol0", Arg.Set_string mol0_file_ref, "xml file of Molecule 0");
   ("-mol1", Arg.Set_string mol1_file_ref, "xml file of Molecule 1");
   ("-ctypes", Arg.Set_string ctypes_file_ref, "file of contact types");
   ("-dist", Arg.Set_float search_distance_ref, "search radius (default 4.5)");
   ("-nonred", Arg.Set non_redundant_ref, "generate non-redundant pairs");
  ]
  (fun a -> raise (Failure "no anonymous arguments"))
  "outputs file of contact pairs between two molecules; removes redundant pairs"
;;

let mol0_file = !mol0_file_ref;;
let mol1_file = !mol1_file_ref;;
let ctypes_file = !ctypes_file_ref;;
let search_distance = !search_distance_ref;;
let non_redundant = !non_redundant_ref;;

type atom = {
  spos: Vec3.t;
  atom_type: string;
  residue_type: string;
  number: int;
};;

let atoms fname =
  let buf = Scanf.Scanning.from_file fname in
  let alist = ref [] in
    Atom_parser.apply_to_atoms ~buffer: buf
      ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let new_atom = {
	  spos = Vec3.v3 x y z;
	  atom_type = aname;
	  residue_type = rname;
	  number = anumber;
	}
	in
	  alist := new_atom :: (!alist);
      );
    Array.of_list (List.rev (!alist))
;;

let atoms0 = atoms mol0_file;;
let atoms1 = atoms mol1_file;;

module JP = Jam_xml_ml_pull_parser;;

module Cont = 
  struct
    type t = string*string
    let compare x y = compare x y
  end

module Contact_Set = Set.Make( Cont);;

module Pair = 
  struct
    type t = atom*atom
    let compare p0 p1 = compare p0 p1
  end

module Pair_Set = Set.Make( Pair);;

module Group = 
  struct
    type t = Pair_Set.t
    let compare p0 p1 = compare p0 p1
  end

module Group_Set = Set.Make( Group);;

module H = Hashtbl;;
module NI = Node_info;;

let contacts = 
  let dict = H.create 1 in
  let parser = JP.new_parser (Scanf.Scanning.from_file ctypes_file) in
    JP.complete_current_node parser;
    let cnode = JP.current_node parser in
    let add_pair (c0: string*string) (c1: string*string) = 
      let c1s = 
	if H.mem dict c0 then
	  H.find dict c0 
	else
	  Contact_Set.empty
      in
      let new_c1s = Contact_Set.add c1 c1s in
	H.replace dict c0 new_c1s
    in
      
    let contact_of_node cnode = 
      let a = NI.string_of_child cnode "atom"
      and r = NI.string_of_child cnode "residue" in
	a,r
    in

    let pair_of_node pnode = 
      let cnode0 = NI.checked_child pnode "contact0"
      and cnode1 = NI.checked_child pnode "contact1"
      in
	contact_of_node cnode0, contact_of_node cnode1
    in

    let enode_opt = JP.child cnode "explicit" in
      (match enode_opt with
	| None -> ()
	| Some enode ->
	    let pnodes = JP.children enode "pair" in
	      List.iter
	      (fun pnode ->
		let c0,c1 = pair_of_node pnode in
		  add_pair c0 c1
	      )
		pnodes
      );

      let cmnodes = JP.children cnode "combinations" in
	List.iter
	  (fun cmnode -> 
	    let mnode0 = NI.checked_child cmnode "molecule0"
	    and mnode1 = NI.checked_child cmnode "molecule1"
	    in
	    let cnodes0_l = JP.children mnode0 "contact" 
	    and cnodes1_l = JP.children mnode1 "contact" 
	    in
	    let cnodes0 = Array.of_list cnodes0_l
	    and cnodes1 = Array.of_list cnodes1_l    
	    in
	    let cons0 = Array.map (fun node -> contact_of_node node) cnodes0
	    and cons1 = Array.map (fun node -> contact_of_node node) cnodes1
	    in

	    let n0,n1 = len cons0, len cons1 in
	      for i0 = 0 to n0-1 do
		let c0 = cons0.(i0) in
		  for i1 = 0 to n1-1 do
		    let c1 = cons1.(i1) in
		      add_pair c0 c1
		  done;
	      done
	  )
	  cmnodes;

	dict
;;


module IIS = Is_inside_spheres;;

let stree1 = IIS.search_tree 
  (fun sph -> sph.spos)
  (fun sph -> 0.0)
  atoms1
;;

let aname atom = 
  (atom.atom_type, atom.residue_type)
;;

let atom_pairs = 
  Array.fold_left
    (fun res atom0 ->
      let key0 = aname atom0 in
	if H.mem contacts key0 then
	  let contacts0 = 
	    H.find contacts key0
	  in	  
	  let pairs_opt = 
	    IIS.fold_tree_within_distance
	      (fun atom -> true)
	      atom0.spos
	      search_distance
	      (fun atom1 -> 
		  if Contact_Set.mem (aname atom1) contacts0 then (
		    [(atom0,atom1)]
		  )
		  else
		    []
	      )
	      List.rev_append
	      stree1
	  in
	    match pairs_opt with
	      | None -> res
	      | Some pairs -> List.rev_append pairs res
	else
	  res
    )
    []
    atoms0
;;

type pset_ref = PSet of Pair_Set.t | PSet_Ref of pset_ref ref;; 

let rec top_pset_ref pf = 
  match !pf with
    | PSet pset -> pf
    | PSet_Ref pff -> top_pset_ref pff
;;

let pset_of pf = 
  match pf with
    | PSet p -> p
    | PSet_Ref pff -> failwith "pset_of: should not get here"
;;

let rec merge_psets p0 p1 =
  let tpf0 = top_pset_ref p0
  and tpf1 = top_pset_ref p1 in
  let ps0 = pset_of !tpf0
  and ps1 = pset_of !tpf1 in
  let ps = PSet (Pair_Set.union ps0 ps1) in
  let pf = PSet_Ref (ref ps) in 
  tpf0 := pf;
  tpf1 := pf
;;  
	
let connected_groups pairs = 
  let keys0, keys1 = H.create 0, H.create 0 in
  List.iter 
    (fun pair ->
      let i0,i1 = pair in
      let replace keys i = 
	if H.mem keys i then
	  let lst = H.find keys i in
	  H.replace keys i (pair::lst)
	else
	  H.add keys i [pair]
      in
      replace keys0 i0;
      replace keys1 i1;
    )
    pairs;
  
  let group_dict = 
    let gdict = H.create 0 in
    List.iter
      (fun pair -> 
	H.add gdict pair 
	  (ref (PSet (Pair_Set.singleton pair)));
      )
      pairs;

    let lump_keys keys = 
      H.iter  
	(fun i plst ->
	  let n = List.length plst in 
	  if n > 1 then
	    let parr = Array.of_list plst in
	    for k0 = 0 to n-1 do
	      for k1 = 0 to k0-1 do
		let pair0 = parr.(k0) in
		let setr0 = H.find gdict pair0 in
		let set0 = !(top_pset_ref setr0) in 		
		let pair1 = parr.(k1) in
		let setr1 = H.find gdict pair1 in
		let set1 = !(top_pset_ref setr1) in
		if not (set0 = set1) then  
		  merge_psets setr0 setr1
	      done
	    done
	)
	keys
    in
    lump_keys keys0;
    lump_keys keys1;

    let clean_gdict = H.create 0 in
    H.iter
      (fun pair pf ->
	let tpf = top_pset_ref pf in
	let ps = pset_of !tpf in
	H.add clean_gdict pair ps;
      )
    gdict;
    clean_gdict
  in
  H.fold 
    (fun pair group gset ->
      if (Pair_Set.cardinal group) > 1 then
	Group_Set.add group gset
      else
	gset
    )
    group_dict
    Group_Set.empty 
;; 

let some_of opt = 
  match opt with
    | Some item -> item
    | None -> failwith "some_of: nothing there"
;;
  
let list_diff lst excluded = 
  let set_of_list lst = 
    (List.fold_left  
       (fun res item ->
	 Pair_Set.add item res
       )
       (Pair_Set.empty)
       lst       
    )
  in
  let orig_set = set_of_list lst
  and excl_set = set_of_list excluded in
  let set = Pair_Set.diff orig_set excl_set in
  Pair_Set.elements set
;;

(* does just one round *)
let stripped_pairs pairs groups = 
  let exc_pairs = 
    Group_Set.fold
      (fun group res -> 
	let closest_pair =
	  let opt, d = 
	    (Pair_Set.fold
	       (fun pair res ->
		 let popt, dmin = res in
		 let d = 
		   let a0,a1 = pair in 
		   Vec3.distance a0.spos a1.spos
		 in
		 if d < dmin then
		   (Some pair), d
		 else
		   res
	       )
	       group
	       (None, infinity)
	    )
	  in
	  some_of opt
	in
	
	let a0c, a1c = closest_pair in 

	let exc_pairs = 
	  Pair_Set.fold
	    (fun pair res ->
	      let a0,a1 = pair in
	      if a0 == a0c then
		if not (a1 == a1c) then
		  pair :: res
		else
		  res
	      else if a1 == a1c then
		pair :: res
	      else
		res
	    )
	    group
	    []
	in
	List.rev_append exc_pairs res
      )
      groups
      []
  in
  list_diff pairs exc_pairs
;;

let final_atom_pairs = 
  if non_redundant then
    let rec loop pairs = 
      let groups = connected_groups pairs in  
      if not (Group_Set.is_empty groups) then
	let new_pairs = stripped_pairs pairs groups in 
	loop new_pairs
      else
	pairs
    in
    loop atom_pairs
  else
    atom_pairs
;;

pr "<rxn_pairs>\n";;
List.iter
  (fun pair ->
    let atom0, atom1 = pair in
      pr "  <pair> %d %d </pair>\n" atom0.number atom1.number
  )
  final_atom_pairs
;;
pr "</rxn_pairs>\n";;
