(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Used internally. Solves a tridiagonal system. The matrix is
contained in the arrays "a", "b", and "c" as shown below, 
the rhs is in "d", and the answer is placed in "x".
Arrays "b", "c", and "d" are destroyed, and a.(0) is unused.
 
b0 c0             x0     d0
a1 b1 c1          x1     d1
   a2 b2 c2       x2     d2
      ....
       an-1 bn-1  xn-1   dn-1
*)
 
let solve ~a ~b ~c ~d ~x =
  let n = Array.length x in
    
    c.(0) <- c.(0)/.b.(0);                          
    d.(0) <- d.(0)/.b.(0);

    for i = 1 to n-1 do
      let id = 1.0/.(b.(i) -. c.(i - 1)*.a.(i)) in
        c.(i) <- c.(i)*.id;                         
        d.(i) <- (d.(i) -. a.(i)*.d.(i - 1))*.id;
    done;

    x.(n - 1) <- d.(n - 1);
    for i = n-2 downto 0 do
      x.(i) <- d.(i) -. c.(i)*.x.(i + 1)
    done
;;

let multiply a b c x y = 
  let n = Array.length x in
    y.(0) <- b.(0)*.x.(0) +. c.(0)*.x.(1);

    for i = 1 to n-2 do
      y.(i) <- a.(i)*.x.(i-1) +. b.(i)*.x.(i) +. c.(i)*.x.(i+1);
    done;

    y.(n-1) <- a.(n-1)*.x.(n-2) +. b.(n-1)*.x.(n-1)
;;
    

(**********************************************************************)
(* test code *)
(*
let n = 20;;

let copy ar = 
  Array.init (Array.length ar) (fun i -> ar.(i))
;;

let b = Array.init n (fun i -> 2.0);;
let a = Array.init n (fun i -> -1.0);;
let c = Array.init n (fun i -> -1.0);;
c.(0) <- 0.0;;
a.(n-1) <- 0.0;;

let bb = copy b;;
let aa = copy a;;
let cc = copy c;;

let d = Array.init n (fun i -> 0.0);;
d.(n/4) <- 1.0;;
d.(3*n/4) <- 1.0;;

let dd = copy d;;

let x = Array.init n (fun i -> 0.0);;

solve a b c d x;;

for i = 0 to n-1 do
  Printf.printf "%d %g\n" i x.(i);
done
;;

let ddd = Array.init n (fun i -> 0.0);;

multiply aa bb cc x ddd;;

Printf.printf "##################################\n";
for i = 0 to n-1 do
  Printf.printf "%d %g %g\n" i dd.(i) ddd.(i);
done
;;

*)

