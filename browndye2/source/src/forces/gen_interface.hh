#pragma once

#include "../molsystem/system_state.hh"
#include "other/mm_nonbonded_parameter_info.hh"
#include "parameters.hh"
#include "share_tet.h"

// Cases:
// core0 - core
// core0 - chain, attached
// core0 - chain, unattached
// core - core
// core - chain, attached
// core - chain, unattached

namespace Browndye{
  typedef Vector< Vector< Atom_Index>, Atom_Index> Chain_Common::* Excl_Ptr;
  typedef typename Chain_Common::Ex_Cor_Map Chain_Common::* Ex_Cor_Map_Ptr;

  //##################################################################################################
  template< class I, template< class> class Outer_Loop, template< class> class Inner_Loop,
          template <class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  class Pair_Applier_Gen{
  public:
    typedef typename I::System System;
    static void doit( System&);

  private:
    static void do_core_core( System& sys);
    static void do_core_chain( System& sys);
    static void do_chain_chain( System& sys);

  };

  //#######################################################################################
  class Atom_Looper_Base{
  public:
    F3 forcei = F3( Zeroi{});
    T3 torquei = T3( Zeroi{});
    Pos ceni;
    const std::map< Atom_Index, Vector< Atom_Index> >  *core_excluded = nullptr, *core_one_four = nullptr;
    const Vector< Vector< Atom_Index>, Atom_Index> *excluded = nullptr, *one_four = nullptr;
  };

  // get fed to collision processor
  template< class Extype, class OFType, class I, class Ia, class Ib,
              template< class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  class Gen_Looper: public Atom_Looper_Base{
  public:
    static constexpr bool hexcl = std::is_same_v< Extype, std::true_type>;
    static constexpr bool hofcl = std::is_same_v< OFType, std::true_type>;
    typedef typename I::System System;
    static constexpr bool is_stat0 = Ia::is_stationary;

    typedef typename Ia::Transform TformA;
    typedef typename Ib::Transform TformB;
    typedef typename Ia::Container ContA;
    typedef typename Ib::Container ContB;
    typedef typename Ia::Refs RefsA;
    typedef typename Ib::Refs RefsB;

    typename I::System& sys;
    // for case of Core-Chain
    Group_Index ig{ maxi};
    Core_Index im{ maxi};

    explicit Gen_Looper( typename I::System& _sys): sys(_sys){}

    void operator()(
            const TformA &tformi, ContA &coni, RefsA &refsi, Pos, Length,
            const TformB &tformj, ContB &conj, RefsB &refsj, Pos, Length);
  };

  //#######################################################################################
  // Extype, if true, says that there are excluded atoms
  template< class Extype, class OFType, class I, class Ia, class Ib, template< class, class> class F,
            Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  void Gen_Looper< Extype, OFType, I, Ia, Ib, F, excl_ptr, ex_cor_map_ptr>::operator()(
          const TformA &tformi, ContA &coni, RefsA &refsi, Pos posi, Length radi,
          const TformB &tformj, ContB &conj, RefsB &refsj, Pos posj, Length radj) {

    auto of_factor = sys.one_four_factor();
    typename Ia::Subsystem loopa( sys, tformi, coni, refsi, posi, radi);
    for (auto ia: refsi){
      typename Ib::Subsystem loopb( sys, tformj, conj, refsj, posj, radj, loopa, ia);

      if constexpr (hexcl) { // core-chain, same group
        auto &catoms = conj.catoms;
        if constexpr (!Ia::is_chain) {
          //auto inum = Ia::atom_number( coni, ia);
          if (core_excluded) {
            auto itr = core_excluded->find(ia);
            if (itr != core_excluded->end()) {
              auto &excl = itr->second;
              for (auto ib: excl) {
                catoms[ib].estatus = Exclusion_Status::Excluded;
              }
            }
            if constexpr (hofcl) {
              if (core_one_four) {
                auto itrf = core_one_four->find(ia);
                if (itrf != core_one_four->end()) {
                  auto &ofr = itrf->second;
                  for (auto ib: ofr) {
                    catoms[ib].estatus = Exclusion_Status::One_Four;
                  }
                }
              }
            }
          }
        }
        else { // intra-chain
          if (excluded) {
            auto &excl = (*excluded)[ia];
            for (auto ib: excl) {
              catoms[ib].estatus = Exclusion_Status::Excluded;
            }
          }
          if constexpr (hofcl) {
            if (core_one_four) {
              auto &excl = (*one_four)[ia];
              for (auto ib: excl) {
                catoms[ib].estatus = Exclusion_Status::One_Four;
              }
            }
          }
        }
      }

      auto inner_loop_innards = [&]( auto ib){

        auto doinner = [&](double factor) {
          F<Ia, Ib>::doit( sys, loopa, loopb, ia, ib, factor);
        };

        if constexpr (hexcl) {
          auto estatus = conj.catoms[ib].estatus;
          if (estatus == Exclusion_Status::None) {
            doinner(1.0);
          } else if (estatus == Exclusion_Status::One_Four) {
            doinner(of_factor);
          }
        } else {
          doinner(1.0);
        }
      };

      if constexpr (Ia::is_chain){
        // intra-chain
        if (&conj == &coni){
          for( auto ib: refsj){
            if (ib >= ia) {
              break;
            }
            inner_loop_innards( ib);
          }
        }
        else{
          for( auto ib: refsj){
            inner_loop_innards( ib);
          }
        }
      }
      else{
        for( auto ib: refsj){
          inner_loop_innards( ib);
        }
      }

      if constexpr (hexcl) {
        for (auto &atom: conj.catoms) {
          atom.estatus = Exclusion_Status::None;
        }
      }

      if constexpr (!Ia::is_chain) {
        loopa.force += loopb.force;
        loopa.torque += cross( loopb.posa - ceni, loopb.force);
      }
    }
    if constexpr (!Ia::is_chain) {
      forcei += loopa.force;
      torquei += loopa.torque;
    }
  }

  //#######################################################################################
  // core-core
  template< class I, template< class> class OLoop, template< class> class ILoop,
          template <class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  void Pair_Applier_Gen<  I, OLoop, ILoop, F, excl_ptr, ex_cor_map_ptr>::do_core_core( System &sys) {

    auto &state = sys.state;
    auto &groups = state.groups;
    auto ng = groups.size();
    for (auto ig: range(ng)) {
      auto &groupi = groups[ig];
      auto &coresi = groupi.core_states;
      auto nmi = coresi.size();

      for (auto jg: range(ig, ng)) {
        auto &groupj = groups[jg];
        auto &coresj = groupj.core_states;
        auto nmj = coresj.size();
        for (auto im: range(nmi)) {
          auto bj = (ig == jg) ? inced(im) : Core_Index(0);
          auto &corei = coresi[im];
          auto &tformi = corei.transform();
          typedef Collision_Detector::Structure<Small_Col_Interface> Col;
          auto &mthreadi = corei.thread();
          Col &coli = *(mthreadi.collision_structure);
          auto ceni = tformi.translation();

          auto do_inner_loop = [&](auto ifacei, auto &coli, const auto &tformi) {
            typedef decltype(ifacei) Ifacei;

            for (auto jm: range(bj, nmj)) {
              auto &corej = coresj[jm];
              if (!share_tet(corei, corej)) {
                auto &tformj = corej.transform();
                auto &mthreadj = corej.thread();
                Col &colj = *(mthreadj.collision_structure);

                Gen_Looper<std::false_type, std::false_type, I, OLoop<Ifacei>, ILoop<Small_Col_Interface>, F, excl_ptr, ex_cor_map_ptr> aloopr( sys);
                aloopr.ceni = ceni;

                Collision_Detector::compute_interaction( aloopr, coli, tformi, colj, tformj);
                auto forcei = aloopr.forcei;
                auto torquei = aloopr.torquei;

                corei.force += forcei;
                corei.torque += torquei;

                auto cenj = tformj.translation();
                corej.force -= forcei;
                corej.torque += cross( cenj - ceni, forcei) - torquei;
              }
            }
          };

          if (im == Core_Index(0) && ig == Group_Index(0)) {
            auto &lcoli = *(corei.common().collision_structure);
            do_inner_loop( Large_Col_Interface<false>{}, lcoli, Blank_Transform{});
          } else {
            do_inner_loop( Small_Col_Interface{}, coli, tformi);
          }
        }
      }
    }
  }

  //###############################################################################################
  // core-chain
  template< class I, template< class> class OLoop, template< class> class ILoop,
          template <class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  void Pair_Applier_Gen<  I, OLoop, ILoop, F, excl_ptr, ex_cor_map_ptr>::do_core_chain( System& sys){

    auto &state = sys.state;
    auto &groups = state.groups;
    auto ng = groups.size();

    for (auto ig: range(ng)) {
      auto &groupi = groups[ig];
      auto &coresi = groupi.core_states;
      auto nmi = coresi.size();

      for (auto jg: range(ng)) {
        auto &groupj = groups[jg];
        auto &chainsj = groupj.chain_states;
        auto ncj = chainsj.size();
        for (auto im: range(nmi)) {
          auto &corei = coresi[im];
          auto &tformi = corei.transform();
          typedef Collision_Detector::Structure<Small_Col_Interface> Col;
          auto &mthreadi = corei.thread();
          Col &coli = *(mthreadi.collision_structure);
          auto ceni = tformi.translation();

          auto do_inner_loop = [&](auto ifacei, auto &coli, const auto &tformi) {

            for (auto jc: range(ncj)) {
              auto &chainj = chainsj[jc];

              if (!share_tet(chainj, corei)) {
                auto &ccom = chainj.common();

                const std::map<Atom_Index, Vector<Atom_Index> > *core_excluded = nullptr, *core_one_four = nullptr;
                {
                  auto &excm = ccom.*ex_cor_map_ptr;
                  auto itr = excm.find(im);
                  if (itr != excm.end()) {
                    core_excluded = &(itr->second);
                  }
                }

                {
                  auto itr = ccom.one_four_core_atoms.find(im);
                  if (itr != ccom.one_four_core_atoms.end()) {
                    core_one_four = &(itr->second);
                  }
                }

                auto do_looper = [&](auto exclass, auto ofclass) {
                  typedef decltype(ifacei) Ifacei;
                  typedef decltype(exclass) Exclass;
                  typedef decltype(ofclass) OFclass;

                  Gen_Looper<Exclass, OFclass, I, OLoop<Ifacei>, ILoop<Chain_Col_Interface>, F, excl_ptr, ex_cor_map_ptr> aloopr( sys);
                  aloopr.ceni = ceni;
                  aloopr.ig = ig;
                  aloopr.im = im;
                  aloopr.core_excluded = core_excluded;
                  aloopr.core_one_four = core_one_four;

                  auto &colj = chainj.collision_info->col_struct;

                  Collision_Detector::compute_interaction( aloopr, coli, tformi, colj, Blank_Transform{});
                  auto forcei = aloopr.forcei;
                  auto torquei = aloopr.torquei;

                  corei.force += forcei;
                  corei.torque += torquei;
                };

                if (core_excluded) {
                  if (core_one_four) {
                    do_looper(std::true_type{}, std::true_type{});
                  } else {
                    do_looper(std::true_type{}, std::false_type{});
                  }
                }
                else {
                  if (core_one_four) {
                    do_looper(std::false_type{}, std::true_type{});
                  }
                  else {
                    do_looper(std::false_type{}, std::false_type{});
                  }
                }
              }
            }
          }; // end do_inner_loop

          if (ig == Group_Index{0} && im == Core_Index{0}) {
            typedef Collision_Detector::Structure<Large_Col_Interface<false> > LCol;
            LCol &lcoli = *(corei.common().collision_structure);
            do_inner_loop(Large_Col_Interface<false>{}, lcoli, Blank_Transform{});
          } else {
            do_inner_loop(Small_Col_Interface{}, coli, tformi);
          }
        }
      }
    }
  }

  //###################################################################################################
  template< class I, template< class> class OLoop, template< class> class ILoop,
          template <class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  void Pair_Applier_Gen<  I, OLoop, ILoop, F, excl_ptr, ex_cor_map_ptr>::do_chain_chain( System& sys){

    auto &state = sys.state;
    auto &groups = state.groups;
    auto ng = groups.size();

    for (auto ig: range(ng)) {
      auto &groupi = groups[ig];
      auto &chainsi = groupi.chain_states;
      auto nci = chainsi.size();

      for (auto jg: range(inced(ig))) {
        auto &groupj = groups[jg];
        auto &chainsj = groupj.chain_states;
        auto ncj = chainsj.size();

        for (auto ic: range(nci)) {
          Chain_State &chaini = chainsi[ic];

          auto &coli = chaini.collision_info->col_struct;
          auto jce = (jg == ig) ? inced(ic) : ncj;

          for (auto jc: range(jce)) {
            auto &chainj = chainsj[jc];
            if ((jg == ig && jc == ic && !chaini.frozen)) { // intra-chain
              Gen_Looper<std::true_type, std::true_type, I, OLoop<Chain_Col_Interface>, ILoop<Chain_Col_Interface>, F, excl_ptr, ex_cor_map_ptr> aloopr(
                      sys);
              auto &com = chaini.common();
              aloopr.excluded = &(com.*excl_ptr);
              Collision_Detector::compute_interaction(aloopr, coli, Blank_Transform{}, coli, Blank_Transform{});
            } else {
              if (!share_tet(chaini, chainj)) {
                Gen_Looper<std::false_type, std::false_type, I, OLoop<Chain_Col_Interface>, ILoop<Chain_Col_Interface>, F, excl_ptr, ex_cor_map_ptr> aloopr(
                        sys);
                auto &colj = chainj.collision_info->col_struct;
                Collision_Detector::compute_interaction(aloopr, coli, Blank_Transform{}, colj, Blank_Transform{});
              }
            }
          }
        }
      }
    }
  }

  //#############################################################################################
  template< class I, template< class> class OLoop, template< class> class ILoop,
              template <class, class> class F, Excl_Ptr excl_ptr, Ex_Cor_Map_Ptr ex_cor_map_ptr>
  void Pair_Applier_Gen< I, OLoop, ILoop, F, excl_ptr, ex_cor_map_ptr>::doit( System& sys) {

    do_core_core(sys);
    auto &state = sys.state;
    auto &groups = state.groups;
    for (auto &group: groups) {
      for (auto &chain: group.chain_states) {
        if (!chain.atoms.empty()) {
          chain.collision_info = std::make_unique<typename Chain_State::Chain_Collision_Info>(chain);
        }
      }
    }

    do_core_chain(sys);
    do_chain_chain( sys);
  }
}
