#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Implements force computation on large groups of point charges using
the Chebyshev interpolation method described in the paper.  The blob is assumed to
be transformed from its home position by the transform, and the torque is taken
about the origin of the blob (the origin of the blob is (0,0,0) transformed to its new position)

Classes:

// contains all the information for a charged molecule
// order is the maximum order of Chebyshev interpolation
template< size_t order, class Interface>
class Blob;

// simple charged point
template< class Charge>
class Point;

// base class of boxes
template< class Charge>
class Box;

// contains Chebyshev coefficients and references to child boxes
template< size_t order, class Charge>
struct Coef_Box: public Box< Charge>;

// contains point charges at bottom
template< class Charge>
class Point_Box: public Box< Charge>;

Interface class (template argument to Blob) has the following members:

  // assume that Charge and Potential are derived from the units in units.hh
  typedef Charge;
  typedef Potential;

  static 
  void transform( const Transform&, const Vec3< Length>&, Vec3< Length>&);

  template< class U>
  static 
  void rotate( const Transform&, const Vec3< U>&, Vec3< double>&);

  // Length from units.hh
  static
  Potential value( const Field& field, const Vec3< Length>& pos);

  // typedef UQuot< Potential, Length>::Res Potential_Gradient;
  // where UQuot is defined in units.hh
  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad);

  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos);

  // As many classes of Transform and Field may defined as needed
};
*/


#include <cstdlib>
#include <cmath>
#include "../lib/vector.hh"
#include <limits>
#include <string>
#include "../xml/jam_xml_pull_parser.hh"
#include "../lib/units.hh"
#include "../xml/node_info.hh"
#include "../lib/array.hh"
#include "../lib/sq.hh"



namespace Browndye::Charged_Blob{

using std::unique_ptr;
using std::make_unique;
using std::string;
namespace JP = JAM_XML_Pull_Parser;

template< class Charge>
class Box;

template< size_t order, class Charge>
struct Coef_Box;

template< class Charge>
class Point_Box;

template< class T, size_t nx, size_t ny, size_t nz> using SArr = 
  Array< Array< Array< T, nz>, ny>, nx>;

  // simple charged point
template< class Charge>
class Point{
public:
  Point();
  Point( const Point&);
  Point& operator=( const Point&);

  Vec3< Length> pos;
  Charge q;

private:
  void copy( const Point< Charge>&);

};

// contains all the information for a charged molecule
template< size_t order, class Interface>
class Blob{
public:

  typedef typename Interface::Charge Charge;
  typedef typename Interface::Potential Potential;
  // require that Force = Charge * Potential / Length, or
  // Energy = Charge*Potential

  typedef decltype( Potential()*Charge()) Energy;
  typedef decltype( Potential()/Length()) Potential_Gradient;

  Blob( const string& filename, Vec3< Length> offset);

  template< class Transform, class Field>
  void get_force_and_torque( const Transform& tform, 
                             const Field& field,
                             Vec3< Force>& force, 
                             Vec3< Torque>& torque) const;

    [[maybe_unused]] void set_distance_threshhold( double); // default 2


  Charge total_charge() const;

  template< class Transform, class Field>
  [[maybe_unused]]
  Energy potential_energy( const Transform& tform, 
                           const Field& field) const;

  template< class Transform, class Field>
  [[maybe_unused]] Energy potential_energy_brute( const Transform& tform,
                                 const Field& field) const;

  [[maybe_unused]]
  void get_charges( Vector< Point< Charge> >& points) const{
    top_box->get_box_charges( points);
  }

private:
  double distance_threshhold;
  unique_ptr< Box< Charge> > top_box;
  unique_ptr< Coef_Box< order-1, Charge> > top_box3;
  Charge charge;

  void initialize( const string& filename);
  void shift_position( const Vec3< Length>&);

  template< class Transform, class Field>
  void add_force_and_torque_on_box(
                                   const Box< Charge>& box,
                                   const Transform& tform,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  template< size_t cbox_order, class Transform, class Field>
  void add_force_and_torque_on_cbox(
                                   const Coef_Box< cbox_order, Charge>& cbox,
                                   const Transform& tform,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  template< class Transform, class Field>
  void add_force_and_torque_on_pbox(
                                   const Point_Box< Charge>& pbox,
                                   const Transform& tform,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  template< class Transform, class Field>
  Energy potential_energy_of_box(
                                 const Box< Charge>& box,
                                 const Transform& tform,
                                 const Field& field) const;
  
  template< class Transform, class Field>
  Energy potential_energy_brute_of_box(
                                       const Box< Charge>& box,
                                       const Transform& tform,
                                       const Field& field) const;
  
  template< size_t cbox_order, class Transform, class Field>
  Energy potential_energy_of_cbox(
                                  const Coef_Box< cbox_order, Charge>& cbox,
                                  const Transform& tform,
                                  const Field& field) const;

  template< size_t cbox_order, class Transform, class Field>
  Energy potential_energy_brute_of_cbox(
                                  const Coef_Box< cbox_order, Charge>& cbox,
                                  const Transform& tform,
                                  const Field& field) const;
  
  template< class Transform, class Field>
  Energy potential_energy_of_pbox(
                                  const Point_Box< Charge>& pbox,
                                  const Transform& tform,
                                  const Field& field) const;



  //*************************************************
  template< class Transform>
  static 
  void transform( const Transform& tform, const Vec3< Length>& in, Vec3< Length>& out){
    Interface::transform( tform, in, out);
  }
  
  template< class U, class Transform>
  static 
  void rotate( const Transform& trans, const Vec3< U>& in, Vec3< U>& out){
    Interface::rotate( trans, in, out);
  }
  
  template< class Field>
  static
  Potential value( const Field& field, const Vec3< Length>& pos){
    return Interface::value( field, pos);
  }
  
  template< class Field>
  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad){
    Interface::get_gradient( field, pos, grad);
  }
  
  template< class Field>
  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos){
    return Interface::distance_less_than( field, r, pos);
  }
};

template< size_t order, class Interface>
void Blob< order, Interface>::shift_position( const Vec3< Length>& center){

  top_box->shift_position( center);
  if (top_box3)
    top_box3->shift_position( center);
}

  // constructor
template< size_t order, class Interface>
Blob< order, Interface>::Blob( const string& filename, Vec3< Length> offset){
  top_box = nullptr;
  top_box3 = nullptr;
  distance_threshhold = 2.0;
  charge = Charge( 0.0);
  
  initialize( filename);
  shift_position( offset);
}
 
template< size_t order, class Interface>
[[maybe_unused]] void Blob< order, Interface>::set_distance_threshhold( double th){
  distance_threshhold = th;
}

template< size_t order, class Interface>
template< class Transform, class Field>
void Blob< order, Interface>::
get_force_and_torque(
                     const Transform& tform,
                     const Field& field,
                     Vec3< Force>& force, Vec3< Torque>& torque) const{
  set_zero( force);
  set_zero( torque);
  
  if (top_box->is_coef_box()){
    //  static_cast< unique_ptr< Coef_Box< order, Charge> > >( top_box);
    auto cbox = static_cast< const Coef_Box< order, Charge>*>(top_box.get());
    
    const double th = distance_threshhold;
    const Length d = cbox->diameter;
    
    Vec3< Length> c;
    transform( tform, top_box3->center, c);
    
    if (distance_less_than( field, th*d, c)){
      add_force_and_torque_on_box( *top_box, tform, field, force, torque);
    }
    else{
      add_force_and_torque_on_cbox( *top_box3, tform, field, force, torque);
    }
  }
  else{
    auto pbox = static_cast< const Point_Box< Charge>* >( top_box.get());
    add_force_and_torque_on_pbox( *pbox, tform, field, force, torque);
  }
}

template< size_t order, class Interface>
template< class Transform, class Field>
[[maybe_unused]]
auto Blob< order, Interface>::
potential_energy( const Transform& tform, const Field& field) const -> Energy{
  
  if (top_box->is_coef_box()){
    const auto* cbox =
      static_cast< const Coef_Box< order, Charge>* >( top_box);
   
    const double th = distance_threshhold;
    const Length d = cbox->diameter;
    
    Vec3< Length> c;
    transform( tform, top_box3->center, c);
    
    if (distance_less_than( field, th*d, c)){
      return potential_energy_of_box( *top_box, tform, field);
    }
    else{
      return potential_energy_of_cbox( *top_box3, tform, field);
    }
  }
  else{
    const auto* pbox =
      static_cast< const Point_Box< Charge>* >( top_box);

    return potential_energy_of_pbox( *pbox, tform, field);
  }
}

template< size_t order, class Interface>
template< class Transform, class Field>
[[maybe_unused]]
auto Blob< order, Interface>::
potential_energy_brute(
                 const Transform& tform,
                 const Field& field) const -> Energy{
  
  if (top_box->is_coef_box()){
    return potential_energy_brute_of_box( *top_box, tform, field);
  }
  else{
    const auto* pbox =
      static_cast< const Point_Box< Charge>* >( top_box);

    return potential_energy_of_pbox( *pbox, tform, field);
  }
}

template< class Charge>
inline
void Point< Charge>::copy( const Point& pt){
  pos[0] = pt.pos[0];
  pos[1] = pt.pos[1];
  pos[2] = pt.pos[2];
  q = pt.q;
}

template< class Charge>
inline 
Point< Charge>::Point( const Point& pt){
  copy( pt);
}

template< class Charge>
inline
Point< Charge>& Point< Charge>::operator=( const Point& pt){
  copy( pt);
  return *this;
}

template< class Charge>
inline
Point< Charge>::Point(){
  q = Charge( NAN);
}

template< class U>
inline
void put_nan( U& u){
  set_fvalue( u, NaN);
}

template< class U>
inline
void put_nan( Vec3< U>& v){
  put_nan( v[0]);
  put_nan( v[1]);
  put_nan( v[2]);
}

template< class U, size_t n>
inline
void fill_with_nans( Array< U,n>& v){
  for( size_t i = 0; i<n; i++)
    put_nan( v[i]);
}

template< class U, size_t nr, size_t nc>
inline
void fill_with_nans( SMat< U,nr,nc>& v){
  for( size_t i = 0; i<nr; i++)
    for( size_t j = 0; j<nc; j++)
      put_nan( v[i][j]);
}

template< class U, size_t nr, size_t nc, size_t nz>
inline
void fill_with_nans( SArr< U,nr,nc,nz>& v){
  for( size_t i = 0; i<nr; i++)
    for( size_t j = 0; j<nc; j++)
      for( size_t k = 0; k<nz; k++)
        put_nan( v[i][j][k]);
}

// base class of boxes
template< class Charge>
class Box{
public:
  virtual ~Box()= default;
  [[nodiscard]] virtual bool is_coef_box() const = 0;
  virtual void shift_position( const Vec3< Length>& pos) = 0;
  virtual Charge total_charge() const = 0;
  virtual void get_box_charges( Vector< Point< Charge> >&) const = 0;
};

// contains Chebyshev coefficients and references to child boxes
template< size_t order, class Charge>
struct Coef_Box: public Box< Charge>{
public: 
  Coef_Box();
  ~Coef_Box()= default;

  [[nodiscard]] bool is_coef_box() const override { return true;}
  Charge total_charge() const override;

  typedef decltype( Charge()/Length()) FType;

  // coefficients for energy, force,and torque
  SArr< Charge, order,order,order> vcoefs;
  SArr< Vec3< FType>, order,order,order> fcoefs;
  SArr< Vec3< Charge>, order,order,order> tcoefs;
  // locations of interpolation points
  SMat< Length, 3, order> zeros;

  unique_ptr< Box< Charge> > child0;
  unique_ptr< Box< Charge> > child1;

  Vec3< Length> center;
  Length diameter;
  Charge charge;

  void shift_position( const Vec3< Length>& offset) override;
  void get_box_charges( Vector< Point< Charge> >&) const override;
};

template< size_t order, class Charge>
void Coef_Box< order, Charge>::get_box_charges( Vector< Point< Charge> >& points) const{
  child0->get_box_charges( points);
  child1->get_box_charges( points);
}

template< size_t order, class Charge>
inline
Charge Coef_Box< order, Charge>::total_charge() const{
  return charge;
}

template< size_t order, class Charge>
void Coef_Box< order, Charge>::shift_position( const Vec3< Length>& offset){
    
  for( size_t i = 0; i<3; i++)
    for( size_t k = 0; k < order; k++)
      zeros[i][k] += offset[i];

  for( size_t ix = 0; ix < order; ix++)
    for( size_t iy = 0; iy < order; iy++)
      for( size_t iz = 0; iz < order; iz++){
        auto& tcoef = tcoefs[ix][iy][iz];
        auto& fcoef = fcoefs[ix][iy][iz];
        tcoef = tcoef + cross( offset, fcoef);
      }

  if (child0)
    child0->shift_position( offset);
  if (child1)
    child1->shift_position( offset);

  center = center + offset;
}

  // constructor
template< size_t order, class Charge>
Coef_Box< order, Charge>::Coef_Box(){
  fill_with_nans( center);
  diameter = Length( NAN);

  child0 = nullptr;
  child1 = nullptr;
  fill_with_nans( fcoefs);
  fill_with_nans( tcoefs);
  fill_with_nans( zeros);
  charge = Charge( NAN);
}
 
// contains point charges at bottom
template< class Charge>
class Point_Box: public Box< Charge>{
public:
  ~Point_Box()= default;
  [[nodiscard]] bool is_coef_box() const override { return false;}
  void shift_position( const Vec3< Length>&) override;
  Charge total_charge() const override;
  //typedef typename Vector< Point< Charge> >::size_type size_type;

  void get_box_charges( Vector< Point< Charge> >&) const override;
  
  Vector< Point< Charge> > points;
};

template< class Charge>
void Point_Box< Charge>::get_box_charges( Vector< Point< Charge> >& opoints) const{

  for( auto& point: points){
    opoints.push_back( point);
  }
}

template< class Charge>
Charge Point_Box< Charge>::total_charge() const{
  Charge sum( 0.0);
  for( auto& point: points) {
    //for( size_type i = 0; i < points.size(); i++)
    sum += point.q;
  }
  return sum;
}

template< class Charge>
void Point_Box< Charge>::shift_position( const Vec3< Length>& offset){
  for( auto& point: points) {
    //for( size_type i = 0; i < points.size(); i++){
    //Point< Charge>& point = points[i];
    point.pos = point.pos + offset;
  }
}

// add force and torque from "cbox" due to "field"
template< size_t order, class I, class Transform, class Field>
void add_ft_of_coef_box( const Coef_Box< order, typename I::Charge>& cbox,
                         const Transform& tform,
                         const Field& field,
                         Vec3< Force>& force, Vec3< Torque>& torque){
  
  typedef typename I::Charge Charge;
  typedef typename I::Potential Potential;
  typedef decltype( Charge()/Length()) FType;
  typedef Charge TType;

  Vec3< Length> pos, pos0;
  Vec3< Force> force0, force1;
  Vec3< Torque> torque0, torque1;
  set_zero( force0);
  set_zero( torque0);

  typedef SArr< Vec3< FType>, order,order,order> FCoefs;
  typedef SArr< Vec3< TType>, order,order,order> TCoefs;
  
  const FCoefs& fcoefs = cbox.fcoefs;
  const TCoefs& tcoefs = cbox.tcoefs;
  for( size_t ix = 0; ix < order; ix++){
    const auto& fcoefsx = fcoefs[ix];
    const auto& tcoefsx = tcoefs[ix];
    pos0[0] = cbox.zeros[0][ix];
    
    for( size_t iy = 0; iy < order; iy++){
      const auto& fcoefsxy = fcoefsx[iy];
      const auto& tcoefsxy = tcoefsx[iy];
      pos0[1] = cbox.zeros[1][iy];
      
      for( size_t iz = 0; iz < order; iz++){
        const Vec3< FType>& fcoefsxyz = fcoefsxy[iz]; 
        const Vec3< TType>& tcoefsxyz = tcoefsxy[iz]; 
        pos0[2] = cbox.zeros[2][iz];
        
        I::transform( tform, pos0, pos);
        Potential val = I::value( field, pos);
        force0 += val*fcoefsxyz;
        torque0 += val*tcoefsxyz;
      }
    }
  }
  I::rotate( tform, force0, force1);
  I::rotate( tform, torque0, torque1);
  force += force1;
  torque += torque1;
}


template< size_t order, class I, class Transform, class Field>
auto potential_energy_of_coef_box( const Coef_Box< order, typename I::Charge>& cbox,
                                  const Field& field,
                                  const Transform& tform){
  
  typedef typename I::Charge Charge;
  typedef typename I::Potential Potential;
  typedef decltype( Charge()*Potential()) VType; 

  Vec3< Length> pos, pos0;

  typedef SArr< Charge, order,order,order> VCoefs;

  const VCoefs& vcoefs = cbox.vcoefs;
  VType epot( 0.0);
  for( size_t ix = 0; ix < order; ix++){
    typedef typename SMat< Charge, order, order>::Const_View VCoefsx;

    const VCoefsx vcoefsx = vcoefs[ix];
    pos0[0] = cbox.zeros[0][ix];
    
    for( size_t iy = 0; iy < order; iy++){
      typedef typename Array< Charge, order>::Const_View VCoefsy;

      const VCoefsy vcoefsxy = vcoefsx[iy];
      pos0[1] = cbox.zeros[1][iy];
      
      for( size_t iz = 0; iz < order; iz++){
        const Charge vcoefsxyz = vcoefsxy[iz]; 
        pos0[2] = cbox.zeros[2][iz];

        I::transform( tform, pos0, pos);
        Potential val = I::value( field, pos);
        
        epot += val*vcoefsxyz;
      }
    }
  }
  return epot;
}  

template< size_t blob_order, class I>
template< size_t order, class Transform, class Field>
void Blob< blob_order, I>::
add_force_and_torque_on_cbox( 
                             const Coef_Box< order, typename I::Charge>& cbox,
                             const Transform& tform,
                             const Field& field,
                             Vec3< Force>& force, Vec3< Torque>& torque) const{
  
  Length d = cbox.diameter*distance_threshhold;
  Vec3< Length> c;
  transform( tform, cbox.center, c);

  if (distance_less_than( field, d, c)){
    add_force_and_torque_on_box( *(cbox.child0), tform, field, 
                                 force, torque);
    add_force_and_torque_on_box( *(cbox.child1), tform, field, 
                                 force, torque);   
  } 
  else
    add_ft_of_coef_box< order, I>( cbox, tform, 
                                   field, force, torque);
}
  
template< size_t blob_order, class I>
template< size_t order, class Transform, class Field>
typename Blob< blob_order, I>::Energy
Blob< blob_order, I>::
potential_energy_of_cbox( 
                         const Coef_Box< order, typename I::Charge>& cbox,
                         const Transform& tform,
                         const Field& field
                         ) const{
  
  Length d = cbox.diameter*distance_threshhold;
  Vec3< Length> c;
  transform( tform, cbox.center, c);

  if (distance_less_than( field, d, c)){
    Energy v0 = potential_energy_of_box( *(cbox.child0), tform, field);
    Energy v1 = potential_energy_of_box( *(cbox.child1), tform, field);
    return v0 + v1;
  } 
  else
    return potential_energy_of_coef_box< order, I>( cbox, field, tform);
}

  // brute force method; just sums over all charges  
template< size_t blob_order, class I>
template< size_t order, class Transform, class Field>
typename Blob< blob_order, I>::Energy
Blob< blob_order, I>::
potential_energy_brute_of_cbox( 
                          const Coef_Box< order, typename I::Charge>& cbox,
                          const Transform& tform,
                          const Field& field
                          ) const{
  
  Vec3< Length> c;
  transform( tform, cbox.center, c);

  Energy v0 = potential_energy_brute_of_box( *(cbox.child0), tform, field);
  Energy v1 = potential_energy_brute_of_box( *(cbox.child1), tform, field);
  return v0 + v1; 
}
  
template< size_t order, class I>
template< class Transform, class Field>
void Blob< order, I>::
add_force_and_torque_on_pbox(
                             const Point_Box< typename I::Charge>& pbox,
                             const Transform& tform,
                             const Field& field,
                             Vec3< Force>& force, Vec3< Torque>& torque) const{
  
  Vec3< Length> origin0( Zeroi{}), origin;
  transform( tform, origin0, origin); 
  
  //typedef typename I::Charge Charge;
  const Vector< Point< Charge> >& points = pbox.points;
  for( auto& point: points){
    Vec3< Length> pos;
    transform( tform, point.pos, pos);

    Vec3< Potential_Gradient> g{};
    get_gradient( field, pos, g);

    Charge q = point.q;
    auto f = -q*g;
    force += f;
 
    auto rpos = pos - origin;
    auto t = cross( rpos, f);
    torque += t;
  }
}

template< size_t order, class I>
template< class Transform, class Field>
typename Blob< order, I>::Energy
Blob< order, I>::
potential_energy_of_pbox(
                         const Point_Box< typename I::Charge>& pbox,
                         const Transform& tform,
                         const Field& field
                         ) const{
  
  Vec3< Length> pos;
  
  //typedef typename I::Charge Charge;
  const Vector< Point< Charge> >& points = pbox.points;
  Energy energy( 0.0);
  for( auto& point: points){
  //for( typename Vector< Point< Charge> >::const_iterator itr = points.begin();
    //   itr != points.end(); ++itr){
    //const Point< Charge>& point = *itr;

    transform( tform, point.pos, pos);
    energy += point.q*value( field, pos);
  }
  return energy;
}

template< size_t order, class I>
template< class Transform, class Field>
void Blob< order, I>::
add_force_and_torque_on_box(
                            const Box< typename I::Charge>& box,
                            const Transform& tform,
                            const Field& field,
                            Vec3< Force>& force, Vec3< Torque>& torque) const{

  //typedef typename I::Charge Charge;

  if (debug){
    auto box_ptr = &box;
    if (!box_ptr){
      error( "add_force_and_torque_on_box: box does not exist");
    }
  }

  if (box.is_coef_box()){
    const auto& cbox =
      static_cast< const Coef_Box< order, Charge>& >( box);
    
    add_force_and_torque_on_cbox( cbox, tform, field, force, torque);

  }
  else{
    const auto& pbox =
      static_cast< const Point_Box< Charge>& >( box);

    add_force_and_torque_on_pbox( pbox, tform, field, force, torque);   
  }
}

template< size_t order, class Interface>
template< class Transform, class Field>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy_of_box(
                        const Box< typename Interface::Charge>& box,
                        const Transform& tform,
                        const Field& field) const{

  //typedef typename Interface::Charge Charge;

  if (box.is_coef_box()){
    const auto& cbox =
      static_cast< const Coef_Box< order, Charge>& >( box);

    return potential_energy_of_cbox( cbox, tform, field);

  }
  else{
    const auto& pbox =
      static_cast< const Point_Box< Charge>& >( box);

    return potential_energy_of_pbox( pbox, tform, field);   
  }
}

  // brute force approach by including every point charge
template< size_t order, class Interface>
template< class Transform, class Field>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy_brute_of_box(
                        const Box< typename Interface::Charge>& box,
                        const Transform& tform,
                        const Field& field) const{

  //typedef typename Interface::Charge Charge;

  if (box.is_coef_box()){
    const auto& cbox =
      static_cast< const Coef_Box< order, Charge>& >( box);

    return potential_energy_brute_of_cbox( cbox, tform, field);

  }
  else{
    const auto& pbox =
      static_cast< const Point_Box< Charge>& >( box);

    return potential_energy_of_pbox( pbox, tform, field);   
  }
}

// Parsing code
template< class U>
void read_v3( JP::Node_Ptr node, Vec3< U>& v){

  v[0] = checked_value_from_node< U>( node, "x");
  v[1] = checked_value_from_node< U>( node, "y");
  v[2] = checked_value_from_node< U>( node, "z");
}

template< class U>
void read_float( JP::Node_Ptr node, U& v){
  v = value_from_node< U>( node);
}

//##############################################################
template< size_t n, class T> 
void read_coefs(  void (read)( JP::Node_Ptr, T&), 
                  const JP::Node_Ptr& node, SArr< T,n,n,n>& coefs){

  //typedef std::list< JP::Node_Ptr> NList;

  auto nodes_x = node->children_of_tag( "xc");

  size_t ix = 0;
  for( auto& node_x: nodes_x){
  //for( auto itr = nodes_x.begin();
    //   itr != nodes_x.end();
      // ++itr){
    //auto node_x = *itr;

    auto nodes_y = node_x->children_of_tag( "yc");

    size_t iy = 0;
    for( auto& node_y: nodes_y){
    //for( NList::iterator itr = nodes_y.begin();
      //   itr != nodes_y.end();
        // ++itr){
      //auto node_y = *itr;
      auto nodes_z = node_y->children_of_tag( "zc");

      size_t iz = 0;
      for( auto& node_z: nodes_z){
      //for( NList::iterator itr = nodes_z.begin();
        //   itr != nodes_z.end();
          // ++itr){
        //auto node_z = *itr;
        read( node_z, coefs[ix][iy][iz]);
        ++iz;
      }
      ++iy;
    }
    ++ix;
  }  
}

template< class Charge>
void read_point( const JP::Node_Ptr& node, Point< Charge>& point);

template< size_t order, class Charge>
unique_ptr< Box< Charge> > new_box( JP::Parser& parser, JP::Node_Ptr);

template< class Charge>
unique_ptr< Point_Box< Charge> > new_point_box( const JP::Node_Ptr&);

//###################################################################
template< size_t order, class Charge>
void get_corners( JP::Node_Ptr& cnode, Coef_Box< order, Charge>& box){
 
  cnode->complete();
  JP::Node_Ptr lnode = checked_child( cnode, "low");
  Vec3< Length> lc;
  read_v3( lnode, lc);
  
  JP::Node_Ptr hnode = checked_child( cnode, "high");
  Vec3< Length> hc;
  read_v3( hnode, hc);
  
  box.diameter = sqrt( sq( hc[0] - lc[0]) +  
                        sq( hc[1] - lc[1]) +  
                        sq( hc[2] - lc[2]) 
                        );  
  box.center[0] = 0.5*( lc[0] + hc[0]);
  box.center[1] = 0.5*( lc[1] + hc[1]);
  box.center[2] = 0.5*( lc[2] + hc[2]);
}

//#################################################################
template< size_t order, class Charge>
void get_coefs( JP::Node_Ptr& cnode, Coef_Box< order, Charge>& box){
  
  cnode->complete();
  read_coefs( read_v3, checked_child( cnode, "force"), box.fcoefs);
  read_coefs( read_v3, checked_child( cnode, "torque"), box.tcoefs);
  read_coefs( read_float, 
              checked_child( cnode, "potential"), box.vcoefs);
}

//####################################################################
template< size_t order, class Charge>
void get_positions( JP::Node_Ptr& cnode, Coef_Box< order, Charge>& box){
  
  cnode->complete();
  auto rarray = [&]( auto& name, int i){
    box.zeros[i] = array_from_node< Length, order>( checked_child( cnode, name));
  };
  
  rarray( "xc", 0);
  rarray( "yc", 1);
  rarray( "zc", 2);
}

//####################################################################
template< size_t order, class Charge>
unique_ptr< Coef_Box< order, Charge> > new_coef_box( JP::Parser& parser,
                                                    JP::Node_Ptr node);

template< size_t order, class Charge>
void get_children( JP::Parser& parser, JP::Node_Ptr& cnode,
                    Coef_Box< order, Charge>& box){
  
  Array< string, 2> tags{ "cbox", "pbox"};
       
  size_t ic = 0;
  
  auto lcbox = [&]( JP::Node_Ptr& bnode){
    auto& cref = (ic == 0) ? box.child0 : box.child1;
    cref = new_coef_box< order, Charge>( parser, bnode);
    ++ic;
  };
  
  auto lpbox = [&]( JP::Node_Ptr& bnode){
    auto& cref = (ic == 0) ? box.child0 : box.child1;
    cref = new_point_box< Charge>( bnode);
    ++ic;
  };
  
  apply_to_each_child_with_tag( cnode, tags, lcbox, lpbox);
  
  if (!(ic == 2 || ic == 0)){
    cnode->perror( "need exactly 0 or 2 children");
  }
}

//###################################################################
template< size_t order, class Charge>
unique_ptr< Coef_Box< order, Charge> >
new_coef_box( JP::Parser& parser, JP::Node_Ptr node){

  auto box = make_unique< Coef_Box< order, Charge> >();

  Array< string,4> tags{ "corners", "coefs", "positions", "children"};

  bool got_corner = false;

  auto lcorner = [&]( JP::Node_Ptr& cnode){
    get_corners( cnode, *box);
    got_corner = true;
  };
  
  bool got_coefs = false;
  
  auto lcoefs = [&]( JP::Node_Ptr& cnode){
    get_coefs( cnode, *box);
    got_coefs = true;
  };
  
  bool got_positions = false;
  
  auto lpositions = [&]( JP::Node_Ptr& cnode){
    get_positions( cnode, *box);
    got_positions = true;
  };
  
  auto lchildren = [&]( JP::Node_Ptr& cnode){
    get_children( parser, cnode, *box);
  };
  
  apply_to_each_child_with_tag( node, tags, lcorner, lcoefs,
                               lpositions, lchildren); 
     
  if (!(got_coefs && got_corner && got_positions)){
    node->perror( "missing tags");
  }
  
  
  box->charge = Charge( 0.0);
  for (size_t ix = 0; ix < order; ix++){
    for (size_t iy = 0; iy < order; iy++){
      for (size_t iz = 0; iz < order; iz++){
        box->charge += box->vcoefs[ix][iy][iz];
  }}}
    
  return box;
}

//######################################################
template< size_t order, class Charge>
unique_ptr< Box< Charge> > new_box( JP::Parser& parser, JP::Node_Ptr node){

  unique_ptr< Box< Charge> > box;

  Array< string, 2> tags{ "cbox", "pbox"};
  int nb = 0;
  
  auto lcoef = [&]( JP::Node_Ptr& cnode){
    box = new_coef_box< order, Charge>( parser, cnode);
    ++nb;
  };
  
  auto lpoint = [&]( JP::Node_Ptr& cnode){
    box = new_point_box< Charge>( cnode);
    ++nb;
  };
  
  apply_to_each_child_with_tag( node, tags, lcoef, lpoint);
  
  if (nb != 1){
    node->perror( "need exactly one box");
  }
  
  return box;
}

//###############################################################
template< size_t order, class Interface>
typename Interface::Charge Blob< order, Interface>::total_charge() const{
  return charge;
}

template< size_t order, class Interface>
void Blob< order, Interface>::initialize( const string& filename){

  //typedef typename Interface::Charge Charge;
  std::ifstream input( filename);

  if (!input.is_open()){
    error( "charged blob file ", filename, " could not be opened");
  }
  JP::Parser parser( input, filename);
  auto top = parser.top();

  Array< std::string, 2> tags{"om1_box", "o_box"};

  auto lom1_box = [&]( JP::Node_Ptr& node){
    
    
    auto get_coef = [&]( JP::Node_Ptr& cnode){
      top_box3 = new_coef_box< order-1, Charge>( parser, cnode);
    };
    
    Array< std::string, 1> tag{"cbox"};
    apply_to_each_child_with_tag( node, tag, get_coef);
  };

  auto lbox = [&]( JP::Node_Ptr& node){
    top_box = new_box< order, Charge>( parser, node);
  };

  apply_to_each_child_with_tag( top, tags, lom1_box, lbox);
  charge = top_box->total_charge();
}

//###########################################################
template< class Charge>
void read_point( const JP::Node_Ptr& node, Point< Charge>& point){

  JP::Node_Ptr pnode = checked_child( node, "pos");
  read_v3( pnode, point.pos);

  JP::Node_Ptr qnode = checked_child( node, "q");
  point.q = value_from_node< Charge>( qnode);
}
 
//################################################################ 
template< class Charge>
unique_ptr< Point_Box< Charge> > new_point_box( const JP::Node_Ptr& node){
  
  auto box = make_unique< Point_Box< Charge> >();
  node->complete();

  auto ponode = checked_child( node, "points");
  
  auto np = checked_value_from_node< size_t>( ponode, "n");
  box->points.resize( np);

  auto poinodes = ponode->children_of_tag( "point");
  if (poinodes.size() != np)
    error( "new_points_box: not enough points", poinodes.size(), np);

  {
    size_t i = 0;
    for( auto& poinode: poinodes){
      read_point( poinode, box->points[i]);
      ++i;
    }
  }

  return box;  
}

}


