(* Reads a pqrxml file of spheres from standard input, and reads in another
pqrxml file containing a subset of the first file, and outputs the first
file with the spheres in the second file marked as "soft". This is used
when some of the spheres are to interact with a soft potential.

Takes the following argument with flag:

-soft : pqrxml file containing only spheres to be softened; needs only atom numbers

The other file comes through standard input.

Called by bd_top
*)


let soft_file_ref = ref "";;

Arg.parse 
  [("-soft", Arg.Set_string soft_file_ref, "pqrxml file containing only spheres to be softened; needs only atom numbers")]
  (fun arg -> raise (Failure "not anonymous arguments"))
  "Inputs pqrxml file of spheres and outputs the same with some of the spheres marked as \"soft\""
;;

let soft_file = !soft_file_ref;;

let soft_indices = 
  let table = Hashtbl.create 0 in
    if not (soft_file = "") then (
      let buf = Scanf.Scanning.from_file soft_file in	
	Atom_parser.apply_to_atoms ~buffer: buf
	~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	  ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	  Hashtbl.add table anumber true;
	);
    );
    table       
;;

let pr = Printf.printf;;

pr "<roottag>\n";;

let resi = ref (-1);;

Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin
  ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
    ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
    if not (rnumber = !resi)  then (
      if not (!resi = (-1)) then
	pr "  </residue>\n";
      
      pr "  <residue>\n";
      pr "    <residue_name> %s </residue_name>\n" rname;
      pr "    <residue_number> %d </residue_number>\n" rnumber;	  
      resi := rnumber;
    );
    
    pr "    <atom>\n";
    pr "      <atom_name>%s</atom_name>\n" aname;
    pr "      <atom_number>%d</atom_number>\n" anumber;
    pr "      <x>%f</x>\n" x;
    pr "      <y>%f</y>\n" y;
    pr "      <z>%f</z>\n" z;
    pr "      <charge>%f</charge>\n" q;
    pr "      <radius>%f</radius>\n" r;

    if Hashtbl.mem soft_indices anumber then 
      pr "      <soft> true </soft>\n";

    pr "    </atom>\n";
 
  )
;;

pr "  </residue>\n";;
pr "</roottag>\n";;

