#include "../xml/node_info.hh"
#include "spqrxml.hh"

namespace{
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
  using std::cout;
  using std::map;
  
  void echo( JP::Node_Ptr node, size_t ns, const string& tag){
    
    for( auto i: range(ns)){
      (void)i;
      cout << ' ';
    }
    
    auto text = checked_value_from_node< string>( node, tag);
    cout << "<" << tag << "> " << text << " </" << tag << ">\n";
  }
  
  //#######################################################################
  void output_residue( JP::Node_Ptr rnode, const string& final_rname, const map< string, string>& sub_table){
      
    cout << "  <residue>\n";
  
    cout << "    <name> " << final_rname << " </name>\n";
    echo( rnode, 4, "number");
     
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      cout << "    <atom>\n";
      auto faname = [&]{
        auto aname = checked_value_from_node< string>( anode, "name");
        auto itr = sub_table.find( aname);
        if (itr == sub_table.end()){
          return aname;
        }
        else{
          return itr->second;
        }
      }();
      
      cout << "      <name> " << faname << " </name>\n";
      echo( anode, 6, "number");
      
      echo( anode, 6, "x");
      echo( anode, 6, "y");
      echo( anode, 6, "z");
      
         
      echo( anode, 6, "charge");
      echo( anode, 6, "radius");
      
      cout << "    </atom>\n";
    }
    cout << "  </residue>\n";  
  }
   
  //#################################################################
  struct Residue_Info{
    string residue;
    Vector< Vector< std::pair< string, size_t> > > variants; 
  };
  
  string final_residue_name( const string& rname, const Vector< string>& atoms){
   
    auto is_in = [&]( const string& a){
      auto atend = atoms.end();
      auto itr = std::find( atoms.begin(), atend, a);
      return (itr != atend);
    }; 
     
    auto is_term = [&]{
      auto b = rname[0];
      return rname.size() == 4 && (b == 'N' || b == 'C');  
    }();
     
    auto res = is_term ? rname.substr( 1,3) : rname;
    
    if (res == "HIS"){
      auto hasD = is_in( "HD1") && is_in( "HD2");
      auto hasE = is_in( "HE1") && is_in( "HE2");
      
      if (hasD){
        if (hasE){
          res = "HIP";
        }
        else{
          res = "HID";
        }
      }
      else{
        if (hasE){
          res = "HIE";
        }
        else{
          error( "HIS has neither HD2 nor HE2");
        }
      }
    }
    else if (res == "HSD"){
      res = "HID";
    }
    else if (res == "HSE"){
      res = "HIE";
    }
    else if (res == "HI+" || res == "HSP"){
      res = "HIP";
    }
    else if (res == "GLU"){
      if (is_in("HE2") || is_in("HE1")){
        res = "GLH";
      }
    }
    else if (res == "GLUP" || res == "GL0"){
      res = "GLH";
    }
    else if (res == "ASP"){
      if (is_in("HD2") || is_in("HD1")){
        res = "ASH";
      }
    }
    else if (res == "ASPP" || res == "AS0"){
      res = "ASH";
    }
    else if (res == "CYS"){
      if (!(is_in("HG") || is_in("HG1"))){
        res = "CYM";
      }
    }
    else if (res == "CY-"){
      res = "CYM";
    } 
    
    // N-terminus
    if (is_in( "H2") && is_in( "H3")){
      res = "N" + res;
    }
    else if (is_in( "HT2") && is_in( "HT3")){
      res = "N" + res;
    }
    
    // C terminus
    else if( is_in( "OXT")){
      res = "C" + res;
    }
    else if (is_in( "O1") && is_in( "O2")){
      res = "C" + res; 
    }
    else if (is_in("OT1") && is_in( "OT2")){
      res = "C" + res; 
    }
    
    return res;
  }
  
  //#########################################################################################################################
  void do_residue( const map< string, Residue_Info>& res_infos, const map< string, Vector< string> >& amber_infos, JP::Node_Ptr rnode){
    
    rnode->complete();
    auto rname = checked_value_from_node< string>( rnode, "name");
    auto anodes = rnode->children_of_tag( "atom");
    
    Vector< string> anames;
    for( auto anode: anodes){
      auto name = checked_value_from_node< string>( anode, "name");
      anames.push_back( name);
    }
    
    auto is_in = [&]( const string& a){
      auto anend = anames.end();
      auto itr = std::find( anames.begin(), anend, a);
      return (itr != anend);
    };
    
    auto frname = final_residue_name( rname, anames);
    map< string, string> sub_table;
    auto amname = (frname.size() == 4) ? frname.substr(1,3) : frname;
    auto& amber_info = amber_infos.at( amname);
    
    auto itr = res_infos.find( amname);
    if (itr != res_infos.end()){
      auto& res_vars = itr->second.variants;
      
      for( auto& var: res_vars){
        bool use = true;
        for( auto& sub: var){
          auto& a = sub.first;
          if (!is_in( a)){
            use = false;
            break;
          }
        }
        if (use){
          for( auto& sub: var){
            auto& a = sub.first;
            auto i = sub.second;
            auto& new_a = amber_info[i];
            sub_table[a] = new_a;
          }
        }
      }
    }
    
    if (frname.size() == 4){
      if (frname[0] == 'N'){
        sub_table["H"] = "H1";
        sub_table["HT1"] = "H1";
        sub_table["HT2"] = "H2";
        sub_table["HT3"] = "H3";
      }
      else if (frname[0] == 'C'){
        sub_table["O1"] = "O";
        sub_table["O2"] = "OXT";
        sub_table["OT1"] = "O";
        sub_table["OT2"] = "OXT";        
      }
    }
    
    sub_table["HN"] = "H";
    
    output_residue( rnode, frname, sub_table);
  }
  
  //################################################
  map< string, Residue_Info> new_residue_info(){
    
    std::stringstream input( rdata);
    JP::Parser parser( input);
    auto top = parser.top();
    top->complete();
    
    map< string, Residue_Info> res;
    
    auto proc_res = [&]( JP::Node_Ptr rnode){
      auto vnodes = rnode->children_of_tag( "variant");
      Residue_Info rinfo;
      rinfo.residue = checked_value_from_node< string>( rnode, "name");
      for( auto vnode: vnodes){
        auto& variant = rinfo.variants.emplace_back();
        auto snodes = vnode->children_of_tag( "sub");
        for( auto snode: snodes){
          auto atom = snode->data()[0];
          size_t ior = atoi( snode->data()[1].c_str());
          variant.emplace_back( atom, ior);
        }
      }
      res[ rinfo.residue] = std::move( rinfo); 
    };
    
    auto osnode = top->child( "others");
    auto rnodes = osnode->children_of_tag( "residue");
    for( auto rnode: rnodes){
      proc_res( rnode);
    }
    
    return res;
  }
  
  //######################################################
  map< string, Vector< string> > new_amber_info(){
    
    std::stringstream input( rdata);
    JP::Parser parser( input);
    auto top = parser.top();
    top->complete();
    
    map< string, Vector< string> > res;
    
    auto proc_res = [&]( JP::Node_Ptr rnode){
      auto rname = checked_value_from_node< string>( rnode, "name");
      res[ rname] = checked_vector_from_node< string>( rnode, "atoms"); 
    };
    
    auto anode = top->child( "amber");
    auto rnodes = anode->children_of_tag( "residue");
    for( auto rnode: rnodes){
      proc_res( rnode);
    }
    
    return res;
    
  }
  
  //################################################
  void main0( int argc, char* argv[]){
    
    if (argc == 2){
      if (argv[1] == string( "-help")){
        cout << "gives pqrxml file uniform names for residues and atoms; inputs and outputs through standard streams\n";
      }
      else{
        cout << "no args except for -help\n";
      }
      exit(0);
    }
          
    auto residue_info = new_residue_info();
    auto amber_info = new_amber_info();
       
    auto dor = [&]( JP::Node_Ptr rnode){
      do_residue( residue_info, amber_info, rnode);
    };
    

    JP::Parser parser( std::cin);
    auto top = parser.top();
    cout << "<roottag>\n";
    parser.apply_to_nodes_of_tag( top, "residue", dor);
    cout << "</roottag>\n";
  }
  
}

int main( int argc, char* argv[]){
  main0( argc, argv);
}
