//#include <algorithm>
#include "chain_common.hh"
//#include "../../xml/node_info.hh"
//#include "../../lib/circumspheres.hh"
#include "../../pathways/atom_remap.hh"
#include "../../group/group_common.hh"
#include "../../molsystem/system_common.hh"
//#include "../../global/pi.hh"
//#include "../../global/physical_constants.hh"
#include "../../lib/bool_of_string.hh"
#include "../home_transform.hh"
//#include "../../structures/bonded_structures.hh"
#include "../../forces/other/mm_bonded_parameter_info.hh"
#include "../../forces/other/cd_bonded_parameter_info.hh"
#include "../../generic_algs/compute_bond_angle_gen.hh"
#include "../../generic_algs/compute_torsion_gen.hh"
#include "chain_state.hh"
#include "../../group/group_state.hh"
#include "../../generic_algs/remove_duplicates.hh"
#include "../../xml/check_chain_nodes.hh"
#include "../../forces/absinth/absinth.hh"
#include "../../lib/pair_vector.hh"
#include "../sphere_overlap.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

using std::make_pair;
using std::string;
using std::move;
using std::max;
using std::min;

//################################################
template< class Key, class Val, typename ... Msgs>
Val mfind( const JP::Node_Ptr& node, const std::map< Key, Val>& f, Key key, Msgs ... msgs){
  auto itr = f.find( key);
  if (itr == f.end())
    node->perror( __FILE__, "mfind: value not found for ", key, msgs...);
  return itr->second;
}

//###########################################################3
template< class T, class Idx>
Idx found_in_vector( const Vector< T, Idx>& vec, const string& name){
  auto e = vec.end();
  auto b = vec.begin();
  auto itr = std::find_if( b, e, [&]( const T& t){return t.name == name;});
  if (itr == e)
    error( __FILE__, __LINE__, name, "not found in Vector");
  return Idx( itr - b);
}

//##########################################################
template< class Strukt>
Strukt Chain_Common::gen_structure_from_node( const Group_Common& group, const Node_Ptr& node) const{
  Strukt strukt{};
  constexpr size_t n = strukt.atomis.isize();
  auto& coris = strukt.coris;
  //auto& on_chain = strukt.on_chain;
  
  auto mres = array_from_node< string, n>(node, "cores");
  if (mres){
    auto& mnames = *mres;

    for( auto i: range( n)){
      auto& mname = mnames[i];
      if (mname != string("*")){  
        try {
          auto cori = found_in_vector( group.cores, mname);
          coris[i] = cori;
        }
        catch (Jam_Exception& exc){
          node->perror( "core name", mname,
                        "not found in structure of order", n);
        }
      }
      else{
        coris[i] = On_Chain_Tag();
      }
    }
  }
    
  auto& atomis = strukt.atomis;
  auto atomjs = checked_array_from_node< Atom_Index, n>( node, "atoms");
  for( auto k: range(n)){
    auto imo = coris[k];
    auto ja = atomjs[k];
    
    if (strukt.on_core(k)){
      auto im = std::get< Core_Index>( imo);
      auto& matom_map = group.cores[ im].atom_imap;
      
      atomis[k] = mfind( node, matom_map, ja,
                        "atom index not found on core");      
    }
    else{
      atomis[k] = mfind( node, atom_imap, ja, "atom index not found");
    }
  }
    
  return strukt;
}

//################################################
template< class Strukt, class Type_Index>
Strukt Chain_Common::structure_from_node( const std::map< string, Type_Index>& dict,
                           const Group_Common& group, const Node_Ptr& node){
  
  auto strukt = gen_structure_from_node< Strukt>( group, node);
  auto tname = checked_value_from_node< string>( node, "type");
    
  auto itr = dict.find( tname);
  if (itr == dict.end()){
    error( "parameter item not found:", tname);
  }
  strukt.type = itr->second;
  
  return strukt;  
}

//################################################
Length_Constraint_Atoms
Chain_Common::length_constraint_from_node( const Group_Common& group, const JP::Node_Ptr& node) const{
  
  auto cons = gen_structure_from_node< Length_Constraint_Atoms>( group, node);
  cons.length = checked_value_from_node< Length>( node, "length"); 
  return cons;
}

//################################################
Coplanar_Constraint_Atoms
Chain_Common::coplanar_constraint_from_node( const Group_Common& group, const JP::Node_Ptr& node) const{
  
  return gen_structure_from_node< Coplanar_Constraint_Atoms>( group, node);
}

/*
std::list< JP::Node_Ptr> cnodes( JP::Node_Ptr node, const string& tag){
  auto stag = tag + string( "s");
  auto pnode = node->child( stag);
  return pnode->children_of_tag( tag);
}
*/

//##################################################################
void Chain_Common::determine_constrained_atoms(){
    
  auto do_cons = [&]( auto& cons){  
    for( auto& con: cons){
      for( auto k: range( con.n)){
        auto iom = con.coris[k];
        if (std::holds_alternative< On_Chain_Tag>( iom)){
          auto ia = con.atomis[k];
          constrained_atoms.push_back( ia);  
        }  
      }
    }
  };  
    
  do_cons( length_constraints);
  do_cons( coplanar_constraints);

  remove_duplicates( constrained_atoms); 
}

//##################################################################
// refactor!
// constructor
// assume that cores in group are read in already
/*
Chain_Common::Chain_Common( const Nonbonded_Parameter_Info &pinfo, const Bonded_Parameter_Info &bpinfo,
                               const Group_Common &group,
                               const Node_Ptr& node,
                               Chain_Index _number) {

  chain_file = node->parser().file_name();

  number = _number;
  chain_file = file;

  std::ifstream input( file);
  if (!input.is_open()){
    error( "chain file", file, "not opened");  
  }
  
  JP::Parser parser( input, file);
  auto node = parser.top();
  node->complete();
  check_chain_input( node);
  return std::make_pair( node, std::move( parser));
}
*/
//#############################################################################
// refactor!
// constructor
// assume that cores in group are read in already
Chain_Common::Chain_Common( const Nonbonded_Parameter_Info& pinfo,
             const Bonded_Parameter_Info& bpinfo, const Group_Common& group,
             const Node_Ptr& node, Chain_Index _number, Group_Index ig): Macro_Struct( node, pinfo, ig){
    
  chain_file = node->parser().file_name();
  auto& sys = group.system;
  auto conode = node->child( "copy");
  name = checked_value_from_node< string>( node, "name");
  number = _number;
  const_dt = value_from_node< Time>( node, "constant_dt");
  
  if (!conode){  
    atoms_file = checked_value_from_node< string>( node, "atoms");
    
    auto cnodes = [&]( const string& tag) -> std::list< JP::Node_Ptr> {
      auto stag = tag + string( "s");
      auto pnode = node->child( stag);
      if (pnode == nullptr){
        return std::list< JP::Node_Ptr>{};
      }
      else{
        return pnode->children_of_tag( tag);
      }
    };

    for( auto& bnode: cnodes( "bond")){
      bonds.push_back( structure_from_node< Bond>( bpinfo.bond_dict, group, bnode));
    }
    for( auto& anode: cnodes( "angle")){
      angles.push_back( structure_from_node< Angle>( bpinfo.angle_dict, group, anode));  
    }

    for( auto& dnode: cnodes( "dihedral")){
      dihedrals.push_back( structure_from_node< Dihedral>( bpinfo.dihedral_dict, group, dnode));
    }

    for( auto& cnode: cnodes( "length_constraint")){
      length_constraints.push_back( length_constraint_from_node( group, cnode));
    }
    for( auto& cnode: cnodes( "coplanar_constraint")){
      coplanar_constraints.push_back( coplanar_constraint_from_node( group, cnode));
    }

    auto na = atoms.size();
    excluded_atoms.resize( na);
    one_four_atoms.resize( na);
    coulomb_excluded_atoms.resize( na);
    {
      auto exat_node = node->child( "excluded_atoms");
      if (exat_node){
        get_excluded_atoms_from_node_gen( excluded_atoms, excluded_core_atoms, group, exat_node);
      }
       
      auto ofat_node = node->child( "one_four_atoms");
      if (ofat_node){
        get_excluded_atoms_from_node_gen( one_four_atoms, one_four_core_atoms, group, exat_node);        
      }
      auto cex_node = node->child( "excluded_coulomb_atoms");
      if (cex_node){
        get_excluded_atoms_from_node_gen( coulomb_excluded_atoms, coulomb_excluded_core_atoms, group, cex_node);
      }
    }
        
    determine_constrained_atoms();
      
    {
      auto fzres = value_from_node< std::string>( node, "never_frozen");
      if (fzres){
        never_frozen = bool_of_str( *fzres);
      }
    }
    
    /*
    auto afileo = value_from_node< std::string>( node, "areas");
    if (afileo.has_value()){
      get_sasas( afileo.value(), atoms);
    }
    */
  }
  
  // construct chain copy; check, probably fix!
  else{
    auto oname = checked_value_from_node< std::string>( conode, "parent");
    auto& groups = sys.groups;
    const Chain_Common* other_ptr = nullptr;
    const Group_Common* other_group_ptr = nullptr;
    for( auto& lgroup: groups){
      for( auto& chain: lgroup.chains){
        if (chain.name == oname){
          other_ptr = &chain;
          other_group_ptr = &lgroup;
          break;
        }
      }
    }
    
    if (!other_ptr){
      conode->perror( "chain copy not found", oname);
    }
    auto& ogroup = *other_group_ptr;
    auto& other = *other_ptr;

    Transform reltform;
    
    // if chain is attached to one or more cores, use the transform of the first core
    if (!(other.bound_atoms.empty())){
      auto mi = other.bound_atoms.begin()->first;
      auto& ocore = ogroup.cores[mi];
      reltform = ocore.copy_tform;
    }
    else{
      reltform = home_transform( conode);  
    }
    // map equivalent cores from original chain to copy of chain
    std::map< Core_Index, Core_Index> core_map;
    {
      auto econode = conode->child( "equivalent_cores");
      if (econode){
        auto econodes = econode->children_of_tag( "match");
        for( auto& mnode: econodes){
          auto cnames = array_from_node< std::string, 2>( mnode);
          auto cnameh = cnames[0];
          auto cnameo = cnames[1];
          
          Core_Index ih(0);
          for( auto& core: group.cores){
            if (cnameh == core.name){
              break;
            }
            ++ih;
          }
          
          if (ih == group.cores.size()){
            econode->perror( "local core name not found", cnameh);
          }
          Core_Index io(0);
          for( auto& core: ogroup.cores){
            if (cnameo == core.name){
              break;
            }
            ++io;
          }
          
          if (io == ogroup.cores.size()){
            econode->perror( "other core name not found", cnameo);
          }
          core_map.insert( make_pair( io, ih));
        }
      }
    }

    atoms = other.atoms.copy();
    atoms_file = other.atoms_file;
    
    for( auto& atom: atoms){
      atom.pos = reltform.transformed( atom.pos);
    }
    atom_imap = other.atom_imap;
    
    auto core_renum = [&]( auto& strukts){
      for( auto& strukt: strukts){
        auto& coris = strukt.coris;
        for( auto i: range( strukt.n)){
            if (strukt.on_core( i)){
            auto mi = std::get< Core_Index>( coris[i]);
            auto new_ires = core_map.find( mi);
            if (new_ires == core_map.end()){
              error( "core index", mi, "not found");
            }
            coris[i] = new_ires->second; 
          }
        }
      }
    };
    
    length_constraints = other.length_constraints.copy();
    core_renum( length_constraints);
    
    coplanar_constraints = other.coplanar_constraints.copy();
    core_renum( coplanar_constraints);
    
    bonds = other.bonds.copy();
    core_renum( bonds);
    
    angles = other.angles.copy();
    core_renum( angles);
    
    dihedrals = other.dihedrals.copy();
    core_renum( dihedrals);
      
    typedef decltype( excluded_core_atoms) Excluded_Core_Atoms;
      
    auto corex_copy = [&]( const Excluded_Core_Atoms& excluded) -> Excluded_Core_Atoms{
      Excluded_Core_Atoms new_excluded;
      for( auto& info: excluded){
        auto mi = info.first;
        auto& inner_map = info.second;
        auto mjres = core_map.find( mi);
        if (mjres == core_map.end()){
          error( __FILE__, __LINE__, "not get here");
        }
        auto mj = mjres->second;
      
        auto ires = new_excluded.insert( make_pair( mj, std::map< Atom_Index, Vector< Atom_Index> >()));
        if (!ires.second){
          error( __FILE__, __LINE__, "not get here");
        }
        auto& new_inner_map = ires.first->second;      
        
        for( auto& pr: inner_map){
          auto ia = pr.first;
          auto& iva = pr.second;
          new_inner_map.insert( make_pair( ia, iva.copy()));
        }
      }
      return new_excluded;
    };
    
    excluded_core_atoms = corex_copy( other.excluded_core_atoms);
    one_four_core_atoms = corex_copy( other.one_four_core_atoms);
     
    never_frozen = other.never_frozen;
    //characteristic_dt = other.characteristic_dt;
  }
  
  get_temp_interactors( node);  
  //characteristic_dt = natural_time_step( solvent, bpinfo, group);
  if (sys.force_field == Force_Field_Type::Absinth){
    compute_eta_max();
  }
}

// read in information and store temporarily until all groups and cores are read in
/*
 <core_interactors>
   <interactor>
     <group> </group>
     <core> </core>
     <atom> </atom>
     <inner_distance> </inner_distance>
     <outer_distance> </outer_distance>
   </interactor>...
 
 </core_interactors>
 
 */

  Chain_Common::Chain_Common(Browndye::Test_Tag tag, Group_Index _ig,
                             const Vector<Browndye::Atom, Browndye::Atom_Index> &atoms) : Macro_Struct(tag, atoms) {
  auto na = atoms.size();
  excluded_atoms.resize( na);
  one_four_atoms.resize( na);
  ig = _ig;
}

//######################################################################################
void Chain_Common::get_temp_interactors( const Node_Ptr& node){

  auto get_coredum_interactors = [&]( const string& iname, Vector< Core_Interactor_Info>& tinteractors){
    
    std::list< JP::Node_Ptr> conodes;
    auto conode = node->child( iname);
    if (conode){
      conodes = conode->children_of_tag( "interactor");
    }
    for( auto& inode: conodes){
      Core_Interactor_Info corei;
      corei.gname = checked_value_from_node< string>( inode, "group");    
      corei.mname = checked_value_from_node< string>( inode, "core");    
      corei.ia = checked_value_from_node< Atom_Index>( inode, "atom");
      corei.rin = checked_value_from_node< Length>( inode, "inner_distance");
      corei.rout = checked_value_from_node< Length>( inode, "outer_distance");
      tinteractors.push_back( corei);
    }
  };

  get_coredum_interactors( "core_interactors", temp_core_interactors);
  get_coredum_interactors( "dummy_interactors", temp_dummy_interactors);

  std::list< JP::Node_Ptr> chnodes;
  auto chnode = node->child( "chain_interactors");
  if (chnode){
    chnodes = chnode->children_of_tag( "interactor");
  }
  for( auto& inode: chnodes){
    Chain_Interactor_Info chaini;
    chaini.gname = checked_value_from_node< string>( inode, "group");
    chaini.cname = checked_value_from_node< string>( inode, "chain");
    
    auto rinop = value_from_node< Length>( inode, "inner_distance");
    if (rinop){
      chaini.rin = rinop.value();
    }
    
    auto routop = value_from_node< Length>( inode, "outer_distance");
    if (routop){
      chaini.rout = routop.value();
    }
    
    auto touchop = value_from_node< string>( inode, "touching");
    if (touchop){
      chaini.touching = bool_of_str( touchop.value());
    }

    if (!(touchop.has_value() || (routop.has_value() && rinop.has_value()))){
      inode->perror( "need some information for the chain interactor");
    }

    temp_chain_interactors.push_back( chaini);    
  }
}

//###########################################################
void Chain_Common::put_core_interactors( const System_Common& sys){
  
  for( auto& temp_corei: temp_core_interactors){
    Core_Interactor corei;
    auto& groups = sys.groups;
    auto ig = found_in_vector( groups, temp_corei.gname);
    auto& group = groups[ig];
    corei.ig = ig;
    auto im = found_in_vector( group.cores, temp_corei.mname);
    corei.im = im;
    auto& core = group.cores[im];
    corei.ia = anumber( core.atom_imap,  temp_corei.ia);
    corei.rin = temp_corei.rin;
    corei.rout = temp_corei.rout;
    core_interactors.push_back( corei);
  }
  temp_core_interactors.clear();
}

//###########################################################
void Chain_Common::put_dummy_interactors( const System_Common& sys){
  
  for( auto& temp_corei: temp_dummy_interactors){
    Dummy_Interactor dummyi;
    auto& groups = sys.groups;
    auto ig = found_in_vector( groups, temp_corei.gname);
    auto& group = groups[ig];
    dummyi.ig = ig;
    auto id = found_in_vector( group.dummies, temp_corei.mname);
    dummyi.id = id;
    auto& dummy = group.dummies[id];
    dummyi.ia = anumber( dummy.atom_imap, temp_corei.ia);
    dummyi.rin = temp_corei.rin;
    dummyi.rout = temp_corei.rout;
    dummy_interactors.push_back( dummyi);
  }
  temp_dummy_interactors.clear();
}

//###############################################################
// done after all cores and chains are loaded in
void Chain_Common::setup_interactors( const System_Common& sys){
    
  put_core_interactors( sys);  
  put_dummy_interactors( sys);
  
  for( auto& temp_chaini: temp_chain_interactors){
    Chain_Interactor chaini;
    auto& groups = sys.groups;
    auto ig = found_in_vector( groups, temp_chaini.gname);
    chaini.ig = ig;
    chaini.ic = found_in_vector( groups[ig].chains, temp_chaini.cname);
    chaini.rin = temp_chaini.rin;
    chaini.rout = temp_chaini.rout;
    chaini.touching = temp_chaini.touching;
    chain_interactors.push_back( chaini);
  }
  temp_chain_interactors.clear();
}

//#################################
Charge Chain_Common::charge() const{
  Charge sum( 0.0);
  for( auto& atom: atoms){
    sum += atom.charge;
  }
  return sum;
}

Time Chain_Common::natural_time_step( const Solvent& solvent, const Bonded_Parameter_Info& bpinfo, const Group_Common& gcommon,
                              const Group_State& gstate, const Chain_State& cstate) const{

  auto fft = bpinfo.ff_type();
  typedef Force_Field_Type FFT;
  if (fft == FFT::Molecular_Mechanics || fft == FFT::Molecular_Mechanics_HP || fft == FFT::Absinth){
    return natural_time_step_gen( solvent, dynamic_cast< const MM_Bonded_Parameter_Info&>(bpinfo), gcommon, gstate, cstate);
  }
  else if (bpinfo.ff_type() == FFT::Spline){
    return natural_time_step_gen( solvent, dynamic_cast< const CD_Bonded_Parameter_Info&>(bpinfo), gcommon, gstate, cstate);    
  }
  else{
    error( __FILE__, __LINE__, "should not get here");
    return Time( NaN);
  }
}

template< size_t n>
class Iface{
public:
  typedef std::pair< Array< Pos, n>, Array< Vec3< Inv_Length>, n> > Chain;
  typedef double Value;
  typedef size_t Group_Index;
  
  static
  Pos position( const Chain& chain, size_t, size_t k){
    auto& poses = chain.first;
    return poses[k];
  }
  
  static
  void set_derivative( Chain& chain, size_t, size_t i, size_t k, Inv_Length deriv){
    auto& derivs = chain.second;
    derivs[i][k] = deriv;
  }
};

template< class BP_Info, size_t n>
class BP_Struct;

template< class BP_Info>
class BP_Struct< BP_Info, 3>{
public:
  const typename BP_Info::Structures::Angle_Type& operator()( const BP_Info& bp_info, Angle_Type_Index i) const{
    return bp_info.angle_types[i];
  }
};

template< class BP_Info>
class BP_Struct< BP_Info, 4>{
public:
  const typename BP_Info::Structures::Dihedral_Type& operator()( const BP_Info& bp_info, Dihedral_Type_Index i) const{
    return bp_info.dihedral_types[i];
  }
};

template< class FF_Bonded_Parameter_Info>
Time Chain_Common::natural_time_step_gen( const Solvent& solvent, const FF_Bonded_Parameter_Info& bpinfo, const Group_Common& gcommon,
                                  const Group_State& gstate, const Chain_State& cstate) const{

  auto atom_of = [&]( auto& strukt, int k){
    auto ia = strukt.atomis[k];
    if (strukt.on_core(k)){
      auto mi = std::get< Core_Index>( strukt.coris[k]);
      auto& latoms = gcommon.cores[mi].atoms;
      return latoms[ia];
    }
    else{
      return atoms[ia];
    }
  };
  
  auto radius_of = [&]( auto& strukt, int k) -> Length{
    return atom_of( strukt, k).radius;    
  };
  
  auto position_of = [&]( auto& strukt, int k) -> Pos{
    auto ia = strukt.atomis[k];
    if (strukt.on_core(k)){
      auto mi = std::get< Core_Index>( strukt.coris[k]); // add home transform here!
      auto& latoms = gcommon.cores[mi].atoms;
      auto& core = gstate.core_states[mi];
      return core.transformed( latoms[ia].pos);
    }
    else{
      return cstate.atoms[ia].pos;
    }
  };
    
  const auto mu = solvent.relative_viscosity*water_viscosity;
  auto mob_factor = 1.0/(6.0*pi*mu);
  
  const double factor = 0.1;
  
  Time dt( large);
  for( auto& bond: bonds){

    auto rad_of = [&]( size_t k){
      return radius_of( bond, k);
    };
    
    auto rad0 = rad_of( 0);
    auto rad1 = rad_of( 1);
    auto mob = mob_factor*(1.0/rad0 + 1.0/rad1);
    auto pos0 = position_of( bond, 0);
    auto pos1 = position_of( bond, 1);
    auto r = distance( pos1, pos0);

    auto& btype = bpinfo.bond_types[ bond.type];
    auto V2 = fabs( btype.d2Vdr2( r));
    dt = min( dt, factor/(V2*mob));
  }

  auto char_time = [&]( auto& compute_a_and_d, auto& strukt) -> Time{
    typedef typename std::decay< decltype( strukt)>::type Strukt;
    constexpr size_t n = Strukt::n;
    typename Iface<n>::Chain angle_info;
    
    for( size_t i: range( n)){
      angle_info.first[i] = position_of( strukt, i);
    }
    double angle;
    compute_a_and_d( angle_info, 0, angle);
    
    auto& stype = BP_Struct<FF_Bonded_Parameter_Info, n>()( bpinfo, strukt.type);
    auto d2Vdth2 = fabs( stype.d2Vda2( angle));
    
    Time tc{ large};
    for( size_t i: range(3)){
      auto grad = angle_info.second[i];
      auto g2 = norm2( grad);
      auto d2Vdr2 = g2*d2Vdth2;
      auto rad = atom_of( strukt, i).radius;
      auto D = mob_factor/rad;
      Time tci = 2.0*factor/(D*d2Vdr2);
      tc = min( tc, tci);
    }    
    
    return tc;
  };
   
  for( auto& angle: angles){
    auto tc = char_time( compute_bond_angle_and_derivatives< Iface<3> >, angle); 
    dt = min( dt, tc);     
  }
  
  for( auto& dihedral: dihedrals){
    auto tc = char_time( compute_torsion_angle_and_derivatives< Iface<4> >, dihedral); 
    dt = min( dt, tc);
  }
  //cout << "dt here 0 " << dt << endl;

  // now do constraints
  const auto kT = solvent.kT;
  auto con_fact = (3.0*pi/100.0)*(mu/kT);
  
  // atom in length constraint should not move more than 1/10 of bond length
  for( auto& cons: length_constraints){
    auto a0 = radius_of( cons, 0);
    auto a1 = radius_of( cons, 1);
    auto min_a = min( a0, a1);
    auto L = cons.length;
    auto tc = con_fact*L*L*min_a;
    dt = min( dt, tc);
  }

  //cout << "dt here 1 " << dt << endl;

  // same for coplanar constraint, but use minimum radius and minimum distance  
  for( auto& cons: coplanar_constraints){
    Length min_a( large);
    Length min_L( large);
    Array< Atom, 4> latoms;
    for( auto k: range( 4)){
      latoms[k] = atom_of( cons, k);
    }
    for( auto& atom: atoms){
      min_a = min( min_a, atom.radius);
    }
    for( auto i: range( 4)){
      for( auto j: range( i)){
        min_L = min( min_L, distance( latoms[i].pos, latoms[j].pos));
      }
    }
    auto tc = con_fact*sq( min_L)*min_a;
    dt = min( dt, tc);
  }

  //cout << "dt here 2 " << dt << endl;


  return dt;
}

//####################################################################
// assume cores are read in already
void Chain_Common::get_excluded_atoms_from_node_gen( Vector< Vector< Atom_Index>, Atom_Index>& ex_atoms,
                                               Ex_Cor_Map& ex_core_atoms, const Group_Common& group,
                                                const JP::Node_Ptr& node) const{
  /*
    <chain_atom>
      <atom> </atom>
      <ex_atoms> </ex_atoms>
    </chain_atom>...
    
    <core>
      <name> </name>
      <core_atom>
        <atom> </atom>
        <ex_atoms> </ex_atoms>
      </core_atom>...
    </core>...
  */
  
  try{
    auto canodes = node->children_of_tag( "chain_atom");
    for( const auto& canode: canodes){
      auto ja = checked_value_from_node< Atom_Index>( canode, "atom");
      auto ia = mfind( canode, atom_imap, ja);
      auto exais = checked_vector_from_node< Atom_Index>( canode, "ex_atoms");
          
      if (ia >= ex_atoms.size()){
        canode->perror( "atom ", ia, " not found in chain");
      }
      auto& ex_ats = ex_atoms[ia];
      for( auto jal: exais){
        ex_ats.push_back( mfind( canode, atom_imap, jal));
      }
    }
    
    auto core_nodes = node->children_of_tag( "core");
    for( const auto& cnode: core_nodes){
      auto lname = checked_value_from_node< std::string>(cnode, "name");
      auto mi = found_in_vector( group.cores, lname);
      auto& core = group.cores[mi];
      auto& camap = ex_core_atoms[mi];
      
      auto lcanodes = cnode->children_of_tag( "core_atom");
      for( const auto& canode: lcanodes){
        auto ka = checked_value_from_node< Atom_Index>( canode, "atom");
        auto ia = mfind( canode, core.atom_imap, ka, "atom not found on core", mi);
        auto exais = checked_vector_from_node< Atom_Index>( canode, "ex_atoms");     
        auto& avec = camap[ia];
        for( auto ja: exais){
          avec.push_back( mfind( canode, atom_imap, ja, "atom not found on chain"));
        }
      }
    }
    
    reorder_excluded_atoms( ex_atoms);
  }
  catch (Jam_Exception& exc){
    node->perror( "getting excluded atoms", exc.what());
  }
}
//####################################################
void Chain_Common::reorder_excluded_atoms( Vector< Vector< Atom_Index>, Atom_Index>& ex_atoms) {
  
  
  // make sure ex_atoms are in proper order
  auto na = ex_atoms.size();
  
  // put in lower ones
  for( auto ia: range( na)){
    auto& ex_atsi = ex_atoms[ia];
    for( auto ja: ex_atsi){
      if (ja > ia){
        auto& ex_atsj = ex_atoms[ja];
        auto endj = ex_atsj.end();
        auto itr = std::find( ex_atsj.begin(), endj, ia);
        if (itr == endj){
          ex_atsj.push_back( ia);
        }
      }
    }
  }
  
  // get rid of ones that are higher
  for( auto ia: range( na)){
    Vector< Atom_Index> new_ex_ats;
    auto& ex_ats = ex_atoms[ia];
    for( auto ja: ex_ats){
      if (ja < ia){
        new_ex_ats.push_back( ja);
      }
    }
    ex_ats = std::move( new_ex_ats);
  }
}

//#######################################################
// later, add the core atoms
void Chain_Common::compute_eta_max(){
   
  auto na = atoms.size();
  Vector< std::set< Atom_Index>, Atom_Index> nebors( na);
  for (auto ia: range( na)){
    auto& nbrs = excluded_atoms[ia];
    for( auto ja: nbrs){
      nebors[ia].insert( ja);
      nebors[ja].insert( ia);
    }
  }
  
  for( auto ia: range(na)){
    auto& nebs = nebors[ia];
   
    auto& atoma = atoms[ia];
    auto posa = atoma.pos;
    auto ra = atoma.radius;
    const auto h = Absinth::rw;
    Volume olap_in{ 0.0}, olap_ot{ 0.0};
    for( auto ib: nebs){
      auto rb = atoms[ib].radius;
      auto posb = atoms[ib].pos;
      auto r = distance( posa, posb);
      olap_in += sphere_overlap( ra, rb, r);
      olap_ot += sphere_overlap( ra + h, rb, r);
    }
    
    auto olap = olap_ot - olap_in;
    auto Vmax = (pi4/3)*(cube(ra + h) - cube(ra));
    atoma.set_eta_max(  1.0 - olap/Vmax);
  }
}
//##################################################################
JP::Node_Ptr Chain_Common::chain_node( const std::string& file) {

  Node_Ptr node = std::make_shared<JP::Node>(file, JP::Node::From_File{});
  check_chain_input(node);
  return node;
}
}
