#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Interface class to Gaussian_Generator to make the Browndye_RNG class

#include <random>
#include <iostream>
#include "../lib/vector.hh"

/*
This class is used to generate gaussian random numbers.
*/

namespace Browndye{

class Browndye_RNG{
public:

  // zero mean, unit variance
  double gaussian(){
    return gauss( rng);
  }

  // uniform between 0 and 1
  double uniform(){
    return unif( rng);
  }

  void set_seed( const Vector< uint32_t>& seed){
    std::seed_seq sseq( seed.begin(), seed.end());
    rng.seed( sseq);
  }

    [[maybe_unused]] void set_state( const std::string& str){
    std::stringstream ss;
    ss << str;
    ss >> rng;
  }

  [[nodiscard]] std::string state() const{
    std::stringstream ss;
    ss << rng;
    return ss.str();
  }

    [[maybe_unused]] std::mt19937_64& underlying_gen(){
    return rng;
  }
  
private:
  std::mt19937_64 rng;
  std::normal_distribution< double> gauss;
  std::uniform_real_distribution< double> unif;
};

}

