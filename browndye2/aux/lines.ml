(* 
Used internally.
Used for parsing plain text lines.

readline buf : reads text from buf until a carriage return is reached.

stripped str : returns str but with whitespace at either end stripped away

split str: returns array of strings from splitting str along runs of whitespace

*)

let readline buf = 
  Scanf.bscanf buf "%s@\n" (fun s -> s)


let stripped str = 
  let n = String.length str in
  let ib = ref 0 in
  let finis = ref false in
  let isc c =
    not (c = ' ' || c = '\n' || c = '\t')
  in
    while (not !finis) && !ib < n do
      let c = str.[!ib] in
	if isc c then
	  finis := true
	else
	  ib := !ib + 1
    done;

    finis := false;
    let ie = ref (n-1) in
      while (not !finis) && !ie > -1 do
	let c = str.[!ie] in
	  if isc c then
	    finis := true
	  else
	  ie := !ie - 1
      done;      
      String.sub str (!ib) ((!ie - !ib) + 1)


let rec split str =
  let cstr c = 
    String.make 1 c

  in
 
  let isw c = 
    (c = ' ') || (c = '\n') || (c = '\t')
  in
  let n = String.length str in
    if n = 1 then
      let c = str.[0] in
      if isw c then
	[||]
      else
	[| cstr c|]
    else
      let n0 = n/2 in
      let n1 = n - n0 in
      let str0 = String.sub str 0 n0
      and str1 = String.sub str n0 n1 in
      let spl0,spl1 = split str0, split str1 in
	if (isw str0.[n0-1]) || (isw str1.[0]) then
	  Array.concat [spl0; spl1]
	else
	  let ns0 = Array.length spl0 in
	  let midstr = spl0.( ns0-1) ^ spl1.(0) in
	  let spl0t = Array.init (ns0-1) (fun i -> spl0.(i)) in
	  let spl1t = 
	    let ns1 = Array.length spl1 in
	      Array.init (ns1-1) (fun i -> spl1.(i+1))
	  in
	    Array.concat [spl0t; [|midstr|]; spl1t]


(**************************************************************)
(* Test code *)      
(*
let buf = Scanf.Scanning.from_file "bull.txt";;

let line = readline buf;;
Printf.printf "line |%s|\n" (stripped line);;
*)

(*
let str = " abc 123   pqr 9  678 ";;

let arr = split str;;

Array.iter
  (fun str -> Printf.printf "%s\n" str)
  arr
;;
*)
