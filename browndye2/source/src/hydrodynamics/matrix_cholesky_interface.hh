#pragma once

#include "../lib/vector.hh"

namespace Browndye{

template< class Value, class _Index>
class Mat_Cholesky_Iface{
public:
  typedef _Index Index;
  template< class T> using GMatrix = Vector< Vector<T, Index>, Index>;
  typedef GMatrix< Value> Matrix;
  typedef decltype( sqrt( Value())) Sqrt_Mobility;
  typedef Value Matrix_Value;
  typedef decltype( sqrt( Value())) Dec_Matrix_Value;
  typedef GMatrix< Dec_Matrix_Value> Dec_Matrix;

  template< class T>
  static
  Index size( const GMatrix<T>& mat){
    return mat.size();
  }

  template< class T>
  static
  T element( const GMatrix<T>& mat, Index i, Index j){
    return mat[i][j];
  }

  template< class T>
  static
  T& element_ref( GMatrix<T>& mat, Index i, Index j){
    return mat[i][j];
  }
  
  template< class T>
  class Vector_Type{
  public:
    typedef Vector< T, Index> Result;
  };

  template< class T>
  static
  void resize( Vector< T, Index>& v, Index n){
    v.resize( n);
  }

  template< class T>
  static
  T element( const Vector< T, Index>& v, Index i){
    return v[i];
  }

  template< class T>
  static
  T& element_ref( Vector< T, Index>& v, Index i){
    return v[i];
  }

  template< class T>
  static
  T transpose( T t){
    return t;
  }

  template< class T>
  static
  bool is_negative( T t){
    return t <= T( 0.0);
  }

  template< class T>
  static
  bool is_zero( T t){
    return t == T( 0.0);
  }

  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }

};
}


