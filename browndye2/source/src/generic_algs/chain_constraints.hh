#pragma once

/*
Interface types and functions:

typedef System;
typedef Mobility, Length;
typedef Constraint_Index, Particle_Index;

template< class Pos>
void get_position( const System&, Particle_Index ip, Pos&);

template< class Pos>
void get_position_change( const System&, Particle_Index ip, Pos&);

template< class Pos>
void set_position( System&, Particle_Index, Pos);

std::pair< Particle_Index, Particle_Index>
length_constraint_particles( System&, Constraint_Index);

Length constraint_length( const System&, Constraint_Index);

bool is_coplanar_constraint( const System&, Constraint_Index);

std::array< Particle_Index, 4>
coplanar_constraint_particles( const System&, Constraint_Index);

Particle_Index n_particles( const System&);
Constraint_Index n_constraints( const System&);

Mobility mobility( const System&, Particle_Index ip);


*************************************************************
*/

#include <list>
#include <set>
#include <functional>

#include "../lib/vector.hh"
#include "../global/error_msg.hh"
#include "../global/limits.hh"
#include "../lib/skyline_cholesky.hh"
#include "symmetric_skyline_matrix.hh"
#include "../lib/sq.hh"
#include "../lib/autodiff.hh"
#include "../generic_algs/conjugate_gradient_minimizer.hh"
//#include "../tet/tet_forces.hh"

namespace Browndye::Chain_Constraint_Satisfier{
  
  class CNeb_Tag{};
  typedef Index< CNeb_Tag> CNeb_Index;
  
  //class Row_Tag{};
  //typedef Index< Row_Tag> Row_Index;
  
  using std::max;
       
  //#############################################
  template< class I>
  class Doer{
  public:
    typedef typename I::Mobility Mobility;
    typedef typename I::Length Length;
    typedef typename I::Particle_Index P_Index;
    typedef typename I::Constraint_Index C_Index;
    typedef typename I::System System;
    typedef decltype( Length()/Mobility()) Impulse;
    typedef Vec3< Length> Pos;

    explicit Doer( const System&);
    Doer copy() const;

    void satisfy_no_init( System&, double tol);
    //void satisfy( System&, double tol, size_t& n_steps);
    void satisfy_simple( System&, double tol, size_t& n_steps);
    double sys_rms_violation( const System&); 

  private:
    typedef decltype( Length()*Length()) Length2;

    typedef Vector< Length> Lengths;

    static
    double constraint_obj( const System&, Length lscale, const Lengths& xs0, const Lengths& xs);

    static
    double alignment_obj( const System&, Length lscale, const Lengths& xs0, const Lengths& xs);

    static
    std::pair< Array< Length, 3>, Array< Length2, 3> > alignment_sums( const Lengths& xs0, const Lengths& xs);

    typedef decltype( 1.0/Length()) Inv_Length;

    static
    void get_constraint_obj_grad( const System& sys, Length lscale, const Lengths& xs0, const Lengths& xs, Vector< Inv_Length>& g);

    static
    void add_alignment_obj_grad(const System& sys, Length lscale, const Lengths& xs0, const Lengths& xs, Vector< Inv_Length>& g);

    static
    auto pos_of( const Vector< Length>& xs, P_Index ip);

    static
    void add_grad( Vector< Inv_Length>& grad, P_Index ip, Inv_Length g0,
                   Inv_Length g1, Inv_Length g2);

    Length rms_abs_violation( const System& sys) const;
    Length avg_length_scale( const System&) const;
    
    Length length_violation( const System&, C_Index ic) const;
    Length coplanar_violation( const System&, C_Index ic) const;

    std::pair<bool, size_t>
    do_simple_newton_loop(System &, double tol);
    

    Length coplanar_scale( const System& sys, C_Index ic) const;
    Vector< Length, C_Index> scales_for_coplanar( const System& sys);
    
    void setup_length( const System& sys, C_Index ic);
    void setup_coplanar( const System& sys, C_Index ic);

    void update_positions( const System& sys,
                            const Vector< Impulse, C_Index>&);


    std::pair< Pos, Pos>
    length_constraint_positions( const System&, C_Index) const;
    
    Array< Pos, 4>
    coplanar_constraint_positions( const System& sys, C_Index ic) const;
    void do_simple_newton_step(System &sys, bool &newton_success);

    typedef Vector< std::list< C_Index>, P_Index> CList;
    
    CList cons_of_parts_list( const System&) const;
    void setup_cons_of_parts( const System&, const CList&);
         
    typedef Vector< std::set< C_Index>, C_Index> NSet;     
    NSet new_cons_nebs_set( const System&, const CList&) const;
    void setup_cons_nebs( const System&, const NSet&);
    Vector< C_Index, C_Index> new_widths( const System&) const;
    void setup_cmatrix( const System& sys);
    
    std::pair< size_t, Array< P_Index, 4> >
    cons_parts( const System& sys, C_Index ic);
    
    void setup( const System& sys);
    void setup_ctdc_matrix( const System& sys);
    
    class PI_Tag{};
    typedef Index< PI_Tag> PI_Index;
    
    // data
    Vector< Vector< C_Index, CNeb_Index>, C_Index> cons_nebs;
    Vector< Vector< C_Index, CNeb_Index>, P_Index> cons_of_parts;

    Vector< Pos, P_Index> xs0, old_xs0;
    Symmetric_Skyline_Matrix< Mobility, C_Index> ctdc_matrix;
    
    Vector< Vector< Array< double, 3>, PI_Index>, C_Index> C;
    Vector< Impulse, C_Index> F;
    Vector< Length, C_Index> mPhi;

    template< class T0, class T1, class T2, class U0, class U1, class U2>
    static
    auto tdot( std::tuple< T0,T1,T2> t, std::tuple< U0,U1,U2> u){
      return std::get<0>( t)*std::get<0>( u) + std::get<1>( t)*std::get<1>( u) + std::get<2>( t)*std::get<2>( u);
    }

    template< class T0, class T1, class T2, class U0, class U1, class U2>
    static
    auto tcross( std::tuple< T0,T1,T2> t, std::tuple< U0,U1,U2> u){
      auto v0 = std::get<1>( t)*std::get<2>( u) - std::get<2>( t)*std::get<1>( u);
      auto v1 = std::get<2>( t)*std::get<0>( u) - std::get<0>( t)*std::get<2>( u);
      auto v2 = std::get<0>( t)*std::get<1>( u) - std::get<1>( t)*std::get<0>( u);
      return std::make_tuple( v0,v1,v2);
    }

    template< class T0, class T1, class T2>
    static
    auto tnorm( std::tuple< T0,T1,T2> t){
      return sqrt( tdot( t,t));
    }

    template< class T0, class T1, class T2, class U0, class U1, class U2>
    static
    auto tplus( std::tuple< T0,T1,T2> t, std::tuple< U0,U1,U2> u){
      auto v0 = std::get<0>( t) + std::get<0>( u);
      auto v1 = std::get<1>( t) + std::get<1>( u);
      auto v2 = std::get<2>( t) + std::get<2>( u);
      return std::make_tuple( v0,v1,v2);
    }

    template< class T0, class T1, class T2, class U0, class U1, class U2>
    static
    auto tminus( std::tuple< T0,T1,T2> t, std::tuple< U0,U1,U2> u){
      auto v0 = std::get<0>( t) - std::get<0>( u);
      auto v1 = std::get<1>( t) - std::get<1>( u);
      auto v2 = std::get<2>( t) - std::get<2>( u);
      return std::make_tuple( v0,v1,v2);
    }

    template< class U, class T0, class T1, class T2>
    static
    auto ttimes( U u, std::tuple< T0,T1,T2> t){
      auto v0 = u*std::get<0>(t);
      auto v1 = u*std::get<1>(t);
      auto v2 = u*std::get<2>(t);
      return std::make_tuple( v0,v1,v2);
    }

  };
     
  //#########################################################  
  template< class I>
  auto Doer<I>::length_constraint_positions( const System& sys, C_Index ic) const
                  -> std::pair< Pos, Pos>{
      
    auto [i0,i1]  = I::length_constraint_particles( sys, ic);
    
    auto x0 = xs0[i0];
    auto x1 = xs0[i1];
     
    return std::make_pair( x0,x1);
  }

  template< class I>
  auto Doer<I>::coplanar_constraint_positions( const System& sys, C_Index ic) const
                  -> Array< Pos, 4>{
     
    auto is  = I::coplanar_constraint_particles( sys, ic);
    return is.mapped( [&]( P_Index i){
      return xs0[i];
    });
  }  

  //##############################################################
  template< class I>
  auto Doer<I>::avg_length_scale( const System& sys) const -> Length {
    
    Length sum{ 0.0};
    auto nc = I::n_constraints( sys);
    for( auto ic: range(nc)){
      if (I::is_coplanar_constraint( sys, ic)){
        auto ls = coplanar_scale( sys, ic);
        sum  += ls;  
      }
      else{
        auto ls = I::constraint_length( sys, ic);
        sum  += ls;
      }
    }
    return sum/(double(nc/C_Index(1)));
  }
  
  //############################################################
  template< class I>
  auto Doer<I>::rms_abs_violation( const System& sys) const -> Length {
    
    auto nc = I::n_constraints( sys);

    Length2 sum2{0.0};
    Length aviol;
    for( auto ic: range(nc)){
      if (I::is_coplanar_constraint( sys, ic)){
        aviol = coplanar_violation( sys, ic);
      }
      else{
        aviol = length_violation( sys, ic);
      }
      sum2 += aviol*aviol;
    }

    auto var = sum2/((double)(nc/C_Index(1)));
    return sqrt( var);
  }

  //##################################################################
  template< class I>
  auto Doer<I>::coplanar_violation( const System& sys, C_Index ic) const -> Length{
    
    auto ps = coplanar_constraint_positions( sys, ic);
      
    auto com = (1.0/3)*( ps[1] + ps[2] + ps[3]);
    auto d2 = ps[2] - ps[1];
    auto d3 = ps[3] - ps[1];
    auto perp = normed( cross( d2,d3));
    auto d0 = ps[0] - com;
    return dot( perp, d0);
  }
   
  //##################################################################
  template< class I>
  auto Doer<I>::length_violation( const System& sys, C_Index ic) const -> Length{
    
    auto [pos0,pos1] = length_constraint_positions( sys, ic);
    auto r0 = I::constraint_length( sys, ic);
    auto r = distance( pos0, pos1);
    return fabs( r - r0);
  }

  //#####################################################
  template< class I>
  auto Doer<I>::cons_of_parts_list( const System& sys) const ->
    Vector< std::list< C_Index>, P_Index>{
       
    auto np = I::n_particles( sys);
    auto nc = I::n_constraints( sys);
    Vector< std::list< C_Index>, P_Index> cons_of_parts_lst( np);   
  
    for (auto ic: range( nc)){
      Array< P_Index, 4> is;
      if (I::is_coplanar_constraint( sys, ic)){
        is = I::coplanar_constraint_particles( sys, ic);
        cons_of_parts_lst[ is[2]].push_back( ic);
        cons_of_parts_lst[ is[3]].push_back( ic);
      }
      else{
        std::tie( is[0],is[1]) = I::length_constraint_particles( sys, ic);
      }
      
      cons_of_parts_lst[ is[0]].push_back( ic);
      cons_of_parts_lst[ is[1]].push_back( ic);
    }
    return cons_of_parts_lst;
  }
  
  //#################################################################
  template< class I>
  void Doer<I>::setup_cons_of_parts( const System& sys,
                                      const CList& clist){
    
    auto np = I::n_particles( sys); 
    cons_of_parts.resize( np);
    for (auto ip: range( np)){
      CNeb_Index ncl( clist[ip].size());
      cons_of_parts[ip].resize( ncl);
      auto itr = clist[ip].begin();
      for (auto ic = CNeb_Index(0); ic < ncl; ic++){
        cons_of_parts[ip][ic] = *itr;
        ++itr;
      }
    }
  }
  
  //#############################################################
  template< class I>
  auto Doer<I>::new_cons_nebs_set( const System& sys,
                                    const CList& clist) const -> NSet{
     
    Vector< std::set< C_Index>, C_Index> nset{};
    
    auto nc = I::n_constraints( sys);
    auto np = I::n_particles( sys);
    nset.resize( nc);
    for (auto ip: range( np)){
      for (auto itr0 = clist[ip].begin(); 
           itr0 != clist[ip].end(); itr0++){ 
        
        C_Index ic0 = *itr0;
        nset[ ic0].insert( ic0);
        for (auto itr1 = clist[ip].begin(); itr1 != itr0; itr1++){
          C_Index ic1 = *itr1;
          nset[ ic0].insert( ic1);
        }
      } 
    }
    return nset;
  }
  
  //###################################################################
  template< class I>
  void Doer<I>::setup_cons_nebs( const System& sys, const NSet& nset){
    
    auto nc = I::n_constraints( sys);
    cons_nebs.resize( nc);
    for (auto icn: range( nc)){
      CNeb_Index nn( nset[ icn].size());
      cons_nebs[ icn].resize( nn);
      
      auto itr = nset[icn].begin();
      for (auto k: range( nn)){
        cons_nebs[ icn][k] = *itr;
        ++itr;
      }
    }
  }
  
  //######################################################
  template< class I>
  auto Doer<I>::new_widths( const System& sys) const ->
    Vector< C_Index, C_Index>{
    
    auto nc = I::n_constraints( sys);
    Vector< C_Index, C_Index> widths( nc, C_Index(0));
    
    for (auto icn: range( nc)){
      for (auto j: range( cons_nebs[icn].size())){
        C_Index jcn = cons_nebs[icn][j];
        if (jcn < icn){
          widths[icn] = max( widths[icn], C_Index(0) + (icn - jcn));
        }
        else{
          widths[jcn] = max( widths[jcn], C_Index(0) + (jcn - icn));
        }
      }
    }
    return widths;
  }
  
  //#######################################################
  template< class I>
  void Doer<I>::setup_cmatrix( const System& sys){
     
    auto nc = I::n_constraints( sys);
    C.resize( nc);
    for (auto ic: range( nc)){
      auto& col = C[ic];
      
      auto is_cop = I::is_coplanar_constraint( sys, ic);
      PI_Index nr{ is_cop ? 4u : 2u};
      
      col.resize( nr);
    }
  }
  
  //###########################################################
  // constructor
  template< class I>
  Doer< I>::Doer( const System& sys){ 
    
    auto clist = cons_of_parts_list( sys);   
    setup_cons_of_parts( sys, clist);
      
    auto nset = new_cons_nebs_set( sys, clist);
    setup_cons_nebs( sys, nset);
    
    auto nc = I::n_constraints( sys);
    F.resize( nc);
    mPhi.resize( nc);

    auto np = I::n_particles( sys);
    xs0.resize( np);

    ctdc_matrix.initialize( nc, new_widths( sys));
     
    setup_cmatrix( sys);
  }
 
  //###########################################################
  // get state from xs0
  template< class I>
  void Doer<I>::setup( const System& sys){
    
    auto nc = I::n_constraints( sys);
        
    for( auto ic: range( nc)){         
      if (I::is_coplanar_constraint( sys, ic)){
        setup_coplanar( sys, ic);          
      }
      else{          
        setup_length( sys, ic);
      }
    }
  }

  //#############################################################
  template< class I>
  void Doer<I>::do_simple_newton_step(System &sys, bool &success) {
    
    success = true;
    setup( sys);
    setup_ctdc_matrix( sys);    
     
    ctdc_matrix.factor( success);
    
    if (success){
      ctdc_matrix.solve( mPhi, F);
      ctdc_matrix.reset();
      update_positions( sys, F);
    } 
  }   
  
  //#########################################################
  template< class I>
  auto Doer<I>::pos_of( const Lengths& xs, P_Index ip){
    auto ip3 = 3*(ip/P_Index(1));
    auto x = xs[ ip3 + 0];
    auto y = xs[ ip3 + 1];
    auto z = xs[ ip3 + 2];
    return Pos{ x,y,z};
  }

  //##########################################################################################
  template< class I>
  void Doer<I>::add_grad( Vector< Inv_Length>& grad,
                       P_Index ip, Inv_Length g0, Inv_Length g1, Inv_Length g2){
    auto ip3 = 3*(ip/P_Index(1));
    grad[ ip3 + 0] += g0;
    grad[ ip3 + 1] += g1;
    grad[ ip3 + 2] += g2;
  }

  //##########################################################################################
  template< class I>
  auto Doer<I>::alignment_sums( const Lengths &xs0, const Lengths &xs) -> std::pair< Array< Length, 3>, Array< Length2, 3> > {

    auto np3 = xs.size();
    P_Index np{ np3/3};

    Array< Length, 3> fsum( Zeroi{});
    Array< Length2, 3> tsum( Zeroi{});
    for( auto i: range( np)){
      auto pos = pos_of( xs, i);
      auto pos0 = pos_of( xs0, i);
      auto dpos = pos - pos0;
      tsum += cross( dpos, pos0);
      fsum += dpos;
    }

    return std::make_pair( fsum, tsum);
  }

  //##########################################################################################
  template< class I>
  double Doer<I>::alignment_obj( const System&, Length lscale, const Lengths& xs0, const Vector<Length>& xs) {

    auto [fsum, tsum] = alignment_sums( xs0, xs);
    auto sc2 = lscale*lscale;
    auto sc4 = sc2*sc2;
    return  0.5*( norm2( fsum)/sc2 + norm2( tsum)/sc4);
  }

  //##########################################################################################
  template< class I>
  void Doer<I>::add_alignment_obj_grad(const System& sys, Length lscale, const Lengths& xs0, const Lengths& xs, Vector< Inv_Length>& g){

    namespace A = Autodiff;

    auto [fsum, tsum] = alignment_sums( xs0, xs);

    auto sc2 = lscale*lscale;
    auto sc4 = sc2*sc2;

    auto np3 = xs.size();
    P_Index np{ np3/3};
    for( auto i: range( np)){
      auto gg = fsum/sc2;
      add_grad( g, i, gg[0], gg[1], gg[2]);
    }

    for( auto i: range(np)){
      auto pos = pos_of( xs, i);
      auto pos0 = pos_of( xs0, i);

      auto p0x = A::constant<0>( pos0[0]);
      auto p0y = A::constant<1>( pos0[1]);
      auto p0z = A::constant<2>( pos0[2]);
      auto cp0 = std::make_tuple( p0x,p0y,p0z);

      auto px = A::inputv<0>( pos[0]);
      auto py = A::inputv<1>( pos[1]);
      auto pz = A::inputv<2>( pos[2]);
      auto cp = std::make_tuple( px,py,pz);

      auto dpos = tminus( cp, cp0);
      auto [a0,a1,a2] = tcross( dpos, cp0);
      auto a0d = A::derivative_struct( a0);
      auto a1d = A::derivative_struct( a1);
      auto a2d = A::derivative_struct( a2);

      auto g0y = A::derivative( a0d, py);
      auto g0z = A::derivative( a0d, pz);

      auto g1x = A::derivative( a1d, px);
      auto g1z = A::derivative( a1d, pz);

      auto g2x = A::derivative( a2d, px);
      auto g2y = A::derivative( a2d, py);

      auto gx = (g1x*tsum[1] + g2x*tsum[2])/sc4;
      auto gy = (g0y*tsum[0] + g2y*tsum[2])/sc4;
      auto gz = (g0z*tsum[0] + g1z*tsum[1])/sc4;

      add_grad( g, i, gx,gy,gz);
    }
  }

  //##########################################################################################
  template< class I>
  double Doer<I>::constraint_obj( const System& sys, Length lscale, const Lengths& xs0, const Lengths& xs){

    auto nc = I::n_constraints( sys);
    double sum = 0.0;
    for( auto ic: range( nc)){
      if (!I::is_coplanar_constraint( sys, ic)) {
        auto [i0, i1] = I::length_constraint_particles(sys, ic);
        auto x0 = pos_of( xs,i0);
        auto x1 = pos_of( xs,i1);
        auto d = x1 - x0;
        auto r = norm(d);
        Length r0 = I::constraint_length(sys, ic);
        auto c = (r - r0)/lscale;
        sum += 0.5*c*c;
      }
      else{
        auto is = I::coplanar_constraint_particles( sys, ic);
        auto x0 = pos_of( xs,is[0]);
        auto x1 = pos_of( xs,is[1]);
        auto x2 = pos_of( xs,is[2]);
        auto x3 = pos_of( xs,is[3]);

        auto com = (1.0/3)*( x1 + x2 + x3);
        auto d2 = x2 - x1;
        auto d3 = x3 - x1;
        auto perp = normed( cross( d2,d3));
        auto d0 = x0 - com;
        auto c =  dot( perp, d0)/lscale;
        sum += 0.5*c*c;
      }
    }
    return sum + alignment_obj( sys,lscale, xs0, xs);
  }

  //###################################################################################################################
  template< class I>
  void Doer<I>::get_constraint_obj_grad( const System& sys, Length lscale, const Lengths& xs0, const Lengths& xs, Vector< Inv_Length>& g){

    namespace A = Autodiff;
    auto nc = I::n_constraints( sys);
    g.fill( Inv_Length{ 0.0});
    for( auto ic: range( nc)){
      if (!I::is_coplanar_constraint( sys, ic)) {
        auto [i0, i1] = I::length_constraint_particles(sys, ic);
        auto x0 = pos_of( xs,i0);
        auto x1 = pos_of( xs,i1);

        auto x0_0 = A::inputv<0>( x0[0]);
        auto x0_1 = A::inputv<1>( x0[1]);
        auto x0_2 = A::inputv<2>( x0[2]);
        auto x1_0 = A::inputv<3>( x1[0]);
        auto x1_1 = A::inputv<4>( x1[1]);
        auto x1_2 = A::inputv<5>( x1[2]);
        auto x0v = std::make_tuple( x0_0, x0_1, x0_2);
        auto x1v = std::make_tuple( x1_0, x1_1, x1_2);

        auto d = tminus( x0v, x1v);
        auto r = tnorm(d);
        Length r0 = I::constraint_length(sys, ic);
        auto r0c = A::constant<0>( r0);
        auto lscalec = A::constant<1>( lscale);
        auto oh = A::constant<2>( 0.5);
        auto c = (r - r0c)/lscalec;
        auto phi = oh*c*c;

        auto dphi = A::derivative_struct( phi);
        auto g0_0 = A::derivative( dphi, x0_0);
        auto g0_1 = A::derivative( dphi, x0_1);
        auto g0_2 = A::derivative( dphi, x0_2);
        auto g1_0 = A::derivative( dphi, x1_0);
        auto g1_1 = A::derivative( dphi, x1_1);
        auto g1_2 = A::derivative( dphi, x1_2);

        add_grad( g, i0, g0_0, g0_1, g0_2);
        add_grad( g, i1, g1_0, g1_1, g1_2);
      }
      else{
        auto is = I::coplanar_constraint_particles( sys, ic);
        auto x0 = pos_of( xs,is[0]);
        auto x1 = pos_of( xs,is[1]);
        auto x2 = pos_of( xs,is[2]);
        auto x3 = pos_of( xs,is[3]);

        auto ot = A::constant<0>( 1.0/3);
        auto oh = A::constant<1>( 0.5);
        auto one = A::constant<2>( 1.0);
        auto clscale = A::constant<4>( lscale);
        auto x0_0 = A::inputv<0>( x0[0]);
        auto x0_1 = A::inputv<1>( x0[1]);
        auto x0_2 = A::inputv<2>( x0[2]);
        auto x1_0 = A::inputv<3>( x1[0]);
        auto x1_1 = A::inputv<4>( x1[1]);
        auto x1_2 = A::inputv<5>( x1[2]);
        auto x2_0 = A::inputv<6>( x2[0]);
        auto x2_1 = A::inputv<7>( x2[1]);
        auto x2_2 = A::inputv<8>( x2[2]);
        auto x3_0 = A::inputv<9>( x3[0]);
        auto x3_1 = A::inputv<10>( x3[1]);
        auto x3_2 = A::inputv<11>( x3[2]);

        auto x0v = std::make_tuple( x0_0, x0_1, x0_2);
        auto x1v = std::make_tuple( x1_0, x1_1, x1_2);
        auto x2v = std::make_tuple( x2_0, x2_1, x2_2);
        auto x3v = std::make_tuple( x3_0, x3_1, x3_2);

        auto com = ttimes( ot, tplus( tplus( x1v, x2v), x3v));
        auto d2 = tminus( x2v, x1v);
        auto d3 = tminus( x3v, x1v);
        auto uperp = tcross( d2, d3);
        auto rperp = tnorm( uperp);
        auto perp = ttimes( (one/rperp), uperp);
        auto d0 = tminus( x0v, com);
        auto c =  tdot( perp, d0)/clscale;
        auto phi = oh*c*c;

        auto dphi = A::derivative_struct( phi);
        auto g0_0 = A::derivative( dphi, x0_0);
        auto g0_1 = A::derivative( dphi, x0_1);
        auto g0_2 = A::derivative( dphi, x0_2);
        auto g1_0 = A::derivative( dphi, x1_0);
        auto g1_1 = A::derivative( dphi, x1_1);
        auto g1_2 = A::derivative( dphi, x1_2);
        auto g2_0 = A::derivative( dphi, x2_0);
        auto g2_1 = A::derivative( dphi, x2_1);
        auto g2_2 = A::derivative( dphi, x2_2);
        auto g3_0 = A::derivative( dphi, x3_0);
        auto g3_1 = A::derivative( dphi, x3_1);
        auto g3_2 = A::derivative( dphi, x3_2);

        add_grad( g, is[0], g0_0, g0_1, g0_2);
        add_grad( g, is[1], g1_0, g1_1, g1_2);
        add_grad( g, is[2], g2_0, g2_1, g2_2);
        add_grad( g, is[3], g3_0, g3_1, g3_2);
      }
    }
    add_alignment_obj_grad( sys, lscale, xs0, xs, g);
  }

  //############################################################
  // slow but robust for case of no satisfying starting state
  template< class I>
  void Doer<I>::satisfy_no_init( System& sys, double tol){

    auto nc = I::n_constraints( sys);

    if (nc == C_Index(0)){
      return;
    }
    else{
      auto np = I::n_particles( sys);
      
      xs0.resize( np);
      for (auto ip: range( np)){
        I::get_position( sys, ip, xs0[ip]);
      }
      
      auto lscale = avg_length_scale( sys);
      auto viol = rms_abs_violation( sys)/lscale;

      if (viol < tol){
        return;
      }
      else{
        auto np3 = 3*(np/P_Index(1));
        Lengths xs( np3), lxs0( np3);
        for( auto ip: range( np)){
          auto pos = xs0[ip];
          auto ip3 = 3*(ip/P_Index(1));
          xs[ip3 + 0] = pos[0];
          xs[ip3 + 1] = pos[1];
          xs[ip3 + 2] = pos[2];
        }
        lxs0.copy_from( xs);

        struct Minsys{
          const System& sys;
          const Lengths& xs0;
          Length lscale;
          Minsys( const System& _sys, const Lengths& _xs0, Length _lscale): sys(_sys), xs0(_xs0){
            lscale = _lscale;
          }
        };

        class Iface{
        public:
          typedef Minsys Func;
          typedef typename I::Length X;

          static
          double value( const Func& msys, const Vector< X>& xs){
            return constraint_obj( msys.sys, msys.lscale, msys.xs0, xs);
          }

          static
          void get_gradient( const Minsys& msys, const Lengths& xs, Vector< Inv_Length>& grad){
            get_constraint_obj_grad( msys.sys, msys.lscale, msys.xs0, xs, grad);
          }
        };

        auto atol = 0.1*sqrt(tol)*lscale;
        Minsys msys( sys, lxs0, lscale);
        Conjugate_Gradient_Minimize::minimize< Iface>( msys, lscale, atol, xs);

        for( auto ip: range( np)){
          auto& pos = xs0[ip];
          pos = pos_of( xs, ip);
        }

        for( auto ip: range(np)){
          I::set_position( sys, ip, xs0[ip]);
        }
      }
    }
  }
  
  //#########################################################
  template< class I>
  auto Doer<I>::cons_parts( const System& sys, C_Index ic)
     -> std::pair< size_t, Array< P_Index, 4> >{
       
    if (I::is_coplanar_constraint( sys, ic)){
      auto ac = I::coplanar_constraint_particles( sys, ic);
      return std::make_pair( 4, ac);
    }
    else{
      auto [ip0,ip1] =
                    I::length_constraint_particles( sys, ic);
      P_Index ipmx{ maxi};      
      Array< P_Index, 4> ps{ ip0,ip1,ipmx,ipmx};
      return std::make_pair( 2, ps);
    }
  }

  //########################################################
  template< class I>
  std::pair<bool, size_t> // succeeded, n_steps
  Doer< I>::do_simple_newton_loop(System &sys, double tol) {
    
    size_t nsteps = 0;
    bool success = true;
    auto lscale = avg_length_scale( sys);
    //bool initl = init;
    F.fill( Impulse( 0.0));
    while( true){
      do_simple_newton_step(sys, success);
      if (!success){
        return std::make_pair( success, nsteps);
      }
      else{
        //initl = false;
        ++nsteps;
      
        auto viol = rms_abs_violation( sys)/lscale;
        if (viol < tol){
          break;
        }
      }
    }
    return std::make_pair( success, nsteps);
  }
  
  
  //#########################################################
  template< class I>
  double Doer<I>::sys_rms_violation( const System& sys){
    
    if (I::n_constraints( sys) == C_Index(0)){
      return 0.0;
    }
    
    auto np = I::n_particles( sys);
    xs0.resize( np);
    
    for( auto ip: range( np)){
      I::get_position( sys, ip, xs0[ip]);
    }
       
    return rms_abs_violation( sys)/avg_length_scale( sys);   
  }

  //################################################################
  template< class I>
  void Doer< I>::satisfy_simple( System& sys, double tol, size_t& n_steps){
          
    n_steps = 0;
    //bool init = true;
    //auto lscale = avg_length_scale( sys);
    typedef typename I::Constraint_Index CIndex;
    if (I::n_constraints( sys) > CIndex(0)){
      auto np = I::n_particles( sys);
      xs0.resize( np);
      old_xs0.resize( np);
      
      for( auto ip: range( np)){
        I::get_position( sys, ip, xs0[ip]);
      }

      setup( sys);
      setup_ctdc_matrix( sys);
      
      bool factor_success;
      ctdc_matrix.factor( factor_success);
      if (!factor_success){
        error( "chain constraints: initial positions not consistent");
      }
      
      for( auto ip: range( np)){
        Pos x;
        I::get_position( sys, ip, x);
        xs0[ip] = x;
      }

      bool success;
      size_t dnsteps;
      std::tie( success, dnsteps) = do_simple_newton_loop( sys, tol);
      //init = false;
      n_steps += dnsteps;
      
      if (!success){
        error( "newton step didnt work");
      }
      
      ctdc_matrix.reset();
   
      for( auto ip: range( np)){
        I::set_position( sys, ip, xs0[ip]);
      }
    }
  }

  
  //##################################
  template< class I>
  auto Doer<I>::copy() const -> Doer{
    Doer doer;
    doer.C = C.copy();
    doer.cons_nebs = cons_nebs.copy();
    doer.cons_of_parts = cons_of_parts.copy();
    doer.ctdc_matrix = ctdc_matrix.copy();
    doer.F = F.copy();
    doer.xs0 = xs0.copy();
    doer.mPhi = mPhi.copy();
    
    return doer;
  }
  
  //########################################################
  template< class I>
  auto Doer<I>::coplanar_scale( const System& sys, C_Index ic) const -> Length{
    
    auto xs = coplanar_constraint_positions( sys, ic);
    auto x0 = xs[0];
    auto ldx1 = xs[1] - x0;
    auto ldx2 = xs[2] - x0;
    auto ldx3 = xs[3] - x0;
    return (norm( ldx1) + norm( ldx2) + norm( ldx3))/3.0;
  }
  
  //################################################################
  template< class I>
  auto Doer<I>::scales_for_coplanar( const System& sys) -> Vector< Length, C_Index>{
    
    auto nc = I::n_constraints( sys);
    Vector< Length, C_Index> length_scales( nc);
    
    for (auto ic = C_Index(0); ic < nc; ic++){
      if (I::is_coplanar_constraint( sys, ic)){   
        length_scales[ic] = coplanar_scale( sys, ic);
      }
      else{
        length_scales[ic] = I::constraint_length( sys, ic); 
      }
    }
    
    return length_scales;
  }

  //##########################################################
  template< class I>
  void
  Doer<I>::setup_length( const System& sys, C_Index ic){
        
    auto [x0,x1] = length_constraint_positions( sys, ic);
    auto d = x1 - x0;
    auto r2 = dot( d,d);
    
    Length r0 = I::constraint_length( sys, ic);   
    Length cons = 0.5*( r2 - r0*r0)/r0;
    mPhi[ic] = -cons;
    
    PI_Index i0{0}, i1{1};
    
    C[ic][i0] = -d/r0;
    C[ic][i1] =  d/r0;
  }
  
  //##################################################
  // eps_kij x_k
  template< class T>
  Mat3< T> eps_vec( Vec3< T> x){
    
    Mat3< T> res( Zeroi{});
    res[0][1] = x[2]; 
    res[1][2] = x[0];
    res[2][0] = x[1];
    
    res[1][0] = res[0][1];
    res[2][1] = res[1][2];
    res[0][2] = res[2][0];
    
    return res;
  }
  
  //###########################################################
  template< class I>
  void Doer<I>::setup_coplanar( const System& sys, C_Index ic){
    
    auto length_scales = scales_for_coplanar( sys);
        
    auto xs = coplanar_constraint_positions( sys, ic);
    
    auto x0 = xs[0];
    auto ldx1 = xs[1] - x0;
    auto ldx2 = xs[2] - x0;
    auto ldx3 = xs[3] - x0;

    // perp vectors
    auto inv_scale2 = 1.0/sq( length_scales[ic]);
    
    auto p2 = inv_scale2*cross( ldx3, ldx1);
    auto p3 = inv_scale2*cross( ldx1, ldx2);
    auto p1 = inv_scale2*cross( ldx2, ldx3);
    auto p0 = -(p1 + p2 + p3);

    mPhi[ic] = -inv_scale2*dot( cross( ldx1,ldx2), ldx3);
    
    const PI_Index i0( 0), i1( 1), i2( 2), i3( 3);
    auto& Cic = C[ic];      
    Cic[i0] = p0;
    Cic[i1] = p1;
    Cic[i2] = p2;
    Cic[i3] = p3;
  }

//#####################################################
template< class I>
void Doer<I>::setup_ctdc_matrix( const System& sys){
   
  ctdc_matrix.reset(); 
  ctdc_matrix.zero();
  
  auto nc = I::n_constraints( sys);
  for( auto ic: range( nc)){
    auto& icol = C[ic];
    
    auto [nip, ips] = cons_parts( sys, ic);
    
    for( auto jc: cons_nebs[ic]){
    
      auto& jcol = C[jc];     
      auto [njp, jps] = cons_parts( sys, jc);
      
      for( auto ki: range( nip)){
        P_Index ip = ips[ki];
        for (auto kj: range( njp)){
          P_Index jp = jps[kj];
          if (jp == ip){
            auto& subrowi = icol[ PI_Index{ ki}];
            auto& subrowj = jcol[ PI_Index{ kj}];
            Mobility mob = I::mobility( sys, ip); 
            auto dotp = dot( subrowi, subrowj);
            ctdc_matrix( ic, jc) += mob*dotp;              
          }
        }
      }
    }
  }
}

//#####################################################
template< class I>
void Doer<I>::update_positions( const System& sys,
                               const Vector< Impulse, C_Index>& sl){

  old_xs0.copy_from( xs0);

  auto nc = I::n_constraints( sys);
  for( auto ic: range( nc)){
    auto& Cic = C[ic];
    auto Fic = sl[ic];
    auto [nip, ips] = cons_parts( sys, ic);

    Array< Impulse, 3> force( Zeroi{});
    Array< decltype( Length2()/Mobility()), 3> torque( Zeroi{});

    for( auto k: range( nip)){
      auto ip = ips[k];
      PI_Index iip( k);
      auto D = I::mobility( sys, ip);
      auto dp = (D*Fic)*Cic[ iip];
      force += dp/D;
      torque += cross( old_xs0[ip], dp/D);
      xs0[ip] += dp;
    }
  }
}
}



