#pragma once

#include "../../lib/units.hh"
#include "../../lib/array.hh"

namespace Browndye{

template< class I, class F>
void partition_contents_ext( const typename I::Container& cont, const typename I::Refs& refs, const F& f,
                             typename I::Refs& refs0, typename I::Refs& refs1){

  typedef typename I::Index Index;
  auto n = refs.size();
  auto n1 = Index(0);
  for( uauto i: range( n)){
    auto pos = I::position( cont, refs, i);
      if (f(pos)) {
          ++n1;
      }
  }
  refs1.resize( n1);
  refs0.resize( n - (n1 - Index(0)));

  auto i0 = Index(0);
  auto i1 = Index(0);
  for( uauto i: range( n)){
    auto pos = I::position( cont, refs, i);
    if (f( pos)){
      refs1[i1] = refs[i];
      ++i1;
    }
    else{
      refs0[i0] = refs[i];
      ++i0;
    }
  }
}
}
