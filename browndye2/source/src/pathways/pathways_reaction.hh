#pragma once

/*
 * pathways_reaction.hh
 *
 *  Created on: Oct 15, 2015
 *      Author: root
 */


#include "pathways_criterion.hh"

namespace Browndye{
namespace Pathways{

  template< class I>
  class Reaction{
  public:
    typedef typename I::Rxn_Index Rxn_Index;
    typedef typename I::State_Index State_Index;
    typedef typename I::Mol_Index Mol_Index;
    typedef typename I::Molecule_Info Molecule_Info;
    
    Rxn_Index n{ maxi};
    std::string name;
    Criterion<I> criterion;
    State_Index state_before{ maxi}, state_after{ maxi};
    std::string state_before_name, state_after_name;

    Reaction( const Molecule_Info&, JP::Node_Ptr);

    Reaction() = default;
    Reaction( const Reaction&) = delete;
    Reaction& operator=( const Reaction&) = delete;
    Reaction( Reaction&&) = default;
    Reaction& operator=( Reaction&&) = default;
    ~Reaction() = default;

  };

  // constructor
  template< class I>
  Reaction<I>::Reaction( const Molecule_Info& minfo, JP::Node_Ptr node):
          criterion( minfo, checked_child( node, "criterion"), Mol_Index(maxi),Mol_Index(maxi)){
    
    name = checked_value_from_node< std::string>( node, "name");
    state_before_name = checked_value_from_node< std::string>( node, "state_before");
    state_after_name = checked_value_from_node< std::string>( node, "state_after");
  }
}
}
