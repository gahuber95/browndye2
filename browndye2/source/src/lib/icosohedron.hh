#pragma once

#include "vector.hh"
#include "array.hh"

namespace Browndye{

  Vector< Vec3< double> > icosohedron_tri_cens( int order);
}
