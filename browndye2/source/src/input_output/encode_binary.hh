#pragma once

/*
Returns a MIME (Base64 format) string representation of the array of floats.
This is the inverse of the Ocaml code found in aux/decode_binary.ml
*/

#include <string>
#include "../lib/vector.hh"

namespace Browndye{

std::string
string_of_floats( const Vector< double>& floats, unsigned int nf);

}

