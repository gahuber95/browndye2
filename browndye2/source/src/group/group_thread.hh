#pragma once
/*
 * group_thread.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include <map>
#include "bd_constrainer.hh"
#include "../core_and_chain/molcore/core_thread.hh"
#include "saved_group_state.hh"
#include "bd_constrainer.hh"

namespace Browndye{

class Group_Common;
class System_Thread;

//#################################################
class Group_Thread{
public:
  Group_Thread() = delete;
  Group_Thread( const Group_Thread&) = delete;
  Group_Thread( Group_Thread&&) = default;
  Group_Thread& operator=( const Group_Thread&) = delete;
  Group_Thread& operator=( Group_Thread&&) = delete;
  ~Group_Thread() = default;
    
  Group_Thread( const Group_Common&, const System_Thread&);
  
  // data
  const Group_Common& common;
  const System_Thread& system_thread;
  // includes unused Molecule 0 on Group 0
  Vector< Core_Thread, Core_Index> core_threads;
  
  //typedef Constraint_Info_And_Doer CIDoer;
  
  struct CID{
    Constraints_Info cinfo;
    std::unique_ptr< Constrainer> doer;
  };
  
  std::map< std::vector<bool>, CID> constrainers;
  
  Saved_Group_State current;
};
}

