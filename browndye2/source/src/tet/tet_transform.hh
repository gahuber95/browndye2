#pragma once
#include <cassert>
#include "../transforms/transform.hh"
#include "../transforms/pure_translation.hh"
#include "../lib/linalg.hh"
#include "../lib/rotations.hh"
#include "v4.hh"
#include "centroid.hh"

// probably not needed

namespace Browndye{

// best fit between two tetrahedra
// returns transform to transform tet0 into tet1
Transform tform_of_tet( const V4< Pos>& tet0,
                         const V4< Pos>& tet1);


//##################################################
// move to its own header
template< class V, class W>
Transform weighted_alignment_tform( const V& xsa, const V& xsb,
                                     const W& wts){
    
  auto cena = weighted_centroid( xsa, wts);
  auto cenb = weighted_centroid( xsb, wts);
  
  typedef typename W::value_type Wt;
  typedef decltype( Length2()*Wt()) Mt;
  
  Mat3< Mt> M( Zeroi{});
  
  auto n = xsa.size();
  assert( n == xsb.size());
  assert( n == wts.size());
  
  for( auto i: range(n)){
    auto wt = wts[i];
    M += outer( wt*(xsa[i] - cena), xsb[i] - cenb);
  }
  
  constexpr size_t X = 0, Y = 1, Z = 2;
    
  SMat< Mt, 4,4> N;
  N[0][0] = M[X][X] + M[Y][Y] + M[Z][Z];
  N[1][0] = M[Y][Z] - M[Z][Y];
  N[2][0] = M[Z][X] - M[X][Z];
  N[3][0] = M[X][Y] - M[Y][X];
  
  N[1][1] = M[X][X] - M[Y][Y] - M[Z][Z];
  N[2][1] = M[X][Y] + M[Y][X];
  N[3][1] = M[Z][X] + M[X][Z];
  
  N[2][2] = -M[X][X] + M[Y][Y] - M[Z][Z];
  N[3][2] =  M[Y][Z] + M[Z][Y];
  
  N[3][3] = -M[X][X] - M[Y][Y] + M[Z][Z];
  
  for( auto ir: range(0,4)){
    for( auto ic: range( ir+1,4)){
      N[ir][ic] = N[ic][ir];
    }
  }
          
  auto evecs = Linalg::eigensystem( N).first;
      
  auto quat = evecs[3]; 
  auto rot = mat_of_quat( quat);
  
  Pure_Translation transa( -cena), transb( cenb);
  Transform tformr( rot, Pos( Zeroi{}));
  
  return transb*tformr*transa;
}

}
