#include "we_simulator.hh"
#include "we_simulator_interface.hh"
#include "we_thread_runner.hh"

// perhaps move some of this functionality to WE_Simulator

namespace Browndye{

typedef std::lock_guard< std::mutex> Lock;

//###########################################################
template< class F>
void WE_Interface::apply_to_object_refs( F& f, WE_Simulator& sim){
    
  for( auto& state: sim.states){
    f( state);
  }
}
 
//########################################################### 
size_t WE_Interface::number_of_threads( WE_Simulator& sim){
  return sim.mthreads.size()/Thread_Index(1);
}

//###########################################################
// used in bin building
void WE_Interface::step_objects_forward( WE_Simulator& sim){
  
  for( auto& item: sim.states){
    auto& state = *(item.second);
    sim.thread_runner->step_state( state, Thread_Index(0));
  }
}

//#############################################################
std::optional< size_t>
WE_Interface::bin( WE_Simulator& sim, Sys_Copy_Index icp){
  
  auto& state = *sim.states.at( icp);
  if (!sim.bin_partitions.empty()){
    Length rxn_coord = state.rxn_coord();
    return found( sim.bin_partitions, 
                  rxn_coord/(state.common().q_rad));
  }
  else {
    return std::nullopt;
  }
}

//############################################################
Sys_Copy_Index WE_Interface::new_index( WE_Simulator& sim){
  
  auto& unused = sim.unused_indices;
  
  Sys_Copy_Index icp;  
  if (unused.empty()){
    //icp = sim.n_states();
    icp = sim.max_idx;
    ++sim.max_idx;
  }
  else{
    icp = unused.top();
    unused.pop();
  }
  
  return icp;
}

//#######################################################
Sys_Copy_Index WE_Interface::new_object( WE_Simulator& sim){
  
  auto& states = sim.states; 
  auto icp = new_index( sim);
  
  auto ith = sim.thread_index( icp);
  auto state0 = std::make_unique< System_State>( sim.mthreads[ith], icp);
  auto ires = states.insert( std::make_pair( icp, std::move( state0)));
  if (!ires.second){
    error( __FILE__, __LINE__, "insertion failed");
  }
  
  auto& new_state = states.at( icp);
  initialize_object( *new_state); 
  
  new_state->wt = 1.0;
  return icp;
}

//##################################################################
// new objects get the same weight
void WE_Interface::get_duplicated_objects( 
                                          WE_Simulator& sim,
                                          Sys_Copy_Index icp,
                                          size_t n_copies, 
                                          std::list< Sys_Copy_Index>& copies){
  
  auto& states = sim.states;
  auto& state = *(states.at( icp));
  double wt = state.wt; ///(1 + n_copies);
  for( auto i: range( n_copies)){
    (void)i;
    
    auto jcp = new_index( sim);
    auto ptr = std::make_unique< System_State>( state);
    auto ires = states.insert( std::make_pair( jcp, std::move( ptr)));
    if (!ires.second){
      error( __FILE__, __LINE__, "insertion failed");
    }
    
    auto& new_state = *(states.at( jcp));
    new_state.wt = wt;
    new_state.number = jcp;
    
    copies.push_back( jcp);
  }
  state.wt = wt;
  
  // a test, debug
  {
    std::set< Sys_Copy_Index> cindices;
    std::set< const System_State*> cptrs;
    for (auto& item: states){
      auto i = item.first;
      auto p = &(*item.second);
      auto resi = cindices.insert( i);
      if (!resi.second){
        error( "index double insertion",i);
      }
      
      auto resp = cptrs.insert( p);
      if (!resp.second){
        error( "ptr double insertion", p);
      }
    }
  }
}

//################################################################
void WE_Interface::remove( WE_Simulator& sim, Sys_Copy_Index icp){
   
  auto& states = sim.states;
  auto itr = states.find( icp);
  if (itr == states.end()){
    error(__FILE__, __LINE__, "nothing to remove");
  }
  
  states.erase( itr);
  sim.unused_indices.push( icp);
}

//###########################################################
const Rxn_Index r1(1);

std::optional< size_t>
WE_Interface::crossing( WE_Simulator& sim, Sys_Copy_Index icp){
    
  auto& state = sim.states.at( icp);  
  auto fate = state->fate;
  bool had_reaction = state->had_reaction;
  bool crossed = had_reaction || fate == System_Fate::Escaped;

  if (crossed){
    if (had_reaction){
      return state->last_rxn/r1;
    }
    else if (fate == System_Fate::Escaped){
      state->fate = System_Fate::In_Action;
      return sim.n_final_rxns/r1;
    }
    else{
      error( "not get here", __FILE__, __LINE__);
      return std::nullopt;
    }
  }
  else{
    return std::nullopt;
  }
}

//###########################################################
size_t WE_Interface::number_of_bins( WE_Simulator& sim){
  return sim.bin_partitions.size();
}

//###########################################################
size_t WE_Interface::number_of_bin_neighbors( WE_Simulator& sim, size_t bin){
  if ((bin == 0) || (bin == sim.bin_partitions.size() - 1)){
    return 1;
  }
  else{
    return 2;
  }
}

//###########################################################
// low bins are closer to reaction
bool WE_Interface::have_population_reversal([[maybe_unused]] WE_Simulator& sim, size_t bin0, double wt0, size_t bin1, double wt1){
  bool res = (((int)bin0 - (int)bin1)*(wt0 - wt1) <= 0.0); 
  return res;
}

//###########################################################
// rxn coordinate is normalized by q-radius
double WE_Interface::reaction_coordinate( WE_Simulator& sim, Sys_Copy_Index icp){

  auto& state = sim.states.at( icp);
  if (state->fate == System_Fate::Final_Rxn){
    return 0.0;
  }
  else{
    Length rxn_coord = state->rxn_coord();
    return rxn_coord/(sim.common->q_rad);
  }
}

//########################################################################
const auto rd1 = r1 - Rxn_Index(0);

void WE_Interface::output( WE_Simulator& sim, size_t icrossing, double flux){
  ::std::ofstream& output = sim.output;
  auto ncross = sim.n_final_rxns + rd1;
  auto& next_fluxes = sim.next_fluxes;

  next_fluxes[ Rxn_Index(icrossing)] = flux;
  if (Rxn_Index( icrossing) == ncross - rd1){
    output.seekp( sim.stream_position);
    output << "    <d> ";
    for (auto i: range( ncross))
      output << next_fluxes[i] << " ";
    output << "</d>\n";
    sim.stream_position = output.tellp();
    
    output << "  </data>\n";
    output << "</rates>\n";
    output.flush();
  }
}

//##################################################
double WE_Interface::uniform( WE_Simulator& sim){
  return sim.rng.uniform();
}

//###################################################################
double WE_Interface::weight( const WE_Simulator& sim, Sys_Copy_Index icp){
   
  return sim.states.at( icp)->wt;
}

//#####################################################################
void WE_Interface::set_weight( WE_Simulator& sim, Sys_Copy_Index icp, double wt){
  
  sim.states.at( icp)->wt = wt;
}

//###############################################################
Sys_Copy_Index WE_Interface::number_of_objects(const WE_Simulator &sim){
  
  return sim.number_of_objects();
}

size_t WE_Interface::number_of_fluxes(const WE_Simulator &sim){
  
  return sim.number_of_fluxes();
}

//############################################################
void WE_Interface::initialize_object( System_State& state){
    
  state.set_initial_state_for_bb();
}

}
