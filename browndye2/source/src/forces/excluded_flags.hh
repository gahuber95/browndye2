#pragma once

#include "../lib/vector.hh"
#include "../global/indices.hh"

namespace Browndye{

void fill_in_excluded_flags( const Vector< Atom_Index>& excluded, Vector< bool, Atom_Index>& flags);

}
