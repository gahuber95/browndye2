#include "dummy_molecule.hh"
#include "../group/group_common.hh"

namespace Browndye {

    Dummy_Molecule::Dummy_Molecule(const JAM_XML_Pull_Parser::Node_Ptr& dnode,
                                   const Nonbonded_Parameter_Info &pinfo,
                                   const Group_Common& group) :

            Macro_Struct( dnode, pinfo, group.number) {

      auto mname = checked_value_from_node< std::string>( dnode, "core");
      auto& cores = group.cores;
      std::optional< Core_Index> imo;
      for( auto& core: cores){
        if (core.name == mname){
          imo = core.number;

          for( auto& atom: atoms){
            atom.pos -= core.hydro_center;
          }
        }
      }
      if (!imo){
        error( "core", mname, "not found");
      }

      im = imo.value();
    }

}