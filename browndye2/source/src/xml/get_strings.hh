#pragma once

#include <string>
#include "../lib/vector.hh"
#include "jam_xml_pull_parser.hh"

namespace Browndye{

Vector< std::string> strings_from_node( JAM_XML_Pull_Parser::Node_Ptr node, std::string tag);

}

