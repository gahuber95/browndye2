#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Various functions to handle infinitesimal and finite rotations.

Finite rotations are handled by computing and storing ahead of time
the analytical solution of
(Furry, WH, "Isotropic Rotational Brownian Motion", 
Physical Review 107 (1) 7-13 (1957))
at a few well-defined multiples of the rotational time scale,
and resolving rotations at arbitrary times by composing several
finite rotations.
*/

#include "spline.hh"
#include "array.hh"

namespace Browndye{

Mat3< double> mat_of_quat( const Array< double,4>& quat);

Array< double, 4> quat_of_mat( const Mat3< double>& mat);

Array< double, 4> quat_product( const Array< double,4>& q0,
                                  const Array< double,4>& q1);

Array< double,4> inverted_quat( const Array< double,4>& q);

Array< double, 4> quat_of_dom( const Vec3< double>& dom);

template< class I>
Array< double,4>
diffusional_rotation( typename I::Random_Number_Generator& rng, 
                               double t);

template< class I>
Array< double, 4> random_quat( typename I::Random_Number_Generator& rng){
  
  Array< double, 4> quat;
  double qnm = 0.0;
  for( unsigned int i=0; i<4; i++){
    quat[i] = I::gaussian( rng);
    qnm += quat[i]*quat[i];
  }  
  qnm = sqrt( qnm);
  
  for( unsigned int i=0; i<4; i++){
    quat[i] /= qnm;
  }
  
  return quat;
}

template< class I>
Mat3< double> random_rotation( typename I::Random_Number_Generator& rng){
  
  auto quat = random_quat<I>( rng);
  return mat_of_quat( quat);
}

template< class I>
Vec3< double> random_unit_vector( typename I::Random_Number_Generator& rng){
  double unm = 0.0;
  Vec3< double> u;
  for( unsigned int i=0; i<3; i++){
    u[i] = I::gaussian( rng);
    unm += u[i]*u[i];
  }  
  unm = std::sqrt( unm);

  for( unsigned int i=0; i<3; i++)
    u[i] /= unm;
  return u;
}

namespace Rotations{
  const Spline< double, double>& rprob0p5(); 
  const Spline< double, double>& rprob1p0(); 
  const Spline< double, double>& rprob2p0(); 

  template< class I>
  Array< double, 4> spline_rot( typename I::Random_Number_Generator& rng, 
                       const Spline< double, double>& rprob){
    
    double pc = I::uniform( rng);
    double phi = rprob.value( pc); 
    Vec3< double> u = random_unit_vector<I>( rng);
    auto dom = phi*u;
    return quat_of_dom( dom);
  }
}

//################################################################
template< class I>
Array< double, 4>
diffusional_rotation( typename I::Random_Number_Generator& rng, double t){
  
  using namespace Rotations;

  auto split_rot = [&]( double tlim, auto spfun){
    auto quat0 = spline_rot<I>( rng, spfun());    
    auto quat1 = diffusional_rotation<I>( rng, t - tlim);
    return quat_product( quat1, quat0);
  };

  if (t < 1.0){
    
    if (t <= 0.25){
      // take infinitesimal step
      double sqdt = std::sqrt( 2*t);
      Vec3< double> dom;
      dom[0] = sqdt*I::gaussian( rng);
      dom[1] = sqdt*I::gaussian( rng);
      dom[2] = sqdt*I::gaussian( rng);
      return quat_of_dom( dom);
    }
    else if (t < 0.5){
      auto quat0 = diffusional_rotation<I>( rng, 0.25);
      auto quat1 = diffusional_rotation<I>( rng, t - 0.25);
      return quat_product( quat1, quat0);
    }
    else { // t < 1.0
      return split_rot( 0.5, rprob0p5);
    }
  }
  
  else{ // t >= 1.0
    
    if (t < 2.0){
      return split_rot( 1.0, rprob1p0);
    }
    else if (t < 4.0){
      return split_rot( 2.0, rprob2p0);
    }
    else{
      // past 4 time constants, it is effectively random.
      return random_quat<I>( rng);
    }
  }
}

//##############################################################
template< class I>
Mat3< double> added_diffusional_rotation( typename I::Random_Number_Generator& rng, 
                               double t, 
                               const Mat3< double>& rot0){
  
  Array< double,4> quat;
  auto quat0 = quat_of_mat( rot0);
  
  auto quat1 = diffusional_rotation<I>( rng, t);
  quat = quat_product( quat1, quat0);
  return mat_of_quat( quat);
}
}

