#pragma once

#include <memory>
#include "../global/limits.hh"
#include "../lib/units.hh"
#include "../lib/array.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../lib/spline.hh"
#include "../global/pi.hh"
#include "../lib/sq.hh"

namespace Browndye{

struct MM_Bonded_Parameter_Info;
struct CD_Bonded_Parameter_Info;

class Bond_Type_Index_Tag{};
typedef Index< Bond_Type_Index_Tag> Bond_Type_Index;

class Angle_Type_Index_Tag{};
typedef Index< Angle_Type_Index_Tag> Angle_Type_Index;

class Dihedral_Type_Index_Tag{};
typedef Index< Dihedral_Type_Index_Tag> Dihedral_Type_Index;

class MM{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  struct Bond_Type{
    Bond_Type() = delete;
    Bond_Type( MM_Bonded_Parameter_Info&, Node_Ptr);
    
    Spring_Constant kr{ NaN};
    Length req{ NaN};
    
    Energy V( Length r) const{
      return 0.5*kr*sq( req - r);
    }
    
    Force mdVdr( Length r) const{
      return kr*( req - r);
    }
    
    Spring_Constant d2Vdr2( [[maybe_unused]] Length r) const{
      return kr;
    }
  };
  
  //#####################################################3
  struct Angle_Type{
    Angle_Type() = delete;
    Angle_Type( MM_Bonded_Parameter_Info&, Node_Ptr);
    
    double theq{ NaN};
    Energy kth{ NaN};
    
    Energy V( double theta) const{
      return 0.5*kth*sq( theq - theta);
    }
    
    Energy mdVdth( double theta) const{
      return kth*( theq - theta);
    }
      
    Energy d2Vdth2( [[maybe_unused]] double theta) const{
      return kth;
    }
    
    Energy d2Vda2( double theta) const{
      return d2Vdth2( theta);
    }
    
  };
  
  struct Dihedral_Type{
    Dihedral_Type() = delete;
    Dihedral_Type( MM_Bonded_Parameter_Info&, Node_Ptr);
    
    static constexpr size_t maxnc = 4;
    size_t nc = 0;
    Array< double, maxnc> gammas;
    Array< size_t, maxnc> ns;
    Array< Energy, maxnc> Vs;
    Dihedral_Type_Index index;
    //bool is_proper; // needed?
    
    Energy V( double phi) const{
      Energy sum{ 0.0};  
      for( auto ic: range( nc)){
        // check on 0.5 factor, sign of gamma
        auto kc = ns[ic];
        double sarg = kc*phi - gammas[ic];
        sum += 0.5*Vs[ic]*( 1.0 + cos( sarg));
      }
      return sum;
    }
    
    Energy mdVdph( double phi) const{
      Energy sum{ 0.0};  
      for( auto ic: range( nc)){
        // check on 0.5 factor, sign of gamma
        auto kc = ns[ic];
        double sarg = kc*phi - gammas[ic];
        sum += (0.5*kc)*Vs[ic]*sin( sarg);
      }
      return sum;
    }
  
    Energy d2Vdph2( double phi) const{
      Energy sum{ 0.0};  
      for( auto ic: range( nc)){
      // check on 0.5 factor, sign of gamma
        auto kc = ns[ic];
        double sarg = kc*phi - gammas[ic];
          sum -= (0.5*kc*kc)*Vs[ic]*cos( sarg);
      }
      return sum;
    }
    
    Energy d2Vda2( double phi) const{
      return d2Vdph2( phi);
    }
    
  };

};

//######################################################
class Broken_Bond_Exception: std::runtime_error{
public:
  Broken_Bond_Exception( const char* msg): std::runtime_error( msg){}
};

class CD{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
    
  // this is for the non-bonded adjacent residue interactions
  struct Bond_Type{
    Bond_Type() = delete;
    Bond_Type( CD_Bonded_Parameter_Info&, Node_Ptr);
    
    std::unique_ptr< Even_Spline< Length, Energy, false> > Vs;
    
    Force mdVdr( Length r) const{
      check_for_break( r);
      return -Vs->first_deriv( r);      
    }
    
    Energy V( Length r) const{
      return Vs->value( r);
    }
    
    Spring_Constant d2Vdr2( Length r) const{
      check_for_break( r);
      return Vs->second_deriv( r);
    }
    
    void check_for_break( Length r) const{
      auto b = Vs->high_bound();
      if (r >= b){
        //error( "bond broken ", r, b);
        //throw Jam_Exception( "bond broken");
        throw Broken_Bond_Exception{""};
      }
    }
  };
  
  struct Angle_Type{
    Angle_Type() = delete;
    Angle_Type( CD_Bonded_Parameter_Info&, Node_Ptr);
    
    std::unique_ptr< Even_Spline< double, Energy, false> > Vs;
    
    Energy V( double theta) const{
      return Vs->value( theta);
    }
    
    Energy mdVdth( double theta) const{
      return -Vs->first_deriv( theta);
    }
    
    Energy d2Vdth2( double theta) const{
      return Vs->second_deriv( theta);
    }        
    
    Energy d2Vda2( double theta) const{
      return d2Vdth2( theta);
    }
    
  };
  
  struct Dihedral_Type{
    Dihedral_Type() = delete;
    Dihedral_Type( CD_Bonded_Parameter_Info&, Node_Ptr);
    
    double adjusted_angle( double phi) const{
      if (phi < Vs->low_bound())
        return phi + 2*pi;
      else
        return phi;
    }
    
    Dihedral_Type_Index index;
    std::unique_ptr< Even_Spline< double, Energy, false> > Vs;
    
    Energy mdVdph( double phi) const{
      return -Vs->first_deriv( adjusted_angle( phi));
    }
    
    Energy d2Vdph2( double phi) const{
      return Vs->second_deriv( adjusted_angle( phi));
    }
    
    Energy d2Vda2( double phi) const{
      return d2Vdph2( phi);
    }
    
    Energy V( double phi) const{
      return Vs->value( adjusted_angle( phi));
    }
    
  };
  
};

}

