#pragma once

#include "node_checker.hh"

namespace Browndye{
  void check_chain_input( JAM_XML_Pull_Parser::Node_Ptr node);
  
}