#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
#include "../xml/check_bdtop_nodes.hh"
#include "../forces/other/blank_nonbonded_parameter_info.hh"
#include "../forces/electrostatic/single_grid.hh"
#include "../lib/bool_of_string.hh"
#include "../global/defaults.hh"
#include "orchestrator.hh"

namespace {

using namespace Browndye;

namespace OC = Orchestrator;
namespace JP = JAM_XML_Pull_Parser;
using std::string;
using std::move;
using std::pair;
using std::make_pair;
using std::make_tuple;
using std::tie;
using std::get;
using std::ofstream;
using std::to_string;
using std::optional;
using JP::Node_Ptr;
using OC::Source;
using OC::Source_From_File;

using OC::new_source_from_file;
using OC::new_source_from_xml_file;

typedef Vector< Source> Sources;

struct Solvent_Sources{
  OC::Source_From_XML_File< double>
    dielectric, debye_length, desolvation, solvent_radius;
};

struct Core_Sources{
  Source hydro, cheby, surf_atoms;
  optional< Source_From_File> mpole_opt;
  Node_Ptr node = nullptr;
  std::unique_ptr< Source> c2_cheby;
  Vector< Source> born_sources;
};

struct Group_Sources{
  Vector< Core_Sources> core_sources;
  Vector< Source_From_File> chain_sources;
  Node_Ptr node = nullptr;
};

class BD_Topper{
public:
  
  static
  void main( const string& filename);
  
private:
  
  static
  Vector< Group_Sources>
  group_sources( optional< Source_From_File>& rxn_source, Node_Ptr,
                       Solvent_Sources& solvent_sources);
  
  static
  Vector< Core_Sources> core_sources( optional< Source_From_File>&,
                                       Node_Ptr,
                                      Solvent_Sources& solvent_sources);
  
  static
  Vector< Source_From_File>
  chain_sources_of_group( const Node_Ptr&, const Node_Ptr&);
  
  static
  string overall_name_from_groups(const Node_Ptr &groups_node);
  
  static
  string generated_solvent_file(const Node_Ptr &top, const string& overall_name);
  
  static
  optional< pair< Source_From_File, Source_From_File> >
  smallest_and_largest_grid_sources( Node_Ptr mnode);
  
  static
  optional< Vector< Source_From_File> > grid_sources( const Node_Ptr& mnode);
  
  static
  pair< Pos, Pos> atom_bounds_from_files( const string& atoms_file);
  
  static
  pair< int, int> min_and_max_grids( const Vector< string>& grid_names, pair< Pos, Pos> atom_bounds);
  
  static
  pair< Pos, Pos> grid_corners(  const string& grid_name);
  
  static
  void output_core_info( ofstream& out, int indent, const Core_Sources&, bool has_desolvation);
  
  static
  void output_chain_info(ofstream& out, int n_indent, const Node_Ptr &cnode);

  static
  void output_dummy_info( ofstream& out, int n_indent, const Node_Ptr &dnode);

  static
  void output_group_info( ofstream& out, int n_indent, const Group_Sources&, bool has_desolvation);
  
  static
  string mpole_name( const string&);
  
  static
  string hydro_params_name( const string&);
  
  static
  string cheby_name( const string&);
  
  static
  string cheby2_name( const string&);
  
  static
  string born_name( const string&);
  
  static
  string chain_name( const string&);
  
  template< class T>
  static
  void print_with_tag( ofstream&, int n_indent, const string& tag, T arg);
};

bool a_bounds_b( Pos alo, Pos ahi, Pos blo, Pos bhi);

Length3 box_volume( Pos lo, Pos hi);

template< class T>
Vector< T> vector_of_list( const std::list< T>&);
}

//########################################
int main( int argc, char *argv[]){
  
  if (argc == 0){
    Browndye::error( "need input file");
  }
  std::string filename( argv[1]);
  BD_Topper::main( filename);
}

//########################################
namespace {
  
using namespace Browndye;  
  
//typedef Vector< string> Tags;
     
void BD_Topper::main( const string& filename){
  
  std::ifstream input( filename);
  JP::Parser parser( input, filename);
  auto top_node = parser.top();
  top_node->complete();
  
  check_bdtop_input( top_node);
  
  auto system_node = top_node->child( "system");   
  if (!system_node){
    error( "\"system\" tag not found");
  }
     
  auto rxn_file_opt = value_from_node< string>( system_node, "reaction_file");
  auto rxn_source_opt = [&]() -> optional< Source_From_File>{
    if (rxn_file_opt.has_value()){
      auto& rxn_file = rxn_file_opt.value(); 
      return new_source_from_file( rxn_file);
    }
    else{
      return std::nullopt;
    }
  }();
  
  auto overall_name = overall_name_from_groups( system_node);
  auto solvent_file = generated_solvent_file( system_node, overall_name);
  
  auto dielectric_source =
     new_source_from_xml_file< double>( solvent_file, {"dielectric"});
  
  auto debye_length_source =
    new_source_from_xml_file< double>( solvent_file, {"debye_length"});
  
  auto desolvation_source =
    new_source_from_xml_file< double>( solvent_file, {"desolvation_parameter"});
  
  auto solvent_radius_source =
    new_source_from_xml_file< double>( solvent_file, {"solvent_radius"});
  
  Solvent_Sources
    solvent_sources{ dielectric_source, debye_length_source,
                      desolvation_source, solvent_radius_source};
  
  auto gsources = group_sources( rxn_source_opt, system_node, solvent_sources);
  
  for( auto& gsource: gsources){
    auto& core_sources = gsource.core_sources;
    auto& chain_sources = gsource.chain_sources;
    
    for( auto& core_source: core_sources){
      core_source.hydro.update();
      core_source.cheby.update();
      core_source.surf_atoms.update();
      if (core_source.mpole_opt){
        auto mpole = core_source.mpole_opt.value();
        mpole.update();
      }
      
      for( auto& bsource: core_source.born_sources){
        bsource.update();
      }
      
      if( core_source.c2_cheby){
        core_source.c2_cheby->update();
      }
    }
    
    for( auto& chain_source: chain_sources){
      chain_source.update();
    }
  }
  
  auto sim_file_name = overall_name + "_simulation.xml";
  ofstream out( sim_file_name);
  out << "<top>\n";
  
  auto reprint = [&]( const string& tag){
    auto opt = value_from_node< string>( top_node, tag);
    if (opt){
      out << "  <" << tag << "> " << *opt << " </" << tag << ">\n";
    }
  };
  
  Vector< string> tags{ 
  "n_threads", 
  "seed", 
  "output", 
  "n_trajectories", 
  "n_trajectories_per_output", 
  "trajectory_file",
  "max_n_steps", 
  "n_copies", 
  "n_bin_copies", 
  "n_steps",
  "n_steps_per_output",
  "min_rxn_dist_file",
  "n_we_steps_per_output", 
  "bin_file"};
  
  for( auto& tag: tags){
    reprint( tag);
  }
  out << "  <system>\n";
  
  auto sys_reprint = [&]( const string& tag){
    auto opt = value_from_node< string>( system_node, tag);
    if (opt){
      out << "    <" << tag << "> " << *opt << " </" << tag << ">\n";
    }
  };
  Vector< string> sys_tags{
    "force_field", "parameters", "start_at_site", "b_radius",
    "density_field", "atom_weights", "hydrodynamic_interactions",
      "n_steps_between_hi_updates"};
  
  for( auto& tag: sys_tags){
    sys_reprint( tag);
  }
  out << "    <solvent_file> " << solvent_file << " </solvent_file>\n";
  
  if (rxn_file_opt.has_value()){
    auto& rxn_file = rxn_file_opt.value();
    out << "    <reaction_file> " << rxn_file << " </reaction_file>\n";
  }
  
  auto tnode = system_node->child( "time_step_tolerances");
  if (tnode){
    out << "    <time_step_tolerances>\n";
    
    auto lreprint = [&]( const string& tag){
      auto opt = value_from_node< string>( tnode, tag);
      if (opt){
        out << "      <" << tag << "> " << *opt << " </" << tag << ">\n";
      }
    };
    
    Vector< string> ltags{ "constant_dt", "minimum_core_dt", "minimum_core_reaction_dt","minimum_chain_dt","minimum_chain_reaction_dt"};
    for( auto& tag: ltags){
      lreprint( tag);
    }
    out << "    </time_step_tolerances>\n";
  }
  
  auto rssnode = system_node->child( "restraints");
  if (rssnode){
    
    out << "    <restraints>\n";
    auto rsnodes = rssnode->children_of_tag( "restraint");
    for( auto& rsnode: rsnodes){
      
      auto rp = [&]( const string& tag){
        auto opt = value_from_node< string>( rsnode, tag);
        if (opt){
          out << "        <" << tag << "> " << *opt << " </" << tag << ">\n";
        }
      };
      
      out << "      <restraint>\n";
      rp( "length");
      rp( "group0");
      rp( "core0");
      rp( "chain0");
      rp( "atom0");
      rp( "group1");
      rp( "core1");
      rp( "chain1");
      rp( "atom1");
      out << "      </restraint>\n";
    }
    out << "    </restraints>\n";
  }
  
  bool has_desolvation = (desolvation_source.result() > 0.0);
  auto gnodes = system_node->children_of_tag( "group");
  for( auto& gsource: gsources){
    output_group_info( out, 4, gsource, has_desolvation);
  }
  out << "  </system>\n";
  out << "</top>\n";
}

//################################################################
string from_node( const Node_Ptr& node, const string& tag){
  return checked_value_from_node< string>( node, tag);
}

//################################################################
void BD_Topper::output_chain_info(ofstream& out, int n_indent,
                                  const Node_Ptr &cnode){
  
  auto psp = [&](){
    for( int i = 0; i < n_indent; i++){
      out << " ";
    }
  };
   
  psp();
  auto name = from_node( cnode, "name");
  out << "<chain> " << chain_name( name) << " </chain>\n";
}

//#################################################################
void BD_Topper::output_dummy_info( ofstream& out, int n_indent,
                                  const Node_Ptr &dnode){

  auto psp = [&](){
    for( int i = 0; i < n_indent; i++){
      out << " ";
    }
  };

  psp();
  out << "<dummy>\n";

  auto ptag = [&]( const string& tag){
    auto text = from_node( dnode, tag);
    psp();
    out << "  <" << tag << "> " << text << " </" << tag << ">\n";
  };

  ptag( "name");
  ptag( "atoms");
  ptag( "core");

  psp();
  out << "</dummy>\n";
}


//#################################################################
bool field_checked( const Node_Ptr& esnode){
  auto& attrs = esnode->attributes();
  auto copt = attrs.value( "checked");
  if (copt.has_value()){
    return bool_of_str( copt.value());
  }
  else{
    return false;
  }
}

//###############################################
void out_efield_label( ofstream& out, const Node_Ptr& esnode){
  
  auto checked = field_checked( esnode);
  
  out << "  <electric_field";
  if (checked){
    out << " checked =\"true\"";
  }
  out << ">\n";
}

//###############################################################
void BD_Topper::output_core_info( ofstream& out, int n_indent,
                                      const Core_Sources& msources, bool has_desolvation){
  
  auto mnode = msources.node;
  
  auto str = [&]( const string& tag){
    return checked_value_from_node< string>( mnode, tag);
  };
  
  auto psp = [&](){
    for( int i = 0; i < n_indent; i++){
      out << " ";
    }
  };
  
  auto pwtag = [&]( int d_indent, const string& tag, auto arg){
    print_with_tag( out, n_indent + d_indent, tag, arg);
  };
  
  psp();
  out << "<core>\n";
  
  auto name = str( "name");
  pwtag( 2, "name", name);
  pwtag( 2, "atoms", name + "_surface_atoms.xml");
  
  auto esnode = mnode->child( "electric_field");
  if (esnode){

    auto enodes = esnode->children_of_tag( "grid");

    psp();
    out_efield_label( out, esnode);
    
    for( auto& enode: enodes){
      auto fname = value_from_node< string>(enode);
      pwtag( 4, "grid", fname);   
    }
  
    pwtag( 4, "multipole_field", mpole_name( name));
    
    psp();
    out << "  </electric_field>\n";
  }
  
  /*
  auto arnode = mnode->child( "areas");
  if (arnode){
    auto file = value_from_node< string>( arnode);
    pwtag( 2, "areas", file);
  }
  */
  
  pwtag( 2, "hydro_params", hydro_params_name( name)); 
  pwtag( 2, "eff_charges", cheby_name( name));
  
  if (has_desolvation){
    pwtag( 2, "eff_charges_squared", cheby2_name( name));
    
    psp();
    out << "  <desolvation_field>\n";
    for( auto& bsource: msources.born_sources){
      auto fileo = bsource.output_file();
      if (!fileo){
        error( __FILE__, __LINE__, "not get here");
      }
      
      auto& file = fileo.value();
      pwtag( 4, "grid", file);
    }
    psp();
    out << "  </desolvation_field>\n";
  }
 
  psp();
  out << "</core>\n";
}

//#################################################################
void BD_Topper::output_group_info( ofstream& out, int n_indent,
                                  const Group_Sources& gsources, bool has_desolvation){
  
  auto gnode = gsources.node;
  
  auto psp = [&](){
    for( int i = 0; i < n_indent; i++){
      out << " ";
    }
  };
  
  psp();
  out << "<group>\n";
  
  auto name = checked_value_from_node< string>( gnode, "name");
  psp();
  out << "  <name> " << name << " </name>\n";

  for( auto& msource: gsources.core_sources){
    output_core_info( out, n_indent+2, msource, has_desolvation);
  }
  
  auto cnodes = gnode->children_of_tag( "chain");
  for( auto& cnode: cnodes){
    output_chain_info( out, n_indent+2, cnode);
  }
  psp();

  auto dnodes = gnode->children_of_tag( "dummy");
  for( auto& dnode: dnodes){
    output_dummy_info( out, n_indent+2, dnode);
  }


  out << "</group>\n";
}

//################################################################
string BD_Topper::generated_solvent_file(const Node_Ptr &top, const string& overall_name){
  
  auto node = top->child( "solvent");
  if (!node){
    error( "need solvent information");
  }
  auto choice = [&]( const string& name, auto default_val){
    auto res = value_from_node< double>( node, name);
    if (res){
      return *res;
    }
    else{
      typedef decltype( default_val) DType;
      return default_val/DType{1.0};
    }
  };
  
  auto filename = overall_name + "_solvent.xml";
  auto dielectric = choice( "dielectric", Default::solvent_dielectric);
  auto debye_length = choice( "debye_length", Default::debye_length);
  auto relative_viscosity = choice( "relative_viscosity", Default::relative_viscosity);
  auto kT = choice( "kT", Default::kT);
  auto desolvation_parameter = choice( "desolvation_parameter", Default::solvation_parameter);
  auto solvent_radius = choice( "solvent_radius", Default::solvent_radius);
  
  ofstream out( filename);
  out << "<top>\n";
  out << "  <dielectric> " << dielectric << " </dielectric>\n";
  out << "  <debye_length> " << debye_length << " </debye_length>\n";
  out << "  <relative_viscosity> " << relative_viscosity << " </relative_viscosity>\n";
  out << "  <kT> " << kT << " </kT>\n";
  out << "  <desolvation_parameter> " << desolvation_parameter << " </desolvation_parameter>\n";
  out << "  <solvent_radius> " << solvent_radius << " </solvent_radius>\n";
  out << "</top>\n";
  
  return filename;
}

//################################################################
string BD_Topper::overall_name_from_groups( const Node_Ptr &groups_node){
  // overall name
  auto group_nodes_lst = groups_node->children_of_tag( "group");
  auto group_nodes = vector_of_list( group_nodes_lst);
  
  auto string_from_node = [&]( const Node_Ptr& node) -> string{
    return checked_value_from_node< string>( node, "name");
  };
  
  auto gnames = group_nodes.mapped( string_from_node);
  
  auto merged_string = [&]( const string& res, const string& name) -> string{
    return res + "_" + name;
  };
  
  if (gnames.size() == 1){
    return gnames[0];
  }
  else{   
    return gnames.folded_left( merged_string); 
  }
}
//###################################################################
Vector< Group_Sources>
BD_Topper::group_sources( optional< Source_From_File>& rxn_source_opt,
                                   Node_Ptr system_node,
                                   Solvent_Sources& solvent_sources){
  
  auto group_nodes_lst = system_node->children_of_tag( "group");
  auto group_nodes = vector_of_list( group_nodes_lst);
  
  auto gsource = [&]( auto gnode){
    auto msources = core_sources( rxn_source_opt, gnode, solvent_sources);
    auto csources = chain_sources_of_group( system_node, gnode);
    return Group_Sources{ std::move( msources), std::move( csources), gnode};
  };
  
  return group_nodes.mapped( gsource);
}

//#######################################
Vector< Source_From_File>
BD_Topper::chain_sources_of_group( const Node_Ptr& system_node,
                                            const Node_Ptr& group_node){
  
  auto chain_nodes_lst = group_node->children_of_tag( "chain");
  if (!chain_nodes_lst.empty()){
  
    std::map< string, string> core_file_of_name;
    {
      auto mnodes = group_node->children_of_tag( "core");
      for( const auto& mnode: mnodes){
        auto name = from_node( mnode, "name");
        auto file = from_node( mnode, "atoms");
        core_file_of_name.insert( std::make_pair( name, file));
      }
    }
    
    auto conn_file = from_node( system_node, "connectivity");
    auto param_file = from_node( system_node, "parameters");
    
    auto conn_source = new_source_from_file( conn_file);
    auto param_source = new_source_from_file( param_file);
    
    auto chain_nodes = vector_of_list( chain_nodes_lst);
    
    auto node_to_source = [&]( Node_Ptr node){
      auto atom_file = from_node( node, "atoms");
      auto atom_source = new_source_from_file( atom_file);
      auto name = from_node( node, "name");
      auto chain_file = chain_name( name);
      
            
      auto lnodes = vector_of_list( node->children_of_tag( "link"));
      
      
      auto command1 = [&](){
        auto command0 = string( "coffdrop_chain -name " + name + " -pqr $0 -params $1 -conn $2");
        
        auto dt_opt = value_from_node< string>( node, "constant_dt");
        if (dt_opt){
          auto dt = dt_opt.value();
          return command0 + " -dt " + dt; 
        }
        else{
          return command0;
        }
      }();
      
      auto command2 = [&](){
        auto cons_opt = value_from_node< string>( node, "constraints");
        if (cons_opt){
          auto cons = cons_opt.value();
          auto has_cons = bool_of_str( cons);
          
          if (has_cons){
            return command1 + " -constraints";
          }
          else{
            return command1;
          }
        }
        else{
          return command1;
        }
      }();
      
      if (lnodes.empty()){
        return new_source_from_file( chain_file, command2,
                                      {atom_source, param_source, conn_source});
      }
      else{
        
        struct Link{
          string core_file;
          size_t core_resid = maxi, chain_resid = maxi;
        };
        
        std::map< string, Vector< Link> > link_of_core;
  
        for( const auto& lnode: lnodes){
          auto core_name = from_node( lnode, "core_name");
          auto& links = link_of_core[ core_name];
          links.emplace_back();
          auto& link = links.back();
          link.core_file = core_file_of_name.at( core_name);
          link.core_resid = checked_value_from_node< size_t>( lnode, "core_residue");
          link.chain_resid = checked_value_from_node< size_t>( lnode, "chain_residue");
        }
          
        int i = 3;
        string core_command( " -cores ");
        Sources core_sources{ atom_source, param_source, conn_source};
        for( auto& info: link_of_core){
          auto& core_name = info.first;
          auto& links = info.second;
          auto& core_file = links.back().core_file;
          auto core_source = new_source_from_file( core_file);
          core_sources.push_back( core_source);
          auto istr = to_string( i);
          core_command += core_name + " $" + istr;
          //core_command.append( {core_name, " $", istr});
          for( auto& link: links){
            auto core_res = to_string( link.core_resid);
            auto chain_res = to_string( link.chain_resid);
            core_command = core_command + ' ' + core_res + ' ' + chain_res + ' '; 
          }
          ++i;
        }
        
        auto final_command = command2 + core_command;
        return new_source_from_file( chain_file, final_command, core_sources);
      }
      
    };
    
    return chain_nodes.mapped( node_to_source);
  }
  
  else{
    return {};
  }
  
}  
  /*
<link>
   <core_name> 
   <core_residue>
   <chain_residue>
 </link>
 ... 0, 1, or 2  
  */
  
//#######################################
Vector< Core_Sources> BD_Topper::core_sources(
                                optional< Source_From_File>& rxn_source_opt,
                                Node_Ptr group_node,
                                Solvent_Sources& solvent_sources){
  
  auto mnodes_lst = group_node->children_of_tag( "core");
  auto mnodes = vector_of_list( mnodes_lst);
  
  auto group_name = checked_value_from_node< string>( group_node, "name");
  
  auto dielectric_source = solvent_sources.dielectric;
  auto debye_length_source = solvent_sources.debye_length;
  auto desolvation_parameter_source = solvent_sources.desolvation;
  auto solvent_radius_source = solvent_sources.solvent_radius;
  
  auto msource = [&]( auto mnode){
    auto core_name = checked_value_from_node< string>( mnode, "name");
    auto atoms_file = checked_value_from_node< string>( mnode, "atoms");
    
    auto has_flag = [&]( const string& flag){
      auto res = value_from_node< string>( mnode, flag);
      return res && bool_of_str(res.value());
    };
    
    bool all_in_surface = has_flag( "all_in_surface");
    bool is_protein = has_flag( "is_protein");
    
    auto atom_file_source = new_source_from_file( atoms_file);
    
    auto surface_file = core_name + "_surface.xml";
    auto s_command = "surface_spheres -probe_radius $0 < $1";
    auto s_spheres_out = new_source_from_file( surface_file, s_command,
                                                {solvent_radius_source, atom_file_source});
    
    auto sl_spheres_out = [&](){
      
      auto has_rxn = rxn_source_opt.has_value();
    
      string slc_mid = has_rxn ? "-rxn $2 " : "";
    
      string sl_command;
      auto sl_com_tail = slc_mid + "-group " + group_name + " -core " + core_name;
      if (all_in_surface){
        sl_command = "make_surface_sphere_list -spheres $1 " + sl_com_tail;
      }
      else{
        sl_command = "make_surface_sphere_list -surface $0 -spheres $1 " + sl_com_tail;
      }
      
      auto sl_file = core_name + "_surface_atoms.xml";
      
      Vector< Source> sl_sources{ s_spheres_out, atom_file_source};
      if (has_rxn){
        sl_sources.push_back( rxn_source_opt.value());
      }
      
      return new_source_from_file( sl_file, sl_command, sl_sources);
      
    }();
    
    auto grid_sources_opt = grid_sources( mnode);
    
    auto mpole_source_opt = [&]() -> optional< Source_From_File>{
      if (grid_sources_opt){
        auto& gsources = grid_sources_opt.value();  
        auto large_grid_source = gsources[0];
        
        string mpole_command( "mpole_grid_fit -dx $0 -solvdi $1 -debye $2");
        auto mpole_file = mpole_name( core_name);
        return new_source_from_file( mpole_file, mpole_command,
                                                 {large_grid_source, dielectric_source, debye_length_source});
      }
      else{
        return std::nullopt;
      }
    }();
    
    auto inside_file_source = [&](){
      auto inside_file = core_name + "_inside.xml";
      
      if (grid_sources_opt){
        auto& grid_sources = grid_sources_opt.value();  
        
        auto small_grid_source = grid_sources.back();
         
        string inside_command( "inside_points -spheres $0 -surface $1 -egrid $2");

        return new_source_from_file( inside_file, inside_command,
                   {atom_file_source, s_spheres_out, small_grid_source});
      }
      else{
        string inside_command( "inside_points -spheres $0 -surface $1");
        return new_source_from_file( inside_file, inside_command,
                                    {atom_file_source, s_spheres_out});
      }
    }();
    
    auto hydro_file = hydro_params_name( core_name);
    string hydro_command( "hydro_params < $0");
    auto hydro_source =  new_source_from_file( hydro_file, hydro_command,
                                         {inside_file_source});
    
    auto charges_command = string( is_protein ? "protein_test_charges" : "test_charges") + " < $0";
    auto charges_file = core_name + "_charges.xml";
    auto charges_source = new_source_from_file( charges_file, charges_command, {atom_file_source});
    
    auto cheby_command = "lumped_charges -pts $0";
    auto cheby_file = cheby_name( core_name);
    auto cheby_source = new_source_from_file( cheby_file, cheby_command, {charges_source});
    
    auto desolve = desolvation_parameter_source.result();
    if (desolve > 0.0){
      // later, turn into source
      double solute_dielectric = Default::solute_dielectric;
      auto solute_dielectric_opt = value_from_node< double>( mnode, "dielectric");
      if (solute_dielectric_opt){
        solute_dielectric = *solute_dielectric_opt;
      }
      
      string born_command( "born_integral -atoms $0 -dx $1 -oeps $2 -debye $3 -ieps " +
                          std::to_string( solute_dielectric));
      
      if (!grid_sources_opt){
        error( "need electric field for Born model");
      }
      auto& gsources = grid_sources_opt.value();
      
      Vector< Source> born_sources;
      for( auto& gsource: gsources){
        const string& gfname = gsource.output_file();
        auto gname = [&]{
          auto n = gfname.size();
          return gfname.substr( 0,n-3);
        }();
        auto born_file = born_name( gname);
        
        born_sources.emplace_back(
              new_source_from_file( born_file, born_command,
               {atom_file_source, gsource, dielectric_source, debye_length_source}));
      }
        
      string charges2_command( "compute_charges_squared < $0");
      auto charges2_file = core_name + "_charges_squared.xml";
      auto charges2_source =
        new_source_from_file( charges2_file, charges2_command, {charges_source});
      
      auto cheby2_command = "lumped_charges -pts $0";
      auto cheby2_file = cheby2_name( core_name);
      auto cheby2_source = new_source_from_file( cheby2_file, cheby2_command, {charges2_source});
      
      auto cheby2_ptr = std::make_unique< Source>( cheby2_source);
      
      return Core_Sources{ hydro_source, cheby_source, sl_spheres_out,
                            mpole_source_opt, mnode,
                            std::move( cheby2_ptr), std::move( born_sources)};
    }
    
    // outputs: cheby, hydro, surface atoms; inside, charges
    else{
      return Core_Sources{ hydro_source, cheby_source, sl_spheres_out,
                            mpole_source_opt, mnode};
    }
  };
  
  return mnodes.mapped( msource);
}

//############################################################################
// first is for outermost grid, last is for innermost
optional< Vector< Source_From_File> >
BD_Topper::grid_sources( const Node_Ptr& mnode){
 
  auto agrids_node = mnode->child( "electric_field");
  if (!agrids_node){
   return std::nullopt;
  }
  else{
    auto grid_nodes = vector_of_list( agrids_node->children_of_tag( "grid"));
    auto grid_names = grid_nodes.mapped( [&]( const Node_Ptr& node){
            return value_from_node< string>( node);});
    
    auto sources = grid_names.mapped( [&]( const string& name){
      return new_source_from_file( name);
    });
    
    auto atoms_file = checked_value_from_node< string>( mnode, "atoms");
    auto abounds = atom_bounds_from_files( atoms_file);
  
    //int ilo, ihi;
    auto [ilo,ihi] = min_and_max_grids( grid_names, abounds);
    std::swap( sources.front(), sources[ihi]);
    std::swap( sources.back(), sources[ilo]);
    return std::move( sources);
  }
}

// not used
/*
optional< pair< Source_From_File, Source_From_File> >
BD_Topper::smallest_and_largest_grid_sources( Node_Ptr mnode){

  auto agrids_node = mnode->child( "electric_field");
  if (!agrids_node){
    return std::nullopt;
  }
  else{
    auto grid_nodes = vector_of_list( agrids_node->children_of_tag( "grid"));
    auto grid_names = grid_nodes.mapped( [&]( Node_Ptr node){ return value_from_node< string>( node);});
  
    auto atoms_file = checked_value_from_node< string>( mnode, "atoms");
    auto abounds = atom_bounds_from_files( atoms_file);
  
    int ilo, ihi;
    tie( ilo, ihi) = min_and_max_grids( grid_names, abounds);
  
    auto& min_grid_file = grid_names[ ilo];
    auto& max_grid_file = grid_names[ ihi];
  
    auto min_source = new_source_from_file( min_grid_file);
    auto max_source = new_source_from_file( max_grid_file);
    return make_pair( min_source, max_source);
  }
}

*/

//########################################################################################
pair< int, int> BD_Topper::min_and_max_grids( const Vector< string>& grid_names, pair< Pos, Pos> atom_bounds){
   
  auto alow = atom_bounds.first;
  auto ahigh = atom_bounds.second;
    
  auto ng = grid_names.size();
  
  Length3 min_vol( large), max_vol( neg_large);
  //int ilo = -1, ihi = -1;
  std::optional< size_t> ilo, ihi;
  for( auto i: range( ng)){
   
    Pos glow, ghigh; 
    tie( glow, ghigh) = grid_corners( grid_names[i]);
    
    auto bounded = a_bounds_b( glow, ghigh, alow, ahigh);
     
    if (bounded){
      auto vol = box_volume( glow, ghigh);
         
      if (vol < min_vol){
        ilo = i;
        min_vol = vol;
      }
      if (vol > max_vol){
        ihi = i;
        max_vol = vol;
      }
    }
  }
  
  if (!ilo || !ihi){
    error( "enclosing grid not found");
  }
  return make_pair( ilo.value(), ihi.value());
}

//##########################################
Length3 box_volume( Pos lo, Pos hi){
  
  return (hi[0] - lo[0])*(hi[1] - lo[1])*(hi[2] - lo[2]);
}

//####################################################
bool a_bounds_b( Pos alo, Pos ahi, Pos blo, Pos bhi){

  bool bounded = true;
  for( auto k: range(3)){
    if (blo[k] < alo[k] || ahi[k] < bhi[k]){
      bounded = false;
      break;
    }
  }
  return bounded;
}

//######################################################################
pair< Pos, Pos> BD_Topper::grid_corners( const string& grid_name){
  
  Pos offset( Zeroi{});
  Single_Grid< double> grid( grid_name, offset, false);
   
  auto low = grid.low_corner();
  auto high = grid.high_corner(); 
  return make_pair( low, high);
}

//#########################################################################
pair< Pos, Pos> BD_Topper::atom_bounds_from_files( const string& atoms_file){
  
  Blank_Nonbonded_Parameter_Info mmnbi;
  auto atoms = mmnbi.atoms_from_file( atoms_file);
  
  Length linf( large), ninf( neg_large);
  Pos low_bound{ linf, linf, linf}, high_bound{ ninf, ninf, ninf};
  
  for( auto& atom: atoms){
    for( auto k: range(3)){
      auto x= atom.pos[k];
      auto radius = atom.radius;
      auto xm = x - radius;
      auto xp = x + radius;
      low_bound[k] = std::min( xm, low_bound[k]);
      high_bound[k] = std::max( xp, high_bound[k]);
    }
  }
  
  return make_pair( low_bound, high_bound);
}

//#######################################
template< class T>
Vector< T> vector_of_list( const std::list< T>& lst){
  auto n = lst.size();
  Vector< T> res;
  res.reserve( n);
  for( auto& t: lst){
    res.push_back( t);
  }
  return res;
}

//######################################################
string BD_Topper::mpole_name( const string& str){
  return str + "_mpole.xml";
}

string BD_Topper::hydro_params_name( const string& str){
  return str + "_hydro_params.xml";
}

string BD_Topper::cheby_name( const string& str){
  return str + "_cheby.xml";
}

string BD_Topper::cheby2_name( const string& str){
  return str + "_squared_cheby.xml";
}

string BD_Topper::born_name( const string& str){
  return str + "_born.dx";
}

string BD_Topper::chain_name( const string& str){
  return str + "_chain.xml";
}


//##################################################################################################
template< class T>
void BD_Topper::print_with_tag( ofstream& out, int n_indent, const string& tag, T arg){
  
  for( int i = 0; i < n_indent; i++){
    out << " ";
  }
  out << ("<" + tag + "> ") << arg << (" </" + tag + ">\n");
}

}

/*
 Plan
 
 <top>
     
   <n_threads>
   <seed>
   <output>

   <n_trajectories>
   <n_trajectories_per_output>
   <max_n_steps> 
   <trajectory_file> 
   <n_steps_per_output>
   
   <n_copies> 
   <n_bin_copies> 
   <n_steps> 
   <n_we_steps_per_output> 
   <bin_file> 
   
   <system>
      <force_field>
      <parameters>
      <connectivity>
      <start_at_site>
      
      <reaction_file>

      <solvent>
        <debye_length>
        <dielectric>
        <relative_viscosity>
        <kT>
        <desolvation_parameter>
        <solvent_radius>
      </solvent>

     <time_step_tolerances>
       <force>
       <reaction>
       <minimum_core_dt>
       <minimum_core_reaction_dt>
       <minimum_chain_dt>
       <minimum_chain_reaction_dt>
     </time_step_tolerances>

     
     <group>
       <name>
       <core>
         <name>
         <all_in_surface>
         <is_protein>
         <atoms>
         <electric_field>
           <grid>
           ...
         </electric_field>
         <dielectric>
       </core>
       ...
   
       <chain>
         <name>
         //<rigid_bonds>
         <atoms>
         
         <link>
           <core_name> 
           <core_residue>
           <chain_residue>
         </link>
         ... 0, 1, or 2
         
       </chain>
       ...
       
     </group>
   ...
   </system>
   
 </top>
 
 
 */
