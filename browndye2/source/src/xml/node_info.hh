#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Various utility functions for extracting information from an
XML node. "Checked" implies that if a value is not found, an
exception is thrown. Those functions with a "tag" argument extract
information from a child node.

*/

#include <sstream>
#include <optional>
#include "jam_xml_pull_parser.hh"
#include "../lib/units.hh"
#include "../lib/array.hh"

namespace Browndye{

template< class Value>
Value value_from_string( JAM_XML_Pull_Parser::Node_Ptr node,
                          const std::string& a){
  std::stringstream ss( a);
  Value val;
  ss >> val;
  if (ss.fail())
    node->perror( "conversion error of", a);
  return val;
}

template< class Value>
std::optional< Value>
value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){
  
  auto cnode = node->child( tag);
  bool found{ cnode};
  //Value value{};
  if (found){
    if (cnode->data().size() > 0){
      return std::make_optional( value_from_string< Value>( node, cnode->data()[0]));
    }
  }
  
  return std::nullopt;
}

template< class Value>
void get_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node,
                          const std::string& tag, Value& value){
  auto value0 = value_from_node< Value>( node, tag);
  if (value0)
    value = *value0;
}


template< class Value>
Value value_from_node( JAM_XML_Pull_Parser::Node_Ptr node){
  
  auto& file = node->file_name();
  auto& tag = node->tag();
  if (node->data().empty())
    error( "not data in node", tag, "of file", file);
  
  return value_from_string< Value>( node, node->data()[0]);  
}

JAM_XML_Pull_Parser::Node_Ptr 
checked_child( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag);

template< class Value, size_t n>
std::optional< Array< Value, n> >
array_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){

  typedef Array< Value, n> T;  
  auto cnode = node->child( tag);
  bool found{ cnode};
  
  if (found){
    auto& data = cnode->data();
    auto nd = data.size();
    if (nd != n)
      node->perror( "array_from_node: actual size", nd, "not equal to",n);

    T res{};    
    for( auto i = 0u; i < n; i++)
      res[i] = value_from_string< Value>( node, data[i]);
    
    return std::make_optional( res);
  }
     
  return std::nullopt;
}


template< class Value, size_t n>
Array< Value, n> 
array_from_node( JAM_XML_Pull_Parser::Node_Ptr node){

  Array< Value, n> res{};    
  auto& data = node->data();
  auto nd = data.size();
  if (nd != n)
    node->perror( "array_from_node: actual size", nd, "not equal to",n);
    
  for( auto i = 0u; i < n; i++)
    res[i] = value_from_string< Value>( node, data[i]);
  
  return res;
}

template< class Value>
//std::pair< bool, Vector< Value> >
std::optional< Vector< Value> >
vector_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){
  
  typedef Vector< Value> T;
  
  auto cnode = node->child( tag);
  bool found{ cnode};

  if (found){
    auto& data = cnode->data();
    auto n = data.size();
    T res{};
    res.reserve( n);
    for( auto i = 0u; i < n; i++){
      auto val = value_from_string< Value>( node, data[i]);
      res.push_back( val);
    }
    return std::make_optional( std::move( res));
  }
     
  return std::nullopt;
}

template< class Value>
Vector< Value> 
vector_from_node( JAM_XML_Pull_Parser::Node_Ptr cnode){
  
  Vector< Value> res{};
  auto& data = cnode->data();
  auto n = data.size();
  res.reserve( n);
  for( auto i = 0u; i < n; i++){
    auto val = value_from_string< Value>( cnode, data[i]);
    res.push_back( val);
  }
     
  return res;   
}


template< class Value, size_t n>
Array< Value,n> checked_array_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){
  
  auto res = array_from_node< Value, n>( node, tag);
  if (!res)
    node->perror( "tag", tag, " not found");

  return *res;
}

template< class Value>
Vector< Value> checked_vector_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){
  
  auto res = vector_from_node< Value>( node, tag);
  if (!res)
    node->perror( "tag", tag, " not found");

  return std::move( *res);
}


template< class Value>
Value checked_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag){

  auto res = value_from_node< Value>( node, tag);
  if (!res){
    node->perror( "tag", tag, " not found");
  }
  return *res;
}

//###################################################################
// applies to each tag once
template< size_t nt, class...F>
bool apply_to_each_child_with_tag( JAM_XML_Pull_Parser::Node_Ptr node,
                      const Array< std::string, nt>& tags,
                      F&...fs){

  auto done = [&]( Array< size_t, nt> count){
    auto f = [&]( bool res, size_t k){
      return res && (k > 0);
    };
    return count.folded_left( f, true);
  };
  
  auto& parser = node->parser();
  return parser.apply_to_next_children_with_tags( node, tags, done, fs...);
}

}

