/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Generates the desolvation energy grid as described in the paper. Uses
the generic Cartesian multipole code to keep the time within 
reasonable limits.

 */

#include <string.h>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <random>
#include "../lib/units.hh"
#include "../lib/vector.hh"
#include "../generic_algs/cartesian_multipole.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
#include "../input_output/get_args.hh"
#include "../global/physical_constants.hh"
#include "../global/limits.hh"
#include "../global/pi.hh"
#include "../global/defaults.hh"
#include "../lib/sq.hh"
#include "../forces/electrostatic/single_grid.hh"
#include "born_grid.hh"

namespace {

using namespace Browndye;
typedef unsigned int uint;

using std::max;
using std::min;
using std::optional;
using std::pair;
using std::nullopt;
using std::make_pair;
using std::partition;

constexpr Length L1{ 1.0};
constexpr Atom_Index A1{ 1};

enum Coord {X,Y,Z};

namespace JP = JAM_XML_Pull_Parser;

//############################################################
Vec3< double> dedim( Pos p){
  return p.mapped( []( Length x){ return x/L1;});
}

template< class T>
auto inv3( Vec3< T> v){
  
  return v.mapped( [&]( T t){ return 1.0/t;});
}

template< class T>
T flrep( T t){
  return t;
}

double flrep( size_t t){
  return (double)t;
}

template< class Ta, class Tb>
auto kprod( Vec3< Ta> a, Vec3< Tb> b){
  
  auto fa = a.mapped( []( Ta t){ return flrep(t);});
  auto fb = b.mapped( []( Tb t){ return flrep(t);});
  
  typedef std::decay_t< decltype( fa[0])> Tfa;
  typedef std::decay_t< decltype( fb[0])> Tfb;
  
  typedef decltype( Tfa()*Tfb()) Tab;
  
  return Vec3< Tab>{ fa[X]*fb[X], fa[Y]*fb[Y], fa[Z]*fb[Z]}; 
}


template< class T>
auto tprod( Vec3< T> v){
  return v[X]*v[Y]*v[Z];
}

//#####################################################
const Vec3< size_t> ones{ 1, 1, 1};
  
//#################################################################### 
struct Grid_Ref{
public:
  typedef Vec3< size_t> IPos;
  optional< pair< IPos, IPos> > sboundso, rboundso;
  typedef Vector< Atom, Atom_Index> Atoms;
  
  struct Atoms_Info{
    std::reference_wrapper< Atoms> atoms;
    Atom_Index loai, hiai;
  };
  
  optional< Atoms_Info> atoms_info;
  
  bool has_sources() const{
    return sboundso.has_value() || atoms_info;
  }
  
  bool has_receivers() const {
    return rboundso.has_value();
  }
  
  pair< IPos, IPos> rbounds() const{
    return rboundso.value();
  }
  
  pair< IPos, IPos> sbounds() const {
    return sboundso.value();
  }
  
  Pos lo_corner, hi_corner;
  size_t nsources = 0;
  
  size_t n_sources( const Born_Grid& cont) const;
};

//####################################################
template< class F>
void apply_to_source_pts( const Born_Grid&, const Grid_Ref& ref, F&& f);

//##############################################
size_t Grid_Ref::n_sources( const Born_Grid& cont) const {

  size_t sum = 0;
  if (has_sources()){
    
    if (atoms_info){
      auto& info = atoms_info.value();
      Atom_Index a1{1};
      return info.hiai/a1 - info.loai/a1;
    }
    else{
      auto f = [&]( size_t ix, size_t iy, size_t iz){
        ++sum;
      };
      
      apply_to_source_pts( cont, *this, f);
    }
  }

  return sum;
}

//############################################################
class Iface{
public:
  
  typedef Born_Grid Container;
  typedef Grid_Ref Refs;
  typedef size_t size_type;

  template< class F>
  static
  void apply_potential( Born_Grid& cont, Grid_Ref& refs, F& f);

  static
  void process_bodies( Born_Grid& cont, Grid_Ref& refs);

  static
  void process_bodies( Born_Grid& cont, Grid_Ref& refs0, Grid_Ref& refs1);

  template< class V>
  static
  void get_r_derivatives( const Born_Grid& cont, uint order, double r, 
                          V& derivs);
  

  template< class V3>
  static
  double near_potential( const Born_Grid& cont, 
                         const Grid_Ref& refs, const V3& pos);
   

  template< class Function> 
  static
  void apply_multipole_function( const Born_Grid& cont, 
                                 const Grid_Ref& refs, Function& f);

  template< class V3>
  static
  void get_bounds( const Born_Grid& cont, V3& low, V3& high);
 

  template< class Refs_Cube, class V3>
  static
  void split_container( const Born_Grid& cont, 
                        const Grid_Ref& refs, 
                        const V3& center,
                        Refs_Cube&& refs_cube);
    
  static
  void empty_references( const Born_Grid& cont, Grid_Ref& refs);
  
  static
  void copy_references( const Born_Grid& cont, Grid_Ref& refs);

  static
  size_t size( const Born_Grid& cont, const Grid_Ref& refs);

  static
  size_t n_receivers( const Born_Grid& cont, const Grid_Ref& refs);

  static
  size_t n_sources( const Born_Grid& cont, const Grid_Ref& refs);

};

//####################################################
size_t Iface::size( const Born_Grid& cont, const Grid_Ref& ref){
 
  return  n_sources( cont, ref) + n_receivers( cont, ref);
}

template< class T>
Vec3< T> max3( Vec3< T> a, Vec3< T> b){
  
  return initialized_array< 3>( [&]( size_t k){return max( a[k], b[k]);});
}

template< class T>
Vec3< T> min3( Vec3< T> a, Vec3< T> b){
  
  return initialized_array< 3>( [&]( size_t k){return min( a[k], b[k]);});
}

//#####################################################################
pair< Pos, Pos> bg_bounds( const Born_Grid& cont){
  
  const Length La{ large};
  Pos loc{ La, La, La}, hic{ -La, -La, -La};
  
  auto& atoms = cont.atoms;
  if (!atoms.empty()){
    for( auto& atom: atoms){
      loc = min3( loc, atom.pos);
    }
  }
  else if (cont.sgrid){
    Vec3< size_t> los( Zeroi{});
    //auto his = cont.sgrid->dimensions();
    loc = min3( loc, cont.sgrid->low_corner());
    hic = max3( hic, cont.sgrid->high_corner());
  }
  
  auto& rec = cont.rgrid;
  
  loc = min3( loc, rec.low_corner());;
  hic = max3( hic, rec.high_corner());
  
  auto ds = hic - loc;
  auto maxh = max( max( ds[X], ds[Y]), ds[Z]);
  Pos msh{ maxh, maxh, maxh};
  
  return make_pair( loc, loc + msh);
}

//####################################################################
void Iface::copy_references( const Born_Grid& cont, Grid_Ref& ref){
  
  if (!cont.atoms.empty()){
    ref.atoms_info = Grid_Ref::Atoms_Info{ cont.atoms, Atom_Index( 0), cont.atoms.size()};
  }
  else if (cont.sgrid){
    Vec3< size_t> los( Zeroi{});
    auto his = cont.sgrid->dimensions();
    ref.sboundso = make_pair( los, his);
  }
  
  auto& rec = cont.rgrid;
  Vec3< size_t> lor( Zeroi{});
  auto hir = rec.dimensions();
  ref.rboundso = make_pair( lor, hir); 
  
  auto bgbnds = bg_bounds( cont);
  ref.lo_corner = bgbnds.first;
  ref.hi_corner = bgbnds.second;
    
  ref.nsources = ref.n_sources( cont);
}

void Iface::empty_references( const Born_Grid& cont, Grid_Ref& refs){

  refs.rboundso = nullopt;
  refs.sboundso = nullopt;
  refs.atoms_info = nullopt;
  refs.nsources = 0;
}

//#############################################################
template< class F>
void apply_to_source_pts( const Born_Grid& cont, const Grid_Ref& ref, F&& f){
  
  if (cont.sgrid){
    auto& sources = *(cont.sgrid);
    auto [losi,hisi] = ref.sbounds();
    for( auto ix: range( losi[X], hisi[X])){
      for( auto iy: range( losi[Y], hisi[Y])){
        for( auto iz: range( losi[Z], hisi[Z])){
          if (sources.value( ix,iy,iz) == 1){
            f( ix,iy,iz);
          }
        }
      }
    }
  }
}

//#############################################################
template< class F>
void apply_to_receiver_pts( const Grid_Ref& ref, F&& f){
  
  if (ref.has_receivers()){
    auto [lori,hiri] = ref.rbounds();
    for( auto ix: range( lori[X], hiri[X])){
      for( auto iy: range( lori[Y], hiri[Y])){
        for( auto iz: range( lori[Z], hiri[Z])){
          f( ix,iy,iz);
        }
      }
    }
  }
}

//#########################################################
// orig is lower corner of entire grid
// no longer used
size_t mid_index( Length orig, Length h, Length mid){
  
  return size_t( ceil( (mid - orig)/h));
}

// bounded + 1
[[maybe_unused]]		
optional< pair< size_t, size_t>>
bounded_indices( Length orig, Length h,
                Length lo, Length hi,
                size_t il, size_t ih){
    
  auto clipped = [&]( int i){
    return (size_t)min( max( (int)il, i), (int)ih);
  };
  
  int ilo = ceil( (lo - orig)/h);
  int ihi = ceil( (hi - orig)/h);
  
  auto jlo = clipped( ilo);
  auto jhi = clipped( ihi);
  if (jlo < jhi){
    
    // check, debug
    {
      assert( orig + (double)jlo*h >= lo);
      assert( jlo >= il);
      assert( jhi <= ih);
    }
    
    return make_pair( jlo, jhi);
  }
  else{
    return nullopt;
  }
}

//##############################################################
template< class F>
void apply_to_cube( F&& f){
  
  for( auto ix: range(2)){
    for( auto iy: range(2)){
      for( auto iz: range(2)){
        f( ix,iy,iz);
      }
    }
  }
}

//###################################################
void check_consistency( const Born_Grid& cont, const Grid_Ref& ref){
  
  for( auto k: range(3)){
    
    auto lo = ref.lo_corner[k];
    auto hi = ref.hi_corner[k];
    if (ref.has_sources()){
      auto& sources = *(cont.sgrid);
      auto loc = sources.low_corner()[k];
      auto h = sources.spacings()[k];
      auto [ilos,ihis] = ref.sbounds();
      auto ilo = ilos[k];
      auto ihi = ihis[k];
      auto glocl = loc + (double)(ilo+1)*h;
      assert( (lo <= glocl) || ilo == 0);
      auto gloch = loc + (double)(ihi-1)*h;    
      assert( (gloch <= hi) || ihi == sources.dimensions()[k]);
    }
    
    if (ref.has_receivers()){
      auto& rec = cont.rgrid;
      auto loc = rec.low_corner()[k];
      auto h = rec.spacings()[k];
      auto [ilos,ihis] = ref.rbounds();
      auto ilo = ilos[k];
      auto ihi = ihis[k];
      auto glocl = loc + (double)(ilo+1)*h;
      auto gloch = loc + (double)(ihi-1)*h;
      
      assert( (lo <= glocl) || ilo == 0);
      assert( (gloch <= hi) || ihi == rec.dimensions()[k]);
    }
  }
  
}

//##############################################################
template< class T>
using Cube = Array< Array< Array< T, 2>, 2>, 2>;
  
// tested  
// also changes the ordering of atoms

Cube< Array< Atom_Index, 2> > partitioned_atoms( Vector< Atom, Atom_Index>& atoms, Pos mid_pt, Atom_Index loai, Atom_Index hiai){
    
  auto bg = atoms.begin();
  auto abg = bg + loai/A1;
  auto aed = bg + hiai/A1;
   
  typedef typename Vector< Atom, Atom_Index>::iterator Atit;
  typedef Array< Atit, 2> Itpair;
  Cube< Itpair> atom_its;
  
  auto amd = partition( abg, aed, [&]( const Atom& atom){
    return atom.pos[Z] < mid_pt[Z];
  });
  
  Array< Itpair, 2> zatits;
  zatits[0] = Itpair{ abg, amd};
  zatits[1] = Itpair{ amd, aed};
  
  auto party = [&]( int kz){
    auto abg = zatits[kz][0];
    auto aed = zatits[kz][1];
    auto amd = partition( abg, aed, [&]( const Atom& atom){
      return atom.pos[Y] < mid_pt[Y];
    });
    
    Array< Itpair, 2> yatits;
    yatits[0] = Itpair{ abg, amd};
    yatits[1] = Itpair{ amd, aed};
    
    auto partx = [&]( int ky){
      auto abg = yatits[ky][0];
      auto aed = yatits[ky][1];
      auto amd = partition( abg, aed, [&]( const Atom& atom){
        return atom.pos[X] < mid_pt[X];
      });
      
      Array< Itpair, 2> xatits;
      xatits[0] = Itpair{ abg, amd};
      xatits[1] = Itpair{ amd, aed};
      
      for( auto kx: range( 2)){
        atom_its[kx][ky][kz] = xatits[kx];
      }
    };
    
    for( auto ky: range( 2)){
      partx( ky);
    }
  };
  
  for( auto kz: range(2)){
    party( kz);
  }
  
  Cube< Array< Atom_Index, 2> > res;
  
  apply_to_cube( [&]( size_t ix, size_t iy, size_t iz){ 
      for( auto iw: range(2)){
        res[ix][iy][iz][iw] = Atom_Index{ (size_t)(atom_its[ix][iy][iz][iw] - bg)};
      }
    }
  );
   
  return res;
}

//#########################################################
// turn into unit test
[[maybe_unused]]		
void test_partitioned_atoms(){
  
  std::mt19937_64 gen;
  std::uniform_real_distribution< double> uni;
  
  const Atom_Index na{ 10000};
  const size_t ntrials = 1000;
  
  std::uniform_int_distribution< size_t> iuni{ 0, na/A1};
    
  for( auto itl: range( ntrials)){
    (void)itl;
      
    auto ranx = [&](){
      return L1*uni( gen);
    };
      
    auto atoms = initialized_vector( na, [&]( Atom_Index){
      Atom atom;
        atom.pos = Pos{ ranx(), ranx(), ranx()};
      return atom;
    });
  
    Pos center{ ranx(), ranx(), ranx()};
    
    Atom_Index b1{ iuni( gen)};
    Atom_Index b2{ iuni( gen)};
    auto ialo = min( b1,b2);
    auto iahi = max( b1,b2);
    
    auto indices = partitioned_atoms( atoms, center, ialo, iahi);
    
    Array< Array< Array< Length, 2>, 2>, 3> bounds;
    Length L0{ 0.0}, L1{ 1.0};
    for( auto k: range( 3)){
      bounds[k][0][0] = L0;
      bounds[k][0][1] = center[k];
      bounds[k][1][0] = center[k];
      bounds[k][1][1] = L1;
    }
         
    auto f = [&]( size_t ix, size_t iy, size_t iz){
      auto iloihi = indices[ix][iy][iz];
      auto ilo = iloihi[0];
      auto ihi = iloihi[1];
      for( auto ia: range( ilo, ihi)){
        auto& atom = atoms[ia];
        auto pos = atom.pos;
        Array< size_t, 3> ixyz{ ix,iy,iz};
        for( auto k: range(3)){
          auto& bs = bounds[k];
          auto x = pos[k];
          auto& b = bs[ixyz[k]];
          if (x < b[0] || x > b[1]){
            error( "error", b[0], x, b[1]);
          }
        }
      }
    };
    
    apply_to_cube( f);
  }
  std::cout << "passed\n";
}

//############################################################################
typedef Array< Array< Array< Grid_Ref, 2>,2>,2> Grid_Refs;

void set_ref_corners( const Grid_Ref& ref, Array< Length, 3> mid_pt, Grid_Refs& grefs){
  
  apply_to_cube([&]( size_t ix, size_t iy, size_t iz){
     auto& gref = grefs[ix][iy][iz];
     Vec3< size_t> ids{ ix,iy,iz};
     auto cids = ones - ids;
   
     gref.lo_corner = kprod( cids, ref.lo_corner) + kprod( ids, mid_pt); 
     gref.hi_corner = kprod( cids, mid_pt) + kprod( ids, ref.hi_corner);
  });
}

//#########################################################
Array< optional< pair< size_t, size_t> >, 3> 
receiver_index_bounds( const Grid_Ref& ref, const Born_Grid& cont, Array< Length, 3> mid_pt, size_t ix, size_t iy, size_t iz){
  
  Array< size_t, 3> inds{ ix,iy,iz}; 
  
  auto rb = ref.rbounds();
  auto alo = rb.first;
  auto ahi = rb.second;  
   
  auto& rec = cont.rgrid;
  auto rloc = rec.low_corner();
  auto hs = rec.spacings();
  
  if (ref.rboundso){
    auto rb = ref.rbounds();
    auto rlo = rb.first;
    auto rhi = rb.second;
  
    return initialized_array< 3>( [&]( size_t k) -> optional< pair< size_t, size_t> >{
        
      auto imid = mid_index( rloc[k], hs[k], mid_pt[k]);
            
      if (inds[k] == 0){
        auto lo = max( alo[k], rlo[k]);
        if (lo < imid){
          auto hi = min( imid, rhi[k]);
          return make_pair( lo, hi);
        }
        else{
          return nullopt;
        }
      }
      else if (inds[k] == 1){
        auto hi = min( ahi[k], rhi[k]);
        if (imid < hi){
          auto lo = max( imid, rlo[k]);
          return make_pair( lo, hi);
        }
        else{
          return nullopt;
        }
      }
      else{
        error( __FILE__, __LINE__, "not get here");
        return nullopt;
      }
      
    });
  }
  else{
    return initialized_array< 3>([&]( size_t){
      return optional< pair< size_t, size_t> >{}; 
    });
  }
};

//#########################################################
Array< optional< pair< size_t, size_t> >, 3> 
source_index_bounds( const Grid_Ref& ref, const Born_Grid& cont, Array< Length, 3> mid_pt, size_t ix, size_t iy, size_t iz){
  
  Array< size_t, 3> inds{ ix,iy,iz}; 
  
  auto sb = ref.sbounds();
  auto alo = sb.first;
  auto ahi = sb.second;  
   
  auto& src = *(cont.sgrid);
  auto sloc = src.low_corner();
  auto hs = src.spacings();
  
  if (ref.sboundso){
    auto sb = ref.sbounds();
    auto slo = sb.first;
    auto shi = sb.second;
  
    return initialized_array< 3>( [&]( size_t k) -> optional< pair< size_t, size_t> >{
        
      auto imid = mid_index( sloc[k], hs[k], mid_pt[k]);
            
      if (inds[k] == 0){
        auto lo = max( alo[k], slo[k]);
        if (lo < imid){
          auto hi = min( imid, shi[k]);
          return make_pair( lo, hi);
        }
        else{
          return nullopt;
        }
      }
      else if (inds[k] == 1){
        auto hi = min( ahi[k], shi[k]);
        if (imid < hi){
          auto lo = max( imid, slo[k]);
          return make_pair( lo, hi);
        }
        else{
          return nullopt;
        }
      }
      else{
        error( __FILE__, __LINE__, "not get here");
        return nullopt;
      }
    });
  }
  else{
    return initialized_array< 3>([&]( size_t){
      return optional< pair< size_t, size_t> >{}; 
    });
  }
};


//##############################################################################
// refactor
template< class Refs_Cube, class V3>
void Iface::split_container( const Born_Grid& cont, 
                      const Grid_Ref& ref, 
                      const V3& mid_pt_in,
                      Refs_Cube&& put_refs_cube
                      ){
   
  Grid_Refs grefs;
  
  auto mid_pt = mid_pt_in.mapped( []( double x){ return L1*x;});
     
  set_ref_corners( ref, mid_pt, grefs);
  
  if (ref.has_receivers()){         
    apply_to_cube([&]( size_t ix, size_t iy, size_t iz){
      
      auto& gref = grefs[ix][iy][iz];
      Array< size_t, 3> inds{ ix,iy,iz}; 
        
      auto rbounds = receiver_index_bounds( ref, cont, mid_pt, ix,iy,iz);
      
      if (rbounds[X] && rbounds[Y] && rbounds[Z]){
        auto lob = rbounds.mapped( [&]( auto bs){
                                      return bs.value().first;});
        auto hib = rbounds.mapped( [&]( auto bs){
                              return bs.value().second;});
        
        gref.rboundso = make_pair( lob, hib);
      }
      
    });
  }
  
  if (ref.atoms_info){
    auto& atinfo = ref.atoms_info.value();
    auto at_indices = partitioned_atoms( atinfo.atoms.get(), mid_pt, atinfo.loai, atinfo.hiai);
    
    apply_to_cube([&]( size_t ix, size_t iy, size_t iz){
      auto& gref = grefs[ix][iy][iz];
      auto& ainds = at_indices[ix][iy][iz];
      auto ialo = ainds[0];
      auto iahi = ainds[1];
      
      if (iahi > ialo){
        auto& atoms = ref.atoms_info.value().atoms.get();
        gref.atoms_info = Grid_Ref::Atoms_Info{ atoms, ialo, iahi};
        gref.nsources = iahi/A1 - ialo/A1;
      }
    });
  }
  
  else if (ref.sboundso){      
    apply_to_cube([&]( size_t ix, size_t iy, size_t iz){
      auto& gref = grefs[ix][iy][iz];
      Array< size_t, 3> inds{ ix,iy,iz}; 
      
      auto sbounds = source_index_bounds( ref, cont, mid_pt, ix,iy,iz);
                
      if (sbounds[X] && sbounds[Y] && sbounds[Z]){
        auto lob = sbounds.mapped( [&]( auto lohi){
                                      return lohi.value().first;});
        auto hib = sbounds.mapped( [&]( auto lohi){
                              return lohi.value().second;});
        
        gref.sboundso = make_pair( lob, hib);
      }
      gref.nsources = gref.n_sources( cont);
      
      check_consistency( cont, gref);
    });
  }
  
  apply_to_cube([&]( size_t ix, size_t iy, size_t iz){
    auto& gref = grefs[ix][iy][iz];
    if (gref.has_receivers() || gref.has_sources()){
      put_refs_cube( ix,iy,iz, std::move( gref));
    }
  });
  
  {
    auto ns = ref.n_sources( cont);
    
    size_t ns8 = 0;
    apply_to_cube( [&]( size_t ix, size_t iy, size_t iz){
      ns8 += grefs[ix][iy][iz].n_sources( cont);
    });
    
    if (ns != ns8){
      error( __LINE__, "n sources ", ns, ns8);
    }
  }
  
  {
    auto ns = Iface::n_receivers( cont, ref);
    
    size_t ns8 = 0;
    apply_to_cube( [&]( size_t ix, size_t iy, size_t iz){
      ns8 += Iface::n_receivers( cont, grefs[ix][iy][iz]); 
    });
    
    if (ns != ns8){
      error( __LINE__, "n receivers ", ns, ns8);
    }
  }
  
}

//#######################################################
template< class V3>
void Iface::get_bounds( const Born_Grid& cont, V3& low, V3& high){

  auto bgbnds = bg_bounds( cont);
  low = dedim( bgbnds.first);
  high = dedim( bgbnds.second);
}

//#######################################################
template< class Function> 
void Iface::apply_multipole_function( const Born_Grid& cont, 
                               const Grid_Ref& ref, Function& f){
  
  if (ref.has_sources()){
    
    const Inv_Length3 IL1{ 1.0};
    Array< double, 1> mpole;
    
    if (cont.sgrid){
      auto& sources = *(cont.sgrid);
    
      auto g = [&]( size_t ix, size_t iy, size_t iz){       
        auto pos = sources.position( ix,iy,iz);

        mpole[0] = cont.svolume*IL1;
        f( 0, dedim( pos), mpole); 
      };
    
      apply_to_source_pts( cont, ref, g);
    }
    else{ 
      auto& ainfo = ref.atoms_info.value();
      auto& atoms = ainfo.atoms.get();
      auto loai = ainfo.loai;
      auto hiai = ainfo.hiai;
      for( auto ia: range( loai, hiai)){
        auto& atom = atoms[ia];
        auto pos = atom.pos;
        auto a = atom.radius;
        mpole[0] = IL1*(pi4/3)*a*a*a;
        f( 0, dedim( pos), mpole); 
      }
    }
  }
}

Inv_Length4 greens_fun( const Born_Grid& cont, Length r){
  auto L = cont.debye_length;
  auto r2 = r*r;
  return sq( 1.0 + r/L)*exp( -2.0*r/L)/(r2*r2);
}

/*
double greens_fun0( const Born_Grid& cont, double r){
  double r2 = r*r;
  return 1.0/(r2*r2);
}
*/

//####################################################
size_t Iface::n_sources( const Born_Grid& cont, const Grid_Ref& ref){
 
  return ref.nsources;
}

//####################################################
size_t Iface::n_receivers( const Born_Grid& cont, const Grid_Ref& ref){
  
  if (ref.has_receivers()){
    auto [lo,hi] = ref.rbounds();
    return tprod( hi - lo);
  }
  else{
    return (size_t)0;
  }
}

//#########################################################
Inv_Length near_potential( const Born_Grid& cont, const Grid_Ref& ref, Pos pos){
  
  Inv_Length sum{ 0.0};
  
  if (ref.atoms_info){
    auto& ainfo = ref.atoms_info.value();
    auto& atoms = ainfo.atoms.get();
    auto loai = ainfo.loai;
    auto hiai = ainfo.hiai;
    for( auto ia: range( loai, hiai)){
      auto& atom = atoms[ia];
      auto gpos = atom.pos;
      auto r = distance( pos, gpos);
      auto a = atom.radius;
      auto volume = (pi4/3)*a*a*a;
      sum += volume*greens_fun( cont, r);
    }
  }
  else{
    const auto volume = cont.svolume;
    auto hs = cont.sgrid->spacings();
    auto tol = 1.0e-4*min( min( hs[X], hs[Y]), hs[Z]);
    auto& sources = *(cont.sgrid);
    
    auto f = [&]( size_t ix, size_t iy, size_t iz){
      auto gpos = sources.position( ix, iy, iz);
      auto r = distance( pos, gpos);
      if (r > tol){
        sum += volume*greens_fun( cont, r);
      }
    };
    
    apply_to_source_pts( cont, ref, f);
  }
  
  return sum;
}

//##########################################################
// r^(-4)
template< class V>
void get_r_derivatives0( const Born_Grid& grid, uint order, double r, 
                         V& derivs){
  derivs[0] = 1.0/(r*r*r*r);
  for (uint i = 1; i <= order; i++){
    derivs[i] = -((3+i)*derivs[i-1]/r);
  }

}

//####################################################
// (1 + r/L)^2 exp(-2r/L) r^(-4)
template< class Vec>
void Iface::get_r_derivatives( const Born_Grid& grid, 
                        uint order, double r, Vec& derivs){

  auto L = grid.debye_length/L1;

  uint order1 = order + 1;
  Vector< double> terms2( order1), terms1( order1), terms0( order1);
  Vector< double> new_terms2( order1), new_terms1( order1), new_terms0( order1);

  for( uint i = 0; i <= order; i++){
    terms2[i] = 0.0;
    terms1[i] = 0.0;
    terms0[i] = 0.0;
  }

  const double exp_term = exp(-2*r/L);
  const double sq_term = 1 + r/L;
  const double r4 = r*r*r*r;
  double V = sq_term*sq_term*exp_term/r4;
 
  terms2[0] = V;

  for( uint io = 0; io <= order; io++){

    double sum = 0.0;
    for( uint i = 0; i <= order; i++){
      sum += terms0[i] + terms1[i] + terms2[i];
    }
    derivs[io] = sum;

    for( uint i = 0; i <= order; i++){
      new_terms2[i] = 0.0;
      new_terms1[i] = 0.0;
      new_terms0[i] = 0.0;
    }
    
    for( uint i = 0; i < order; i++){
      new_terms1[i] += 2*terms2[i]/(L*sq_term);
      new_terms0[i] += terms1[i]/(L*sq_term);

      new_terms0[i] -= 2*terms0[i]/L;
      new_terms1[i] -= 2*terms1[i]/L;
      new_terms2[i] -= 2*terms2[i]/L;

      new_terms2[i+1] -= terms2[i]*(i+4)/r;
      new_terms1[i+1] -= terms1[i]*(i+4)/r;
      new_terms0[i+1] -= terms0[i]*(i+4)/r;
    }

    for( uint i = 0; i <= order; i++){
      terms0[i] = new_terms0[i];
      terms1[i] = new_terms1[i];
      terms2[i] = new_terms2[i];
    }
  }
}

//####################################################
template< class F>
void Iface::apply_potential( Born_Grid& cont, Grid_Ref& ref, F& f){
  
  auto& rec = cont.rgrid; 
  auto g = [&]( size_t ix, size_t iy, size_t iz){
    auto pos = rec.position( ix,iy,iz);       
    auto V = f( dedim( pos));
    rec.add_to_value( ix,iy,iz, V/L1);
  };
  
  apply_to_receiver_pts( ref, g);
}

//#####################################################
// a has receivers, b has sources
void process_half( Born_Grid& cont, Grid_Ref& refa, Grid_Ref& refb){
  
  if (refb.nsources > 0){  
    auto& rec = cont.rgrid;
    auto f = [&]( size_t ix, size_t iy, size_t iz){
      auto posi = rec.position( ix,iy,iz); 
      auto V = near_potential( cont, refb, posi);
      rec.add_to_value( ix, iy, iz, V);
    }; 
    
    apply_to_receiver_pts( refa, f);
  }  
}

//#############################################################
void Iface::process_bodies( Born_Grid& cont, Grid_Ref& ref){
  
  process_half( cont, ref, ref);
}

//##################################################### 
void Iface::process_bodies( Born_Grid& cont, Grid_Ref& ref0, Grid_Ref& ref1){

  process_half( cont, ref0, ref1);
  process_half( cont, ref1, ref0);   
}

//#########################################################
void main1( int argc, char* argv[]){
  
  const auto ddebye = Default::debye_length/Length(1.0);

  if (argc > 1 && strcmp( argv[1], "-help") == 0){
    printf( "computes desolvation grid\n");
    printf( "-in: name of input xml file, if -atoms is not used\n");
    printf( "-atoms: name of input atom file, if -in is not used");
    printf( "-dx: name of dx file used as template");
    printf( "-vperm: vacuum permittivity (default in units of A, ps, kT at 298)\n");
    printf( "-ieps: dielectric of solute (default %f)\n",
               Default::solute_dielectric);
      printf( "-oeps: dielectric of solvent (default %f)\n",
               Default::solvent_dielectric);
    printf( "-debye: Debye length (default %f)\n", ddebye);
    exit(0);
  }

  Length debye_length{ double_arg( argc, argv, "-debye", ddebye)};
  Permittivity vac_perm{ double_arg( argc, argv, "-vperm", vacuum_permittivity/Permittivity(1.0))};
  auto pal = double_arg( argc, argv, "-ieps", Default::solute_dielectric);
  auto sal = double_arg( argc, argv, "-oeps", Default::solvent_dielectric);
  auto inameo = string_arg_opt( argc, argv, "-in");
  auto anameo = string_arg_opt( argc, argv, "-atoms");
  auto dname = string_arg( argc, argv, "-dx");
  
  const decltype( Energy()*Length()/sq( Charge())) factor = 3.0*(sal - pal)/(pi4*sal*(2*sal + pal)*vac_perm); // SDA version
  
  auto grid = [&]{
    if (inameo){
      auto iname = inameo.value();
      std::ifstream input( iname);
      JP::Parser parser( input, iname);
      return Born_Grid( dname, parser);
    }
    else if (anameo){
      auto aname = anameo.value();
      return Born_Grid( dname, aname);
    }
    else{
      error( "need an input for -in or -atoms");
      std::string dummy;
      return Born_Grid( dname, dummy);
    }
  }();
   
  grid.debye_length = debye_length;
  auto& rec = grid.rgrid;

  const size_t order = 6;
  Cartesian_Multipole::Calculator< Iface> calc( order, grid);
  
  calc.set_max_number_per_cell( 64);
  calc.setup_cells();
  calc.compute_expansion();
  calc.compute_interactions_with_potential();
  
  auto ns = rec.dimensions();
  const size_t nx = ns[X];
  const size_t ny = ns[Y];
  const size_t nz = ns[Z];
  
  auto rlowc = rec.low_corner();
  auto hs = rec.spacings();
  
  printf(
"object 1 class gridpositions counts %d %d %d\norigin %g %g %g\ndelta %g 0.0 0.0\ndelta 0.0 %g 0.0\ndelta 0.0 0.0 %g\nobject 2 class gridconnections counts %d %d %d\nobject 3 class array type double rank 0 items %d data follows\n",
(int)nx, (int)ny, (int)nz,
rlowc[X]/L1, rlowc[Y]/L1, rlowc[Z]/L1, 
hs[X]/L1, hs[Y]/L1, hs[Z]/L1, 
(int)nx, (int)ny, (int)nz, (int)(nx*ny*nz));

  size_t ip = 0;
  for( auto ix: range( nx)){
    for( auto iy: range( ny)){
      for( auto iz: range( nz)){
      
        printf( "%g ", (factor*grid.rgrid.value( ix,iy,iz)).value);
        ++ip;
        if (ip == 3){
          printf( "\n");
          ip = 0;
        }
  }}}
  if (ip != 0){
    printf( "\n");
  }
  
  printf( "attribute \"dep\" string \"positions\"\nobject \"regular positions regular connections\" class field\ncomponent \"positions\" value 1\ncomponent \"connections\" value 2\ncomponent \"data\" value 3\n");
}
}

int main( int argc, char* argv[]){
  //test_partitioned_atoms();
  main1( argc, argv);
}
