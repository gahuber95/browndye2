#pragma once
/*
 * chain_collision_interface.hh
 *
 *  Created on: Jul 19, 2016
 *      Author: root
 */


#include "../../structures/atom.hh"
#include "chain_state_atom.hh"
#include "../../lib/vector.hh"
#include "../../transforms/transform.hh"
#include "../../transforms/blank_transform.hh"

namespace Browndye{

class Chain_State;

// assume that there are no lower cell levels; no splitting takes place.
class Chain_Col_Interface{
public:
  typedef Vector< Chain_State_Atom, Atom_Index> CAtoms;
  typedef Vector< Atom, Atom_Index> Atoms;
    [[maybe_unused]] typedef Pos Position;
  typedef Atom_Index Index;
  typedef ::Length Length;

  struct Container{
    const Atoms& atoms; // will be redundant
    CAtoms& catoms;
    Chain_State& chain;
    Chain_Index ic;
    Group_Index ig;
    
    Container( Chain_State& _chain, const Atoms& _atoms, CAtoms& _catoms, Chain_Index _ic, Group_Index _ig):
       atoms(_atoms), catoms(_catoms), chain(_chain), ic(_ic), ig(_ig){}

    Container( Container&& other) noexcept:  atoms( other.atoms), catoms( other.catoms), chain( other.chain), ic( other.ic), ig( other.ig){}

    // debug
    [[nodiscard]] Atom_Index size() const noexcept;
  };

  typedef Atom_Index Ref; // single reference to atom
  typedef Vector< Atom_Index, Atom_Index> Refs; // references to atoms in Container
  typedef Blank_Transform Transform; // translation and rotation
  typedef Atom_Index size_type; // integer index to atoms
    [[maybe_unused]] typedef decltype( Atom_Index{} - Atom_Index{}) difference_type;

  static
  void clear_transform( Container&, Refs&){}

  static
  void does_have_near_interaction( Container&);

  static constexpr bool is_changeable = false;
  static constexpr bool is_chain = true;

  static
  void copy_references( Container& cont, Refs& arefs){
    // eventuall do away with this redundancy
    auto& atoms = cont.atoms;
    auto n = atoms.size();
    arefs.resize( n);
    for( auto i = Atom_Index{0}; i<n; i++) {
      arefs[i] = i;
    }
  }

  static
  void copy_references( const Container&, const Refs& arefs0,
                        Refs& arefs1){

    auto n = arefs0.size();
    arefs1.resize( n);
    for( auto i: range( n))
      arefs1[i] = arefs0[i];
  }

  static
  size_type size( const Container& cont){
    auto& atoms = cont.atoms;
    return atoms.size();
  }

  static
  Atom_Index size( const Container&, const Refs& refs){
    return refs.size();
  }

  static
  Pos position( const Container& cont, const Refs& refs, Atom_Index i){
    auto& catoms = cont.catoms;
    return catoms[refs[i]].pos;
  }

  static
  Pos position( const Container& cont, Ref i){
    auto& catoms = cont.catoms;
    return catoms[i].pos;
  }

  static
  Pos transformed_position( const Blank_Transform&, Pos pos){
    return pos;
  }

  static
  Length radius( const Container& cont, const Refs& refs, Atom_Index i){
    auto& atoms = cont.atoms;
    return atoms[refs[i]].interaction_radius;
  }

  template< class F>
  [[maybe_unused]] static
  void partition_contents( const Container& cont,  const Refs& refs, const F& f, Refs& refs0, Refs& refs1);

  static
  void empty_container( Container&, Refs& refs){
    refs.clear();
  }

  static
  void swap( Container&, Refs& refs, Atom_Index i, Atom_Index j){
    auto ref = refs[i];
    refs[i] = refs[j];
    refs[j] = ref;
  }

  // not needed for Collision_Detector, but for Browndye force calculations
  // needed????
  static
  Atom_Type_Index atom_type( const Container& cont, Ref i){
    auto& atoms = cont.atoms;
    return atoms[i].type;
  }

  static
  Vec3< Length> transformed_position( const Container& cont, const Blank_Transform, Ref i){
    auto& catoms = cont.catoms;
    return catoms[i].pos;
  }

  static
  Length physical_radius( const Container& cont, Ref i){
    auto& atoms = cont.atoms;
    return atoms[i].radius;
  }
  
  static
  Charge charge( const Container& cont, Ref i){
    auto& atoms = cont.atoms;
    return atoms[i].charge;
  }

  static
  void add_force( Container& cont, Ref i, const Vec3< Force>& F){
    auto& catoms = cont.catoms;
    catoms[i].force += F;
  }
  
  static
  Atom_Index atom_number( const Container&, Ref){
    error( __FILE__, __LINE__, "should not get here");
    return Atom_Index{0};
  }
  
  static
  Length2 sasa( const Container& cont, Ref i){
    auto& atoms = cont.atoms;
    return atoms[i].sasa();
  }
  
};

template<class F>
[[maybe_unused]]
void Chain_Col_Interface::partition_contents( const Chain_Col_Interface::Container &,
                                              const Chain_Col_Interface::Refs &, const F &,
                                              Chain_Col_Interface::Refs &,
                                              Chain_Col_Interface::Refs &) {

    error( "chain_collision_interface", __LINE__, "not get here");
}

inline
Atom_Index Chain_Col_Interface::Container::size() const noexcept {
    return atoms.size();
}

}
