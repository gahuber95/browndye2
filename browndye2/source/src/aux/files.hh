#pragma once

#include "files_macros.hh"
#include <string>

#ifdef USING_POSIX
  #include <unistd.h>
#else
#ifdef USING_CPP_FILE
  #include <filesystem>
#else
#ifdef USING_EXP_CPP_FILE
  #include <experimental/filesystem>
#else
#ifdef USING_BOOST
  #include <boost/filesystem.hpp>
#endif
#endif
#endif
#endif

namespace Browndye{
namespace Orchestrator{  // someday give them their own namespace

#ifdef USING_POSIX
  //#include <unistd.h>
  typedef time_t File_Time;
  constexpr int ftype = 2; 

#else
#ifdef USING_CPP_FILE
  //#include <filesystem>
  namespace FS = std::filesystem;
  typedef FS::file_time_type::duration::rep File_Time;
  constexpr int ftype = 0; 
  
#else
#ifdef USING_EXP_CPP_FILE
  //#include <experimental/filesystem>
  namespace FS = std::experimental::filesystem;
  typedef FS::file_time_type::duration::rep File_Time;
  constexpr int ftype = 1; 

#else  
#ifdef USING_BOOST
  //#include <boost/filesystem.hpp>
  namespace FS = boost::filesystem;
  typedef std::time_t File_Time;
  inline constexpr int ftype = 3; 
#endif
#endif
#endif
#endif


File_Time file_time( const std::string& name);
bool file_exists( const std::string& name);
void remove_file( const std::string& name);
void rename_file( const std::string& from, const std::string& to);

}}
