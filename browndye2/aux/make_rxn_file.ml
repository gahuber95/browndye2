(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*  
Not called by bd_top.

Generates an XML file describing a reaction pathway from the output of 
make_rxn_pairs.  Take the following arguments with flags:

-pairs : file of reaction pairs (output from make_rxn_pairs)
-distance : distance required for reaction
-nneeded : number of pairs needed for reaction
*)


let pair_file_ref = ref ""
let distance_ref = ref nan
let n_needed_ref = ref 1000000
let state_from_ref = ref ""
let state_to_ref = ref ""
let mol0_ref = ref ""
let part0_ref = ref ""
let mol1_ref = ref ""
let part1_ref = ref ""
let rxn_name_ref = ref "";;
                                                     
Arg.parse [
  ("-pairs", Arg.Set_string pair_file_ref, "file of reaction pairs (output from make_rxn_pairs)");
  ("-distance", Arg.Set_float distance_ref, "distance required for reaction");
  ("-nneeded", Arg.Set_int n_needed_ref, "number of pairs needed for reaction");

  ("-rxn", Arg.Set_string rxn_name_ref, "name of reaction");
  
  ("-state_from", Arg.Set_string state_from_ref,
   "name of state before reaction");
  
  ("-state_to", Arg.Set_string state_to_ref,
   "name of state aftr reaction");
  
  ("-mol0", Arg.Tuple [Arg.String (fun s -> mol0_ref := s);
                       Arg.String (fun s -> part0_ref := s)],
   "Molecule 0, two names: group and (core or chain)");
  
  ("-mol1", Arg.Tuple [Arg.String (fun s -> mol1_ref := s);
                       Arg.String (fun s -> part1_ref := s)],
   "Molecule 1, two names: group and (core or chain)");
]
  (fun a -> Printf.fprintf stderr "needs args\n";)
  "Generates XML file describing reaction pathway from the output of make_rxn_pairs"
;;

let pair_file = !pair_file_ref
let distance = !distance_ref
let n_needed = !n_needed_ref
let mol0 = !mol0_ref
let mol1 = !mol1_ref
let part0 = !part0_ref
let part1 = !part1_ref
let rxn_name = !rxn_name_ref
let state_from = !state_from_ref
let state_to = !state_to_ref
let buf = Scanf.Scanning.from_file pair_file

module JP = Jam_xml_ml_pull_parser;;
let parser = JP.new_parser buf;;

let pr = Printf.printf;;

pr "<roottag>\n";
pr "  <first_state> %s </first_state>\n" state_from;
pr "  <reactions>\n";
pr "    <reaction>\n";
pr "      <name> %s </name>\n" rxn_name;
pr "      <state_before> %s </state_before>\n" state_from;
pr "      <state_after> %s </state_after>\n" state_to;
pr "      <criterion>\n";
pr "        <molecules>\n";
pr "          <molecule0> %s %s </molecule0>\n" mol0 part0;
pr "          <molecule1> %s %s </molecule1>\n" mol1 part1;
pr "        </molecules>\n";
pr "        <n_needed> %d </n_needed>\n" n_needed;;

JP.apply_to_nodes_of_tag parser "pair"
    (fun pnode ->
      JP.complete_current_node parser;
      let stm = JP.node_stream pnode in
	Scanf.bscanf stm " %d %d" 
	  (fun i0 i1 -> 
	    pr "        <pair>\n";
	    pr "          <atoms> %d %d </atoms>\n" i0 i1;
	    pr "          <distance> %f </distance>\n" distance;
	    pr "        </pair>\n";
	  )
    )
;;

pr "      </criterion>\n";;
pr "    </reaction>\n";;
pr "  </reactions>\n";;

(*
pr "  <states>\n";;

pr "    <state>\n";;
pr "      <number> 0 </number>\n";;
pr "      <rxn_from> 0 </rxn_from>\n";;
pr "    </state>\n";;

pr "    <state>\n";;
pr "      <number> 1 </number>\n";;
pr "      <rxn_to> 0 </rxn_to>\n";;
pr "    </state>\n";;


pr "  </states>\n";;
*)

pr "</roottag>\n";;
