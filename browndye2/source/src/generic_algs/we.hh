/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  This implements the Weighted_Ensemble_Dynamics::Sampler class, which
  is used to carry out weighed ensemble simulations.  Information about
  this class, including a description of the I class, is found
  in doc/we.html.

  For the multithreaded version, see "we_multithreaded.hh".
*/

#include <cstdio>
#include <iostream>
#include <cmath>
#include "../lib/vector.hh"
#include <list>
#include <queue>
#include <limits>
#include <algorithm>
#include <memory>
#include "../lib/index.hh"
#include "../global/error_msg.hh"


namespace Browndye::Weighted_Ensemble_Dynamics{

  using std::list;
  using std::make_tuple;
  using std::tuple;
  using std::max;
  
  const size_t max_int = std::numeric_limits< size_t>::max();
  
  //class Copy_Index_Tag{};
  //typedef Index< Copy_Index_Tag> Copy_Index; // check whether needed
  
  class Bin_Index_Tag{};
  typedef Index< Bin_Index_Tag> Bin_Index;
  
  class LBin_Index_Tag{};
  typedef Index< LBin_Index_Tag> LBin_Index;

  constexpr Bin_Index b0( 0), b1(1);
  constexpr auto db1 = b1 - b0;
  constexpr LBin_Index lb0( 0), lb1(1);
  constexpr auto dlb1 = lb1 - lb0;
  //constexpr Copy_Index c0( 0), c1( 1);
  //constexpr auto dc1 = c1 - c0;
 
  //####################################################
  template< class I>
  class Sampler{

  public:
    typedef typename I::Info Info;
    typedef typename I::Object Object;
    typedef typename I::Copy_Index Copy_Index;
   
    void set_number_of_moves_per_output( size_t);

    void initialize_objects();
    void update_fluxes();
    void renormalize_objects();
    void renormalize_objects_1D();

    // Function: f( Object, double);
    //template< class Function>
    //void apply_to_weighted_copies( Function& f);
  
    explicit Sampler( Info&);
    ~Sampler();

    void generate_bins( list< double>&);

    Info user_info(){
      return info;
    }

    void step_objects_forward();

  private:
    // data
    class Obj_Wrapper;
    typedef typename list< Obj_Wrapper>::iterator Obj_Itr;
    
    bool is_building_bins;
    list< Obj_Wrapper> wobjects;
    
    Copy_Index n_objects;
    size_t n_fluxes;
    
    Info& info;
    
    //double time;
    size_t n_steps_per_output;
    size_t istep;
    Vector< double> fluxes;
    
    const Copy_Index c0{ 0}, c1{ 1};
    
    // private functions
    Object new_object();
        
    std::optional< size_t> crossing( Object);
    
    double uniform();
     
    [[nodiscard]] Bin_Index number_of_bins() const;
    
    void get_duplicated_objects( Object, size_t n_copies, 
                                 list< Object>&);
    static
    void remove( Info&, Object);
  
    size_t number_of_bin_neighbors( size_t);
    
    bool have_population_reversal( size_t, double, size_t, double);

    // f( size_t)
    template< class Function>
    void apply_to_bin_neighbors( size_t, Function&);
    
    double reaction_coordinate( Object);

    [[nodiscard]] Vector< LBin_Index, Bin_Index> lumped_bin_info() const;
    
   
    void combine_small_copies( list< Obj_Wrapper>& wobjects_bin,
                                double ideal_wt);
    
    void duplicate_objects( list< Obj_Wrapper>& wobjects_bin,
                              double ideal_wt);
    
    [[nodiscard]] double ideal_weight([[maybe_unused]] const Vector< Bin_Index, LBin_Index>& nbins_in_lbin,

                         const Vector< double, LBin_Index>& lbin_wts,
                          LBin_Index ib) const;
      
    tuple< Vector< Copy_Index, LBin_Index>, Vector< double, LBin_Index> >
     lumped_bin_contents( const Vector< LBin_Index, Bin_Index>& lbin_info);
    
    tuple< bool, Vector< double>, Vector< Obj_Itr> >
    weights_and_iterators( size_t n_in_lbin_cur,
                                      double ideal_wt, Obj_Itr& itrb);
    
    [[nodiscard]] tuple< double, Vector< double> >
    cumulative_weights( const Vector< double>& wts) const;
    
    size_t copy_choice( const Vector< double>& cwts);
    
    static
    double round( double a);
    
    [[nodiscard]] Vector< Bin_Index, LBin_Index>
    lbin_centers( LBin_Index n_lbins,
                 const Vector< bool, Bin_Index>& is_forward_max) const;
       
    
    //################################
    // class definitions
    class Obj_Wrapper{
    public:
      Obj_Wrapper( Info& _info, Object _obj): info(_info),obj(_obj){}
      ~Obj_Wrapper(){
        remove( info, obj);
      }

      Obj_Wrapper( const Obj_Wrapper&) = delete;
      Obj_Wrapper& operator=( const Obj_Wrapper&) = delete;

      [[nodiscard]] double weight() const{
        return I::weight( info, obj);
      }
      
      void set_weight( double wt){
        I::set_weight( info, obj, wt);
      }
      
      [[nodiscard]] Bin_Index bin() const{
        auto ib = I::bin( info, obj);
        return Bin_Index( ib ? ib.value() : max_int);
      }

      Info& info;
      Object obj;
      Bin_Index ibin;
      LBin_Index lbin;
    };
    
    //###########################################
    class Bin_Neighbor_Getter{
    public:
      size_t n_nebors;
      Vector< size_t> nebors;

      Bin_Neighbor_Getter(){
        n_nebors = 0;
      }

      void operator()( size_t bin){
        nebors[ n_nebors] = bin;
        ++n_nebors;
      }

      void setup( Info& _info, size_t ib){
        size_t num_nebors = I::number_of_bin_neighbors( _info, ib);
        nebors.resize( num_nebors);
        n_nebors = 0;
        I::apply_to_bin_neighbors( _info, ib, *this);
      }

    };

    //###############################################
    class Object_Comparer{
    public:
      
      bool operator()( const Obj_Wrapper& obj0, Obj_Wrapper& obj1) const{
        if (obj0.ibin == obj1.ibin){
          return obj0.weight() < obj1.weight();
        }
        else{
          return obj0.ibin < obj1.ibin;
        }
      }
    };
    
    void sort_wobjects( std::list< Obj_Wrapper>& );
    
  };

  //#########################################################
  // ordered increasing according to bin, then weight
  template< class I>
  void Sampler<I>::sort_wobjects( std::list< Obj_Wrapper>& wobjs){
     
    wobjs.sort( Object_Comparer());
  }

  //#####################################
  template< class I>
  double Sampler<I>::round( double a){
    double low = floor( a);
    if (a > low + 0.5){ 
      return low + 1.0;
    }
    else{
      return low;
    }
  }

  //#########################################################
  template< class I>
  void Sampler< I>::set_number_of_moves_per_output( size_t n){
    n_steps_per_output = n;
  }

  //###################################
  template< class I>
  typename I::Object 
  Sampler< I>::new_object(){
    return I::new_object( info);
  }
  
  //#####################################################
  template< class I>
  void Sampler< I>::get_duplicated_objects( Object obj, 
                                                    size_t n_copies, 
                                                    list< Object>& dups){
    I::get_duplicated_objects( info, obj, n_copies, dups);
  }
  
  //#####################################################
  template< class I>
  std::optional< size_t> Sampler< I>::crossing( Object obj){
    return I::crossing( info, obj);
  }
  
  //#####################################################
  template< class I>
  void Sampler< I>::step_objects_forward(){
    I::step_objects_forward( info);
  }
  
  //#####################################################
  template< class I>
  Bin_Index Sampler< I>::number_of_bins() const{
    return Bin_Index( I::number_of_bins( info));
  }
  
  //#####################################################
  template< class I>
  void Sampler< I>::remove( Info& info, Object obj){
    I::remove( info, obj);
  }
 
  //#####################################################
  template< class I>
  size_t Sampler< I>::number_of_bin_neighbors( size_t ib){
    
    return I::number_of_bin_neighbors( info, ib);
  }

  //#####################################################
  template< class I>
  bool Sampler< I>::have_population_reversal( size_t ib0, double wt0, 
                                                      size_t ib1, double wt1){
    
    return I::have_population_reversal( info, ib0, wt0, ib1, wt1);
  }

  //#####################################################
  // f( size_t)
  template< class I>
  template< class Function>
  void Sampler< I>::apply_to_bin_neighbors( size_t ib, Function& f){
    
    I:: apply_to_bin_neighbors( info, ib, f);
  }

  //#####################################################
  template< class I>
  double Sampler< I>::uniform(){
    
    return I::uniform( info);
  }
  
  //#####################################################
  template< class I>
  double Sampler< I>::reaction_coordinate( Object obj){
    
    return I::reaction_coordinate( info, obj);
  }

  //################################################################
  template< class I>
  void Sampler< I>::update_fluxes(){

    for( auto& wobj: wobjects){
      Object obj = wobj.obj;
     
      auto icropt = crossing( obj);
      if (icropt){
        auto ic = icropt.value();
        fluxes[ ic] += wobj.weight();
      }
    }

    if (istep == n_steps_per_output){
      for( auto i: range( n_fluxes)){
        I::output( info, i, fluxes[i]);
      }
    
      fluxes.fill( 0.0);
      istep = 0;
    }
    else{
      ++istep;
    }
  }

  //###################################################################
  template< class I>
  Vector< Bin_Index, LBin_Index> 
  Sampler<I>::lbin_centers( LBin_Index n_lbins,
                            const Vector< bool, Bin_Index>& is_forward_max) const{
     
    auto n_bins = is_forward_max.size();
    Vector< Bin_Index, LBin_Index> lbins( n_lbins);
    auto i_lbin = lb0;
    for( auto i: range( n_bins)){
      if (is_forward_max[i]){
        lbins[i_lbin] = i;
        ++i_lbin;
      }
    }
    return lbins;
  }

  //#############################################################
  template< class I>
  Vector< LBin_Index, Bin_Index> Sampler<I>::lumped_bin_info() const{
   
    auto n_bins = number_of_bins();
    Vector< double, Bin_Index> bin_wts( n_bins, 0.0);
  
    for( auto& obj: wobjects){
      auto ib = obj.ibin;
      bin_wts[ib] += obj.weight();
    }
  
    double current_max_wt = bin_wts[b0];
    auto is_forward_max = initialized_vector( n_bins, [&]( Bin_Index i){
      if (bin_wts[i] > current_max_wt){
        current_max_wt = bin_wts[i];
        return true;
      }
      else{
        return (i == b0);
      } 
    });
    
    auto n_lbins = is_forward_max.folded_left( [&]( LBin_Index sum, bool ismx){
      return ismx ? sum + dlb1 : sum;
    }, lb0);
     
    auto lbins = lbin_centers( n_lbins, is_forward_max);
    
    auto bot_lbin = initialized_vector( n_lbins + dlb1,
                                       [&]( LBin_Index i){
      if (i == lb0){
        return b0;
      }
      else if (i == n_lbins){
        return n_bins;
      }
      else{
        return (lbins[i-dlb1] + (lbins[i] - b0))/2 + db1;
      }
    });
    
    Vector< LBin_Index, Bin_Index> lbin_info( n_bins, LBin_Index(max_int));
    for( auto ilb: range( n_lbins)){
      
      for( auto i: range( bot_lbin[ ilb], bot_lbin[ ilb + dlb1])){
        lbin_info[i] = ilb;
      }
    }
    
    return lbin_info;
  }
  
  //################################################
  template< class I>
  auto Sampler< I>::weights_and_iterators( size_t n_in_lbin_cur,
                                    double ideal_wt, Obj_Itr& itrb) ->
  tuple< bool, Vector< double>, Vector< Obj_Itr> > {
  
    Vector< double> wts; //( n_in_lbin_cur);
    Vector< Obj_Itr> itrs; //( n_in_lbin_cur, wobjects_bin.end());
    size_t nwts = 0;
  
    // Create next combined copy
    bool done = false;
    double totwt = 0.0;
    bool small_done = false;
    while (!done){
      double wt = itrb->weight();
  
      if (wt < 0.5*ideal_wt){
        totwt += wt;
        wts.push_back( wt);
        itrs.push_back( itrb);
        ++nwts;
        ++itrb;
        if (totwt > ideal_wt){
          done = true;
        }
      }
      else{
        small_done = true;
        done = true;
        if (totwt + wt < 1.5*ideal_wt){
          totwt += wt;
          wts.push_back( wt);
          itrs.push_back( itrb);
          ++nwts;
          ++itrb;
        }
      }
  
      if (nwts == n_in_lbin_cur){
        done = true;
      }
    }
    //itrs.push_back( wobjects_bin.end()); // needed?
    
    return make_tuple( small_done, std::move( wts), std::move( itrs));
  }
  
  //#########################################################
  template< class I>
  tuple< double, Vector< double> >
  Sampler<I>::cumulative_weights( const Vector< double>& wts) const{
           
    auto nwts = wts.size();
    Vector< double> cwts( nwts);
    cwts[0] = wts[0];
    for( auto iwt: range( 1,nwts)){
      cwts[ iwt] = cwts[ iwt-1] + wts[ iwt];
    }
    
    auto totwt = cwts.back();
    for( auto& cw: cwts){
      cw /= totwt;
    }
    return make_tuple( totwt, std::move( cwts));
  }
  
  //###########################################################
  template< class I>
  size_t Sampler<I>::copy_choice( const Vector< double>& cwts){
    size_t ichoice = 0;
    double choice = uniform();
    while( cwts[ichoice] < choice) ++ichoice;
    return ichoice;
  }
  
  //###########################################################
  template< class I>
  void Sampler<I>::combine_small_copies( list< Obj_Wrapper>& wobjects_bin,
                                         double ideal_wt){
   
    auto itrb = wobjects_bin.begin();
    
    bool small_done = false;
    while(!small_done){
       
      auto nleft = [&]() {
        size_t nl = 0;
        for( auto itr = itrb; itr != wobjects_bin.end(); ++itr){
          ++nl;
        }
       
        return nl;
      }();
      
      if (nleft < 2){
        small_done = true;
        break;
      }
      
      auto [sdone, wts, itrs] = weights_and_iterators( nleft,
                                                        ideal_wt, itrb);
      
      small_done = sdone;
            
      auto nwts = wts.size();
      if (nwts > 0){ 
        auto [totwt, cwts] = cumulative_weights( wts);
         
        auto ichoice = copy_choice( cwts);
        itrb = itrs[ ichoice];
        itrb->set_weight( totwt);   
          
        if (ichoice > 0){
          wobjects_bin.erase( itrs[0], itrs[ichoice]);
        }
        if (ichoice < nwts-1){
          auto eitr = itrs.back();
          ++eitr;
          wobjects_bin.erase( itrs[ ichoice+1], eitr);
        }
      
        ++itrb;
        
        auto wt_compare = [&]( const Obj_Wrapper& wobj0,
                              const Obj_Wrapper& wobj1){
          
          auto wt0 = wobj0.weight();
          auto wt1 = wobj1.weight();
          return wt0 < wt1;
        };
        
        wobjects_bin.sort( wt_compare);
      }
    }
  }
  
  //#################################################
  template< class I>
  void Sampler<I>::duplicate_objects( list< Obj_Wrapper>& wobjects_bin,
                                     double ideal_wt){
    
    for (auto itr = wobjects_bin.begin(); itr != wobjects_bin.end(); ++itr){
      auto& wobj = *itr;
      auto wt = wobj.weight();
      if (wt > 2*ideal_wt){ // 1.5?
        auto n_copies = (size_t)(wt/ideal_wt)-1; // round?
        wobj.set_weight( wt/(n_copies+1));  
        
        typedef list< Object> Obj_List;
        Obj_List objs;
        get_duplicated_objects( wobj.obj, n_copies, objs);
        for( auto obj: objs){
          auto jtr = wobjects_bin.emplace( itr, info, obj);
          auto& new_wobj = *jtr;
          new_wobj.ibin = wobj.ibin;
        }
      }
    }
  }
  
  //##############################################################
  template< class I>
  double Sampler<I>::ideal_weight( const Vector< Bin_Index, LBin_Index>& nbins_in_lbin,
                                  const Vector< double, LBin_Index>& lbin_wts,
                                    LBin_Index ib) const{
    
    auto n_bins = number_of_bins();
    
    auto idn = [&](){
      auto nobj = n_objects/Copy_Index(1);
      auto nbl = nbins_in_lbin[ib]/Bin_Index(1);
      auto nb = n_bins/Bin_Index(1);
      return (double)((nobj*nbl)/nb);
    }();
    
    auto ideal_n = max( 1.0, idn);
    return lbin_wts[ib]/ideal_n;
  }
  
  //########################################################
  template< class I>
  auto
  Sampler<I>::lumped_bin_contents( const Vector< LBin_Index, Bin_Index>& lbin_info)
      -> tuple< Vector< Copy_Index, LBin_Index>, Vector< double, LBin_Index> >{
    
    auto n_lbins = lbin_info.back() + dlb1;
    Vector< Copy_Index, LBin_Index> n_in_lbins( n_lbins, Copy_Index(0));
    Vector< double, LBin_Index> lbin_wts( n_lbins, 0.0);

    const auto dc1 = c1 - c0;
    for( auto& wobj: wobjects){
      n_in_lbins[ wobj.lbin] += dc1;
      lbin_wts[ wobj.lbin] += wobj.weight();
    }

    auto n_bins = number_of_bins();
   
    Vector< Bin_Index, LBin_Index> nbins_in_lbin( n_lbins, b0);
    for( auto i: range( n_bins)){
      ++(nbins_in_lbin[ lbin_info[i]]);
    }

    return make_tuple( move( n_in_lbins), std::move( lbin_wts));
  }
  
  //#################################################
  template< class I>
  void Sampler< I>::renormalize_objects_1D(){
      
    for( auto& wobj: wobjects){
      wobj.ibin = wobj.bin();
    }
      
    auto lbin_info = lumped_bin_info();  
    
    // assign each object a lumped bin
    for( auto& wobj: wobjects){
      wobj.lbin = lbin_info[ wobj.ibin];
    }
    
    sort_wobjects( wobjects);
    
    auto [n_in_lbins, lbin_wts] = lumped_bin_contents( lbin_info);
    
    auto n_bins = number_of_bins();
    
    auto n_lbins = n_in_lbins.size();
    Vector< Bin_Index, LBin_Index> nbins_in_lbin( n_lbins, Bin_Index(0));
    for( auto i: range( n_bins)){
      ++(nbins_in_lbin[ lbin_info[i]]);
    }
  
    Obj_Itr begin_itr;
    Obj_Itr end_itr = wobjects.begin();
    for( auto ib: range( n_lbins)){
      const auto n_in_lbin = n_in_lbins[ib];

      if (n_in_lbin == c0){
        continue;
      }
      
      auto ideal_wt = ideal_weight( nbins_in_lbin, lbin_wts, ib);
      
      begin_itr = end_itr;
      for( auto i: range( n_in_lbin)){
        (void)i;
        ++end_itr;
      }

      list< Obj_Wrapper> wobjects_bin;
      wobjects_bin.splice( wobjects_bin.end(), wobjects, begin_itr, end_itr);
      
      combine_small_copies( wobjects_bin, ideal_wt);
      if (ideal_wt > 0.0){
        duplicate_objects( wobjects_bin, ideal_wt);
      }
      
      wobjects.splice( end_itr, wobjects_bin, wobjects_bin.begin(),
                                     wobjects_bin.end());
    }
    
  }
  
  //##############################################################
  // don't trust yet
  template< class I>
  void Sampler< I>::renormalize_objects(){
  
    // lump bins if needed
    size_t n_bins = number_of_bins();
  
    std::queue< size_t> queu;

    Vector< double> bin_wts( n_bins, 0.0);

    for( auto& obj: wobjects){
      size_t ib = obj.bin();
      bin_wts[ib] += obj.weight();
    }

    // figure out which bins are centers of lumped bins
    Vector< size_t> lbin_info( n_bins, max_int);
    size_t n_lbins = 1;

    for( auto ib: range( n_bins)){
    //for (size_t ib = 0; ib < n_bins; ib++){
      Bin_Neighbor_Getter bng;
      bng.setup( info, ib);
      size_t n_nebors = bng.n_nebors;
    
      if (bin_wts[ib] > 0.0){
        bool is_main_bin = true;
        for( auto knb: range( n_nebors)){
        //for( size_t knb=0; knb < n_nebors; knb++){
          size_t inb = bng.nebors[ knb];
          if (have_population_reversal( ib, bin_wts[ib], 
                                        inb, bin_wts[inb])){
            is_main_bin = false;
            break;
          }
        }
        if (is_main_bin){
          lbin_info[ib] = n_lbins;
          ++n_lbins;
        }
      }
    }
  
    // assign bins to lumped bins
    lbin_info[0] = 0;
    for( auto ib: range( n_bins)){
    //for (size_t ib = 0; ib < n_bins; ib++){
      if (lbin_info[ib] != max_int){
        queu.push( ib);
      }
    }
  
    while( !queu.empty()){
      size_t ib = queu.front();
      queu.pop();
    
      Bin_Neighbor_Getter bng;
      bng.setup( info, ib);
      size_t n_nebors = bng.n_nebors;

      for( auto knb: range( n_nebors)){
      //for( size_t knb=0; knb < n_nebors; knb++){
        size_t inb = bng.nebors[ knb];
        if (lbin_info[inb] == max_int){
          lbin_info[inb] = lbin_info[ib];
          queu.push( inb);
        }
      }           
    }   
        
    sort_wobjects( wobjects);
    
    Vector< size_t> n_in_lbins( n_lbins, 0);
    Vector< double> lbin_wts( n_lbins, 0.0);

    for( auto& wobj: wobjects){
      auto ib = wobj.bin();
      n_in_lbins[ ib] += 1;
      lbin_wts[ ib] += wobj.weight();
    }

    Vector< size_t> nbins_in_lbin( n_lbins, 0);
    for( auto i: range( n_bins)){
    //for( size_t i=0; i<n_bins; i++){
      ++(nbins_in_lbin[ lbin_info[i]]);
    }

    Obj_Itr begin_itr;
    Obj_Itr end_itr = wobjects.begin();
    for( size_t ib=0; ib<n_lbins; ib++){
      size_t n_in_lbin = n_in_lbins[ib];

      if (n_in_lbin == 0)
        continue;

      auto ideal_n = max( 1.0, (double)(n_objects*nbins_in_lbin[ib])/n_bins);
      auto ideal_wt = lbin_wts[ib]/ideal_n;


      begin_itr = end_itr;
      for( size_t i=0; i<n_in_lbin; i++) ++end_itr;
    
      list< Obj_Wrapper> wobjects_bin;
      wobjects_bin.splice( wobjects_bin.end(), wobjects, begin_itr, end_itr);
      auto itrb = wobjects_bin.begin();

      // Combine small copies 
      bool small_done = false;
      while(!small_done){      

        Vector< double> wts( n_in_lbin);
        Vector< Obj_Itr> itrs( n_in_lbin, wobjects_bin.end());
        size_t nwts = 0;
      
        // Create next combined copy
        bool done = false;
        double totwt = 0.0;
        while (!done){
          double wt = itrb->weight();
          if (wt < 0.5*ideal_wt){
            totwt += wt;
            wts[nwts] = wt;
            itrs[nwts] = itrb;
            ++nwts;
            ++itrb;
            if (totwt > ideal_wt)
              done = true;
          }
          else{
            small_done = true;
            done = true;
            if (totwt + wt < 1.5*ideal_wt){
              totwt += wt;
              wts[nwts] = wt;
              itrs[nwts] = itrb;
              ++nwts;
              ++itrb;
            }
          }
          if (nwts == n_in_lbin)
            small_done = true;
        }

        // Cumulative weights
        if (nwts > 0){
          Vector< double> cwts( nwts);
          cwts[0] = wts[0];
          for( size_t iwt=1; iwt<nwts; iwt++)
            cwts[ iwt] = cwts[ iwt-1] + wts[ iwt];
          for( size_t iwt=0; iwt<nwts; iwt++)
            cwts[ iwt] /= totwt;
        
          size_t ichoice = 0;
          double choice = uniform();
          while( cwts[ichoice] < choice) ++ichoice;
          itrb = itrs[ ichoice];
          itrb->set_weight( totwt);

          if (ichoice > 0)
            wobjects_bin.erase( itrs[0], itrs[ichoice]);
          if (ichoice < nwts-1){
            auto eitr = itrs[ nwts-1];
            ++eitr;
            wobjects_bin.erase( itrs[ ichoice+1], eitr);
          }
        
          ++itrb;
          
          sort_wobjects( wobjects_bin);
        }
      }

      for (auto itr = wobjects_bin.begin(); itr != wobjects_bin.end(); ++itr){
        auto& wobj = *itr;
        auto wt = wobj.weight();
        if (wt > 1.5*ideal_wt){
          size_t n_copies = (size_t)(round( wt/ideal_wt))-1; 
          wobj.set_weight( wt/(n_copies+1));

          typedef list< Object> Obj_List;
          Obj_List objs;
          get_duplicated_objects( wobj.obj, n_copies, objs);
          
          for( auto& obj: objs){
            wobjects_bin.emplace( itr, info, obj);
          }
        }
      }
      wobjects.splice( end_itr, wobjects_bin, wobjects_bin.begin(),
                       wobjects_bin.end());
    }
  }
  
  //##############################################################
  // constructor
  template< class I>
  Sampler< I>::Sampler( Info& _info): info( _info){
   
    n_steps_per_output = 1;
    istep = 0;

    is_building_bins = false;
    
    n_objects = I::number_of_objects( info);
    n_fluxes = I::number_of_fluxes( info);
    fluxes.resize( n_fluxes);
  }

  //########################################
  // destructor
  template< class I>
  Sampler< I>::~Sampler()= default;
  
  template< class I>
  void Sampler< I>::initialize_objects(){
   
    for( auto i: range( n_objects)){
      (void)i;
      Object obj = new_object();
      wobjects.emplace_back( info, obj);
    }
  }
  
  //####################################
  template< class I>
  struct Obj_W_Coord{
    typename I::Object obj;
    double rxn_coord;
  };

  //################################################
  template< class I>
  class OWC_Less_Than{
  public:
    typedef Obj_W_Coord< I> OWC;

    bool operator()( const OWC& owc0, const OWC& owc1) const{
      return owc0.rxn_coord < owc1.rxn_coord;
    }  
  };

  //################################################################
  // partitions initially empty. At end, reaction coordinates are
  // ordered from highest to zero 
  template< class I>
  void Sampler< I>::generate_bins( list< double>& partitions){
    typedef Obj_W_Coord< I> OWC;
    typedef OWC_Less_Than< I> OWC_LT;

    Vector< OWC, Copy_Index> objs_w_coord( n_objects);
    for( auto i: range( n_objects)){
      Object obj = new_object();

      OWC& owc = objs_w_coord[i];
      owc.obj = obj;
      owc.rxn_coord = reaction_coordinate( obj);
    }

    while (true){
      step_objects_forward();

      for( auto i: range( n_objects)){
        OWC& owc = objs_w_coord[i];
        Object obj = owc.obj;
        owc.rxn_coord = reaction_coordinate( obj);
      } 

      std::sort( objs_w_coord.begin(), objs_w_coord.end(), OWC_LT());

      Copy_Index nkeep{ (4*(n_objects/c1))/5};
      auto ndiscard = n_objects - (nkeep - c0);
      
      for( auto i: range( ndiscard)){
        auto id = i + (nkeep - c0);
        Object obj = objs_w_coord[ id].obj;
        remove( info, obj);
      
        Copy_Index ik{ size_t((nkeep/c1)*uniform())};

        list< Object> objs;
        get_duplicated_objects( objs_w_coord[ik].obj, 1, objs);
      
        objs_w_coord[ id].obj = objs.front();
      }

      double rxn_coord = objs_w_coord.front().rxn_coord;

      if (rxn_coord <= 0.0){
        break;
      }

      if (!partitions.empty()){
        printf( "rxn coord %g\n", rxn_coord);
        fflush( stdout);
      }

      if (partitions.empty() || rxn_coord < partitions.front())
        partitions.push_front( rxn_coord);
    }
    //partitions.push_front( 0.0);
  }

}

