/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Staged_Simulator
*/
#include <string> // debug
#include "staged_simulator.hh"
#include "../input_output/trajectory_outputter.hh"
#include "stager.hh"

//**************************************************************
// Outputting code

namespace Browndye{
  
//************************************************************************
// Movement code

typedef std::lock_guard< std::mutex> Lock;

Stager Staged_Simulator::run_thread( System_State* statep){
  try{
    return run_individual( *statep);
  }
  catch (...){
    Lock lock( mutex);
    except = std::current_exception();
    has_error = true;
    return Stager( statep->common());
  }
}

//#########################################################
void Staged_Simulator::run(){
  
  n_trajs_completed = 0;
  n_trajs_started = 0;
 
  auto rt = [&]( System_State* statep) -> Stager{
    return run_thread( statep);
  };
  
  auto async_f = [&]( System_State* statep){
    return std::async( std::launch::async, rt, statep);
  };

  Vector< std::future< Stager>, Sys_Copy_Index> results;
  for( auto& item: states){
    auto& state = *(item.second);
    results.emplace_back( async_f( &state));
  }
  
  if (has_error){
    rethrow_exception( except);
  }
    
  Vector< Stager, Sys_Copy_Index> stagers;
  for( auto& res: results){
    stagers.emplace_back( res.get());
  }
  
  auto total_stager = stagers.folded_left( added);
  
  auto n = total_stager.distances.size();
  
  Vector< Inv_Length, Bin_Index> left_cAs( n);
  for( auto i: range( deced(n))){
    auto n_crosses = total_stager.n_crosses[i][0];
    auto counts = total_stager.counts[i];
    auto fsum = total_stager.flux_sums[i][0];
    left_cAs[i] = (((double)n_crosses)/counts)*fsum;
  }

  Vector< Inv_Length, Bin_Index> rite_cAs( n);
  for( auto i: range( Bin_Index(1), n)){
    auto n_crosses = total_stager.n_crosses[i][1];
    auto counts = total_stager.counts[i];
    auto fsum = total_stager.flux_sums[i][1];
    rite_cAs[i] = (((double)n_crosses)/counts)*fsum;
  }
  
  auto prvec = [&]( const auto& v){
    for( auto x: v){
      std::cout << x << ' ';
    }
    std::cout << std::endl;
  };
  
  // output for now
  {
    Lock lock( mutex);
    
    std::cout << "counts\n";
    prvec( total_stager.counts);
  
    std::cout << "left cAs\n";
    prvec( left_cAs);
  
    std::cout << "right cAs\n";
    prvec( rite_cAs);
  
    std::cout << "times\n";
    prvec( total_stager.times);
  
    std::cout << "crossings\n";
    prvec( total_stager.n_crosses);
  }
}

//##########################################################
Stager Staged_Simulator::run_individual( System_State& state){
  
  state.set_initial_state();
  Stager stager( state.common());  
  
  {
    Lock lock( mutex);
    ++n_trajs_started;
  }
  
  run_trajectory( state, stager);
  
  if (!has_error){
    Lock lock( mutex);
    n_bd_steps += state.n_bd_steps;
  }
  
  return stager;
}

//#########################################################################################
void Staged_Simulator::run_trajectory( System_State& state, Stager& stager){
       
  auto outputter = new_trajectory_outputter( state);
    
  auto& fate = state.fate;
  fate = System_Fate::Undefined;
  
  bool finished = false;
  while (!finished){
 
    if (outputter && 
        (state.n_full_steps % n_steps_per_output == 0)){
      outputter->output_state( state);
      state.t_since_output = Time( 0.0);
    }     
    
    state.do_large_step( Thread_Index());
   
    if (has_error){
      break;
    }
           
    auto [reject,ib] = stager.add_data_reject( state);
    if (reject){
      state.restore();
    }
    else{
      state.ibin = ib;
    }
    
    finished = state.n_full_steps >= max_n_steps;
  }
}

//**************************************************************
// Initialization code

namespace JP = JAM_XML_Pull_Parser;
using std::tie;

// constructor
Staged_Simulator::Staged_Simulator( JP::Node_Ptr node): Single_Traj_Simulator( node){

  n_trajs = checked_value_from_node< size_t>( node, "n_trajectories");
  
  auto get_val = [&]( const std::string& tag, auto& val){
    typedef std::decay_t< decltype(val)> T;
    auto valo = value_from_node<T>( node, tag);
    if (valo){
      val = *valo;
    }
  };
  
  get_val( "max_n_steps", max_n_steps);
  
  std::string dors;
  get_val( "print_rng_state", dors);
  //do_output_rng_state = (dors == std::string("yes"));
  common->do_staged = true;
}
}
