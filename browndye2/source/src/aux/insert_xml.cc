// insert_xml file0 tag file1
// Inserts file1 into the given tag of file0. Puts it at the end of the first
// occurence, and outputs to standard output

#include "../xml/jam_xml_pull_parser.hh"

using namespace Browndye;
namespace JP = JAM_XML_Pull_Parser;
using std::string;
using std::cout;

void psp( int indent){
  for( auto i: range( indent)){
    (void)i;
    cout << ' ';
  }
}

//################################################################
void print( JP::Node_Ptr node, const string& tag,
            const Vector< string>& text, int indent, bool& done){

  const int iinc = 2;

  auto& ttag = node->tag();
  psp( indent);
  auto& data = node->data();
  cout << '<' << ttag;
  
  auto& attrs = node->attributes();
  auto& dict = attrs.dict;
  
  if (!dict.empty()){
    cout << ' ';
  }
  
  for( auto& item: dict){
    auto& key = item.first;
    auto& value = item.second;
    cout << key << "=\"" << value << "\" ";
  }
  
  cout << "> ";
  
  for (auto& item: data){
    cout << item << ' ';
  }
  
  bool no_data = data.empty();
  
  if (no_data){
    cout << std::endl;
  }
  
  for( auto child: node->all_children()){
    print( child, tag, text, indent + iinc, done);
  }
  
  if (!done && ttag == tag){
    if (!no_data){
      error( "cannot insert into tag with free data");
    }
    done = true;
    
    for( auto& line: text){
      psp( indent + iinc);
      cout << line << std::endl;
    }
  }
  
  if (no_data){
    psp( indent);
  }
  cout << "</" << ttag << ">\n";
}

//#######################################################################
void doit( JP::Node_Ptr node, const string& tag,
            const Vector< string>& text){
  
  node->complete();
  bool done = false;
  print( node, tag, text, 0, done);
}

//##########################################################
Vector< string> inserted_text( const string& file){
  
  std::ifstream input( file);
  Vector< string> res;
  
  for (string line; std::getline( input, line); ) {
    res.push_back( line);
  }
  
  return res;
}

//######################################################
int main( int argc, char *argv[]){
  
  if (argc != 4){
    error( "wrong number of arguments");
  }
  
  std::string file0( argv[1]), tag( argv[2]), file1( argv[3]);
  
  std::ifstream input( file0);
  JP::Parser parser( input, file0);
  auto top = parser.top();
  
  auto text = inserted_text( file1);
  doit( top, tag, text);
  
}