(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)
(* 
Convenience function.  Generates a test charge for each sphere.
Uses standard input and output.
*)


Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates eff charges for charged residues using the SDA model"
;;


let pr = Printf.printf;;
  
pr "<root>\n";;
    
let total_q = ref 0.0;;
  
module H = Hashtbl;;
  
let dict = H.create( 10);;
  
let h = 0.5 and mh = -0.5 and mo = -1.0;;
(
  (* atomic pqr *)
  H.add dict ("N" ,"NTR")  1.0;
  H.add dict ("NZ" ,"LYS")  1.0;
  H.add dict ("NH1" ,"ARG")  0.5;
  H.add dict ("NH2" ,"ARG")  0.5;
  H.add dict ("OD1" ,"ASP")  mh;
  H.add dict ("OD2" ,"ASP")  mh;
  H.add dict ("OE1" ,"GLU")  mh;
  H.add dict ("OE2" ,"GLU")  mh;
  H.add dict ("OT1" ,"CTR")  mh;
  H.add dict ("OT2" ,"CTR")  mh;
  H.add dict ("O1" ,"CTR")  mh;
  H.add dict ("O2" ,"CTR")  mh;
  H.add dict ("O" ,"CTR")  mh;
  H.add dict ("OXT", "CTR")  mh;

  (* coffdrop *)
  H.add dict ("NG", "ARG") 1.0;
  H.add dict ("OG", "ASP") mo;
  H.add dict ("OG", "GLU") mo;
  H.add dict( "NG", "LYS") 1.0;
  H.add dict( "NG", "HIP") 1.0;
);; 


 Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin
   ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
     ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
       let q = 
         match H.find_opt dict (aname, rname) with
         | Some qq -> qq
         | None -> 0.0

       in
       if (not (q = 0.0)) then(
	 pr "  <point>\n";
	 pr "    <residue> %s </residue>\n" rname;
	 pr "    <number> %d </number>\n" rnumber;
	 pr "    <atom_type> charge_center </atom_type>\n";
         pr "    <x> %g </x> <y> %g </y> <z> %g </z>\n" x y z;
	 pr "    <charge> %g </charge>\n" q;
	 pr "  </point>\n";
	 total_q := !total_q +. q;
       )
   )
;;
 
pr "  <total_charge> %g </total_charge>\n" !total_q;;

pr "</root>\n";;

