/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Transform class.

*/

#include <cassert>
#include "transform.hh"
#include "../lib/inverse3x3.hh"
#include "../lib/rotations.hh"
#include "../lib/sq.hh"

namespace Browndye{

Transform Transform::inverse( ) const{
   
  auto inv_R = Browndye::inverse( R);
  auto inv_t = -1.0*(inv_R*t);  
  return Transform( inv_R, inv_t);
}

//#######################################################
// later, convert to use SLERP interpolation
Transform Transform::fractional_tform( double frac) const{
    
  if (debug){
    assert( 0.0 < frac && frac <= 1.0);
  }
  
  auto rot = rotation();
  auto q0 = quat_of_mat( rot);
  
  auto qval0 = sqrt( sq( q0[1]) + sq( q0[2]) + sq( q0[3]));
  auto phi0 = 2.0*asin( qval0);
  Array< double, 3> qxyz{ q0[1], q0[2], q0[3]};
  auto u = normed( qxyz);
  
  auto phi = frac*phi0;
  
  auto hphi = 0.5*phi;
  auto qval = sin( hphi);
  
  auto uq = qval == 0.0 ? Array< double,3>( Zeroi{}) : qval*u;
  Array< double, 4> q{ cos( hphi), uq[0],uq[1],uq[2]};
  
  auto frot = mat_of_quat( q);
  
  auto trans = translation();
  auto ftrans = frac*trans;
  
  return Transform( frot, ftrans);
}

//#############################################################
std::tuple< double, double> slerp_coefs( double phi, double lam){
  
  auto lamc = 1 - lam;
  if (phi > 0.1){
    auto sinph = sin( phi);
    auto c0 = sin( lamc*phi)/sinph;
    auto c1 = sin( lam*phi)/sinph;
    return std::make_tuple( c0,c1);
  }
  else{
    auto c0 = lamc;
    auto c1 = lam;
    return std::make_tuple( c0,c1);
  }
}

//#################################################################
Transform interped_tform( const Transform& tform0,
                           const Transform& tform1, double lam){
  
  auto rot0 = tform0.rotation();
  auto q0 = quat_of_mat( rot0);
  
  auto rot1 = tform1.rotation();
  auto q1 = quat_of_mat( rot1);
  
  auto cosph = dot( q0,q1);
  auto phi = acos( cosph);
  
  auto [c0,c1] = slerp_coefs( phi, lam);
  auto q = c0*q0 + c1*q1;
  auto rot = mat_of_quat( q);
  
  auto trans0 = tform0.translation();
  auto trans1 = tform1.translation();
  
  auto trans = (1-lam)*trans0 + lam*trans1;
  return Transform( rot, trans);
}

}

//##################################################
/*
 
Turn into unit test

namespace{
  using namespace Browndye;
  
  class Iface{
  public:
    typedef std::mt19937_64 Random_Number_Generator;
    
    static
    double gaussian( std::mt19937_64& gen){
      std::normal_distribution< double> gauss;
      return gauss( gen);
    }
  };
  
  void main0(){
    std::mt19937_64 gen;
    
    auto rot0 = random_rotation< Iface>( gen);
    //Mat3< double> rot0 = id_matrix<3>();
    std::uniform_real_distribution< double> uni;
    
    auto trans0 = initialized_array< 3>([&]( size_t){
      return Length( 0.0);
    });
   
    Transform tform0( rot0, trans0);
    
    auto rot1 = random_rotation< Iface>( gen);
    
    auto trans1 = initialized_array< 3>([&]( size_t){
      return Length( 0.0);
    });
    
    Transform tform1( rot1, trans1);
    
    const double lam = 1.0/3;
    
    auto tformh = interped_tform( tform0, tform1, lam);
    
    auto dform = tformh*tform0.inverse();
    auto new_tform = dform*dform*dform*tform0;
   
    auto new_rot = new_tform.rotation();
    auto new_trans = new_tform.translation();
   
    //std::xout << trans1 << '\n';
    //std::xout << new_trans << '\n';
   
    for( auto& row: rot1){
      std::xout << row << '\n';
    } 
    std::xout << '\n';
    
    for( auto& row: new_rot){
      std::xout << row << '\n';
    } 
    std::xout << '\n';
  }
}

//########################################
 Turn into unit test
namespace {

using namespace Browndye;

void main0(){
  Mat3< double> rot( Zeroi{});
  
  rot[0][1] = -1.0;
  rot[1][0] = 1.0;
  rot[2][2] = 1.0;
  
  Length L1{ 1.0}, L0{ 0.0};
  Pos trans{ L0,L0,L1};
  
  Transform tform( rot, trans);
  
  auto htform = tform.fractional_tform( 0.5);
  
  auto hrot = htform.rotation();
  
  std::xout << "rotation\n";
  for( auto row: hrot){
    std::xout << row << '\n';
  }
  
  auto htrans = htform.translation();
  std::xout << "translation " << htrans << '\n';
}

}
*/


