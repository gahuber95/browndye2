(* 
Used internally

polynomial m x: evaluates the m_th Legendre polynomial at x

sum coefs x: evaluates Sum_i=0 to n (coefs[i]*legendre_polynomial_i(x))
  where n is the length of coefs
*)

(* polynomial m x *)
val polynomial : int -> float -> float

(* sum coefs x *)
val sum : float array -> float -> float
