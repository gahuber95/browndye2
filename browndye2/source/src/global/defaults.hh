#pragma once

#include "../lib/units.hh"
#include "limits.hh"

namespace Browndye{
  
  namespace Default{
    const double solute_dielectric = 4.0;
    const double solvent_dielectric = 78.0;
    const Energy kT{ 1.0};
    const double relative_viscosity = 1.0;
    const Length debye_length{ large};
    const Length solvent_radius{ 1.5};
    const double solvation_parameter = 1.0;
  }
  
  constexpr bool compute_energy = false;
}
