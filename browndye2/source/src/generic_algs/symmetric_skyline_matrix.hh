#pragma once

#include "../lib/skyline_cholesky.hh"
#include "ssm_interface.hh"
#include "../lib/array.hh"

namespace Browndye{

//#############################################
template< class T, class Index>
class Symmetric_Skyline_Matrix{
public:

  //typedef decltype( Index(1) - Index(0)) Idiff;
  typedef T value_type;
  
  Symmetric_Skyline_Matrix copy() const;
  
  void check_indices( Index i, Index j) const{
    if (true){ // debug
      if (i < j)
        check_indices( j,i);
      else{
        if (Index(0) + (i - j) > widths[i]){
          error( "banded matrix: out of bounds\n",
                 "width ", widths[i]," i ", i," j ", j);
        }
      }
    }
  }

  bool exists( Index i, Index j) const{
    if (j <= i){
      return (j >= i - (bandwidth(i) - Index(0)));
    }
    else{
      return exists( j,i);
    }
  }
  
  T& operator()( Index i, Index j){
    check_indices( i,j);
    if (j > i){
      return (*this)(j,i);
    }
    else {
      return data[i][Index(0) + (i-j)];
    }
  }
  
  const T& operator()( Index i, Index j) const{
    check_indices( i,j);
    if (j > i){
      return (*this)( j,i);
    }
    else {
      return data[i][Index(0) + (i-j)];
    }
  }
  
  // width is 0 for diagonal matrix
  void initialize( const Index& _n, Vector< Index, Index>&& _widths){
    n = _n;
    widths = std::move( _widths);
    data.resize( n);
    for (auto i = Index(0); i < n; i++){
      data[i].resize( widths[i] + (Index(1) - Index(0)));
    }
    factored = false;
  }
  
  void factor( bool& success);
  
  template< class B, class X>
  void solve( const Vector< B, Index>& b, Vector< X, Index>& x) const;
  
  void zero(){
    for (Index i = Index(0); i < n; i++){
      data[i].fill( T{0.0});
    }
  }
  
  Index size() const{
    return n;
  }
  
  void reset(){
    factored = false;
  }
  
  Index bandwidth( const Index& i) const{
    return widths[i];
  }
   
  template< class ... M> 
  void error( M ... args) const{
    Browndye::error( args...);
  }
       
  void copy_from( const Symmetric_Skyline_Matrix& other);     
       
private:
  bool factored = false; 
  Index n = Index(0);
  
  Vector< Index, Index> widths;
  Vector< Vector<T, Index>, Index> data; // lower triangle stored
};

template< class T, class Index>
auto Symmetric_Skyline_Matrix< T, Index>::copy() const ->
   Symmetric_Skyline_Matrix
{
  Symmetric_Skyline_Matrix ssm;
  ssm.n = n;
  ssm.data = data.copy();
  ssm.factored = factored;
  ssm.widths = widths.copy();
  return ssm;
}

//####################################################
template< class T, class Index>
void Symmetric_Skyline_Matrix< T, Index>::factor( bool& success){
  if (factored){
    error( "Symmetric Skyline Matrix: already factored");
  }

  typedef decltype( sqrt( T(1.0))) Sqrt_T;
  auto& dmat = 
    reinterpret_cast< Symmetric_Skyline_Matrix< Sqrt_T, Index>& >(*this); 

  Skyline_Cholesky::decompose< SSM_Interface< T, Index> >( *this, dmat, success);
  
  factored = true;
}

//###########################################################
template< class T, class Index>
template< class B, class X>
void Symmetric_Skyline_Matrix< T, Index>::solve( const Vector< B, Index>& b, 
                                                 Vector< X, Index>& x) const{
  if (!factored){
    error( __FILE__, __LINE__, "not yet factored");
  }

  typedef decltype( sqrt( T(1.0))) Sqrt_T;
  auto& dmat = 
    reinterpret_cast< const Symmetric_Skyline_Matrix< Sqrt_T, Index>& >(*this); 

  Skyline_Cholesky::solve< SSM_Interface< T, Index> >( dmat, b, x);
}

//######################################################
template< class T, class Index>
void Symmetric_Skyline_Matrix< T, Index>::
    copy_from( const Symmetric_Skyline_Matrix& other){
      
  factored = other.factored;
  data = other.data.copy();
  n = other.n;
  widths = other.widths.copy();
}     

}
