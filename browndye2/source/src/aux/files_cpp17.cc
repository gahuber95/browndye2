#include "./files.hh"

#if (FTYPE != 1)

namespace Browndye{
namespace Orchestrator{

using std::string;

//############################
File_Time file_time( const string& name){
  
  auto path = FS::current_path();
  path /= name;
  auto ltw = FS::last_write_time( path);
  return ltw.time_since_epoch().count();
}

//############################
bool file_exists( const string& name){
  auto path = FS::current_path();
  path /= name;
  return FS::exists( path);
}

//######################################
void remove_file( const std::string& name){
  auto path = FS::current_path();
  path /= name;
  FS::remove( path);
}

//###############################################################33
void rename_file( const std::string& from, const std::string& to){
  FS::path pfrom( from);
  FS::path pto( to);
  FS::rename( pfrom, pto);
}
}
}
#endif
