#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Defines interface class to Mover_Base in "multithread_mover_base.hh".

*/
#include <cstddef>

namespace Browndye{

namespace Multithread{

template< class I>
class Mover_Base;

template< class I>
class Mover;

template< class I>
class MT_Interface{
public:
  
  typedef typename I::Exception Exception;
  typedef Mover< I>* Objects_Ref;
  typedef size_t Object_Ref;

  static void do_move( Mover_Base< MT_Interface<I> >&, Objects_Ref, size_t, [[maybe_unused]] Object_Ref);

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects_Ref objects);

  static size_t size( Objects_Ref objects);

  static void set_null( Objects_Ref& objects);

  static bool is_null( Objects_Ref objects);

};

template< class I>
template< class Func>
void MT_Interface< I>::
apply_to_object_refs( Func& f, 
                      Mover< I>* mmover){

  const size_t n = mmover->mover.n_threads();
  for( size_t i=0; i<n; i++)
    f( i);
}

template< class I>
size_t MT_Interface< I>::size( Mover< I>* mmover){
  return mmover->n_threads;
}

template< class I>
void MT_Interface< I>::set_null( Mover< I>*& mmover){
  mmover = nullptr;
}

template< class I>
bool MT_Interface< I>::is_null( Mover< I>* mmover){
  return (mmover == nullptr);
}

template< class I>
void MT_Interface< I>::do_move( Mover_Base< MT_Interface<I> >&,
                                Mover< I>* mmover,
                                size_t ithread, [[maybe_unused]] size_t idum){
  mmover->thread_move( ithread);
}
}
}
