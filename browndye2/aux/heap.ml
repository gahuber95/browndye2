(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* used internally. Implements a mutable heap data structure. *)

let len = Array.length


type 'a t = {
  n0: int;
  mutable n: int;
  compare: 'a->'a -> int;
  mutable tree: 'a array;
}

let is_empty heap =
  heap.n < 1

let created compare n0 = 
  {
    n0 = n0;
    n = 0;
    compare = compare;
    tree = [||];
  }
    
let resize heap n = 
  heap.tree <- 
    Array.init n 
    (fun i ->
      if i < (len heap.tree) then
	heap.tree.(i)
      else
	heap.tree.(0)
    )


let add heap item = 
  heap.n <- heap.n + 1;
  let n = heap.n in

    if (len heap.tree) = 0 then (
      heap.tree <- Array.init (max heap.n0 1) (fun i -> item)
    );

    if n > (len heap.tree) then (
      resize heap ((len heap.tree)*2)
    );

    let tree = heap.tree in  
      tree.( n-1) <- item;
      let finis = ref false in

      let i = ref (n-1) in
	while not !finis do
	  if !i == 0 then
	    finis := true
	  else
	    let ip = (!i-1)/2 in
	      if (heap.compare tree.(ip) tree.(!i)) < 0 then (
		let temp = tree.(ip) in
		  tree.(ip) <- tree.(!i);
		  tree.(!i) <- temp;
		  i := ip;
	      )
	      else
		finis := true
	done


let popped_max heap =
  if heap.n < 1 then
    raise (Failure "heap is empty");

  heap.n <- heap.n - 1;
  let tree = heap.tree in
  let item = tree.(0) in
  let n = heap.n in
    tree.(0) <- tree.(n);
    let finis = ref false in
    let i = ref 0 in
      while not !finis do
	let ic0, ic1 = 2*(!i) + 1, 2*(!i) + 2 in
	  if ic0 > (n-1) then
	    finis := true
	  else
	    let ic = 
	      if ic1 > (n-1) then
		ic0
	      else (
		if (heap.compare tree.(ic0) tree.(ic1)) > 0 then ic0 else ic1
	      )
	    in
	      if (heap.compare tree.(!i) tree.(ic)) < 0 then (
		let temp = tree.(ic) in
		  tree.(ic) <- tree.(!i);
		  tree.(!i) <- temp;
		  i := ic;
	      )
	      else
		finis := true;
      done;
      
      if n < (len tree)/2 then
	resize heap ((len tree)/2);
	  
      item

let size heap = heap.n 
	  
(************************************************************)
(*
let heap = created compare 1;;

let data = [2;3;1;6;7;8;4;5;9;0];;

List.iter
  (fun i ->
     add heap i
  )
  data
;;

for i = 0 to 5 do
  Printf.printf "%d\n" (popped_max heap);
done
;;

add heap (-1);;
add heap 20;;
Printf.printf "added\n";;

for i = 0 to (heap.n-1) do
  Printf.printf "%d\n" (popped_max heap);
done
;;

*)
