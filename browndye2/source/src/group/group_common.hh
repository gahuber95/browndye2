#pragma once

/*
 * group_common.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "../lib/vector.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../core_and_chain/molcore/core_common.hh"
#include "../core_and_chain/chain/chain_common.hh"
#include "../core_and_chain/dummy_molecule.hh"
#include "constraint.hh"

namespace Browndye{

class System_Common;
class System_Thread;

class Group_Common{
public:
  Group_Common( const System_Common&, const JAM_XML_Pull_Parser::Node_Ptr& node, Group_Index);
  Group_Common() = delete;
  Group_Common( const Group_Common&) = delete;
  Group_Common& operator=( const Group_Common&) = delete;
  Group_Common( Group_Common&&) = default;
  Group_Common& operator=( Group_Common&&) = delete;
  Group_Common( Test_Tag, const System_Common&, Vector< Core_Common, Core_Index>&&, Vector< Chain_Common, Chain_Index>&&, Group_Index);

  struct Mobility_Info{
    Pos center;
    Inv_Length trans_mobility;
    Inv_Length3 rot_mobility;
  };
  
  Mobility_Info mobility_info;

  const System_Common& system;
  const Group_Index number;
  std::string name;
  
  // first core of Group 0 is immobile
  Vector< Core_Common, Core_Index> cores;
  Vector< Chain_Common, Chain_Index> chains;
  Vector< Dummy_Molecule, Dummy_Index> dummies; 
  
  Charge charge{ NaN};

  // for constraints
  Vector< Cor_Cha_Index, Chain_Index> chain_orders; // initialize!
  Vector< Cor_Cha_Index, Core_Index> core_orders; // initialize!
  Vector< Vector< Core_Index>, Chain_Index> bound_cores_of_chains;
  
  Vector< Chain_Bead, Bead0_Index> all_chain_beads;
  Vector< Core_Bead, Bead0_Index> all_core_beads;
  Vector< Tet_Bead, Bead0_Index> all_tet_beads;
    
  // bead indices are ordered (all_chain_beads, all_core_beads, all_tet_beads)
  // constraint indices are ordered (length, coplanar, tet_core, tet tet (on thread))
  Vector< Length_Constraint, Constraint0_Index> all_length_constraints;
  Vector< Coplanar_Constraint, Constraint0_Index> all_coplanar_constraints;
  
  [[nodiscard]] std::pair< Length, Length> field_and_atom_limits( const Solvent&) const;
  
private:
  [[nodiscard]] Length smooth_radius( const Solvent&) const;
  [[nodiscard]] Length furthest_atom_extent() const;
  
  Mobility_Info mobility_info_of( System_Thread &sthread) const;
  
  [[nodiscard]] Length initial_radius_guess_from_cores() const;
  [[nodiscard]] Length initial_radius_guess_from_chains() const;
  [[nodiscard]] EPotential rms_potential_deviation( const Solvent&, Length) const;
  [[nodiscard]] Length initial_radius_guess() const;
  
  template< class A>
  Length bisected_rms_radius( const Solvent&, EPotential tol, Length r0, A args) const;
  
  [[nodiscard]] Length outer_radius_for_search( const Solvent&, EPotential tol, Length r0) const;
  [[nodiscard]] Length inner_radius_for_search( const Solvent&, EPotential tol, Length r0) const;
  
  [[nodiscard]] Length furthest_extent_from_cores() const;
  [[nodiscard]] Length furthest_extent_from_chains() const;
  
  [[nodiscard]] EPotential electric_potential( const Solvent&, Pos) const;
  [[nodiscard]] EPotential electric_potential_from_cores(Pos) const;
  [[nodiscard]] EPotential electric_potential_from_chains( const Solvent&, Pos) const;

  void setup_bound_core_atoms();
  void setup_constraint_info();
  void setup_coreless_chains();
  void setup_all_chain_beads();
  void setup_all_core_beads();
  void setup_all_cm_constraints();
  void setup_bound_cores_of_chains();
  void setup_all_tet_beads();
  void get_dummies( const JAM_XML_Pull_Parser::Node_Ptr&, const Nonbonded_Parameter_Info&);
};

}

