/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Generic code for a Cartesian-based multipole implementor.  See
doc/multipole.html for details.
*/

#include <cstdio>
#include <cmath>
#include "cartesian_multipole.hh"

#define FOR( i,begin,end) for( unsigned int i = (begin); i < (end); i++) 

namespace Browndye::Cartesian_Multipole{

const unsigned int MAX_ORDER = 20;

Multipole_Info::Multipole_Info( unsigned int _order):
  order( _order), ni( n_indices( order)){

  if (order > MAX_ORDER){
    char msg[1000];
    sprintf( msg, "order %d exceeds maximum order %d\n",
             order, MAX_ORDER);
    error( msg);
  }

  factorials[0] = 1;
  FOR( i, 1, MAX_ORDER+2){
    factorials[i] = i*factorials[i-1];
  }

  generate_indices();
  generate_responsibilities();
  setup_inv_indices();
  generate_convsum_array();
  generate_trifactorials();
  generate_minus_one_powers();
  generate_derivative_coefficients();   

  n_per_cell = 20;
  ratio = 0.5;
}

void generate_indices_for_order( unsigned int order, 
                                 Vector< Index3>& indices, unsigned int& n){
  const unsigned int op1 = order + 1;
  FOR( ix, 0, op1){
    FOR( iy, ix, op1){
      indices[n][Z] = ix;
      indices[n][Y] = (unsigned int)( iy-ix);
      indices[n][X] = (unsigned int)( order - iy);
      ++n;
    }
  }
}

unsigned int n_indices( unsigned int order){
  return (order + 1)*(order + 2)*(order + 3)/6;
}


void Multipole_Info::generate_indices(){
  indices.resize( n_indices( order));
  unsigned int n = 0;
  FOR( i, 0, order+1){
    generate_indices_for_order( i, indices, n);
  }
}

void Multipole_Info::generate_responsibilities(){
  unsigned int n = indices.size();
  resps.resize( n);

  FOR( ichild, 0, n){
    const Index3& index = indices[ ichild];
    const unsigned int ix = index[ X];
    const unsigned int iy = index[ Y];
    const unsigned int iz = index[ Z];

    unsigned int jx = ix;
    unsigned int jy = iy;
    unsigned int jz = iz;
    unsigned int dir = 999;

    if (ix >= iy && ix >= iz){
      --jx;
      dir = 0;
    }
    else if (iy > ix && iy >= iz){
      --jy;
      dir = 1;
    }
    else{
      --jz;
      dir = 2;
    }

    FOR( iparent, 0, ichild){
      const Index3& lindex = indices[iparent];
      if (lindex[X] == jx && lindex[Y] == jy && lindex[Z] == jz){
        Index3& resp = resps[ iparent];
        resp[ dir] = ichild;
      }
    }
  }
}

void Multipole_Info::setup_inv_indices(){

  inv_indices.resize( order+1);
  FOR( ix, 0, order+1){
    const unsigned int ny = 1+order-ix;
    inv_indices[ix].resize( ny);
    FOR( iy, 0, ny){
      const unsigned int nz = 1+order-ix-iy;
      inv_indices[ix][iy].resize( nz);
      FOR( iz, 0, nz){
        FOR( k, 0, indices.size()){
          if (indices[k][X] == ix && indices[k][Y] == iy &&
              indices[k][Z] == iz){
            inv_indices[ix][iy][iz] = k;
          }
        }
      }
    }
  }
}

double Multipole_Info::factorials[ MAX_ORDER+2]; 
//{1,1,2,6,24,120, 720, 5040, 40320, 362880, 3628800};  

// assume that factors is properly sized 

void Multipole_Info::generate_scaled_rfactors( 
                                  const Vec3< double>& r,
                                  Vector< double>& cps) const{


  generate_rfactors( r, cps);
  FOR( i, 0, cps.size()){
    cps[i] /= trifacts[i];
  }
}

// no factorial scaling
void Multipole_Info::generate_rfactors( 
                       const Vec3< double>& r,
                       Vector< double>& cs) const{

  cs.resize( ni);
  const unsigned int nid = n_indices( order-1);
  
  cs[0] = 1.0;
  FOR( ip, 0, nid){
    const Index3& resp = resps[ip];
    FOR( k, 0, 3){
      const unsigned int ic = resp[k];
      if (ic > 0){
        cs[ic] = cs[ip]*r[k];
      }
    }
  }  
}

void Multipole_Info::generate_convsum_array(){
  
  convsum.resize( ni);

  FOR( m, 0, ni){
    const Index3& mindex = indices[m];
    unsigned int max_p = ni;
    FOR( p, 0, ni){
      const Index3& pindex = indices[p];

      unsigned int ord = 0;
      FOR( k,0,3)
        ord += mindex[k] + pindex[k];

      if (ord > order){
        max_p = p;
        break;
      }
    }

    convsum[m].resize( max_p);
    FOR( p, 0, max_p){
      const Index3& pindex = indices[p];
      const unsigned int ix = mindex[X] + pindex[X];
      const unsigned int iy = mindex[Y] + pindex[Y];
      const unsigned int iz = mindex[Z] + pindex[Z];
      convsum[m][p] = inv_indices[ix][iy][iz];
    }    
  }

}


void Multipole_Info::generate_trifactorials(){

  const unsigned int n = indices.size();
  trifacts.resize( n);
  FOR( i, 0, n){
    const Index3& index = indices[i];
    trifacts[i] = 
      factorials[ index[X]]*factorials[ index[Y]]*factorials[ index[Z]];
  }
}


void Multipole_Info::add_shifted_polynomial( unsigned int lorder,  
                                             const Vector< double>& cps,
                                             const Vector< double>& vs0,
                                             Vector< double>& vs) const{
  const unsigned int nil = n_indices(lorder);
  FOR(m, 0, nil){
    double sum = 0.0;
    const Ivec1& cvsm = convsum[m];
    FOR( p, 0, cvsm.size()){
      sum += vs0[ cvsm[p]]*cps[p];
    }
    vs[ m] += sum;
  }
}

//#####################################################################
void Multipole_Info::add_taylor_expansion( const Vector< double>& qs,
                                           const Vector< double>& ds,
                                           Vector< double>& vs) const{

  const unsigned int nil = convsum.size();
  FOR( m, 0, nil){
    unsigned int mm = 0;
    const Index3& index = indices[m];
    FOR( k, 0, 3){
      mm += index[k];
    }
    const double sgn = (mm % 2 == 0) ? 1.0 : -1.0;

    double sum = 0.0;
    const Ivec1& cvsm = convsum[m];
    FOR( p, 0, cvsm.size()){
      auto ic = cvsm[p];
      double dss = ds[ ic];
      double q = qs[p];
      //sum += ds[ cvsm[p]]*qs[p];
      sum += dss*q;
    }
    vs[ m] += sgn*sum;
  }
}

//#####################################################################
double Multipole_Info::first_taylor_term( const Vector< double>& qs,
                                           const Vector< double>& ds) const{
  
  unsigned int mm = 0;
  const Index3& index = indices[0];
  FOR( k, 0, 3){
    mm += index[k];
  }
  const double sgn = (mm % 2 == 0) ? 1.0 : -1.0;

  double sum = 0.0;
  const Ivec1& cvsm = convsum[0];
  FOR( p, 0, cvsm.size()){
    auto ic = cvsm[p];
    double dss = ds[ ic];
    double q = qs[p];
    sum += dss*q;
  }
  return sgn*sum;
}


//#################################################################
/*
void Multipole_Info::add_taylor_expansion( unsigned int lorder,
                                           const Vector< double>& qs,
                                           const Vector< double>& ds,
                                           Vector< double>& vs) const{
  
  const unsigned int ni = n_indices( lorder);
  FOR( m, 0, ni){
    unsigned int mm = 0;
    const Index3& index = indices[m];
    FOR( k, 0, 3)
      mm += index[k];
    const double sgn = (mm % 2 == 0) ? 1.0 : -1.0;

    double sum = 0.0;
    const Ivec1& cvsm = convsum[m];
    FOR( p, 0, cvsm.size()){
      sum += ds[ cvsm[p]]*qs[p];
    }
    vs[ m] += sgn*sum;
  }
}
*/

//  f deriv, r power, xyz power

void Multipole_Info::generate_minus_one_powers(){

  m1powers.resize( ni);

  FOR( i, 0, ni){
    const Index3& index = indices[i];
    unsigned int mm = 0;
    FOR( k, 0, 3)
      mm += index[k];
    double sgn = (mm % 2 == 0) ? 1.0 : -1.0;
    m1powers[i] = sgn;
  }
}


void Multipole_Info::generate_reversed_cs( const Vector< double>& cs0,
                                           Vector< double>& cs) const{
  FOR( i, 0, cs0.size())
    cs[i] = m1powers[i]*cs0[i];
}


// includes -1 multiples; rfactors have no factorials
void Multipole_Info::
generate_greens_derivatives( 
                            const Vector< double>& rfactors,
                            const Vector< double>& fderivs,
                            Vector< double>& derivs
                            ) const{
  
  derivs.resize( ni);

  double r = sqrt( sq( rfactors[1]) + sq( rfactors[2]) + sq( rfactors[3])); 
  Vector< double> rs( 2*order);
  rs[0] = 1.0;
  FOR( i, 1, 2*order){
    rs[i] = rs[i-1]*r;
  }

  FOR( i, 0, deriv_coefs.size()){
    const Vector< Deriv_Term>& lcoefs = deriv_coefs[i];
    double sum = 0.0;
    for( auto& dterm: lcoefs){
      sum += rfactors[ dterm.xyz_power]*fderivs[ dterm.f_deriv]*dterm.scale/rs[ dterm.r_power];
    }
    derivs[ i] = sum*m1powers[i];
  }
}

//void print_indices3( FILE* fp, const Vector< Index3>& id);

void Multipole_Info::generate_derivative_coefficients(){

  deriv_coefs.resize( ni);

  deriv_coefs[0].resize( 1);
  deriv_coefs[0][0].scale = 1;

  FOR( i, 0, ni){
    const Index3& resp = resps[i];
    FOR( k, 0, 3){
      const unsigned int ci = resp[k];
      if (ci > 0){

        const Vector< Deriv_Term>& lcoefs = deriv_coefs[i];
        for( auto& dterm: lcoefs){
          const unsigned int ixyz = dterm.xyz_power;

          Deriv_Term dterms[3];
          for( auto& dt: dterms){
            dt = dterm;
          }

          bool included[3] = {false, false, false};

          // xyz term
          {
            unsigned int ip[3];
            ip[X] = indices[ixyz][X];
            ip[Y] = indices[ixyz][Y];
            ip[Z] = indices[ixyz][Z];

            if (ip[k] > 0){
              included[0] = true;
              ip[k] -= 1;
              dterms[0].xyz_power = inv_indices[ ip[X]][ ip[Y]][ ip[Z]];
              dterms[0].scale *= (int)(ip[k] + 1);
            }
          }

          // r term
          {
            const auto m = dterms[1].r_power;
            if (m > 0){
              included[1] = true;

              unsigned int ip[3];
              ip[X] = indices[ixyz][X];
              ip[Y] = indices[ixyz][Y];
              ip[Z] = indices[ixyz][Z];
              ip[k] += 1;
              dterms[1].xyz_power = inv_indices[ ip[X]][ ip[Y]][ ip[Z]];
              
              
              dterms[1].r_power = m+2;
              dterms[1].scale *= -(int)m;
            }
          }

          // f term
          {
            unsigned int ip[3];
            ip[X] = indices[ixyz][X];
            ip[Y] = indices[ixyz][Y];
            ip[Z] = indices[ixyz][Z];
            ip[k] += 1;


            dterms[2].xyz_power = inv_indices[ ip[X]][ ip[Y]][ ip[Z]];
            dterms[2].r_power += 1;
            dterms[2].f_deriv += 1;
            included[2] = true;
          }
  
          FOR( jj, 0, 3) if (included[jj]){
            const Deriv_Term& ldterm = dterms[jj];
            Vector< Deriv_Term>& ccoefs = deriv_coefs[ci];
            const unsigned int nc = ccoefs.size();
            bool found = false;
            FOR( it, 0, nc){
              Deriv_Term& cdterm = ccoefs[it];
              if (cdterm.xyz_power == ldterm.xyz_power &&
                  cdterm.r_power == ldterm.r_power &&
                  cdterm.f_deriv == ldterm.f_deriv){
                cdterm.scale += ldterm.scale;
                found = true;
                break;
              }
            }
            if (!found){
              ccoefs.push_back(ldterm);
            }
          }
        }
      }
    }
  }
}
} // namespace Browndye::Cartesian_Multipole


//*****************************************************************
// Test code
/*
int main(){
  const unsigned int np = 2;
  const unsigned int order = 5;
  const unsigned int n_per_cell = 4;

  V = 0.0;
  Va = 0.0;

  Vector< Particle> parts( np);
  srand( 111111);

  FOR( ip, 0, np) {

    FOR( k, 0, 1)
      parts[ip].r[k] = rand()/((double)RAND_MAX);

    parts[ip].q = (ip % 20) == 0 ? 1.0 : -1.0;


    FOR( k, 0, 3){
      parts[ip].F[k] = 0.0;
      parts[ip].Fa[k] = 0.0;
    }
  }
 
  Multipole_Structure< Engine> ms( order, parts);
  ms.set_max_number_per_cell( n_per_cell);
  ms.setup_cells();
  ms.reload_contents();
  ms.compute_expansion();

  const unsigned int npt = 10;
  Vector< Particle> points( npt);

  FOR( ip, 0, npt) {
    FOR( k, 0, 1)
      points[ip].r[k] = rand()/((double)RAND_MAX);
  }

  double dx[3];
  FOR( i, 0, npt){
    const Particle& point = points[i];

    double V = ms.potential_at_point( point.r);
    
    double Va = 0.0;
    FOR( j, 0, np){
      const Particle& part = parts[j];
      FOR( k, 0, 3)
        dx[k] = point.r[k] - part.r[k];
        double r = sqrt( sq( dx[X]) + sq( dx[Y]) + sq( dx[Z]));
        double q = part.q;
        Va += q/r;
    }
    printf( "%d V %f %f\n", i, Va, V);
  }

  
}
  
*/

/******************************************************************/
/*
int main(){
  Vector< Index3> indices;
  unsigned int order = 5;
  unsigned int ni = n_indices( order);

  generate_indices( order, indices);


  FOR( i, 0, indices.size())
    printf( "%d %d %d %d\n", i, indices[i][X], indices[i][Y], indices[i][Z]);
  
  printf("\n");

  Vector< Index3> resps;

  generate_responsibilities( indices, resps);

  FOR( i, 0, resps.size()){
    printf( "%d ", i);
    FOR( k, 0, 3)
      printf( " %d ", resps[i][ k]); 
    printf( "\n");
  }
  printf( "\n");

  Ivec3 inv_indices;
  setup_inv_indices( order, indices, inv_indices);

  FOR( ix, 0, order+1){
    FOR( iy, 0, inv_indices[ix].size()){
      FOR( iz, 0, inv_indices[ix][iy].size())
        printf( "%d %d %d %d\n", ix,iy,iz, inv_indices[ix][iy][iz]);
    }
  }
  printf( "\n");

  Vector< double> trifacts;
  generate_trifactorials( indices, trifacts);

  double r[3] = {1.0, 0.0, 0.0};
  Vector< double> rfactors( ni);

  generate_scaled_rfactors( order, resps, trifacts, r, rfactors);

  FOR( i, 0, rfactors.size()){
    printf( "%d %d %d %f\n", indices[i][X], indices[i][Y], indices[i][Z],
             rfactors[i]);
  }
  printf( "\n");

  Ivec2 convsum; 
  generate_convsum_array( order, indices, inv_indices, convsum);

  FOR( m, 0, convsum.size()){
    printf( "%d %d %d %d\n", m, indices[m][X], indices[m][Y], indices[m][Z]);
    FOR( p, 0, convsum[m].size())
      printf( "%d ", convsum[m][p]);
    printf( "\n");
  }
  printf( "\n");

  Vector< double> vs0( ni), vs( ni);
  vs0[0] = 0.0;
  vs0[1] = 0.0;
  vs0[2] = 0.0;
  vs0[3] = 0.0;
  vs0[4] = 0.0;
  vs0[5] = 0.0;
  vs0[6] = 1.0;
  vs0[7] = 0.0;
  vs0[8] = 0.0;
  vs0[9] = 1.0;
  
  shift_polynomial( convsum, rfactors, vs0, vs);

  FOR( i, 0, 10)
    printf( "%d %f\n", i, vs[i]);
  printf( "\n");
  

  Vector< double> qs0( ni), qs( ni);
  FOR( i, 0, ni)
    qs0[i] = 0.0;
  qs0[3] = 11.0;

  add_shifted_multipoles( convsum, order, rfactors, qs0, qs);
  FOR( i, 0, ni)
    printf( "%d %d %d %d %f\n", i, 
            indices[i][0], indices[i][1], indices[i][2], 
            qs[i]);

  printf( "\n");

  printf( "derivatives\n");
  Vector< Vector< Deriv_Term> > coefs;
  generate_derivative_coefficients( order, resps, indices, inv_indices,
                                    coefs);

  FOR( i, 0, coefs.size()){
    printf( "%d %d %d %d\n", i, indices[i][X], indices[i][Y], indices[i][Z]);
    const Vector< Deriv_Term>& lcoefs = coefs[i];
    FOR( j, 0, lcoefs.size()){
      const Deriv_Term& dterm = lcoefs[j];
      const unsigned int ix = dterm.xyz_power;
      printf( "xyz %d %d %d r %d f %d scale %d\n",
              indices[ix][X], indices[ix][Y], indices[ix][Z],
              dterm.r_power, dterm.f_deriv, dterm.scale);
    }
    printf( "\n");
  }

  Vector< double> m1powers;

  generate_minus_one_powers( order, indices, m1powers);

*/

  //*******************************************************





