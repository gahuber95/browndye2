/*
 * group_thread.cc
 *
 *  Created on: Sep 12, 2015
 *      Author: ghuber
 */
 
 
#include "group_common.hh"
#include "group_thread.hh"
#include "group_state.hh"
#include "../molsystem/system_thread.hh"

namespace Browndye{

// constructor
Group_Thread::Group_Thread( const Group_Common& _common, const System_Thread& _system):
      common(_common), system_thread(_system){
  
  core_threads.reserve( common.cores.size());
  for( auto& core: common.cores){
    auto is_used =
      (common.number > Group_Index(0)) || (core.number > Core_Index(0));
    core_threads.emplace_back( core, *this, is_used);
  }
}
}
