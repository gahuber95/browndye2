#pragma once

#include <stddef.h>
//#include "../global/indices.hh"

namespace Browndye{
  constexpr size_t ntet = 4;
}

// algorithms to be changed if ntet != 4:
// best_links, tet_triple_product in group_state.cc
// tet_forces.cc
// angle_of_dx in do_bd_step.cc
// unit_tetrahedron in tet.cc
// tet_transform.cc
// equivalent_tet_geometry in tet