#pragma once

/*
 * outtag.hh
 *
 *  Created on: Sep 12, 2015
 *      Author: ghuber
 */

#include <fstream>
#include <string>
#include "../lib/units.hh"

namespace Browndye{

template< class T>
[[maybe_unused]] T output_value( const T& t){
  return t;
}

template< class M, class L, class T, class Q, class VType>
VType output_value( const Unit< M,L,T,Q,VType>& u){
  return fvalue( u);
}

template< class U>
inline
void outtag( std::ofstream& output, const std::string& tag, U value, int ident){
  for( int i = 0; i < ident; i++)
    output << " ";

  output << "<" << tag << "> " << output_value( value) << " </" << tag << ">\n";
}

}

