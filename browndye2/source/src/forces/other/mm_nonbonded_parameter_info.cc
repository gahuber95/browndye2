/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements classes and functions described in "parameter_info.hh"
*/


#include <algorithm>
#include "../../xml/node_info.hh"
#include "mm_nonbonded_parameter_info.hh"
#include "redundant_index_families.h"

//****************************************************************
// Following code used for initializing

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;
using std::max;
using std::min;

MM_Nonbonded_Parameter_Info_Base::~MM_Nonbonded_Parameter_Info_Base()= default;

MM_Nonbonded_Parameter_Info_Base::MM_Nonbonded_Parameter_Info_Base( Force_Field_Type _fft){
  fft = _fft;
}

// static
auto MM_Nonbonded_Parameter_Info_Base::AB_of_sigeps( Energy epsilon, Length sigma) -> std::pair< A_Coef, B_Coef>{
  
  auto sig2 = sq( sigma);
  auto sig4 = sq( sig2);
  auto sig6 = sig4*sig2;
  auto sig12 = sq( sig6);
  auto eps4 = 4.0*epsilon;
  auto A = eps4*sig12;
  auto B = eps4*sig6;
  return std::make_pair( A,B);
}

auto MM_Nonbonded_Parameter_Info_Base::mixed( A_Coef A0, B_Coef B0,
                                          A_Coef A1, B_Coef B1) -> std::pair< A_Coef, B_Coef>{
  
  bool p0{ B0 > B_Coef( 0.0)};
  bool p1{ B1 > B_Coef( 0.0)};
  
  A_Coef A;
  B_Coef B;
  if (p0 && p1){ // Lorenz-Berthelot rules
    
    auto sig0 = sqrt( cbrt( A0/B0));
    auto sig1 = sqrt( cbrt( A1/B1));
    
    auto eps0 = B0*B0/A0;
    auto eps1 = B1*B1/A1;
    
    auto sig = 0.5*(sig0 + sig1);
    auto eps = sqrt( eps0*eps1); // B*B/A
    
    auto sig2 = sig*sig;
    auto sig6 = sig2*sig2*sig2; // A/B
    
    B = eps*sig6;
    A = sig6*B;
  }
  else{ // average lengths
    auto sig0 = sqrt( cbrt( A0));
    auto sig1 = sqrt( cbrt( A1));
    auto sig = 0.5*( sig0 + sig1);
    auto sig2 = sig*sig;
    A = sig2*sig2*sig2;
    B = B_Coef( 0.0);
  }
  
  return std::make_pair( A,B);
}
  
using std::map;
using std::pair;
using std::string;
using std::make_pair;  
  
//##################################################################################################
void MM_Nonbonded_Parameter_Info_Base::get_pair_parameters( Node_Ptr top_node, map< pair2< string>, Atom_Type_Index>& pre_dict,
        map< pair2<Atom_Type_Index>, LJ_Parameters>& pair_params) {

  auto pp_node = top_node->child("pair_parameters");

  if (pp_node) {
    auto pnodes = pp_node->children_of_tag("parameter");
    for (auto pnode: pnodes) {
      auto atom0 = checked_value_from_node<string>(pnode, "atom0");
      auto atom1 = checked_value_from_node<string>(pnode, "atom1");
      auto residue0 = checked_value_from_node<string>(pnode, "residue0");
      auto residue1 = checked_value_from_node<string>(pnode, "residue1");

      const Atom_Type_Index ntab{ pre_dict.size()};
      auto ar0 = make_pair(residue0, atom0);
      auto iresa = pre_dict.insert( make_pair( ar0, ntab));
      auto nta = iresa.first->second;

      const Atom_Type_Index ntbb{ pre_dict.size()};
      auto ar1 = make_pair(residue1, atom1);
      auto iresb = pre_dict.insert( make_pair( ar1, ntbb));
      auto ntb = iresb.first->second;

      auto lj_paramsl = [&] {
        auto Aopt = value_from_node<A_Coef>(pnode, "A");
        if (Aopt) {
          auto A = Aopt.value();
          auto B = checked_value_from_node<B_Coef>(pnode, "B");
          return LJ_Parameters{A, B};
        }
        else {
          auto epsilon = checked_value_from_node<Energy>(pnode, "epsilon");
          auto sigma = checked_value_from_node<Length>(pnode, "sigma");
          auto [A, B] = AB_of_sigeps(epsilon, sigma);
          return LJ_Parameters{A, B};
        }
      }();

      pair_params.insert_or_assign( make_pair(nta, ntb), lj_paramsl);
    }
  }
}

//##################################################################################################
void MM_Nonbonded_Parameter_Info_Base::input_params( Vector< JP::Node_Ptr>& top_nodes, Vector< LJ_Parameters, Atom_Type_Index>& single_params,
                            map< pair2<Atom_Type_Index>, LJ_Parameters>& pair_params, map< pair2< string>, Atom_Type_Index>& pre_dict) {

  for (auto &top_node: top_nodes) {
    auto unode = top_node->child("units");
    set_energy_units(unode);
    one_four_factor = checked_value_from_node<double>(top_node, "one_four_factor");
    get_hydrophobic_params(top_node);

    auto residue_nodes = top_node->children_of_tag("residue");
    for (auto &rnode: residue_nodes) {
      auto residue_name = checked_value_from_node<string>(rnode, "name");

      auto atom_nodes = rnode->children_of_tag("atom");
      for (auto &anode: atom_nodes) {
        auto atom_name = checked_value_from_node<string>(anode, "name");

        const auto nt = single_params.size();
        pair key{ residue_name, atom_name};
        auto ires = pre_dict.insert( make_pair( key, nt));

        auto Aopt = value_from_node<A_Coef>( anode, "A");
        auto Bopt = value_from_node<B_Coef>( anode, "B");

        auto add_lj = [&]( auto A, auto B){
          LJ_Parameters ljp{ A,B};
          auto ip = ires.first->second;
          if (ires.second){ // new entry
            single_params.push_back( ljp);
          }
          else { // rewritten entry
            single_params[ip] = ljp;
          }
        };

        if (Aopt) {
          auto A = Aopt.value();
          auto B = Bopt ? Bopt.value() : B_Coef(0.0);
          add_lj( A,B);
        }
        else {
          auto eps_opt = value_from_node<Energy>(anode, "epsilon");
          if (eps_opt) {
            auto sigma = checked_value_from_node<Length>(anode, "sigma");
            auto epsilon = eps_opt.value();

            auto [A, B] = AB_of_sigeps(epsilon, sigma);
            add_lj( A,B);
          }
        }

      }
    }

    get_pair_parameters( top_node, pre_dict, pair_params);
  }
}
//###################################################################################################
void MM_Nonbonded_Parameter_Info_Base::get_absinth_params( Vector< Node_Ptr>& top_nodes) {

  for (auto &top_node: top_nodes) {
    auto gnode = top_node->child("solvation_groups");
    if (gnode) {
      auto gfiles = vector_from_node<string>(gnode);
      for (auto &gfile: gfiles) {
        std::ifstream input(gfile);
        JP::Parser parser(input, gfile);
        auto top_nodel = parser.top();
        top_nodel->complete();
        auto rnodes = top_nodel->children_of_tag("residue");
        for (auto &rnode: rnodes) {
          auto rname = checked_value_from_node<string>(rnode, "name");
          auto gnodes = rnode->children_of_tag("group");
          for (auto &gnodel: gnodes) {
            auto energy = converted_energy(checked_value_from_node<Energy>(gnodel, "energy"));

            auto atom_nameso = vector_from_node<string>(gnodel, "atoms");
            if (atom_nameso) {
              auto &anames = atom_nameso.value();
              auto weight = 1.0 / (int) anames.size();
              auto aenergy = weight * energy;
              for (auto &aname: anames) {
                SE_Key key{rname, aname};
                surface_energies.insert_or_assign(key, aenergy);
              }
            } else {
              auto atom_indiceso = vector_from_node<Atom_Index>(gnodel, "atom_numbers");
              if (!atom_indiceso) {
                auto gname = checked_value_from_node<std::string>(gnodel, "name");
                error("ABSINTH surface energy: need either atoms or atom_numbers in", gname);
              }
              auto &atom_indices = atom_indiceso.value();
              auto weight = 1.0 / (int) atom_indices.size();
              auto aenergy = weight * energy;
              for (auto ia: atom_indices) {
                SE_Key key{rname, ia};
                surface_energies.insert_or_assign(key, aenergy);
              }
            }
          }
        }
      }
    }
  }
}

//#################################################################################################################
// check to see all pairs are defined
void MM_Nonbonded_Parameter_Info_Base::check_lj_pairs() const{

  auto n = lj_params.n_rows();
  if (lj_params.n_cols() != n) {
    error(__FILE__, __LINE__, "not get here");
  }

  for (auto i: range(n)) {
    for (auto j: range(n)) {
      auto opt = lj_params(i, j);
      if (!opt) {
        // make more clear by using original type indices!
        error("Lennard-Jones parameters not defined for ", i, j);
      }
    }
  }
}

//#################################################################################################################
typedef Vector< Vector< std::optional< MM_Nonbonded_Parameter_Info_Base::LJ_Parameters>, Atom_Type_Index>, Atom_Type_Index> Pre_Params;

class Reduced_Indexer{
public:
  explicit Reduced_Indexer( const Pre_Params& pre_lj_params);

  Atom_Type_Index operator()( Atom_Type_Index) const;
  [[nodiscard]] Atom_Type_Index n_indices() const;

private:
  map< Atom_Type_Index, Atom_Type_Index> index_families;
  map< Atom_Type_Index, Atom_Type_Index> new_order;
};

// constructor
Reduced_Indexer::Reduced_Indexer( const Pre_Params& pre_lj_params) {

  index_families = redundant_index_families( pre_lj_params);

  for (auto &entry: index_families) {
    auto ip = entry.second;
    Atom_Type_Index nnt( new_order.size());
    new_order.insert( make_pair( ip, nnt));
  }
}

Atom_Type_Index Reduced_Indexer::operator()( Atom_Type_Index i) const{

  auto ifres = index_families.find(i);
  if (ifres == index_families.end()) {
    error(__FILE__, __LINE__, "not get here");
  }
  auto nores = new_order.find( ifres->second);
  if (nores == new_order.end()) {
    error(__FILE__, __LINE__, "not get here");
  }
  return nores->second;
}

  Atom_Type_Index Reduced_Indexer::n_indices() const {
    return Atom_Type_Index{ new_order.size()};
  }

//###################################################################################
// constructor
// assume top_node is completed
// refactor!
MM_Nonbonded_Parameter_Info_Base::MM_Nonbonded_Parameter_Info_Base( Force_Field_Type _fft, Vector< Node_Ptr>& top_nodes) {

  fft = _fft;
  map< pair2<string>, Atom_Type_Index> pre_dict;
  Vector< Vector< std::optional< LJ_Parameters>, Atom_Type_Index>, Atom_Type_Index> pre_lj_params;
  Vector< LJ_Parameters, Atom_Type_Index> single_params;
  map< pair2< Atom_Type_Index>, LJ_Parameters> pair_params;

  input_params( top_nodes, single_params, pair_params, pre_dict);

  // fill in parameters from combining rules
  const Atom_Type_Index nt{pre_dict.size()};
  pre_lj_params.resize(nt);
  for (auto &row: pre_lj_params) {
    row.resize(nt);
  }

  auto nsp = single_params.size();

  for (auto it: range(nsp)) {
    auto abi = single_params[it];
    auto ai = abi.A;
    auto bi = abi.B;
    for (auto jt: range(nsp)) {
      auto abj = single_params[jt];
      auto aj = abj.A;
      auto bj = abj.B;

      auto ab = mixed(ai, bi, aj, bj);
      pre_lj_params[it][jt] = LJ_Parameters{ ab.first, ab.second};
    }
  }

  // fill in specific pairs
  for (auto &item: pair_params) {
    auto [it0, it1] = item.first;
    auto param = item.second;
    pre_lj_params[it0][it1] = param;
    pre_lj_params[it1][it0] = param;
  }

  Reduced_Indexer reduced_index( pre_lj_params);

  // map data structures to final data structure
  auto nnt = reduced_index.n_indices();
  lj_params.resize( nnt, nnt);

  const auto npp = pre_lj_params.size();

  for (auto i: range(npp)) {
    auto red_i = reduced_index(i);
    for (auto j: range(npp)) {
      auto red_j = reduced_index(j);
      auto &pre_paramo = pre_lj_params[i][j];
      if (pre_paramo) {
        auto &pre_param = pre_paramo.value();
        auto &paramo = lj_params( red_i, red_j);
        LJ_Parameters param;
        param.A = converted_energy( pre_param.A);
        param.B = converted_energy( pre_param.B);
        paramo = param;
      }
    }
  }

  for (auto &entry: pre_dict) {
    auto &key = entry.first;
    auto i = entry.second;
    auto red_i = reduced_index(i);
    dict.insert( make_pair( key, red_i));
  }

  if (fft == Force_Field_Type::Absinth) {
    get_absinth_params( top_nodes);
    check_lj_pairs();
  }
}

//###################################################################
// if defaults are to be changed
void MM_Nonbonded_Parameter_Info_Base::get_hydrophobic_params( const JP::Node_Ptr& node){
  
  auto& hp = this->hp_params;
  auto hnode = node->child( "hydrophobic");
  
  if (hnode){  
    auto ao = value_from_node< Length>( hnode, "a");
    auto bo = value_from_node< Length>( hnode, "b");
    auto co = value_from_node< double>( hnode, "c");
    auto betao = value_from_node< Surface_Tension>( hnode, "beta");
    
    if (ao){
      hp.a = ao.value();
    }
    if (bo){
      hp.b = bo.value();
    }
    if (co){
      hp.c = co.value();
    }
    if (betao){
      hp.beta = betao.value();
    }
      
    hp.compute_factor();
  }
}

//###################################################################
Force MM_Nonbonded_Parameter_Info_Base::sasa_force( Length r,
                        const Atom& atom0, const Atom& atom1) const{
  
  auto a = hp_params.a;
  auto b = hp_params.b;
  
  auto force = [&]( Length radius, Length2 area){
    auto ri = r + radius;
    if (ri < a || b < ri){
      return Force( 0.0);
    }
    else{
      return hp_params.factor*area;
    }
  };
  
  auto radius0 = atom0.radius;
  auto radius1 = atom1.radius;
  auto area0 = atom0.sasa();
  auto area1 = atom1.sasa();
  
  auto F0 = force( radius0, area1);
  auto F1 = force( radius1, area0);
  return F0 + F1;
}

//#######################################################################
void MM_Nonbonded_Parameter_Info_Base::establish_interaction_radii( Vector<Atom, Atom_Index>& atoms) const{

  constexpr double AB_cutoff{ 2.5};
  constexpr double  A_cutoff{ 2.0};
  
  auto cutoff = [&]( Length a, Length b) -> Length{
    if (b > Length( 0.0)){
      auto sig = a*a/b;
      return AB_cutoff*sig;
    }
    else{
      return A_cutoff*a;
    }
  };
   
  auto uabsinth = uses_absinth(); 
   
  auto np = lj_params.n_rows();
  Vector< Length, Atom_Type_Index> int_radii( np);
  int_radii.fill( Length{ 0.0});
  
  for( auto& atom: atoms){
    auto ahs = hs_lj_a_factor()*atom.radius;
    atom.interaction_radius = A_cutoff*ahs;
    if (uabsinth && atom.type == Atom_Type_Index( maxi)){
      error( "no type found for atom", atom.number);
      // still need to output chain or core name
    }
  }
  
  for( auto i: range( np)){
    auto row = lj_params.row( i);
    for( auto j: range( np)){
      auto& paramo = row[j];
      if (paramo){
        auto param = paramo.value();
        auto E1 = Energy(1.0);
        Length a = sqrt( sqrt( cbrt( param.A/E1)));
        Length b = sqrt( cbrt( param.B/E1));
        int_radii[i] = max( int_radii[i], cutoff( a,b));
      }
      else{
        if (uabsinth){
          
          auto fnd = [&]( Atom_Type_Index it) -> const Atom&{
            auto itr = std::find_if( atoms.begin(), atoms.end(), [&]( const Atom& atom){
              return atom.type == i;
            });
            return *itr;
          };
          
          auto& atomi = fnd( i);
          auto& atomj = fnd( j);
          error( "Required for ABSINTH: no LJ parameters found for atom pair", atomi.number, atomj.number); 
        }
        
      }
    }
  }
  
  for( auto& atom: atoms){
    auto itype = atom.type;  
    if (itype != Atom_Type_Index( maxi)){
      atom.interaction_radius = max( atom.interaction_radius, int_radii[ itype]);
    }
  }

  if (fft == Force_Field_Type::Molecular_Mechanics_HP) {
    auto b = hp_params.b;
    for (auto &atom: atoms) {
      atom.interaction_radius = max(atom.interaction_radius, atom.radius + b);
    }
  }
  else if (fft == Force_Field_Type::Absinth){
    for( auto& atom: atoms){
      atom.interaction_radius = max( atom.interaction_radius, atom.radius + Absinth::rw);
    }
  }


}

}

//******************************************************************
// Test code
/*
struct Atom{
  Length pos[3];
  Length radius;
  Charge charge;
  Vector< int>::size_type type;
  size_t number;
};

class Interface{
public:
  typedef Vector< Atom> Spheres;
  typedef Spheres::size_type size_type;

  static
  void resize( Vector< Atom>& atoms, size_type n){
    atoms.resize( n);
  }

  static
  void put_position( Spheres& atoms, size_type i, Length* pos){
    atoms[i].pos[0] = pos[0];
    atoms[i].pos[1] = pos[1];
    atoms[i].pos[2] = pos[2];
  }

  static
  void put_charge( Spheres& atoms, size_type i, Charge charge){
    atoms[i].charge = charge;
  }

  static
  void put_radius( Spheres& atoms, size_type i, Length radius){
    atoms[i].radius = radius;
  }

  static
  void put_type( Spheres& atoms, size_type i, 
                 Near_Interactions::size_type type){
    atoms[i].type = type;
  }

  static
  void put_number( Spheres& atoms, size_type i, 
                   Near_Interactions::size_type number){
    atoms[i].number = number;
  }


};

int main(){
  Near_Interactions::MM_Nonbonded_Parameter_Info pi( "vparam-amber-parm94.xml");

  Vector< Atom> atoms;
  Near_Interactions::get_atoms_from_file< Interface>( 
                pi, "t-atoms.pqrxml", atoms, false, true);


  size_t n = atoms.size();
  for( size_t i = 0; i < n; i++){
    size_t itype = atoms[i].type;
    Energy eps = pi.parameters( itype).epsilon;
    printf( "atom %d (%g %g %g) q %g r %g e %g\n",
            atoms[i].number,
            atoms[i].pos[0], atoms[i].pos[1], atoms[i].pos[2], 
            atoms[i].charge, atoms[i].radius, eps); 
  }

}
*/
