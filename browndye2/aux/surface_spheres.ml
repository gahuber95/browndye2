
(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
  Called by bd_top.

  Finds surface spheres and triangles formed by rolling a probe sphere
  across the molecule surface.

  Generates reduced surface of spheres from pqrxml file describing spheres. 
  Outputs list of surface triangles, indices of surface spheres, indices of 
  spheres completely enclosed by a surface sphere, and indices of "danglers", 
  or spheres that are exterior but are not in a surface triangle.

  Uses standard input and output.  Takes the following arguments with flags:

  -probe_radius : changes probe radius from default of 1.5
  -all : simply include all spheres in surface

*)

let len = Array.length;;

module H = Hashtbl;;

module Int = 
struct
  type t = int
  let compare x y = compare x y
end

module Indices = Set.Make( Int);;

open Vec3;;

let error msg = 
  raise (Failure msg)
;;

let sq2 = sqrt 2.0;;

let sq x = x*.x;;

let print3 a = 
  Printf.printf "%f %f %f\n" a.x a.y a.z
;;

let some x = 
  match x with
    | Some y -> y
    | None -> error "some: is nothing"


let normed_diff a b = 
  let diff = vdiff a b in
  let nrm = vnorm diff in
  vscaled (1.0/.nrm) diff
;;

let normed a = 
  let nrm = vnorm a in
  vscaled (1.0/.nrm) a
;;

let pri = Printf.printf;;

let contactable a2pos a2r cr pr = 
  let x,y,z = a2pos in
  let r = sqrt (x*.x +. y*.y) in
  ((sq( r -. cr)) +. z*.z) < (sq (a2r +. pr))

;;    

module IIS = Is_inside_spheres;;

let rec tree_size tree = 
  match tree with
    | IIS.Empty -> 0
    | IIS.Leaf lf -> 1
    | IIS.Branch br ->
      let c0,c1 = br.IIS.children in
      (tree_size c0) + (tree_size c1)
;;

let two_pi = 2.0*.3.1415926;;

let has_triangle_inequality r0 r1 r2 = 
  let inq s0 s1 s2 = 
    s0 < (s1 +. s2)
  in
  (inq r0 r1 r2) && (inq r1 r2 r0) && (inq r2 r0 r1) 
;;

(* position of first return value views a0 a1 a2 as clockwise *)
let trio_contact_points a0 a1 a2 pr = 

  let r_and_z r0 r1 d = 
    let cth = (r0*.r0 +. d*.d -. r1*.r1)/.(2.0*.d*.r0) in
    let sth = sqrt (1.0 -. cth*.cth) in
    let r = r0*.sth
    and z = r0*.cth in
    r,z
  in

  let diff01 = vdiff a1.IIS.spos a0.IIS.spos in
  let d01 = vnorm diff01 in
  let zaxis = vscaled (1.0/.d01) diff01 
  and r0,r1,r2 = a0.IIS.radius +. pr, a1.IIS.radius +. pr, a2.IIS.radius +. pr in
  
  (* circle radius, distance of circle center from a0 *)
  let cr,da = r_and_z r0 r1 d01 in
  (* intersection center *)
  let ceni = vsum a0.IIS.spos (vscaled da zaxis) in

  (* displacement of a2 from circle center *)
  let pos2 = vdiff a2.IIS.spos ceni in

  (* distance of a2 from circle plane *)
  let a2h = dot zaxis pos2 in
    (* radius of projection of a2 on circle plane *)
  if (abs_float a2h) >= r2 then
    None

  else
    let rp2 = sqrt (r2*.r2 -. (a2h*.a2h)) in
        (* projected displacement of a2 from circle center *)
    let yvec =  vdiff pos2 (vscaled a2h zaxis) in
          (* projected distance of a2 from circle center *)
    let d2 = vnorm yvec in
    if not (has_triangle_inequality cr rp2 d2) then
      None
    else
      
      let x, y = r_and_z cr rp2 d2 in
      let yaxis = vscaled (1.0/.d2) yvec in    
      let xaxis = cross yaxis zaxis in
      
      let y_comp = vscaled y yaxis    
      and x_comp = vscaled x xaxis in
      
      let ipt0 = vdiff y_comp x_comp
      and ipt1 = vsum y_comp x_comp 
      in
      Some (ipt0, ipt1)
;;

let select_atom res0 res1 = 
  let theta0_opt, a0L = res0 
  and theta1_opt, a1L = res1
  in
  match theta0_opt, theta1_opt with
    | None, None -> res0
    | theta0, None -> res0
    | None, theta1 -> res1
    | theta0, theta1 -> (
      if theta0 < theta1 then
        res0
      else
        res1
    )
;;

let next_theta_and_sphere theta_fun origin r sphere_tree =

  let theta_a_opt = 
    IIS.fold_tree_within_distance (fun sph -> sph.IIS.active)
      origin r
      (fun a ->
        let theta_opt = theta_fun a in
        theta_opt, a
      )
      select_atom
      sphere_tree
  in
  match theta_a_opt with
    | None -> None
    | Some theta_a ->
      let theta_opt, a = theta_a in
      match theta_opt with
        | None -> None
        | Some theta ->
          Some (theta, a)

;;

let add_sphere dict i = 
  if not (H.mem dict i) then
    H.add dict i true
;;

let deg th = 180.0*.th/.3.1415926;;

(* first position and contacting sphere in counterclockwise
   direction around axis along a0 viewed from a1.
   a0,a1,a2 end up clockwise
*)

let new_probe_position p a0 a1 sphere_tree =

  let ppos = p.IIS.spos
  and pr = p.IIS.radius
  and a0pos = a0.IIS.spos
  and a0r = a0.IIS.radius
  and a1pos = a1.IIS.spos
  in

  let ez = normed_diff a1pos a0pos in
  let ex = 
    let diff = vdiff ppos a0pos in
    normed (vdiff diff (vscaled (dot diff ez) ez))  
  in
  let ey = cross ez ex in
  let circle_radius, origin = 
    let costhe = dot (normed_diff a1pos a0pos) (normed_diff ppos a0pos) in
    let h = (a0r +. pr)*.costhe in
    (sqrt ((sq (a0r +. pr)) -. h*.h)),
    (vsum a0pos (vscaled h ez)) 
  in

  let theta_of_a2 a2 = 

    if a2 == a0 || a2 == a1 then 
      None
        
    else 
      let a2r = a2.IIS.radius in 
      
      let a2p_dist = distance a2.IIS.spos ppos 
      and big_r = pr +. a2r 
      and a02_dist = distance a2.IIS.spos a0pos in
      
      if (a02_dist +. a2r) <= a0r then (
        error "theta_of_a2: should be no insiders"
      )
      else (
        if  a2p_dist < 0.99*.big_r then (
          Printf.fprintf stderr "collision %d pos %g %g %g rad %g\n" a2.IIS.actual_number a2.IIS.spos.x a2.IIS.spos.y a2.IIS.spos.z a2.IIS.radius;
          Printf.fprintf stderr "a2p_dist %f big_r %f\n" a2p_dist big_r;
          error "collision with probe sphere"
        );
        
        let a2relpos = 
          let diff = vdiff a2.IIS.spos origin in
          (dot diff ex), (dot diff ey), (dot diff ez)
        in
        if contactable a2relpos a2r circle_radius pr then (
          let ax,ay,az = a2relpos in
          if ay > 0.0 && a2p_dist <= big_r then
            Some 0.0
              
          else
            (* guard against rounding error *)
            let zsqrt x = if x < 0.0 then 0.0 else sqrt x in

            let contact_h = pr*.az/.( pr +. a2r) in
            let dtheta = 
              let prh = zsqrt (pr*.pr -. (sq contact_h))
              and a2rh = zsqrt (( a2r*.a2r -. (sq ( contact_h -. az))))
              in
              let a,b,c = 
                circle_radius, (sqrt (ax*.ax +. ay*.ay)), prh +. a2rh 
              in 
              let cosdthe = (a*.a +. b*.b -. c*.c)/.(2.0*.a*.b) in
              acos cosdthe
            in
            
            let new_theta = 
              let theta0 = 
                let th = atan2 ay ax in
                if th < 0.0 then
                  th +. two_pi
                else
                  th
              in
              let theta = theta0 -. dtheta in
              theta
            in
            Some new_theta
        )
        else 
          None
      )
  in
  let theta_a2_opt =
    next_theta_and_sphere 
      theta_of_a2 origin (circle_radius +. pr) sphere_tree 
  in
  match theta_a2_opt with
    | None -> None
    | Some theta_a2 -> 
      let theta, a2 = theta_a2 in
      let new_pos = 
        vsum 
          origin 
          (vscaled circle_radius 
             (vsum 
                (vscaled (cos theta) ex) 
                (vscaled (sin theta) ey)
             )
          )
      in
      Some (a2, new_pos)            
;;

type  'a problem = {
  ubound: float;
  ptree: 'a IIS.tree;
}
;;

let fmin (a:float) (b:float) = 
  if a < b then a else b
;;

(* returns 2nd contact, new probe  *)
let second_contact probe a0 sphere_tree =

  let origin = a0.IIS.spos in
  let r0 = a0.IIS.radius in
  let rp = probe.IIS.radius in
  let ez = normed_diff probe.IIS.spos origin in
  
  let theta_of_a1 a1 =
    let r1 = a1.IIS.radius in
    let d1 = distance a1.IIS.spos origin in
    let gap = d1 -. (r1 +. r0) in
    if (a1 == a0) || (gap >= (2.0*.rp)) then (
      None
    )
    else if d1 +. r1 < r0 then( 
      error "theta_of_a1: should have no insiders";
    )
    else
      
        (* angle between probe and a1 *)
      let dth = 
        let a,b,c = r0 +. rp, d1, r1 +. rp in     
        let cosdthe = (a*.a +. b*.b -. c*.c)/.(2.0*.a*.b) in
        acos cosdthe
      and th1 = (* angle between a1 and z-axis (initial probe position) *)
        let costh1 = dot ez (normed_diff a1.IIS.spos origin) in
        acos costh1
      in
      if dth > th1 then
        error "second contact: possible collision";
      Some (th1 -. dth) 
        
  in
  let theta_a1_opt = 
    next_theta_and_sphere 
      theta_of_a1 origin (r0 +. 2.0*.rp) sphere_tree 
  in

  match theta_a1_opt with
    | None -> None
    | Some a1_theta -> 
      let theta, a1 = a1_theta in
      let ex =
        let vx = (cross (vdiff a1.IIS.spos origin) ez) in
        let vxn = vnorm vx in 
        if vxn = 0.0 then
          v3 1.0 0.0 0.0
        else
          vscaled (1.0/.vxn) vx
      in
      let ey = cross ez ex in

          (* rotate about ex *)
      let new_ppos = 
        let s,c = sin theta, cos theta in
        vsum origin
          (vscaled (r0 +. rp) (vsum (vscaled c ez) (vscaled s ey)))
      in
      Some (a1, {probe with IIS.spos = new_ppos; })
;;

let lowest_z_sphere sph_cont = 
  let rec loop zlist = 
    match zlist with
      | hd :: tail -> if hd.IIS.active then hd, tail else loop tail
      | [] -> error "lowest_z_sphere: empty"
  in
  let sph, tail = loop sph_cont.IIS.zlist in
  sph_cont.IIS.zlist <- sph :: tail;
  sph
;;

let first_contact pr sph_cont =
  match sph_cont.IIS.tree with
    | IIS.Empty -> None
    | _ -> 
      let a0 = lowest_z_sphere sph_cont in
      
      let probe = {
        IIS.radius = pr;
        IIS.spos = (
          let ez = v3 0.0 0.0 1.0 in
          vsum a0.IIS.spos (vscaled (-.(pr +. a0.IIS.radius)) ez)
        );
        IIS.number = -1;
        IIS.active = false;
        IIS.actual_number = -1;
      }
      in
      Some (a0, probe)
;;

type trio_or_dangler = 
  | Trio of (int*int*int)*Vec3.t 
  | Dangler of IIS.sphere
  | Nothing
;;

(* returns (a0,a1,a2), ppos where a0,a1,a2 are cw *)
let first_trio pr sph_cont =
  let sphere_tree = sph_cont.IIS.tree in
  let a0_probe0_opt = first_contact pr sph_cont in
  match a0_probe0_opt with
    | None -> Nothing
    | Some a0_probe0 ->
      let a0, probe0 = a0_probe0 in
      let a1_probe1_opt = second_contact probe0 a0 sphere_tree in 
      match a1_probe1_opt with
        | None -> Dangler a0
        | Some a1_probe1 ->
          let a1, probe1 = a1_probe1 in             
          let a2_ppos_opt = 
            new_probe_position probe1 a0 a1 sph_cont.IIS.tree
          in
          match a2_ppos_opt with
            | None -> Dangler a0
            | Some a2_ppos ->
              let a2, ppos = a2_ppos in
              Trio ((a0.IIS.number,a1.IIS.number,a2.IIS.number), ppos)
                
;;

let ordered (i:int) (j:int) = 
  if i < j then
    i,j
  else
    j,i
;;

let execute_on_stack pr stack trios sph_cont =
  (* cw, with i0 lowest *)
  let entry = Stack.pop stack in
  let old_tri, pos = entry in
  let i0,i1,i2 = old_tri in

  let probe = {
    IIS.spos = pos;
    IIS.radius = pr;
    IIS.number = -1;
    IIS.active = false;
    IIS.actual_number = -1;
  }
  in

  let spheres = sph_cont.IIS.spheres 
  and tree = sph_cont.IIS.tree in
  let process_pair i0 i1 i2old =
    let a0 = spheres.(i0)
    and a1 = spheres.(i1) 
    in
    let a2_new_pos_opt = new_probe_position probe a0 a1 tree in
    match a2_new_pos_opt with

      | None -> error "execute_on_stack: should find new probe position\n"

      | Some a2_new_pos -> 
        let a2, new_pos = a2_new_pos in
        let i2 = a2.IIS.number in
        let j0,j1,j2 = 
          if i0 < i1 && i0 < i2 then
            i0,i1,i2
          else if i1 < i0 && i1 < i2 then
            i1,i2,i0
          else
            i2,i0,i1
        in
        let new_tri = j0,j1,j2 in
        if not (H.mem trios (j0,j1,j2)) then (
                (* Printf.printf "trio %d %d %d\n" j0 j1 j2; flush stdout; *)
          H.add trios new_tri true;
          Stack.push (new_tri, new_pos) stack;
        );
        
  in
  process_pair i1 i0 i2;
  process_pair i2 i1 i0;
  process_pair i0 i2 i1

;;

let keys dict = 
  let key_dict = H.create 0 in
  H.iter
    (fun key value ->
      if not (H.mem key_dict key) then
        H.add key_dict key true;
    )
    dict;
  H.fold 
    (fun key dum res -> key :: res)
    key_dict
    []
;;

let hsize dict = List.length (keys dict);;

let build_cage trios sph_cont probe_radius trio_info = 
  let stack = Stack.create() in
  let (a0,a1,a2), ppos = trio_info in
  let i0,i1,i2 = a0.IIS.number, a1.IIS.number, a2.IIS.number in
  let ii0, ii1, ii2 = 
    if i0 < i1 && i0 < i2 then
      i0,i1,i2
    else if i1 < i0 && i1 < i2 then
      i1,i2,i0
    else
      i2,i0,i1
  in    
  H.add trios (ii0,ii1,ii2) true;
  Stack.push ((ii0, ii1, ii2), ppos) stack;
  
  while not (Stack.is_empty stack) do
    execute_on_stack probe_radius stack trios sph_cont;
  done
;;

(* returns sorted array *)
let surface_indices trios = 
  let add = Indices.add in

  let iots = 
    Array.of_list
      (Indices.elements
         (Array.fold_left 
            (fun res tri ->
              let i0,i1,i2 = tri in
              add i0 (add i1 (add i2 res))
            )
            Indices.empty
            trios
         )
      )
  in
  Array.sort compare iots;
  iots
;;

let hcadd tbl key value = 
  if not (H.mem tbl key) then
    H.add tbl key value
;;

let hmerge_to tbl0 tbl1 = 
  H.iter 
    (fun key value -> hcadd tbl1 key value)
    tbl0
;;

let hmerged tbl0 tbl1 = 
  let tbl = H.create 0 in
  hmerge_to tbl0 tbl;
  hmerge_to tbl1 tbl;
  tbl
;;

let hcopy tbl0 = 
  let tbl = H.create 0 in
  hmerged tbl0 tbl
;;

let hdiff tbl1 tbl0 = 
  let tbl = H.create 0 in
  H.iter
    (fun key value ->
      if not (H.mem tbl0 key) then
        H.add tbl key value;
    )
    tbl1;
  tbl
;;

let hsubtract tbl tbl0 = 
  H.iter
    (fun key value ->
      if H.mem tbl0 key then
        H.remove tbl0 key
    )
    tbl
;;

let remove_ears trios ear_spheres = 
  let ears = 
    H.fold
      (fun trio dum list ->
        let i0,i1,i2 = trio in
        let rtrio = i0,i2,i1 in
        if H.mem trios rtrio then
          trio :: rtrio :: list
        else
          list
      )
      trios
      []
  in
  List.iter
    (fun trio ->
      H.remove trios trio;
      let i0,i1,i2 = trio in
      H.replace ear_spheres i0 true;
      H.replace ear_spheres i1 true;
      H.replace ear_spheres i2 true;
    )
    ears;
  
  let trio_spheres = 
    let ts_dict = H.create 0 in
    H.iter
      (fun trio dum ->
        let i0,i1,i2 = trio in
        H.replace ts_dict i0 true;
        H.replace ts_dict i1 true;
        H.replace ts_dict i2 true;
      )
      trios;
    ts_dict
  in
  hsubtract trio_spheres ear_spheres
;;

let triangle_center spheres tri = 
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).IIS.spos
  and p1 = spheres.(i1).IIS.spos
  and p2 = spheres.(i2).IIS.spos
  in
  let center,r = IIS.Spheres.circumcircle p0 p1 p2 in
  center
;;

let triangle_size spheres tri = 
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).IIS.spos
  and p1 = spheres.(i1).IIS.spos
  and p2 = spheres.(i2).IIS.spos in
  let center,r = IIS.Spheres.circumcircle p0 p1 p2 in
  r
;;

let new_tri_search_tree triangles spheres = 
  IIS.search_tree
    (fun tri ->
      triangle_center spheres tri
    )
    (fun tri ->
      triangle_size spheres tri
    )
    triangles
;;

let inside_tester spheres trios =
  if (len trios) < 4 then
    (fun pt -> false)
  else (
    let tri_search_tree = new_tri_search_tree trios spheres in
    let poses = Array.map (fun sph -> sph.IIS.spos) spheres in
    let max_sradius = Array.fold_left (fun res sph -> max res sph.IIS.radius) 0.0 spheres in  
    let center, pradius = IIS.Spheres.smallest_enclosing_sphere poses in
    let radius = 2.0*.(max_sradius +. pradius) in
    let outer_pt = vsum (v3 0.0 0.0 radius) center in
    (fun pt ->
      IIS.is_inside_spheres tri_search_tree spheres outer_pt pt
    )
  )
;;

let ordered_trios ijk = 
  let i,j,k = ijk in
  let nmax = max (max i j) k
  and nmin = min (min i j) k
  in
  let nmid = 
    let max_or_min n = (n = nmax) || (n = nmin) in
    if not (max_or_min i) then
      i
    else if not (max_or_min j) then
      j
    else
      k
  in
  (nmin, nmid, nmax), (nmin, nmax, nmid)
;;

let rec sphere_tree_size tree =
  match tree with
    | IIS.Empty -> 0
    | IIS.Leaf lf -> 
      if lf.IIS.child.IIS.active then 1 else 0
    | IIS.Branch br ->
      let ch0, ch1 = br.IIS.children in
      (tree_size ch0) + (sphere_tree_size ch1)
;;

let new_trio_dicts probe_radius all_spheres =
  let danglers = H.create 0
  and all_trios = H.create 0 
  and sph_cont = IIS.partitioned_spheres all_spheres 
  in
  
  let rec loop nrem0 nrem =
    if nrem > 0 then
      let new_nrem0 = 
        if nrem < nrem0/2  then (
          IIS.update_sphere_container sph_cont;  
          nrem
        )
        else
          nrem0
      in
      
      let ftrio =  first_trio probe_radius sph_cont in
      
      match ftrio with
        | Nothing -> ()
          
        | Dangler ad -> (
          H.add danglers ad.IIS.number true;
          ad.IIS.active <- false;
          loop new_nrem0 (nrem-1);
        )
          
        | Trio (aaa, ppos) ->
          
          let i0,i1,i2 = aaa in
          let a0,a1,a2 = all_spheres.(i0), all_spheres.(i1), all_spheres.(i2) in
          let trios = H.create 0 in
          let ear_spheres = H.create 0 in
          build_cage trios sph_cont probe_radius ((a0,a1,a2),ppos);
          remove_ears trios ear_spheres;
          H.iter (fun i dum -> all_spheres.(i).IIS.active <- false) 
            ear_spheres;
          hmerge_to ear_spheres danglers;

          if (H.length trios) > 0 then (
            
            let trio_sphere_dict = 
              let res = H.create 0 in
              H.iter
                (fun trio dum ->
                  let i0,i1,i2 = trio in
                  let cadd i = H.replace res i true in
                  cadd i0; cadd i1; cadd i2;
                )
                trios;
              res
            in
            
            H.iter
              (fun i dum ->
                all_spheres.(i).IIS.active <- false;
              )
              trio_sphere_dict;
            
            let cage_center, cage_radius = 
              let actual_trio_sphere_array = 
                Array.of_list (List.rev (List.rev_map (fun i -> all_spheres.(i)) (keys trio_sphere_dict))) in
              IIS.smallest_enclosing_sphere_of_spheres actual_trio_sphere_array 
            in                       
            
            let inside_cage = 
              let trio_array = Array.of_list (keys trios) in
              inside_tester all_spheres trio_array
            in 
            
            let n_interiors =
              match
                IIS.fold_tree_within_distance
                  (fun sph -> sph.IIS.active) 
                  cage_center cage_radius 
                  (fun sph  -> 
                    if sph.IIS.active && (inside_cage sph.IIS.spos) then (      
                      sph.IIS.active <- false; 
                      1;
                    )
                    else
                      0               
                  )
                  (+)
                  sph_cont.IIS.tree;
              with
                | Some nint -> nint
                | None -> 0
            in
            hmerge_to trios all_trios;
            
            let nremoved = n_interiors + (hsize trio_sphere_dict) + (hsize ear_spheres) in                    
            loop new_nrem0 (nrem - nremoved); 
          )                 
  in        
  let n = sphere_tree_size sph_cont.IIS.tree in
  loop n n;
  all_trios, danglers
;;

type tri_contact = 
  | Plane
  | Vertex of int
  | Edge of int*int
;;

let triangle_contact spheres tri pos =
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).IIS.spos
  and p1 = spheres.(i1).IIS.spos
  and p2 = spheres.(i2).IIS.spos
  in
  
  let d01 = vdiff p1 p0
  and d12 = vdiff p2 p1
  and d20 = vdiff p0 p2
  in
  let perp = cross d01 d12 in
  let p01 = cross perp d01  
  and p12 = cross perp d12
  and p20 = cross perp d20 
  in
  if 
    (dot (vdiff pos p0) p01) > 0.0 &&
      (dot (vdiff pos p1) p12) > 0.0 &&
      (dot (vdiff pos p2) p20) > 0.0 
  then ( (* on triangle *)
    let nperp = normed perp in

    (abs_float (dot (vdiff pos p0) nperp)), Plane
  )
  else
    
    let dist_to_line p0 p1 = 
      let d01 = vdiff p1 p0 in
      let nd01 = vnorm d01 in
      let axis = vscaled (1.0/.nd01) d01 in
      let d0 = vdiff pos p0 in
      let proj = dot d0 axis in

      if proj > nd01 || proj < 0.0 then
        None
      else
        Some (distance d0 (vscaled proj axis))
    in
    let min_line_dist = 
      List.fold_left
        (fun res ldistij ->
          let ldist, ij = ldistij in
          match ldist with
            | None -> res
            | Some d -> 
              let old_d, old_ij = res in
              match old_d with
                | None -> (Some d), ij
                | Some old_d ->
                  if d < old_d then
                    (Some d), ij
                  else
                    res
        )
        (None,(-1,-1))
        [ (dist_to_line p0 p1), ordered i0 i1; 
          (dist_to_line p1 p2), ordered i1 i2; 
          (dist_to_line p2 p0), ordered i2 i0;] 
    in

    let min_vert_dist = 
      let d0 = distance p0 pos
      and d1 = distance p1 pos
      and d2 = distance p2 pos
      in
      let min_d = fmin (fmin d0 d1) d2 in
      if min_d = d0 then
        d0, i0
      else if min_d = d1 then
        d1, i1
      else
        d2, i2
    in
    
    let med, edge = min_line_dist 
    and mvd, vertex = min_vert_dist in

    match med with
      | None ->
        mvd, (Vertex vertex);

      | Some d -> 
        if d < mvd then
          let j0,j1 = edge in
          d, (Edge (j0, j1))
        else
          mvd, (Vertex vertex)
;;

let triangle_distance spheres tri pos =
  let d, contact = triangle_contact spheres tri pos in
  d
;;

let is_in items item = 
  if (len items) = 0 then
    false
  else
    let rec recurse il ih = 
      if ih - il = 0 then
        il
      else
        let im = (ih + il)/2 in
        if item = items.(im) then
          im
        else if item < items.(im) then
          recurse il im
        else
          recurse (im+1) ih
    in
    let im = recurse 0 ((len items)-1) in
    item = items.(im)         
;;

let rot_to_min i0 i1 i2 = 
  if i0 < i1 && i0 < i2 then
    i0,i1,i2
  else if i1 < i0 && i1 < i2 then
    i1,i2,i0
  else
    i2,i0,i1
;;

let array_3d nx ny nz a = 
  Array.init nx
    (fun i ->
      Array.init ny 
        (fun i ->
          Array.init nz
            (fun i -> a)))
;;

type grid_type = 
  | Known of bool
  | Unknown
;;

(* returns trios, surface sphere indices, dangler sphere indices *)
let probe_radius = ref 1.5;;
let cornerx = ref nan;;
let cornery = ref nan;;
let cornerz = ref nan;;
let vsize = ref nan;;
let ngridx = ref 100;;
let ngridy = ref 100;;
let ngridz = ref 100;;
let include_all_ref = ref false;;


Arg.parse 
  [
    ("-probe_radius", (Arg.Set_float probe_radius), 
     ": changes probe radius from default of 1.5"
    );

    ("-all", Arg.Set include_all_ref, ": simply include all spheres in surface");

  ] 
  (fun a -> 
    Printf.printf "needs args\n";
  )
  "Generates reduced surface of spheres from xml file describing spheres. Outputs list of surface triangles, indices of surface spheres, indices of spheres completely enclosed by a surface sphere, and indices of \"danglers\", or spheres that are exterior but are not in a surface triangle."
;;

let include_all = !include_all_ref;;

let is_nan x = 
  match classify_float x with
    | FP_nan -> true
    | _ -> false
;;

(* returns list of cycles *)
let rec edge_cycles vert epairs = 

  match epairs with
    | head :: tail ->
      let pair, (u0,u1) = head in

      let rec loop cycle epair0 u started =
        
        if started && epair0 = head then
          cycle
        else

          let epair1 = 
            List.find 
              (fun ep -> 
                let (p0,p1), offvs = ep in
                (u = p0 || u = p1) && (not (ep == epair0)) &&
                  (not (List.mem ep cycle))
              )
              epairs
          in
          let pair1, (u0,u1) = epair1 in
          
          let vv0,vv1 = pair1 in
          let u_next = if vv0 = vert then u1 else u0 in
          loop (epair1::cycle) epair1 u_next true
            
      in
      let v0,v1 = pair in
      let u = if v0 = vert then u1 else u0 in
      let cycle = loop [] head u false in
      let rest = 
        List.filter 
          (fun epair -> not (List.memq epair cycle))
          epairs
      in
      cycle :: (edge_cycles vert rest)
        
    | [] -> []
;;      

let rec is_inside sph0 tree =
  let pos0 = sph0.IIS.spos in
  match tree with
    | IIS.Empty -> false
    | IIS.Leaf lf -> 
      let sph1 = lf.IIS.child in
      if (sph1 == sph0) then
        false
      else
        let d = distance pos0 sph1.IIS.spos in
        sph0.IIS.radius +. d < sph1.IIS.radius 

    | IIS.Branch br ->
      let d = distance pos0 br.IIS.bpos in
      if d < br.IIS.bradius then
        let ch0, ch1 = br.IIS.children in
        (is_inside sph0 ch0) || (is_inside sph0 ch1)
      else
        false
;;

let insider_spheres spheres = 
  let sph_cont = IIS.partitioned_spheres spheres in
  let tree = sph_cont.IIS.tree in
  Array.of_list
    (List.filter
       (fun sph -> is_inside sph tree)
       (Array.to_list spheres)
    )
;;

let surface_insiders sspheres insiders = 
  let sph_cont = IIS.partitioned_spheres sspheres in
  let tree = sph_cont.IIS.tree in
  Array.of_list
    (List.filter
       (fun sph -> is_inside sph tree)
       (Array.to_list insiders)
    )
;;

let jiggle spheres =
  let factor = 0.001 in
  let seed = 1111111 in
  Random.init seed;

  let nebors = IIS.nearest_neighbors (fun sph -> sph.IIS.spos) spheres in

  H.iter
    (fun sph nebor ->
      let d = distance sph.IIS.spos nebor.IIS.spos in
      let rand () = factor*.d*.( 1.0 -. 2.0*.(Random.float 1.0)) in
      sph.IIS.spos.x <- sph.IIS.spos.x +. rand();
      sph.IIS.spos.y <- sph.IIS.spos.y +. rand();
      sph.IIS.spos.z <- sph.IIS.spos.z +. rand();
    )
    nebors
;;

(* checks topology of cage: each pair must have two triangles *)
let check_consistency trios = 
  let pair_dict = H.create 0 in

  Array.iter
    (fun trio ->
      let hadd i j = 
        H.add pair_dict (i,j) true;
        H.add pair_dict (j,i) false;
      in
      let i0,i1,i2 = trio in
      hadd i0 i1;
      hadd i1 i2;
      hadd i2 i0;
    )
    trios;
  let pairs = H.fold
    (fun pair parity list ->
      if parity then
        pair :: list
      else 
        list
    )
    pair_dict
    [] 
  in
  List.iter
    (fun pair -> 
      let i0,i1 = pair in
      H.remove pair_dict pair;
      if H.mem pair_dict (i1,i0) then
        H.remove pair_dict (i1,i0)
      else
        error "not consistent"
          
    )
    pairs;

  if (hsize pair_dict) > 0 then
    error "not consistent"
;;

let do_xyzr () =

  let all_sphere_list = ref [] in
  let i = ref 0 in

  let put_sphere ~rname ~rnumber ~aname ~anumber:anum ~x ~y ~z ~radius:r ~charge:q = 

    let sphere = {
      IIS.spos = v3 x y z;
      IIS.radius = r;
      IIS.number = !i;
      IIS.active = true;
      IIS.actual_number = anum;
    }
    in
    i := !i + 1;
    all_sphere_list :=  sphere :: (!all_sphere_list);
  in    
  Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin ~f: put_sphere;
  
  let all_spheres = Array.of_list (List.rev !all_sphere_list) in

  jiggle all_spheres;

  let spheres, insiders = 
    let insiders = insider_spheres all_spheres in
    let insider_dict = H.create 0 in 
    Array.iter (fun insider -> H.add insider_dict insider true) insiders;
    let sphere_list = 
      List.filter (fun sph -> not (H.mem insider_dict sph)) !all_sphere_list
    in
    Array.of_list sphere_list, insiders
  in
  Array.iteri (fun i sph -> sph.IIS.number <- i) spheres;
  
  let print0 () = 
    pri "<reduced_surface>\n";
    pri "  <n-spheres> %d </n-spheres>\n" (len spheres);
    pri "  <probe_radius> %f </probe_radius>\n" !probe_radius;
  in

  let print_spheres spheres indices = 
    pri "      <n> %d </n>\n" (len indices);
    pri "      <indices>\n";
    Array.iteri
      (fun i index ->
        if i mod 10 = 0 then pri "        ";
        pri "%d " spheres.(index).IIS.actual_number;
        if i mod 10 = 9 then pri "\n";
      )         
      indices;
    pri "\n";
    pri "      </indices>\n"
      
  in    
  if (len spheres) < 0 || include_all then (
    print0();
    pri "  <trios> </trios>\n";
    pri "  <spheres>\n";
    pri "    <surface> <n> 0 </n> </surface>\n";
    pri "    <danglers>\n";
    let indices = Array.init (len all_spheres) (fun i -> i) in
    print_spheres all_spheres indices;        
    pri "    </danglers>\n";        
    pri "    <insiders> <n> 0 </n> </insiders>\n";          
  )
    
  else (    
    let trio_dict, danglers = new_trio_dicts !probe_radius spheres in
    let trios = Array.of_list (keys trio_dict) in
    check_consistency trios;
    
    let ssindices = surface_indices trios     in
    let dsindices = Array.of_list (keys danglers) in
    let isindices = 
      let sds_indices = Array.append ssindices dsindices in
      let sds_spheres = Array.map (fun i -> spheres.(i)) sds_indices in
      let si_spheres = surface_insiders sds_spheres insiders in
      Array.map (fun sph -> sph.IIS.number) si_spheres
    in
    print0();
    
    pri "  <trios>\n";
    pri "    <n> %d </n>\n" (len trios);
    Array.iter
      (fun trio ->
        let ix,iy,iz = trio in
        let tn i = spheres.(i).IIS.actual_number in
        pri "      <trio>%d %d %d </trio>\n" 
          (tn ix) (tn iy) (tn iz)
      )
      trios;
    pri "  </trios>\n";
    
    pri "  <spheres>\n";            
    
    pri "    <surface>\n";
    print_spheres spheres ssindices;
    pri "    </surface>\n";
    
    pri "    <danglers>\n";
    print_spheres spheres dsindices;
    pri "    </danglers>\n";
    
    pri "    <insiders>\n";
    print_spheres all_spheres isindices;
    pri "    </insiders>\n";       
  );
  pri "  </spheres>\n";
  pri "</reduced_surface>\n";
;;

do_xyzr();;  


