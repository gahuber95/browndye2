(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Used internally.  This module is used to construct tree data structures
of spheres and bounding triangles, and to use those structures to determine 
whether a point lies inside the assemblage of spheres.
*)

open Vec3;;

let error msg = 
  raise (Failure msg)
;;

type sphere = {
  spos: Vec3.t;
  radius: float;
  mutable number: int;
  mutable active: bool;
  actual_number: int;
};;

type 'a branch = {
  bpos: Vec3.t;
  bradius: float;
  children: ('a tree)*('a tree)
}

and 'a leaf = {
  lpos: Vec3.t;
  lradius: float;
  child: 'a;
}

and 'a tree = 
  | Leaf of 'a leaf
  | Branch of 'a branch
  | Empty
;;

type sphere_container = {
  mutable tree: sphere tree;
  low_corner: Vec3.t;
  high_corner: Vec3.t;
  spheres: sphere array;
  mutable zlist: sphere list;
}
;;

let rec items_of_tree tree = 
  match tree with
    | Leaf leaf ->
	[leaf.child]
    | Branch br ->
	let child0, child1 = br.children in
	let items0 = items_of_tree child0
	and items1 = items_of_tree child1 in
	  List.rev_append items0 items1
    | Empty -> []
;;

let rec with_removed tree sph = 
  match tree with
    | Leaf leaf ->
	if leaf.child == sph then
	  Empty
	else
	  tree

    | Branch br ->
	if (distance br.bpos sph.spos) < br.bradius then
	  let child0, child1 = br.children in
	  let new0 = with_removed child0 sph
	  and new1 = with_removed child1 sph in

	    match new0, new1 with
	      | Branch br0, Empty -> new0
	      | Empty, Branch br1 -> new1
	      | Empty, Empty -> Empty
	      | _,_ ->
		  Branch 
		    {br with
		       children = new0, new1
		    }
	else
	  tree

    | Empty -> Empty
;;

(* p0 to left, p1 to right, q0 straight above p0-p1, q1 below.  
Returns 1 if q0-q1 passes above p0-p1, -1 if to left, and 0 if
through *)
let line_passing p0 p1 q0 q1 = 
  let d0 = vdiff p0 q0
  and d1 = vdiff p1 q0 in
  let cp = cross d1 d0 in
  let dd = vdiff q1 q0 in
    compare (dot dd cp) 0.0  
;;


let distance_point_line p0 p1 p = 
  let d01 = vdiff p0 p1
  and d = vdiff p p1 in
  let alpha = (dot d01 d)/.(dot d01 d01) in
    if alpha <= 0.0 then
      distance p1 p
    else if alpha >= 1.0 then
      distance p0 p
    else
      let mid = vsum (vscaled alpha p0) (vscaled (1.0-.alpha) p1) in
	distance mid p
;;

type intersection = | IYes | INo | IEdge;;

(* number of crossings, true if edge is hit *)
let rec n_crossings tree crossing p0 p1 = 
  match tree with
    | Leaf leaf -> (
	match crossing leaf.child p0 p1 with
	  | INo -> 0, false
	  | IYes -> 1, false
	  | IEdge -> 1, true
      )
    | Branch branch -> 
	if (distance_point_line p0 p1 branch.bpos) > branch.bradius then
	  0, false
	else 
	  let child0, child1 = branch.children in
	  let n0, e0 = n_crossings child0 crossing p0 p1
	  and n1, e1 = n_crossings child1 crossing p0 p1
	  in
	    (n0 + n1), (e0 || e1)
    | Empty -> 0, false
;;


let triangle_line_intersection spheres tri p0 p1 =
  let i0,i1,i2 = tri in
  let q0,q1,q2 = spheres.(i0).spos, spheres.(i1).spos, spheres.(i2).spos in

  let lp01 = line_passing p0 p1 q0 q1
  and lp12 = line_passing p0 p1 q1 q2
  and lp20 = line_passing p0 p1 q2 q0
  in  
    if (lp01 = 0) || (lp12 = 0) || (lp20 = 0) then
      IEdge
    else if (lp01 = lp12) && (lp12 = lp20) then 
      let nrm = 
	let d01 = vdiff q1 q0
	and d02 = vdiff q2 q0
	in
	  cross d01 d02
      in
      let dp0 = vdiff p0 q0
      and dp1 = vdiff p1 q0
      in
	if ((dot dp0 nrm) *. (dot dp1 nrm)) < 0.0 then
	  IYes
	else
	INo
    else
      INo
;;

let is_inside_spheres tri_search_tree spheres outer_pt0 pt =

  let perturb = 1.0e-8 *. (distance pt outer_pt0) in

  let outer_pt = {
    x = outer_pt0.x;
    y = outer_pt0.y;
    z = outer_pt0.z;
  }
  in
  let nc = 
    let finis = ref false in
    let nc_ref = ref 0 in
      while not !finis do
	let crossing = triangle_line_intersection spheres in
	let nc, edge = n_crossings tri_search_tree crossing pt outer_pt in
	  if edge then (
	    outer_pt.x <- outer_pt.x +. (Random.float perturb);
	    outer_pt.y <- outer_pt.y +. (Random.float perturb);
	    outer_pt.z <- outer_pt.z +. (Random.float perturb);
	  )
	  else (
	    nc_ref := nc;
	    finis := true
	  )
      done;
      !nc_ref
  in
    ((nc mod 2) = 1)
;;

module V3 = 
struct
  type t = Vec3.t
  type reff = int
  type container = Vec3.t array
  let v3 = v3
  let xyz v = v.x, v.y, v.z
  let sum = vsum
  let diff = vdiff
  let scaled = vscaled
  let dot = dot
  let cross = cross
  let distance = distance
  let position arr i = arr.(i)
  let to_array arr = Array.init (Array.length arr) (fun i -> i)
end
;;

let fold12 f g items = 
  match items with
    | head :: tail ->
	List.fold_left
	  (fun res item ->
	    g res (f item)
	  )
	  (f head)
	  tail
    | [] -> 
	error "fold12: nothing in list"
;;

let array_fold12 f g items = 
  let res = ref (f items.(0)) in
  let n = Array.length items in
    for i = 1 to n-1 do
      let item = items.(i) in
      res := g !res (f item)
    done;
    !res
;;

module Spheres = Circumspheres.M( V3);;

(* uses radii as well *)
let smallest_enclosing_sphere_of_spheres spheres = 
  let pts = Array.map (fun sph -> sph.spos) spheres in
  let center, r = Spheres.smallest_enclosing_sphere pts in
  let aug_r = 
    Array.fold_left
      (fun res sph ->
	let d = (distance sph.spos center) +. sph.radius in
	  max d res
      )
      0.0
      spheres
  in
    center, aug_r
;;

let rec search_tree coords radius items = 

  let n = Array.length items in

    if n == 1 then
      let item = items.(0) in
	Leaf {
	  child = item;
	  lpos = coords item;
	  lradius = radius item;
	}
    else (
      let pts = Array.map coords items in
      let center, max_rp = 
	  Spheres.smallest_enclosing_sphere pts
      in
      let max_item, max_r = 
	array_fold12
	  (fun item -> item, (distance center (coords item)) +. (radius item))
	  (fun res0 res1 ->
	    let item0, r0 = res0
	    and item1, r1 = res1
	    in
	      if r0 > r1 then
		res0
	      else
		res1
	  )
	  items
      in
      let axis = normed_diff (coords max_item) center in
	
      let sitems = 
	Array.init n (fun i -> items.(i)) 
      in
	
	Array.sort
	  (fun item0 item1 ->
	    let proj item = 
	      dot (vdiff (coords item) center) axis
	    in
	      compare (proj item0) (proj item1)
	  )
	  sitems;
	
	let nleft, nright = 
	  let hn = n/2 in
	    if (n mod 2) = 0 then
	      hn,hn
	    else
	      hn+1, hn
	in
	  
	let low_items = 
	  Array.init 
	    nleft 
	    (fun i -> sitems.(i))
	and high_items = 
	  Array.init
	    nright
	    (fun i -> sitems.(i+nleft))
	in

	let low_tree = search_tree coords radius low_items
	and high_tree = search_tree coords radius high_items
	in
	  Branch {
	    bpos = center;
	    bradius = max_r;
	    children = low_tree, high_tree;
	  }
    )
;;

let partitioned_spheres spheres = 

  let bounds f = 
    Array.fold_left
      (fun res sph ->
	let lowx, highx = res in
	let x = f sph.spos in
	  if x < lowx then
	    x, highx
	  else if x > highx then
	    lowx, x
	  else
	    res
      )
      (infinity, neg_infinity)
      spheres
  in


  let lowx, highx = 
    bounds (fun pos -> pos.x)

  and lowy, highy = 
    bounds (fun pos -> pos.y)

  and lowz, highz = 
    bounds (fun pos -> pos.z)
  in

   {
     tree = search_tree 
       (fun sph -> sph.spos) (fun sph -> sph.radius) spheres;
     
     
     low_corner = v3 lowx lowy lowz;
     high_corner = v3 highx highy highz;
     spheres = spheres;
     zlist = 
       let zspheres = 
	 Array.init (Array.length spheres) (fun i -> spheres.(i)) in
	 Array.sort 
	   (fun sph0 sph1 -> 
	     let sval sph = sph.spos.z -. sph.radius in
	       compare (sval sph0) (sval sph1)
	   )
	   zspheres;
	 Array.to_list zspheres
	 ;
	     
  }

let rec updated tree = 
  match tree with
    | Leaf leaf ->
	if leaf.child.active then
	  tree
	else
	  Empty

    | Empty -> Empty

    | Branch br ->
	let ch0,ch1 = br.children in
	let uch0 = updated ch0
	and uch1 = updated ch1 
	in
	  match uch0, uch1 with
	    | Empty, _ -> uch1
	    | _, Empty -> uch0
	    | _, _ -> 
		Branch 
		  {br
		   with children = uch0, uch1;
		  }
;;

let update_sphere_container sph_cont = 
  sph_cont.tree <- updated (sph_cont.tree)
;;


let rec fold_tree_within_distance is_active center r f g tree =

  let ftwd = fold_tree_within_distance is_active center r f g in

    match tree with
      | Leaf leaf ->
	  if (is_active leaf.child) && 
	    ((distance leaf.lpos center) -. leaf.lradius < r)
	  then 
	    Some (f leaf.child)
	  
	  else
	    None

      | Branch branch ->
	  let d = (distance branch.bpos center) -. branch.bradius in 
	    if d < r then
	      let child0, child1 = branch.children in
	      let res0 = ftwd child0
	      and res1 = ftwd child1
	      in
		match res0, res1 with
		  | Some rs0, Some rs1 -> 
		      Some (g rs0 rs1)
		  | Some rs0, None -> res0
		  | None, Some rs1 -> res1
		  | None, None -> None

	    else
	      None
      | Empty -> None
;;

let iter_tree_within_distance is_active center r f tree = 
  let ff = (fun i -> f i; 0) in
  ignore 
    (fold_tree_within_distance is_active center r ff
	(fun i0 i1 -> 0) tree
    )
;;

module H = Hashtbl;;

let nearest_neighbors coords items =

  let dist item0 item1 = 
    let pos0 = coords item0
    and pos1 = coords item1 in
      distance pos0 pos1
  in
 
  let pairs = H.create 0 in

  let pos_n_rad tree = 
    match tree with
      | Leaf leaf -> leaf.lpos, leaf.lradius
      | Branch br -> br.bpos, br.bradius
      | Empty -> v3 nan nan nan, 0.0
  in

  let cadd item neb =
 
    if H.mem pairs item then (
      let neb0 = H.find pairs item in
	if (dist neb item) < (dist neb0 item) then
	  H.replace pairs item neb
    )
    else
      H.add pairs item neb
  in
    
  let are_close tree0 tree1 =
    match tree0, tree1 with
      | Leaf lf0, Leaf lf1 -> true

      | _, _ -> 

	  let pos0, rad0 = pos_n_rad tree0
	  and pos1, rad1 = pos_n_rad tree1
	  in
	  let d = distance pos0 pos1 in
	  let gap = d -. (rad0 +. rad1) in
	  let max_rad = max rad0 rad1 in
	    gap <= max_rad
  in

  let rec pair_up_cousins tree0 tree1 =
    if are_close tree0 tree1 then (
      match tree0, tree1 with
	| Leaf lf0, Leaf lf1 ->
	    let ch0, ch1 = lf0.child, lf1.child in
	      cadd ch0 ch1;
	      cadd ch1 ch0;
	      
	| Branch br0, Branch br1 ->
	    let ch0a, ch0b = br0.children
	    and ch1a, ch1b = br1.children
	    in
	      pair_up_cousins ch0a ch1a;
	      pair_up_cousins ch0a ch1b;
	      pair_up_cousins ch0b ch1a;
	      pair_up_cousins ch0b ch1b;

	| Branch br0, Leaf lf1 ->
	    let ch0a, ch0b = br0.children in
	      pair_up_cousins ch0a tree1;
	      pair_up_cousins ch0b tree1;

	| Leaf lf0, Branch br1 ->
	    let ch1a, ch1b = br1.children in
	      pair_up_cousins tree0 ch1a;
	      pair_up_cousins tree0 ch1b;

	| _,_ -> ()
    )
  in
  let rec pair_up_brothers tree0 tree1 = 
    if are_close tree0 tree1 then (
      match tree0, tree1 with
	| Leaf lf0, Leaf lf1 ->
	    let ch0, ch1 = lf0.child, lf1.child in
	      cadd ch0 ch1;
	      cadd ch1 ch0;
	
	| Branch br0, Branch br1 ->
	    let ch0a, ch0b = br0.children
	    and ch1a, ch1b = br1.children
	    in
	      pair_up_brothers ch0a ch0b;
	      pair_up_brothers ch1a ch1b;
	      pair_up_cousins ch0a ch1a;
	      pair_up_cousins ch0a ch1b;
	      pair_up_cousins ch0b ch1a;
	      pair_up_cousins ch0b ch1b;
      
	| Branch br0, Leaf lf1 ->
	    let ch0a, ch0b = br0.children in
	      pair_up_brothers ch0a ch0b;
	      pair_up_cousins ch0a tree1;
	      pair_up_cousins ch0b tree1;

	| Leaf lf0, Branch br1 ->
	    let ch1a, ch1b = br1.children in
	      pair_up_brothers ch1a ch1b;
	      pair_up_cousins tree0 ch1a;
	      pair_up_cousins tree0 ch1b;

	| _,_ -> ()
    )
  in
  let tree = search_tree coords (fun item -> 0.0) items in
    (match tree with 
      | Branch br ->
	  let cha, chb = br.children in
	    pair_up_brothers cha chb
      | Leaf lf -> ()
      | Empty -> ()
    );
    pairs
;;    




