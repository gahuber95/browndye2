#pragma once

#include <list>
#include "../../global/error_msg.hh"
#include <list>
#include <iostream>
#include <memory>
#include <string>
#include "../../global/debug.hh"

/* 
Tree owns all pointers given to it.

Interface static members:
typename Contained;
typename Point;

returns true if t0 encloses t1
bool encloses( const T& t0, const T& t1)

template< class Info>
bool is_inside( Info&, const Point&, const T& t)

bool overlap( const T& t0, const T& t1)

void error( const char* msg)

*/



namespace Browndye::SField_Ptr{
  using std::make_shared;
  using std::shared_ptr;
  //using std::unique_ptr;

template< class Interface>
class Tree{
public:
  typedef typename Interface::Contained T;
  typedef typename Interface::Point Point;
  typedef Interface I;

  struct Hint{
    typedef Tree<I> Tr;
    const Tree<I>* data = nullptr;
  };

  void insert( shared_ptr<T> item);

  Hint first_hint() const;

  template< class Info>
  T* lowest_found( Info& info, const Point&, Hint&) const;

  Tree();

  void check_consistency() const;
  void check_for_loops() const;

  template< class F>
  void apply( F&& f);

  const T& top_item() const;

private:
  shared_ptr<T> self;
  std::list< std::shared_ptr< Tree<I> > > children;
  Tree<I>* parent = nullptr;

  void check_consistency_rec( const Tree<I>* ) const;
};

template< class I>
inline
auto Tree< I>::top_item() const -> const T&{
  return *self;
}

template< class I>
template< class F>
void Tree< I>::apply( F&& f){
  f( *self);
  for( auto& child: children){
    child->apply( f);
  }
}
  
template< class I>
inline
auto Tree<I>::first_hint() const -> Hint{
  Hint hint{};
  hint.data = this;
  return hint;
}

/*
inline
int srep( int* t){
  if (t == nullptr){
    return -1;
  }
  else{
    return *t;
  }
}
*/

template< class I>
void Tree<I>::check_consistency_rec( const Tree<I>* top) const{

  const std::string head( "sfield_ptr_tree consistency check: ");

  if (self == nullptr){
    if (this != top){
      auto msg =  head + "non top has null self";
      I::error( msg);
    }
  }
  else{
    for( auto& ptr: children){
      const Tree<I>& child = *ptr;
      if (!I::encloses( *self, *(child.self))){
        auto msg = head + "child not contained by parent";
        I::error( msg);
      }
    }
  }
  
  for( auto& iptr: children){
    const Tree<I>& child = *iptr;
    for( auto& jptr: children){
      if (jptr != iptr){
        const Tree<I>& child2 = *jptr;
        if (I::encloses( *(child.self), *(child2.self))){ 
          auto msg = head + "one is contained by other on same level";
          I::error( msg);
        }
      }
    }
  }

  for( auto& ptr: children){
    const Tree<I>& child = *ptr;
    if (child.parent != this){
      auto msg = head + "parent connectivity is wrong";
      I::error( msg);
    }
  }

  for( auto& ptr: children){
    const Tree<I>& child = *ptr;
    child.check_consistency_rec( top);
  }

}

// call on top only
template< class I>
void Tree<I>::check_consistency() const{
  check_consistency_rec( this);
}

template< class I>
void Tree<I>::check_for_loops() const{
  for( auto& ptr: children){
    if (ptr.use_count() > 1){
      error( "count = ", ptr.use_count());
    }
    ptr->check_for_loops();
  }
}

  // constructor
template< class I>
Tree<I>::Tree(){
  self = nullptr;
  parent = nullptr;
}

//##########################################################
template< class I>
void Tree<I>::insert( shared_ptr<T> item){

  if (!self || I::encloses( *self, *item)){
    bool encloses_children{ false};
    bool encloses_all{ true};

    for( auto& ptr: children){
      Tree< I>& child = *ptr;
      const T& child_item = *(child.self);
      if (I::overlap( *(child.self), *item) && 
          !(I::encloses( child_item, *item) || I::encloses( *item, child_item))){
        I::error( "sfield_ptr_tree: should not have overlap");
      }
    }

    for( auto& ptr: children){
      Tree< I>& child = *ptr;
      if (I::encloses( *(child.self), *item)){      
        child.insert( item);
        return;
      }
      else if (I::encloses( *item, *(child.self))){
        encloses_children = true;       
      }
      else{
        encloses_all = false;
      }
    }

    if (!self && encloses_all){
      self = item;
      return;
    }

    else{
      auto new_tree = make_shared< Tree<I> >();
      new_tree->self = item;
      new_tree->parent = this;

      if (encloses_children){
        std::list< std::shared_ptr< Tree<I> > > grandchildren{};
        
        for( auto& ptr: children){
          Tree< I>& grandchild = *ptr;
          if (I::encloses( *item, *(grandchild.self))){
            grandchild.parent = &(*new_tree);
            grandchildren.push_back( ptr);
          }     
        }
        
        new_tree->children = grandchildren;
        for( auto& ptr: grandchildren){
          children.remove( ptr);
        }
        
      } 
      children.push_back( new_tree);
    }
  }

  else if (I::encloses( *item, *self)) {
    auto new_tree = make_shared< Tree<I> >();    
    new_tree->self = self;
    new_tree->parent = this;
    self = item;
    for( auto& child_ptr: children){
      new_tree->children.push_back( child_ptr);
      child_ptr->parent = &(*new_tree);
    }

    children.clear();
    children.push_back( new_tree);
  }

  else{
    if (I::overlap( *self, *item)){
      I::error( "sfield_ptr_tree: should not have overlap");
    }

    auto new_tree = make_shared< Tree<I> >();
    new_tree->self = self;
    for( auto& child_ptr: children){
      new_tree->children.push_back( child_ptr);
      child_ptr->parent = &(*new_tree);
    }
    new_tree->parent = this;

    self.reset(); // perhaps not needed
    children.clear();

    auto new_tree2 = make_shared< Tree<I> >();
    new_tree2->self = item;
    new_tree2->parent = this;
    children.push_back( new_tree);
    children.push_back( new_tree2);
  }
}

//##############################################################
template< class I> template< class Info>
auto Tree<I>::lowest_found( Info& info, const Point& pt, Hint& hint) const -> T*{

  typedef Tree<I> Tr;
  const Tr* current = hint.data;
  
  if (debug)
    if (current == nullptr)
      I::error( "lowest_found: no hint");

  while ( !(current == nullptr || I::is_inside( info, pt, *(current->self)))) {
    current = current->parent;
  }
  if (current == nullptr)
    return nullptr;

  else {
    while(true){
      if (current->children.empty()){
        hint.data = current;
        return current->self.get();   
      }
      else{
        bool no_child_contains{ true};
        auto& lchildren = current->children;
        for( auto& child_ptr: lchildren){
          if (I::is_inside( info, pt, *(child_ptr->self))){
            current = &(*child_ptr);
            no_child_contains = false;
            break;
          }
        }
        if (no_child_contains){
          hint.data = current;
          return current->self.get(); 
        }
      }
    }
  }
}
}

