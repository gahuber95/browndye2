#include "../lib/vector.hh"
#include "../lib/array.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
#include "../input_output/get_args.hh"

using namespace Browndye;

typedef Array< double, 3> V3;
namespace JP = JAM_XML_Pull_Parser;

struct EIndex{
  double value;
  double dt;
};

//###############################################
Vector< V3> chain_poses( JP::Node_Ptr cnode){
  
  auto xyz = vector_from_node< double>( cnode);
  auto n3 = xyz.size();
  auto n = n3/3;
  Vector< V3> res;
  res.reserve( n);
  for( auto i: range(n)){
    auto i3 = 3*i;
    auto x = xyz[i3+0];
    auto y = xyz[i3+1];
    auto z = xyz[i3+2];
    V3 pos{ x,y,z};
    res.push_back( pos);
  }
  return res;
}

//##################################################################
double eindex( const Vector< V3>& poses0, const Vector< V3>& poses1){
  
  auto n0 = poses0.size();
  auto n1 = poses1.size();
  
  double sum = 0.0;
  for( auto i0: range( n0)){
    double closest = large;
    auto pos0 = poses0[i0];
    for( auto i1: range( n1)){
      auto pos1 = poses1[i1];
      auto d = distance2( pos0, pos1);
      closest = std::min( closest, d);
    }
    sum += closest;
  }
  
  return sqrt( sum/n0);
}

//################################################
EIndex eindex_and_dt( JP::Node_Ptr snode){
  
  auto cnodes = snode->children_of_tag( "chain");
  auto itr = cnodes.begin();
  auto cnode0 = *itr;
  ++itr;
  auto cnode1 = *itr;
  
  auto poses0 = chain_poses( cnode0);
  auto poses1 = chain_poses( cnode1);
  
  auto ei0 = eindex( poses0, poses1);
  auto ei1 = eindex( poses1, poses0);
  auto ei = 0.5*(ei0 + ei1);
  
  auto dt = checked_value_from_node< double>( snode, "dt");
  
  EIndex eidt;
  eidt.dt = dt;
  eidt.value = ei;
  return eidt;
}

//###############################################
Vector< EIndex> ent_indices( JP::Parser& parser){
  
  Vector< EIndex> res;
  
  auto f = [&]( JP::Node_Ptr snode){
    auto eidt = eindex_and_dt( snode);
    res.push_back( eidt);
  };
  
  auto top = parser.top();
  parser.apply_to_nodes_of_tag( top, "state", f);
  return res;
}

//###################################################
// input is output of process_trajectory
void main0( int argc, char* argv[]){
  
  const std::string msg = "inputs trajectory file (output of"
  " process_trajectories) through standard input and outputs"
  " entanglement index to standard output";
  
  print_help( argc, argv, msg);
  
  if (argc > 1){
    error( "no arguments");
  }
  
  JP::Parser parser( std::cin);
  auto eis = ent_indices( parser);
  double t = 0.0;
  for( auto ei: eis){
    std::cout << t << ' ' << ei.value << std::endl;
    t += ei.dt;
  }
}

//########################################
int main( int argc, char* argv[]){
  
  try{
    main0( argc, argv);
  }
  catch( const Jam_Exception& exc){
    std::cerr << exc.what() << "\n";
    return 1;
  }
  
  return 0;
}
