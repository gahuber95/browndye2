#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>
#include "../global/error_msg.hh"
#include "../input_output/get_args.hh"
#include "../xml/node_info.hh"

namespace {
using namespace Browndye;    
using std::cout;
using std::setw;  
namespace JP = JAM_XML_Pull_Parser;
    
      
//##############################################################    
void output_atoms_with_areas( const std::string& mname,
                             const Vector< double>& areas){
  
  size_t ia = 0;
  
  cout << "<top>\n";
  
  auto process = [&]( JP::Node_Ptr rnode){
    
    rnode->complete();
    auto rname = checked_value_from_node< std::string>( rnode, "name");
    auto rnumber = checked_value_from_node< int>( rnode, "number");
    
    cout << "  <residue>\n";
    cout << "    <name> " << rname << " </name>\n";
    cout << "    <number> " << rnumber << " </number>\n";
    
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      auto fval = [&]( const std::string& tag){
        return checked_value_from_node< double>( anode, tag);
      };
      
      auto x = fval( "x");
      auto y = fval( "y");
      auto z = fval( "z");
      auto charge = fval( "charge");
      auto radius = fval( "radius");
      auto aname = checked_value_from_node< std::string>( anode, "name");
      auto anumber = checked_value_from_node< double>( anode, "number");
      auto area = areas[ia];
      cout << "    <atom>\n";
      cout << "      <name> " << aname << " </name>\n";
      cout << "      <number> " << anumber << " </number>\n";
      cout << "      <x> " << x << " </x>\n";
      cout << "      <y> " << y << " </y>\n";
      cout << "      <z> " << z << " </z>\n";
      cout << "      <charge> " << charge << " </charge>\n";
      cout << "      <radius> " << radius << " </radius>\n";
      cout << "      <sasa> " << area << " </sasa>\n";
      cout << "    </atom>\n";
      ++ia;
    }
    cout << "  </residue>\n";

  };
  
  std::ifstream input( mname);
  JP::Parser parser( input, mname);
  auto top = parser.top();
  parser.apply_to_nodes_of_tag( top, "residue", process);
  
  cout << "</top>\n";
}
    
//###########################################################
Vector< double> areas_from_apbs_file( std::ifstream& afile){

  Vector< double> res;

  char buffer[1000];
  while( !afile.eof()){
    afile.getline( buffer, 1000);
    std::string line( buffer);
    
    const std::string front( "  SASA for atom");
    auto nf = front.size();
    auto tfront = line.substr( 0, nf);
    if (tfront == front){
      
      char junk[3][100];
      int i;
      double area;
      
      sscanf( line.c_str(), "%s %s %s %d: %lf",
            junk[0], junk[1], junk[2], &i, &area);
      
      res.push_back( area);
    }    
  }
  return res;
}

//###############################################
struct File_Holder{
  FILE* fp;
  std::string name;
  
  ~File_Holder(){
    unlink( name.c_str());
  }
};

//##################################################
void main0( int argc, char *argv[]){

  if (argc != 2){
    error( "wrong number of args");
  }
  
  std::string help_msg = "takes xml file (output of pqr2xml) as only argument and outputs"
  " solvent-assessible surface areas.  Uses solvent radius of 1.4 A";
  print_help( argc, argv, help_msg);
  
  std::string code("read\n"
"    mol xml \n"
"end\n"
" \n"
"apolar name anp\n"
"    bconc 0.0\n"
"    grid 0.0 0.0 0.0\n"
"    dpos 0.0\n"
"    press 0.0\n"
"    swin 0.3\n"
"    gamma 1.0\n"
"    srad 1.4\n"
"    sdens 20.0\n"
"    srfm sacc\n"
"    temp 298\n"
"    mol 1\n"
"    calcenergy comps\n"
"    calcforce no\n"
"end\n"
"quit\n");
  
  // srad = 1.4
  std::string pqr_name = argv[1];
  code.insert( 17, pqr_name);
  
  auto tmp_file = [&]{
    char cfname[]{ "bdapbsXXXXXX"};
    auto idesc = mkstemp( cfname);
    if (idesc == -1){
      error( "get_areas: file could not be made");
    }
    
    auto fp = fdopen( idesc, "w");
    File_Holder fh;
    fh.fp = fp;
    fh.name = std::string( cfname);
    return fh;
  };
  
  auto com_hd = tmp_file();
  auto com_file = com_hd.fp;
  auto& com_name = com_hd.name;
  fprintf( com_file, "%s", code.c_str());
  fclose( com_file);
    
  auto ares_hd = tmp_file();
  auto ares_file = ares_hd.fp;
  auto& ares_name = ares_hd.name;
  fclose( ares_file); 
  
  auto command = std::string( "apbs ") + com_name + " > " + ares_name;
  auto irs = std::system( command.c_str());
  if (irs != 0){
    error( "apbs command did not work");
  }
  
  std::ifstream afile( ares_name);  
  auto areas = areas_from_apbs_file( afile);
  output_atoms_with_areas( pqr_name, areas);
}

}

//################################
int main( int argc, char *argv[]){
  main0( argc, argv);
}
