#pragma once

#include "../../lib/units.hh"
#include "../../lib/array.hh"
#include "../../transforms/transform.hh"
#include "../../forces/electrostatic/field.hh"
#include "../../forces/electrostatic/born_field_interface.hh"
#include "../../forces/electrostatic/v_field_interface.hh"
#include "../../structures/atom.hh"
#include "../../generic_algs/charged_blob_simple.hh"
#include "../../forces/electrostatic/field_for_blob.hh"
#include "../../forces/electrostatic/blob_interface.hh"
#include "../chain/chain_state.hh"
#include "core_common.hh"
#include "../../lib/class_selector.hh"
#include "../../tet/tet.hh"
#include "../chain/chain_state_atom.hh"
#include "core_thread.hh"

namespace Browndye{

class Group_State;
class Group_Common;

// important states
// whether force and torque are up to date w.r.t. transform
// whether grid hints are up to date w.r.t. transform ?
class Core_State{
public:
  Core_State() = delete;
  Core_State( const Core_State&);
  Core_State& operator=( const Core_State&);
  Core_State( Core_State&&) = default;
  Core_State& operator=( Core_State&&)  noexcept = default;

  Core_State( Core_Thread&, const Core_Common&, Group_State& group,
              const Vector< Group_Common, Group_Index>&, bool for_mob = false);

  void set_to_start(const Vector<Group_Common, Group_Index> &);

  [[nodiscard]] Core_Index number() const;
  [[nodiscard]] Group_Index group_number() const;
  [[nodiscard]] const Core_Common& common() const;
  Core_Thread& thread();
  [[nodiscard]] const Core_Thread& thread() const;
  void rethread( Core_Thread&);

  template< bool is_mobile>
  auto& collision_structure();
 
  [[nodiscard]] const std::shared_ptr< Field::Field< V_Field_Interface> >& v_field() const;
  [[nodiscard]] const std::shared_ptr< Field::Field< Born_Field_Interface> >&  born_field() const;
  
  typedef Core_Common::Q_Blob_Interface Q_Blob_Interface;
  [[nodiscard]] const std::shared_ptr< Charged_Blob::Blob< 4, Q_Blob_Interface> >& q_blob() const;
 
  typedef Core_Common::Q2_Blob_Interface Q2_Blob_Interface;
  [[nodiscard]] const std::shared_ptr< Charged_Blob::Blob< 4, Q2_Blob_Interface> >& q2_blob() const;
 
  [[nodiscard]] const Vector< Atom, Atom_Index>& atoms() const;
  //[[nodiscard]] Length hydro_radius() const;
  
  void update_constrained_atoms();
  
  // Data 
  Vec3< Force> force;
  Vec3< Torque> torque;
  Vector< Pos, Ind_Atom_Index> constrained_poses; // need to set up, update from tet

  typedef Field::Field< V_Field_Interface>::Hint VHint;
  typedef Field::Field< Born_Field_Interface>::Hint BHint;

  Vector< Vector< VHint, Core_Index>, Group_Index> v_hints;
  Vector< Vector< BHint, Core_Index>, Group_Index> born_hints;

  std::optional< Tet_Index> itet;

  // other functions
  template< bool is_mobile0, bool is_mobile1>
  friend
  auto collision_structures( Core_State& state0, Core_State& state1);
  
  [[nodiscard]] const Transform& transform() const;
  [[nodiscard]] Pos transformed( Pos) const;
  [[nodiscard]] Pos translation() const;
  [[nodiscard]] Mat3< double> rotation() const;
  
  void transform_transform( const Transform&);

  void set_transform( const Transform&);
 
private:
  //friend class Tet< Group_State>;
  
  Transform _transform;
  std::reference_wrapper< const Group_Common> group_ref;
  std::reference_wrapper< Core_Thread> thread_ref;
  std::reference_wrapper< const Core_Common> common_ref;
  
  void reset_hints( const Vector< Group_Common, Group_Index>&);
  void copy_from( const Core_State&);
};

// for saving during a time step
struct Small_Core{
  Vector< Pos, Ind_Atom_Index> constrained_poses;
  F3 force;
  T3 torque;
};

//##############################################################
inline
void Core_State::transform_transform( const Transform& tform){
  
  _transform = tform*_transform;
}

inline
const Transform& Core_State::transform() const{
  return _transform;
}

inline
const Core_Common& Core_State::common() const{
 return common_ref.get();
}

inline
const Core_Thread& Core_State::thread() const{
  return thread_ref.get();
}

inline
Core_Thread& Core_State::thread(){
  return thread_ref.get();
}

inline
void Core_State::rethread( Core_Thread& cthread){
  thread_ref = cthread;
}

template< bool is_mobile>
auto& Core_State::collision_structure(){
  
  return choice< is_mobile>( *(thread().collision_structure),
                              *(common().collision_structure));
}

inline
const std::shared_ptr< Field::Field< V_Field_Interface> >& Core_State::v_field() const{
  return common().v_field;  
}

inline
const std::shared_ptr< Field::Field< Born_Field_Interface> >& Core_State::born_field() const{
  return common().born_field;  
}

inline
auto Core_State::q_blob() const -> const std::shared_ptr< Charged_Blob::Blob< 4, Q_Blob_Interface> >& {
  return common().q_blob;
}

inline
auto Core_State::q2_blob() const -> const std::shared_ptr< Charged_Blob::Blob< 4, Q2_Blob_Interface> >& {
  return common().q2_blob;
}

inline
Core_Index Core_State::number() const{
  return common().number;
}

inline
const Vector< Atom, Atom_Index>& Core_State::atoms() const{
  return common().atoms;  
}

/*
inline
Length Core_State::hydro_radius() const{
  return common().h_radius;
}
*/

inline
void Core_State::set_transform( const Transform& tform){
  _transform = tform;
}

inline
Pos Core_State::transformed( Pos pos) const{
  return _transform.transformed( pos);
}

inline
Pos Core_State::translation() const{
  return _transform.translation();
}

inline
Mat3< double> Core_State::rotation() const{
  return _transform.rotation();
}

template< bool is_mobile0, bool is_mobile1>
auto collision_structures( Core_State& state0, Core_State& state1){
  
  auto& common0 = state0.common();
  auto& common1 = state1.common();
  auto& thread0 = state0.thread();
  auto& thread1 = state1.thread();
  
  auto mob_col0 = std::ref( *(thread0.collision_structure));
  auto mob_col1 = std::ref( *(thread1.collision_structure));
  auto sta_col0 = std::ref( *(common0.collision_structure));
  auto sta_col1 = std::ref( *(common1.collision_structure));
  //auto alt_col0 = std::ref( *(thread0.collision_structure_alt));
  
  return choice< is_mobile0>(
      choice< is_mobile1>(
         //common0.are_same( common1) ? std::make_pair( mob_col0, alt_col0) : std::make_pair( mob_col0, mob_col1),
         std::make_pair( mob_col0, mob_col1),
         std::make_pair( mob_col0, sta_col1)                            
      ),
      std::make_pair( sta_col0, mob_col1)
  );
}

}
