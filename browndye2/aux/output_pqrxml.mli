(* 
Used internally. Outputs pqrxml file from information in a 
container of charged spheres (atoms).  The CHARGED_SPHERE
module type has functions for extracting pqrxml information
and for advancing along the container. The function "f"
is called to do the actual outputting.

*)

module type CHARGED_SPHERE =
  sig
    type t
    type container
    type index
    val atom_name: t -> string
    val residue_name: t -> string
    val atom_number: t -> int
    val residue_number: t -> int
    val position: t -> Vec3.t
    val charge: t -> float
    val radius: t -> float
    val first_index: container -> index
    val next: container -> index -> (t option)*index 
  end


module M:
  functor (CS : CHARGED_SPHERE) ->
    sig
      val f : CS.container -> out_channel -> unit
    end


(* 
Does not use the module interface. The "info" function returns a tuple
with the following items:
residue number, residue name, atom number, atom name, position, charge, radius 
*)
val sphere_array: spheres: 'a array -> info: ('a -> int*string*int*string*Vec3.t*float*float) -> channel: out_channel -> unit
