/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that gets called for nam_simulation

#include "../input_output/release_info.hh"
#include "staged_simulator.hh"
#include "simulation.hh"

namespace Browndye{

int main( int argc, char* argv[]){

  if (argc < 2){
    error( "staged_simulation: need input file name\n");
  }

  std::string file( argv[1]);

  print_release_info();
 
  try{
    Simulation simulation( file);
    Staged_Simulator stas( simulation.top_node);
    stas.run();
  }
  
  catch( const Jam_Exception& exc){
   std::cerr << exc.what() << "\n";
   return 1;
  }
  
  return 0;
}
}
//##################################
int main( int argc, char* argv[]){
  return Browndye::main( argc, argv);
}

//#include <gperftools/profiler.h> and surround the sections you want to profile with ProfilerStart("nameOfProfile.log"); and ProfilerStop();
