#include <sstream>
#include "split.hh"

namespace{
  using std::string;
}

namespace Browndye{

Vector< string> split( const string& str, char sp){

  string new_str = str;
  for (auto i = 0u; i < new_str.size(); ++i){
    if (new_str[i] == sp){
      new_str[i] = ' ';
    }
  }

  std::istringstream iss( new_str);
  Vector< string> res;
  while (!iss.eof()){
    string s;
    iss >> s;
    res.push_back(s);
  }
  return res;
}

string trimmed( const string& str){
  size_t ib = 0;
  while (isspace( str[ib])){
    ++ib;
  }
  size_t ie = str.size() - 1;
  while (isspace( str[ie])){
    --ie;
  }
  return str.substr( ib, ie - ib + 1);
}

}
