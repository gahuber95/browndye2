(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Outputs rate constants and 95% confidence intervals. Input is results file from "nam_simulation". Uses kdb_calculator for actual calculation.
*)


let pi = 3.1415926;;

module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;

Arg.parse 
  []
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Outputs rate constants and confidence intervals. Input is results file from \"nam_simulation\""
;;

let rparser = JP.new_parser (Scanf.Scanning.stdin);;
JP.complete_current_node rparser;;
let rnode = JP.current_node rparser;;

let snode = NI.checked_child rnode "solvent";;
let mnode = NI.checked_child rnode "molecule_info";;

let rxnode = NI.checked_child rnode "reactions" ;;
let ntrajs = NI.int_of_child rxnode "n_trajectories";;
let cnodes = Array.of_list (JP.children rxnode "completed");;
let nrxns = Array.length cnodes;;

(* let kdb = Kdb_calculator.f snode mnode;; *)
let kdb = NI.float_of_child mnode "b_reaction_rate";;

let conv_factor = 602000000.0;; (* 1/M s, make it an option later *)

let pr = Printf.printf;;

pr "<rates>\n";
pr "  <!-- 95%% confidence intervals -->\n";

for i = 0 to nrxns-1 do
  let cnode = cnodes.(i) in
  let ns = NI.int_of_child cnode "n"
  and name = NI.string_of_child cnode "name" in

  let beta = (float ns)/.(float ntrajs) in
  let rate beta = 
    conv_factor*.kdb*.beta 
  in
  let sdev = sqrt ( beta*.(1.0 -. beta)/.(float ntrajs)) in
  let high_beta = beta +. 2.0*.sdev
  and low_beta = beta -. 2.0*.sdev
  in

    pr "  <rate>\n";
    pr "    <name> %s </name>\n" name;
    pr "    <rate_constant>\n";
    pr "      <low> %g </low>\n" (rate low_beta);
    pr "      <mean> %g </mean>\n" (rate beta);
    pr "      <high> %g </high>\n" (rate high_beta);
    pr "    </rate_constant>\n";
    pr "\n";
    pr "    <reaction_probability>\n";
    pr "      <low> %g </low>\n" low_beta;
    pr "      <mean> %g </mean>\n" beta;
    pr "      <high> %g </high>\n" high_beta;
    pr "    </reaction_probability>\n";
    pr "  </rate>\n";
done
;;

pr "</rates>\n";


  



 
