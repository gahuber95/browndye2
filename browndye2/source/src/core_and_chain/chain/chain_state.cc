/*
 * chain_state.cc
 *
 *  Created on: Jul 19, 2016
 *      Author: root
 */


#include "../../lib/range.hh"
#include "../../hydrodynamics/rotne_prager.hh"
#include "../../lib/cholesky.hh"
#include "chain_state.hh"
#include "../reset_hints.hh"

namespace Browndye{

const Atom_Index maxats{ maxi};

// constructor
Chain_State::Chain_State( const Chain_Common& _chain, const Vector< Group_Common, Group_Index>& gcommons):
  chain_ref( _chain){  
  reset_to_start( gcommons);
}

void Chain_State::reset_to_start( const Vector< Group_Common, Group_Index>& gcommons){

  auto& catoms = common().atoms;
  auto n = catoms.size();
  atoms.resize( n);
  for( auto i: range( n)){
    auto& satom = atoms[i];
    satom.pos = catoms[i].pos;
    satom.force = Vec3< Force>( Zeroi());
  }

  reset_hints( gcommons);
}

void Chain_State::reset_hints( const Vector< Group_Common, Group_Index>& gcommons){
  Browndye::reset_hints( *this, gcommons);
}

Pos Chain_State::hydro_center() const{
    
  Vec3< Length2> wc(Zeroi{});
  Length rsum{ 0.0};
  auto& latoms = common().atoms;
  for( auto i: range( latoms.size())){
    auto r = latoms[i].radius;
    wc += r*latoms[i].pos;
    rsum += r;
  }
  return wc/rsum;    
}

void Chain_State::shift( Pos d){
  for( auto& atom: atoms)
    atom.pos += d;
}

// copy constructor
Chain_State::Chain_State( const Chain_State& other):
  chain_ref( other.chain_ref){

  copy_from( other);  
}
  
Chain_State& Chain_State::operator=( const Chain_State& other){
  
  chain_ref = other.chain_ref;
  copy_from( other);
  return *this;
}

void Chain_State::copy_from( const Chain_State& other){
  
  atoms = other.atoms.copy();
  frozen = other.frozen;
  v_hints = other.v_hints.copy();
  born_hints = other.born_hints.copy();
}

//##################################################################
// constructor
Chain_State::Chain_Collision_Info::Chain_Collision_Info( Chain_State& cstate):
  container( cstate, cstate.chain_ref.get().atoms, cstate.atoms, cstate.chain_ref.get().number, cstate.chain_ref.get().ig),
  col_struct( container, maxats){}

  // constructor
Chain_State::Chain_Collision_Info::Chain_Collision_Info( Chain_Collision_Info&& other) noexcept:
  container( std::move( other.container)), col_struct( container, maxats){}


// just intrachain
/*
Length Chain_State::rms_constraint_error() const{
  
  Length2 sum{ 0.0};
  auto& cons = common().length_constraints;
  for( auto& con: cons){
    auto i0 = con.atomis[0];
    auto i1 = con.atomis[1];
    auto& atom0 = atoms[i0];
    auto& atom1 = atoms[i1];
    auto pos0 = atom0.pos;
    auto pos1 = atom1.pos;
    auto r = distance( pos0, pos1);
    auto d = con.length;
    sum += sq( r - d);
  }
  auto nc = cons.size();
  double fnc = nc/Constraint_Index(1);
  return sqrt( sum/fnc);
}
*/

}

/*
diff( c, th, 2);
                                                  2  2    2
                 a b cos(th)                     a  b  sin (th)
(%o3)  ------------------------------- - ------------------------------
                               2    2                        2    2 3/2
       sqrt(- 2 a b cos(th) + b  + a )   (- 2 a b cos(th) + b  + a )
       
(%i4) ratsimp(%);

*/

/*
int main(){
  Chain chain{};
  chain.atoms.resize( Atom_Index(8));

  auto& atoms = chain.atoms;
  for( auto& atom: atoms){
    atom.interac_radius = Length( 1.0);
    atom.radius = Length( 1.0);
  }

  std::default_random_engine gen{};
  std::normal_distribution< double> gauss{};

  const double scale = 2.0;

  for( auto ix: range(2))
    for( auto iy: range(2))
      for( auto iz: range(2))
        atoms[ Atom_Index( 4*ix + 2*iy + iz)].pos =
            Pos{ scale*Length( 1.0 - 2*ix + 0.001*gauss(gen)),
                 scale*Length( 1.0 - 2*iy + 0.001*gauss(gen)),
                 scale*Length( 1.0 - 2*iz + 0.001*gauss(gen))};

  Chain_State state( chain);

  auto factor = state.radius_mobility_factor( chain);
  std::out << factor << "\n";
}
*/
