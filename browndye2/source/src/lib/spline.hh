#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Spline is a generic implementation of the cubic spline.
*/


//#include "units.hh"
#include "vector.hh"
#include "found.hh"

namespace Browndye{

// xref, yref are true if the spline holds just a reference to the Vector.
// later add support for different indices and possibly different containers
template< class X, class Y, bool yref>
class Spline_Base{
public:
  typedef decltype( Y()/X()) dYdX;
  typedef decltype( X()*X()) X2;
  typedef decltype( Y()/X2()) d2YdX2;

  Spline_Base() = delete;
  Spline_Base( const Spline_Base&) = default;
  Spline_Base( Spline_Base&&) = default;
  Spline_Base& operator=( const Spline_Base&) = default;
  Spline_Base& operator=( Spline_Base&&) = default;

  // later, add one for moved-from vectors?
  Spline_Base( const Vector< Y>& ys_in);
  
  template< class Xs>
  Y value( size_t n, size_t i, const Xs& xs, X x) const;
  
  template< class Xs>
  dYdX first_deriv( size_t n, size_t i, const Xs& xs, X x) const;
  
  template< class Xs>
  d2YdX2 second_deriv( size_t n, size_t i, const Xs& xs, X x) const;

  size_t size() const{
    return y2s.size();
  }

protected:
  template< class T, bool is_ref>
  class Real_Or_Ref;

  template< class T>
  class Real_Or_Ref< T, false>{
  public:
    typedef Vector<T> Res;
    
    static
    Vector<T> init( const Vector<T>& t){
      return t.copy();
    }
  };

  template< class T>
  class Real_Or_Ref< T, true>{
  public:
    typedef const Vector<T>& Res;
    
    static
    const Vector<T>& init( const Vector<T>& t){
      return t;
    }
  };

  typename Real_Or_Ref< Y, yref>::Res ys;
  Vector< d2YdX2> y2s;

  static void tri_diag_solve( const Vector< X>& a, 
                              const Vector< X>& b, 
                              Vector< X>& c, 
                              Vector< dYdX>& d, 
                              Vector< d2YdX2>& x);

  void initialize( const Vector< X>& xs);
};


/*******************************************************************/
template< class X, class Y, bool yref> 
void Spline_Base< X, Y, yref>::tri_diag_solve( const Vector< X>& a, 
                             const Vector< X>& b, 
                             Vector< X>& c, 
                             Vector< dYdX>& d, 
                             Vector< d2YdX2>& x){

  const size_t n = x.size();
  const X X1(1.0);

  c[0] = X(1)*c[0]/b[0];
  d[0] = X(1)*(d[0]/b[0]);
  double id;
  for(size_t i = 1; i != n; i++){
    id = X1*( 1.0/(b[i] - c[i-1]*a[i]/X1));
    c[i] = c[i]*id;                 
    d[i] = (d[i] - (a[i]*d[i-1]/X1))*id;
  }
  
  x[n-1] = d[n-1]/X1;
  for(int i = n-2; i != -1; i--)
    x[i] = (d[i] - c[i]*x[i+1])/X1;
}

/*******************************************************************/
template< class X, class Y, bool yref>
void Spline_Base< X,Y,yref>::initialize( const Vector< X>& xs){

  const auto n = xs.size() < ys.size() ? xs.size() : ys.size(); 
  y2s.resize( n);

  Vector< X> a( n);
  for (size_t i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      a[i] = X(0.0);
    else
      a[i] = (xs[i] - xs[i-1])/6.0; 
  }

  Vector< X> b( n);
  for (size_t i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      b[i] = X(1.0);
    else
      b[i] = (xs[i+1] - xs[i-1])/3.0; 
  }

  Vector< X> c( n);
  for (size_t i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      c[i] = X( 0.0);
    else
      c[i] = (xs[i+1] - xs[i])/6.0; 
  }

  Vector< dYdX> d( n);
  for (size_t i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      d[i] = dYdX( 0.0);
    else
      d[i] = 
        (ys[i+1] - ys[i])/(xs[i+1] - xs[i]) - 
        (ys[i] - ys[i-1])/(xs[i] - xs[i-1]);
  }

  tri_diag_solve( a,b,c,d,y2s);
}

/**************************************************************/
// constructor
template< class X, class Y, bool yref>
Spline_Base< X,Y,yref>::Spline_Base( const Vector< Y>& _ys):
  ys( Real_Or_Ref< Y, yref>::init(_ys)){}

/**************************************************************/
template< class X, class Y, bool yref>
template< class Xs>
Y Spline_Base< X,Y,yref>::value( size_t n, size_t i, const Xs& xs, X x) const{
  
  if (i == (n-1))
    return ys[n-1];
  else{
    const X xp = xs(i+1);
    const X xm = xs(i);
    const X delx = xp - xm;
    const double a = (xp - x)/delx;
    const double b = 1.0 - a;
    const X2 c = (a*a*a - a)*delx*delx/6.0;
    const X2 d = (b*b*b - b)*delx*delx/6.0;
    return a*ys[i] + b*ys[i+1] + c*y2s[i] + d*y2s[i+1];
  }
}

/**************************************************************/
template< class X, class Y, bool yref>
template< class Xs>
auto
Spline_Base< X,Y,yref>::first_deriv( size_t n, size_t i, const Xs& xs, X x) const -> dYdX{ 
  
  if (i == (n-1)){
    const X xp = xs(i);
    const X xm = xs(i-1);
    const X delx = xp - xm;
    return (ys[i] - ys[i-1])/delx + delx*y2s[i-1]/6.0 + delx*y2s[i]/3.0;
  }
  else{
    const X xp = xs(i+1);
    const X xm = xs(i);
    const X delx = xp - xm;
    const double a = (xp - x)/delx;
    const double b = 1.0 - a;
    return
      (ys[i+1] - ys[i])/delx - 
      (3.0*a*a - 1.0)*delx*y2s[i]/6.0 + 
      (3.0*b*b - 1.0)*delx*y2s[i+1]/6.0;
  }
}

/**************************************************************/
template< class X, class Y, bool yref>
template< class Xs>
auto
Spline_Base< X,Y,yref>::second_deriv( size_t n, size_t i, const Xs& xs, X x) const -> d2YdX2{ 
  
  if (i == (n-1)){
    return y2s[i];
  }
  else{
    const X xp = xs(i+1);
    const X xm = xs(i);
    const X delx = xp - xm;
    const double a = (xp - x)/delx;
    const double b = 1.0 - a;
    return a*y2s[i] + b*y2s[i+1];
  }
}


//###########################################################################
template< class X, class Y, bool xref=false, bool yref=false>
class Spline: public Spline_Base< X,Y,yref>{
public:
  typedef Spline_Base< X,Y,yref> Parent;
  typedef typename Parent::dYdX dYdX;
  typedef typename Parent::d2YdX2 d2YdX2;
  
  using Spline_Base< X,Y,yref>::Spline_Base;
  
  Spline( const Vector< X>& xs, const Vector< Y>& ys);
  
  Y value( X x) const;
  
  dYdX first_deriv( X x) const;
  d2YdX2 second_deriv( X x) const;
  
private:
  typedef typename Parent::template Real_Or_Ref< X, xref> RoR;
  typename RoR::Res xs;  
};

/**************************************************************/
// constructor
template< class X, class Y, bool xref, bool yref>
Spline< X,Y,xref,yref>::Spline( const Vector< X>& _xs, const Vector< Y>& _ys):
  Parent( _ys), xs( RoR::init(_xs)){
  
  if (xs.size() != _ys.size())
     error( "Spline: x and y must be the same size", xs.size(), _ys.size());
  
  this->initialize( xs);
} 

/**************************************************************/
template< class X, class Y, bool xref, bool yref>
auto Spline< X,Y,xref,yref>::value( X x) const -> Y{
  
  auto xsf = [&]( size_t i){
    return xs[i];
  };
  
  const size_t n = xs.size();
  const size_t i = found( xs, x);
  
  return Parent::value( n, i, xsf, x);
}

/**************************************************************/
template< class X, class Y, bool xref, bool yref>
auto Spline< X,Y,xref,yref>::first_deriv( X x) const -> dYdX{
  
  auto xsf = [&]( size_t i){
    return xs[i];
  };
  
  const size_t n = xs.size();
  const size_t i = found( xs, x);
  
  return Parent::first_deriv( n, i, xsf, x);
}

/**************************************************************/
template< class X, class Y, bool xref, bool yref>
auto Spline< X,Y,xref,yref>::second_deriv( X x) const -> d2YdX2{
  
  auto xsf = [&]( size_t i){
    return xs[i];
  };
  
  const size_t n = xs.size();
  const size_t i = found( xs, x);
  
  return Parent::second_deriv( n, i, xsf, x);
}

//###########################################################################
template< class X, class Y, bool yref=false>
class Even_Spline: public Spline_Base< X,Y,yref>{
public:
  typedef Spline_Base< X,Y,yref> Parent;
  typedef typename Parent::dYdX dYdX;
  typedef typename Parent::d2YdX2 d2YdX2;
  
  using Spline_Base< X,Y,yref>::Spline_Base;
  
  Even_Spline( X x0, X xf, const Vector< Y>& ys);
  
  Y value( X x) const;
  //size_t size() const;
  using Spline_Base< X,Y, yref>::size;

  dYdX first_deriv( X x) const;
  d2YdX2 second_deriv( X x) const;

  std::pair< X,X> bounds() const;
  
  X low_bound() const;
  X high_bound() const;
  
private:
  const size_t n;
  X x0,xf,dx;
};

template< class X, class Y, bool yref>
X Even_Spline< X,Y,yref>::low_bound() const{
  return x0;
}

template< class X, class Y, bool yref>
X Even_Spline< X,Y,yref>::high_bound() const{
  return xf;
}

//##########################################3
// constructor
template< class X, class Y, bool yref>
Even_Spline< X,Y,yref>::Even_Spline( X _x0, X _xf, const Vector< Y>& _ys):
  Parent( _ys), n(_ys.size()){
  
  x0 = _x0;
  xf = _xf;
  dx = (xf - x0)/((double)(n-1));
  Vector< X> xs( n);
  for( auto i: range( n))
    xs[i] = x0 + ((double)i)*dx;
  
  this->initialize( xs);
} 

//##########################################
template< class X, class Y, bool yref>
auto Even_Spline< X,Y,yref>::value( X x) const -> Y{
  
  auto xsf = [&]( size_t i){
    return x0 + ((double)i)*dx;
  };
  
  const size_t i = (x - x0)/dx;
  return Parent::value( n, i, xsf, x);
}

//##########################################3
template< class X, class Y, bool yref>
auto Even_Spline< X,Y,yref>::first_deriv( X x) const -> dYdX{
  
  auto xsf = [&]( size_t i){
    return x0 + ((double)i)*dx;
  };
  
  const size_t i = (x - x0)/dx;
  
  return Parent::first_deriv( n, i, xsf, x);
}

//##########################################3
template< class X, class Y, bool yref>
auto Even_Spline< X,Y,yref>::second_deriv( X x) const -> d2YdX2{
  
  auto xsf = [&]( size_t i){
    return x0 + ((double)i)*dx;
  };
   
  const size_t i = (x - x0)/dx;
  
  return Parent::second_deriv( n, i, xsf, x);
}

//##########################################3
template< class X, class Y, bool yref>
std::pair< X,X> Even_Spline< X,Y,yref>::bounds() const{
  return std::make_pair( x0, x0 + dx*(n-1));
}

//##########################################3
/*
template< class X, class Y, bool yref>
size_t Even_Spline< X,Y,yref>::size() const{
  return this->ys.size();
}
  */
}  



