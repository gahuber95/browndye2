(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* Reads in a grid file of 0's and 1's as output from "inside_points", 
and generates a grid of distances of a 0 to the nearest 1 and vice versa.  
The grid spacing is read from the same file.  

The distances are computed by allowing each boundary point to "spread" to 
neighboring points and displace farther boundary points.

*)

let pr = Printf.printf;;
let len = Array.length;;
let sq x = x*.x;;

let array_init2 nx ny f =
  Array.init nx
    (fun ix ->
      Array.init ny (f ix)
    )
;;

let array_init3 nx ny nz f =
  Array.init nx
    (fun ix -> 
      array_init2 ny nz (f ix)
    )
;;

(* 0 points are colored Black, 1 points White *)
type color = White | Black;;

type cell_type = {
  bcolor: color;
  mutable distance2: float;
  mutable nebor: int;
  mutable alive: bool;
}


let xml_format = ref true;;

Arg.parse
  [("-plain", 
   (Arg.Unit (fun () -> xml_format := false)),
   ": input data is in plain format"
   );
  ]
 (fun a -> ()
 )
  "Outputs distances of 0's from 1's. Default input is xml format as output from inside_points"
;;

let igrid_info = Igrid_parser.grid !xml_format Scanf.Scanning.stdin;;

let igrid = igrid_info.Igrid_parser.data;;
let icorner = igrid_info.Igrid_parser.corner;;
let ispacing = igrid_info.Igrid_parser.spacing;;

open Bigarray;;

let hx,hy,hz = ispacing;;
let nx,ny,nz = Array3.dim1 igrid, Array3.dim2 igrid, Array3.dim3 igrid;;

let max_d2 = 
  (sq (hx*.(float nx))) +. (sq (hy*.(float ny))) +. (sq (hz*.(float nz)));; 

let grid = 
  array_init3 nx ny nz
    (fun ix iy iz -> {
       bcolor = (
	 match igrid.{ix,iy,iz} with
	   | 0 -> Black
	   | 1 -> White
	   | _ ->(
	       Printf.fprintf stderr "value %d at %d %d %d\n" 
		 ix iy iz igrid.{ix,iy,iz};
	       raise (Failure "grid input must be 0 or 1");
	     )
       );
       nebor = -1;
       alive = false;
       distance2 = max_d2;
     }
    )
;;

let nyz = ny*ny;;

let int3to1 ix iy iz = 
  ix*nyz + iy*nz + iz
;;     
let int1to3 n = 
  let ix = n/nyz in
  let iy = (n - ix*nyz)/nz in
  let iz = n - ix*nyz - iy*nz in
    ix,iy,iz
;;

let apply_neb_fun do_neb ix iy iz = 
  if ix > 0 then
    do_neb (ix-1) iy iz;
  if ix < nx-1 then
    do_neb (ix+1) iy iz;
  
  if iy > 0 then
    do_neb ix (iy-1) iz;
  if iy < ny-1 then
    do_neb ix (iy+1) iz;
  
  if iz > 0 then
    do_neb ix iy (iz-1);
  if iz < nz-1 then
    do_neb ix iy (iz+1)
;;

let initialize () = 
  for ix = 0 to nx-1 do
    for iy = 0 to ny-1 do
      for iz = 0 to nz-1 do
	let cell = grid.(ix).(iy).(iz) in	  

	let do_neb jx jy jz = 
	  let neb_cell = grid.(jx).(jy).(jz) in
	    if not (neb_cell.bcolor = cell.bcolor) then
	      let d2 = 
		(sq (hx*.(float (ix - jx)))) +.  (sq (hy*.(float (iy - jy)))) +.  
		  (sq (hz*.(float (iz - jz))))
	      in
		if d2 < neb_cell.distance2 then (
		  neb_cell.distance2 <- d2;
		  neb_cell.nebor <- int3to1 ix iy iz;
		  neb_cell.alive <- true;
		)
	in
	  apply_neb_fun do_neb ix iy iz;
	  
      done;
    done;
  done
;;
  

(* Each grid point examines the nearest boundary points of its
neighbors. If a neighbor's boundary point is closer, than that
boundary point becomes the closest.  This is repeated until there
are no more changes of boundary points. Time required scales as n^(4/3), 
where n is the number of grid points. *) 
let loop () = 
  let change = ref false in
    for ix = 0 to nx-1 do
      for iy = 0 to ny-1 do
	for iz = 0 to nz-1 do
	  let cell = grid.(ix).(iy).(iz) in
	    if cell.alive then (
	      cell.alive <- false;
	      let icx,icy,icz = int1to3 cell.nebor in
		  	     
	      let do_neb jx jy jz = 
		let neb_cell = grid.(jx).(jy).(jz) in
		  if neb_cell.bcolor = cell.bcolor then
		    let d2 = 
		      (sq (hx*.(float (icx - jx)))) +.  (sq (hy*.(float (icy - jy)))) +.  
			(sq (hz*.(float (icz - jz))))
		    in
		      if d2 < neb_cell.distance2 then (
			neb_cell.distance2 <- d2;
			neb_cell.nebor <- int3to1 icx icy icz;
			neb_cell.alive <- true;
			change := true;
		      )
	      in
		apply_neb_fun do_neb ix iy iz;
	    )		
	done;
      done;
    done;
    !change
;;
      

initialize();;
let finis = ref false;;
while not !finis do
  finis := not (loop());
done
;;


(*************************************************)
Output_grid.f ~data: grid ~corner: icorner
  ~spacing: ispacing ~processed: (fun cell -> sqrt cell.distance2)
;;




