(* 
Parses sphere (atom) objects in XML data.

Used internally.
Applies function "f" to each atom object in XML object in "buffer".
Arguments of "f" are:
  rname - contents of "residue_name" tag
  rnumber - contents of "residue_number" tag
  aname - contents of "atom_name" tag
  anumber - contents of "atom_number" tag
  x, y, z - coordinates found in "x","y","z" tags
  radius - contents of "radius" tag
  charge - contents of "charge" tag
*)

type ftype = 
    rname:string -> rnumber:int -> 
  aname:string -> anumber:int -> 
  x:float -> y:float -> z:float -> 
  radius:float -> charge:float -> unit
    
(* applies function f to contents of input buffer buf *)
val apply_to_atoms :
  buffer: Scanf.Scanning.scanbuf -> f: ftype -> unit

(* prints out atom information in PQR format to standard output *)
val print_pqr_atom: ftype
