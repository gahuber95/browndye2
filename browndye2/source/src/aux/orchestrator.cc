#include "orchestrator.hh"

namespace Browndye{
namespace Orchestrator{

//############################
void subbed_string_test(){
  auto inputs = std::make_tuple( 11, 22.2, "abc");
  std::string command( "do $0 and then $1 and finally $2");
  auto new_command = subbed_string( command, inputs);
  std::string final_command = "do 11 and then 22.2 and finally abc";
  if (new_command != final_command)
    error( "subbed_string_test failed:", final_command, new_command);
}

//######################################
// constructor
XML_File_Info::XML_File_Info(){
  input.open( oname());
  parser = std::make_unique< JP::Parser>( input, oname());
  top = parser->top();
  top->complete();
}

//#########################################################################
string subbed_string( const string& command0, const Vector< string>& input){
  
  auto command = command0;
  auto n = input.size();
  for( auto i: range( n)){
    command = subbed_string_inner( i, command, input[i]);
  }
  return command;
}

//####################################################################
void run_command( const string& command, const string& output_file){
      
  std::cout << "command " << command << "\n";
  auto ret = std::system( command.c_str());
  auto res = (WEXITSTATUS( ret) == 0);
  if (!res){
    remove_file( output_file);
    error( command, "failed");
  }
}

//###########################################################
File_Time most_recent_time( const Vector< Source>& inputs){
  
  if (inputs.empty()){
    error( "most_recent_time: no inputs");  
  }
  
  if (inputs.size() == 1){
    return inputs[0].time();
  }
  else{
    auto ftimes = inputs.mapped( [&]( const Source& input){
      return input.time();
    });
    
    return *std::max_element( ftimes.begin(), ftimes.end());
  }
}

//#############################################################
void updatet( Vector< Source>& inputs){
  for( auto& source: inputs)
    source.update();
}

Vector< string> results( const Vector< Source>& inputs){
  return inputs.mapped( [](const Source& input){ return input.result();});
}

Source_From_File
new_source_from_file( const string& output_file, const string& command,
                       Vector< Source>& inputs){
  
 return Source_From_File( output_file, command, inputs); 
}

Source_From_File
new_source_from_file( const string& output_file, const string& command,
                     std::initializer_list< Source> inputs){
  
  auto vinputs = vector_from_ilist( inputs);
  return new_source_from_file( output_file, command, vinputs);
}

Source_From_File
new_source_from_file( const string& output_file){
  return Source_From_File( output_file);
}

// constructor
Source_From_File::Source_From_File( const string& out_file,
                                           const string& command,
                                           Vector< Source>& inputs){
  innards = make_shared< Innards>();
  innards->out_file = out_file;
  innards->command = command;
  innards->inputs = inputs.copy();
}

// constructor
Source_From_File::Source_From_File( const string& out_file){
  innards = make_shared< Innards>();
  innards->out_file = out_file;
}

//############################
File_Time Source_From_File::Innards::time() const{
  
  auto ftime = file_time( out_file);
  if (!inputs.empty()){
    auto inp_time = most_recent_time( inputs);
    return std::max( ftime, inp_time);
  }
  else{
    return ftime;
  }
}

//############################
void Source_From_File::Innards::update(){
  
  updatet( inputs);
  
  auto run = [&](){
    auto final_command = subbed_string( command,
    results( inputs)) + " > " + out_file;
    run_command( final_command, out_file);
  };
  
  if (!command.empty()){
    if (!file_exists( out_file)){
      run();
    }
    else{
      if (!inputs.empty()){
        if (file_time( out_file) < most_recent_time( inputs)){
          run();
        }
      }
      else{
        run();
      }
    }
  }
  
}

//############################
void Source_From_File::update(){
  innards->update();
}

// constructor
Source::Source( Source_From_File& other){
  innards = other.innards;
}

Source::Source( Source_From_File&& other){
  innards = other.innards;
}

Innards_Gen::~Innards_Gen(){}

File_Time Source::time() const{
  return innards->time();
}

void Source::update(){
  innards->update();
}

string Source::result() const{
  return innards->result();
}

Source_From_File::Innards::~Innards(){}


}}
