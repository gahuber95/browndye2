(* 
Used internally.
Data structure used for 3-D vectors.
*)

type t = {
  mutable x : float;
  mutable y : float;
  mutable z : float;
}

val v3 : float -> float -> float -> t
val vscaled : float -> t -> t
val vdiff : t -> t -> t
val vsum : t -> t -> t
val vnorm : t -> float
val distance : t -> t -> float
val distance2 : t -> t -> float
val normed_diff : t -> t -> t
val normed : t -> t
val dot : t -> t -> float
val cross : t -> t -> t
