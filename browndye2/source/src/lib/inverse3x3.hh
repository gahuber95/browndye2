#pragma once

#include "array.hh"

namespace Browndye{

template< class T>
Mat3< decltype(1.0/T())> inverse( Mat3< T> A){
  
  Mat3< decltype( T()*T())> Acon;
  Acon[0][0] = A[1][1]*A[2][2] - A[2][1]*A[1][2];
  Acon[0][1] = A[2][1]*A[0][2] - A[0][1]*A[2][2];
  Acon[0][2] = A[0][1]*A[1][2] - A[0][2]*A[1][1];
  
  auto Adet = Acon[0][0]*A[0][0] + Acon[0][1]*A[1][0] + Acon[0][2]*A[2][0];
  
  Mat3< decltype( 1.0/T())> Ainv;
  
  Ainv[0][0] = Acon[0][0]/Adet;
  Ainv[0][1] = Acon[0][1]/Adet;
  Ainv[0][2] = Acon[0][2]/Adet;
  
  Ainv[1][0] = (A[1][2]*A[2][0] - A[1][0]*A[2][2])/Adet;
  Ainv[1][1] = (A[0][0]*A[2][2] - A[2][0]*A[0][2])/Adet;
  Ainv[1][2] = (A[1][0]*A[0][2] - A[0][0]*A[1][2])/Adet;
  
  Ainv[2][0] = (A[1][0]*A[2][1] - A[1][1]*A[2][0])/Adet;
  Ainv[2][1] = (A[2][0]*A[0][1] - A[0][0]*A[2][1])/Adet;
  Ainv[2][2] = (A[0][0]*A[1][1] - A[0][1]*A[1][0])/Adet;
  
  return Ainv;
}

}

