#pragma once
#include "../../lib/units.hh"
#include "../../lib/vector.hh"
#include "../../lib/array.hh"
#include "../../global/indices.hh"
#include "../../core_and_chain/chain/chain_common.hh"

namespace Browndye{

template< class Strukt, class Core_State, class Chain_State>
class Bonded_Structure_Interface{
public:

  typedef ::Length Length;
  typedef typename Strukt::Value Value;
  typedef decltype( Value()/Length()) Deriv;

  struct Chain_Ref{
    const Vector< Core_State, Core_Index>& core_states;
    const Chain_State& chain_state;
    Array< Array< Deriv, 3>, Strukt::n> derivs;
  
    Chain_Ref( const Vector< Core_State, Core_Index>& _core_states, const Chain_State& _cstate):
      core_states(_core_states), chain_state( _cstate){}
  };
  
  typedef Chain_Ref Chain;
  typedef typename Strukt::Index Group_Index;
  
  static
  Vec3< Length> position( const Chain_Ref& chain_ref, Group_Index gi, size_t ip){
    auto& cstate = chain_ref.chain_state;
    auto& chain = cstate.common();
    const Strukt* stag = nullptr;
    auto& strukt = bstrukt( chain, stag, gi);
    auto ai = strukt.atomis[ip];
    
    if (!strukt.on_core(ip)){
      return cstate.atoms[ai].pos;
    }
    else{
      auto mi = std::get< Core_Index>( strukt.coris[ip]);
      auto& core_state = chain_ref.core_states[mi];
      auto& core_common = core_state.common();
      auto& atoms = core_common.atoms;
      return core_state.transformed( atoms[ai].pos);
    }
  }
  
  static
  void set_derivative( Chain_Ref& chain_ref, Group_Index, size_t ip, size_t k, Deriv d){
    chain_ref.derivs[ip][k] = d;
  }
    
};
}


