#include "system_thread.hh"
#include "system_state.hh"

namespace Browndye{

// constructor
System_Thread::System_Thread( const System_Common& _common,
                               Thread_Index _number,
                               const Vector< uint32_t>& seed):
  common_ref(_common){
  number = _number;
  groups.reserve( common().groups.size());
  for( auto& cg: common().groups){
    groups.emplace_back( cg, *this);
  }
  rng.set_seed( seed);
}


//####################################################################
const Mobility_Info&
System_Thread::mob_info_of_key( const System_State& state,
                                 std::vector< bool> key){
  
  auto itr = mob_infos.find( key);
  if (itr != mob_infos.end()){
    return itr->second;
  }
  
  else{
    auto res = mob_infos.insert(
                             std::make_pair( key, Mobility_Info{ state}));
    assert( res.second);
    auto new_itr = res.first;
    return new_itr->second;
  }
}

//###################################################################
auto System_Thread::hydro_doer_of_key( System_State& state,
                                        std::vector< bool> key) -> Mob_Info_And_Doer&{
  
  auto itr = hydro_doers.find( key);
  if (itr != hydro_doers.end()){
    return itr->second;
  }
  else{
    auto& mob_info = mob_info_of_key( state, key);
    
    Mob_Info_And_State mias{ state, mob_info};
    typedef Generic_Mobility_Full::Doer< System_Mobility_Interface> SDoer; 
    SDoer doer{ mias};
    doer.update(mias);
    
    Mob_Info_And_Doer mindoer{ mob_info, std::move( doer)};
  
    auto ires = hydro_doers.insert( make_pair( key, std::move( mindoer)));
    assert( ires.second);
    
    auto jtr = ires.first;
    return jtr->second;
  }
}
}

