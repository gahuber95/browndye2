#pragma once

/*
 * tet.hh
 *
 *  Created on: Aug 26, 2016
 *      Author: root
 */


#include <memory>
#include "../global/indices.hh"
#include "../lib/vector.hh"
#include "../lib/array.hh"
#include "../structures/atom.hh"
#include "../transforms/transform.hh"
#include "../hydrodynamics/generic_mobility_full.hh"
#include "v4.hh"
#include "tet_number.hh"
#include "resistance_matrices.hh"
#include "unit_tet.hh"
#include "centroid.hh"
#include "tet_forces.hh"
#include "../generic_algs/remove_if.hh"

namespace Browndye{

//class Group_Beads;
//class Group_State;
//template< class GS>
//class Bead_Chain_Inv_Length_I;

// fixed  
struct Tet_Fixed{ 
  const Core_Indices core_indices;
  const Chain_Indices chain_indices;
  const Pos com0;
  const Mat3< double> smsmt;
  const Atom_Index i1, i2;
  const Vector< Transform, Ind_Core_Index> core_tforms0;
  const Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index> chain_poses0;
  const Length length;
  const Length radius;
  const V4< Pos> poses0;
  Inv_Length tmob;
  Inv_Length3 rmob;
  //Core_Index imin_core;
  
  /*
  struct Core_Chain_Con{
    Core_Index icore;
    Chain_Index ichain;
    Atom_Index icore_atom, ichain_atom, icore_cons;
  };
  */
  //Vector<  Core_Chain_Con, Constraint_Index> core_chain_cons;

    Tet_Fixed( const Core_Indices& _core_indices,
                          const Chain_Indices& _chain_indices,
                          Pos _com0, Mat3< double> _smsmt, Atom_Index _i1, Atom_Index _i2,
                          Vector< Transform, Ind_Core_Index>&& _core_tforms0,
                          Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index>&& _chain_poses0,
                          Length _length, Length _radius, V4< Pos> _poses0, Inv_Length _tmob, Inv_Length3 _rmob);

};

//#################################################
class Tet_Base{
public:  
  struct Small_Tet_With_Forces: public Small_Tet{
    V4< F3> forces;
  };
  
  // public data
  std::shared_ptr< const Tet_Fixed> fixed;
  V4< F3> forces;
  V4< Pos> poses;

  // more functions
  [[nodiscard]] Transform transform_of() const;
  [[nodiscard]] bool constraints_not_far_off() const;
   
  [[nodiscard]] const Core_Indices& core_indices() const;
   
  void get_from_small_tet( const Small_Tet_With_Forces&);
   
  [[nodiscard]] Length hi_radius( bool has_hi) const;
  
  [[nodiscard]] Inv_Length3 rotational_mobility() const;
  
  [[nodiscard]] Length length() const;
  [[nodiscard]] Length bead_radius() const;

  [[nodiscard]] Small_Tet_With_Forces small_tet() const;
      
  #ifdef DEBUG
  ~Tet_Base(){
    fixed.reset();
  }
  #endif
      
private:
  // private data  
protected:  
  static
  std::tuple< V4< Pos>, Length, Length>
  blank_small_tet();
  
  //static
  //Small_Tet test_tet( Length d, Length radius);
  
  static 
  std::pair< Length, Length>
  equivalent_tet_geometry_hr( bool has_hi, Length hradius);
};

//###############################################
template< class Group_State>
class Tet: public Tet_Base{
public:
  
  Tet() = delete;
  Tet( Group_State&, const Core_Indices&, const Chain_Indices&);
  
  Tet( const Tet&);
  Tet& operator=( const Tet&);
  Tet( Tet&& other) noexcept ;
  Tet& operator=( Tet&&) noexcept ;
  
  // applied after tet vertices have been moved with bd and reconstrained
  void push_transforms_down();
  void push_core_tforms_down();  
    
  // assume that torque is about center of tet
  void compute_vertex_forces();
  
  static  
  std::tuple< Inv_Length, Pos>
  mobility_and_center( const Group_State&);  
  
  //void link_to_cores(); 
  
  // changeable
  Group_State* state = nullptr;

  // public types
  struct Init_Bead_Info{
    const Group_State& state;
    const Core_Indices& core_indices;
    const Chain_Indices& chain_indices;
  };
  
  void set_group_state( Group_State& gs){
    state = &gs;
  }
  
  #ifdef DEBUG
  ~Tet(){
    state = nullptr;
  }
  #endif
  
private:
  friend class Tet_Tester;
  
  void copy_from( const Tet& other); 
  
  static
  Small_Tets small_core_tets( const Init_Bead_Info&);
  
  static
  std::tuple< Inv_Length, Pos>
  mobility_and_center( const Bead_Info< Group_State>&);
    
  static
  Resistance_Matrices
  tet_resistance_matrices( const Bead_Info< Group_State>&);
  
  // returns tet half-length, bead radius, center, hydro radius
  static
  std::tuple< Length, Length, Pos, Length>
  equivalent_tet_geometry( const Init_Bead_Info&);
  
  std::unique_ptr< Tet_Fixed>
  new_fixed( const Group_State&, const Core_Indices&, const Chain_Indices&) const;
  
  [[nodiscard]] Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index>
  new_chain_positions( const Chain_Indices&) const;
  
  [[nodiscard]] Vector< Transform, Ind_Core_Index>
  new_core_transforms( const Core_Indices&)  const;
   
  static
  std::tuple< V4< Pos>, Length, Length>
  tet_bead_positions_and_radii( const Init_Bead_Info&);    
};

//#######################################
/*
template< class GS>
void Tet< GS>::link_to_cores(){
  
  if (debug){
    auto& cis = core_indices();
    for( auto ic: cis){
      auto& core = state.core_states[ic];
      core.tet = this;
    }
  }
}
*/
//############################################
template< class GS>
void Tet< GS>::copy_from( const Tet& other){
  
  fixed = other.fixed;
  forces = other.forces;
  poses = other.poses;
  //link_to_cores();
}

// copy constructor
template< class GS>
Tet< GS>::Tet( const Tet& other):
  state( other.state){
  copy_from( other);
}

template< class GS>
Tet< GS>& Tet< GS>::operator=( const Tet& other){

  if (&other != this) {
    state = other.state;
    copy_from(other);
  }
  return *this;
}

// move constructor
template< class GS>
Tet< GS>::Tet( Tet&& other) noexcept:
  state( other.state){
  copy_from( other);
}

template< class GS>
Tet< GS>& Tet< GS>::operator=( Tet&& other) noexcept{
  
  state = other.state;
  copy_from( other);
  return *this;
}

std::pair< Inv_Length, Inv_Length3> 
mobs_from_tet_geom( Length d, Length radius);

//#####################################################
inline
auto Tet_Base::small_tet() const -> Small_Tet_With_Forces{
  
  Small_Tet_With_Forces stet;
  stet.poses = poses;
  stet.radius = fixed->radius;
  stet.forces = forces;
  stet.fixed = &(*fixed);
  return stet;
}

//##################################################
inline
void Tet_Base::get_from_small_tet( const Small_Tet_With_Forces& stet){
  
  poses = stet.poses;
  forces = stet.forces;
}

//################################################
inline
Inv_Length3 Tet_Base::rotational_mobility() const{
  
  return fixed->rmob;
}


inline
Length Tet_Base::length() const{
  
  return fixed->length;
}

inline
Length Tet_Base::bead_radius() const{
  
  return fixed->radius;
}

inline
auto Tet_Base::core_indices() const -> const Core_Indices&{
   return fixed->core_indices;
 } 

//######################################################################
std::tuple< Atom_Index, Pos, Atom_Index, Pos>
best_triad_pair( const V4< Pos>& poses);

Mat3< double> triad_matrix( Pos r1, Pos r2);

template< class GS>
auto Tet< GS>::new_fixed( const GS& state0,
                         const Core_Indices& core_indices0,
                          const Chain_Indices& chain_indices0) const -> std::unique_ptr< Tet_Fixed>{
  
  
  auto chain_indices1 = chain_indices0.copy();
  
  auto has_no_native = [&]( Chain_Index ic){
     return state0.chain_has_no_native( ic);
   };
   
  remove_if_true( chain_indices1, has_no_native);
  
  Init_Bead_Info ibinfo{ state0, core_indices0, chain_indices1};
  
  auto [poses, radius, hradius] = tet_bead_positions_and_radii( ibinfo);
    
  auto tmob = 1.0/(pi6*hradius);
  auto rmob = 1.0/(pi8*hradius*sq( hradius));
  
  auto com0 = centroid( poses);
  auto length = distance( poses[Atom_Index(1)], poses[Atom_Index(0)]);
  
  auto [i1,r1,i2,r2] = best_triad_pair( poses);
  auto smsmt = triad_matrix( r1, r2);
    
  auto core_tforms = new_core_transforms( core_indices0);
  auto chain_poses = new_chain_positions( chain_indices1);

  return std::make_unique< Tet_Fixed>( core_indices0.copy(), std::move( chain_indices1), com0, smsmt, i1,i2,
                    std::move( core_tforms), std::move( chain_poses), length, radius, poses, tmob, rmob);
}

//##########################################################
// constructor
// assumes chains' collision structures are up to date
template< class Group_State>
Tet< Group_State>::Tet( Group_State& state0,
                         const Core_Indices& core_indices,
                         const Chain_Indices& chain_indices){
  state = &state0;
  fixed = new_fixed( state0, core_indices, chain_indices);
  poses = fixed->poses0;
  
  //link_to_cores();
}

//#############################################################
template< class GS>
std::tuple< V4< Pos>, Length, Length>
Tet< GS>::tet_bead_positions_and_radii( const Init_Bead_Info& binfo){
    
  auto nc = binfo.chain_indices.size();
  auto nm = binfo.core_indices.size();
  if (nc == Ind_Chain_Index(0) && nm == Ind_Core_Index(0)){
    return blank_small_tet();
  }
  else{  
    auto [hlength, radius, com, hradius] =
      equivalent_tet_geometry( binfo);
      
    auto uverts = unit_tetrahedron();
    
    auto poses = uverts.mapped( [len=hlength,c=com]( auto upos){
      return len*upos + c;
    });
    
    return make_tuple( poses, radius, hradius);
  }
}

//#########################################################################
template< class GS>
Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index>
Tet< GS>::new_chain_positions( const Chain_Indices& chain_indices) const{
  
  Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index> chain_poses;
  for( auto ci: chain_indices){
    chain_poses.emplace_back();
    auto& tchain = chain_poses.back();
    auto& chain = state->chain_states[ci];
    auto& atoms = chain.atoms;
    tchain.reserve( atoms.size());
    for( auto& atom: atoms){
      tchain.push_back( atom.pos);
    }
  }
  
  return chain_poses;
}

//######################################################################### 
template< class GS>
Vector< Transform, Ind_Core_Index>
Tet< GS>::new_core_transforms( const Core_Indices& core_indices)  const{
  
  Vector< Transform, Ind_Core_Index> core_tforms;
  for( auto mi: core_indices){
     auto& core = state->core_states[mi];
     core_tforms.push_back( core.transform());
  }
  return core_tforms;
}

//########################################################################
template< class GS>
void Tet< GS>::compute_vertex_forces(){
  
  Vec3< Force> F( Zeroi{});
  Vec3< Torque> T( Zeroi{});

  auto t_cen = centroid( poses);
  
  for( auto mi: fixed->core_indices){
    auto& core = state->core_states[mi];
    F += core.force;
    auto m_cen = core.translation();
    T += cross( m_cen - t_cen, core.force) + core.torque;
  }

  for( auto ci: fixed->chain_indices){
    auto& cstate = state->chain_states[ci];
    for( auto& satom: cstate.atoms){
      F += satom.force;
      T += cross( satom.pos - t_cen, satom.force);
    }
  }
  
  forces = tet_vertex_forces( poses, F, T);
}

//########################################################################
template< class GS>
void Tet< GS>::push_core_tforms_down(){
  
  auto& core_indices = fixed->core_indices;
  auto& core_tforms0 = fixed->core_tforms0;
  auto tform = transform_of();
  
  for( auto iim: range( core_indices.size())){
    auto im = core_indices[iim];
    auto& core = state->core_states[im];
    auto new_tform = tform*core_tforms0[iim];
    core.set_transform( new_tform);
  } 
}

//############################################################
template< class GS>
void Tet< GS>::push_transforms_down(){
  
  auto tform = transform_of();
  
  auto& core_indices = fixed->core_indices;
  auto& core_tforms0 = fixed->core_tforms0;
  auto& chain_indices = fixed->chain_indices;
  auto& chain_poses0 = fixed->chain_poses0;
  
  poses = fixed->poses0.mapped( [&]( Pos pos){
    return tform.transformed( pos);
  });
  
  // needed because of round-off error?
  for( auto iim: range( core_indices.size())){
    auto im = core_indices[iim];
    auto& core = state->core_states[im];
    auto new_tform = tform*core_tforms0[iim];
    core.set_transform( new_tform);
    core.update_constrained_atoms();
  }
  
  for( auto iic: range( chain_indices.size())){
    auto ic = chain_indices[iic];
    auto& cstate = state->chain_states[ic];
    auto& catoms = cstate.atoms;
    auto& cposes = chain_poses0[iic];
    for( auto ai: range( catoms.size())){
      catoms[ai].pos = tform.transformed( cposes[ai]);
    }
  }
}

//##########################################################################
// constructor
template< class Group_State>
Bead_Info< Group_State>::Bead_Info( const Group_State& _state,
                               const Small_Tets& _stets,
                              const Chain_Indices& _chain_indices):
  state(_state), stets(_stets), chain_indices(_chain_indices){
  
  nc_atoms = Atom_Index(0);
  constexpr Atom_Index a0(0);
  for( auto ci: chain_indices){
    nc_atoms += state.chain_states[ci].n_atoms() - a0;
  }
}

//##################################################
// return length scale, bead radius, mobility center, hydro radius
template< class GS>
std::tuple< Length, Length, Pos, Length>
Tet< GS>::equivalent_tet_geometry( const Init_Bead_Info& ibinfo){
  
  auto& state = ibinfo.state;
  auto has_hi = state.has_hi();
  auto small_tets = small_core_tets( ibinfo);
  
  Bead_Info binfo( state, small_tets, ibinfo.chain_indices);
  
  auto [tmob, cen] = mobility_and_center( binfo);
  auto hradius = 1.0/(pi6*tmob);
  
  auto [hlength, radius] =
    equivalent_tet_geometry_hr( has_hi, hradius);
  return make_tuple( hlength, radius, cen, hradius);
}

//#####################################################
template< class GS>
std::tuple< Inv_Length, Pos>
Tet< GS>::mobility_and_center( const GS& group){

  //cout << "mobility and center" << endl;
  auto indc = []( Chain_Index ic){
    return Ind_Chain_Index{ ic/Chain_Index(1)};
  };
    
  auto nc = group.chain_states.size();
  Chain_Indices chain_indices( indc( nc));
  for( auto ic: range( nc)){
    chain_indices[ indc( ic)] = ic;
  }
  
  auto indm = []( Core_Index ic){
    return Ind_Core_Index{ ic/Core_Index(1)};
  };
  
  auto nm = group.core_states.size();
  Core_Indices core_indices( indm( nm));
  for( auto im: range( nm)){
    core_indices[ indm( im)] = im;
  }
  
  Init_Bead_Info ibinfo{ group, core_indices, chain_indices};
  auto small_tets = small_core_tets( ibinfo);
  Bead_Info binfo( group, small_tets, ibinfo.chain_indices);

  //cout << "mobility center almost at end " << endl;
  return mobility_and_center( binfo);
}

//##############################################################std####
template< class GS>
auto Tet< GS>::small_core_tets( const Init_Bead_Info& ibinfo) -> Small_Tets{
  
  auto& state = ibinfo.state;
  bool has_hi = state.has_hi();
  Small_Tets small_tets;
     
  for( auto mi: ibinfo.core_indices){
    auto& core_state = state.core_states[mi];
    auto& common = core_state.common();
    auto [d,bradius] = equivalent_tet_geometry_hr( has_hi, common.h_radius); 
    auto uverts = unit_tetrahedron();
    
    auto& stet = small_tets.emplace_back();
    stet.radius = bradius;
    auto& poses = stet.poses;
    for( auto i: range( Atom_Index(4))){
      poses[i] = core_state.transformed( d*uverts[i]);
    }
  }
   
  return small_tets;
}

//####################################################################
template< class GS>
Resistance_Matrices
Tet< GS>::tet_resistance_matrices( const Bead_Info< GS>& binfo){

  auto has_hi = binfo.state.has_hi();
  if (has_hi){
    return resistance_matrices_hi( binfo);
  }
  else{
    return resistance_matrices_nohi( binfo);
  }
}

//#################################################################
template< class GS>
std::tuple< Inv_Length, Pos>
Tet< GS>::mobility_and_center( const Bead_Info< GS>& binfo){

  //cout << "inner mobility and center" << endl;
  auto res_mats = tet_resistance_matrices( binfo);  
  auto cen = res_mats.hc;
  auto& A = res_mats.A;
  auto a = inverse( A);
  auto tmob = trace( a)/3.0;

  //cout << "end inner mobility and center" << endl;
  return std::make_tuple( tmob, cen);
}

}


