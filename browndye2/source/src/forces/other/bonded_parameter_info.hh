#pragma once

#include <map>
#include "../../structures/bonded_structures.hh"
#include "../force_field_type.hh"
#include "parameter_info.hh"

namespace Browndye{

class Bonded_Parameter_Info: public Parameter_Info{
public:
  std::map< std::string, Bond_Type_Index> bond_dict;
  std::map< std::string, Angle_Type_Index> angle_dict;
  std::map< std::string, Dihedral_Type_Index> dihedral_dict;
  
  [[nodiscard]] virtual Force_Field_Type ff_type() const = 0;
  virtual ~Bonded_Parameter_Info()= default;
};
}
