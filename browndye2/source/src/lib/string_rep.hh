#pragma once

#include <sstream>

namespace Browndye{
  
  template< class T>
  std::string string_rep( T&& data){
    
    std::stringstream ss;
    ss << std::forward<T>( data);
    std::string rep;
    ss >> rep;
    return rep;
  }
}