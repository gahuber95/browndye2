#pragma once

#include "vector.hh"

namespace Browndye{
  
  template< class T, class Index_R, class Index_C, bool is_cons>
  class Big_Matrix_Row;
  
  template< class T, class Index_R = size_t, class Index_C = size_t>
  class Big_Matrix{
  public:
    
    Big_Matrix();
    Big_Matrix( Index_R, Index_C);
    Big_Matrix( const Big_Matrix&) = delete;
    Big_Matrix& operator=( const Big_Matrix&) = delete;
    Big_Matrix( Big_Matrix&&) = default;
    Big_Matrix& operator=( Big_Matrix&&) = default;
    
    Index_R n_rows() const;
    Index_C n_cols() const;
    
    void resize( Index_R, Index_C);
    Big_Matrix copy() const;
    
    const T& operator()( Index_R, Index_C) const;
    T& operator()( Index_R, Index_C);
    
    Big_Matrix_Row< T, Index_R, Index_C, true> row( Index_R) const;
    Big_Matrix_Row< T, Index_R, Index_C, false> row( Index_R);
    
  private:
    friend class Big_Matrix_Row< T, Index_R, Index_C, true>;
    friend class Big_Matrix_Row< T, Index_R, Index_C, false>;
    
    Index_R nr;
    Index_C nc;
    Vector< T> data;
    
    size_t index( Index_R, Index_C) const;
    void check_bounds( Index_R, Index_C) const;
  };
  
  //######################################################
  template< class T, class IR, class IC, bool is_cons>
  class Big_Mat_Type;
  
  template< class T, class IR, class IC>
  class Big_Mat_Type< T, IR, IC, true>{
  public:
    typedef const Big_Matrix< T, IR, IC>& Type;
    typedef const T& Elem_Ref;
  };

  template< class T, class IR, class IC>
  class Big_Mat_Type< T, IR, IC, false>{
  public:
    typedef Big_Matrix< T, IR, IC>& Type;
    typedef T& Elem_Ref;
  };
  
  //##########################################################
  template< class T, class Index_R, class Index_C, bool is_cons>
  class Big_Matrix_Row{
  public:
    typedef typename Big_Mat_Type< T, Index_R, Index_C, is_cons>::Elem_Ref Elem_Ref;
    
    Elem_Ref& operator[]( Index_C);
    const T& operator[]( Index_C) const;
    Index_C size() const;
    
  private:
    friend class Big_Matrix< T, Index_R, Index_C>;
    typedef typename Big_Mat_Type< T, Index_R, Index_C, is_cons>::Type Mat_Ref;
    
    Big_Matrix_Row( Mat_Ref, Index_R);
    

    Mat_Ref matrix;
    size_t offset;
  };
  
  //###########################################################
  template< class T, class IR, class IC, bool is_cons>
  const T& Big_Matrix_Row< T,IR,IC,is_cons>::operator[]( IC ic) const{
    
    auto i = offset + (ic/IC(1));
    return matrix.data[i];
  }
  
  template< class T, class IR, class IC, bool is_cons>
  auto Big_Matrix_Row< T,IR,IC,is_cons>::operator[]( IC ic) -> Elem_Ref&{
    
    auto i = offset + (ic/IC(1));
    return matrix.data[i];
  }
  
  //###########################################################
  // constructor
  template< class T, class IR, class IC, bool is_cons>
  Big_Matrix_Row< T,IR,IC,is_cons>::Big_Matrix_Row( Mat_Ref mr, IR ir)
    :matrix( mr)
  {
    size_t nc = mr.n_cols()/IC(1);
    offset = (ir/IR(1))*nc;
  }
  
  template< class T, class IR, class IC, bool is_cons>
  IC Big_Matrix_Row< T,IR,IC,is_cons>::size() const{
    return matrix.n_cols();
  }
  
  //########################################################
  template< class T, class IR, class IC>
  Big_Matrix_Row< T, IR, IC, true>
  Big_Matrix< T,IR,IC>::row( IR ir) const{
    
    return Big_Matrix_Row< T, IR, IC, true>( *this, ir);
  }
  
  template< class T, class IR, class IC>
  Big_Matrix_Row< T, IR, IC, false>
  Big_Matrix< T,IR,IC>::row( IR ir){
  
    return Big_Matrix_Row< T, IR, IC, false>( *this, ir);
  }
  
  //########################################################
  // constructors
  template< class T, class IR, class IC>
  Big_Matrix< T,IR,IC>::Big_Matrix(){
    nr = IR{0};
    nc = IC{0};
  }
  
  //################################################
  template< class T, class IR, class IC>
  Big_Matrix< T,IR,IC>::Big_Matrix( IR _nr, IC _nc){
    
    resize( _nr, _nc);
  }
  //################################################
  template< class T, class IR, class IC>
  IR Big_Matrix< T,IR,IC>::n_rows() const{
    return nr;
  }

  template< class T, class IR, class IC>
  IC Big_Matrix< T,IR,IC>::n_cols() const{
    return nc;
  }
  //################################################
  template< class T, class IR, class IC>
  size_t Big_Matrix< T,IR,IC>::index( IR ir, IC ic) const{
   
    if constexpr (debug){
      check_bounds( ir, ic);
    } 
    
    return (ir/IR(1))*(nc/IC(1)) + (ic/IC(1));
  }
  
  //###########################################################
  template< class T, class IR, class IC>
  void Big_Matrix< T,IR,IC>::check_bounds( IR ir, IC ic) const{
    
    if (ir >= nr){
      error( "Big_Matrix: row index out of bounds", nr, ir);
    }
    
    if (ic > nc){
      error( "Big_Matrix: column index out of bounds", nc, ic);
    }
    
  }
  
  //#######################################################
  template< class T, class IR, class IC>
  T& Big_Matrix< T,IR,IC>::operator()( IR ir, IC ic){
    
    return data[ index( ir,ic)];
  }
  
  template< class T, class IR, class IC>
  const T& Big_Matrix< T,IR,IC>::operator()( IR ir, IC ic) const{
  
    return data[ index( ir,ic)];
  }

  //####################################################
  template< class T, class IR, class IC>
  void Big_Matrix< T,IR,IC>::resize( IR _nr, IC _nc){
    
    nr = _nr;
    nc = _nc;
    size_t nt = (nr/IR(1))*(nc/(IC(1)));
    data.resize( nt);
    
    if constexpr (debug){
      typedef JAM_Vector::Default_Value< T> DV;
      for( auto i: range( nt)){
        data[i] = DV::value();
      }
    }
  }
  
  //######################################################
  template< class T, class IR, class IC>
  Big_Matrix< T, IR,IC> Big_Matrix< T,IR,IC>::copy() const{
    
    Big_Matrix res;
    res.nr = nr;
    res.nc = nc;
    res.data = data.copy();
    return res;
  }
}