#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Uses a binary search to find the position of a item in an ordered array.
*/

#include <algorithm>
#include "vector.hh"

namespace Browndye{

// returns index of lower value
// for interval [x1,x2], if x == x1, will return index of x1
template< class U, class Idx>
Idx found( const Vector< U,Idx>& a, U x){
  
  auto itr = std::upper_bound( a.begin(), a.end(), x);
  
  if (debug){
    if (itr == a.begin())
      error( "found: below lower bound", x);
    else if (itr == a.end())
      error( "found: above upper bound", x);
  }
  
  return Idx( itr - a.begin() - 1);   
}

}

