#pragma once
#include "jam_xml_pull_parser.hh"

namespace Browndye{

//###################################################
 class Tag;
 
template< class ... Ts>
std::shared_ptr< Tag> tag( Ts ... ts){
  return std::make_shared< Tag>( ts...);
}

//############################################
class Tag{
public:
  typedef std::string string;
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  template< class ... Ts>
  Tag( const string&, Ts ... ts);
   
  void check_node( Node_Ptr) const;

private:
  std::pair< SList< string>, SList< string> >
  spurious_n_used( std::list< Node_Ptr>& nchildren) const;
 
  template< class T0, class ... Ts>
  void add( T0 t0, Ts ... ts);
  
  void add(){}

  const Node_Ptr&
  child_node_of_tag( const std::list< Node_Ptr>& nchildren,
                              const string& utag) const;

  const Tag& child_tag_of_tag( const string& utag) const;

  string name;    
  Vector< std::shared_ptr< Tag> > children; 
};

//##############################################
// constructor
template< class ... Ts>
Tag::Tag( const string& _name, Ts ... ts){
  name = _name;
  add( ts...);
}

//###################################################
template< class T0, class ... Ts>
void Tag::add( T0 t0, Ts ... ts){
  
  if constexpr (std::is_same< const char*, T0>::value){
    children.push_back( std::make_shared< Tag>( string( t0)));
  }
  else{
    children.push_back( t0);
  }
  
  add( ts...);
}

}