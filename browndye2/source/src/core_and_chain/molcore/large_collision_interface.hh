#pragma once

#include <utility>
#include "../../structures/atom.hh"
#include "../../lib/vector.hh"
#include "../../transforms/blank_transform.hh"
#include "partition_contents_ext.hh"

namespace Browndye{

  template< bool mobile>
  class Large_Col_Interface_Base;

  template<>
  class Large_Col_Interface_Base< true>{
  public:
    typedef Vector< Atom, Atom_Index > Container;
    typedef Atom_Index Ref; // single reference to sphere
  };

  template<>
  class Large_Col_Interface_Base< false>{
  public:
    typedef const Vector< Atom, Atom_Index > Container; // of spheres
    typedef Atom_Index Ref; // single reference to sphere
  };

template< bool is_mobile>
class Large_Col_Interface: public Large_Col_Interface_Base< is_mobile>{
public:

  typedef Large_Col_Interface_Base< is_mobile> Parent;
  typedef typename Parent::Ref Ref;
  typedef typename Parent::Container Container;

  typedef Vector< Ref, Ind_Atom_Index> Refs; // references to spheres in Container
  typedef Blank_Transform Transform; // translation and rotation
  typedef Ind_Atom_Index Index; // integer index to spheres
  typedef Pos Position;
  typedef ::Length Length;

  static constexpr bool is_changeable = is_mobile;
  static constexpr bool is_chain = false;

  static
  void does_have_near_interaction( Container&){}

  static
  Ref reference( const Container&, const Refs& refs, Index i){
    return refs[i];
  }
  
  // called to tell all spheres that previous transforms are now forgotten.
  static
  void clear_transform( Container&, Refs&){}

  static
  void copy_references( Container& atoms, Refs& arefs){

    auto n = converted< Ind_Atom_Index >( atoms.size());
    arefs.resize( n);
    for( auto i: range( n)) {
      arefs[i] = converted< Atom_Index>( i);
    }
  }

  static
  void copy_references( const Container&, const Refs& arefs0,
                        Refs& arefs1){
    auto n = arefs0.size();
    arefs1.resize( n);
    for( auto i: range( n)) {
      arefs1[i] = arefs0[i];
    }
  }

  static
  Index size( const Container& atoms){
    return converted< Ind_Atom_Index>(atoms.size());
  }

  static
  Index size( const Container&, const Refs& refs){
    return refs.size();
  }

  static
  Pos position( const Container& atoms, Ref ref){
    return atoms[ref].pos;
  }

  static
  Pos position( const Container& atoms, const Refs&, Ref ref){
    return atoms[ref].pos;
  }

  static
  Pos position( const Container& atoms, const Refs& refs, Index i){
    return atoms[ refs[i]].pos;
  }

  static
  Pos transformed_position( Pos pos, const Transform&){
    return pos;
  }

  static
  Length radius( const Container& atoms, const Refs& refs, Ind_Atom_Index i){
    return atoms[refs[i]].interaction_radius;
  }

  template< class F>
  static
  void partition_contents( const Container& atoms, const Refs& refs, const F& f,
                           Refs& refs0, Refs& refs1){
    partition_contents_ext< Large_Col_Interface>( atoms, refs, f, refs0, refs1);
  }

  static
  void empty_container( Container&, Refs& refs){
    refs.resize( Ind_Atom_Index{0});
  }

  static
  void swap( Container&, Refs& refs, Index i, Index j){
    auto ref = refs[i];
    refs[i] = refs[j];
    refs[j] = ref;
  }
  
  // not needed for Collision_Detector, but for Browndye force calculations
  static
  Atom_Type_Index atom_type( const Container& atoms, Ref ref){
    return atoms[ref].type;
  }
  
  static
  Vec3< Length> transformed_position( Container& atoms, const Refs& refs, const Blank_Transform, Index i){
    return atoms[ refs[i]].pos;
  }

  static
  Vec3< Length> transformed_position( Container& atoms, const Blank_Transform, Ref ref){
    return atoms[ref].pos;
  }

  static
  Length physical_radius( const Container& atoms, Ref ref){
	  return atoms[ref].radius;
  }

  static
  Pos position( const Container& atoms, Index i){
	  return atoms[i].pos;
  }

  //static
  //void add_force([[maybe_unused]] const Container& atoms, [[maybe_unused]] Ref ref, const Vec3< Force>&){}
  
  // only needed for compiling; not used
  static
  Charge charge( const Container& atoms, const Refs&, Ref i){
    //error( __FILE__, __LINE__, "should not get here");
    return atoms[i].charge;
  }
  
  static
  Length2 sasa( const Container& atoms, Ref ref){
    return atoms[ref].sasa();
  }
  
  static
  Atom_Index atom_number( const Container& atoms, Ref ref){
    return ref;
  }

};

}

