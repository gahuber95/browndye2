#pragma once

#include "../../structures/atom.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../generic_algs/collision_detector.hh"
#include "small_collision_interface.hh"
#include "../../transforms/transform.hh"
#include "core_common.hh"

namespace Browndye{

class Group_Thread;

class Core_Thread: public Pre_Core_Thread{
public:

  // Molecule 0 on Group 0 is unused; all others are used
  Core_Thread( const Core_Common&, const Group_Thread&, bool is_used);
  
  Core_Thread() = delete;
  ~Core_Thread() = default;
  Core_Thread( const Core_Thread&) = delete;
  Core_Thread( Core_Thread&&) = default;
  Core_Thread& operator=( const Core_Thread&) = delete;
  Core_Thread& operator=( Core_Thread&&) = delete;

  [[nodiscard]] const Core_Thread* parent_copy() const;
  //Pos transformed_position( const Transform&, Atom_Index i);

  typedef Collision_Detector::Structure< Small_Col_Interface> Small_Col_Structure;

  const Group_Thread& group;

  //std::shared_ptr< Small_Col_Interface::Container> container, container_alt; // not used for Group 0 Core 0
  std::shared_ptr< Small_Col_Structure> collision_structure; //, collision_structure_alt;

private:
  void setup_child();
  void setup_not_child();
};
}
