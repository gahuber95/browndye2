#pragma once

/*
 * rxn_pair.hh
 *
 *  Created on: Oct 15, 2015
 *      Author: root
 */

#include "../global/limits.hh"
#include "../xml/node_info.hh"

namespace Browndye{
namespace Pathways{
  namespace JP = JAM_XML_Pull_Parser;

  template< class I>
  class Rxn_Pair{
  public:
    typedef typename I::Mol_Index Mol_Index;
    typedef typename I::Atom_Index Atom_Index;
    typedef typename I::Length Length;
    
    Mol_Index mol0{ maxi}, mol1{ maxi};
    Atom_Index atom0{ maxi}, atom1{ maxi};
    Length distance{ NaN}, req_distance{ NaN};

    Rxn_Pair( JP::Node_Ptr node, Mol_Index imol0, Mol_Index imol1);
    Rxn_Pair() = delete;
  };


template< class I>
Rxn_Pair<I>::Rxn_Pair( JP::Node_Ptr node, Mol_Index imol0, Mol_Index imol1){
  
  auto i01 = checked_array_from_node< Atom_Index, 2>( node, "atoms");
  atom0 = i01[0];
  atom1 = i01[1];
  
  req_distance = checked_value_from_node< Length>( node, "distance");

  auto mmax = Mol_Index( maxi);
  if (imol0 == mmax || imol1 == mmax)
    error( "molecules must be defined in reaction criterion");

  mol0 = imol0;
  mol1 = imol1;

  /*
  if (imol0 < imol1){
    mol0 = imol0;
    mol1 = imol1;
    atom0 = i0;
    atom1 = i1;
  }
  else if (imol0 > imol1){
    mol0 = imol1;
    mol1 = imol0;
    atom0 = i1;
    atom1 = i0;
  }
  else
    error( "pair_from_node: molecule indices must be different", imol0);
    */
}

}}

