#pragma once

/*
 * pos.hh
 *
 *  Created on: Jul 11, 2016
 *      Author: root
 */

#include "../lib/units.hh"
#include "../lib/array.hh"

namespace Browndye{

typedef Vec3< Length> Pos;
typedef Vec3< Force> F3;
typedef Vec3< Torque> T3;

}

