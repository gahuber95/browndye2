#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements the multipole expansion of the electric field outside
of the outer-most grid.

*/

#include "../../xml/jam_xml_pull_parser.hh"
#include "../../lib/units.hh"
#include "../../lib/array.hh"
#include "../../global/pi.hh"
#include "../../global/pos.hh"
#include "../../global/back_door.hh"

namespace Browndye{

 class [[maybe_unused]] Unit_Test_Tag{};

class Charge_Field{
public:
  typedef std::string string;

  Charge_Field( const string& file, Pos offset);
  //Charge_Field( std::ifstream&, Pos offset);
  //Charge_Field( JAM_XML_Pull_Parser::Parser&, Pos offset);
  //Charge_Field( Unit_Test_Tag){}

  [[nodiscard]] EPotential potential( Pos pos) const;
  [[nodiscard]] Vec3< EPotential_Gradient> gradient( Pos pos) const;
  [[nodiscard]] Length debye_length() const;
  //[[nodiscard]] Permittivity vacuum_permittivity() const;
  //[[nodiscard]] double solvent_dielectric() const;

  // real charge divided by 4 pi eps alpha; eps - vacuum permittivity,
  // alpha - solvent dielectric
  [[nodiscard]] Charge eff_charge() const;
  [[nodiscard]] Pos center() const;
  //Length fitted_radius() const;

  friend class Browndye::Back_Door;

private: 
  typedef JAM_XML_Pull_Parser::Parser Parser;
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  void initialize( Parser&, Pos offset);
  void initialize( std::ifstream&, const string& file, Pos offset);
  void shift_position( Pos);

  void get_potential_n_grads( Pos pos, EPotential& phi_out,
                              Vec3< EPotential_Gradient>& grad,
                              bool get_grad) const;

  Length cx,cy,cz;
  Charge charge;
  Length debye;
  Permittivity vperm;
  double solvdi = NaN;
};

inline
Length Charge_Field::debye_length() const{
  return debye;
}
  
inline
Charge Charge_Field::eff_charge() const{
  return charge;
}

/*
inline
Permittivity Charge_Field::vacuum_permittivity() const{
  return vperm;
}

inline
double Charge_Field::solvent_dielectric() const{
  return solvdi;
}
*/

inline
Pos Charge_Field::center() const{
  return Pos{ cx,cy,cz};
}

inline
void Charge_Field::shift_position( Pos offset){
  cx += offset[0];
  cy += offset[1];
  cz += offset[2];
}

}

