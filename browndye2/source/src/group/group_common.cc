/*
 * group_common.cc
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "../molsystem/system_common.hh"
#include "group_common.hh"
#include "group_thread.hh"
#include "../input_output/outtag.hh"
#include "../lib/icosohedron.hh"
#include "group_state.hh"
#include "../generic_algs/remove_duplicates.hh"
#include "../molsystem/system_state.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

//#################################################################################################################################
// constructor
// assume node is completed
Group_Common::Group_Common( const System_Common& _system, const JP::Node_Ptr& node, Group_Index _number): system(_system), number(_number){

  //cout << "new group" << endl;
  name = checked_value_from_node< std::string>( node, "name");
  
  Core_Index im( 0);
  Chain_Index ic( 0);
  Cor_Cha_Index icm( 0);
  auto& pinfo = *(system.pinfo);
  auto& bpinfo = *(system.bpinfo);
  
  for( const auto& cnode: node->all_children()){
    
    if (cnode->tag() == "core"){
      cores.emplace_back( pinfo, cnode, system, number, im);
       ++im;
       core_orders.push_back( icm);
       ++icm;
    }
    else if (cnode->tag() == "chain"){
      //cout << "chain" << endl;
      auto file = value_from_node< std::string>( cnode);
      auto lcnode = Chain_Common::chain_node( file);
      chains.emplace_back( pinfo, bpinfo, *this, lcnode, ic, number);
      ++ic;
      chain_orders.push_back( icm);
      ++icm;
      //cout << "end chain" << endl;
    }
  }
  
  // compute total charge
  charge = Charge( 0.0);
  for( auto& core: cores){
    charge += core.charge();
  }
  for( auto& chain: chains){
    charge += chain.charge();
  }  

  //cout << "before setup constraint" << endl;
  setup_constraint_info();
  //cout << "after setup constraint " << endl;
   
  get_dummies( node, pinfo); 

  //cout << "got here a" << endl;

  Vector< uint32_t> descriptor;
  System_Thread sthread( system, Thread_Index(0), descriptor);
  //cout << "got here b" << endl;
  mobility_info = mobility_info_of( sthread);
  //cout << "end new group" << endl;
}

//######################################################
void Group_Common::get_dummies( const JP::Node_Ptr& node, const Nonbonded_Parameter_Info& pinfo){
  
  auto dnodes = node->children_of_tag( "dummy");
  for( const auto& dnode: dnodes){
    dummies.emplace_back( dnode, pinfo, *this);
  }
}

//#########################################################################
auto Group_Common::mobility_info_of( System_Thread &sthread) const ->
    Mobility_Info {
  
  Group_Thread gthread( *this, sthread);
  Group_State gstate( *this, gthread, true);

  auto [tmob,center] =
    Tet< Group_State>::mobility_and_center( gstate);
   
  auto rmob = 27.0*pi*pi*tmob*sq( tmob); 
   
  return Mobility_Info{ center, tmob, rmob};
}

//###################################################################
std::pair< Length, Length>
Group_Common::field_and_atom_limits( const Solvent& solvent) const{
  
  auto smooth = smooth_radius( solvent);
  auto atom_extent = furthest_atom_extent();
  return std::make_pair( smooth, atom_extent);
}

//#################################################################
Length Group_Common::smooth_radius( const Solvent& solvent) const{
  
  auto radius0 = initial_radius_guess();
  EPotential tol( 0.01);
  
  auto rout = outer_radius_for_search( solvent, tol, radius0); 
   
  if (rout == Length( 0.0)){
    return Length( 0.0);
  }
  else{
    auto rin = inner_radius_for_search( solvent, tol, radius0);  
    
    auto sin = rms_potential_deviation( solvent, rin);
    auto sout = rms_potential_deviation( solvent, rout);
     
    auto args = std::make_tuple( rout, sout, rin, sin);
    return bisected_rms_radius( solvent, tol, radius0, args);
  }
}

//##############################################################################
template< class A>
Length Group_Common::bisected_rms_radius( const Solvent& solvent, EPotential tol,
                                         Length radius0, A args) const{
  
  Length rin, rout;
  EPotential fin, fout;
  std::tie( rout, fout, rin, fin) = args;
   
  auto rmid = 0.5*( rout + rin);
  if ((rout - rin) < 0.01*radius0){
    return rmid;
  }
  else{
    auto fmid = rms_potential_deviation( solvent, rmid);
    if (fmid > tol){
      auto largs = std::make_tuple( rout, fout, rmid, fmid);
      return bisected_rms_radius( solvent, tol, radius0, largs);
    }
    else{
      auto largs = std::make_tuple( rmid, fmid, rin, fin);
      return bisected_rms_radius( solvent, tol, radius0, largs);
    }
  }
}

//#############################################################################
Length Group_Common::outer_radius_for_search( const Solvent& solvent,
                                             EPotential tol, Length r0) const{
  auto r = r0;
  while( rms_potential_deviation( solvent, r) > tol){
    r *= 2.0;
  }
  return r;
}

Length Group_Common::inner_radius_for_search( const Solvent& solvent,
                                             EPotential tol,
                                             Length r0) const{
  auto r = r0;
  while( rms_potential_deviation( solvent, r) < tol && r > 1.0e-20*r0){
    r *= 0.5;
  }
  return r;
}

//#############################################################################
EPotential
Group_Common::rms_potential_deviation( const Solvent& solvent, Length r) const{
  
  Pos center( Zeroi{}); //mobility_info.center;
  auto surface = icosohedron_tri_cens( 4);
  
  auto Vs = surface.mapped( [&]( auto u){
    return this->electric_potential( solvent, center + r*u);
  });
  auto n = Vs.size();
  auto fn = (double)n;

  const EPotential V0( 0.0);
  auto Vavg =
    Vs.folded_left(
       [](auto a, auto b){return a+b;}, V0)/fn;
  
  auto s2sum = Vs.folded_left(
       [&]( auto sum, auto v){
          return std::max( sum, sq( v - Vavg));
        }, sq(V0));
  
  return sqrt( s2sum/fn);
}

//##################################################################################
EPotential Group_Common::electric_potential( const Solvent& solvent, Pos pos) const{
  
  auto V_core = electric_potential_from_cores(pos);
  auto V_chain = electric_potential_from_chains( solvent, pos); 
 
  return V_core + V_chain;
}

//###############################################################################
EPotential Group_Common::electric_potential_from_chains( const Solvent& solvent,
                                                        Pos pos) const{
  
  auto debye = solvent.debye_length;
  auto dielectric = solvent.dielectric;
  
  EPotential V( 0.0);
  for( auto& chain: chains){
    for( auto& atom: chain.atoms){
      auto r = distance( pos, atom.pos);
      V += exp(-r/debye)*atom.charge/(pi4*vacuum_permittivity*dielectric*r);
    }
  }
  return V;
}

//#############################################################################
EPotential Group_Common::electric_potential_from_cores( Pos pos) const{
  EPotential V( 0.0);
  for( auto& core: cores){
    if (core.v_field){
      auto offset = core.hydro_center;
      auto hint = core.v_hint0;
      V += core.v_field->potential( pos - offset, hint);
    }
  }
  return V;
}

//##################################################
Length Group_Common::initial_radius_guess() const{
    
  auto rm = initial_radius_guess_from_cores();
  auto rc = initial_radius_guess_from_chains();  
  return std::max( rm, rc);  
}

//#############################################################
Length Group_Common::initial_radius_guess_from_cores() const{
  
  auto gcenter = mobility_info.center;
  return cores.folded_left(
    [&]( Length maxr, const Core_Common& core){
      if (core.v_field){
        Pos low,high;
        core.v_field->get_corners( low, high);
        auto rad = 0.5*distance( low, high);
        return std::max( maxr, rad + norm( core.hydro_center - gcenter));
      }
      else{
        return maxr;
      }
    },
    Length( 0.0)
  );
}

//##############################################################
Length Group_Common::initial_radius_guess_from_chains() const{
  
  Pos gcenter( Zeroi{}); // = mobility_info.center;
  return chains.folded_left(
    [&]( Length maxr, const Chain_Common& chain){
      auto res = maxr;
      for( auto& atom: chain.atoms){
        res = std::max( res, norm( gcenter - atom.pos));  
      }
      return res;
    },
    Length( 0.0)
  );
}

//##############################################
Length Group_Common::furthest_atom_extent() const{
  
  auto rextm = furthest_extent_from_cores();
  auto rextc = furthest_extent_from_chains();
  return std::max( rextc, rextm);
}

//##########################################################
Length Group_Common::furthest_extent_from_chains() const{

  return chains.folded_left(
    [&]( Length rmax, const Chain_Common& chain){
      auto res = rmax;
      for( auto& atom: chain.atoms){
        res = std::max( res, norm( atom.pos) + atom.radius);
      }
      return res;
    },
    Length( 0.0)); 
}

//###################################################################
Length Group_Common::furthest_extent_from_cores() const{
  
  //auto center = mobility_info.center;
  Pos center( Zeroi{});
  return cores.folded_left(
    [&]( Length rmax, const Core_Common& core){
      auto res = rmax;
      for( auto& atom: core.atoms){
        res = std::max( res, norm( atom.pos - center) + atom.radius);
      }
      return res;
    },
    Length( 0.0));
}

//#####################################################################
using std::make_pair;
using std::map;

void Group_Common::setup_bound_core_atoms(){
  
  auto nm = cores.size();
  Vector< Ind_Atom_Index, Core_Index>
    ind_atom_indices( nm, Ind_Atom_Index(0));
  
  auto nc = chains.size();
  
  for( auto ic: range( nc)){
    
    auto bca = [&]( size_t jmax, bool is_constraint, auto& strukts){
      
      for( auto& strukt: strukts){
        for( auto j: range( jmax)){
          if (strukt.on_core( j)){
            auto im = std::get< Core_Index>( strukt.coris[j]);
            auto& core = cores[im];
                    
            core.bound_atoms.emplace_back();
            auto& atom_info = core.bound_atoms.back();
            
            atom_info.chain_infos.emplace_back();
            auto& chain_info = atom_info.chain_infos.back();
            
            chain_info.is_constrained = is_constraint;
            chain_info.ic = ic;
            atom_info.ia = strukt.atomis[ j];        
          }
        }
      }
    };
    
    auto& chain = chains[ic];
    bca( 2, false, chain.bonds);
    bca( 2, true, chain.length_constraints);
    bca( 3, false, chain.angles);
    bca( 4, false, chain.dihedrals);
    bca( 4, true, chain.coplanar_constraints);
  }
  
  for( auto& core: cores){
    auto& ba = core.bound_atoms;
    
    typedef Core_Common::Atom_Info AI;
    
    auto comp_ainfo = [&]( auto op){
      
      return [op]( const AI& a0, const AI& a1){
        return op( a0.ia, a1.ia);
      };
    };
    
    auto bab = ba.begin();
    auto bae = ba.end();
    std::sort( bab, bae, comp_ainfo( std::less< Atom_Index>()));

    Vector< Ind_Atom_Index> starts;
    starts.push_back( Ind_Atom_Index(0));
    auto n = ba.size();
        
    Ind_Atom_Index a1{ 1};
    auto ia1 = a1 - Ind_Atom_Index(0);
    for( auto i: range( a1, n)){
      if (ba[i].ia != ba[i-ia1].ia){
        starts.push_back( i);
      }
    }
    starts.push_back( n);
    
    // collect chain info onto first in group
    auto ns = starts.size();
    for( auto is: range( ns-1)){
      auto js0 = starts[is];
      auto js1 = starts[is+1];
      for( auto js: range( js0+ia1, js1)){
        ba[js0].chain_infos.push_back( ba[js].chain_infos.back());
      }
    }
    
    auto new_end = std::unique( bab, bae,
                             comp_ainfo( std::equal_to< Atom_Index>()));
    ba.erase( new_end, bae);
  }
}

//################################################
void Group_Common::setup_coreless_chains(){
  
  for( auto& core: cores){
    for( auto& ba: core.bound_atoms){
      for( auto& ca: ba.chain_infos){
        auto& chain = chains[ ca.ic];
        chain.is_coreless = false;
      }
    }
  }
  // should only be one coreless chain, and that with no cores
  
  auto ncl = chains.folded_left( []( int sum, const Chain_Common& chain){
    return sum + (chain.is_coreless ? 1 : 0);
  }, 0);
  
  if (ncl > 1){
    error( "cannot have a group with more than one chain unattached to a core");
  }
  else if (ncl == 1){
    if (cores.size() > Core_Index(0)){
      error( "chain must be attached to a core if one exists");
    }
  }
}

//##################################################
void Group_Common::setup_all_chain_beads(){
  auto nc = chains.size();
  for( auto ic: range( nc)){
    auto& chain = chains[ic];
    for( auto& ia: chain.constrained_atoms){
      all_chain_beads.emplace_back( ic, ia);
    }
  }
}

//################################################
void Group_Common::setup_all_tet_beads(){
  
  auto nm = cores.size();
  auto nt_max{ std::max( nm/Core_Index(1), size_t(1))};
  
  Bead0_Index nb{ nt_max*ntet};
  all_tet_beads.reserve( nb);
  
  for( auto it: range( Tet_Index( nt_max))){
    for( auto ia: range( Atom_Index( ntet))){
      all_tet_beads.emplace_back( it, ia);
    }
  }
}

//#############################################
void Group_Common::setup_all_core_beads(){
 
  auto nm = cores.size();
  for( auto im: range( nm)){
    auto& core = cores[im];
    auto& cba = core.bound_atoms;
    auto nia = cba.size();
    for( auto iia: range( nia)){
      auto& ainfo = cba[ iia];
      
      bool is_constrained = false;
      for( auto& cinfo: ainfo.chain_infos){
        is_constrained |= cinfo.is_constrained;
      }
      if (is_constrained){
        all_core_beads.emplace_back( im, iia);
      }
    }
  }
}

//###########################################
// refactor, using CC_Constraint_Setupper class
void Group_Common::setup_all_cm_constraints(){

  std::map< Chain_Bead, Bead0_Index> chain_bead_map;
  for( auto ib: range( all_chain_beads.size())){
    auto cbead = all_chain_beads[ib];
    chain_bead_map[ cbead] = ib;
  }
  
  std::map< Core_Bead, Bead0_Index> core_bead_map;
  for( auto ib: range( all_core_beads.size())){
    auto mbead = all_core_beads[ib];
    core_bead_map[ mbead] = ib;
   }
  
  std::map< std::pair< Core_Index, Atom_Index>, Ind_Atom_Index>
    core_iatom_map;
  
  auto nm = cores.size();
  for( auto im: range( nm)){
    auto& bas = cores[im].bound_atoms;
    for( auto iia: range( bas.size())){
      auto& ba = bas[iia];
      auto ia = ba.ia;
      core_iatom_map[ make_pair( im,ia)] = iia;
    }
  }
  
  const auto nacb = all_chain_beads.size() - Bead0_Index(0);
  
  auto bead_of = [&]( Chain_Index ic, Atom_Index ia,
                        Opt_Core_Index imo) -> Bead0_Index{
    
    if (std::holds_alternative<  On_Chain_Tag>( imo)){
      Chain_Bead cbead{ ic, ia};
      auto ib = chain_bead_map.at( cbead);
      return ib;
    }
    else{
      auto im = std::get< Core_Index>( imo);
      auto iia = core_iatom_map.at( make_pair( im,ia));
      Core_Bead mbead{ im, iia};
      auto ib = core_bead_map.at( mbead);
      return ib + nacb;
    }  
  };
  
  auto nc = chains.size();
  for( auto ic: range( nc)){
    auto& chain = chains[ic];
    for( auto& lcons: chain.length_constraints){
      
      Length_Constraint con;
      con.beads[0] = bead_of( ic, lcons.atomis[0], lcons.coris[0]);
      con.beads[1] = bead_of( ic, lcons.atomis[1], lcons.coris[1]);
      con.length = lcons.length;
        
      all_length_constraints.push_back( con);
    }
  }
  
  for( auto ic: range( nc)){
    auto& chain = chains[ic];
    for( auto& ccons: chain.coplanar_constraints){
      
      auto cbref = [&]( size_t k){
        return bead_of( ic, ccons.atomis[k], ccons.coris[k]);
      };
      
      Coplanar_Constraint con;
      con.beads = initialized_array< 4>([&]( size_t k){
        return cbref( k);
      });
      
      all_coplanar_constraints.push_back( con);
    }
  }  
}

//############################################
void Group_Common::setup_bound_cores_of_chains(){
  auto nc = chains.size();
  bound_cores_of_chains.resize( nc);
  
  auto nm = cores.size();
  for( auto im: range( nm)){
    auto& bas = cores[im].bound_atoms;
    for( auto& ba: bas){
      auto& chinfos =  ba.chain_infos;
      for( auto& chinfo: chinfos){
        auto ic = chinfo.ic;
        bound_cores_of_chains[ ic].push_back( im);
      }
    }
  }
  
  for( auto& bcc: bound_cores_of_chains){
    remove_duplicates( bcc);
  }
}

//#################################################
void Group_Common::setup_constraint_info(){

  setup_bound_core_atoms();
  setup_coreless_chains();
  setup_all_chain_beads();
  setup_all_core_beads();
  setup_all_tet_beads();
  setup_all_cm_constraints();
  setup_bound_cores_of_chains();   
}

// constructor
Group_Common::Group_Common( Test_Tag, const System_Common& sys, Vector< Core_Common, Core_Index>&& _cores, Vector< Chain_Common, Chain_Index>&& _chains, Group_Index num):
  system( sys), number( num){

  cores = std::move(_cores);
  Cor_Cha_Index icm( 0);
  for( auto im: range( cores.size())){
    (void)im;
    core_orders.push_back( icm);
    ++icm;
  }

  chains = std::move( _chains);
  for( auto ic: range( chains.size())){
    (void)ic;
    chain_orders.push_back( icm);
    ++icm;
  }

  setup_constraint_info();
}


}
