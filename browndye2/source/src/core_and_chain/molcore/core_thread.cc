/*
 * molecule_thread.cc
 *
 *  Created on: Jul 11, 2016
 *      Author: root
 */

#include "core_thread.hh"
#include "../../group/group_thread.hh"
#include "../../molsystem/system_thread.hh"

namespace Browndye{

const Core_Thread* Core_Thread::parent_copy() const{
  
  auto& system_thread = group.system_thread;
  
  for( auto& ogroup: system_thread.groups){
    for( auto& ocore: ogroup.core_threads){
      auto& ocommon = ocore.common;
      if (common.copy_parent == &ocommon)
        return &ocore;
    }
  }
   
  return nullptr;
}

// if the core has copies ("children"), then it will have an extra copy of the atom positions, collision structure, and container.
// if the core is a copy, then its atom positions, etc. are all pointers to the parent.
// if it is not a copy and has no copies, then it has one set of atom positions, etc.

Core_Thread::Core_Thread( const Core_Common& _common, const Group_Thread& _group, bool is_used):
    Pre_Core_Thread(_common), group(_group){

  if (is_used){
  
    if (common.copy_parent)
      setup_child();
    else
      setup_not_child();
  }

  auto na = common.atoms.size();
  ab_atoms.resize( na);
}

void Core_Thread::setup_not_child(){
  auto& atoms = common.atoms;
  auto na = atoms.size();
  atom_tposes.resize( na);

  if (group.common.number != Group_Index(0) || common.number != Core_Index(0)) {
    collision_structure = std::make_shared<Small_Col_Structure>(*this,max_per_cell);
  }
  /*
  if (common.has_copy_children()){
    atom_tposes_alt.resize( na);
    container_alt = std::make_shared< Small_Col_Interface::Container>( atoms, atom_tposes_alt);
    collision_structure_alt = std::make_shared<  Small_Col_Structure>( *container_alt, max_per_cell);
  }
   */
}

void Core_Thread::setup_child(){
  /*
  auto copy = parent_copy();
  if (copy){
    container = copy->container;
    collision_structure = copy->collision_structure;
    container_alt = copy->container_alt;
    collision_structure_alt = copy->collision_structure_alt;
  }
  else{
    error( __FILE__, __LINE__, "no copy found; should not get here");
  }
   */
}

}
