#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include <set>

#include "../molsystem/system_state.hh"
#include "simulator.hh"

namespace Browndye{

class WE_Simulator;

class WE_Interface{
public:
  typedef Sys_Copy_Index Object;
  typedef WE_Simulator Info;
  typedef Jam_Exception Exception;
  typedef Sys_Copy_Index Copy_Index;

  static
  void step_objects_forward( WE_Simulator& sim);

  static
  std::optional< size_t> bin( WE_Simulator& sim, Sys_Copy_Index icp);


  static
  Sys_Copy_Index new_object( WE_Simulator&);

  static
  void get_duplicated_objects( WE_Simulator&, 
                               Sys_Copy_Index, size_t n_copies,
                               std::list< Sys_Copy_Index>&);

  static
  void remove( WE_Simulator&, Sys_Copy_Index icp);

  static
  std::optional< size_t> crossing( WE_Simulator& sim, Sys_Copy_Index icp);

  static
  size_t number_of_bins( WE_Simulator& sim);

  static
  size_t number_of_bin_neighbors( WE_Simulator& sim, size_t bin);

  template< class Function> 
  static
  void apply_to_bin_neighbors( WE_Simulator& sim, size_t bin, 
                               Function& f);

  // low bins are closer to reaction
  static
  bool have_population_reversal([[maybe_unused]] WE_Simulator& sim, size_t bin0, double wt0,
                                 size_t bin1, double wt1);

  typedef WE_Simulator& WE_Simulator_Ptr;
  
  static
  double uniform( WE_Simulator& sim);

  static
  void output( WE_Simulator& sim, size_t crossing, double flux);

  static
  double reaction_coordinate( WE_Simulator& sim, Sys_Copy_Index icp);

  static
  size_t number_of_threads( WE_Simulator& sim);

  static
  double weight( const WE_Simulator& sim, Sys_Copy_Index icp);

  static
  void set_weight( WE_Simulator& sim, Sys_Copy_Index icp, double wt);
    
  template< class F>
  static
  void apply_to_object_refs( F& f, WE_Simulator& sim);
  
  static
  Sys_Copy_Index number_of_objects( const WE_Simulator&);
  
  static
  size_t number_of_fluxes( const WE_Simulator&);
  
private:
  static
  Sys_Copy_Index new_index( WE_Simulator& sim);
  
  static
   void initialize_object( System_State&);
};
}
