/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Top-level parsing classes used in the C++ code.
Implements a parser using the same model as the Ocaml version in
jam_xml_ml_pull_parser.ml. Implements two classes: the Parser and
the Node.  It wraps the Parser_1F class.
*/

#include <algorithm>
#include "jam_xml_pull_parser.hh"

namespace Browndye{
namespace JAM_XML_Pull_Parser{
  
using std::shared_ptr;
using std::move;
  
//#########################################
// assume node is completed
Node_Ptr Node::child( const std::string& tag){
  
  check_parser();
  auto itr = std::find_if( children.begin(), children.end(),
  [&]( Node_Ptr& node){
       return (node->tag() == tag);});
    
  if (itr == children.end()){
    return Node_Ptr();
  }
  else{
    return *itr;
  }       
}

void Node::check_parser() const{
  if (debug){
    if (!_parser){
      error( "no parser present", tag());
    }
  }
}

std::shared_ptr< Node> node_from_file( const std::string& file){
  
  return std::make_shared< Node>( file, Node::From_File{});
}

//###############################################################
// assume node is completed
std::list< Node_Ptr> Node::children_of_tag( const std::string& tag){
  
  check_parser();
  complete();
  std::list< Node_Ptr> tchildren;
  
  for( auto& child: children){
    if (child->tag() == tag){
      tchildren.push_back( child);
    }
  }
    
  return tchildren;
}

//#################################################
Parser::~Parser(){
  
  if (debug){
    std::stack< Node_Ptr> nodes;
    nodes.push( top_node);
    while (!nodes.empty()){
      auto node = nodes.top();
      node->_parser = nullptr;
      nodes.pop();
      for( auto child: node->children){
        nodes.push( child);
      }
    }
  }
}

//###################################################
const std::list< Node_Ptr>& Node::all_children() const{
  
  check_parser();
  return children;
}

//#####################################################################
// constructor
  Node::Node( const string& tag, const JP::Attrs& _attrs, Parser& parser):
    ttag( tag), attrs( _attrs), _parser(&parser){}

  // constructor
  Node::Node( const string& file, From_File){
    
    input.open( file);
    owned = std::make_unique< Parser>( input, file); 
    auto top = owned->top();
    top->complete();
    top->copy_to( *this);
  }
//###############################
bool Parser::done() const{
  return traversal_done;
}

//#########################################################################
void begin_tag( Parser& parser, const string& tag, const JP::Attrs& attrs){

  std::shared_ptr< Node> pnode;
  if (!parser.leading.empty()){
    pnode = parser.leading.top();
  }
  auto node = std::make_shared< Node>( tag, attrs, parser);
  
  node->line_num = parser.current_line_number();
 
  if (!parser.top_node){
    parser.top_node = node;
    parser.leading.push( parser.top_node);
  }
  else{    
    auto& cnodes = pnode->children;
    cnodes.insert( cnodes.end(), node);
    parser.leading.push( node);
  }
}

//########################################################################
void end_tag( Parser& parser, [[maybe_unused]] const string& ctag, Vector< string>&& data){
  
  auto& node = parser.leading.top();
  node->sdata = std::move( data);
  node->completed = true;
  parser.leading.pop();
}

//#################################################################
  // constructor
Parser::Parser( std::ifstream& _stream, const std::string& fname): 
  stream( _stream)
{
  jparser = std::make_unique< JParser>( *this, begin_tag, end_tag, fname);
  
  if (!stream.good()){
    error( "Parser constructor: stream not good for", fname);
  }
  while (!top_node){
    parse_some();
  }
}

//#################################################################
  // constructor
Parser::Parser( std::istream& _stream): 
  stream( _stream)
{
  jparser = std::make_unique< JParser>( *this, begin_tag, end_tag, "cin");
  
  if (!stream.good()){
    error( "Parser constructor: stream not good");
  }
  while (!top_node){
    parse_some();
  }
}

//#################################################################
  // constructor
Parser::Parser( std::stringstream& _stream): 
  stream( _stream)
{
  jparser = std::make_unique< JParser>( *this, begin_tag, end_tag, "cin");
  
  if (!stream.good()){
    error( "Parser constructor: stream not good");
  }
  while (!top_node){
    parse_some();
  }
}

//########################################
void Parser::complete_node( Node* node){
  
  while (!node->completed){
    parse_some();
  } 
}

//#####################################
void Parser::parse_some(){
  jparser->parse_some( stream, parsing_done);
}

//###################################################
// node not necessarily completed at start or end
// once it is found, the nodes behind are deleted
std::pair< Node_Ptr, size_t>
Parser::next_child_with_tag( Node_Ptr node,
                            const Vector< std::string>& tags){
  
  node->check_parser();
  auto& children = node->children;
  auto nt = tags.size();
  while(true){

    //for( auto& cnode: children){
    for( auto itr = children.begin(); itr != children.end(); ++itr){
      auto cnode = *itr;
      for( auto it: range( nt)){
        auto& tag = tags[it];
        if (cnode->tag() == tag){
          children.erase( children.begin(), itr);
          if (cnode->completed){
            children.erase( itr);
          }
          return std::make_pair( cnode, it);
        }     
      }   
    }
     
    if (node->completed){
      node->children.clear();
      return std::make_pair( shared_ptr< Node>(), nt);
    }
    else{
      parse_some();
    }
  }
}
//#############################################
Node_Ptr Parser::next_child_with_tag( Node_Ptr node, const string& tag){  

  node->check_parser();
  Vector< string> tags{ tag};
  return next_child_with_tag( node, tags).first;
}

//#############################################
Node Node::copy() const{
  Node node;
  /*
  node._parser = _parser;
  node.attrs = attrs;
  std::copy( children.begin(), children.end(),
              std::back_inserter( node.children));
  node.completed = completed;
  node.line_num = line_num;
  node.ttag = ttag;
  node.sdata = sdata.copy();
  */
  copy_to( node);
  return node;
}

void Node::copy_to( Node& node) const{
  node._parser = _parser;
  node.attrs = attrs;
  std::copy( children.begin(), children.end(),
              std::back_inserter( node.children));
  node.completed = completed;
  node.line_num = line_num;
  node.ttag = ttag;
  node.sdata = sdata.copy();
}

//#########################################################
Node_Ptr Node::with_child_removed( const string& tag) const{
  
  check_parser();
  Node* new_ptr = new Node( copy());
  Node_Ptr node_ptr( new_ptr);
  
  auto& nchildren = node_ptr->children;
  auto itr = std::find_if( nchildren.begin(), nchildren.end(),
                       [&]( const Node_Ptr& node){
    return (node->tag() == tag);
  });
  
  if (itr == nchildren.end()){
    perror( "with_child_removed: not found, ", tag);
  }
  
  nchildren.erase( itr);
  return node_ptr;
}

//#########################################################
Node_Ptr Node::with_child_added_str( const string& tag, const string& data) const{
  
  check_parser();
  Node* new_ptr = new Node( copy());
  Node_Ptr node_ptr( new_ptr);
  
  auto& nchildren = node_ptr->children;
  
  auto new_child_ptr = new Node();
  new_child_ptr->ttag = tag;
  new_child_ptr->completed = true;
  new_child_ptr->sdata.push_back( data);
  nchildren.emplace_back( new_child_ptr);
  
  return node_ptr;
}

//####################################################
Node_Ptr Node::with_child_replaced_str( const string& ntag,
                                     const string& otag,
                                       const string& data) const{
  
  return with_child_removed( otag)->with_child_added( ntag, data);
}

//##############################################################
    [[maybe_unused]] Node_Ptr Node::with_child_replaced( const string& tag,
                                     const Node_Ptr& cnode) const{
  
  check( cnode);
  auto rnode = with_child_removed( tag);
  rnode->children.push_back( cnode);
  return rnode;
}

//##############################################################
    [[maybe_unused]] Node_Ptr Node::with_child_added( const Node_Ptr& cnode) const{
 
  check( cnode);
  check_parser();
  Node* new_ptr = new Node( copy());
  Node_Ptr node_ptr( new_ptr);
  
  node_ptr->children.push_back( cnode);
  return node_ptr;
}

//##############################################################
    [[maybe_unused]] void Node::add_attribute( const string& key, const string& val){
  check_parser();
  attrs.insert( key, val);
}

//##################################################
void Node::check( const Node_Ptr& ptr) const{
  
  if (!ptr){
    perror( "node pointer is null");
  }  
}

}}
