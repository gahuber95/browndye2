(*
Computes and outputs to standard output the bounding box of 
the molecule described by the pqrxml file in standard input
*)

let xmin = ref infinity;;
let xmax = ref neg_infinity;;
let ymin = ref infinity;;
let ymax = ref neg_infinity;;
let zmin = ref infinity;;
let zmax = ref neg_infinity;;

Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin
  ~f: (fun ~rname:rnam ~rnumber:rnum ~aname:anam ~anumber:anum 
    ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
    if x +. r > !xmax then
      xmax := x +. r;
    if x -. r < !xmin then
      xmin := x -. r;

    if y +. r > !ymax then
      ymax := y +. r;
    if y -. r < !ymin then
      ymin := y -. r;

    if z +. r > !zmax then
      zmax := z +. r;
    if z -.r < !zmin then
      zmin := z -. r;
  )
;;

let pr = Printf.printf;;

let avg x y = 0.5*.( !x +. !y);;

pr "<root>\n";
pr "  <low_corner> %g %g %g </low_corner>\n" !xmin !ymin !zmin;;
pr "  <high_corner> %g %g %g </high_corner>\n" !xmax !ymax !zmax;;
pr "  <lengths> %g %g %g </lengths>\n" 
  (!xmax -. !xmin) (!ymax -. !ymin) (!zmax -. !zmin);;
pr "  <center> %g %g %g </center>\n" 
  (avg xmin xmax) (avg ymin ymax) (avg zmin zmax)
;; 
pr "</root>\n";


