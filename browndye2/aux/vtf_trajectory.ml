(*
Converts a trajectory file into a VTF file suitable for animating with
VMD.  It takes the following arguments, with flags:
 
-trajf : trajectory file
-pqr : output trajectory of concatenated PQR files instead of VTF

bug: does not yet output bonds connecting chains to cores
*)



let do_pqr_ref = ref false;;

Arg.parse
  [
    ("-pqr", Arg.Set do_pqr_ref, "output trajectory of PQR files");
  ]
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Outputs trajectory in VTF format for viewing with VMD. Receives, through standard input, the output of process_trajectories."
;;

let do_pqr = !do_pqr_ref;;

let pr = Printf.printf;;
let sq x = x*.x;;

module SS = Scanf.Scanning;;
module V = Vec3;;
module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info ;;

type atom = {
  mutable pos: V.t;
  aname: string;
  rname: string;
  anumber: int;
  rnumber: int;
  radius: float;
  charge: float;
};;

let mv_prod mat v =    
  let bx = mat.(0).V.x*.v.V.x +. mat.(0).V.y*.v.V.y +. mat.(0).V.z*.v.V.z;
  and by = mat.(1).V.x*.v.V.x +. mat.(1).V.y*.v.V.y +. mat.(1).V.z*.v.V.z;
  and bz = mat.(2).V.x*.v.V.x +. mat.(2).V.y*.v.V.y +. mat.(2).V.z*.v.V.z
  in
    V.v3 bx by bz
;;

let parser = JP.new_parser SS.stdin;;

(*
type 'a option =
  | Some of 'a 
  | None
;;
 *)

let some_of opt =
  match opt with
  | Some a -> a
  | None -> raise (Failure "opt error")
;;

let found_node parser tag =
  while not ((JP.is_current_node_tag parser tag) || JP.finished parser) do 
    JP.next parser;
  done;

  if JP.finished parser then
    None
  else (
    JP.complete_current_node parser;
    Some (JP.current_node parser)
  )
;;

let yfound_node parser tag =
  some_of (found_node parser tag)
;;

let node_int node = 
  JP.int_from_stream (JP.node_stream node)
;;

let quat_to_mat quat = 
  let mat = [|
    V.v3 nan nan nan;
    V.v3 nan nan nan;
    V.v3 nan nan nan;
  |]
  in    
  let qw = quat.(0)
  and qx = quat.(1)
  and qy = quat.(2)
  and qz = quat.(3)
  in    
    mat.(0).V.x <- 1.0 -. 2.0*.qy*.qy -. 2.0*.qz*.qz;
    mat.(0).V.y <- 2.0*.qx*.qy -. 2.0*.qz*.qw;
    mat.(0).V.z <- 2.0*.qx*.qz +. 2.0*.qy*.qw;
    
    mat.(1).V.x <- 2.0*.qx*.qy +. 2.0*.qz*.qw;
    mat.(1).V.y <- 1.0 -. 2.0*.qx*.qx -. 2.0*.qz*.qz;
    mat.(1).V.z <- 2.0*.qy*.qz -. 2.0*.qx*.qw;
    
    mat.(2).V.x <- 2.0*.qx*.qz -. 2.0*.qy*.qw;
    mat.(2).V.y <- 2.0*.qy*.qz +. 2.0*.qx*.qw;
    mat.(2).V.z <- 1.0 -. 2.0*.qx*.qx -. 2.0*.qy*.qy;
    mat
;;


let tv t = 
  let x,y,z = t in
    V.v3 x y z
;;
    
let vec3 node = 
  let stm = JP.node_stream node in
    tv (JP.float3_from_stream stm)
;;

let force_model =
  let node = yfound_node parser "force_model_type" in
  let res = JP.string_from_stream (JP.node_stream node) in
  if not (res = "spline" || res = "molecular_mechanics" ||
       res = "ABSINTH") then
    raise (Failure (res^" force model is not defined"))
  else
    res
;;

let core_centers =
  let cms_node = yfound_node parser "core_centers" in
  let cm_nodes = JP.children cms_node "center" in
  List.map vec3 cm_nodes
;;

let atoms_of_file mol_file =
  let alist = ref [] in
    Atom_parser.apply_to_atoms ~buffer: (SS.from_file mol_file)
      ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	~x:x ~y:y ~z:z ~radius:r ~charge:q -> 
	let pos = V.v3 x y z in
	let a = {
	  pos = pos;
	  aname = aname;
	  rname = rname;
	  anumber = anumber;
	  rnumber = rnumber;
	  radius = r;
	  charge = q;
	}
	in
	  alist := a :: (!alist);
      );
    Array.of_list (List.rev !alist)
;;

type atoms_type = Core | Chain | Time;;
  
let atypes =
  let node = yfound_node parser "pattern" in
  let pnames = JP.strings_from_stream (JP.node_stream node) in
  List.map
    (fun name ->
      if name = "core" then
        Core
      else if name = "chain" then
        Chain
      else if name = "dt" then
        Time
      else
        raise (Failure "must be core or chain")
    )
    pnames
;;

let files0 =
  let node = yfound_node parser "atom_files" in
  JP.strings_from_stream (JP.node_stream node)
;;

(* need blank file to accomodate time tag *)
let files = "" :: files0;;

let atoms =
  let rec loop atypes files res =
    match atypes, files with
    | atype::rest_atypes, file::rest_files ->
       let catoms = 
         match atype with
         | Core -> atoms_of_file file
                                    
         | Chain ->
            let cparser = JP.new_parser (SS.from_file file) in
            let node = yfound_node cparser "atoms" in
            let file = JP.string_from_stream (JP.node_stream node) in
            atoms_of_file file 

         | Time ->
            [||]
       in
       loop rest_atypes rest_files (catoms::res)
    | [],[] -> res
    | _,_ -> raise (Failure "mismatch between files and pattern numbers")
  in
  List.rev (loop atypes files [])
;;

let assoc_opt a lst =
  let rec loop lst =
    match lst with
    | [] -> None
    | head::tail ->
       let ha,hb = head in
       if a = ha then
         Some hb
       else
         loop tail
  in
  loop lst
;;

let is_proper node =
  let attrs = JP.node_attributes node in
  let imp = assoc_opt "improper" attrs in 
  match imp with
  | None -> true
  | Some is_imp -> 
     not (is_imp = "true")
    
;;

let bonds =
  let rec loop atypes files res =
    match atypes, files with
    | atype::rest_atypes, file::rest_files ->
       let lbonds =
         match atype with
         | Core | Time -> [||]
         | Chain ->
            let fbnodes =
              let cparser = JP.new_parser (SS.from_file file) in
              let nodeo = found_node cparser "bonds" in
              match nodeo with
              | None -> []
              | Some node -> 
                 let bnodes = JP.children node "bond" in 
                 List.filter
                   (fun bnode ->
                     if (is_proper bnode) then
                       let cnodes = JP.children bnode "cores" in
                       match cnodes with
                       | [] -> true
                       | _ -> false
                     else                       
                       false
                   )
                   bnodes
                 
            and cnodes0 =
              let cparser = JP.new_parser (SS.from_file file) in
              let node_opt = found_node cparser "length_constraints" in
              match node_opt with
              | Some node -> JP.children node "length_constraint"
              | None -> []
            in
            let cnodes =
              List.filter
                (fun cnode ->            
                   if (is_proper cnode) then
                     let cnodes = JP.children cnode "cores" in
                     match cnodes with
                     | [] -> true
                     | _ -> false
                   else
                     false
                )
                cnodes0
            in
            let cbnodes = fbnodes @ cnodes in
            Array.of_list @@
              List.map
                (fun bnode ->
                  match JP.child bnode "atoms" with
                  | Some anode ->
                     let strm = JP.node_stream anode in
                     let a0 = JP.int_from_stream strm in
                     let a1 = JP.int_from_stream strm in
                     a0,a1
                  | None -> raise (Failure "atoms not found in bond")
                )
                cbnodes
       in
       loop rest_atypes rest_files (lbonds::res)
    | [],[] -> res
    | _,_ -> raise (Failure "mismatch between files and pattern numbers")
  in
  List.rev (loop atypes files [])
;;

let pr_header () =
  let atom_imap = Hashtbl.create 0 in
  let afind x = Hashtbl.find atom_imap x in
  let anum = ref 0 in
  let cnum = ref 0 in
  List.iter
    (fun sub_atoms ->
      Array.iter
        (fun atom ->
          Hashtbl.add atom_imap (!cnum,atom.anumber) !anum;
          anum := !anum + 1;
        )
        sub_atoms;
      cnum := !cnum + 1;
    )
    atoms;

  cnum := 0;
  List.iter
    (fun sub_atoms -> 
      Array.iter
        (fun atom ->
          let ia = afind (!cnum, atom.anumber) in
          pr "a %d t %s res %s r %f\n" ia atom.aname atom.rname atom.radius; 
        )
        sub_atoms;
      cnum := !cnum + 1;           
    )
    atoms;

  cnum := 0;
  List.iter
    (fun sub_bonds ->
      Array.iter
        (fun bond ->
          let i0,i1 = bond in
          let b0 = afind (!cnum, i0) in
          let b1 = afind (!cnum, i1) in
          pr "b %d:%d\n" b0 b1;
        )
        sub_bonds;
      cnum := !cnum + 1;
    )
    bonds
;;



if not do_pqr then(
  pr_header();
);;

let translate_atoms atoms c =
  Array.iter
    (fun atom ->
      atom.pos <- V.vdiff atom.pos c
    )
    atoms
;;

let rec trans_loop atypes centers atoms =
  match atypes,atoms with
  | atype::rest_atype, atoma::rest_atoms -> (
     match atype with
     | Core -> (
        match centers with
        | center :: rest_centers ->
           translate_atoms atoma center;
           trans_loop rest_atype rest_centers rest_atoms;
        | [] -> raise (Failure "not enough core centers")
     )
     | Chain | Time ->
        trans_loop rest_atype centers rest_atoms;
  )
  | [],[] -> ()
  | _,_ ->
     raise (Failure "number of atom files and types do not match")
;;



(**************************************************************)
let print_atom_pqr atom pos = 

  let aname = atom.aname in
    
  let apre,amid,apos = 
    match String.length aname with
      | 1 -> " ", aname, "  "
      | 2 -> " ", aname, " "
      | 3 -> 
          let first = aname.[0] in
            if 'A' <= first && first <= 'Z' then
              " ", aname,""
            else
              "", aname, " "
      | 4 -> "", aname, ""
      | _ -> raise (Failure "should not get here")
  in
  let output = 
    Printf.sprintf "ATOM %6d %s%s%s %-3s%6d     %7.3f %7.3f %7.3f %7.4f %7.3f\n" 
      atom.anumber apre amid apos atom.rname atom.rnumber 
      pos.V.x pos.V.y pos.V.z atom.charge atom.radius
  in
    Printf.printf "%s" output
;;

(*************************************************************)
trans_loop atypes core_centers atoms;;
  
JP.go_up parser;;
JP.apply_to_nodes_of_tag
  parser "state"
  (fun node ->
    JP.complete_current_node parser;
    let anodes = JP.all_children node in

    let rec loop anodes atoms = (
      match anodes, atoms with
      | anode::rest_nodes, sub_atoms::rest_atoms ->
         let tag = JP.node_tag anode in

         let strm = JP.node_stream anode in

         if tag = "chain" then (
           let coords = JP.floats_from_stream strm in
           if do_pqr then(
             Array.iteri
               (fun i atom ->
                 let i3 = 3*i in
                 let pos = V.v3 coords.(i3) coords.(i3+1) coords.(i3+2) in
                 print_atom_pqr atom pos;
               )
               sub_atoms;
           )
           else(
             let n3 = Array.length coords in
             let n = n3/3 in
             for i = 0 to n-1 do
               let i3 = 3*i in
               pr "%g %g %g\n" coords.(i3) coords.(i3+1) coords.(i3+2);
             done;
           );
           
           loop rest_nodes rest_atoms (* should be here? *)
         )
         else if tag = "core" then
           let info = JP.floats_from_stream strm in
           let x,y,z = info.(0), info.(1), info.(2) in
           let trans = V.v3 x y z in
           let q0,q1,q2,q3 = info.(3+0),info.(3+1),info.(3+2),info.(3+3) in
           let q = [| q0;q1;q2;q3 |] in
           let rot = quat_to_mat q in
           Array.iter
             (fun atom ->
               let pos1 = mv_prod rot atom.pos in
               let pos = V.vsum trans pos1 in
               if do_pqr then (
                 print_atom_pqr atom pos;
               )
               else (
                 pr "%g %g %g\n" pos.V.x pos.V.y pos.V.z;
               )
             )
             sub_atoms;
           
           loop rest_nodes rest_atoms

         else if tag = "dt" then(
           loop rest_nodes rest_atoms
         )
         else
           raise (Failure "must be core, chain, or dt")
                 
      | [],[] ->
         if do_pqr then
           pr "TER\n";
         
      | _,_ -> raise (Failure "mismatched number of nodes and atom arrays")
             
      )
                              
    in
    
    if not do_pqr then
      pr "timestep\n";
    
    loop anodes atoms;              
  )
;;

(*  
let mbyte = 1 lsl 20;;

if not to_out then
  pr "Estimated file size: %iM\n" (!nc_ref/mbyte);
 *)
 


