#pragma once

// based on https://projectivegeometricalgebra.org/

#include <iostream> 
#include <utility>
#include <cmath>

namespace Geom_Alg{

typedef unsigned int uint;
using std::make_pair;
using std::pair;
using std::sqrt;

constexpr uint ntop = 4;

class None{};

template< int v>
struct IC{
    static constexpr int value = v;
    using value_type = int;
};

typedef IC<0> I0;

constexpr I0 i0;
constexpr IC<1> i1;
constexpr IC<-1> im1;

//###################################################
constexpr None operator*( None, None){
  return None{};
}

template< class T>
constexpr None operator*( None, T){
  return None{};
}

template< class T>
constexpr None operator*( T, None){
  return None{};
}

template< class T, int n>
auto operator*( T t, IC< n>){
  return t*n;
}

template< class T, int n>
auto operator*( IC< n>, T t){
  return n*t;
}

template< int n>
constexpr None operator*( IC< n>, None){
  return None{};
}

template< int n>
constexpr None operator*( None, IC< n>){
  return None{};
}

template< int n0, int n1>
constexpr auto operator*( IC< n0>, IC< n1>){
  return IC< n0*n1>{};
}

template< int n>
constexpr None operator*( IC<n>, IC<0>){
  return None{};
}

template< int n>
constexpr None operator*( I0, IC<n>){
  return None{};
}

template< class T>
constexpr None operator*( T, I0){
  return None{};
}

template< class T>
constexpr None operator*( I0, T){
  return None{};
}

constexpr None operator*( None, I0){
  return None{};
}

constexpr None operator*( I0, None){
  return None{};
}

//###################################################
constexpr uint n_bits_below( uint i, uint a){

  if (i == 0){
    return 0;
  }
  else{
    auto im = i-1;
    auto prev = n_bits_below( i-1, a);
    auto inc =  ((1 << im) & a) >> im;
    return prev + inc;
  }
}

//###########################################
constexpr uint n_inversions( uint a, uint b){
  
  uint n = 0;
  for( int i = 0; i < 4; i++){
    if (((1 << i) & a) > 0){
      n += n_bits_below( i, b);
    }
  }
  return n;
}

//###################################
constexpr int power( uint n, int i){
  
  if (n == 0){
    return 1;
  }
  else if (n % 2 == 0){
    auto th = power( n/2, i);
    return th*th;
  }
  else{
    return i*power( n-1, i);
  }
}

//#################################################
constexpr auto sign_inversions( uint a, uint b){
  auto ni = n_inversions( a,b);
  return power( ni, -1);
}

//##############################################
constexpr auto wedge( uint a, uint b){
    
  if (a == 0 && b == 0){
    return make_pair( 0u, 1);
  }
  else if ((a & b) > 0){
    return make_pair( 0u, 0);
  }
  else{
    auto res = a | b;
    auto sgn = sign_inversions( a,b);
    return make_pair( res, sgn);
  }
}

//###########################################
constexpr bool has_two_4( uint a, uint b){
  
  const uint mask = 0b1000;
  return ((a & mask) > 0) && ((b & mask) > 0);
}

constexpr uint gdot( uint a, uint b){
  
  if ((a & b) > 0){
    return has_two_4( a,b) ? 0u : 1u;
  }
  else{
    return 0u;
  }
}


//############################################
constexpr pair< uint, int> geometric( uint a, uint b){
    
  if (a == 0 && b == 0){
    return make_pair( 0u, 1);
  }
  else if (has_two_4( a,b)){
    return make_pair( 0u, 0);
  }
  else{
    auto ni = n_inversions( a,b);
    auto sgn = power( ni, -1);
    auto res = a ^ b;
    
    if (res == 0){ // if same
      auto gd = gdot( a,b);
      return make_pair( 0u, sgn*gd);
    }
    else{
      return make_pair( res, sgn);
    }
  }
}

//#################################################
constexpr uint grade2( uint n){
    
  if (n == 0){
    return 0;
  }
  else if (n == 1 || n == 2){
    return 1;
  }
  else{
    return 2;
  }
}

constexpr uint grade( uint n){
  uint hin = n & 3;
  uint lon = n >> 2;
  return grade2( lon) + grade2( hin);
}

constexpr uint antigrade( uint n){
  
  return ntop - grade( n);
}

//############################################
constexpr uint iabs( int a){
  if (a > 0){
    return a;
  }
  else{
    return -a;
  }
}

//###########################################################
constexpr auto dot( uint a, uint b){

  auto rs = geometric( a, b);
  auto res = rs.first;
  
  if (iabs( (int)grade(a) - (int)grade(b)) == grade( res)){
    return rs;
  }
  else{
    return make_pair( 0u, 0);
  }
}
  

//#############################################
constexpr uint elem(){
  return 0;
}

constexpr uint elem( uint a){
  return 1 << (a-1);
}

constexpr uint elem( uint a, uint b){
  return elem( a) | elem( b);
}

constexpr uint elem( uint a, uint b, uint c){
  return elem( a,b) | elem( c);
}

constexpr uint elem( uint a, uint b, uint c, uint d){
  return elem( a,b,c) | elem( d);
}

//##########################################################
template< uint level, bool em0, bool em1, class T0, class T1>
class SVec;

template< uint level, class T0, class T1>
class SVec< level, false, false, T0, T1>{
public:
  typedef T0 Sub0;
  typedef T1 Sub1;
  
  static constexpr bool empty0 = false;
  static constexpr bool empty1 = false;
  
  Sub0 sub0;
  Sub1 sub1;
  
  auto subv0() const{
    return sub0;
  }
  
  auto subv1() const{
    return sub1;
  }
};

template< uint level, class T0, class T1>
class SVec< level, false, true, T0, T1>{
public:
  typedef T0 Sub0;
  typedef T1 Sub1;

  static constexpr bool empty0 = false;
  static constexpr bool empty1 = true;
  
  Sub0 sub0;
  
  auto subv0() const{
    return sub0;
  }
  
  auto subv1() const{
    return Sub1{};
  }
};

template< uint level, class T0, class T1>
class SVec< level, true, false, T0, T1>{
public:
  typedef T0 Sub0;
  typedef T1 Sub1;
  
  static constexpr bool empty0 = true;
  static constexpr bool empty1 = false;
  
  Sub1 sub1;
  
  auto subv0() const{
    return Sub0{};
  }
  
  auto subv1() const{
    return sub1;
  }
};

template< uint level, class T0, class T1>
class SVec< level, true, true, T0, T1>{
public:
  typedef T0 Sub0;
  typedef T1 Sub1;
  
  static constexpr bool empty0 = true;
  static constexpr bool empty1 = true;
  
  auto subv0() const{
    return Sub0{};
  }
  
  auto subv1() const{
    return Sub1{};
  }
};

//#########################################################
template< class T>
inline constexpr uint level_of = 0;

template< uint level, class T0, class T1, bool e0, bool e1>
inline constexpr uint level_of< SVec< level, e0, e1, T0, T1> > = level;

template< class V0, class V1>
auto new_svec( V0 v0, V1 v1){
  
  constexpr auto level = std::max( level_of< V0>, level_of< V1>) + 1;
  
  constexpr bool empty0 = std::is_empty_v< V0>;
  constexpr bool empty1 = std::is_empty_v< V1>;
  
  typedef SVec< level, empty0, empty1, V0, V1> SV;
  
  if constexpr (empty0){
    if constexpr (empty1){
      return SV{};
    }
    else{
      return SV{ v1};
    }
  }
  else{
    if constexpr (empty1){
      return SV{ v0};
    }
    else{
      return SV{ v0, v1};
    }
  }
  
}

//##############################################################
template< uint n>
None elementi( None){
  return None{};
}

template< uint n, uint level, bool em0, bool em1, class T0, class T1>
auto elementi( SVec< level, em0, em1, T0, T1> v){
  
  static_assert( level > 0, "level must be > 0");
  
  if constexpr (level == 1){
    if constexpr (n == 0){
      return v.subv0();
    }
    else{
      return v.subv1();
    }
  }
  else{
    constexpr auto mask = 1 << (level-1);
    constexpr auto subn = (~mask) & n;
    if constexpr ((mask & n) == 0){
      return elementi< subn>( v.subv0());
    }
    else{
      return elementi< subn>( v.subv1());
    }
  }
}

//##########################################
// assume 1-based indices
template< uint... ns>
class E{};

template<>
class E<>{
public:
  static constexpr uint value = 0;
};

template< uint n0, uint... ns>
class E< n0, ns...>{
public:
  static_assert( n0 <= 4, "must be <= 4");
  static_assert( n0 > 0, "must be > 0");
  
  static constexpr uint value = (1 << (n0-1)) + E< ns...>::value;
};

//##############################################################
template< uint... ns, uint level, bool em0, bool em1, class T0, class T1>
auto element( SVec< level, em0, em1, T0, T1> v){
  
  constexpr auto idx = E< ns...>::value;
  return elementi< idx>( v);
}

template< uint... ns>
None element( None){
  return None{};
}

//##############################
template< class T>
T operator+( T t, None){
  return t;
}

template< class T>
T operator+( None, T t){
  return t;
}

template< int n0, int n1>
auto operator+( IC< n0>, IC< n1>){

  constexpr int n = n0 + n1;
  if constexpr (n == 0){
    return None{};
  }
  else{
    return IC< n>{};
  }
}

template< int n, class T>
auto operator+( IC< n>, T t){
  
  return n + t;
}

template< int n, class T>
auto operator+( T t, IC< n>){
  
  return t + n;
}


template< int n>
auto operator+( IC< n>, None){
  
  return IC< n>{};
}

template< int n>
auto operator+( None, IC< n>){
  
  return IC< n>{};
}


//##############################
template< class T>
T operator-( T t, None){
  return t;
}

template< class T>
T operator-( None, T t){
  return im1*t;
}
  
template< int n0, int n1>
auto operator-( IC< n0>, IC< n1>){

  constexpr int n = n0 - n1;
  if constexpr (n == 0){
    return None{};
  }
  else{
    return IC< n>{};
  }
}

template< int n, class T>
auto operator-( IC< n>, T t){
  
  return n - t;
}

template< int n, class T>
auto operator-( T t, IC< n>){
  
  return t - n;
}

template< int n>
auto operator-( IC< n>, None){
  
  return IC< n>{};
}

template< int n>
auto operator-( None, IC< n>){
  
  return IC< -n>{};
}

//##############################
template< class T>
inline constexpr bool is_nothing = false;

template<>
inline constexpr bool is_nothing< None> = true;

template<>
inline constexpr bool is_nothing< I0> = true;

//##############################
template< uint n, uint level, class A>
auto placed( None, A a){
  
  static_assert( level > 0, "level must be > 0");  

  if constexpr (is_nothing<A>){
    return None{};
  }
  
  else if constexpr (level == 1){
    if constexpr (n == 0){
      return new_svec( a, None{});
    }
    else{
      return new_svec( None{}, a);
    }
  }
  
  else{
    constexpr auto mask = 1 << (level-1);
    constexpr auto subn = (~mask) & n;
    constexpr bool is0 = (mask & n) == 0;
    constexpr auto levelm = level-1;
    
    if constexpr (is0){
      auto newv = placed< subn, levelm>( None{}, a);
      return new_svec( newv, None{});
    }
    else{
      auto newv = placed< subn, levelm>( None{}, a);
      return new_svec( None{}, newv);
    }  
  }
}

template< uint n, uint level, bool em0, bool em1, class T0, class T1>
auto placed( SVec< level, em0, em1, T0, T1> v, None){
  return v;
}

template< uint n, uint level, bool em0, bool em1, class T0, class T1>
auto placed( SVec< level, em0, em1, T0, T1> v, I0){
  return v;
}


template< uint n, uint level, bool em0, bool em1, class T0, class T1, class A>
auto placed( SVec< level, em0, em1, T0, T1> v, A a){
    
  static_assert( level > 0, "level must be > 0");  
    
  if constexpr (level == 1){
    if constexpr (n == 0){
      return new_svec( a, v.subv1());
    }
    else{
      return new_svec( v.subv0(), a); 
    }
  }
  
  else{
    constexpr auto mask = 1 << (level-1);
    constexpr auto subn = (~mask) & n;
    constexpr bool is0 = (mask & n) == 0;
    constexpr auto levelm = level-1;
    
    if constexpr (is0){
      auto newv = placed< subn, levelm>( v.subv0(), a);
      return new_svec( newv, v.subv1());
    }
    else{
      auto newv = placed< subn, levelm>( v.subv1(), a);
      return new_svec( v.subv0(), newv);
    }      
  } 
}

//##############################
template< class Idx, class T>
auto new_sparse_vector( Idx, T t){
  
  return placed< Idx::value, ntop>( None{}, t);
}

template< class Idx, class T, class... Rest>
auto new_sparse_vector( Idx, T t, Rest... rest){
  
  auto v0 = new_sparse_vector( rest...);
  return placed< Idx::value, ntop>( v0, t);
}

//##############################
None operator+( None, None){
  return None{};
}

template< uint level, bool em0, bool em1, class T0, class T1>
auto operator+( SVec< level, em0, em1, T0, T1> va, None){
  return va;
}

template< uint level, bool em0, bool em1, class T0, class T1>
auto operator+( None, SVec< level, em0, em1, T0, T1> va){
  return va;
}

template< uint levela, bool ema0, bool ema1, class Ta0, class Ta1,
          uint levelb, bool emb0, bool emb1, class Tb0, class Tb1>
auto operator+( SVec< levela, ema0, ema1, Ta0, Ta1> va,
                 SVec< levelb, emb0, emb1, Tb0, Tb1> vb){
  
  auto sv0 = va.subv0() + vb.subv0();
  auto sv1 = va.subv1() + vb.subv1();
  return new_svec( sv0, sv1);
}

//##############################
None operator-( None, None){
  return None{};
}

template< uint level, bool em0, bool em1, class T0, class T1>
auto operator-( SVec< level, em0, em1, T0, T1> va, None){
  return va;
}

template< uint level, bool em0, bool em1, class T0, class T1>
auto operator-( None, SVec< level, em0, em1, T0, T1> va){
  return im1*va;
}

template< uint levela, bool ema0, bool ema1, class Ta0, class Ta1,
          uint levelb, bool emb0, bool emb1, class Tb0, class Tb1>
auto operator-( SVec< levela, ema0, ema1, Ta0, Ta1> va,
                 SVec< levelb, emb0, emb1, Tb0, Tb1> vb){
  
  auto sv0 = va.subv0() - vb.subv0();
  auto sv1 = va.subv1() - vb.subv1();
  return new_svec( sv0, sv1);
}

  
//##############################
None operator*( double, None){
  return None{};
}

/*
template< int n>
None operator*( IC<n>, None){
  return None{};
}

None operator*( None, None){
  return None{};
}
*/
    
template< uint levela, bool ema0, bool ema1, class Ta0, class Ta1>
auto operator*( double x, SVec< levela, ema0, ema1, Ta0, Ta1> va){
  
  auto sv0 = x*va.subv0();
  auto sv1 = x*va.subv1();
  return new_svec( sv0, sv1);
}

//############################################################
template< class T>
auto half( T t){
  return t/2;
}
  
template< int n>
auto half( IC<n>){
  if constexpr (n%2 == 0){
    return IC< n/2>{};
  }
  else{
    return 0.5*n;
  }
}
  
None half( None){
  return None{};
}
  
template< uint levela, bool ema0, bool ema1, class Ta0, class Ta1>
auto half( SVec< levela, ema0, ema1, Ta0, Ta1> va){
  
  auto sv0 = half( va.subv0());
  auto sv1 = half( va.subv1());
  return new_svec( sv0, sv1);
}
  
  
//############################################################
template< class Op, uint na, uint nb, uint level,
          bool em0, bool em1, class T0, class T1>
auto gen_prodi( SVec< level, em0, em1, T0, T1> v, None){

  return None{};
}

template< class Op, uint na, uint nb, uint level,
          bool em0, bool em1, class T0, class T1>
auto gen_prodi( None, SVec< level, em0, em1, T0, T1> v){

  return None{};
}

template< class Op, uint na, uint nb, uint level>
None gen_prodi( None, None){
  return None{};
}


template< int N, class T0, class T1>
auto choice( T0 t0, T1 t1){
  if constexpr (N == 0){
    return t0;
  }
  else{
    return t1;
  }
}

template< class Op, uint na, uint nb, uint level,
          bool ema0, bool ema1, class Ta0, class Ta1,
          bool emb0, bool emb1, class Tb0, class Tb1>
auto gen_prodi( SVec< level, ema0, ema1, Ta0, Ta1> va,
                 SVec< level, emb0, emb1, Tb0, Tb1> vb){

  constexpr uint na0 = na << 1;
  constexpr uint na1 = na0 + 1;
  constexpr uint nb0 = nb << 1;
  constexpr uint nb1 = nb0 + 1;
   
  if constexpr (level == 1){ 
    constexpr auto p00 = Op::result( na0, nb0);
    constexpr auto p01 = Op::result( na0, nb1);
    constexpr auto p10 = Op::result(na1, nb0);
    constexpr auto p11 = Op::result( na1, nb1);
    
    constexpr int s00 = p00.second;
    constexpr int s01 = p01.second;
    constexpr int s10 = p10.second;
    constexpr int s11 = p11.second;
    
    constexpr IC< s00> is00;
    constexpr IC< s01> is01;
    constexpr IC< s10> is10;
    constexpr IC< s11> is11;
    
    constexpr uint g00 = p00.first;
    constexpr uint g01 = p01.first;
    constexpr uint g10 = p10.first;
    constexpr uint g11 = p11.first;
    
    auto r00 = choice< s00>( None{}, is00*va.subv0()*vb.subv0());
    auto r01 = choice< s01>( None{}, is01*va.subv0()*vb.subv1());
    auto r10 = choice< s10>( None{}, is10*va.subv1()*vb.subv0());
    auto r11 = choice< s11>( None{}, is11*va.subv1()*vb.subv1());
   
    auto v00 = placed< g00, ntop>( None{}, r00);
    auto v01 = placed< g01, ntop>( None{}, r01);
    auto v10 = placed< g10, ntop>( None{}, r10);
    auto v11 = placed< g11, ntop>( None{}, r11);
     
    return v00 + v01 + v10 + v11;
  }  
  else{
    constexpr auto levelm = level-1;
    auto v00 = gen_prodi< Op, na0, nb0, levelm>( va.subv0(), vb.subv0());
    auto v01 = gen_prodi< Op, na0, nb1, levelm>( va.subv0(), vb.subv1());
    auto v10 = gen_prodi< Op, na1, nb0, levelm>( va.subv1(), vb.subv0());    
    auto v11 = gen_prodi< Op, na1, nb1, levelm>( va.subv1(), vb.subv1());
    
    return v00 + v01 + v10 + v11;
  }
}

template< class Op, class V0, class V1>
auto gen_prod( V0 v0, V1 v1){
  return gen_prodi< Op, 0,0>( v0, v1);
}

//#################################################
class Geo_Op{
public:

  static
  constexpr auto result( uint a, uint b){
    return geometric( a,b); 
  }
};

template< class V0, class V1>
auto geo_prod( V0 v0, V1 v1){
  
  return gen_prod< Geo_Op>( v0, v1);
}

//#################################################
class Ext_Op{
public:
  
  static
  constexpr auto result( uint a, uint b){
    return wedge( a,b); 
  }
};

template< class V0, class V1>
auto ext_prod( V0 v0, V1 v1){
  
  return gen_prod< Ext_Op>( v0, v1);
}

//#################################################
class Dot_Op{
public:

  static
  constexpr auto result( uint a, uint b){
    return dot( a,b);
  }
};

template< class V0, class V1>
auto dot_prod( V0 v0, V1 v1){
  
  return gen_prod< Dot_Op>( v0, v1);
}
  

//##############################
constexpr auto reverse_sign( uint n){
  
  auto g = grade( n);
  auto c = (g*(g-1))/2;
  return power( c, -1);
}

constexpr auto antireverse_sign( uint n){
  
  auto g = antigrade( n);
  auto c = (g*(g-1))/2;
  return power( c, -1);
}

//##############################
constexpr auto right_complement( uint n){
  
  constexpr uint mask = 0b1111;
  auto res = mask & (~n);
  auto rsgn = sign_inversions( n, res);
  return make_pair( res, rsgn);
}

constexpr pair< uint, int> left_complement( uint n){
  
  constexpr uint mask = 0b1111;
  auto res = mask & (~n);
  auto rsgn = sign_inversions( res, n);
  return make_pair( res, rsgn);
 
}

//##############################
constexpr auto reverse( uint n){
  
  auto rsgn = reverse_sign( n);
  return make_pair( n, rsgn);
}

constexpr auto anti_reverse( uint n){
  
  auto rsgn = antireverse_sign( n);
  return make_pair( n, rsgn);
}
//#################################################
template< class Op, uint n, uint level>
None gen_funi( None){
  return None{};
}

template< class Op, uint n, uint level,
          bool em0, bool em1, class T0, class T1>
auto gen_funi( SVec< level, em0, em1, T0, T1> v){
  
  constexpr uint n0 = n << 1;
  constexpr uint n1 = n0 + 1;
     
  if constexpr (level == 1){ 
    constexpr auto p0 = Op::result( n0);
    constexpr auto p1 = Op::result( n1);
    
    constexpr int s0 = p0.second;
    constexpr int s1 = p1.second;
    
    constexpr IC< s0> is0;
    constexpr IC< s1> is1;
    
    constexpr uint g0 = p0.first;
    constexpr uint g1 = p1.first;
    
    auto r0 = choice< s0>( None{}, is0*v.subv0());
    auto r1 = choice< s1>( None{}, is1*v.subv1());
   
    auto v0 = placed< g0, ntop>( None{}, r0);
    auto v1 = placed< g1, ntop>( None{}, r1);
     
    return v0 + v1;
  }  
  else{
    constexpr auto levelm = level-1;
    auto v0 = gen_funi< Op, n0, levelm>( v.subv0());
    auto v1 = gen_funi< Op, n1, levelm>( v.subv1());
    
    return v0 + v1;
  }
}

template< class Op, class V>
auto gen_fun( V v){
  return gen_funi< Op, 0>( v);
}
//#################################################
class RComp{
public:

  static
  constexpr auto result( uint a){
    return right_complement( a); 
  }
};

template< class V>
auto rcomp( V v){
  
  return gen_fun< RComp>( v);
}

//##############################
class LComp{
public:

  static
  constexpr auto result( uint a){
    return left_complement( a); 
  }
};

template< class V>
auto lcomp( V v){
  
  return gen_fun< LComp>( v);
}

//##############################
class Reverse{
public:

  static
  constexpr auto result( uint a){
    return reverse( a); 
  }
};

template< class V>
auto rev( V v){
  
  return gen_fun< Reverse>( v);
}

//##############################
class Anti_Reverse{
public:

  static
  constexpr auto result( uint a){
    return anti_reverse( a); 
  }
};

template< class V>
auto anti_rev( V v){
  
  return gen_fun< Anti_Reverse>( v);
}

//#################################################
typedef pair< uint, int> (*GProd)( uint, uint);

template< GProd gprod>
constexpr auto anti_product( uint a, uint b){
  
  auto ra_res = right_complement( a);
  auto ra = ra_res.first;
  auto sa = ra_res.second;
  
  auto rb_res = right_complement( b);
  auto rb = rb_res.first;
  auto sb = rb_res.second;
  
  auto wres = gprod( ra,rb);
  auto w = wres.first;
  auto sw = wres.second;
  
  auto cres = left_complement( w);
  auto c = cres.first;
  auto sc = cres.second;
  
  auto sgn = sa*sb*sw*sc;
  return make_pair( c, sgn);
}

constexpr auto antiwedge( uint a, uint b){
  return anti_product< wedge>( a,b);
}

constexpr auto antigeometric( uint a, uint b){
  return anti_product< geometric>( a,b);
}

constexpr auto antidot( uint a, uint b){
  return anti_product< dot>( a,b);
}

/*
constexpr auto antiwedge( uint a, uint b){
  
  auto ra_res = right_complement( a);
  auto ra = ra_res.first;
  auto sa = ra_res.second;
  
  auto rb_res = right_complement( b);
  auto rb = rb_res.first;
  auto sb = rb_res.second;

  auto wres = wedge( ra,rb);
  auto w = wres.first;
  auto sw = wres.second;
  
  auto cres = left_complement( w);
  auto c = cres.first;
  auto sc = cres.second;
  
  auto sgn = sa*sb*sw*sc;
  return make_pair( c, sgn);
}

//#################################################
constexpr auto antigeometric( uint a, uint b){
  
  auto ra_res = right_complement( a);
  auto ra = ra_res.first;
  auto sa = ra_res.second;
  
  auto rb_res = right_complement( b);
  auto rb = rb_res.first;
  auto sb = rb_res.second;

  auto wres = geometric( ra,rb);
  auto w = wres.first;
  auto sw = wres.second;
  
  auto cres = left_complement( w);
  auto c = cres.first;
  auto sc = cres.second;
  
  auto sgn = sa*sb*sw*sc;
  return make_pair( c, sgn);
}
*/


//###########################################
class Ext_Anti_Op{
public:
  
  static
  constexpr auto result( uint a, uint b){
    return antiwedge( a,b); 
  }
};

template< class V0, class V1>
auto ext_anti_prod( V0 v0, V1 v1){
  
  return gen_prod< Ext_Anti_Op>( v0, v1);
}

//###########################################
class Geo_Anti_Op{
public:
  
  static
  constexpr auto result( uint a, uint b){
    return antigeometric( a,b); 
  }
};

template< class V0, class V1>
auto geo_anti_prod( V0 v0, V1 v1){
  
  return gen_prod< Geo_Anti_Op>( v0, v1);
}

//###########################################
class Dot_Anti_Op{
public:
  
  static
  constexpr auto result( uint a, uint b){
    return antigeometric( a,b); 
  }
};

template< class V0, class V1>
auto dot_anti_prod( V0 v0, V1 v1){
  
  return gen_prod< Dot_Anti_Op>( v0, v1);
}


//##################################################
template< uint n>
constexpr None scaledi( double, None){
  return None{};
}

template< uint n, int i>
constexpr None scaledi( IC<i>, None){
  return None{};
}

template< uint n, class S, uint level, bool em0, bool em1, class T0, class T1>
auto scaledi( S s, SVec< level, em0, em1, T0, T1> v){
  
  constexpr uint n0 = n << 1;
     
  if constexpr (level == 1){ 
    auto p0 = v.subv0();
    auto p1 = v.subv1();
    
    constexpr bool is_p0 = !is_nothing< decltype( p0)>;
    constexpr bool is_p1 = !is_nothing< decltype( p1)>;
    
    if constexpr (is_p0 && is_p1){
      constexpr uint n1 = n0 + 1;
      
      auto v0 = new_sparse_vector( n0, s*p0);
      auto v1 = new_sparse_vector( n1, s*p1);
      return v0 + v1;
    } 
    else if constexpr( is_p0 && !is_p1){
      return new_sparse_vector( IC<n0>{}, s*p0);
    }
    else if constexpr ( !is_p0 && is_p1){
      constexpr uint n1 = n0 + 1;
      return new_sparse_vector( IC<n1>{}, s*p1);
    }
    else{
      return None{};
    }
  }  
  else{
    constexpr uint n1 = n0 + 1;
    auto v0 = scaledi< n0>( s, v.subv0());
    auto v1 = scaledi< n1>( s, v.subv1());
    return v0 + v1;
  }
}

template< class S, class V>
auto scaled( S s, V v){
  return scaledi< 0>( s, v);
}

//#############################################
constexpr auto uweight( uint a){
  
  if ((a & 0b1000) == 0){
    return make_pair( a, 0);
  }
  else{
    return make_pair( a, 1);
  }
}

class Weight{
public:
  static
  constexpr auto result( uint a){
    return uweight( a);
  }
};

template< class V>
auto weight( V v){
  
  return gen_fun< Weight>( v);
}

//#############################################
constexpr auto ubulk( uint a){
  
  if ((a & 0b1000) == 0){
    return make_pair( a, 1);
  }
  else{
    return make_pair( a, 0);
  }
}

class Bulk{
public:
  static
  constexpr auto result( uint a){
    return ubulk( a);
  }
};

template< class V>
auto bulk( V v){
  
  return gen_fun< Bulk>( v);
}
//#########################################
/*
double inv_sqrt( double x){
  return 1.0/sqrt(x);
}

IC<1> inv_sqrt( IC<1>){
  return i1;
}

template< int n>
double inv_sqrt( IC<n>){
  return 1.0/sqrt( (double)n);
}
*/
template< int n>
double sqrt( IC<n>){
  return std::sqrt( (double)n);
}

double sqrt( IC<1>){
  return 1.0;
}

// returns coefficient of 1234 element
template< class V>
auto weight_norm( V v){
  
  auto w2 = dot_anti_prod( v, anti_rev(v));
  auto c = element< 1,2,3,4>( w2);
  return sqrt( c);
}

template< class V>
auto bulk_norm( V v){
  
  auto w2 = dot_prod( v, rev(v));
  double c = element< >( w2);
  return sqrt( c);
}

/*
double inv( double x){
  return 1.0/x;
}

IC<1> inv( IC<1>){
  return i1;
}
*/

template< class V>
auto unitized( V v){
  
  auto w = weight_norm( v);
  if constexpr (std::is_same_v< decltype(w), IC<1> >){
    return v;
  }
  else{
    return scaled( 1.0/w, v);
  }
}

//#################################################
template< class T1, class T2, class T3>
auto point( T1 t1, T2 t2, T3 t3){
  
  return new_sparse_vector( E<1>{}, t1, E<2>{}, t2,
                           E<3>{}, t3, E<4>{}, i1);
}  

  // line from a to b
template< class Va, class Vb>
auto line_of_points( Va pa, Vb pb){
  
  return ext_prod( pb, pa);
}

// two planes following each other ccw, along with their
// directions, give a positive line. Same handedness as cross product.
template< class Va, class Vb>
auto line_of_planes( Va pa, Vb pb){
  
  return ext_anti_prod( pa, pb);
}

template< class VL, class Vf>
auto point_of_line_and_plane( VL L, Vf f){

  return ext_anti_prod( L, f);
}

template< class Vp, class VL>
auto plane_with_point_and_perp_to_line( Vp p, VL L){
  
  return ext_prod( lcomp( weight( L)), p);
}

template< class Va, class Vb>
auto plane_of_point_and_line( Va pa, Vb pb){
  
  return ext_prod( pa, pb);
}

template< class Vf, class VL>
auto plane_of_line_and_perp_plane( VL line , Vf plane){
  
  return ext_prod( lcomp( weight( plane)), line);
}

template< class Vp, class Vq>
auto translation_from_point_to_point( Vp p, Vq q){

  auto pq = unitized( p + q);
  return geo_anti_prod( pq, p);
}

template< class VQ, class V>
auto transformed( VQ Q, V v){

  auto t1 = geo_anti_prod( Q, v);
  auto aQ = anti_rev( Q);
  return geo_anti_prod( t1 , aQ);
}

template< class Vf, class Vp>
auto home_transform( Vf f, Vp p){
  
  auto orig = point( i0,i0,i0);
  auto trans = translation_from_point_to_point( p, orig);

  auto tf = [&]{
    auto fx = element< 2,3,4>( f);
    auto fy = element< 1,3,4>( f);
    auto fz = element< 1,2,4>( f);
    return new_sparse_vector( E<2,3,4>{}, fx, E<1,3,4>{}, fy,
                               E<1,2,4>{}, fz);
  }();
  
  auto rot = [&]{
    auto fx = element< 2,3,4>( tf);
    auto fy = element< 1,3,4>( tf);
    auto xy_comp = fx*fx + fy*fy;
    auto fz = element< 1,2,4>( tf);
    if (xy_comp < 1.0e-6*fabs( fz)){
      if (fz > 0.0){
        return new_sparse_vector( E<1,2,3,4>{}, 1.0,
                                   E<1,4>{}, 0.0, E< 2,4>{}, 0.0);
      }
      else{
        const double s2 = 1.0/sqrt(2.0);
        auto rline = new_sparse_vector( E<1,4>{}, s2, E<2,4>{}, s2);
        return rline + new_sparse_vector( E<1,2,3,4>{}, 0.0);
      }
    }
    else{
      auto planexy = new_sparse_vector( E<1,2,4>{}, 1.0);
      auto mid_tf = unitized( planexy + tf);
      return geo_anti_prod( planexy, mid_tf);
    }
  }();
  
  return geo_anti_prod( rot, trans);
}

// assume planes are unitized
// returns angle between -pi and pi
// angle from pf to pg, ccwise looking down common line
template< class Vf, class Vg>
auto angle_between_planes( Vf pf, Vg pg){
  
  auto rot = unitized( geo_anti_prod( pf, pg));
  auto c = element< 1,2,3,4>( rot);
  
  //auto line = unitized( line_of_planes( pf, pg));
  
  //auto s_as = geo_anti_prod( weight( rot), anti_rev( line));
  //auto s = element< 1,2,3,4>( s_as);
 
  if constexpr (is_nothing< decltype( c)>){
    return acos( 0.0);
  }
  else{
    //auto hthe = atan2( s, c);
    //return hthe;
    return acos( c);
  }
}

//#######################################################
template< class Va, class Vb>
auto commutator_nm( Va a, Vb b){
  return half( geo_prod( a, rev(b)) -
               geo_prod( b, rev(a))
              );
}

template< class Va, class Vb>
auto commutator_np( Va a, Vb b){
  return half( geo_prod( a, rev(b)) +
               geo_prod( b, rev(a))
              );
}

template< class Va, class Vb>
auto commutator_um( Va a, Vb b){
  return half( geo_anti_prod( a, anti_rev(b)) -
               geo_anti_prod( b, anti_rev(a))
              );
}

template< class Va, class Vb>
auto commutator_up( Va a, Vb b){
  return half( geo_anti_prod( a, anti_rev(b)) +
               geo_anti_prod( b, anti_rev(a))
              );
}

//#######################################################
template< class Stream, int n>
Stream& operator<<( Stream& stm, IC<n>){
  stm << n;
  return stm;
}

void display( uint a){
  std::cout << '|';
  for( uint i = 0; i < 4; i++){
    if (((1 << i) & a) > 0){
      std::cout << i+1;
    }
  }
  std::cout << "| ";
}

template< int n>
std::iostream& operator<<( std::iostream& out, IC<n>){
  out << n;
  return out;
}

template< uint n>
void displayi( None){}

template< uint n, uint level, bool em0, bool em1, class T0, class T1>
void displayi( SVec< level, em0, em1, T0, T1> v){
  
  constexpr uint n0 = n << 1;
  
  if constexpr (level == 1){
    if constexpr (!std::is_same_v< None, decltype( v.subv0())>){
      display( n0);
      std::cout << v.subv0() << std::endl;
    }

    if constexpr (!std::is_same_v< None, decltype( v.subv1())>){
      constexpr uint n1 = n0 + 1;
      display( n1);
      std::cout << v.subv1() << std::endl;
    }
    
  }
  else{
    constexpr uint n1 = n0 + 1;
    displayi< n0>( v.subv0());
    displayi< n1>( v.subv1());
  }
}

template< uint level, bool em0, bool em1, class T0, class T1>
void display( SVec< level, em0, em1, T0, T1> v){
  
  displayi< ntop>( v);
}

  
}
