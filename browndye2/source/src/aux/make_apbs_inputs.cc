// inputs: original input file with additional info:
// - solvent ion concentrations
// - inner grid spacings for each molecule
// output: new input file, apbs input files

#include <cassert>
#include <fstream>
#include "../generic_algs/cartesian_multipole.hh"
#include "../forces/other/blank_nonbonded_parameter_info.hh"
#include "../global/physical_constants.hh"
#include "../structures/solvent.hh"
#include "../global/pi.hh"
#include "../lib/bisect.hh"
#include "../lib/slist.hh"
#include "../xml/node_info.hh"
#include "../lib/bool_of_string.hh"
#include "../input_output/get_args.hh"

namespace{
  using namespace Browndye;
  
  using namespace JAM_XML_Pull_Parser;

  constexpr size_t X = 0;
  constexpr size_t Y = 1;
  constexpr size_t Z = 2;

  constexpr size_t mpole_order = 6;
  const Length L1{ 1.0}, L0{ 0.0};
  const Charge Q1{ 1.0};
  
  typedef decltype( 1.0/Q1) Inv_Charge;

  typedef Vector< Atom, Atom_Index> Atoms;
  typedef Vector< size_t> Indices;

  //###################################################################
  using std::min;
  using std::max;
  using std::tie;
  using std::pair;
  using std::make_pair;
  using std::move;
 
  typedef pair< Pos,Pos> Poses2;
  
  //###################################################
  Atoms atoms_from_file( const string& file){
    Blank_Nonbonded_Parameter_Info npi;
    return npi.atoms_from_file( file);
  }
  
  //####################################################################  
  Charge total_charge( const Atoms& atoms){
    
    return atoms.folded_left( [&]( Charge sum, const Atom& atom){
      return sum + atom.charge;
    }, Charge{ 0.0});
  }
  
  //###############################################################
  struct CPoint{
    Pos pos;
    // energy is scaled by kT
    decltype( 1.0/Charge()) phi{ 0.0};
  };
  
  //#########################
  struct AP_Container{
    const Atoms& atoms;
    //Vector< CPoint> points;
    Solvent solvent;
    
    explicit AP_Container( const Atoms& atoms): atoms( atoms){}
  };
  
  //######################################
  struct AP_Refs{
    Vector< Atom_Index> aindices;
    //Indices pindices;
    
    AP_Refs( AP_Refs&& other):
      aindices( move( other.aindices)){}
      
    AP_Refs& operator=( AP_Refs&& other){
      aindices = move( other.aindices);
      return *this;
    }
    
    AP_Refs(){}
  };
  
  //######################################################
  class EInterface{
  public:
    
    typedef AP_Container Container;
    typedef AP_Refs Refs;
    typedef size_t size_type;
     
    static
    void process_bodies( Container& cont, const Refs& refs);
  
    static
    void process_bodies( Container& cont, const Refs& refs0, const Refs& refs1);
  
    template< class V>
    static
    void get_r_derivatives( Container& cont, unsigned int order, double r, 
                            V& derivs);
  
    template< class V3>
    static
    Inv_Charge near_potential( const Container& cont, 
                           const Refs& refs, const V3& pos);
  
    template< class Function> 
    static
    void apply_multipole_function( const Container& cont, 
                                   const Refs& refs, Function& f);
  
    template< class V3>
    static
    void get_bounds( const Container& cont, V3& low, V3& high);
  
    template< class Refs_Cube, class V3>
    static
    void split_container( const Container& cont, 
                          const Refs& refs, 
                          const V3& center,
                          const Refs_Cube& refs_cube);
  
    static
    void empty_references( const Container& cont, Refs& refs);
  
    static
    void copy_references( const Container& cont, 
                          const Refs& refs0,
                          Refs& refs1
                          );
  
    static
    void copy_references( const Container& cont, Refs& refs);
  
    static
    size_type size( const Container& cont, const Refs& refs);
  
    static
    size_type n_receivers( const Container& cont, const Refs& refs){
      return 0;
    }
  
    static
    size_type n_sources( const Container& cont, const Refs& refs);
    
  private:
    static
    void process_bodies_1way( Container& cont,
                               const Refs& refs0, const Refs& refs1);
    
    template< class V3>
    static
    auto parted_cont( const Container& cont,
                      const V3& center, const Refs& refs,
                      size_t k) -> pair< Refs,Refs>;
    
  };
  
  //###############################################################
  Array< double, 3> dim_pos( Pos pos){
    Array< double, 3> upos{ pos[X]/L1, pos[Y]/L1, pos[Z]/L1};
    return upos;
  }
  
  //#############################################################
  decltype( L1/(Q1*Q1)) field_factor( const Solvent& solvent){
    
    const auto dielectric = solvent.dielectric;
    const auto eps = vacuum_permittivity;
    const auto kT = solvent.kT;
    return 1.0/(4.0*pi*eps*dielectric*kT);
  }
  
  //##################################################################
  template< size_t m>
  Array< Array< int, m+1>, m+1> rd_coefficients(){
    
    constexpr size_t mo = m+1;
    
    Array< int, mo> c0{1,0,0,0,0,0,0};
    Array< int, mo> c1{-1,-1,0,0,0,0,0};
    Array< int, mo> c2{1,2,2,0,0,0,0};
    Array< int, mo> c3{-1,-3,-6,-6,0,0,0};
    Array< int, mo> c4{1,4,12,24,24,0,0};
    Array< int, mo> c5{-1,-5,-20,-60,-120,-120,0};
    Array< int, mo> c6{ 1,6,30,120,360,720,720};
    
    Array< Array< int, mo >, mo> cs{ c0,c1,c2,c3,c4,c5,c6};
    return cs;
  }
  
  //###########################################################
  template< size_t m>
  Array< double, m+1> kappas( double kappa){
    
    constexpr size_t mo = m+1;
    Array< double, mo> kps;
    kps[0]  = 1.0;
    for( auto k: range(1,mo)){
      kps[k] = kps[k-1]*kappa; // could be done once, before
    }
    return kps;
  }
  
  //########################################################
  template< size_t m>
  Array< double, m+1> inv_rs( double r){
  
    constexpr size_t mo = m+1;
    Array< double, mo> rinv;
    rinv[0] = 1.0;
    for( auto k: range(1,mo)){
      rinv[k] = rinv[k-1]/r;
    }
    return rinv;
  }
  //#################################################################
  template< class V>
  void EInterface::get_r_derivatives( Container& cont, unsigned int order,
                                       double r, V& derivs){
    
    assert( order == mpole_order);
    
    const auto& solvent = cont.solvent;
    const auto kappa = L1/solvent.debye_length;
    constexpr size_t m = mpole_order;
    auto cs = rd_coefficients< m>();
    
    auto kps = kappas< m>( kappa);
    auto rinv = inv_rs< m>(r);
    
    auto efactor = field_factor( solvent);
    double U = (Q1*Q1/L1)*efactor*exp(-kappa*r)/r;
    for( auto io: range( order+1)){
      auto& dv = derivs[io];
      dv = 0.0;
      auto& c = cs[io];
      for( auto k: range( io+1)){
        dv += c[k]*rinv[k]*kps[io-k];
      }
      dv *= U;
    }
  }
  
  //########################################################
  template< class V3>
  Inv_Charge EInterface::near_potential( const Container& cont, 
                                     const Refs& refs, const V3& pos){
    
    const auto debye = cont.solvent.debye_length;
    decltype( Q1/L1) sum{ 0.0};
    for( auto ip: refs.aindices){
      auto& atom = cont.atoms[ip];
      auto posa = atom.pos;
      auto q = atom.charge;
      
      auto r = distance( posa, pos);
      sum += q*exp(-r/debye)/r;
    }
    return field_factor( cont.solvent)*sum;
  }
  
  //#############################################################
  template< class Function> 
  void EInterface::apply_multipole_function( const Container& cont, 
                                 const Refs& refs, Function& f){
    
    for( auto ip: refs.aindices){
      auto& atom = cont.atoms[ip];
      auto upos = dim_pos( atom.pos);
      auto q = atom.charge;
      Array< double, 1> mpole{ q/Q1};
      f( 0, upos, mpole);
    }
  }
  
  //##################################################################
  pair< Pos, Pos> minmax( pair< Pos, Pos> lohi, Pos pos,
                                 Length radius){
    
    auto [lo,hi] = lohi;
      
    auto mn = [&,lo=lo]( size_t i){
      return min( pos[i]-radius, lo[i]);
    };
    
    auto mx = [&,hi=hi]( size_t i){
      return max( pos[i]+radius, hi[i]);
    };
    
    auto new_lo = initialized_array< 3>( mn);
    auto new_hi = initialized_array< 3>( mx);
    
    return make_pair( new_lo, new_hi);  
  }
  
  //###################################################################
  Poses2 bounds_g( const Atoms& atoms, Length pad){
    
    const Length lg{ large};
    const Length ng{ neg_large};
    Pos hi0{ ng,ng,ng}, lo0{ lg,lg,lg};
    pair ext0{ lo0, hi0};
     
    auto [lo, hi] =
      atoms.folded_left( [&]( auto res, const Atom& atom){
      return minmax( res, atom.pos, atom.radius + pad);
    }, ext0);
    
    if (hi[X] < lo[X] || hi[Y] < lo[Y] || hi[Z] < lo[Z]){
      error( "bounds are reversed");
    }
    
    return make_pair( lo, hi);
  }
   
  //###################################################################
  template< class V3>
  void EInterface::get_bounds( const Container& cont, V3& low, V3& high){
     
    auto [lo,hi] = bounds_g( cont.atoms, L0);
    
    for( auto k: range(3)){
      low[k] = lo[k]/L1;
      high[k] = hi[k]/L1;
    }
  }
  
  //##############################################################
  template< class V3>
  auto EInterface::parted_cont( const Container& cont,
                            const V3& center, const Refs& refs,
                            size_t k) -> pair< Refs,Refs>{
      
    Refs refs0, refs1;
    Length c{ center[k]};
    
    auto [ats0,ats1] =  refs.aindices.partitioned( [&]( Atom_Index ia){
      return cont.atoms[ia].pos[k] < c;
    });
    
    refs0.aindices = move( ats0);
    refs1.aindices = move( ats1);
    
    return make_pair( move( refs0), move( refs1));
  }
  
  //########################################################
  template< class Refs_Set, class V3>
  void EInterface::split_container( const Container& cont, 
                                      const Refs& refs, 
                                      const V3& center,
                                const Refs_Set& set_refs){
     
    auto parted = [&]( const Refs& refs, size_t k){
      return parted_cont( cont, center, refs, k);
    };
    
    auto [c0,c1] = parted( refs, 0);
    auto [c00,c01] = parted( c0, 1);
    auto [c000, c001] = parted( c00, 2);
    set_refs( 0,0,0, move( c000));
    set_refs( 0,0,1, move( c001));
    
    auto [c010, c011] = parted( c01, 2);
    set_refs( 0,1,0, move( c010));
    set_refs( 0,1,1, move( c011));
    
    auto [c10,c11] = parted( c1, 1);
    auto [c100, c101] = parted( c10, 2);
    set_refs( 1,0,0, move( c100));
    set_refs( 1,0,1, move( c101));
    
    auto [c110, c111] = parted( c11, 2);
    set_refs( 1,1,0, move( c110));
    set_refs( 1,1,1, move( c111));
  }
   
  //################################################################## 
  void EInterface::empty_references( const Container& cont, Refs& refs){
    
    refs.aindices.clear();
  }
   
  //################################################################
  void EInterface::copy_references( const Container& cont, Refs& refs){
    
    auto na = cont.atoms.size()/Atom_Index(1);
    refs.aindices = initialized_vector( na, []( size_t i){
      return Atom_Index(i);
    });
  }
 
  //############################################################
  auto EInterface::size( const Container& cont,
                          const Refs& refs) -> size_type{
    
    return refs.aindices.size();
  }
   
  //####################################################
  auto EInterface::n_sources( const Container& cont,
                               const Refs& refs) -> size_type {
    
    return refs.aindices.size();
  } 
  
  typedef Cartesian_Multipole::Calculator< EInterface> MPoles;
  
  
  //##############################################################
  void compute_mpoles( AP_Container& cont, Vector< CPoint>& cpts){
    
    MPoles mcalc( mpole_order, cont);
    mcalc.set_max_number_per_cell( maxi);
    mcalc.setup_cells();
    mcalc.compute_expansion();
    auto pot_calc = mcalc.potential_computer();
    for( auto& pt: cpts){
      Vec3< double> upos;
      for( auto k: range(3)){
        upos[k] = pt.pos[k]/L1;
      }
      pt.phi = pot_calc( upos)/Q1;
    }
  }
  
  //###########################################
  Poses2 inner_bounds( const Atoms& atoms, Length pad){
    
    auto [lo,hi] = bounds_g( atoms, pad);
    return make_pair( lo, hi);
  }
  
  //##################################################
  Length allowed_width( double rtol, Length debye, Length r){
    
    const double factor = 2.84;
    if (debye > Length( 1.0e6)){
      return (factor*sqrt(rtol/2))*r;
    }
    else{
      auto rs = r/debye;
      return debye*factor*sqrt(rtol/(2/rs + 2/(rs*rs) + 1));
    }
  }
  
  //######################################################
  template< class T>
  Vector< T> dconcated( Vector< T>&& v0, Vector< T>&& v1){
    
    auto n0 = v0.size();
    auto n1 = v1.size();
    return initialized_vector( n0+n1, [&]( size_t i){
      if (i < n0){
        return v0[i];
      }
      else{
        return v1[ i - n0];
      }
    });
  }
  
  //######################################################
  template< class T>
  Vector< T> dconcatenated( Vector< Vector< T> >&& vecs){
    
    auto n = vecs.size();
    if (n == 1){
      return move( vecs[0]);
    }
    else if (n == 2){
      return dconcated( move( vecs[0]), move( vecs[1]));
    }
    else{
      auto hn = n/2;
      auto sv0 = initialized_vector( hn, [&]( size_t i){
        return move( vecs[i]);
      });
      auto sv1 = initialized_vector( n - hn, [&]( size_t i){
        return move( vecs[hn + i]);
      });
      
      return dconcated( dconcatenated( move( sv0)),
                        dconcatenated( move( sv1)));
    }
  }
  
  //##########################################################
  class Face_Maker{
  public:
    Face_Maker( Pos lo, Pos hi, bool is_lo, int iface, Length gw);
    Vector< CPoint> face_row( size_t ky) const;
    
    Pos lo, hi;
    int idx,idy,idz;
    size_t nx,ny;
    Length z;
    Length hx,hy;
  };
  
  //######################################################
  // constructor
  Face_Maker::Face_Maker( Pos _lo, Pos _hi, bool is_lo,
                           int iface, Length grid_h){
    
    lo = _lo;
    hi = _hi;
    
    if (hi[X] < lo[X] || hi[Y] < lo[Y] || hi[Z] < lo[Z]){
      error( __FILE__, __LINE__, "lo and hi out of order");
    }
    
    idz = iface;
    idx = (iface + 1)%3;
    idy = (iface + 2)%3;
    
    auto width = hi - lo;
    auto wx = width[idx];
    auto wy = width[idy];
    nx = wx/grid_h + 2;
    ny = wy/grid_h + 2;
    
    hx = wx/((double)(nx-1));
    hy = wy/((double)(ny-1));
    z = (is_lo ? lo : hi)[idz];
  }
  
  //#####################################################
  Vector< CPoint> Face_Maker::face_row( size_t ky) const{
    
    auto y = lo[idy] + hy*(double)ky;
    return initialized_vector( nx, [&]( size_t kx){
      auto x = lo[idx] + hx*(double)kx;
      CPoint cpt;
      cpt.pos[idx] = x;
      cpt.pos[idy] = y;
      cpt.pos[idz] = z;
      return cpt;
    });
  }
  
  //####################################################
  Vector< CPoint> face_of( Pos lo, Pos hi, bool is_lo,
                            int iface, Length grid_h){
     
    Face_Maker face_maker( lo,hi,is_lo,iface, grid_h);
    
    auto row = [&]( size_t ky){
      return face_maker.face_row( ky);
    };
     
    auto ny = face_maker.ny; 
    Vector< Vector< CPoint> > rows =
                        initialized_vector( ny, [&]( size_t ky){
      return row( ky);
    });
    
    return dconcatenated( move( rows)); 
  }
  
  //########################################################
  Poses2 padded_corners( Poses2 corners, Length d){
    
    auto [ilo,ihi] = corners;
    Pos dvec{ d,d,d};
    auto hi = ihi + dvec;
    auto lo = ilo - dvec;
    return make_pair( lo,hi);
  }
  
  //###########################################################
  Vector< CPoint> all_faces( Poses2 corners,
                            Length grid_h, Length d){
    
    Vector< pair< int,bool> >
         ifaces{ {X,true},{X,false},{Y,true},{Y,false},
                 {Z,true},{Z,false}};
        
    auto lohi = padded_corners( corners, d);  
    auto lo = lohi.first;
    auto hi = lohi.second;
      
    return dconcatenated( ifaces.mapped( [&]( auto finfo){
      auto [iface,is_lo] = finfo;
      return face_of( lo, hi, is_lo, iface, grid_h);
    }));
  }
  
  //##########################################################
  Length grid_width_error( const Atoms& atoms, const Solvent& solvent,
                      Length grid_h, Poses2 corners, Length d){
    
    auto debye = solvent.debye_length;
    auto faces = all_faces( corners, grid_h, d);
    
    AP_Container cont( atoms);
    cont.solvent = solvent;
   
    compute_mpoles( cont, faces);
    
    auto max_phi = faces.folded_left(
       [&]( Inv_Charge mphi, const CPoint& cpt){
            return max( mphi, fabs( cpt.phi));
      }, Inv_Charge( 0.0));
    
    const auto atol = 0.01/Q1;
    auto rtol = atol/max_phi;
    
    auto aw = allowed_width( rtol, debye, d);
    return aw - 2.0*grid_h;
  }
  
  //##########################################################
  Poses2 inner_grid_bounds( const Atoms& atoms, const Solvent& solvent,
                            Length grid_h, Poses2 corners){
    
    auto gwe = [&]( Length d){
      return grid_width_error( atoms, solvent, grid_h, corners, d);
    };
    
    Length lo0{ 0.1};
    Length hi0 = Bisect::bound( 2.0, gwe, lo0);
    const Length tol{ 1.0e-3};
    auto min_d =  Bisect::solution( gwe, lo0, hi0, tol);
    
    auto [ilo,ihi] = corners;    
    Pos dd{ min_d, min_d, min_d};
    auto hi = ihi + dd;
    auto lo = ilo - dd;
        
    return make_pair( lo,hi);
  }
  
  //###########################################
  Pos appx_hydro_center( const Atoms& batoms){
        
    Array< Length2, 3> psum0( Zeroi{});
    Length rsum0{ 0.0};
    
    auto [psum,rsum] =
      batoms.folded_left( [&]( auto sum, const Atom& atom){
      auto a = atom.radius;
      auto [psum0,rsum0] = sum;
      auto psum = psum0 + a*atom.pos;
      auto rsum = rsum0 + a;
      return make_pair( psum,rsum);
    }, make_pair( psum0,rsum0));
    
    return psum/rsum;
  }
  
  //###############################################################
  class Outer_Box_Finder{
  public:
    Outer_Box_Finder( const Atoms& atoms, const Solvent& solvent,
                       Length gridh);
    
    Inv_Charge grid_poten_error( Length d) const;
    Poses2 outer_box() const;
    
    const Atoms& atoms;
    const Solvent& solvent;
    Length grid_h;
    Poses2 corners;
    Pos center;
    decltype( L1/(Q1*Q1)) efactor;
    Charge Q;
    Length debye;
    
    const Vector< pair< int,bool> >
        ifaces{ {X,true},{X,false},{Y,true},{Y,false},{Z,true},{Z,false}};
  };
  
  //###########################################################
  const double pad_fudge = 1.1;
  
  // constructor
  Outer_Box_Finder::Outer_Box_Finder( const Atoms& atoms,
                                       const Solvent& solvent,
                                          Length _grid_h):
    atoms(atoms), solvent( solvent){
        
    corners = inner_bounds( atoms, pad_fudge*solvent.srad); 
    debye = solvent.debye_length;
    Q = total_charge( atoms);   
    center = appx_hydro_center( atoms);  
    efactor = field_factor( solvent);
    grid_h = _grid_h;
  }
  
  //#############################################################
  Inv_Charge Outer_Box_Finder::grid_poten_error( Length d) const{
    
    auto faces = all_faces( corners, grid_h, d);
          
    AP_Container cont( atoms);
    cont.solvent = solvent;
    
    compute_mpoles( cont, faces);
    
    auto max_dev = faces.folded_left(                                 
          [&]( Inv_Charge mdev, const CPoint& cpt){
      
      auto r = distance( cpt.pos, center);
      auto rphi = efactor*Q*exp(-r/debye)/r;
      auto dev = fabs( rphi - cpt.phi); 
      return max( dev, mdev);
    }, Inv_Charge( 0.0));
    
    return max_dev;
  }
  
  //#####################################################
  const Inv_Charge phi_atol{ 0.01};
  
  Poses2 Outer_Box_Finder::outer_box() const{
    
    auto min_d = [&]() -> Length{
      Length Lm{0.1};
      if (grid_poten_error( Lm) < phi_atol){
        return Lm;
      }
      else{
        auto f = [&]( Length d){
          return grid_poten_error( d) - phi_atol;
        };
        
        Length lo0{ 0.1};
        Length hi0 = Bisect::bound( 2.0, f, lo0);
        const Length tol{ 1.0e-3};
        return Bisect::solution( f, lo0, hi0, tol);
      }
    }();
        
    return padded_corners( corners, min_d);
  }
  
  //##############################################################
  Poses2 outer_box( const Atoms& atoms,
                              const Solvent& solvent, Length grid_h){
    
    Outer_Box_Finder obf( atoms, solvent, grid_h);
    return obf.outer_box();    
  }
  
  //########################################################
  using std::optional;
  
  template< class T, size_t n>
  optional< Array< T, n> > opt_array( Array< optional< T>, n> xs){
    
    typedef optional< T> To;
    
    bool has_val = xs.folded_left( [&]( bool tf, To x){
      return tf & x.has_value();
    }, true);
    
    if (has_val){
      return xs.mapped( [&]( To xo){ return xo.value();});
    }
    else{
      return std::nullopt;
    }
  }
  
  //######################################################
  Vector< Vector< size_t> > possible_dims(){
    
    const size_t max_nlev = 10;
    Vector< size_t> cs{ 1,2,3,5};
    
    return initialized_vector( max_nlev, [&]( size_t nlevm1){
      auto nlev = nlevm1+1;
      return cs.mapped( [&]( size_t c){
        return (size_t)(c*(1 << (nlev+1)) + 1);
      });
    }); 
  }
  
  //######################################################
  Vector< optional< Array< size_t, 3> > >
  optional_indices( const Vector< Vector< size_t> >& ns,
                     Array< size_t, 3> ms){
    
    return ns.mapped( [&]( const Vector< size_t>& row){
      auto idxos = ms.mapped( [&](size_t ix) -> optional< size_t>{
      
        auto itr = std::upper_bound( row.begin(), row.end(), ix);
        if (itr != row.end()){
          return *itr;
        }
        else{
          return std::nullopt;
        }
      }
    );
    
    return opt_array( idxos); 
  });
  }
  
  //######################################################
  double grid_size( Array< size_t, 3> idx){
    
    return idx.folded_left( [&]( double prod, size_t i){
      return prod*((double)i);
    }, 1.0);
  }
  
  //####################################################
  bool optional_index_comp( std::optional< Array< size_t, 3> > oidxa,
                             std::optional< Array< size_t, 3> > oidxb){
    
    if( oidxa.has_value()){
      if (oidxb.has_value()){
        return grid_size( oidxa.value()) <
                grid_size( oidxb.value());
      }
      else{
        return true;
      }
    }
    else{
      return false;
    }
  }
  
  //#####################################################
  Array< size_t, 3> grid_dims( Pos widths, Length h){
    
    auto ns = possible_dims();
    
    auto ms = widths.mapped( [&]( Length w){
      return (size_t)(w/h)+1;
    });
    
    auto oidxs = optional_indices( ns, ms);
    auto itr = std::min_element( oidxs.begin(), oidxs.end(),
                                optional_index_comp);
                           
    auto best_oidx = *itr;
    return best_oidx.value();
  }
  
  //########################################################
  struct Box{
    Array< size_t, 3> dims;
    Length h;
    size_t lo_level, hi_level;
    Pos low;
    
    Pos center() const;
    Pos widths() const;
    
    typedef std::tuple< Array< size_t, 3>, Length, size_t, size_t, Pos> Rep;
    Rep rep() const;
    bool operator==( const Box&) const;
  };
  
  //###########################################################
  auto Box::rep() const -> Rep{
    return std::make_tuple( dims, h, lo_level, hi_level, low);
  }
  
  [[maybe_unused]]
  bool Box::operator==( const Box& other) const{
    return rep() == other.rep();
  }
  
  //###########################################################
  Pos Box::widths() const{
    auto fdims = dims.mapped( []( size_t i){ return (double)(i-1);});
    return h*fdims;
  }
  
  //######################################
  Pos Box::center() const {
    return low + 0.5*widths();
  }
  
  //###############################################################
  Pos bwidths( Poses2 bounds){
    auto [lo,hi] = bounds;
    return hi - lo;
  }
  
  //########################################################
  class Recursive_Boxer{
  public:
    const Atoms& atoms;
    const Solvent& solvent;
    Poses2 inner_bounds, outer_bounds;
    Length grid_h;
    
    Recursive_Boxer( const Atoms& atoms, const Solvent& solvent,
                      Poses2 inner_bounds,
                      Poses2 outer_bounds, Length grid_h);
    
    Box first_box() const;
    SList< Box> rboxes( SList< Box>) const;
    double grid_savings( Poses2 bounds0,
                          Poses2 bounds1,
                            Length h0, Length h1) const;
  };
  
  //#########################################################
  // constructor
  Recursive_Boxer::Recursive_Boxer( const Atoms& atoms,
                                     const Solvent& solvent,
                                    Poses2 inner_bounds,
                                    Poses2 outer_bounds,
                                   Length grid_h):
    atoms( atoms),
    solvent( solvent),
    inner_bounds( inner_bounds),
    outer_bounds( outer_bounds),
    grid_h( grid_h){
  }
  
  //###########################################################
  Pos bcenter( Poses2 bounds){
    auto [lo,hi] = bounds;
    return 0.5*(lo + hi);
  }
   
  //###########################################################
  Box Recursive_Boxer::first_box() const{
    
    auto bounds = inner_grid_bounds( atoms, solvent, grid_h,
                                           inner_bounds);
       
    auto widths = bwidths( bounds);
    auto dims = grid_dims( widths, grid_h);
    
    Box box;
    box.dims = dims;
    box.h = grid_h;
    box.lo_level = 0;
    box.hi_level = 0;
    
    auto center = bcenter( inner_bounds);
    auto width = dims.mapped( [&]( size_t i){
      return ((double)(i-1))*grid_h;
    });
    
    box.low = center - 0.5*width;   
    return box;
  }
  
  //############################################################
  double Recursive_Boxer::grid_savings( Poses2 bounds0,
                                       Poses2 bounds1,
                                       Length h0, Length h1) const{
    
    auto widths0 = bwidths( bounds0);
    auto widths1 = bwidths( bounds1);
    auto vol0 = widths0[X]*widths0[Y]*widths0[Z];
    auto mem0 = vol0/h0;
    auto vol1 = widths1[X]*widths1[Y]*widths1[Z];
    auto mem1 = vol1/h1;
    auto bmem0 = vol1/h0;
    return bmem0/(mem0 + mem1);
  }
  
  //###############################################################
  class Next_Box_Maker{
  public:
    const Recursive_Boxer& rboxer;
    SList< Box> boxes0;
    Poses2 bounds0;
    Pos center;
    
    Next_Box_Maker( const Recursive_Boxer&, SList< Box>,
                   Poses2, Pos center);
    SList< Box> next_boxes() const;
    Box bigger_box( Array< size_t, 3> dims1) const;
    Box coarse_box( Array< size_t, 3> dims1, Length h1) const;
  };
  
  //###########################################################
  // constructor
  Next_Box_Maker::Next_Box_Maker( const Recursive_Boxer& rboxer,
                                 SList< Box> boxes0,
                                 Poses2 bounds0, Pos center):
    rboxer( rboxer), boxes0( boxes0),
    bounds0( bounds0), center( center){}
  
  //######################################################
  Poses2 dim_bounds( Pos center, Array< size_t, 3> dims,
                                Length h){
     
    auto hwidth = 0.5*h*dims.mapped( []( size_t i){
                               return (double)(i-1);});     
    return make_pair( center - hwidth, center + hwidth);
  }  
  
  //#################################################
  Pos box_low( const Box& box0, Array< size_t, 3> dims1, Length h1){
    
    auto center = box0.center();
    
    auto width1 = dims1.mapped( [&]( size_t i){
      return ((double)(i-1))*h1;
    });
    
    return center - 0.5*width1;
  }
  
  //############################################################
  Box Next_Box_Maker::bigger_box( Array< size_t, 3> dims1) const{
    
    auto& box0 = head( boxes0);
    auto dlev = box0.hi_level - box0.lo_level;
    
    auto fdims1 = dims1.mapped( [&]( size_t i ){
      return (1 << dlev)*2*(i-1) + 1;});
    
    auto h0 = box0.h;
    
    Box box;
    box.dims = fdims1;
    box.lo_level = box0.lo_level;
    box.hi_level = box0.hi_level + 1;
    box.h = h0; 
    box.low = box_low( box0, fdims1, h0);
    
    return box;
  }
  
  //#################################################################
  Box Next_Box_Maker::coarse_box( Array< size_t, 3> dims1,
                                   Length h1) const{
    
    auto& box0 = head( boxes0);
    Box box;
    box.dims = dims1;
    box.h = h1;
    box.lo_level = box0.hi_level + 1;
    box.hi_level = box.lo_level;
    box.low = box_low( box0, dims1, h1);
    
    return box;
  }
  
  //#########################################################
  SList< Box> Next_Box_Maker::next_boxes() const{
    
    auto grid_h = rboxer.grid_h;
    auto& atoms = rboxer.atoms;
    auto& solvent = rboxer.solvent;
    auto& box0 = head( boxes0);
    
    auto h0 = box0.h;
    auto h1 = grid_h*(double)(2 << box0.hi_level);

    auto pbounds1 = inner_grid_bounds( atoms, solvent, h1, bounds0);    
    auto pwidths1 = bwidths( pbounds1);
    auto dims1 = grid_dims( pwidths1, h1);
    auto bounds1 = dim_bounds( center, dims1, h1);  

    auto savings = rboxer.grid_savings( bounds0, bounds1, h0, h1);
    
    if (savings < 1.2){
      auto tboxes = tail( boxes0);
      return appended( bigger_box( dims1), tboxes);
    }
    else{
      auto box = coarse_box( dims1, h1);
      return appended( box, boxes0);
    }
  } 
  
  //###########################################################
  bool contains( Pos widthsa, Pos widthsb){
    return (widthsa[X] > widthsb[X] && widthsa[Y] > widthsb[Y] &&
            widthsa[Z] > widthsb[Z]);
  }
  
  //#############################################################
  SList< Box> Recursive_Boxer::rboxes( SList< Box> boxes0) const{
      
    if (is_empty( boxes0)){
      return rboxes( slist( first_box()));
    }
    else{
      auto center = bcenter( inner_bounds); 
       
      const auto& box0 = head( boxes0);
      
      auto h0 = box0.h;
      auto bounds0 = dim_bounds( center, box0.dims, h0);
      auto widths0 = bwidths( bounds0);
      auto owidths = bwidths( outer_bounds);
      
      if (contains( widths0, owidths)){  
        return boxes0;
      }
      else{
        Next_Box_Maker nbm( *this, boxes0, bounds0, center);
        return rboxes( nbm.next_boxes());
      } 
    }
  }
  
  //######################################################
  SList< Box> box_list( const Atoms& atoms,
                        const Solvent& solvent,
                         Poses2 inner_bounds,
                         Poses2 outer_bounds,
                          Length grid_h){
    
    Recursive_Boxer rboxer( atoms, solvent, inner_bounds, 
                              outer_bounds, grid_h);
    
    return rboxer.rboxes( SList< Box>());
  }
  
  //###########################################################  
  pair< std::unique_ptr< Parser>, Node_Ptr>
    xml_from_file( const string& file){
    
    std::ifstream input( file);
    auto parser = std::make_unique< Parser>( input, file);
    auto top = parser->top();
    top->complete();
    return make_pair( move( parser), top);
  }
   
  //##############################################
  struct Ion{
    Length radius;
    Charge charge;
    Inv_Length3 conc; // in molar units
  };
  
  //###########################################
  class Input_Info{
  public:
    void main( const string& file);
    
  private:
    friend class Core_Info;
    
    void get_ions( const Node_Ptr&);
    Length debye_of_ions( Energy kT, double dielectric);
    void get_solvent( const Node_Ptr& snode);
    Node_Ptr new_solvent_node( const Node_Ptr& snode) const;
    SList< Box> grids_from_file( Length grid_h, const string& file) const;
    Node_Ptr with_grid_nodes( const Node_Ptr& cnode) const;
    //void make_apbs_in( const string& name, const Box& box) const;
    Node_Ptr with_new_core_nodes( const Node_Ptr& gnode) const;
    Node_Ptr with_new_group_nodes( 
                                    const Node_Ptr& system_node) const;
    template< class T>
    void opt_set( T&, const Node_Ptr&, const string&);
    
    Vector< Ion> ions;
    Solvent solvent;
  };
  
  //############################################
  class Core_Info{
    friend class Input_Info;
    
    void output_apbs_files();
    Core_Info( const Input_Info& ii): input_info( ii){}
    
    Node_Ptr enode_with_grid_nodes( const Node_Ptr& enode0) const;
    void add_ag_attribute( size_t n_ext, Node_Ptr enode) const;
    
    string name;
    double solute_dielectric;
    string atom_file;
    SList< Box> boxes;
    SList< string> grid_names;
    const Input_Info& input_info;
    Length grid_h;
  };
  
  //######################################################
  void Core_Info::output_apbs_files(){
   
    auto nb = size( boxes);
    auto bnums = iota( nb);
    auto num_boxes = zipped( boxes, bnums);
      
    auto& outer_box = head( boxes);
    auto center = outer_box.center();
    
    SList< string> rgrid_names;
    iterate( [&]( auto& num_box){
      auto ib = std::get<1>( num_box);
      const Box& box = std::get<0>( num_box);
      string gname = name + string_rep( ib);
      rgrid_names = appended( gname, rgrid_names);
      std::ofstream out( gname + ".in");
      auto dims = box.dims;
      auto widths = box.widths();
      auto& solvent = input_info.solvent;
      auto kT = solvent.kT;
      
      out << "read\n";
      out << "  mol xml " << atom_file + '\n'; 
      if (ib > 0){
        out << "  pot dx " << name << ib-1 << ".dx\n";
      }
      out << "end\n\n";
      
      out << "elec\n";
      out << "  mg-manual\n";
      out << "  dime " << dims << '\n';
      out << "  glen " << widths << '\n';
      out << "  gcent " << center << '\n';
      out << "  mol 1\n";
      out << "  lpbe\n";
      
      if (ib > 0){
        out << "  usemap pot 1\n";
        out << "  bcfl map\n";
      }
      else{
        out << "  bcfl sdh\n";
      }
      
      out << '\n';
      for( auto& ion: input_info.ions){
        out << "  ion charge " << ion.charge << " conc " << ion.conc
            << " radius " << ion.radius << '\n';
      }
      out << '\n';  
      
      out << "  pdie " << solute_dielectric << '\n';
      out << "  sdie " << solvent.dielectric << '\n';
      out << "  srfm smol\n";
      out << "  chgm spl2\n";
      out << "  sdens 10.0\n";
      out << "  srad " << solvent.srad << '\n';
      out << "  temp " << 298.0*kT << '\n';
      out << "  write pot dx " << name << ib << '\n';
      
      out << "end\n";
      out << "quit\n";
      
    }, num_boxes);
    
    grid_names = reversed( rgrid_names);
  }
  
  //######################################################
  template< class T>
  T from_node( const Node_Ptr& node, const string& tag){
  
    return checked_value_from_node< T>( node, tag);  
  }
  
  //######################################
  template< class T>
  std::optional< T> ofrom_node( const Node_Ptr& node, const string& tag){
  
    return value_from_node< T>( node, tag);
  }
  
  //##########################################################
  void Input_Info::get_ions( const Node_Ptr& ions_node){
    
    auto ion_nodes = ions_node->children_of_tag( "ion");
    for( auto inode: ion_nodes){
      Ion ion;
      ion.radius = from_node< Length>( inode, "radius");
      ion.charge = from_node< Charge>( inode, "charge");
      ion.conc = from_node< Inv_Length3>( inode, "conc");
      ions.push_back( ion);
    }
  }
  
  //#############################################################
  Length Input_Info::debye_of_ions( Energy kT, double dielectric){
    // convert from M to A^3
    const double conv = 6.02e-4;
    typedef decltype( Q1*Q1*Inv_Length3()) Ion_Str;
    auto ion_strength = ions.folded_left( [&]( Ion_Str sum,
                                              const Ion& ion){
      auto q = ion.charge;
      auto conc = ion.conc*conv;
      return sum + q*q*conc;
    }, Ion_Str( 0.0));
    
    typedef decltype( Charge()*Charge()*Inv_Length3()) Ionic_Strength;
    if (ion_strength == Ionic_Strength{ 0.0}){
      return Length{ large};
    }
    else{
      auto eps = vacuum_permittivity*dielectric;
      return sqrt( kT*eps/ion_strength);
    }
  }
  
  //#######################################################
  Node_Ptr solvent_node( const Node_Ptr& system_node){
    
    auto snode = system_node->child( "solvent");
    if (!snode){
      system_node->perror( "no solvent tag");
    }
    return snode;
  }
  
  //########################################################
  template< class T>
  void Input_Info::opt_set( T& val, const Node_Ptr& snode, const string& tag){
    auto oval = value_from_node< T>( snode, tag);
    if (oval){
      val = oval.value();
    }
  }
    
  //####################################################
  void Input_Info::get_solvent( const Node_Ptr& snode){
    
    auto oset = [&]( auto& val, const string& tag){
      opt_set( val, snode, tag);
    };
    
    auto& s = solvent;
    oset( s.kT, "kT");
    oset( s.dielectric, "dielectric");
    oset( s.relative_viscosity, "relative_viscosity");
    oset( s.desolve_fudge, "desolvation_parameter");
    oset( s.srad, "solvent_radius");
    
    auto ions_node = snode->child( "ions");
    if (ions_node){
      get_ions( ions_node);
    }
    
    auto debye = debye_of_ions( solvent.kT, solvent.dielectric);
    solvent.debye_length = debye;
  }
  
  //#############################################################
  Node_Ptr Input_Info::new_solvent_node( const Node_Ptr& snode) const{
    
    auto debye = solvent.debye_length; 
    return snode->with_child_added( "debye_length", debye);
  }
  
  //#############################################################
  Node_Ptr with_replaced_nodes( const Node_Ptr& node,
                               const Vector< Node_Ptr>& new_children,
                               const string& tag){
    
    Node_Ptr new_node( new Node{ node->copy()});
    while( new_node->child( tag)){
      new_node = new_node->with_child_removed( tag);
    }
    
    for( auto nchild: new_children){
      new_node = new_node->with_child_added( nchild);
    }
    
    return new_node;
  }
 
  //##########################################################
  SList< Box> Input_Info::grids_from_file( Length grid_h,
                                           const string& file) const{
      
    auto atoms = atoms_from_file( file);
    auto obounds = outer_box( atoms, solvent, grid_h);
    auto ibounds = inner_bounds( atoms, pad_fudge*solvent.srad);
    
    return box_list( atoms, solvent, ibounds, obounds, grid_h);
  }
  
  //################################################################
  void Core_Info::add_ag_attribute( size_t n_ext, Node_Ptr enode) const{
        
    auto echildren = enode->children_of_tag( "grid");
    for( auto& echild: echildren){
      echild->add_attribute( "source", "make_apbs_inputs");
    }
  }
  
  //##########################################################
  Node_Ptr Core_Info::enode_with_grid_nodes( 
                                   const Node_Ptr& enode0) const{
    
    auto enode = enode0;
    const string ext( ".dx");
    for( auto& name: grid_names){
      auto file = name + ext;
      enode = enode->with_child_added( "grid", file);
    }
    
    add_ag_attribute( ext.size(), enode);
    return enode;
  }
  
  //##########################################################
  Node_Ptr Input_Info::with_grid_nodes( const Node_Ptr& cnode) const{
    
    if( cnode->child( "electric_field")){
      return cnode;
    }
      
    {
      auto nf = value_from_node< string>( cnode, "no_field");
      if (nf){
        if (bool_of_str( nf.value())){
          return cnode;
        }    
      }
    }
    
    // otherwise
    auto cname = from_node< string>( cnode, "name");
    auto afile = from_node< string>( cnode, "atoms");
    auto grid_h = from_node< Length>( cnode, "grid_spacing");
    auto sdielectric = from_node< double>( cnode, "dielectric");
  
    auto boxes = grids_from_file( grid_h, afile);
  
    Node_Ptr enode{ new Node( "electric_field", cnode->parser(),
                               Node::Initially_Complete())};
      
    Core_Info core_info( *this);
    core_info.name = cname;
    core_info.atom_file = afile;
    core_info.boxes = boxes;
    core_info.grid_h = grid_h;
    core_info.solute_dielectric = sdielectric;
    core_info.output_apbs_files();
      
    auto enode1 = core_info.enode_with_grid_nodes( enode);
    enode1->add_attribute( "checked", "true"); // perhaps not needed
    
    return cnode->with_child_added( enode1);
    
  }
  
  //###########################################################
  Node_Ptr Input_Info::with_new_core_nodes( const Node_Ptr& gnode) const{
    
     auto cnodes = gnode->children_of_tag( "core");
     
     Vector< Node_Ptr> new_cnodes;
     std::transform( cnodes.begin(), cnodes.end(),
                    std::back_inserter( new_cnodes),
                    [&]( const Node_Ptr& cnode){
       return with_grid_nodes( cnode);});
       
     return with_replaced_nodes( gnode, new_cnodes, "core");
  } 
  
  //#############################################################
  Node_Ptr Input_Info::with_new_group_nodes( 
                                const Node_Ptr& system_node) const{
        
    auto gnodes = system_node->children_of_tag( "group");
    
    Vector< Node_Ptr> new_gnodes;
    std::transform( gnodes.begin(), gnodes.end(),
                   std::back_inserter( new_gnodes),
                   [&]( const Node_Ptr& gnode){
      
      return with_new_core_nodes( gnode);});
    
    return with_replaced_nodes( system_node, new_gnodes, "group");
  }
  
  //#################################################################
  void Input_Info::main( const string& file){
    
    auto top = xml_from_file( file).second;
    top->complete();
    auto system_node = top->child( "system");
    if (!system_node){
      top->perror( "system not found");
    }
      
    auto snode = solvent_node( system_node);
    get_solvent( snode);
    auto new_snode = new_solvent_node( snode);
    auto system_node1 =
      system_node->with_child_replaced( "solvent", new_snode);
    
    auto system_node2 = with_new_group_nodes( system_node1);
    
    auto top2 = top->with_child_replaced( "system", system_node2);
    top2->output( std::cout);
  }

  //########################################  
  void main0( int argc, char* argv[]){
    
    if (argc < 2){
       error( "need input file name\n");
    }
     
    if (has_flag( argc, argv, "-help") ||
        has_flag( argc, argv, "--help")){
      
      std::cout << "takes input file as input and outputs to"
                    " standard output a modified input file,"
                    " while generating .in files for APBS\n";
    }
    else{ 
      std::string file( argv[1]);
      Input_Info iifo;
      iifo.main( file);
    }
  }
}

//##########################################
int main( int argc, char* argv[]){
  main0( argc, argv);  
}
