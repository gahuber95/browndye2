#pragma once

#include <map>
#include "../lib/vector.hh"
#include "../global/error_msg.hh"

namespace Browndye{

template< class Atom_Index>
Atom_Index anumber( const std::map< Atom_Index, Atom_Index>& map, Atom_Index i){
  auto itr = map.find( i);
  if (itr == map.end())
    error( __FILE__, __LINE__, "atom number ", i,  " does not exist");
  return itr->second;
}

//##################################################################
/*
template< class Atom_Index>
std::map< Atom_Index, Atom_Index> atom_number_map( const Vector< Atom_Index, Atom_Index>& numbers){

  std::map< Atom_Index, Atom_Index> imap{};
  for( auto i: range( numbers.size())){
  //for (auto i = Atom_Index(0); i < numbers.size(); ++i){
    auto res = imap.insert( std::make_pair( numbers[i], i));
    if( !res.second)
      error( __FILE__, __LINE__, "duplicate atom number ", numbers[i]);
  }
    
  return imap;
}
*/
}

