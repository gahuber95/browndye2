#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#include <string>
#include "../lib/vector.hh"

/*
Extracts arguments from the command-line input to a program,
using flags to denote the arguments.

For example, if the user calls
program -f 12.3
and inside "program" is the command 
get_double_arg( argc, argv, "-f", ff);

where argc and argv are passed into "main",
then "ff" will have the value 12.3.

lone_arg grabs the first string argument that is not a flag or preceded by a flag
*/

namespace Browndye{

double double_arg( int argc, char* argv[], const char* flag, double defalt = NAN);

[[maybe_unused]] std::optional< double>
double_opt_arg( int argc, char* argv[], const char* flag);

[[maybe_unused]] int int_arg( int argc, char* argv[], const char* flag);

std::string string_arg( int argc, char* argv[], const std::string& flag);

std::optional< std::string>
string_arg_opt( int argc, char* argv[], const std::string& flag);

[[maybe_unused]] Vector< std::string> string_args( int argc, char* argv[], const std::string& flag);

[[maybe_unused]] void print_help( int argc, char* argv[], const std::string& msg);

[[maybe_unused]] bool has_flag( int argc, char* argv[], const std::string& flag);

[[maybe_unused]] std::string lone_arg( int argc, char* argv[]);

}

