(* 
Not in tool chain, but might be useful.
Reads a pqrxml file in standard input, and generates a parameter file from a 
pqrxml file such that the resulting force is a repulsive 1/r^12 force with 
energy equal to kT (at 298 K) when the distance is the sum of atom radii
*)


Arg.parse []
  (fun arg -> Printf.printf "no arguments\n"; exit 0)
  "Generates a parameter file from a pqrxml file such that the resulting force is a repulsive 1/r^12 force with energy equal to kT (at 298 K) when distance is sum of atom radii"
;;

module H = Hashtbl;;

let radii = H.create 0;;

Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin
  ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
    ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
     let key = aname,rname in
       H.replace radii key r
  )
;;  

let residues = H.create 0;;
  
H.iter
  (fun key r ->
     let atom, residue = key in
       
       if not (H.mem residues residue) then
	 H.add residues residue (H.create 0);

       let atoms = H.find residues residue in
	 H.replace atoms atom r
  )
  radii
;;

let pr = Printf.printf;;

pr "<parameters>\n";;

let eps0 = 1.0;;
let sfactor = 4.0;;

let eps = eps0/.( sfactor**12.0 -. 2.0*.sfactor**6.0);;

H.iter
  (fun residue atoms ->
     pr "  <residue>\n";
     pr "    <name> %s </name>\n" residue;

     H.iter
       (fun atom r -> 
	  pr "    <atom>\n";
	  pr "      <name> %s </name>\n" atom;

	  pr "      <radius> %g </radius>\n" (sfactor*.r);
	  pr "      <epsilon> %g </epsilon>\n" eps;
	  
	  pr "    </atom>\n";
       )
       atoms;

     pr "  </residue>\n";
  )
  residues
;;

pr "</parameters>\n";;
