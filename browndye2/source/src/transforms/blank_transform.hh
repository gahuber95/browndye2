#pragma once

#include "../lib/units.hh"
#include "../global/pos.hh"

namespace Browndye{

class Blank_Transform{
  
public:
  Pos translation() const{
    return Pos( Zeroi());
  }

  Pos translated( const Pos& before) const{
    return before;
  }

  template< class U>
  Vec3< U> rotated( Vec3<U> before) const{
    return before;
  }

  Mat3< double> rotation() const{
    return id_matrix<3>();
  }

  Pos transformed( Pos before) const {
    return before;
  }

  Blank_Transform inverse() const{
    return Blank_Transform();
  }

  Blank_Transform operator*( Blank_Transform) const{
    return Blank_Transform();
  }

  void set_zero(){}
};

}

