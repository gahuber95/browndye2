/*
 * tet.cc
 *
 *  Created on: Aug 26, 2016
 *      Author: root
 */


#include "tet.hh"
#include "../group/group_state.hh"
#include "resistance_matrices.hh"
#include "../motion/constraint_tolerance.hh"


namespace Browndye{

using ::std::move;
using ::std::get;
using ::std::make_pair;
using ::std::pair;

//V4< Vec3< double> > unit_tetrahedron();

constexpr Atom_Index ia4( ntet);

//############################################
Mat3< double> triad_matrix( Pos r1, Pos r2){
  // from Triad Method (wikipedia)
  Mat3< double> smsmt;
  auto& sh = smsmt[0];
  auto& mh = smsmt[1];
  auto& scmh = smsmt[2];
  
  sh = normed( r1);
  mh = normed( cross( r1,r2));
  scmh = normed( cross( sh,mh));
  
  return smsmt;
}

constexpr Atom_Index a4( 4);

//##############################################
std::tuple< Atom_Index, Pos, Atom_Index, Pos>
best_triad_pair( const V4< Pos>& poses){
   
  auto com0 = centroid( poses);
    
  V4< Pos> ds;
  for( auto i: range(a4))
    ds[i] = poses[i] - com0;
  V4< Length> dnorms;
  for( auto i: range(a4))
    dnorms[i] = norm( ds[i]);
  
  Atom_Index i1, i2;
  Pos r1( NaNi{}),r2( NaNi{});
  Vec3< Length2> ucp;
  Length4 cp2{ 0.0};
  for( auto i: range(a4)){
    auto dsi = ds[i];
    for( auto j: range(i)){
      auto dsj = ds[j];
      auto trial_cp = cross( dsi, dsj);
      auto trial_cp2 = norm2( trial_cp);
      if (trial_cp2 > cp2){
        cp2 = trial_cp2;
        ucp = trial_cp;
        if (dnorms[i] > dnorms[j]){
          r1 = ds[i];
          i1 = i;
          r2 = ds[j];
          i2 = j;
        }
        else{
          r1 = ds[j];
          i1 = j;
          r2 = ds[i];
          i2 = i;
        }
      }
    }
  } 
   
  return make_tuple( i1,r1, i2,r2); 
}

//###########################################################################
// assumes tet constraints are satisfied
Transform quick_tform_of_tet( const Tet_Fixed& fixed,
                                    const V4< Pos>& xposes){
  
  auto com = centroid( xposes);
  
  Mat3< double> SMSMt;
  auto& Sh = SMSMt[0];
  auto& Mh = SMSMt[1];
  auto& ScMh = SMSMt[2];
  
  auto i1 = fixed.i1;
  auto i2 = fixed.i2;
  auto& smsmt = fixed.smsmt;
  auto& com0 = fixed.com0;
  
  Sh = normed( xposes[ i1] - com);
  auto r2 = xposes[ i2] - com;
  Mh = normed( cross( Sh, r2));
  ScMh = normed( cross( Sh, Mh));
  
  auto rot = transpose( SMSMt)*smsmt;
  
  Length L0( 0.0);
  
  Pure_Translation back_shift( -com0), forward_shift( com);
  Transform rott( rot, Pos{ L0,L0,L0});
  
  return forward_shift*rott*back_shift;
}

//#############################################################
Transform Tet_Base::transform_of() const{
 
  return quick_tform_of_tet( *fixed, poses);
}

//##########################################################
using std::move;

// constructor
Tet_Fixed::Tet_Fixed( const Core_Indices& _core_indices,
                 const Chain_Indices& _chain_indices,
                 Pos _com0, Mat3< double> _smsmt, Atom_Index _i1, Atom_Index _i2,
                 Vector< Transform, Ind_Core_Index>&& _core_tforms0,
                Vector< Vector< Pos, Atom_Index>, Ind_Chain_Index>&& _chain_poses0,
                 Length _length, Length _radius, V4< Pos> _poses0, Inv_Length _tmob, Inv_Length3 _rmob):
  core_indices( _core_indices.copy()),
  chain_indices( _chain_indices.copy()),
  com0(_com0), smsmt( _smsmt), i1(_i1), i2(_i2),
  core_tforms0( std::move(_core_tforms0)),
  chain_poses0( std::move( _chain_poses0)),
  length( _length), radius( _radius), poses0( _poses0),
    tmob(_tmob),rmob(_rmob)
{
  //imin_core = core_indices.folded_left( [&]( Core_Index res, Core_Index i){
  //  return std::min( res, i);
  //}, Core_Index( maxi));
}

//##########################################
std::tuple< V4< Pos>, Length, Length>
Tet_Base::blank_small_tet(){
  
  auto uverts = unit_tetrahedron();
  V4< Pos> poses;
  Length d(1.0);
  for( auto i: range( ia4)){
    poses[i] = d*uverts[i];
  }
  
  return make_tuple( poses, d, Length( NaN));  
}

//###########################################################################
constexpr double hi_rad_factor = 0.874104;
constexpr double nohi_rad_factor = 0.25;

pair< Length, Length>
Tet_Base::equivalent_tet_geometry_hr( bool has_hi, Length hradius){

  Length d, radius;
  if (has_hi){
    radius = 0.4708*hradius;
    d = 0.7768*hradius;
    //radius = hi_rad_factor*hradius;
    //d = 0.327552*radius;
  }
  else {
    radius = 0.25*hradius;
    d = 1.155*hradius;
    //radius = nohi_rad_factor*hradius;
    //d = sqrt( cube( hradius)/(3.0*radius));
  }
  
  return make_pair( d, radius);
}

//###############################################
Length Tet_Base::hi_radius( bool has_hi) const{
  
  auto rad = fixed->radius;
  if (has_hi){
    return rad/hi_rad_factor; 
  }
  else{
    return rad/nohi_rad_factor;
  }
}

//######################################################################
[[maybe_unused]] Small_Tet test_tet( Length d, Length bradius){
  
  auto uverts = unit_tetrahedron();
  Small_Tet tet0;
  tet0.radius = bradius;
  auto& poses = tet0.poses;
  for( auto i: range(ia4)){
    poses[i] = d*uverts[i];
  }
  return tet0;
}

//####################################################################
bool Tet_Base::constraints_not_far_off() const{
  
  auto L = fixed->length;
  
  for( auto i: range( Atom_Index(4))){
    auto posi = poses[i];
    for( auto j: range(i)){
      auto posj = poses[j];
      auto d = distance( posi, posj);
      if (fabs( d - L)/L > constraint_backstep_tol){
        return false;
      }
    }
  }
  
  return true;     
}

//#######################################
// turn into unit test
class Tet_Tester{
public:
  static
  void test();
  
  Tet_Tester( bool _hi): hi(_hi){}
  
  const bool hi;
  
  struct Fake_Atom{
    Pos pos;
    Length radius;
  };
  
  struct Fake_Chain_Common{
    Vector< Fake_Atom, Atom_Index> atoms;
  };
  
  struct Fake_Chain_State{
    Atom_Index n_atoms() const{
      return Atom_Index(0);
    }
    
    Fake_Chain_Common _common;
    const Fake_Chain_Common& common() const{
      return _common;
    }
    
    Vector< Fake_Atom, Atom_Index> atoms;
  };
  
  class GS{
  public:
    GS( bool _hi): hi(_hi){}
    
    const bool hi;
    typedef Fake_Chain_State Chain_State;
    bool has_hi() const { return hi;}
    Vector< Fake_Chain_State, Chain_Index> chain_states;
  };
  
};
   
//##########################################################
/*
void Tet_Tester::test(){
  
  const bool hi = false; // test both cases
  auto [d,bradius] = equivalent_tet_geometry( hi, Length( 1.0));
  
  std::cout << d << ' ' << bradius << std::endl;
  
  Small_Tets stets{ test_tet( d, bradius)};
  Chain_Indices chain_indices;
  
  GS gs( hi);
  Bead_Info< GS> binfo( gs, stets, chain_indices);
  
  auto [tmob,cen] = Tet< GS>::mobility_and_center( binfo);
  
  auto [d1,bradius1] = equivalent_tet_geometry_from_mobs( hi, tmob);
    
  std::cout << d1 << ' ' << bradius1 << std::endl;
}
*/

//##################################################
typedef Chain_State Alt_Chain_State;

void generate_mobility_ratio(){

  const bool has_hi = true;
  
  auto uposes = unit_tetrahedron();
  Length a( 1.0);
  
  struct Fake_Group{
    typedef Alt_Chain_State Chain_State;
    Vector< Chain_State, Chain_Index> chain_states;
  };
  
  Chain_Indices cis;
  Small_Tets stets;
  auto& stet = stets.emplace_back();
    
  auto print_ratio = [&]( double x){
    
    // half-length of tet side
    Length d(x);
    
    for( auto k: range( Atom_Index(4))){
      stet.poses[k] = d*uposes[k];
    }
    
    stet.radius = a;
    
    Bead_Info< Fake_Group> binfo{ Fake_Group{}, stets, cis};
    
    auto rmats = [&]{
      if (has_hi){
        return resistance_matrices_hi( binfo);
      }
      else{
        return resistance_matrices_nohi( binfo);
      }
    }();
        
    auto A = rmats.A;
    auto C = rmats.C;
    auto tres = trace( A)/3.0;
    auto rres = trace( C)/3.0;
    auto tmob = 1.0/tres;
    auto rmob = 1.0/rres;
    // should be 1
    auto ratio = rmob/(tmob*sq(tmob)*27.0*pi*pi);
    std::cout << d << ' ' << 1 - ratio << std::endl;
        
    auto R = 1.0/(pi6*tmob);    
    std::cout << "a factor " << a/R << std::endl;
    std::cout << "d factor " << d/R << std::endl;
  };

  if (has_hi){  
    print_ratio( 1.65);
  }
  else{
    print_ratio( 4.62);
  }
}

}

/*
int main(){
  //Browndye::Tet_Tester::test();
  Browndye::generate_mobility_ratio();
}
*/
