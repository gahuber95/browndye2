#pragma once

#include <list>
#include "multithread_mover_base.hh"


#include "multithread_mover_pre.hh"

namespace Browndye{
namespace Multithread{
/*
Given a container of objects and a function to execute on each object,
the Mover class divides up the work among several threads.

It wraps Mover_Base, but can handle arbitrary numbers of objects.
The interface is the same; see "multithread_mover_base.hh".

*/
 
//******************************************************************
template< class I>
class Mover{
public:
  typedef typename I::Exception Exception;
  typedef typename I::Objects_Ref Objects;
  typedef typename I::Object_Ref Object;
  typedef void (*Function)( Objects, Object);

  void do_moves();
  Mover();
  ~Mover();
  void set_objects( Objects);
  void set_number_of_threads( unsigned int);
  
  void check_for_interruption() const{
    mover.check_for_interruption();
  }

private:
  friend class MT_Interface< I>;

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects objects){
    I::apply_to_object_refs( f, objects);
  }

  void do_move( Objects lobjects, unsigned int ithread, Object obj){
    I::do_move( *this, lobjects, ithread, obj);
  }

  static void set_null( Objects& objects){
    I::set_null( objects);
  }

  static bool is_null( Objects objects){
    return I::is_null( objects);
  }


  //**********************************************
  Mover_Base< MT_Interface< I> > mover; 
  unsigned int n_threads;
  std::list< Object> object_queue;
  //pthread_mutex_t mutex;
  std::mutex mutex;
  Objects objects;
  bool started;

  void check_objects();
  void thread_move( unsigned int ithread);
};

  // constructor
template< class I>
Mover< I>::Mover(){
  //pthread_mutex_init( &mutex, NULL);
  I::set_null( objects);
  n_threads = 1;
  started = false;
}

template< class I>
void Mover< I>::set_number_of_threads( unsigned int n){
  started = false;
  n_threads = n;
}

  // destructor
template< class I>
Mover< I>::~Mover(){
  //pthread_mutex_destroy( &mutex);
}

template< class I>
void Mover< I>::set_objects( Objects obj){
  objects = obj;
  started = false;
}

template< class I>
class Object_Getter{
public:
  typedef typename I::Object_Ref Object;

  Object_Getter( std::list< Object>& oq): object_queue( oq){}

  void operator()( Object obj){
    object_queue.push_back( obj);
  }

  std::list< Object>& object_queue;
};

template< class I>
void Mover< I>::check_objects(){
  if (is_null( objects))
    error( "Mover: objects not yet set");
}

template< class I>
void Mover< I>::do_moves(){
  if (!started){
    mover.set_objects( this);
    started = true;
  }

  Object_Getter< I> og( object_queue);
  apply_to_object_refs( og, objects);

  mover.do_moves();
}

template< class I>
void Mover< I>::thread_move( unsigned int ithread){
  while (true){
    std::lock_guard lock( mutex);
    if (object_queue.empty()){
      break;
    }
    else{
      Object object = object_queue.back();
      object_queue.pop_back();
      do_move( objects, ithread, object);
    }
  }
}

}
}
//************************************************************************
/* Example, test code


#include "multithread_mover.hh"

double taylor_exp( double x){
  unsigned int n = 1000000;
  double den = 1.0;
  double sum = 1.0;
  double power = 1.0;
  for (unsigned int i = 1; i<n; i++){
    den *= i;
    power *= x;
    sum += power/den;
  }
  return sum;
}

class Interface{
public:

  typedef Jam_String::String< char> Exception;
  typedef unsigned int Object_Ref;
  typedef std::vector< double>* Objects_Ref;

  template< class F>
  static
  void apply_to_object_refs( F& f, Objects_Ref objs){
    for( unsigned int i = 0; i < objs->size(); ++i)
      f( i);
  }

  static
  void do_move( Objects_Ref objs, unsigned int ithread, int i){
    double x = (*objs)[i];
    printf( "%d %d %g %g\n", ithread, i, x, taylor_exp( x));
  }

  static
  void set_null( Objects_Ref& objs){
    objs = nullptr;
  }

  static
  bool is_null( Objects_Ref objs){
    return (objs == nullptr);
  }

 
};

int main(){

  const unsigned int n = 100;
  std::vector< double> vec( n);
  for( unsigned int i = 0; i < n; i++)
    vec[i] = (double)i/n;

  Multithread::Mover< Interface> mover;

  mover.set_objects( &vec);
  mover.set_number_of_threads( 4);
  mover.do_moves();

}


*/


