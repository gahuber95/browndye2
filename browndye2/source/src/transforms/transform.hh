#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
The transform class contains the affine rigid body transformation
(rotation and translation), implemented as a 4x4 matrix.
*/


#include "../lib/units.hh"
#include "../global/pos.hh"

namespace Browndye{

class Blank_Transform;

class Transform{
public:

  Transform();
  Transform( const Mat3< double>& rot, const Pos& trans);

  Transform( const Transform&) = default;
  Transform( Transform&&) = default;
  Transform& operator=( const Transform&) = default;
  Transform& operator=( Transform&&) = default;

  Transform inverse() const;
  Pos translated( const Pos&) const;
  Pos transformed( const Pos&) const;

  template< class U>
  Vec3< U> rotated( const Vec3< U>&) const;

  Pos translation() const;

  Mat3< double> rotation() const;

  Transform fractional_tform( double frac) const;

  friend
  Transform operator*( const Transform&, const Transform&);

  friend
  Transform operator*( const Blank_Transform&, const Transform&);

  friend
  Transform operator*( const Transform&, const Blank_Transform&);

  Transform translated_t( Pos) const;

  Transform with_rotation( Mat3< double>) const;
  
  friend
  Transform interped_tform( const Transform&, const Transform&);
  
private:
  Mat3< double> R;
  Pos t;
};

template< class U>
Vec3< U> Transform::rotated( const Vec3< U>& r) const{
  return R*r;  
}

inline
Transform Transform::translated_t( Pos d) const{
  
  Transform res( *this);
  res.t += d;
  return res;  
}

inline
Transform operator*( const Blank_Transform&, const Transform& tform){
  Transform res( tform);
  return res;
}

inline
Transform operator*( const Transform& tform, const Blank_Transform&){
  Transform res( tform);
  return res;
}


inline
Transform Transform::with_rotation( Mat3< double> rot) const{
  Transform res( *this);
  res.R = rot;
  return res;
}

// constructor
inline
Transform::Transform(): R( id_matrix<3>()), t( Zeroi{}){}

// constructor
inline
Transform::Transform( const Mat3< double>& rot, const Pos& trans):
  R( rot), t( trans){}

inline
Mat3< double> Transform::rotation() const{
  return R;  
}

inline
Pos Transform::translated( const Pos& r) const{
  return r + t;
}

inline
Pos Transform::transformed( const Pos& r) const{
  
  return translated( rotated( r));  
}

inline
Pos Transform::translation() const{
  return t;
}

inline
Transform operator*( const Transform& T0, const Transform& T1){
  return Transform( T0.R*T1.R, T0.R*T1.t + T0.t);  
}

}

