#pragma once

/*
 * chain_state.hh
 *
 *  Created on: Jul 19, 2016
 *      Author: root
 */

#include "chain_common.hh"
#include "../../transforms/transform.hh"
#include "chain_state_atom.hh"
#include "../../forces/electrostatic/field.hh"
#include "../../forces/electrostatic/born_field_interface.hh"
#include "../../forces/electrostatic/v_field_interface.hh"
#include "../../group/test_tag.hh"

namespace Browndye{

class Group_Common;

class Chain_State{
public:
  Chain_State() = delete;
  Chain_State( const Chain_State& other);
  Chain_State& operator=( const Chain_State& other);
  Chain_State( Chain_State&& other) = default;
  Chain_State& operator=( Chain_State&& other) = delete;

  Chain_State( const Chain_Common& chain,
                        const Vector< Group_Common, Group_Index>&);

  Chain_State(Test_Tag, const Chain_Common &); // for testing

  [[nodiscard]] const Chain_Common& common() const{
    return chain_ref.get();
  }

  [[nodiscard]] Pos hydro_center() const;
  void shift( Pos);
  
  //[[nodiscard]] Length atom_radius( Atom_Index) const;
  
  template< class TForm>
  void transform( TForm);
  
  void reset_to_start( const Vector< Group_Common, Group_Index>&);

  [[nodiscard]] std::optional< Time> constant_dt() const;

  // type definitions
  typedef Chain_Col_Interface::Container Chain_Container;
  typedef Collision_Detector::Structure< Chain_Col_Interface> Chain_Collision_Structure;

  struct Chain_Collision_Info{
    Chain_Container container;
    Chain_Collision_Structure col_struct;
    
    explicit Chain_Collision_Info( Chain_State&);
    Chain_Collision_Info( Chain_Collision_Info&&) noexcept ;
    
    Chain_Collision_Info& operator=( Chain_Collision_Info&& other) = delete;
  };

  [[nodiscard]] Atom_Index n_atoms() const;

  Group_Index group_number() const;

  // data
  std::reference_wrapper< const Chain_Common> chain_ref;
  Vector< Chain_State_Atom, Atom_Index> atoms;
  bool frozen = false;
  bool has_near_interaction = false;
  
  typedef Field::Field< V_Field_Interface>::Hint VHint;
  typedef Field::Field< Born_Field_Interface>::Hint BHint;
  
  Vector< Vector< VHint, Core_Index>, Group_Index> v_hints;
  Vector< Vector< BHint, Core_Index>, Group_Index> born_hints;
  
  // used in force_impl
  std::unique_ptr< Chain_Collision_Info> collision_info;
  
  std::optional< Tet_Index> itet;
  
  //[[nodiscard]] Length rms_constraint_error() const;
  
  #ifdef DEBUG
  ~Chain_State(){
    collision_info.reset();
  }
  #endif
  
private:    
  void reset_hints( const Vector< Group_Common, Group_Index>&);
  
  void copy_from( const Chain_State&);
};

//############################################
template< class TForm>
void Chain_State::transform( TForm tform){
  for( auto& atom: atoms)
    atom.pos = tform.transformed( atom.pos);
}

//###############################################
/*
inline
Length Chain_State::atom_radius( Atom_Index ia) const{
  return common().atoms[ia].radius;
}
 */

inline
std::optional< Time> Chain_State::constant_dt() const{
  return common().const_dt;
}

inline
Atom_Index Chain_State::n_atoms() const{
  return atoms.size();
}

inline
void Chain_Col_Interface::does_have_near_interaction( Browndye::Chain_Col_Interface::Container& cont) {
  cont.chain.has_near_interaction = true;
}

inline
Group_Index Chain_State::group_number() const {
  return common().ig;
}

}


