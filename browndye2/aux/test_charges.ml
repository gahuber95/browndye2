(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)
(* 
Convenience function.  Generates a test charge for each sphere.
Uses standard input and output.
*)


Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates test charge for each sphere."
;;


let pr = Printf.printf;;

pr "<root>\n";;

let total_q = ref 0.0;;

 Atom_parser.apply_to_atoms ~buffer: Scanf.Scanning.stdin
   ~f: (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
     ~x:x ~y:y ~z:z ~radius:r ~charge:q ->

     if ((abs_float q) > 0.0) then(
       pr "  <point>\n";
       pr "    <residue> %s </residue>\n" rname;
       pr "    <number> %d </number>\n" rnumber;
       pr "    <atom_type> charge-center </atom_type>\n";
       pr "    <x> %g </x> <y> %g </y> <z> %g </z>\n" x y z;
       pr "    <charge> %g </charge>\n" q;
       pr "  </point>\n";
       total_q := !total_q +. q;
     )
   )
;;

pr "  <total_charge> %g </total_charge>\n" !total_q;;

pr "</root>\n";;

