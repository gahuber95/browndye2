#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Generic code for a Cartesian-based multipole implementor.  See
doc/multipole.html for details.

Interface:

typedef size_type

unsigned int I::maximum_multipole_order( const Container&, const Refs&);

template< class Function>
void I::apply_multipole_function( const Container&, const Refs&, Function& f, 
                                       double* r, double* mpole);

f has operator() overloaded: ( const unsigned int order)

Before each time f is called, the user must place the
object position in 'r' and its multipole in 'mpole'.
Then, f is called with the multipole order placed in the
argument 'order'.

template< class Function>
void I::apply_potential( Container&, Refs&, Function& f)

f has operator() overloaded: double ( const double* r)

The position is placed in r, and the potential is returned.

template< class Function>
void I::apply_field( Container&, Refs&, Function& f)

f has operator() overloaded: void ( const double* r, double* field)  

void I::process_bodies( Container&, Refs&)

void I::process_bodies( Container&, Refs&, Refs&)


void I::split_container( Container&, Refs&, unsigned int dir,
                                 Refs* const cube[][2][2])


void I::empty_references( Container&, Refs&);

void I::copy_references( Container&, Refs&);
void I::copy_references( Container&, Refs&, Refs&);

size_type I::size( Container&, const Refs&);
size_type I::n_sources( Container&, const Refs&);
size_type I::n_receivers( Container&, const Refs&);


void I::get_bounds( Container&, const Refs&,  
                         double* low, double* high)


Must provide derivatives up to "order"
void I::get_r_derivatives( Container&, unsigned int order, double r, double* derivs)


double I::near_potential( Container&, Refs&, const double* r);

void I::get_near_field( Container&, Refs&, const double* r,
                             double* F);

*/

#include <memory>
#include "../lib/array.hh"
#include "../lib/vector.hh"
#include "../lib/sq.hh"



namespace Browndye::Cartesian_Multipole{

static const unsigned int X = 0;
static const unsigned int Y = 1;
static const unsigned int Z = 2;

enum Contained_Type {Sources, Receivers, Both, None};

inline
void zero( Vector< double>& v){
  v.fill( 0.0);
}

typedef Vector< unsigned int> Ivec1;
typedef Vector< Ivec1> Ivec2;
typedef Vector< Ivec2> Ivec3;


class Index3{
public:
  Index3(){
    data[X] = 0;
    data[Y] = 0;
    data[Z] = 0;
  }

  unsigned int operator[]( unsigned int i) const{
    return data[i];
  }

  unsigned int& operator[]( unsigned int i){
    return data[i];
  }

private:
  Array< unsigned int, 3> data;
};

class Deriv_Term{
public:
  unsigned int f_deriv;
  unsigned int r_power;
  unsigned int xyz_power;
  int scale;

  Deriv_Term(){
    f_deriv = 0;
    r_power = 0;
    xyz_power = 0;
    scale = 0;
  }
};

unsigned int n_indices( unsigned int order);

class Multipole_Info{

public:
  explicit Multipole_Info( unsigned int _order);
  
  void generate_scaled_rfactors( const Vec3< double>& r, 
                                 Vector< double>& factors) const;

  void generate_rfactors( const Vec3< double>& r, 
                          Vector< double>& factors) const;

  void add_shifted_polynomial( unsigned int lorder, const Vector< double>& cps,
                               const Vector< double>& vs0,
                               Vector< double>& vs) const;
  
  void add_taylor_expansion( const Vector< double>& qs,
                             const Vector< double>& ds,
                             Vector< double>& vs) const;

  [[nodiscard]] double first_taylor_term( const Vector< double>& qs,
                             const Vector< double>& ds) const;
  
  template< class Moments>
  void add_shifted_multipoles( unsigned int lorder,
                               const Vector< double>& cps,
                               const Moments& qs0,
                               Vector< double>& qs) const;

  void generate_greens_derivatives( const Vector< double>& rfactors,
                                    const Vector< double>& fderivs,
                                    Vector< double>& derivs
                                    ) const;

  [[nodiscard]] unsigned int number_of_indices() const{
    return ni;
  }
  
  //void print_info( FILE* fp) const;

  // default 20
  void set_max_number_per_cell( unsigned int n){
    n_per_cell = n;
  }

  [[nodiscard]] unsigned int max_number_per_cell() const{
    return n_per_cell;
  }

  // diameter to center-to-center distance
  // default 
  void set_distance_ratio( double r){
    ratio = r;
  }

  [[nodiscard]] double distance_ratio() const{
    return ratio;
  }

  void generate_reversed_cs( const Vector< double>& cs0,
                             Vector< double>& cs) const;

  [[nodiscard]] unsigned int multipole_order() const{
    return order;
  }

 static double factorials[];

private:
  void generate_indices();
  void generate_responsibilities();
  void setup_inv_indices();
  void generate_convsum_array();
  void generate_trifactorials();
  void generate_minus_one_powers();
  void generate_derivative_coefficients();      

  const unsigned int order;
  const unsigned int ni;
  Vector< Index3> indices;
  Vector< Index3> resps;
  Ivec3 inv_indices;
  Vector< double> trifacts;
  Ivec2 convsum;
  Vector< Vector< Deriv_Term> > deriv_coefs;
  Vector< double> m1powers;

  size_t n_per_cell;
  double ratio;  
};

template< class Moments>
void Multipole_Info::add_shifted_multipoles( const unsigned int lorder,
                                             const Vector< double>& cps,
                                             const Moments& qs0,
                                             Vector< double>& qs) const{
   
  const unsigned int nil = n_indices(lorder);

  for (unsigned int m = 0; m < nil; m++){
    const Ivec1& cvsm = convsum[m];
    const double q0 = qs0[ m];
    for (unsigned int p = 0; p < cvsm.size(); p++){
      qs[ cvsm[p]] += q0*cps[p];  
    }
  }
}



template< class I>
class Far_Nebor_Interaction_Info{
public:
  typedef Vector< double> DS;
  typedef Array< Array< Array< DS, 7>, 7>, 7> Cube;

  Vector< Cube> ds_cubes;
  
  void set_up( const Multipole_Info& info, 
               typename I::Container& cont, 
               unsigned int n_levels, double toph){

    // later change to avoid recalculating with each setup
    if (n_levels > 2){
      ds_cubes.resize( n_levels-2);

      double h = toph/4;
      for (unsigned int ilevel = 2; ilevel < n_levels; ++ilevel){
        Cube& ds_cube = ds_cubes[ ilevel-2];
        
        Vec3< double> diff;
        for (int ix = -3; ix <= 3; ix++){
          diff[X] = ix*h;
          for (int iy = -3; iy <= 3; iy++){
            diff[Y] = iy*h;
            for (int iz = -3; iz <= 3; iz++){
              diff[Z] = iz*h;

              if (ix != 0 || iy != 0 || iz != 0){
                double r = sqrt( sq( diff[0]) + sq( diff[1]) + sq( diff[2]));

                Vector< double> rs;
                info.generate_rfactors( diff, rs);

                Vector< double> fderivs( info.multipole_order()+1);
                
                I::get_r_derivatives( cont, info.multipole_order(), 
                                              r, fderivs);
                
                DS& ds = ds_cube[ix+3][iy+3][iz+3];
                ds.resize( info.number_of_indices());
                info.generate_greens_derivatives( rs, fderivs, ds);
              }
            }
          }
        }       
        
        h /= 2;
      }
    }
  }
  
};

//###############################################################
template< class T>
class Cube_2x2{
public:
  const T& operator()( unsigned int ix, unsigned int iy, unsigned int iz) const{
    check( ix,iy,iz);
    return data[ix][iy][iz];
  }

  T& operator()( unsigned int ix, unsigned int iy, unsigned int iz){
    check( ix,iy,iz);
    return data[ix][iy][iz];
  }

private:
  void check( unsigned int ix, unsigned int iy, unsigned int iz) const{
    if (debug)
      if (ix > 2 || iy > 2 || iz > 2){
	      char msg[1000];
        sprintf( msg, "Cube_2x2 out of bounds %d %d %d\n", ix, iy, iz);
        error( msg);
      }
  }
  

  T data[2][2][2];
};


template< class I>
class Calculator;

template< class I>
class Potential_Computer;

//################################################################
template< class I>
class Cell{
public:
  //typedef Cell< I> MC;
  typedef Multipole_Info MI;
  typedef typename I::Container Cr;
  typedef Far_Nebor_Interaction_Info< I> Fnii;

  explicit Cell( const MI&);

  void create_top_cell( const MI&, Cr&, double);
  void compute_expansion( const MI& info, const Fnii&, Cr& conr);
  void compute_interactions_with_potential( const MI&, Cr&);
  void compute_interactions_with_field( const MI&, Cr&);
  //void reload_contents( Cr&);
  void even_bottoms( const MI&, Cr& conr);
  Cell* neighbor( int, int, int);

  //void print( FILE*) const;
    
private:
  typedef typename I::Refs Refs;
  typedef typename I::Container Container;
  friend class Calculator< I>;
  friend class Potential_Computer<I>;

  void split_cells( const MI&, Cr&);
  void split_this_cell( const MI&, Cr&, int flag);
  //void compute_center_and_diameter();
  //static void link_cousins( const MI& info, Cell& cell0, Cell& cell1);
  //void link_brothers( const MI& info);

  void do_sideways_sweep( const MI& info, const Fnii&);
  void do_upward_sweep( const MI& info, Cr&);

  //void split_cells( const MI& info);

  void do_downward_sweep( const MI& info);
  void even_bottoms_inner( const MI&, Cr& conr, bool&);

  double potential_at_point( const Multipole_Info&,
               const Vector< double>&,
                const Vector< double>&, Vector< double>&) const;

  [[nodiscard]] bool is_bottom() const{
    bool res = true;
    for (unsigned int ix = 0; ix < 2; ix++){
      for (unsigned int iy = 0; iy < 2; iy++){
        for (unsigned int iz = 0; iz < 2; iz++){
          if (children(ix,iy,iz)){
            res = false;
            goto loopend;
          }
        }
      }
    }
  loopend:
    return res;    
  }

  //void reload_cells( Cr&);

  [[nodiscard]] unsigned int depth() const;

  void apply_with_nephews( Container& conr, 
                           Cell* cousin);
    
  void set_contained_type( Container& conr);

  [[nodiscard]] bool has_receivers() const{
    return contained == Receivers || contained == Both;
  }

  [[nodiscard]] bool has_sources() const{
    return contained == Sources || contained == Both;
  }

  bool do_pair( const Cell< I>* cell1) const;

  void zero_out_sums();

  /*
  struct Far_Nebor_Info{
    Vector< double> ds;
    Cell* nebor = nullptr;
  };
  */

  //*********************************
  Refs contents;
  
  Cube_2x2< std::unique_ptr<Cell> > children;

  Vector< double> qs;
  Vector< double> vs;
  Vector< double> cps; // from child to parent
  
  Vec3< double> center;
  double length;

  int idx,idy,idz;
  unsigned int level; // 0 is top
  Cell* parent = nullptr;
  Contained_Type contained;

  //*********************************
  struct Particle_Multipole_Getter{
    Vector< double> cps;
    
    Cell* cell = nullptr;
    const Multipole_Info* info = nullptr;
    Vec3< double> r;

    Particle_Multipole_Getter(){
      double nan = std::numeric_limits<double>::quiet_NaN();
      for( int k = 0; k < 3; k++){
        r[k] = nan;
      }
    }

    template< class L3, class Moments>
    void operator()( const unsigned int order, const L3& pos, 
                     const Moments& moments){
   
      for( int k = 0; k < 3; k++){
        r[k] = pos[k] - cell->center[k];
      }
      info->generate_scaled_rfactors( r, cps);
      info->add_shifted_multipoles( order, cps, moments, cell->qs);
    }
  };

  struct Taylor_Applier{
    const Cell& cell;
    const MI& info;
    Vector< double> cps;
    Vector< double> vs;
    Vec3< double> dr;

    Taylor_Applier( const MI& _info, const Cell& _cell): 
      cell(_cell), info(_info), cps( _info.number_of_indices()), vs( 4){}
    
    
    template< class L3>
    void Do( unsigned int order, const L3& r){
      for( int k = 0; k < 3; k++){  
        dr[k] = r[k] - cell.center[k];
      }
      for (int i = 0; i < 4; i++){
        vs[i] = 0.0;
      }
      
      info.generate_scaled_rfactors( dr, cps);
      info.add_shifted_polynomial( order, cps, cell.vs, vs);
    }
  };
  
  struct Field_Applier: public Taylor_Applier{
    Field_Applier( const MI& info, const Cell& cell):
      Taylor_Applier( info, cell){}

    void operator()( const double* r, double* F){
      this->Do( 1, r);
      for( int k = 0; k < 3; k++){  
        F[k] = this->vs[k+1];
      }
    }
  };
  
  struct Potential_Applier: public Taylor_Applier{

    Potential_Applier( const MI& info, const Cell& cell):
      Taylor_Applier( info, cell){}

    template< class L3>
    double operator()( const L3& r){
      this->Do( 0, r);
      return this->vs[0];
    }
  };

  template< class T>
  static
  T max( T x, T y){
    return x > y ? x : y;
  }
};

//################################################################
template< class I>
double Cell<I>::potential_at_point( const Multipole_Info& info,
              const Vector< double>& rs,
              const Vector< double>& fderivs, Vector< double>& derivs) const{
  
  info.generate_greens_derivatives( rs, fderivs, derivs);
  return info.first_taylor_term( qs, derivs);
}

//###########################################################
// depth = 1 for bottom

template< class I>
unsigned int Cell< I>::depth() const{

  unsigned int ndep = 1;
  for (unsigned int ix = 0; ix < 2; ix++){
    for (unsigned int iy = 0; iy < 2; iy++){
      for (unsigned int iz = 0; iz < 2; iz++){
        auto& child = children( ix,iy,iz);
        if (child){
          ndep = max( ndep, 1 + child->depth());
        }
      }
    }
  }
  return ndep;
}

//######################################################
template< class I>
void Cell< I>::
compute_expansion( const Multipole_Info& info,
                   const Far_Nebor_Interaction_Info< I>& fn_info,
                   typename I::Container& conr){

  this->zero_out_sums();
  this->do_upward_sweep( info, conr);
  this->do_sideways_sweep( info, fn_info);
  this->do_downward_sweep( info);
}

//#####################################################
template< class I>
Cell<I>* Cell<I>::neighbor( int kx, int ky, int kz){
  
  if (kx == 0 && ky == 0 && kz == 0){
    return this;
  }
  else if (level == 0){
    return nullptr;
  }
  else{
    int jdx = idx + kx;
    int jdy = idy + ky;
    int jdz = idz + kz;

    int idx1 = jdx >> 1; 
    int idy1 = jdy >> 1; 
    int idz1 = jdz >> 1;

    int idx0 = parent->idx;
    int idy0 = parent->idy;
    int idz0 = parent->idz;
    
    auto uncle = parent->neighbor( idx1 - idx0, idy1 - idy0, idz1 - idz0);
    if (uncle){
      
      int kx1 = jdx - (idx1 << 1); 
      int ky1 = jdy - (idy1 << 1); 
      int kz1 = jdz - (idz1 << 1); 
      
      return uncle->children(kx1,ky1,kz1).get();
    }
    else{
      return nullptr;   
    }
  }
}

//################################################################
template< class I>
void Cell< I>::
apply_with_nephews( typename I::Container& conr, Cell< I>* cousin){

  if (cousin->is_bottom()){
    I::process_bodies( conr, contents, cousin->contents);
  }
  else{
    for (unsigned int ix = 0; ix < 2; ix++){
      for (unsigned int iy = 0; iy < 2; iy++){
        for (unsigned int iz = 0; iz < 2; iz++){
          auto& child = cousin->children(ix,iy,iz);
          if (child){
            apply_with_nephews( conr, child.get());
  } } } } }
}

//###############################################################
template< class I>
void Cell< I>::do_sideways_sweep( const Multipole_Info& info,
                                    const Far_Nebor_Interaction_Info< I>& fn_info){

  using ::abs;

  if (has_sources()){
    if (parent){
      for (int jx = -1; jx <= 1; jx++){
        for (int jy = -1; jy <= 1; jy++){
          for (int jz = -1; jz <= 1; jz++){       
            if (jx != 0 || jy != 0 || jz != 0){
              auto uncle = parent->neighbor( jx,jy,jz);
              if (uncle && uncle->has_receivers()){
                for( unsigned int kx = 0; kx <= 1; kx++){
                  for( unsigned int ky = 0; ky <= 1; ky++){
                    for( unsigned int kz = 0; kz <= 1; kz++){
                      auto& cousin = uncle->children(kx,ky,kz);
                      if (cousin && cousin->has_receivers()){                
                        int dx = cousin->idx - idx;
                        int dy = cousin->idy - idy;
                        int dz = cousin->idz - idz;
                        
                        if (abs( dx) > 1 || abs( dy) > 1 || abs( dz) > 1){
                          
                          const Vector< double>& ds = 
                            fn_info.ds_cubes[ level-2][dx+3][dy+3][dz+3];
                           
                          info.add_taylor_expansion( qs, ds, cousin->vs);
  } } } } } } } } } } }     
    
  for (unsigned int ix = 0; ix < 2; ix++){
    for (unsigned int iy = 0; iy < 2; iy++){
      for (unsigned int iz = 0; iz < 2; iz++){
        auto& child = children(ix,iy,iz);
        if (child){
          child->do_sideways_sweep( info, fn_info);
  } } } } }
}

//#################################################################
template< class I>
void Cell< I>::
compute_interactions_with_potential( const Multipole_Info& info,
                                     typename I::Container& conr){

  if (is_bottom()){
    Potential_Applier pa( info, *this);
    I::apply_potential( conr, contents, pa);
    I::process_bodies( conr, contents);    

    for (int jx = -1; jx <= 1; jx++){
      for (int jy = -1; jy <= 1; jy++){
        for (int jz = -1; jz <= 1; jz++){
          if (jx != 0 || jy != 0 || jz != 0){

            auto nebor = neighbor( jx,jy,jz);
            if (nebor){       
              if (nebor->is_bottom()){
                bool doi;
                int jxyz = jx + jy + jz;
                if (jxyz > 0){
                  doi = true;
                }
                else if (jxyz < 0){
                  doi = false;
                }
                else {
                  int jxy = jx + jy;
                  if (jxy > 0){
                    doi = true;
                  }
                  else if (jxy < 0){
                    doi = false;
                  }
                  else{
                    doi = (jx > 0);
                  }
                }

                if (doi){
                  I::process_bodies( conr, contents, nebor->contents);
                } 
              }
              else { // not bottome
                apply_with_nephews( conr, nebor);
  } } } } } } }
  
  else { // not bottom
    for (unsigned int ix = 0; ix < 2; ix++){
      for (unsigned int iy = 0; iy < 2; iy++){
        for (unsigned int iz = 0; iz < 2; iz++){
          auto& child = children(ix,iy,iz);
          if (child){
            child->compute_interactions_with_potential( info, conr);
  } } } } }
}

//###########################################################
template< class I>
bool Cell< I>::do_pair(  const Cell< I>* cell1) const{
  
  bool r0 = has_receivers();
  bool r1 = cell1->has_receivers();
  bool s0 = has_sources();
  bool s1 = cell1->has_sources();
  return ((r0 && s1) || (r1 && s0));
}
  
//############################################################3
  // genericize later
template< class I>
void Cell< I>::
compute_interactions_with_field( const Multipole_Info& info,
                                 typename I::Container& conr){

  if (is_bottom()){
    
    if (has_receivers()){
      Field_Applier fa( info, *this);
      I::apply_field( conr, contents, fa);
      if (has_sources()){
        I::process_bodies( conr, contents);    
      }
    }

    for (int jx = -1; jx <= 1; jx++){
      for (int jy = -1; jy <= 1; jy++){
        for (int jz = -1; jz <= 1; jz++){
          if (jx != 0 || jy != 0 || jz != 0){
            
            auto& nebor = neighbor( jx,jy,jz);
            if (nebor){       
              if (nebor->is_bottom()){
                bool doi;
                int jxyz = jx + jy + jz;
                if (jxyz > 0){
                  doi = true;
                }
                else if (jxyz < 0){
                  doi = false;
                }
                else {
                  int jxy = jx + jy;
                  if (jxy > 0){
                    doi = true;
                  }
                  else if (jxy < 0){
                    doi = false;
                  }
                  else{
                    doi = (jx > 0);
                  }
                }
                
                if (doi){
                  if (do_pair( nebor)){
                    I::process_bodies( conr, contents, nebor->contents);
                  }
                }
              }
              else { // not bottom
                if (do_pair( nebor)){
                  apply_with_nephews( conr, nebor);
                  
  } } } } } } } }
  else { // not bottom
    for (unsigned int ix = 0; ix < 2; ix++){
      for (unsigned int iy = 0; iy < 2; iy++){
        for (unsigned int iz = 0; iz < 2; iz++){
          auto& child = children(ix,iy,iz);
          if (child){
            child->compute_interactions_with_field( info, conr);
  } } } } }
}

//################################################################
template< class I>
void Cell< I>::
do_downward_sweep( const Multipole_Info& info){
   
  if (has_receivers()){

    if (!is_bottom()){
      for (unsigned int ix = 0; ix < 2; ix++){
        for (unsigned int iy = 0; iy < 2; iy++){
          for (unsigned int iz = 0; iz < 2; iz++){
            auto& child = children(ix,iy,iz);
            if (child){
              info.add_shifted_polynomial( info.multipole_order(),
                                           child->cps, vs, child->vs);
              
              child->do_downward_sweep( info);
  } } } } } }
}

//############################################################
template< class I>
void Cell< I>::
do_upward_sweep( const Multipole_Info& info, 
                 typename I::Container& conr){
  
  if (has_sources()){

    if (is_bottom()){
      Particle_Multipole_Getter pmg;
      pmg.cell = this;
      pmg.info = &info;
      pmg.cps.resize( info.number_of_indices());   
      I::apply_multipole_function( conr, contents, pmg);
    }
    
    else{
      for (unsigned int ix = 0; ix < 2; ix++)
        for (unsigned int iy = 0; iy < 2; iy++)
          for (unsigned int iz = 0; iz < 2; iz++){
            auto& child = children(ix,iy,iz);
            if (child){
              child->do_upward_sweep( info, conr);
              info.add_shifted_multipoles( info.multipole_order(), 
                                           child->cps, 
                                           child->qs, 
                                           qs);     
            }
          }
    }
  }
}

//##################################################################
unsigned int ncell();

// constructor
template< class I>
Cell< I>::Cell( const Multipole_Info& info){

  contained = None;
  parent = nullptr;
  level = 0;
  idx = idy = idz = 0;

  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++)
        children(ix,iy,iz) = nullptr;

  const double nan = std::numeric_limits<double>::quiet_NaN();

  for( int k = 0; k < 3; k++)
    center[k] = nan;
  
  length = nan;
  qs.resize( info.number_of_indices());
  vs.resize( info.number_of_indices());
  parent = nullptr;
}

//###############################################################
template< class I>
void Cell< I>::
split_this_cell( const Multipole_Info& info,
                 typename I::Container& conr, int){
  
  for (unsigned int ix = 0; ix < 2; ix++){
    for (unsigned int iy = 0; iy < 2; iy++){
      for (unsigned int iz = 0; iz < 2; iz++){
        children(ix,iy,iz) = std::make_unique< Cell>(info);
      }
    }
  }
  
  //typedef unsigned int uint;
  
  auto set_ref = [&]( size_t ix, size_t iy, size_t iz, Refs&& refs){
    children( ix,iy,iz)->contents = std::move( refs);
  };
  
  I::split_container( conr, contents, center, set_ref);
  
  for (int ix = 0; ix < 2; ix++)
    for (int iy = 0; iy < 2; iy++)
      for (int iz = 0; iz < 2; iz++){
        auto& child = children(ix,iy,iz);
        if (I::size( conr, child->contents) == 0){
          child.reset();
        }
        else{
          child->idx = (idx << 1) + ix;
          child->idy = (idy << 1) + iy;
          child->idz = (idz << 1) + iz;
          child->level = level + 1;
          child->length = length/2;
          double h = length/4;
          child->center[X] = center[X] + h*(2*ix - 1); 
          child->center[Y] = center[Y] + h*(2*iy - 1); 
          child->center[Z] = center[Z] + h*(2*iz - 1);
          child->parent = this;
        }
        
      }
  
  
  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++){
        auto& child = children(ix,iy,iz);
        if (child){
          //double c[3];
          Vec3< double> c;
          c[0] = child->center[0] - center[0];
          c[1] = child->center[1] - center[1];
          c[2] = child->center[2] - center[2];
          info.generate_scaled_rfactors( c, child->cps);
        }
      }  
}
  
//##############################################################  
template< class I>
void Cell< I>::
split_cells( const Multipole_Info& info,
             typename I::Container& conr){

  if (I::size( conr, contents) > info.max_number_per_cell()){ 
    split_this_cell( info, conr, 0);
 
    for (unsigned int ix = 0; ix < 2; ix++){
      for (unsigned int iy = 0; iy < 2; iy++){
        for (unsigned int iz = 0; iz < 2; iz++){
          auto& child = children(ix,iy,iz);
          if (child != nullptr){
            child->split_cells( info, conr);
          }
        }
      }
    }
   
    I::empty_references( conr, contents);
  }

}

//################################################################
template< class I>
void Cell< I>::
create_top_cell( const Multipole_Info& info, 
                 typename I::Container& conr,
                 double padding){
  //double low[3], high[3];
  Vec3< double> low, high;
  I::get_bounds( conr, low, high);
  
  for( int k = 0; k < 3; k++)
    center[k] = 0.5*( low[k] + high[k]);

  length = max( max( high[0] - low[0], high[1] - low[1]), high[2] - low[2]) + 2*padding;

  I::copy_references( conr, contents);

  bool is_empty = (I::size( conr, contents) == 0);
  
  if (!is_empty &&
      (I::size( conr, contents) > info.max_number_per_cell())){ 

    split_cells( info, conr);
  } 
}

//#################################################################
template< class I>
void Cell< I>::even_bottoms( const Multipole_Info& info, 
                                    typename I::Container& conr){
  
  bool changed;
  do{
    changed = false;
    even_bottoms_inner( info, conr, changed);
  }
  while (changed);
}

//############################################################
template< class I>
void Cell< I>::even_bottoms_inner( 
                           const Multipole_Info& info, 
                           typename I::Container& conr,
                           bool& changed){

  if (!(I::size( conr, contents) == 0)){
    if (is_bottom()){
      
      bool has_sources = (I::n_sources( conr, contents) > 0);
      bool has_receivers = (I::n_receivers( conr, contents) > 0);
  
      for (int kx = -1; kx <= 1; kx++){
        for (int ky = -1; ky <= 1; ky++){
          for (int kz = -1; kz <= 1; kz++){
            if (kx != 0 || ky != 0 || kz != 0){
              auto nebor = neighbor( kx, ky, kz);
              if (nebor){
                bool n_has_sources = (I::n_sources( conr, nebor->contents) > 0);
                bool n_has_receivers = (I::n_receivers( conr, nebor->contents) > 0);
                if (!nebor->is_bottom() && ((n_has_sources && has_receivers) || (n_has_receivers && has_sources))){ 
                    
                  changed = true;
                  split_this_cell( info, conr, 1);
                
                  for (unsigned int ix = 0; ix < 2; ix++){
                    for (unsigned int iy = 0; iy < 2; iy++){
                      for (unsigned int iz = 0; iz < 2; iz++){
                        auto& child = children(ix,iy,iz);
                        if (child){
                          child->even_bottoms_inner( info, conr, changed);
    } } } } } } } } } } }
  
    else{ // not bottom
      for (unsigned int ix = 0; ix < 2; ix++){
        for (unsigned int iy = 0; iy < 2; iy++){
          for (unsigned int iz = 0; iz < 2; iz++){
            auto& child = children(ix,iy,iz);
            if (child != nullptr)
              child->even_bottoms_inner( info, conr, changed); 
    } } } }
  }
} 
       
//###################################################################       
template< class I>
void Cell< I>::set_contained_type( typename I::Container& conr){
  bool has_sources, has_receivers;

  if (is_bottom()){
    has_receivers = (I::n_receivers( conr, contents) > 0);
    has_sources = (I::n_sources( conr, contents) > 0);
  }
  else{
    has_receivers = false;
    has_sources = false;

    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          auto& child = children(ix,iy,iz);
          if (child){
            child->set_contained_type( conr);
            has_receivers |= child->has_receivers();
            has_sources |= child->has_sources();
          }    
        }
  }  
  
  if (has_receivers && has_sources) contained = Both;
  else if (has_receivers && !has_sources) contained = Receivers;
  else if (!has_receivers && has_sources) contained = Sources;
  else {
    contained = None;
  }
}

//#################################################################33
template< class I>
void Cell< I>::zero_out_sums(){
  zero( vs);
  zero( qs);

  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++){
        auto& child = children(ix,iy,iz);
        if (child != nullptr){
          child->zero_out_sums();
        }
      }
}

//#######################################################
template< class I>
class Potential_Computer;

template< class I>
class Calculator{

public:
  typedef typename I::Container Container;
  typedef typename I::size_type size_type;

  Calculator( unsigned int order, Container& contents);

  void setup_cells();

  void compute_expansion();
  void compute_interactions_with_potential();
  void compute_interactions_with_field();

  // default 20
  void set_max_number_per_cell( size_type n){
    info.set_max_number_per_cell( (unsigned int)n);
  }

  size_type max_number_per_cell() const{
    return (size_type)(info.max_number_per_cell());
  }

  // diameter to (center-to-center distance) ratio
  // default 0.5
  void set_distance_ratio( double r){
    info.set_distance_ratio( r);
  }

  [[nodiscard]] double distance_ratio() const{
    return info.distance_ratio();
  }

  void reload_contents();

  // default 0.0
  void set_padding( double p){
    padding = p;
  }
  
  Potential_Computer< I> potential_computer() const;
  [[nodiscard]] double potential_at_point( Vec3< double> pos) const;

  template< class L3, class F3>
  void get_field_at_point( const L3& r, F3& F) const;
  
private:
  friend class Potential_Computer<I>;
  
  Far_Nebor_Interaction_Info< I> fn_info;
  Container& contents;
  Multipole_Info info;
  Cell< I> top_cell;
  double padding;
};

//####################################################
template< class I>
class Potential_Computer{
public:
  double operator()( Vec3< double> pos) const;
  
  explicit Potential_Computer( const Calculator< I>&);
  
private:
  mutable Vector< double> rs, fderivs, derivs;
  const Calculator<I>& calc;
};

//#############################################################
// constructor
template< class I>
Potential_Computer< I>::Potential_Computer( const Calculator< I>& calc):
  calc( calc){
   
  auto& info = calc.info;
  auto ni = info.number_of_indices();
  fderivs.resize( ni+1);
  derivs.resize( ni+1);
  rs.resize( ni);
}

//##############################################################
template< class I>
double Potential_Computer< I>::operator()(Vec3<double> pos) const{
  
  auto& top_cell = calc.top_cell;
  auto rpos = pos - top_cell.center;
   
  auto& info = calc.info; 
  auto r = norm( rpos);
  auto& contents = calc.contents;
  I::get_r_derivatives( contents, info.multipole_order(), r, fderivs);
  
  info.generate_rfactors( rpos, rs);
  info.generate_greens_derivatives( rs, fderivs, derivs);
  
  auto& qs = top_cell.qs;
  return info.first_taylor_term( qs, derivs);
}

//###################################################################
template< class I>
auto Calculator<I>::potential_computer() const -> Potential_Computer<I>{
  return Potential_Computer<I>( *this);
}

//###########################################################
template< class I>
Calculator< I>::
Calculator( unsigned int order, typename I::Container& _contents):
  contents( _contents), info( order) , top_cell( info){
  padding = 0.0;
}

//#############################################################
// might be able to speed up by doing allocations ahead of time
template< class I>
double Calculator< I>::potential_at_point( Vec3< double> pos)const{

  auto rpos = pos - top_cell.center;
  
  const unsigned int ni = info.number_of_indices();
  Vector< double> fderivs( ni+1);
  auto r = norm( rpos);
  I::get_r_derivatives( contents, info.multipole_order(), r, fderivs);
  
  Vector< double> rs( ni);
  info.generate_rfactors( rpos, rs);

  Vector< double> derivs( ni+1);
  return top_cell.potential_at_point( info, rs, fderivs, derivs);
}
 
//################################################################# 
template< class I>
template< class L3, class F3>
void Calculator< I>::
get_field_at_point( const L3& r, F3& F) const{

  const unsigned int ni = info.number_of_indices();
  Vector< double> fderivs( info.multipole_order()+1);
  Vector< double> rs( ni);
  Vector< double> ds( ni);
  Vector< double> vs(4);

  top_cell.get_field_at_point( info, contents, rs, fderivs, ds,vs, r, F);
}

//#############################################################
template< class I>
void Calculator< I>::setup_cells(){
  top_cell.create_top_cell( info, contents, padding);
  fn_info.set_up( info, contents, top_cell.depth(), top_cell.length);
  top_cell.even_bottoms( info, contents);
  top_cell.set_contained_type( contents);
}

template< class I>
void Calculator< I>::
reload_contents(){
  top_cell.reload_contents( contents);
}

template< class I>
void Calculator< I>::compute_interactions_with_potential(){
  top_cell.compute_interactions_with_potential( info, contents);
}

template< class I>
void Calculator< I>::compute_interactions_with_field(){
  top_cell.compute_interactions_with_field( info, contents);
}


template< class I>
void Calculator< I>::compute_expansion(){
  top_cell.compute_expansion( info, fn_info, contents);
}

}
