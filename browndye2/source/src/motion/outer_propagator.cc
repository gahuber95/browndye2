#include "outer_propagator.hh"
#include "../global/pi.hh"
#include "../lib/romberg.hh"
#include "../generic_algs/step_near_absorbing_surface.hh"
#include "../lib/rotations.hh"
#include "../molsystem/snas_interface.hh"

namespace Browndye{

using std::min;
using std::max;

// constructor
Outer_Propagator::Outer_Propagator( Length br, Length mr, bool hi, Solvent solvent, OP_Group_Info g0, OP_Group_Info g1){
  
  auto q0 = g0.q, q1 = g1.q;
  
  kT = solvent.kT;
  auto mu = solvent.viscosity();
  auto epss = solvent.dielectric*vacuum_permittivity;
  V_factor = q0*q1/(pi4*epss*kT);
  D_factor = kT/mu;
  debye_length = solvent.debye_length;
  bradius = br;
  qradius = 20.0*mr;
  has_hydro_interact = hi;
  a0 = D_factor/(pi6*g0.Dtrans);
  a1 = D_factor/(pi6*g1.Dtrans);  
  a2 = 0.5*(a0*a0 + a1*a1);
  Drot0 = g0.Drot;
  Drot1 = g0.Drot;
  
  return_prob = relative_rate( bradius)/relative_rate( qradius);
  
  bradius_cover = cover( true);
  qradius_cover = cover( false);  
}

decltype( Length3()/Time()) Outer_Propagator::relative_rate( Length b) const{
  
  auto dpre = D_factor/pi6;
  auto ainv = 1.0/a0 + 1.0/a1;
  
  auto dpara  = [&]( Length r) -> Diffusivity {
    if (has_hydro_interact)
      return dpre*( ainv - 3.0/r + 2.0*a2/(r*r*r)); 
    else
      return dpre*ainv;
  };

  double tol = 1.0e-6;
  auto integrand = [&]( Inv_Length s) -> auto {
    if (s == Inv_Length(0.0)){
      return 1.0/(dpre*ainv);
    }
    else {
      auto r = 1.0/s;
      auto v = V_factor*(exp (-1.0/(s*debye_length)))*s;
      return exp(v)/dpara( r);
    }
  };

  auto igral = Romberg::integral( integrand, Inv_Length( 0.0), 1.0/b, tol);
  return pi4/igral;
}

// finds boundary inside linear boundary inside of which particle is unlikely to move beyond linear region
Length Outer_Propagator::cover( bool is_inner) const{
  
  Length bndy;
  if (is_inner)
    bndy = ts_boundary( bradius, true);
  else
    bndy = ts_boundary( qradius, false);
  
  Inv_Length F;
  Length L;
  
  auto force = [&]( auto r){
    auto rm1 = 1.0/r;
    auto L = debye_length;
    auto expf = exp(-r/L);
    auto V = V_factor*expf*rm1;
    return V*( rm1 + 1.0/L);
  };  
  
  if (is_inner){
    F = force( bradius);
    L = bndy - bradius;
  }
  else{
    F = -force( qradius);
    L = qradius - bndy;
  }
  
  auto prob = [&]( Length x0){
    return erfc( (L - x0*x0*F - x0)/(2.0*x0))/( erf( (x0*F + 1.0)/2.0) + 1.0);
  };
  
  // bisect
  const double thresh = 0.01;
  const double tol = 1.0e-6;
  Length lo{ 0.0}, hi{ L};
  while (hi - lo > tol*L){
    auto mid = 0.5*( lo + hi);
    auto pmid = prob( mid);
    if (pmid > thresh)
      hi = mid;
    else
      lo = mid;
  }
  
  auto x0 = 0.5*( lo + hi);
  if (is_inner)
    return bradius + x0;
  else
    return qradius - x0;
}

// find distance from boundary where force is no longer linear
Length Outer_Propagator::ts_boundary( Length rad, bool is_inner) const{

  constexpr double curve_tol = 0.05;
  
  auto reldiff = [&]( Length r) -> double {
    auto L = debye_length;
    
    double rd;
    if (L/Length(1.0) > 0.1*large){
      rd = 2.0*fabs( r - rad)/r;
    }
    else{
      rd = (2.0*L*L + 2.0*r*L + r*r)*fabs(r - rad)/(r*L*(L+r));
    }
    
    return  rd - curve_tol;
  };
  
  // bracket
  Length rlo, rhi;
  {
    Length rb;
    if (is_inner){
      rb = 2.0*rad;
      while (reldiff( rb) < 0.0)
        rb *= 2.0;
    }
    else {
      rb = 0.5*rad;
      while( reldiff( rb) < 0.0)
        rb *= 0.5;
    }
    
    if (is_inner){
      rlo = rad;
      rhi = rb;
    }
    else{
      rlo = rb;
      rhi = rad;
    }
  }
  
  // bisect
  Length rb;
  {
    auto fhi = reldiff( rhi);
    auto flo = reldiff( rlo);
    while (rhi - rlo > 1.0e-6*rad){
      auto rm = 0.5*(rhi + rlo);
      auto fm = reldiff( rm);
      if (is_inner){
        if (fm*fhi < 0.0){
          rlo = rm;
          flo = fm;
        }
        else{
          rhi = rm;
          fhi = fm;
        }
      }
      else{
        if (fm*flo < 0.0){
          rhi = rm;
          fhi = fm;
        }
        else{
          rlo = rm;
          flo = fm;
        }
      }
    }
    
    rb = 0.5*( rlo + rhi);
  }
  
  if (is_inner)
    return min( rb, (1.0 + curve_tol)*bradius);
  else
    return max( rb, (1.0 - curve_tol)*qradius);
}


std::pair< bool, Transform>
Outer_Propagator::new_state( Browndye_RNG& gen, Transform tform1) const{
  
  auto init_pos1 = tform1.translation();
  
  bool done = false;
  auto pos = init_pos1;
  Time t( 0.0);
  
  bool reached_q = false, reached_b = false;
  
  auto do_when_near_q = [&]( bool survives, Length r, Length new_x) -> void{
    if (!survives){
      if (gen.uniform() < return_prob){
        pos = (bradius/r)*pos;
        reached_b = true;
        reached_q = false;
      }
      else
        reached_q = true;
      
      t = Time( large);
    }
    else{
      auto new_r = qradius - new_x;
      pos = (new_r/r)*pos;
      reached_q = false;
    }
    
  };
  
  while (!done){
    
    if (reached_b || reached_q)
      done = true;
    
    else {
      auto r = norm( pos);
      
      if (r <= bradius){
        pos = (bradius/r)*pos;
        reached_b = true;
      }
      
      else if (r >= qradius){
        do_when_near_q( false, r, r); 
      }
      
      else{
        auto rm1 = 1.0/r;
        auto L = debye_length;
        auto expf = exp(-r/L);
        auto V = V_factor*expf*rm1;
        auto Fr0 = V*( rm1 + 1.0/L);
        
        Inv_Length2 rm2;
        Inv_Length3 rm3{ NaN};
        Diffusivity D0, Di;
        auto ainv = (1.0/a0 + 1.0/a1)/pi6;
        if(!has_hydro_interact)
          D0 = D_factor*ainv;
        else{
          rm2 = rm1*rm1;
          rm3 = rm2*rm1;
          Di = -2.0*D_factor*(2.0*rm1 - (4.0/3)*(a2*rm3))/pi8;
          D0 = D_factor*ainv + Di;
        }
        
        if (r < bradius_cover){    
           Time delta_t;
           auto x = r - bradius;
           Length new_x;
           bool survives;
           
           std::tie( survives, new_x, delta_t) = step_near_absorbing_surface< SNAS_I>( gen, x, Fr0, D0);
           reached_b = !survives;
           t += delta_t;
           
           auto new_r = bradius + new_x;
           pos = (new_r/r)*pos;
        }
        else if (r > qradius_cover){
          Time delta_t;
          auto x = qradius - r;
          Length new_x;
          bool survives;
          
          std::tie( survives, new_x, delta_t) = step_near_absorbing_surface< SNAS_I>( gen, x, -Fr0, D0);
          
          do_when_near_q( survives, r, new_x);
        }
        
        else{
          auto L2 = L*L;
          // auto L3 = L2*L;
   
          auto Fr1 = -V/L2 - 2.0*Fr0*rm1;
          auto Fr2 =  Inv_Length3( 0.0); //V/L3 - 3.0*Fr1*rm1;
          
          constexpr double alpha = 0.01;
          Time dtf;
          
          //auto ainv = (1.0/a0 + 1.0/a1)/pi6;
          if(!has_hydro_interact){
            if (Fr0 != Inv_Length(0.0) && r < 3.0*L)
              dtf = alpha/fabs( D0*Fr1);  //alpha*fabs( Fr0/(D0*(Fr0*Fr1 + Fr2)));
            else
              dtf = Time( large);
          }
          
          else{
            auto rm4 = rm1*rm3;
            D0 = D_factor*ainv + Di;
            auto D1 = -3.0*Di*rm1 - D_factor*rm2/pi;
            auto D2 = -4.0*D1*rm1 + D_factor*rm3/pi;
            auto D3 = -5.0*D2*rm1 - 2.0*D_factor*rm4/pi;
            
            Velocity num;
            decltype( Velocity()/Time()) den;
            if (fabs( Fr0) > Inv_Length( 0.0) && r < 3.0*L){
              num = D1 + D0*Fr0;
              den = D0*D3 + (D1 + 2.0*Fr0*D0)*D2 + Fr0*sq( D1) + (3.0*Fr1 + sq( Fr0))*D0*D1 + (Fr2 + Fr0*Fr1)*sq( D0);
            }
            else {
              num = D1;
              den = D0*D3 + D1*D2;
            }
            dtf = alpha*fabs( num/den);
          }
          
          auto dt_edge = sq( min( qradius - r, r - bradius))/(18.0*D0);
          auto dt = min( dt_edge, dtf);  
          t += dt;
          
          Diffusivity Dr = D0;
          
          for( auto k: range(3))
            pos[k] += Dr*(Fr0/r)*pos[k]*dt;
          
          auto sDrdt = sqrt( 2.0*Dr*dt);
          if (has_hydro_interact){
            auto ur = rm1*pos;
            auto x = ur[0];
            auto y = ur[1];
            auto z = ur[2];
            auto rho = sqrt( x*x + y*y);
            Vec3< double> ut,up;
            if (rho == 0.0){
              ut = Vec3< double>{ 1.0, 0.0, 0.0};
              up = Vec3< double>{ 0.0, 1.0, 0.0};
            }
            else{
              ut = Vec3< double>{ z*x/rho, z*y/rho, -rho};
              up = Vec3< double>{ -y/rho, x/rho, 0.0};
            }
            auto Dt = D_factor*(ainv - 2.0*(rm1 - 2.0*a2*rm3)/pi8); 
            auto sDtdt = sqrt( 2.0*Dt*dt);
            auto g0 = gen.gaussian();
            auto g1 = gen.gaussian();
            auto g2 = gen.gaussian();
            pos += (sDrdt*g0)*ur + (sDtdt*g1)*ut + (sDtdt*g2)*up;
          }
          else{
            for( auto k: range(3))
              pos[k] += sDrdt*gen.gaussian();
          }
        }
      }
    }
  }
  
  if (reached_b){
    auto init_rot1 = tform1.rotation();
  
    auto rott = [&]( double tau) -> Mat3< double> {
      auto quat = diffusional_rotation< SNAS_I>( gen, tau);
      return mat_of_quat( quat);
    };
    
    auto rot0 = rott( t*Drot0);
    auto rot1 = rott( t*Drot1);
    
    Transform tform0( rot0, Pos( Zeroi{}));   
    Transform ltform1( rot1*init_rot1, pos);
    auto rel_tform1 = tform0.inverse() * ltform1;
     
    return std::make_pair( true, rel_tform1);
  }
  else
    return std::make_pair( false, tform1);
  
}
}
