#pragma once

/*
 * range.hh
 *
 *  Created on: Jul 21, 2016
 *      Author: root
 */

#include "../global/error_msg.hh"

#define uauto [[maybe_unused]] auto

namespace Browndye{

template< class I>
class Range {
 public:
  
  class Iterator {
    friend class Range;
  public:
    I operator*() const {
      return data;
    }
    const Iterator &operator++() {
      ++data;
      return *this;
    }
    
    bool operator==(const Iterator &other) const {
      return data == other.data;
    }

    bool operator!=(const Iterator &other) const {
      return data != other.data;
    }
    
  protected:
    Iterator(I start) : data (start) { }
    
  private:
    I data;
  };

  Iterator begin() const {
    return b;
  }

  Iterator end() const {
    return e;
  }

  Range( I end): b( I{0}), e{end}{}

  Range( I begin, I end): b(begin), e(end){
    if (begin > end){
      b = I(0);
      e = I(0);
    }
  }
  
private:
  Iterator b,e;
};

template< class Ib, class Ie>
Range<Ib> range( Ib b, Ie e){
  return Range<Ib>( b, (Ib)e);
}

template< class I>
Range<I> range( I e){
  return Range<I>( e);
}

template< class I>
Range<I> range1( I e){
  return Range<I>( I(1), e);
}

}

