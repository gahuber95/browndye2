#pragma once

/*
 * spring_info.hh
 *
 *  Created on: Sep 23, 2015
 *      Author: root
 */

#include "../lib/units.hh"
#include "../global/indices.hh"
#include "../xml/jam_xml_pull_parser.hh"

// probably not needed

namespace Browndye{

struct Spring{
  explicit Spring( JAM_XML_Pull_Parser::Node_Ptr);
  Spring() = delete;

  Length r;
  Spring_Constant ks;
  Atom_Index i0, i1;
};

}

