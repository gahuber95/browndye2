#include <iostream>

double sq( double x){
  return x*x;
}

int main(){
  double low = 10.0;
  double hie = 16.0;
  int n = 101;

  auto h = 2.0*low/(n-1);
  for( int i = 0; i < n; i++){
    auto x = i*h;
    auto V = (hie/sq(low))*sq(x - low);
    std::cout << V << ' ';
  }
  
  
}
