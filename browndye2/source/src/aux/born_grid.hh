#pragma once

#include "../xml/jam_xml_pull_parser.hh"
#include "../global/pos.hh"
#include "../structures/atom.hh"
#include "../forces/electrostatic/single_grid.hh"

namespace Browndye{

class Born_Grid{
public:  
  typedef JAM_XML_Pull_Parser::Parser Parser;
  
  Born_Grid( const std::string& df_file, const std::string& atom_file);
  Born_Grid( const std::string& df_file, Parser& parser);
  Born_Grid() = delete;
  Born_Grid& operator=( const Born_Grid&) = delete;

  std::unique_ptr< Single_Grid_Nograd< unsigned int> > sgrid;
  Length3 svolume;
      
  mutable Vector< Atom, Atom_Index> atoms;
  Single_Grid_Nograd< Inv_Length> rgrid;
  
  Length debye_length;
};

}
