#include <cmath>
#include "../global/pi.hh"

//Quick & dirty, tolerance under +-6e-3. Work based on "A handy approximation for the error function and its inverse" by Sergei Winitzki.

namespace Browndye{

double inv_erf( double x){
  
  if (fabs(x) < 0.01)
    return (sqrt(pi)/2)*x;
  
  else{
    const auto sgn = (x < 0) ? -1.0 : 1.0;
    
    const auto xx = (1 - x)*(1 + x);        // x = 1 - x*x;
    const auto lnx = log(xx);
    
    const auto tt1 = 2/(pi*0.147) + 0.5*lnx;
    const auto tt2 = (1/0.147)*lnx;
    
    return sgn*sqrt(-tt1 + sqrtf(tt1*tt1 - tt2));
  }
}

}

/*
int main(){
  int n = 10000;
  for( int i = -99*n/100; i < 99*n/100; i++){
    auto p = i/float(n);
    auto x = inv_erf( p);
    std::xout << x << " " << p << "\n";
  }
}
*/
