#include "../xml/node_info.hh"
#include "coffdrop.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

Vector< Residue, Residue_Index> residues_of_file( const std::string& atom_file){

  Vector< Residue, Residue_Index> residues;
  XML_Info xinfo( atom_file);
  
  auto res_nodes_lst = xinfo.top->children_of_tag( "residue");
  Vector< JP::Node_Ptr, Residue_Index> res_nodes;
  for( auto res_node: res_nodes_lst)
    res_nodes.push_back( res_node);
  
  auto nr = res_nodes.size();
  for( auto ir: range( nr)){
    auto& res_node = res_nodes[ir];
    auto rname = checked_value_from_node< std::string>(res_node, "name");
    
    auto atom_nodes = res_node->children_of_tag( "atom");
    residues.emplace_back();
    auto& residue = residues.back();
    residue.name = rname;
    residue.number = checked_value_from_node< Residue_Index>( res_node, "number");
    for( auto atom_node: atom_nodes){
      CAtom atom;
      atom.name = checked_value_from_node< std::string>( atom_node, "name");
      auto x = checked_value_from_node< Length>( atom_node, "x");
      auto y = checked_value_from_node< Length>( atom_node, "y");
      auto z = checked_value_from_node< Length>( atom_node, "z");
      auto q = checked_value_from_node< Charge>( atom_node, "charge");
      atom.pos = Pos{ x,y,z};
      atom.charge = q;
      atom.number = checked_value_from_node< Atom_Index>( atom_node, "number");
      residue.atoms.push_back( atom);
    }
  }

  Residue_Index r0( 0), r1( 1);  
  auto& res0 = residues[ r0];
  
  bool has_ace = res0.name == "ACE";
  if (has_ace){
    auto& res1 = residues[ r1];
    for( auto& atom: res0.atoms)
      res1.atoms.push_back( atom);
  }
  
  auto dr1 = r1 - r0;
  auto& resn = residues[ nr - dr1];
  bool has_nme = resn.name == "NME";
  if (has_nme){
    auto& resm = residues[ nr - 2*dr1];
    for( auto& atom: resn.atoms)
      resm.atoms.push_back( atom);
  }
  
  if (has_ace)
    residues.erase( residues.begin());
  
  if (has_nme)
    residues.erase( residues.begin() + (nr - dr1)/r1);
   
  return residues;
}

}
