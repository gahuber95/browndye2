(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Used internally

polynomial m x: evaluates the m_th Legendre polynomial at x

sum coefs x: evaluates Sum_i=0 to n (coefs[i]*legendre_polynomial_i(x))
  where n is the length of coefs
*)

let polynomial m x = 
  let rec loop n pn pnm1 =
    if n = m then
      pn
    else
      let fn = float n in
      let pnp1 = ((2.0*.fn +. 1.0)*.x*.pn -. fn*.pnm1)/.(fn +. 1.0) in
	loop (n+1) pnp1 pn 
  in
  let p0 = 1.0 in
    if m = 0 then
      p0
    else
      let p1 = x in
	loop 1 p1 p0 
;;

let sum coefs x = 
  let m = Array.length coefs in
  let rec loop n pn pnm1 sum =
    if n = m then
      sum
    else
      let fn = float n in
      let pnp1 = ((2.0*.fn +. 1.0)*.x*.pn -. fn*.pnm1)/.(fn +. 1.0) in
      let new_sum = sum +. coefs.(n)*.pn in
	loop (n+1) pnp1 pn new_sum
  in
  let p0, p1 = 1.0, x in
    loop 1 p1 p0 (coefs.(0)*.p0)
;;


(*******************************************************************)
(* test code *)
(*
let c = [| 1.0; 2.0; 3.0 |];;

Printf.printf "%g\n" (sum c 1.0);;
*)

(*
for n = 0 to 5 do
  Printf.printf "n %d p %g\n" n (polynomial n 0.0);
done
;;
*)


(*
let n = 50;;

let c = Array.init n (fun i -> ((float i) +. 0.5)*.(polynomial i 1.0));;

for i = -100 to 100 do
  let x = 0.01*.(float i) in
    Printf.printf "%g %g\n" x (sum c x)
done
;;

*)



