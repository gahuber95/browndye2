#pragma once

#include <map>
#include <set>
#include "../../lib/vector.hh"
#include "../../lib/spline.hh"
#include "../../global/indices.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../lib/array.hh"
#include "nonbonded_parameter_info.hh"

namespace Browndye{

class CD_Nonbonded_Parameter_Info: public Nonbonded_Parameter_Info{
public:
  explicit CD_Nonbonded_Parameter_Info( JAM_XML_Pull_Parser::Parser&);
  CD_Nonbonded_Parameter_Info() = default;
  CD_Nonbonded_Parameter_Info( const CD_Nonbonded_Parameter_Info& pinfo) = delete;
  CD_Nonbonded_Parameter_Info( CD_Nonbonded_Parameter_Info&& pinfo) = default;
  CD_Nonbonded_Parameter_Info& operator=( const CD_Nonbonded_Parameter_Info& pinfo) = delete;
  CD_Nonbonded_Parameter_Info& operator=( CD_Nonbonded_Parameter_Info&& pinfo) = default;
  ~CD_Nonbonded_Parameter_Info() override= default;
  
  [[nodiscard]] Vec3< Force> force( Vec3< Length> r01, Length r, const Atom&, const Atom&) const;
  [[nodiscard]] Energy potential( Length r, const Atom&, const Atom&) const;
  
  void establish_interaction_radii( Vector< Atom, Atom_Index>&) const override;
  
  double one_four_factor = 1.0;
  
private:
  // types
  class Spline_Tag{};
  typedef Index< Spline_Tag> Spline_Index;
  typedef Even_Spline< Length,Energy, false> VSpline;
  
  typedef JAM_XML_Pull_Parser::Parser Parser;
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  class Basic_ATom_Type_Tag{};
  typedef Index< Basic_ATom_Type_Tag> Basic_Atom_Type_Index;
  
  class Residue_Type_Tag{};
  typedef Index< Residue_Type_Tag> Residue_Type_Index;

  template< class K, class V>
  using map = std::map< K,V>;
  
  typedef map< Basic_Atom_Type_Index, std::string> Index_Atom_Map;
  typedef map< Residue_Type_Index, std::string> Index_Residue_Map;
  
  struct RB_Name_Info{
    Index_Residue_Map reses;
    Index_Atom_Map atoms;
  };
  
  typedef std::pair< Residue_Type_Index, Basic_Atom_Type_Index> RB_Pair;
  typedef map< RB_Pair, Atom_Type_Index> RB_To_A;
  //typedef Vector< VSpline, Spline_Index> Splines;
  //typedef map< Array< Atom_Type_Index,2>, Spline_Index> Atoms_Spline_Map;
  //typedef map< Atom_Type_Index, map< Residue_Type_Index, Basic_Atom_Type_Index> > A_To_R_To_B;
  typedef map< Array< Atom_Type_Index, 2>, Spline_Index> AA_To_Spline;
  
  struct Spline_Info{
    RB_To_A rb_to_a;
    Vector< VSpline, Spline_Index> fs;
    AA_To_Spline aa_to_spline;
    map< Atom_Type_Index, std::set< Atom_Type_Index> > wild_to_as;
  };

  struct Types_Info{
    Array< Residue_Type_Index, 2> rtypes;
    Array< Basic_Atom_Type_Index, 2> btypes;
  };

  // data
  RB_Name_Info rb_names;
  Array< Length, 2> distances;
  Vector< VSpline, Spline_Index> Vs;
  Vector< Vector< Spline_Index, Atom_Type_Index>, Atom_Type_Index>
    aa_to_spline;
  
  // functions
  void get_atom_and_residue_info([[maybe_unused]] Parser&, const Node_Ptr&);
  void get_energy_units([[maybe_unused]] Parser&, const Node_Ptr&);
  void get_pairs_info( Parser&, const Node_Ptr&);
  
  [[nodiscard]] static auto atom_index_info( const Node_Ptr&) -> Index_Atom_Map;
  [[nodiscard]] static auto residue_index_info( const Node_Ptr&) -> Index_Residue_Map;
  
  //auto potential_splines( Parser& parser) const -> Spline_Info;
  static Spline_Info with_wildcards_expanded( Spline_Info&&) ;
  //A_To_R_To_B arb_map( const RB_To_A&, std::set< Residue_Type_Index>&) const;
  
  static
  std::set< Residue_Type_Index> residues( const RB_To_A&);
  
  Spline_Info spline_info_from_parser( Parser&, const Node_Ptr&) const;
   
  static
  Types_Info types_from_node( const Node_Ptr&);
  
  static
  void put_types_into_rb_to_a( const Types_Info&, RB_To_A&);
  
  static
  void add_spline_types( const RB_To_A&, const Types_Info&, AA_To_Spline&);
  
  void put_spline_info_into_array( const Spline_Info&);
  
  static
  Atom_Type_Index max_atom_type_index( const Spline_Info&);
  
  static
  Spline_Info with_wildcard_info( Spline_Info&&);
  
  void complete_dict( const RB_Name_Info&, const Spline_Info&);
};
}
