#pragma once
#include "../lib/vector.hh"
#include "../global/indices.hh"
#include "../global/pos.hh"
#include "../motion/wiener_vector.hh"

namespace Browndye{

class System_State;

//##############################################
// stored at thread level
struct Mobility_Info{
  typedef Vector< std::pair< Group_Index, Mob_Div_Index>, Sys_Mob_Div_Index> Divisions;
  Divisions divisions; 
  
  typedef decltype( sqrt(Energy()/Viscosity())) WF;
  WF wiener_factor{ NaN}; // sqrt( 2kT/mu)
  
  Mobility_Info( const System_State&);
  
  static
  WF new_wiener_factor( const System_State&);
  
  Mobility_Info( const Divisions& divs, WF wfactor):
   divisions( divs.copy()), wiener_factor( wfactor){}
  
  Mobility_Info copy() const{
    return Mobility_Info{ divisions, wiener_factor};
  }
};

//##############################################
// generated on the fly
class Mob_Info_And_State{
public:
  System_State& state;
  const Mobility_Info& mobility_info;
  bool add_stochastic_terms = false;  
    
  typedef decltype( Time()/Viscosity()) FF;
  FF force_factor; // dt/mu
  
  Mob_Info_And_State( System_State&, const Mobility_Info&);
  void set_time_step( const System_State&, Time);
  
private:
  static
  FF new_force_factor( const System_State&, Time);
};

//##############################################################
class System_Mobility_Interface{
public:
  typedef Sys_Mob_Div_Index Div_Index;
  typedef Browndye::Atom_Index Bead_Index;
  typedef ::Length Length;
  typedef ::Time Time;
  typedef Inv_Length Mobility;
  typedef Mob_Info_And_State System;
  
  static
  Sys_Mob_Div_Index n_divisions( const System& sys);
 
  static
  Atom_Index n_beads( const System& sys, Div_Index mci);
   
  static
  void get_centers( const System& sys, Vector< Pos, Div_Index>& poses);
   
  static
  void get_mobilities( const System& sys, Vector< Vector< Inv_Length, Atom_Index>, Div_Index>& mobs);
  
  static
  Pos bead_center( const System& sys, Div_Index, Atom_Index);
  
  static
  Length bead_radius( const System& sys, Div_Index, Atom_Index);
  
  //class Velocity_Tag{};
  class Force_Tag{};
  
  // v, dx
  static
  void add_quantity( const System&, System_State&, Div_Index, Atom_Index, Pos dx);
  
  // force
  static
  Vec3< Length2> quantity( const System& sys, Force_Tag, Div_Index, Atom_Index);
   
   // dw
  static
  Vec3< Length1p5> quantity( const System& sys, const Wiener_Vector&, Div_Index, Atom_Index);
  
  // coarse_dw
  static
  Vec3< Length1p5> div_quantity( const System& sys, const Wiener_Vector&, Div_Index);
  
  template< class T>
  class Info_Type;
  
private:
  template< class FTet, class FChain>
  static
  auto bead_quantity( FTet&, FChain&, const System&, Div_Index, Atom_Index);
  
  static
  Pos div_center( const System&, Div_Index);
};

template<>
class System_Mobility_Interface::Info_Type< System_Mobility_Interface::Force_Tag>{
public:
  typedef Length2 Result;
};

template<>
class System_Mobility_Interface::Info_Type< Wiener_Vector>{
public:
  typedef Length1p5 Result;
};

//#####################################
// constructor
inline
Mob_Info_And_State::Mob_Info_And_State( System_State& _state,
                                       const Mobility_Info& mob_info):
  
  state( _state), mobility_info( mob_info){}


inline
void Mob_Info_And_State::set_time_step( const System_State& lstate, Time dt){
  force_factor = new_force_factor( lstate, dt);
}

}
