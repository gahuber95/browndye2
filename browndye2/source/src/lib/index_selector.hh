#pragma once
#include <cstddef>
#include <type_traits>

namespace Browndye{

template< class T, class I>
class Vector;

namespace Index_Selector{

  class Blank{};
  
  template< bool tf>
  class IBlocker;
  
  template<>
  class IBlocker< true>{
  public:
    typedef Blank type;
  };
  
  template<>
  class IBlocker< false>{
  public:
    typedef size_t type;
  };
 
  // uses "expression SFINAE"
  template< typename T>
  struct has_copy_method{
  private:
    typedef std::true_type yes;
    typedef std::false_type no;

    template<typename U> static
    auto test(int) -> decltype(std::declval<U>().copy(), yes()){ return yes{};}
    
    template<typename> static
    no test(...){ return no{};}

  public:
  	static constexpr bool value = std::is_same< decltype(test<T>(0)),yes>::value;
  };
}
}

