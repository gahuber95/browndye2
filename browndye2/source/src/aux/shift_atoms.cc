#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"

using namespace Browndye;
namespace JP = JAM_XML_Pull_Parser;
using std::string;
using std::cout;

void echo( JP::Node_Ptr node, size_t ns, const string& tag){
  
  for( auto i: range(ns)){
    (void)i;
    cout << ' ';
  }
  
  auto text = checked_value_from_node< string>( node, tag);
  cout << "<" << tag << "> " << text << " </" << tag << ">\n";
}

//#############################################################
void do_residue( Array< double, 3>& offset, JP::Node_Ptr rnode){

  cout << "  <residue>\n";

  echo( rnode, 4, "name");
  echo( rnode, 4, "number");
   
  auto anodes = rnode->children_of_tag( "atom");
  for( auto anode: anodes){
    cout << "    <atom>\n";
    echo( anode, 6, "name");
    echo( anode, 6, "number");
   
    auto dx = offset[0];
    auto dy = offset[1];
    auto dz = offset[2];
    
    auto x = checked_value_from_node< double>( anode, "x");
    auto y = checked_value_from_node< double>( anode, "y");
    auto z = checked_value_from_node< double>( anode, "z");
    cout << "        <x> " << x + dx << " </x>\n";
    cout << "        <y> " << y + dy << " </y>\n";
    cout << "        <z> " << z + dz << " </z>\n";
    
    echo( anode, 6, "charge");
    echo( anode, 6, "radius");
    
    cout << "    </atom>\n";
  }
  cout << "  </residue>\n";
}
  
//###########################################################  
int main(int argc, char **argv){
   
  if (argc == 2 && argv[1] == string( "-help")){
    cout << "shift_atoms x y z takes a pqrxml file from standard input"
    "and shifts each atom by x y z; outputs to standard output\n";
  }
  
  else{
    if (argc != 4){
      error( "need proper number of arguments");
    }
  
    double x = atof( argv[1]);
    double y = atof( argv[2]);
    double z = atof( argv[3]);
   
    Array< double, 3> offset{ x,y,z}; 
 
    auto dor = [&]( JP::Node_Ptr rnode){
      do_residue( offset, rnode);
    };
  
    JP::Parser parser( std::cin);
    auto top = parser.top();
    cout << "<roottag>\n";
    parser.apply_to_nodes_of_tag( top, "residue", dor);
    cout << "</roottag>\n";
  }
}
