#pragma once

#include "../../xml/jam_xml_pull_parser.hh"
#include "../../structures/bonded_structures.hh"
#include "bonded_parameter_info.hh"
#include "../force_field_type.hh"

namespace Browndye{

struct CD_Bonded_Parameter_Info: public Bonded_Parameter_Info{

  typedef JAM_XML_Pull_Parser::Parser Parser;
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  CD_Bonded_Parameter_Info() = delete;
  explicit CD_Bonded_Parameter_Info( Parser&);

  Array< Length, 2> distances;
  Vector< double> bond_angle_values, dihedral_angle_values;

  Vector< CD::Bond_Type, Bond_Type_Index> bond_types;
  Vector< CD::Angle_Type, Angle_Type_Index> angle_types;
  Vector< CD::Dihedral_Type, Dihedral_Type_Index> dihedral_types;
    
  [[nodiscard]] Force_Field_Type ff_type() const override{
    return Force_Field_Type::Spline;
  }
  
  ~CD_Bonded_Parameter_Info() override = default;
  
  typedef CD Structures;
  
private:
  void set_pairs( Parser&, const Node_Ptr&);
  void set_angles( Parser&, const Node_Ptr&);
  void set_dihedrals( Parser&, const Node_Ptr&);
  
};
}
