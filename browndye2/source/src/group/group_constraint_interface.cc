#include "group_constraint_interface.hh"
#include "group_state.hh"
#include "../molsystem/system_thread.hh"

//################################################################
// implementation
namespace Browndye{
    
//#########################################################################################
Mobility
Group_Constraint_Interface::mobility( const System& gc, Bead_Index ib){
  
  auto& state = *gc.state;
  auto radius = state.bead_radius( ib);
  auto& solvent = state.solvent();
  auto mu = solvent.viscosity();
  return pi6/(mu*radius);
}
  
//#####################################################################
void Group_Constraint_Interface::
get_position( const System& gc, Bead_Index ib, Pos& pos){
  
    pos = gc.state->bead_position( ib);
}
//#####################################################################
void Group_Constraint_Interface::
set_position( System& gc, Bead_Index ib, Pos pos){
  
  gc.state->bead_position( ib) = pos;
}

//#####################################################################
/*
void Group_Constraint_Interface::
get_position_change( const System& gc, Bead_Index ib, Pos& dpos){
  
  Pos old_pos = gc.state->saved_bead_position( ib);
  auto new_pos = gc.state->bead_position( ib);
  dpos = new_pos - old_pos;
}
*/
//############################################################################
using std::make_pair;

std::pair< Bead_Index, Bead_Index>
Group_Constraint_Interface::length_constraint_particles( const System& gc,
                                                    Constraint_Index ic){
  
  return gc.state->lconstraint_beads( ic);
}

//############################################################################
Array< Bead_Index, 4>
Group_Constraint_Interface::coplanar_constraint_particles( const System& gc,
                                                            Constraint_Index ic){
  
  auto ca =  gc.state->cconstraint_beads( ic);
  return { ca[0], ca[1], ca[2], ca[3]};
}

//#############################################################################################
Length Group_Constraint_Interface::constraint_length( const System& gc,
                                                      Constraint_Index ic){
  return gc.state->constraint_length( ic);   
}

//################################################################
bool Group_Constraint_Interface::is_coplanar_constraint( const System& gc,
                                                                      Constraint_Index ic){
   return gc.state->is_coplanar_constraint( ic);
}
  
size_t Constraints_Info::ithread() const{
  return (thread->system_thread.number)/Thread_Index(1);
}
 
//##################################################################
//using std::ref;

//Saved_Refs
/*
auto saved( const Group_Thread& thread, Group_State& state){
   
  return std::make_tuple( ref( state.beginning( thread)),
                         ref( state.ending( thread)),
                         ref( state.current( thread)));
}
*/

}
