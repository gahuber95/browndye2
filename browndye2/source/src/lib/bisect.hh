#pragma once

#include "../global/debug.hh"

namespace Browndye{

namespace Bisect{

//#####################################
template <typename T>
int sgn(T val) {
  return (T(0.0) < val) - (val < T(0.0));
}

//#####################################
template< class F, class X>
X bound( double growth, F&& f, X xlo){
  
  auto xhi = growth*xlo;
  auto ylo = f( xlo);
  auto yhi = f( xhi);
  
  auto slo = sgn( ylo);
  while( slo*sgn( yhi) > 0){
    xhi *= growth;
    yhi = f( xhi);
  }
  
  return xhi;
}

//#####################################
template< class F, class X>
X upper_bound( F&& f, X xlo){
  
  return bound( 2.0, f, xlo);
}

//#####################################
template< class F, class X>
X lower_bound( F&& f, X xlo){
  
  return bound( 0.5, f, xlo);  
}

//#####################################
template< class F, class X>
X solution( F&& f, X xlo_in, X xhi_in, X tol){
  
  auto xlo = xlo_in;
  auto xhi = xhi_in;
  
  auto ylo = f( xlo);
  auto yhi = f( xhi);
  
  if (debug){
    if (sgn( ylo)*sgn( yhi) > 0){
      error( "bisect solution: not bound", xlo, xhi);
    }
  }
  
  while( xhi - xlo > tol){
    auto xmid = 0.5*( xlo + xhi);
    auto ymid = f( xmid);
    if (sgn( ylo)*sgn( ymid) < 0){
      xhi = xmid;
    }
    else{
      xlo = xmid;
    }
  }
  
  return 0.5*( xlo + xhi);
}
           
}
}
