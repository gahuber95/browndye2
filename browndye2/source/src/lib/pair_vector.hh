#pragma once
#include <cassert>
#include <functional>
#include "vector.hh"

namespace Browndye{
  
  template< class Va, class Vb>
  class Pair_Vector{
  public:
    Pair_Vector( const Va& _va, const Vb& _vb): va(_va), vb(_vb){
      assert( va.size() == vb.size());
    }
    
    static_assert( std::is_same_v< typename Va::index_type,
                                    typename Vb::index_type>);
    
    typedef typename Va::index_type index_type;
    
    typedef typename Va::value_type A;
    typedef typename Vb::value_type B;
    
    static constexpr bool AisC =
      std::is_copy_constructible_v< A> && std::is_copy_assignable_v< A>;
    
    static constexpr bool BisC =
      std::is_copy_constructible_v< B> && std::is_copy_assignable_v< B>;
    
    typedef std::conditional_t< AisC, A, const A&> Aref;
    typedef std::conditional_t< BisC, B, const B&> Bref;
    
    typedef std::tuple< Aref, Bref> value_type;
    
    index_type size() const{
      return va.size();
    }
    
    template< class F>
    auto mapped( F&& f){
      return initialized_vector( size(), [&]( index_type i){
        return f( (*this)[i]);
      });
    }
    
    template< class F, class A>
    auto folded_left( F&& f, A&& a){
      auto r = a;  
      for( auto i: range( size())){
        r = f( r, (*this)[i]);
      }  
      return r;
    }
    
    template< class F>
    Vector< value_type> filtered( F&& f){
      auto n = size();
      Vector< value_type> res;
      for( auto i: range(n)){
        auto item = (*this)[i];
        if (f( item)){
          res.push_back( item);
        }
      }
      return res;
    }
    
    Aref avalue( index_type i) const{
      return va[i];
    }
    
    Bref bvalue( index_type i) const{
      return vb[i];
    }
    
    value_type operator[]( index_type i) const{
      return std::forward_as_tuple( avalue(i), bvalue(i));
    };
    
    const Va& va;
    const Vb& vb;
  };

  //#######################################################3
  template< class Va, class Vb>
  Pair_Vector< Va, Vb> pair_vector( const Va& va, const Vb& vb){

    return Pair_Vector< Va,Vb>( va,vb);
  }
  
}
