#pragma once

/*
Used internally by XML parser.

This is a "stack of strings" which is used to concatenate many
small strings together while avoiding quadratic scaling of effort.
As strings are added to the stack, they are gradually coalesced
into one big string.
 */

#include <stack>
#include "str.hh"

namespace Browndye{
namespace JAM_XML_Parser{

typedef std::stack< String> Str_Stack;

void add_string( Str_Stack& str_stack, String str);

/* Finish coalescing the strings together, empty the
   stack, and return one big string *)
*/
const String string_from_stack( Str_Stack& str_stack);

}}

