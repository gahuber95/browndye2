#include "mm_bonded_parameter_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

MM_Bonded_Parameter_Info::MM_Bonded_Parameter_Info(Browndye::Force_Field_Type _fft) {
  fft = _fft;
}

MM_Bonded_Parameter_Info::MM_Bonded_Parameter_Info( Force_Field_Type _fft, Vector< JP::Node_Ptr>& nodes){

  fft = _fft;
  std::map< Bond_Type_Index, MM::Bond_Type> bond_dict;
  std::map< Angle_Type_Index, MM::Angle_Type> angle_dict;
  std::map< Dihedral_Type_Index, MM::Dihedral_Type> dihedral_dict;
  
  for( auto node: nodes){
  
    auto unode = node->child( "units");
    set_energy_units( unode);  
    
    auto top_bnode = node->child( "bonds");
    if (top_bnode){
      auto bnodes = top_bnode->children_of_tag( "bond");
      for( auto bnode: bnodes){
        MM::Bond_Type btype( *this, bnode);
        Bond_Type_Index nb{ bond_dict.size()};
        bond_dict.insert_or_assign( nb, btype);
      }
    }
    
    auto top_anode = node->child( "angles");
    if (top_anode){
      auto anodes = top_anode->children_of_tag( "angle");
      for( auto anode: anodes){
        MM::Angle_Type atype( *this, anode);
        Angle_Type_Index na{ angle_dict.size()};
        angle_dict.insert_or_assign( na, atype);
      }
    }
    
    auto top_dnode = node->child( "dihedrals");
    if (top_dnode){
      auto dnodes = top_dnode->children_of_tag( "dihedral");
      for( auto dnode: dnodes){
        MM::Dihedral_Type dtype( *this, dnode);
        Dihedral_Type_Index nd{ dihedral_dict.size()};
        dihedral_dict.insert_or_assign( nd, dtype);
      }
    }
  }
  
  auto sfill = [&]( auto& dict, auto& types){
    typedef std::decay_t< decltype( dict)> Dict;
    typedef typename Dict::key_type TIndex;
    TIndex n{ dict.size()};
    types.reserve( n);
    for( auto& item: dict){
      auto& stct = item.second;
      types.push_back( stct);
    }
  };

  sfill( bond_dict, bond_types);
  sfill( angle_dict, angle_types);
  sfill( dihedral_dict, dihedral_types);
}

}
