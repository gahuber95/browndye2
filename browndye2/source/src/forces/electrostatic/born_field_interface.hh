#pragma once

#include <string>
#include "../../lib/units.hh"

namespace Browndye{

class Born_Field_Interface{
public:
  typedef ::Charge OCharge;
  typedef decltype( OCharge()*OCharge()) Charge2;
  typedef decltype( Energy()/Charge2()) Potential;
  typedef Charge2 Charge;
  typedef decltype( Charge2()*Charge2()) Charge4;
  typedef decltype( Charge4()/(Length()*Energy())) Permittivity;
  typedef decltype( Potential()/Length()) Potential_Gradient;
  typedef std::string String;
};

}

