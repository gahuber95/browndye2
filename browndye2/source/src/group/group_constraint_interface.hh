#pragma once

#include "constraint.hh"
#include "group_common.hh"
#include "../molsystem/system_common.hh"
#include "../lib/overload.hh"
#include "../tet/tet_number.hh"
#include "../tet/tet_transform.hh"

namespace Browndye{

typedef std::variant< Chain_Index, Tet_Index> Chain_Or_Tet_Index;

//####################################
class Group_Thread;

// stored at thread level
struct Constraints_Info{
   
  Vector< Bead0_Index, Bead_Index> beads;
  Vector< Bead_Index, Bead0_Index> bead_indices;
  IDiff< Bead0_Index_Tag> mdiff, tdiff;
  IDiff< Constraint0_Index_Tag> cop_diff, tm_diff, tt_diff;
  
  Vector< Length_Constraint, Constraint0_Index> tet_core_constraints;
  Vector< Length_Constraint, Constraint0_Index> tet_tet_constraints;
  Vector< Constraint0_Index, Constraint_Index> constraints;
  
  // used for mobility
  Vector< Chain_Or_Tet_Index, Mob_Div_Index> divs;
    
  Group_State* state = nullptr;
  Group_Thread* thread = nullptr;
  bool initial_satisfaction = false;
  
  [[nodiscard]] size_t ithread() const;
};

//######################################
// generated on the fly
/*
template< class Group_State>
struct Con_Info_And_State{
  Group_State& state;
  const Constraints_Info& constraint_info;
  
  Con_Info_And_State( Group_State& _state, const Constraints_Info& _constraint_info):
    state(_state), constraint_info(_constraint_info){}
};
*/

//############################################################
class Group_Constraint_Interface;
class Group_State;

/*
struct Constraint_Info_And_Doer{
  //int count = 0; // is this used? Perhaps later
  
  Constraint_Info_And_Doer( Constraints_Info&& _info):
    info( std::move( _info)){}
};
*/

//#########################################
// chains, [core_atoms, tets]
class Group_Constraint_Interface{
public:
  typedef Constraints_Info System;
  typedef ::Mobility Mobility;
  typedef ::Length Length;
  typedef Browndye::Constraint_Index Constraint_Index;
  typedef Bead_Index Particle_Index;
  
  // technically should be generic
  static
  void get_position( const System&, Bead_Index ip, Pos&);
  
  static
  void get_position_change( const System&, Bead_Index ip, Pos&);
  
  static  
  void set_position( System&, Bead_Index, Pos);

  static
  std::pair< Bead_Index, Bead_Index>
  length_constraint_particles( const System&, Constraint_Index ic);
  
  static  
  Length constraint_length( const System&, Constraint_Index ic);

  static  
  bool is_coplanar_constraint( const System&, Constraint_Index ic);

  static
  Array< Bead_Index, 4>
  coplanar_constraint_particles( const System&, Constraint_Index);
  
  static  
  Bead_Index n_particles( const System&);

  static  
  Constraint_Index n_constraints( const System&);

  static  
  Mobility mobility( const System&, Bead_Index);
  
  //static
  //void get_interpolated_chains( const Group_Thread&, Group_State& gc, double frac);
  
  //static
  //void get_interpolated_cores( const Group_Thread&, Group_State& gc,
  //                              const Vector< Transform, Tet_Index>&);
  
  //static
  //Vector< Transform, Tet_Index>
  //interped_tforms( const Group_Thread&, Group_State& gc, double frac);
};

//########################################################################################
inline  
Constraint_Index
Group_Constraint_Interface::n_constraints( const System& gc){
  return gc.constraints.size();
}

inline
Bead_Index Group_Constraint_Interface::n_particles( const System& gc){
  return gc.beads.size();
}

}

