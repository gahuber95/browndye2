#pragma once

#include "v4.hh"
#include "../global/pos.hh"

namespace Browndye{

struct Tet_Fixed;

typedef Vector< Chain_Index, Ind_Chain_Index> Chain_Indices;
typedef Vector< Core_Index, Ind_Core_Index> Core_Indices;

struct Small_Tet{
  const Tet_Fixed* fixed = nullptr;
  V4< Pos> poses;
  Length radius;
};

typedef Vector< Small_Tet, Ind_Chain_Index> Small_Tets;

template< class Group_State>
struct Bead_Info{   
   const Group_State& state;
   const Small_Tets& stets;
   const Chain_Indices& chain_indices;
   Atom_Index nc_atoms;
   
   Bead_Info( const Group_State&, const Small_Tets&,
               const Chain_Indices&);
 };

struct Force_And_Torque{
  Vec3< Length2> force = Vec3< Length2>( Zeroi{});
  Vec3< Length3> torque = Vec3< Length3>(Zeroi{});
};  

struct RB_Motion{
  enum class Type {Translation, Rotation};
  
  Type type;
  size_t k;
};

template< class T>
class Bead_Chain_Mobility_Info_Type{};

template<>
class Bead_Chain_Mobility_Info_Type< Force_And_Torque>{
public:
  typedef Length2 Result;
};

template<>
class Bead_Chain_Mobility_Info_Type< RB_Motion>{
public:
  typedef Length Result;
};

// perhaps not needed
template< class GS>
struct Tet_Bead_Info{
  const Bead_Info< GS>& binfo;
  //Pos hc;
  
  Tet_Bead_Info( const Bead_Info< GS>& _binfo):
    binfo(_binfo){}
};

//#############################################3
// layout: core tets and then chains
template< class GS>
class Bead_Chain_Mobility_I{
public:
  typedef Tet_Bead_Info< GS> Bead_Info;
  typedef Bead_Info System;
  typedef Ind_Chain_Index Div_Index;
  typedef Atom_Index Bead_Index;
  typedef ::Length Length;
  typedef ::Time Time;
  typedef Inv_Length Mobility;
  typedef Length2 Force;
  typedef Length3 Torque;
  typedef Length Velocity;
  typedef typename GS::Chain_State Chain_State; 
        
  static  
  constexpr Ind_Chain_Index c0{ 0};
  
  static
  constexpr Ind_Chain_Index c1{ 1};
  
  static
  constexpr Atom_Index a0{ 0};
  
  static
  Ind_Chain_Index n_divisions( const Tet_Bead_Info< GS>& tbinfo){
    auto& binfo = tbinfo.binfo;
    auto nd =  binfo.chain_indices.size() +
            (binfo.stets.size() - c0);
    
    return nd;
  }
  
  //##########################################################
  template< class Ft, class Fc>
  static
  auto f_of_division( Ft& ft, Fc& fc,
                       const Tet_Bead_Info< GS>& tbinfo,
                       Ind_Chain_Index ic){
    
    auto& binfo = tbinfo.binfo;
    auto& stets = binfo.stets;
    auto nt = stets.size();
    if (ic < nt){
      return ft( stets[ic]);
    }
    else{
      auto jc = ic - (nt - c0);
      auto kc = binfo.chain_indices[ jc];
      return fc( binfo.state.chain_states[kc]);
    }    
  }
  
  //##########################################################
  static
  Atom_Index n_beads( const Tet_Bead_Info< GS>& tbinfo, Ind_Chain_Index ic){
    
    auto ft = [&]( const Small_Tet& stet){
      return Atom_Index( 4);
    };
    
    auto fc = [&]( const Chain_State& cstate){
      return cstate.atoms.size();
    };
    
    return f_of_division( ft, fc, tbinfo, ic);
  }
  
  //##########################################################
  static
  Pos bead_center( const Tet_Bead_Info< GS>& tbinfo,
                    Ind_Chain_Index ic, Atom_Index ia){
  
    //auto hc = tbinfo.hc;
    
    auto ft = [&]( const Small_Tet& stet){
      return stet.poses[ia];
    };
  
    auto fc = [&]( const Chain_State& cstate){
      return cstate.atoms[ia].pos;
    };
    
    return f_of_division( ft, fc, tbinfo, ic);
  }
  
  //##########################################################
  static
  Length bead_radius( const Tet_Bead_Info< GS>& tbinfo,
                       Ind_Chain_Index ic, Atom_Index ia){
    
    auto ft = [&]( const Small_Tet& stet){
      return stet.radius;
    };
    
    auto fc = [&]( const Chain_State& cstate){
      return cstate.common().atoms[ia].radius;
    };
    
    return f_of_division( ft, fc, tbinfo, ic);
  }
  
  //##########################################################
  static
  void add_quantity( const Tet_Bead_Info< GS>& tbinfo, Force_And_Torque& ft,
                      Ind_Chain_Index ic, Atom_Index ia, Vec3< Force> F){
    
    ft.force += F;
    auto r = bead_center( tbinfo, ic, ia);        
    ft.torque += cross( r, F);
  }
   
  // rotation about {0,0,0}
  static
  Vec3< Velocity> quantity( const Tet_Bead_Info< GS>&, RB_Motion, Ind_Chain_Index ic, Atom_Index ia);
  
  template< class T>
  class Info_Type{
  public:
    typedef typename Bead_Chain_Mobility_Info_Type< T>::Result Result;
  };
  
};

//##################################################################
template< class GS>
Vec3< Length>
Bead_Chain_Mobility_I< GS>::quantity( const Tet_Bead_Info< GS>& tbinfo,
                                     RB_Motion rbm,
                                    Ind_Chain_Index ic, Atom_Index ia){
  
  auto mtype = rbm.type;
  auto k = rbm.k;
  
  if (mtype == RB_Motion::Type::Translation){
    Vec3< Velocity> res( Zeroi{});
    res[k] = Velocity{1.0};
    return res;
  }
  else{
    Vec3< double> axis( Zeroi{});
    axis[k] = 1.0;
    auto pos = bead_center( tbinfo, ic, ia);
    auto res = cross( axis, pos);
    return res;
  }
}

}
