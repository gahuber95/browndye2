/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of NAM_Simulator
*/


#include <string> // debug
#include "nam_simulator.hh"
#include "../input_output/trajectory_outputter.hh"
//**************************************************************
// Outputting code

namespace Browndye {

    typedef std::string String;

//************************************************************************
// Movement code

    typedef std::lock_guard<std::mutex> Lock;

    void NAM_Simulator::run_thread(System_State *statep) {

      try {
        run_individual(*statep);
      }
      catch (...) {
        Lock lock(mutex);
        except = std::current_exception();
        has_error = true;
      }
    }

//#########################################################
    void NAM_Simulator::run() {

      const auto nrxns = common->n_reactions();
      completed_rxns.resize(nrxns + (Rxn_Index(1) - Rxn_Index(0)));
      completed_rxns.fill(0);
      n_trajs_completed = 0;
      n_trajs_started = 0;

      auto rt = [&](System_State *statep) {
          run_thread(statep);
      };

      auto async_f = [&](System_State *statep) {
          return std::async(std::launch::async, rt, statep);
      };

      std::vector<std::future<void> > results;
      for (auto &item: states) {
        auto &state = *(item.second);
        results.emplace_back(async_f(&state));
      }

      for (auto &res: results) {
        res.get();
      }

      if (has_error) {
        rethrow_exception( except);
      }
    }

//##########################################################
    void NAM_Simulator::run_individual( System_State &state) {

      auto outputter = new_trajectory_outputter(state);

      // stays in loop until all trajectories are done
      bool finished = false;
      while (!finished) {
        {
          Lock lock(mutex);
          if (n_trajs_started >= n_trajs) {
            finished = true;
          }
          else {
            ++n_trajs_started;
          }
          n_bd_steps += state.n_bd_steps;
        }

        if (!finished) {
          state.set_initial_state();
          start_trajectory_output(state, outputter);
          run_trajectory(state, outputter);

          if (has_error) {
            finished = true;
          }
          else {
            end_trajectory_output(state, outputter);
            output_results(state);
          }
        }
      }

      if (outputter) {
        outputter->end_trajectories();
      }
    }

//########################################################################
    void NAM_Simulator::output_results(const System_State &state) {

      Lock lock(mutex);

      auto out_dists = !(min_rxn_coord_name.empty());

      if (out_dists) {
        min_rxn_coords.emplace_back(state.minimum_rxn_coord);
      }

      if ((n_trajs_completed % n_trajs_per_output) == 0) {

        auto &common = state.common();
        std::ofstream outp(output_name);
        begin_output(outp);

        outp << "  <reactions>\n";
        outp << "    <n_trajectories> " << n_trajs_completed << " </n_trajectories>\n";
        outp << "    <stuck> " << n_stuck << " </stuck>\n";
        outp << "    <escaped> " << n_escaped << " </escaped>\n";

        const auto nrxns = common.n_reactions();
        for (auto irxn: range(nrxns)) {
          outp << "    <completed>\n";
          outp << "      <name> " <<
               common.reaction_name(irxn) << " </name>\n";
          outp << "      <n> " << completed_rxns[irxn] << " </n>\n";
          outp << "    </completed>\n";
        }

        outp << "  </reactions>\n";
        outp << "  <n_bd_steps> " << n_bd_steps << "</n_bd_steps>\n";
        outp << "</rates>\n";
        outp.flush();
        outp.close();

        if (out_dists) {

          bool first_one = (n_trajs_completed == n_trajs_per_output);
          std::ios_base::openmode omode = first_one ? std::ios_base::out : std::ios_base::app;
          std::ofstream loutp(min_rxn_coord_name.c_str(), omode);

          if (first_one) {
            loutp << "<min_dists>\n";
          }

          for (auto r: min_rxn_coords) {
            loutp << "  <min_dist> " << r <<
                  " </min_dist>\n";
          }

          min_rxn_coords.clear();

          if (n_trajs_completed == n_trajs) {
            loutp << "</min_dists>\n";
          }

          loutp.close();
        }
      }

    }

//###################################################################################################
/*
    std::unique_ptr<Trajectory_Outputter>
    NAM_Simulator::new_trajectory_outputter( const System_State &state) {

      auto &thread = state.thread();
      auto &common = thread.common();

      std::unique_ptr<Trajectory_Outputter> outputter;

      if (!(traj_file_name.empty())) {
        std::string number(uitoa(thread.number / Thread_Index(1)));
        std::string suffix(".xml");
        std::string itext(".index");

        auto ftf_name = traj_file_name + number + suffix;
        auto ftif_name = traj_file_name + number + itext + suffix;

        auto nopb = n_output_states_per_block;
        outputter = std::make_unique<Trajectory_Outputter>(common, ftf_name,
                                                           ftif_name, nopb);
      }

      return outputter;
    }
*/
//#########################################################################################
    void NAM_Simulator::run_trajectory(System_State &state, const std::unique_ptr<Trajectory_Outputter> &outputter) {

      auto &fate = state.fate;
      fate = System_Fate::Undefined;

      bool finished = false;
      while (!finished) {

        if (outputter &&
            (state.n_full_steps % n_steps_per_output == 0)) {
          outputter->output_state(state);
          state.t_since_output = Time(0.0);
        }

        const auto istate = state.current_rxn_state;
        state.do_large_step(Thread_Index());

        if (state.current_rxn_state != istate) {
          change_of_state(state, outputter);
        }

        check_done(state, finished);
        check_stuck(state, finished);
      }
    }

//##################################################################
    void NAM_Simulator::change_of_state(System_State &state,
                                        const std::unique_ptr<Trajectory_Outputter> &outputter) {

      auto &fate = state.fate;
      auto &common = state.common();
      const auto new_istate = state.current_rxn_state;

      if (outputter) {
        if ((fate == System_Fate::In_Action) &&
            (state.n_full_steps <= max_n_steps)) {
          outputter->end_subtrajectory(fate,
                                       common.reaction_name(state.last_rxn),
                                       common.state_name(new_istate));

          outputter->start_subtrajectory(common.state_name(new_istate));
        }

        outputter->output_state(state);
        state.t_since_output = Time(0.0);
      }

      Lock lock(mutex);
      ++completed_rxns[state.last_rxn];
    }

//####################################################################
    void NAM_Simulator::check_done(System_State &state, bool &finished) {

      auto &fate = state.fate;
      if (fate != System_Fate::In_Action) {
        Lock lock(mutex);

        ++n_trajs_completed;
        if (fate == System_Fate::Escaped) {
          ++n_escaped;
        }

        finished = true;
      }
    }

//###########################################################
    void NAM_Simulator::check_stuck(System_State &state, bool &finished) {

      if (!finished && state.n_full_steps > max_n_steps) {
        state.fate = System_Fate::Stuck;
        Lock lock(mutex);

        ++n_stuck;
        ++n_trajs_completed;
        finished = true;
      }
    }

//**************************************************************
// Initialization code

    namespace JP = JAM_XML_Pull_Parser;

// constructor
    NAM_Simulator::NAM_Simulator(JP::Node_Ptr node) : Single_Traj_Simulator(node) {

      n_trajs = checked_value_from_node<size_t>(node, "n_trajectories");

      auto get_val = [&](const std::string &tag, auto &val) {
          typedef std::decay_t<decltype(val)> T;
          auto valo = value_from_node<T>(node, tag);
          if (valo) {
            val = *valo;
          }
      };

      get_val("min_rxn_dist_file", min_rxn_coord_name);
      get_val("n_output_states_per_block", n_output_states_per_block);

      std::string dors;
      get_val("print_rng_state", dors);
      //do_output_rng_state = (dors == std::string("yes"));
    }
}
