/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation. Uses Cartesian formulation out to quadrupole level.

//#include <cmath>
#include <fstream>
//#include <limits>
#include "charge_field.hh"
#include "../../xml/node_info.hh"
//#include "../../global/pi.hh"
#include "../../lib/autodiff.hh"

namespace {
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
}

namespace Browndye{
  using namespace Autodiff; 
  
  void Charge_Field::get_potential_n_grads( Pos pos,
                                           EPotential& phi_out,
                                     Vec3< EPotential_Gradient>& grad,
                                     bool get_grad) const{

    auto ccx = constant<1>( cx);
    auto ccy = constant<2>( cy);
    auto ccz = constant<3>( cz);
    auto cdebye = constant<4>( debye);
    auto factor = constant<5>( charge/(pi4*vperm*solvdi));
    
    auto posx = inputv<1>( pos[0]);
    auto posy = inputv<2>( pos[1]);
    auto posz = inputv<3>( pos[2]);
    
    auto x = posx - ccx;
    auto y = posy - ccy;
    auto z = posz - ccz;
    
    auto r2 = x*x + y*y + z*z;
    auto r = sqrt( r2);
   
    auto phi = factor*exp(-r/cdebye)/r;
    phi_out = value( phi);
    
    if (get_grad){
      auto dphi = derivative_struct( phi);
      grad[0] = derivative( dphi, posx);
      grad[1] = derivative( dphi, posy);
      grad[2] = derivative( dphi, posz);
    }
  }

  //#########################################################
  EPotential Charge_Field::potential( Pos pos) const{
    Vec3< EPotential_Gradient> grad;
    EPotential phi;
    get_potential_n_grads( pos, phi, grad, false);
    return phi;
  }

  //############################################################
  auto Charge_Field::gradient( Pos pos) const -> Vec3< EPotential_Gradient>{

    EPotential phi;
    Vec3< EPotential_Gradient> grad;
    get_potential_n_grads( pos, phi, grad, true);
    return grad;
  }
  
  //############################################################## 
  inline
  JP::Node_Ptr child( const JP::Node_Ptr& node, const string& tag){
    JP::Node_Ptr cnode = checked_child( node, tag);
    if (cnode == nullptr){
      error( "child: no node of ", tag);
    }
    return cnode;
  } 

  double fdata( const JP::Node_Ptr& node){
    return stod( node->data()[0]);
  }

  template< class Value>
  void get_v3_from_node( const JP::Node_Ptr& node, const string& tag,
                         Value& x, Value& y, Value& z){
    
    auto cnode = child( node, tag);
    
    auto xnode = child( cnode, "x");
    auto ynode = child( cnode, "y");
    auto znode = child( cnode, "z");
    
    set_fvalue( x, fdata( xnode));
    set_fvalue( y, fdata( ynode));
    set_fvalue( z, fdata( znode));
  }

  // constructor
  Charge_Field::Charge_Field( const string& file, Pos offset){
    std::ifstream input( file);
    
    if (!input.is_open()){
      error( "charge file ", file, " could not be opened");
    }
    initialize( input, file, offset);
  }

  // constructor
  /*
  Charge_Field::Charge_Field( std::ifstream& input, Pos offset){ 
    initialize( input, "", offset);
  }
  */

  /*
  // constructor
  Charge_Field::Charge_Field( JP::Parser& parser, Pos offset){
    initialize( parser, offset);
  }
*/

  void Charge_Field::initialize( std::ifstream& input, const string& file, Pos offset){
    JP::Parser parser( input, file);
    initialize( parser, offset);
  }
  
  void Charge_Field::initialize( JP::Parser& parser, Pos offset){
  
    auto node = parser.top();
    node->complete();

    auto gval = [&]( auto& val, const string& tag){
      typedef std::remove_reference_t< decltype( val)> T;
      val = checked_value_from_node< T>( node, tag);
    };

    gval( charge, "charge");
    gval( debye, "debye");
    gval( vperm, "vacuum_permittivity");
    gval( solvdi, "solvent_dielectric");
    
    get_v3_from_node( node, "center", cx,cy,cz);  
    shift_position( offset); 
  }
  
}


