#pragma once

#include <numeric>
#include <tuple>
#include <random>
#include "../lib/vector.hh"
#include "../lib/array.hh"
#include "../lib/range.hh"
#include "../lib/linalg.hh"
#include "rotne_prager.hh"
#include "../lib/inverse3x3.hh"
#include "../lib/solve3.hh"
#include "mobility_matrices.hh"
#include "mat3_division.hh"
#include "../lib/rotations.hh"

// different system can be used from that given to the constructor

namespace Browndye{
  
namespace Generic_Mobility{

using std::move;
using std::get;

class Atom_Tag{};
class Div_Tag{};

//#################################################
template< class I>
struct Doer{
  typedef typename I::Div_Index DIndex; // division
  typedef typename I::Bead_Index AIndex;
  typedef typename I::System System;
  typedef typename I::Length Length;
  typedef typename I::Time Time;
  
  typedef decltype( 1.0/Length()) Mob;
  typedef decltype( sqrt( Mob())) Sqrt_Mob;
  typedef decltype( 1.0/Sqrt_Mob()) Inv_Sqrt_Mob;
  typedef decltype( 1.0/Mob()) Inv_Mob;
  typedef Vec3< Length> Pos;
  typedef decltype( Length()*Length()) Len2;
  typedef decltype( Len2()*Length()) Len3;
  typedef decltype( 1.0/Length()) Inv_Len;
  
  Doer() = delete;
  Doer( const Doer&) = delete;
  Doer& operator=( const Doer&) = delete;
  Doer( Doer&&) = default;
  Doer& operator=( Doer&&) = default;
  Doer( const System&);

  class Blank{};
  Doer( Blank){};

  Doer copy() const;

  template< class F_Vec, class V_Vec>
  void
  add_product_with_mobility( const System& system, const F_Vec& fvec, V_Vec& vvec) const;
  
  template< class W_Info, class D_Vec>
  void add_stochastic_propagation( const System&, const W_Info&, D_Vec&) const;
    
  std::tuple< Mat3< Length>, Mat3< Len3>, Vec3< Length> >
  resistance_matrices_and_com( const System&) const;   
    
  template< class V_Vec, class F_Vec>
  void
  add_product_with_resistance( const System& system, const V_Vec& vvec, F_Vec& fvec) const;  
    
  void update( const System&);
    
  void test_center( std::mt19937_64& gen) const;
    
//#########################################################################  
private:
  void resize( const System&);
  
  template< class T, class Idx>
  using GMatrix = Vector< Vector< Mat3<T>, Idx>, Idx>;
  
  template< class T, class Idx>
  using GVector = Vector< Vec3< T>, Idx>;
  
  // data
  Vector< GMatrix< Mob, AIndex>, DIndex> diag_M; 
  Vector< GMatrix< Sqrt_Mob, AIndex>, DIndex> diag_L; 
  GVector< Length, DIndex> mob_centers;
  Vector< Length, DIndex> hydro_radii;
  
  GMatrix< Mob, DIndex> coarse_M; // assume full storage
  GMatrix< Sqrt_Mob, DIndex> coarse_L;
  
  typedef decltype( sqrt( Time())) Sqrt_Time;
  mutable GVector< Length, AIndex> temp_dx;
  
  // private functions
  void compute_coarse_M( const System&);
  
  template< class T>
  static
  void resize_coarse( const System& system, GMatrix< T, DIndex>& M);
  
  template< class T>
  static
  void resize_diag( const System&, Vector< GMatrix< T, AIndex>, DIndex>&);
  
  template< class F_Info, class V_Info>
  void add_fine_velocities( const System& system, const F_Info& finfo, V_Info& vinfo) const;
  
  template< class F_Info, class V_Info>
  void add_coarse_velocities( const System& system, const F_Info& finfo, V_Info& vinfo) const;
  
  template< class T, class Idx>
  static
  GMatrix< decltype( sqrt(T())), Idx>
  cholesky_of( const GMatrix< T, Idx>& M);
  
  template< class Lt, class Vt, class Idx>
  static
  auto solution( const GMatrix< Lt, Idx>& L,
                const GVector< Vt, Idx>& v);
  
  void compute_hydro_quantities( const System& system);
  void update_off_diag_M( const System& system);
  void do_cholesky_decomposition( const System& system);
  void compute_diag_L( DIndex id);
  void compute_offdiag_L( DIndex id);
  Pos approximate_center( const System& system,
                           const Vector< Pos, AIndex>& centers,
                            const Vector< Length, AIndex>& radii) const;
  auto centers_and_radii( const System& system, DIndex id) const;
  void compute_diag_M( const System&);
  
  auto centers_radii_and_approx_center( const System& system, DIndex id) const;
  
  static
  auto AB_resistance_matrices( const Vector< Pos, AIndex>& centers, const GMatrix< Sqrt_Mob, AIndex>& L);
  
  static
  Mat3< Len3> C_resistance_matrix( const Vector< Pos, AIndex>& centers, const Vector< Length, AIndex>& radii,
                                  const GMatrix< Sqrt_Mob, AIndex>& L);
  
  static
  std::pair< Pos, Length> mob_center_and_hydro_radius( Mat3< Length> A, Mat3< Len2> B, Mat3< Len3> C) ;
   
  template< class M, class Index, class V>
  static
  auto sym_product( const Vector< Vector< M, Index>, Index>& m,
                 const Vector< V, Index>& v);
   
  template< class DoA, class M, class Indexi, class V_Info,
            class Indexj, class D>
  void get_lower_and_diag_product( Indexi id, const System&,
                                     const GMatrix< M, Indexj>& m,
                                     const V_Info& vinfo,
                                     GVector< D, Indexj>& d) const;
  
  static
  auto centers_for_test( AIndex na, Length radius, Length side_len);
  
  static
  void rotate_and_shift( std::mt19937_64& gen, Pos pos, Vector< Pos, AIndex>& centers);
  
  static
  auto random_position( std::mt19937_64& gen, Length side_len);
  
  static
  auto rotne_prager_matrix( Length a0, Length a1, Pos cen0, Pos cen1);
  
  static
  auto mob_center( const Vector< Vector< Mat3< Sqrt_Mob>, AIndex>, AIndex>& L,
                    const Vector< Pos, AIndex>& centers, Length radius);
  
  static
  auto single_trans_matrix( Length a);
  
  class Copy_Tag{};
  Doer( const Doer& other, Copy_Tag);
  
  // used for Wiener process
  template< class Info>
  auto cf_quantity( Atom_Tag, const System& system,
                     const Info& info, DIndex id, AIndex ia) const{
    return I::quantity( system, info, id, ia);
  }
  
  template< class Info>
  auto cf_quantity(  Div_Tag, const System& system,
                     const Info& info, DIndex, DIndex id) const{
    return I::div_quantity( system, info, id);
  }
  
  Vec3< Length> whole_center() const;
  Mat3< Len3> coarse_C_matrix( Vec3< Length> center) const;
  Mat3< Length> A_matrix() const;
  Mat3< Len3> fine_C_matrix( const System&) const; 
};

//#########################################################################
// constructor
template< class I>
Doer<I>::Doer( const Doer& other, Copy_Tag){
  diag_M = other.diag_M.copy();
  diag_L = other.diag_L.copy();
  mob_centers = other.mob_centers.copy();
  hydro_radii = other.hydro_radii.copy();
  coarse_L = other.coarse_L.copy();
  coarse_M = other.coarse_M.copy();
}

template< class I>
Doer<I> Doer<I>::copy() const{
  return Doer( *this, Copy_Tag());
}



//#########################################################################
template< class I>
template< class F_Info, class V_Info>
void
Doer<I>::add_product_with_mobility( const System& system,
                                       const F_Info& finfo, V_Info& vinfo) const{
  
  add_fine_velocities( system, finfo, vinfo);
  add_coarse_velocities( system, finfo, vinfo);
}

//##########################################################################
// ensure that W_Info has the sqrt(2) factor already in it!
// (change later)
template< class I>
template< class W_Info, class D_Vec>
void Doer<I>::add_stochastic_propagation( const System& system, const W_Info& winfo, D_Vec& dvec) const{
 
  auto nd = I::n_divisions( system);
  for( auto id: range( nd)){
    auto na = I::n_beads( system, id);
    
    auto& L = diag_L[id];
    get_lower_and_diag_product< Atom_Tag>( id, system, L, winfo, temp_dx);
    
    Vec3< Length> sumd( Zeroi{});
    for( auto ia: range( na)){
      sumd += temp_dx[ia];
    }
    
    Vec3< Length> avgd = (1.0/(na/AIndex(1)))*sumd;
    for( auto ia: range( na)){
      temp_dx[ia] -= avgd;
    }
    
    for( auto ia: range( na)){
      I::add_quantity( system, dvec, id, ia, temp_dx[ia]);
    }
  }
      
  GVector< Length, DIndex> dx( nd);  
  get_lower_and_diag_product< Div_Tag>( DIndex{}, system, coarse_L, winfo, dx);  
  
  for( auto id: range( nd)){
    auto dxd = dx[id];
    auto na = I::n_beads( system, id);
    for( auto ia: range( na)){
      I::add_quantity( system, dvec, id, ia, dxd);
    }
  }

}

//##########################################################
template< class Mt, class Idx>
class I_Mat_Cholesky{
public:
  typedef decltype( sqrt( Mt())) Lt;
     
  template< class T>
  using GMatrix = Vector< Vector< Mat3<T>, Idx>, Idx>;
  
  typedef GMatrix< Mt> Matrix;
  typedef GMatrix< Lt> Dec_Matrix;
  typedef Mat3<Mt> Matrix_Value;
  typedef Mat3<Lt> Dec_Matrix_Value;
  typedef Idx Index;
  
  template< class T>
  static
  auto element( const GMatrix< T>& M, Idx i, Idx j){
    return M[i][j];
  }
  
  template< class T>
  static
  auto& element_ref( GMatrix< T>& M, Index i, Index j){
    return M[i][j];
  }
  
  template< class T>
  static
  Mat3< T> transpose( Mat3< T> m){
    return Browndye::transpose( m);
  }
  
  template< class T>
  static
  Index size( const GMatrix< T>& M){
    return M.size();
  }
    
  template< class T>
  class Vector_Type{
  public:
    typedef Vector< Vec3< T>, Idx> Result;
  };
   
  template< class T>
  static
  Vec3<T> element( const Vector< Vec3<T>, Idx>& v, Index i){
    return v[i];
  }
  
  template< class T>
  static
  Vec3<T>& element_ref( Vector< Vec3<T>, Idx>& v, Index i){
    return v[i];
  }
  
  template< class MV>
  static
  MV zero(){
    return MV( Zeroi{});
  }
  
  template< class T>
  static
  auto determinant( Mat3< T> M){
    auto a = M[0][0];
    auto b = M[0][1];
    auto c = M[0][2];
    auto d = M[1][0];
    auto e = M[1][1];
    auto f = M[1][2];
    auto g = M[2][0];
    auto h = M[2][1];
    auto i = M[2][2];
    
    return a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h;
  }
  
  template< class T>
  static
  bool is_negative( Mat3<T> M){
    typedef decltype( T()*T()*T()) T3;
    auto det = determinant( M);  
    return det < T3( 0.0);
  }
  
  template< class T>
  static
  bool is_zero( Mat3<T> M){
    typedef decltype( T()*T()*T()) T3;
    auto det = determinant( M);  
    return det == T3( 0.0);
  }
  
  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }
};

//###################################################################
template< class I>
auto Doer<I>::whole_center() const -> Vec3< Length> {
 
  auto nd = coarse_L.size();
  Vec3< Len2> c_sum( Zeroi{});
  Length a_sum{ 0.0};
  for( auto id: range( nd)){
    auto a = hydro_radii[id];
    auto cen = mob_centers[id];
    a_sum += a;
    c_sum += a*cen;
  }
  return (1.0/a_sum)*c_sum;  
}

//###############################################################
template< class I>
auto Doer<I>::coarse_C_matrix( Vec3< Length> center) const -> Mat3< Len3>{
  
  typedef decltype( 1.0/Len2{}) Vel;
  typedef decltype( 1.0/Len3()) Om;  
  
  auto nd = coarse_L.size();
  GVector< Vel, DIndex> vs( nd);
  GVector< Inv_Len, DIndex> cforce( nd);

  Mat3< Len3> C;     

  for( int k: range(3)){
    for( auto id: range( nd)){
      auto& v = vs[id];
      Vec3< Om> omega( Zeroi{});
      omega[k] = Om( 1.0);
      v = cross( omega, (mob_centers[id] - center));
    }
    
    Cholesky::solve< I_Mat_Cholesky< Mob, DIndex>>( coarse_L, vs, cforce);
    
    Vec3< double> Tsum( Zeroi{});
    for( auto id: range( nd)){
      auto f = cforce[id];
      auto d = mob_centers[id] - center;
      Tsum += cross( d, f);
    }
    C[k] = (1.0/Om(1.0))*Tsum;
  }
  return C;
}

//##########################################################
template< class I>
auto Doer<I>::A_matrix() const -> Mat3< Length>{
  auto nd = coarse_L.size();
  GVector< Inv_Len, DIndex> cforce( nd);
  
  typedef decltype( 1.0/Len2{}) Vel;
  Mat3< Length> A;
  GVector< Vel, DIndex> vs( nd);
  for( int k: range(3)){
    for( auto& v: vs){
      v = Vec3< Vel>( Zeroi{});
      v[k] = Vel( 1.0);
    }
    
    Cholesky::solve< I_Mat_Cholesky< Mob, DIndex>>( coarse_L, vs, cforce);
    
    Vec3< Inv_Len> Fsum( Zeroi{});
    for( auto& f: cforce){
      Fsum += f;
    }
    A[k] = (1.0/Vel(1.0))*Fsum;
  }
  return A;
}

//###################################################################
template< class I>
auto Doer<I>::fine_C_matrix( const System& system) const -> Mat3< Len3>{
  
  typedef decltype( 1.0/Len3()) Om;
  typedef decltype( 1.0/Len2{}) Vel;
  
  Mat3< Len3> C( Zeroi{});
  
  auto nd = coarse_L.size();
  for( auto id: range( nd)){
    auto& L = diag_L[id];
    auto na = L.size();
    
    GVector< Vel, AIndex> vf( na);
    
    for( int k: range(3)){
      Vec3< Om> omega( Zeroi{});
      omega[k] = Om( 1.0);
    
      auto cen = mob_centers[id];
      for( auto ia: range( na)){
        auto pos = I::bead_center( system, id, ia);
        auto d = pos - cen;
        vf[ia] = cross( omega, d);
      }
      
      GVector< Inv_Len, AIndex> fforce( na);
      Cholesky::solve< I_Mat_Cholesky< Mob, AIndex>>( L, vf, fforce);
      
      Vec3< double> Tsum( Zeroi{});
      for( auto ia: range( na)){
        auto f = fforce[ia];
        auto pos = I::bead_center( system, id, ia);
        auto d = pos - cen;
        Tsum += cross( d, f);
      }
      
      C[k] += (1.0/Om(1.0))*Tsum;
    }
  }
  
  return C;
}

//###################################################################
template< class I>
auto Doer<I>::resistance_matrices_and_com( const System& system) const ->
  std::tuple< Mat3< Length>, Mat3< Len3>, Vec3< Length> >{
    
  // scaled units: F -> 1/L, v -> L^-2  
  auto A = A_matrix();  
  auto center = whole_center();  
  auto Cc = coarse_C_matrix( center);    
  auto Cf = fine_C_matrix( system);
  auto C = Cc + Cf; 
   
  return std::make_tuple( A, C, center);    
}


//#########################################
// constructor
template< class I>
Doer<I>::Doer( const System& system){

  resize( system);
}

//###############################################################
template< class I>
void Doer<I>::compute_coarse_M( const System& system){

  auto nd = I::n_divisions( system);
  for( auto id: range( nd)){
    auto ceni = mob_centers[id];
    auto ai = hydro_radii[id];
    coarse_M[id][id] = single_trans_matrix( ai);
   
    for( auto jd: range( id)){
      auto cenj = mob_centers[jd];
      auto aj = hydro_radii[jd];
      auto d = cenj - ceni;
      auto M = Rotne_Prager::trans_matrix( ai, aj, d);
      coarse_M[id][jd] = M;
      coarse_M[jd][id] = M;
    }
  }
}

//#################################################################
template< class I>
void
Doer<I>::update( const System& system){
    
  compute_diag_M( system);
  do_cholesky_decomposition( system);  
  
  compute_hydro_quantities( system);
  compute_coarse_M( system);  
  coarse_L = cholesky_of( coarse_M);
}

//#########################################################
template< class I>
auto Doer<I>::centers_for_test( AIndex na, Length radius, Length side_len){
  
  Vector< Pos, AIndex> centers( na);
      
  AIndex i( 0);
  for( auto kx: range( 2))
    for( auto ky: range( 2))
      for( auto kz: range( 2)){
        auto x = (0.5 - (double)kx)*side_len;
        auto y = (0.5 - (double)ky)*side_len;
        auto z = (0.5 - (double)kz)*side_len;
        auto& cen = centers[i];
        cen[0] = x;
        cen[1] = y;
        cen[2] = z;
        ++i;
      }
   
  return centers;
}

//#############################################################################
template< class I>
auto Doer<I>::random_position( std::mt19937_64& gen, Length side_len){
  
  auto blen = 5.0;
  std::uniform_real_distribution< double> uni( -blen, blen);
  Pos pos;
  for( auto k: range(3))
     pos[k] = side_len*( uni( gen));
  return pos;
}

//########################################################################
class Rot_Interface{
public:
  typedef std::mt19937_64 Random_Number_Generator;
  
  static
  double gaussian( std::mt19937_64& gen){
    std::normal_distribution< double> gauss;
    return gauss( gen);
  }
};

//############################################################################################
template< class I>
void Doer<I>::rotate_and_shift( std::mt19937_64& gen, Pos pos, Vector< Pos, AIndex>& centers){
 
  auto rot = random_rotation< Rot_Interface>( gen);
  for( auto& center: centers)
    center = rot*center;
    
  for( auto& center: centers)
    center += pos;
}


//#############################################################################
template< class I>
template< class F_Info, class V_Info>
void Doer<I>::add_coarse_velocities( const System& system,
                                       const F_Info& finfo, V_Info& vinfo) const{
  
  auto nd = diag_M.size();
  typedef decltype( I::quantity( system, finfo, nd, AIndex{})) Ft;
  
  Vector< Ft, DIndex> Fc( nd, Ft( Zeroi{}));
  
  for( auto id: range( nd)){
    auto na = I::n_beads( system, id);
    auto& F = Fc[id];
    for( auto ia: range( na)){
      F += I::quantity( system, finfo, id, ia);
    }
  }
  
  typedef decltype( coarse_M[ DIndex{}][ DIndex{}]) Mt;
  typedef decltype( Mt{}*Ft{}) Vt;
  Vector< Vt, DIndex> vc( nd, Vt( Zeroi{}));
  
  for( auto id: range(nd)){

    auto& v = vc[id];
    for( auto jd: range(nd)){
      v += coarse_M[id][jd]*Fc[jd];
    }
  }
  
  for( auto id: range( nd)){
    auto na = I::n_beads( system, id);
    
    auto v = vc[id];
    for( auto ia: range( na)){
      I::add_quantity( system, vinfo, id, ia, v);
    }
  }  
}

//########################################################################
template< class I>
template< class F_Info, class V_Info>
void Doer<I>::add_fine_velocities( const System& system, const F_Info& finfo, V_Info& vinfo) const{
  
  auto nd = diag_M.size();
  for( auto id: range( nd)){
    
    auto na = I::n_beads( system, id);
    typedef decltype( I::quantity( system, finfo, id, AIndex())) Ft;
    Vector< Ft, AIndex> F( na);
    for( auto ia: range( na)){
      F[ia] = I::quantity( system, finfo, id, ia);
    }
    
    Ft Ftot( Zeroi{}); // make more generic
    for( auto f: F){
      Ftot += f;
    }
    
    auto frac = 1.0/(na/AIndex(1));
    
    auto Fmean = frac*Ftot;
    
    for( auto& f: F){
      f -= Fmean;
    }
        
    auto v = sym_product( diag_M[id], F);
    
    typename decltype( v)::value_type vtot( Zeroi{}); // make more generic
    for( auto va: v){
      vtot += va;
    }
    
    auto vmean = frac*vtot; 
        
    for( auto& va: v){
      va -= vmean;
    }
    
    for( auto ia: range( na)){
      I::add_quantity( system, vinfo, id, ia, v[ia]);
    }
  }
}

//########################################################################
template< class I>
template< class M, class Index, class V>
auto Doer<I>::sym_product( const Vector< Vector< M, Index>, Index>& m,
               const Vector< V, Index>& v){
 
  auto n = m.size();
  typedef decltype( M()*V()) S;
  
  Vector< S, Index> res( n);
  for( auto i: range( n)){
    S sum( Zeroi{});
    for( auto j: range( i)){  
      sum += m[i][j]*v[j];
    }
    for( auto j: range( i, n)){
      sum += transpose( m[j][i])*v[j];
    }
    res[i] = sum;
  }
  return res;
}

//########################################################################
template< class I>
template< class DoA, class M, class Indexi, class V_Info,
           class Indexj, class D>
 void Doer<I>::get_lower_and_diag_product( Indexi id,
                                    const System& system,      
                                    const GMatrix< M, Indexj>& m,
                                    const V_Info& vinfo,
                                    GVector< D, Indexj>& d) const{  
  
  
  auto n = m.size();
  for( auto i: range( n)){
    Vec3<D> sum( Zeroi{});
    for( auto j: range( i + (Indexj(1) - Indexj(0)))){
      auto subv = cf_quantity( DoA{}, system, vinfo, id, j);      
      sum += m[i][j]*subv;
    }  
    d[i] = sum;
  }
}

//####################################################################
// L is cholesky decomp of actual matrix

template< class I>
template< class Lt, class Bt, class Idx>
auto Doer<I>::solution( const GMatrix< Lt, Idx>& L,
                       const GVector< Bt, Idx>& b){

  auto n = b.size();

  typedef decltype( Lt()*Lt()) Mt;
  typedef decltype( Bt()/Mt()) Xt;
  
  GVector< Xt, Idx> x( n);
  
  Cholesky::solve< I_Mat_Cholesky< Mt, Idx> >( L, b, x);
  return x;
}

//####################################################################
template< class I>
void Doer< I>::resize( const typename I::System& system){
    
  resize_diag( system, diag_M);
  resize_diag( system, diag_L);
  resize_coarse( system, coarse_M);
  resize_coarse( system, coarse_L);
  
  // refactor
  AIndex na{ 0};
  auto nd = I::n_divisions( system);
  for( auto id: range( nd)){
    na = std::max( na, I::n_beads( system, id));
  }
  
  temp_dx.resize( na);
}

//###############################################################################
template< class I>
template< class T>
void Doer< I>::resize_coarse( const System& system, GMatrix< T, DIndex>& M){
  
  auto nd = I::n_divisions( system);
  M.resize( nd);
  for( auto& row: M){
    row.resize( nd);
  }
}

//#########################################################################
template< class I>
template< class T>
void Doer< I>::resize_diag( const System& system, Vector< GMatrix< T, AIndex>, DIndex>& diag_A){
  
  auto nd = I::n_divisions( system);
  diag_A.resize( nd);
  for( auto id: range( nd)){
    auto na = I::n_beads( system, id);
    diag_A[id].resize( na);
    for( auto ia: range( na))
      diag_A[id][ia].resize( ia + (AIndex(1) - AIndex(0)));
  } 
}

//###################################################################
template< class I>
void Doer<I>::do_cholesky_decomposition( const System& system){
  
  auto nd = I::n_divisions( system);
  for( auto id: range( nd)){
    diag_L[id] = cholesky_of( diag_M[id]);  
  }
}

//#############################################################
template< class I>
void Doer<I>::compute_diag_M( const System& system){
  
  auto nd = I::n_divisions( system);
  for( auto id: range( nd)){
    
    auto cr_res = centers_radii_and_approx_center( system, id);
    
    auto& centers = get<0>( cr_res);
    auto& radii = get<1>( cr_res);
     
    auto na = I::n_beads( system, id);
    for( auto ia: range( na)){
      auto ai = radii[ia];
      diag_M[id][ia][ia] = single_trans_matrix( ai);
      
      auto posi = centers[ia];
      for( auto ja: range( ia)){
        auto aj = radii[ja];
        auto posj = centers[ja];
        auto rij = posj - posi;
        diag_M[id][ia][ja] = Rotne_Prager::trans_matrix( ai, aj, rij);
      }
    }
  }
}

//################################################################
template< class I>
auto Doer<I>::single_trans_matrix( Length a){
  
  if (a > Length{0.0}){
    return Rotne_Prager::single_t_mobility( a)*id_matrix<3>();
  }
  else{
    return Inv_Len( large)*id_matrix< 3>();
  }
}


//################################################################
template< class I>
void Doer<I>::compute_hydro_quantities( const System& system){
  
  auto nd = I::n_divisions( system);
  mob_centers.resize( nd);
  hydro_radii.resize( nd);
    
  for( auto id: range( nd)){
    auto cr_res = centers_radii_and_approx_center( system, id);
    auto& centers = get<0>( cr_res);
    auto& radii = get<1>( cr_res);
    auto appx_center = get<2>( cr_res);
    
    if (centers.size() > AIndex{0}){
      auto& L = diag_L[id]; 
      auto AB = AB_resistance_matrices( centers, L);
      auto& A = AB.first;
      auto& B = AB.second;
      auto C = C_resistance_matrix( centers, radii, L);
            
      auto cen_rad = mob_center_and_hydro_radius( A, B, C);
      auto com = cen_rad.first;
      auto hydro_radius = cen_rad.second;
       
      hydro_radii[id] = hydro_radius;
      mob_centers[id] = appx_center + com;
    }
    else{
      hydro_radii[id] = Length( 0.0);
      mob_centers[id] = Vec3< Length>( Zeroi{});
    }
  }
}

//#####################################################################################
template< class I>
auto Doer<I>::centers_radii_and_approx_center( const System& system, DIndex id) const{
  
  auto cr_res = centers_and_radii( system, id);
  auto& centers = get<0>( cr_res);
  auto& radii = get<1>( cr_res);
  auto appx_center = approximate_center( system, centers, radii);
  for( auto& center: centers){
    center -= appx_center;
  }
  
  return std::make_tuple( move( centers), move( radii), appx_center);
}

//#############################################################################################################
template< class I>
auto Doer<I>::mob_center_and_hydro_radius( Mat3< Length> A, Mat3< Len2> B, Mat3< Len3> C) -> std::pair< Pos, Length>{
  
  auto mob_res = mobility_matrices_and_com( A, B, C);
  auto com = get< 3>( mob_res);
  auto a = get< 0>( mob_res);
  auto b = get< 1>( mob_res);
  auto c = get< 2>( mob_res);
  
  auto acen = a - cross( cross( com, c), com) - cross( com, b) + cross( transpose(b), com);
  auto mean_a = trace( acen)/3.0;
  auto hydro_radius = 1.0/(pi6*mean_a);
  
  return std::make_pair( com, hydro_radius);
}

//##########################################################################################################
template< class I>
auto Doer<I>::C_resistance_matrix( const Vector< Pos, AIndex>& centers, const Vector< Length, AIndex>& radii,
                                        const Vector< Vector< Mat3< Sqrt_Mob>, AIndex>, AIndex>& L) -> Mat3< Len3>{
  
  // rotational component
  Len3 crot( 0.0);
  for( auto& radius: radii){
    crot += pi8*radius*radius*radius;
  }

  auto C = crot*id_matrix<3>();
  
  // translational component
  for( int k: range( 3)){
    
    typedef Length Vel;
    auto na = L.size();
    Vector< Vec3< Vel>, AIndex> v( na);
    
    typedef double Om;
    Vec3< Om> omega( Zeroi{});
    omega[k] = Om( 1.0);
    
    for( auto ia: range( na)){
      v[ia] = cross( omega, centers[ia]);
    }
    auto F = solution( L, v);
    
    for( auto ia: range( na)){
      C[k] += cross( centers[ia], F[ia]);
    }
  }
  
  return C;
}

//###################################################################################################
template< class I>
auto Doer<I>::AB_resistance_matrices( const Vector< Pos, AIndex>& centers, const GMatrix< Sqrt_Mob, AIndex>& L){
   
  Mat3< Length> A;
  Mat3< Len2> B( Zeroi{});
  for( int k: range( 3)){
    
    typedef double Vel;
    Vec3< Vel> vp( Zeroi{});
    vp[k] = Vel(1.0);
    
    auto na = L.size();
    Vector< Vec3< Vel>, AIndex> v( na, vp);
    auto F = solution( L, v);
     
    A[k] = std::accumulate( F.begin(), F.end(), Vec3< Length>(Zeroi{}));
    
    for( auto ia: range( na)){
      auto T = cross( centers[ia], F[ia]);
      for( auto kk: range(3))
        B[kk][k] += T[kk];
    }
  }
  
  return std::make_pair( A, B);
}

//#############################################################
template< class I>
auto Doer<I>::centers_and_radii( const System& system, DIndex id) const{
  
  auto na = I::n_beads( system, id);
  Vector< Pos, AIndex> centers( na);
  Vector< Length, AIndex> radii( na);
  for( auto ia: range( na)){
    centers[ia] = I::bead_center( system, id, ia);
    radii[ia] = I::bead_radius( system, id, ia);
  }
  
  return std::make_pair( move( centers), move( radii));
}

//########################################################################
template< class I>
auto Doer<I>::approximate_center( const System& system,
                                 const Vector< Pos, AIndex>& centers,
                                 const Vector< Length, AIndex>& radii) const -> Pos{
  
  auto na = centers.size();
  
  if (na > AIndex(0)){
    Vec3< Len2> csum( Zeroi{});
    Length rsum( 0.0);
    for( auto ia: range( na)){
      auto r = radii[ia];
      csum += r*centers[ia];
      rsum += r;
    }
    return (1.0/rsum)*csum;
  }
  else{
    return Vec3< Length>( Zeroi{}); 
  }
}


//##################################################################
template< class I>
template< class T, class Idx>
auto Doer<I>::cholesky_of( const GMatrix< T, Idx>& M) -> GMatrix< decltype( sqrt(T())), Idx>{
  
  typedef decltype( sqrt( T())) Lt;
  auto n = M.size();  
  GMatrix< Lt, Idx> L( n);
  for( auto i: range(n)){
    L[i].resize( i + (Idx(1) - Idx(0)));
  }
  
  bool success;    
  Cholesky::decompose< I_Mat_Cholesky< T, Idx> >(M, L, success);
  if (!success){
    error( "Cholesky failed", __LINE__);
  }
  
  return L;
}
}
}


