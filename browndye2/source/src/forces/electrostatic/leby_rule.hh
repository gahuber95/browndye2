#pragma once
#include "../../lib/array.hh"

namespace Browndye{
  size_t rule_size();
  
  // integral over unit sphere.  Multiply sum by 4pi
  // point, weight
  std::pair< Array< double, 3>, double> leby_point( size_t i);
}