#pragma once
#include <cmath>
#include <type_traits>
#include "../global/pi.hh"

/* 
Given positions of four points, computes 
the dihedral angle in phi and the corresponding derivatives.
Points are numbered 0 through 3.
The angle is defined as the rotation of r3 - r2 counterclockwise
about r2 - r1 while looking from r2 towards r1 and keeping r1 - r0 
fixed.


Interface class must have the following types and static functions:

types: Chain, Group_Index, Value

functions:

Pos position( const Chain& chain, const Group_Index& iq, size_t ip) -
get the position of the ip^th point (0,1,2,3). The user-defined
index iq can be used to select the group of four points. Pos must be indexible like
 an array and have consistent units.
 
void set_derivative( Chain& chain, const Group_Index& iq, size_t ip,
          size_t k, Deriv a)
sets the k^th component (0,1,2) of the derivative with respect to the 
ip^th point (0,1,2,3) to a. The user-defined
index iq can be used to select the group of four points.
Type Deriv must be consistent with the other units


*/

//##########################################################
namespace Browndye{

template< class I>
void compute_torsion_angle_and_derivatives( typename I::Chain& chain, typename I::Group_Index ig, typename I::Value &phi){
  
  typedef typename I::Value Value;
  
  auto p1 = I::position( chain, ig, 0);
  typedef std::decay_t< decltype( p1[0])> Length;
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];
  
  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];
  
  auto p3 = I::position( chain, ig, 2);
  auto x3 = p3[0];
  auto y3 = p3[1];
  auto z3 = p3[2];  
  
  auto p4 = I::position( chain, ig, 3);
  auto x4 = p4[0];
  auto y4 = p4[1];
  auto z4 = p4[2];  

  // r12 = x2 - x1
  auto a13 = x2 - x1;
  auto a14 = y2 - y1;
  auto a15 = z2 - z1;
  
  // r23 = x3 - x2
  auto a16 = x3 - x2;
  auto a17 = y3 - y2;
  auto a18 = z3 - z2;

  // r34 = x4 - x3
  auto a19 = x4 - x3;
  auto a20 = y4 - y3;
  auto a21 = z4 - z3;

  // t1 = r12 x r23
  auto a22 = a14*a18 - a15*a17;
  auto a23 = a15*a16 - a13*a18;
  auto a24 = a13*a17 - a14*a16;

  // t2 = r23 x r34
  auto a25 = a17*a21 - a18*a20;
  auto a26 = a18*a19 - a16*a21;
  auto a27 = a16*a20 - a17*a19;

  // |t1|, |t2|
  auto a28 = sqrt( a22*a22 + a23*a23 + a24*a24);
  auto a29 = sqrt( a25*a25 + a26*a26 + a27*a27);

  // u1 = t1/|t1|
  double a30 = a22/a28;
  double a31 = a23/a28;
  double a32 = a24/a28;

  // u2 = t2/|t2|
  double a33 = a25/a29;
  double a34 = a26/a29;
  double a35 = a27/a29;

  // u1 x u2 (figure out sign of angle)
  double c1 =  a31*a35 - a32*a34;
  double c2 =  a32*a33 - a30*a35;
  double c3 =  a30*a34 - a31*a33;
  
  // dot with r23
  double sign = (c1*a16 + c2*a17 + c3*a18)/Length(1.0);
  
  // cos(phi) = u1.u2
  double a36 = a30*a33 + a31*a34 + a32*a35; 

  double a41,a42,a43,a44;
  Length a37, a38, a39, a40;
  bool large_cos = (fabs(a36) > 0.99);
  if (large_cos){
    // large cosine
    large_cos = true;

    // t3 = r23 x u2
    a37 = a17*a35 - a18*a34;
    a38 = a18*a33 - a16*a35;
    a39 = a16*a34 - a17*a33;
    
    // |t3|
    a40 = sqrt( a37*a37 + a38*a38 + a39*a39);

    // u3 = t3/|t3|
    a41 = a37/a40;
    a42 = a38/a40;
    a43 = a39/a40;

    // cos( psi) = u3.u1
    a44 = a41*a30 + a42*a31 + a43*a32;
    
    // phi = asin( cos(psi))
    if (a36 > 0.0)
      phi = -asin( a44);
    else if (sign > 0.0)
      phi = Value(pi) + asin( a44);
    else
      phi = -Value(pi) + asin( a44);
  }
  else {
    if (sign > 0.0)
      phi = acos( a36);
    else
      phi = -acos( a36);    
  }

  // Now do the backwards differentiation
  

  double b45 = 1.0;

  double b30,b31,b32,b33,b34,b35;
  if (large_cos){
    // phi
    double b44;
    if (a36 > 0.0)
      b44 = -b45/sqrt( 1.0 - a44*a44);
    else
      b44 = b45/sqrt( 1.0 - a44*a44);

    // cos(psi)
    auto b41 = b44*a30;
    auto b42 = b44*a31;
    auto b43 = b44*a32;

    b30 = b44*a41;
    b31 = b44*a42;
    b32 = b44*a43;

    // u3
    auto b37 = b41/a40;
    auto b38 = b42/a40;
    auto b39 = b43/a40;
    auto b40 = -(b41*a37 + b42*a38 + b43*a39)/(a40*a40);

    // |t3|
    b37 += b40*a37/a40;
    b38 += b40*a38/a40;
    b39 += b40*a39/a40;

    // t3
    //auto b16 = a34*b39 - a35*b38;
    //auto b17 = a35*b37 - a33*b39;
    //auto b18 = a33*b38 - a34*b37;

    b33 = b38*a18 - b39*a17;
    b34 = b39*a16 - b37*a18;
    b35 = b37*a17 - b38*a16;
  }

  else { // not large cos
    // phi
    double b36;
    if (sign > decltype(sign)( 0.0))
      b36 = -b45/sqrt( 1.0 - a36*a36);
    else 
      b36 = b45/sqrt( 1.0 - a36*a36);

    // cos(phi)
    b30 = b36*a33;
    b33 = b36*a30;
    b31 = b36*a34;
    b34 = b36*a31;
    b32 = b36*a35;
    b35 = b36*a32;
  }
  
  // u2
  auto b25 = b33/a29;
  auto b26 = b34/a29;
  auto b27 = b35/a29;
  auto b29 = -(b33*a25 + b34*a26 + b35*a27)/(a29*a29);

  // u1
  auto b22 = b30/a28;
  auto b23 = b31/a28;
  auto b24 = b32/a28;
  auto b28 = -(b30*a22 + b31*a23 + b32*a24)/(a28*a28);

  // |t1|
  b25 += b29*a25/a29;
  b26 += b29*a26/a29;
  b27 += b29*a27/a29;

  // |t2|
  b22 += b28*a22/a28;
  b23 += b28*a23/a28;
  b24 += b28*a24/a28;

  // t2
  auto b16 = a20*b27 - a21*b26;
  auto b17 = a21*b25 - a19*b27;
  auto b18 = a19*b26 - a20*b25;

  auto b19 = b26*a18 - b27*a17;
  auto b20 = b27*a16 - b25*a18;
  auto b21 = b25*a17 - b26*a16;

  // t1
  auto b13 = a17*b24 - a18*b23; 
  auto b14 = a18*b22 - a16*b24;
  auto b15 = a16*b23 - a17*b22;

  b16 += b23*a15 - b24*a14;
  b17 += b24*a13 - b22*a15;
  b18 += b22*a14 - b23*a13;

  // r34
  I::set_derivative( chain, ig, 3,0,  b19);
  I::set_derivative( chain, ig, 3,1,  b20);
  I::set_derivative( chain, ig, 3,2,  b21);

  // r23 and r34
  I::set_derivative( chain, ig, 2,0,  b16 - b19);
  I::set_derivative( chain, ig, 2,1,  b17 - b20);
  I::set_derivative( chain, ig, 2,2,  b18 - b21);

  // r23 and r12
  I::set_derivative( chain, ig, 1,0,  b13 - b16);
  I::set_derivative( chain, ig, 1,1,  b14 - b17);
  I::set_derivative( chain, ig, 1,2,  b15 - b18);

  // r12
  I::set_derivative( chain, ig, 0,0,  -b13);
  I::set_derivative( chain, ig, 0,1,  -b14);
  I::set_derivative( chain, ig, 0,2,  -b15);
}

//##########################################################
template< class I>
[[maybe_unused]] auto torsion_angle( const typename I::Chain& chain, typename I::Group_Index ig) -> typename I::Value{
  
  typedef typename I::Value Value;  
        
  auto p1 = I::position( chain, ig, 0);
  typedef std::decay_t< decltype( p1[0])> Length;  
  
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];
  
  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];
  
  auto p3 = I::position( chain, ig, 2);
  auto x3 = p3[0];
  auto y3 = p3[1];
  auto z3 = p3[2];  
  
  auto p4 = I::position( chain, ig, 3);
  auto x4 = p4[0];
  auto y4 = p4[1];
  auto z4 = p4[2];  

  // r12 = x2 - x1
  auto a13 = x2 - x1;
  auto a14 = y2 - y1;
  auto a15 = z2 - z1;

  // r23 = x3 - x2
  auto a16 = x3 - x2;
  auto a17 = y3 - y2;
  auto a18 = z3 - z2;

  // r34 = x4 - x3
  auto a19 = x4 - x3;
  auto a20 = y4 - y3;
  auto a21 = z4 - z3;

  // t1 = r12 x r23
  auto a22 = a14*a18 - a15*a17;
  auto a23 = a15*a16 - a13*a18;
  auto a24 = a13*a17 - a14*a16;

  // t2 = r23 x r34
  auto a25 = a17*a21 - a18*a20;
  auto a26 = a18*a19 - a16*a21;
  auto a27 = a16*a20 - a17*a19;

  // |t1|, |t2|
  auto a28 = sqrt( a22*a22 + a23*a23 + a24*a24);
  auto a29 = sqrt( a25*a25 + a26*a26 + a27*a27);

  // u1 = u1/|u1|
  auto a30 = a22/a28;
  auto a31 = a23/a28;
  auto a32 = a24/a28;

  // u2 = u2/|u2|
  auto a33 = a25/a29;
  auto a34 = a26/a29;
  auto a35 = a27/a29;

  // u1 x u2 (figure out sign of angle)
  auto c1 =  a31*a35 - a32*a34;
  auto c2 =  a32*a33 - a30*a35;
  auto c3 =  a30*a34 - a31*a33;
  
  // dot with r23
  double sign = (c1*a16 + c2*a17 + c3*a18)/Length(1.0);
  
  // cos(phi) = u1.u2
  double a36 = a30*a33 + a31*a34 + a32*a35; 

  Value phi;
  if (fabs(a36) > 0.99){
    // large cosine

    // t3 = r23 x u2
    auto a37 = a17*a35 - a18*a34;
    auto a38 = a18*a33 - a16*a35;
    auto a39 = a16*a34 - a17*a33;
    
    // |t3|
    auto a40 = sqrt( a37*a37 + a38*a38 + a39*a39);

    // u3 = t3/|t3|
    auto a41 = a37/a40;
    auto a42 = a38/a40;
    auto a43 = a39/a40;

    // cos( psi) = u3.u1
    auto a44 = a41*a30 + a42*a31 + a43*a32;
    
    // phi = asin( cos(psi))
    if (a36 > 0.0)
      phi = -asin( a44);
    else if (sign > 0.0)
      phi =  Value( pi) + asin( a44);
    else
      phi = -Value( pi) + asin( a44);
  }
  else {
    if (sign > 0.0)
      phi = acos( a36);
    else
      phi = -acos( a36);    
  }
  return phi;
}
}
