#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <random>
#include "../global/error_msg.hh"
#include "../input_output/get_args.hh"
#include "../global/pi.hh"
#include "../forces/other/mm_nonbonded_parameter_info.hh"

namespace {
using namespace Browndye;    
using std::cout;
namespace JP = JAM_XML_Pull_Parser;
    
const Length srad{ 1.4};  
  
//##############################################################    
void output_atoms_with_vols( const std::string& mname,
                             const Vector< Length3>& vols){
  
  size_t ia = 0;
  
  cout << "<top>\n";
  
  auto process = [&]( JP::Node_Ptr rnode){
    
    rnode->complete();
    auto rname = checked_value_from_node< std::string>( rnode, "name");
    auto rnumber = checked_value_from_node< int>( rnode, "number");
    
    cout << "  <residue>\n";
    cout << "    <name> " << rname << " </name>\n";
    cout << "    <number> " << rnumber << " </number>\n";
    
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      auto fval = [&]( const std::string& tag){
        return checked_value_from_node< double>( anode, tag);
      };
      
      auto x = fval( "x");
      auto y = fval( "y");
      auto z = fval( "z");
      auto charge = fval( "charge");
      auto radius = fval( "radius");
      auto aname = checked_value_from_node< std::string>( anode, "name");
      auto anumber = checked_value_from_node< double>( anode, "number");
      auto areao = value_from_node< double>( anode, "sasa");
      auto vol = vols[ia];
      cout << "    <atom>\n";
      cout << "      <name> " << aname << " </name>\n";
      cout << "      <number> " << anumber << " </number>\n";
      cout << "      <x> " << x << " </x>\n";
      cout << "      <y> " << y << " </y>\n";
      cout << "      <z> " << z << " </z>\n";
      cout << "      <charge> " << charge << " </charge>\n";
      cout << "      <radius> " << radius << " </radius>\n";
      if (areao){
        cout << "      <sasa> " << areao.value() << " </sasa>\n";
      }
      cout << "      <volume> " << vol << " </volume>\n";
      cout << "    </atom>\n";
      ++ia;
    }
    cout << "  </residue>\n";

  };
  
  std::ifstream input( mname);
  JP::Parser parser( input, mname);
  auto top = parser.top();
  parser.apply_to_nodes_of_tag( top, "residue", process);
  
  cout << "</top>\n";
}
    
//###############################################
Pos ran_pt( std::mt19937_64& gen, Pos cen, Length radius){
  
  std::uniform_real_distribution< double> uni;
  
  Length r{ large};
  Pos pos( Zeroi{});
  do{
    pos = initialized_array< 3>([&]( size_t k){
      return cen[k] + radius*(1.0 - 2.0*uni( gen));
    });
    r = distance( pos, cen);
  }
  while( r > radius);
  
  return pos;
}

//####################################################################
Vector< Vector< size_t> > atom_nebs_of( const Vector< Atom>& atoms){
  
  auto na = atoms.size();
  Vector< Vector< size_t> > atom_nebs( na);
  
  for( auto i: range(na)){
    auto& atomi = atoms[i];
    auto posi = atomi.pos;
    for( auto j: range( i)){
      auto& atomj = atoms[j];
      auto posj = atomj.pos;
      auto r = distance( posi, posj);
      if (r < atomi.radius + atomj.radius){
        atom_nebs[i].push_back(j);
        atom_nebs[j].push_back( i);
      }
    }
  }
  return atom_nebs;
};


//##################################################################
Vector< Atom> atoms_of_file( const std::string& pqr_file){
  
  std::ifstream input( pqr_file);
  JP::Parser parser( input, pqr_file);
  auto top = parser.top();
  
  Vector< Atom> atoms;
  
  auto process = [&]( JP::Node_Ptr rnode){
    
    rnode->complete();
    
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      
      auto fval = [&]( const std::string& tag){
        return checked_value_from_node< Length>( anode, tag);
      };
      
      auto x = fval( "x");
      auto y = fval( "y");
      auto z = fval( "z");
      auto radius = fval( "radius");
      
      auto& atom = atoms.emplace_back();
      atom.pos = Pos{ x, y, z};
      atom.radius = radius + srad;
    }
  };
  
  parser.apply_to_nodes_of_tag( top, "residue", process);
  return atoms;
}

//########################################################################
Vector< size_t> counts_of( const Vector< Atom>& atoms, const Vector< Vector< size_t> >& atom_nebs, size_t nsp){
  
   std::mt19937_64 gen;
   
   auto na = atoms.size();
   Vector< size_t> counts( na, 0);
   for( auto ia: range(na)){
     auto& atom = atoms[ia];
     auto cen = atom.pos;
     for( auto isp: range( nsp)){
       (void)isp;
       auto pt = ran_pt(  gen, cen, atom.radius);
       auto r0 = distance( pt, cen);
       bool contained = true;
       for( auto ineb: atom_nebs[ia]){
         auto pnb = atoms[ineb].pos;
         if (distance( pt, pnb) < r0){
           contained = false;
           break;
         }
       }
       if (contained){
         ++(counts[ia]);
       }
     }
   }
   return counts;
 }

//###########################################################################
Vector< Length3> vols_from_atoms( const std::string& pqr_file){
    
  auto atoms = atoms_of_file( pqr_file); 
  auto atom_nebs = atom_nebs_of( atoms);
  
  const size_t nsp = 10000;
  auto counts = counts_of( atoms, atom_nebs, nsp);
  
  auto na = atoms.size();
  return initialized_vector( na, [&](size_t ia){
    auto knt = counts[ia];
    auto frac = (double)knt/nsp;
    auto a = atoms[ia].radius;
    auto vol = (pi4/3)*a*a*a;
    return frac*vol;
  });
 
}

//##################################################
void main0( int argc, char *argv[]){

  if (argc != 2){
    error( "wrong number of args");
  }
  
  std::string help_msg = "takes xml file (output of pqr2xml) as only argument and outputs"
  " atom volumes.  Adds solvent radius of 1.4 A";
  print_help( argc, argv, help_msg);
  std::string pqr_name = argv[1];
    
  auto vols = vols_from_atoms( pqr_name);
  output_atoms_with_vols( pqr_name, vols);
}

}

//################################
int main( int argc, char *argv[]){
  main0( argc, argv);
}
