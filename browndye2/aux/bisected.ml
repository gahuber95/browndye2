let solution g xl xr tol = 
  let rec loop xl fl xr fr =
    let xh = 0.5*.( xr +. xl) in
    if xr -. xl < tol then
      xh
    else
      let fh = g xh in
      if fh*.fl > 0.0 then
	loop xh fh xr fr
      else
	loop xl fl xh fh
  in
  loop xl (g xl) xr (g xr)
       
let bracket g x = 
  let g0 = g x in
  let rec loop x = 
    if (g x)*.g0 < 0.0 then
      x
    else
      loop (2.0*.x)
  in
  loop (2.0*.x)
