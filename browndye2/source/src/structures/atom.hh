#pragma once

#include "../lib/units.hh"
#include "../lib/array.hh"
#include "../lib/vector.hh"
#include "../global/limits.hh"
#include "../global/indices.hh"
#include "../global/pos.hh"

namespace Browndye{

struct Atom{

  Length radius{ NaN}, interaction_radius{ NaN};
  Pos pos;
  
  Atom_Index number{ maxi}; // must be sequential number in core, not necessarily from PQR file
  Atom_Type_Index type{ maxi};
  Charge charge{ NaN};
  Energy mass_weight{ NaN}; // later, put this and sasa into separate arrays
  
  static constexpr size_t noth = 3;
  Array< double, noth> other;
  
  static constexpr size_t SASA = 0;
  static constexpr size_t Eta_Max = 1;
  static constexpr size_t DG = 2;
  
  [[nodiscard]] Length2 sasa() const{
    return Length2{ other[SASA]};
  }
  
  void set_sasa( Length2 input){
    other[ SASA] = input/Length2( 1.0);
  }

  [[nodiscard]] double eta_max() const{
    return other[ Eta_Max];
  }
   
  void set_eta_max( double e){
    other[ Eta_Max] = e;
  }

  void set_surface_energy( Energy v){
    other[ DG] = v/Energy{ 1.0};
  }
  
  [[nodiscard]] Energy surface_energy() const{
    return Energy{1.0}*other[DG];
  }
  
  Volume volume{ NaN}; // later put into other
  
  #ifdef DEBUG
  Atom(){
    for( auto k: range( noth)){
      other[k] = NaN;
    }
  }
  #endif
};

}

