#include "../xml/jam_xml_pull_parser.hh"
#include "../transforms/transform.hh"
#include "../xml/node_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

Transform home_transform( JP::Node_Ptr& conode){

  auto trans = checked_array_from_node< Length, 3>( conode, "translation");
  auto rotvec = checked_array_from_node< double, 9>( conode, "rotation");
  Mat3< double> rot;
  for( auto i: range(3))
    for( auto j: range(3))
      rot[i][j] = rotvec[ 3*i + j];
  
  return { rot, trans};
  
  /*
  Pos sum( Zeroi{});
  for( auto& atom: atoms)
    sum += atom.pos;
  auto centroid = sum/((double)(atoms.size()/Atom_Index(1)));
  
  Transform tform1( id_matrix<3>(), -centroid), tform2( rot, Pos(Zeroi{})), tform3( id_matrix<3>(), centroid + trans);        
  return tform3*tform2*tform1;
  */
}
}