#include <algorithm>
#include <set>
#include "cd_nonbonded_parameter_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

using std::pair;
using std::make_pair;
using std::map;
using std::max;
using std::min;
using std::move;
using std::set;

template< class Key, class T>
bool mcontains( const map< Key, T>&, const Key&);

//##############################################################################
// constructor
CD_Nonbonded_Parameter_Info::CD_Nonbonded_Parameter_Info( JP::Parser& parser){
   
  Array< std::string, 3> tags{ "units", "types", "pairs"}; 
  
  auto lenergy = [&]( Node_Ptr& cnode){
    get_energy_units( parser, cnode); 
  };
  
  bool got_types = false;
  
  auto ltypes = [&]( Node_Ptr& cnode){
    get_atom_and_residue_info( parser, cnode);
    got_types = true;
  };
  
  
  auto lpairs = [&]( Node_Ptr& cnode){
    get_pairs_info( parser, cnode);
    
    if (!got_types){
      cnode->perror( "no types to match pairs");
    }
  };
  
  auto top = parser.top();
  apply_to_each_child_with_tag( top, tags, lenergy, ltypes, lpairs);
}

//#################################################################
void CD_Nonbonded_Parameter_Info::get_pairs_info( Parser& parser,
                                                const Node_Ptr& node){
    
  Array< std::string, 2> tags{ "distance", "potentials"};
  
  auto ldistance = [&]( Node_Ptr& dnode){
    dnode->complete(); 
    distances = array_from_node< Length, 2>( dnode);
  };
  
  auto ldata = [&]( Node_Ptr& cnode){
    auto spline_info = spline_info_from_parser( parser, cnode);
    Vs = std::move( spline_info.fs);
    put_spline_info_into_array( spline_info);
    complete_dict( rb_names, spline_info);
  };

  auto res = apply_to_each_child_with_tag( node, tags, ldistance, ldata);
  if (!res){
    node->perror( "missing tag");
  }

}

//##############################################################################################
void CD_Nonbonded_Parameter_Info::get_atom_and_residue_info([[maybe_unused]] Parser& parser, const Node_Ptr& tnode){
  
  tnode->complete();
  auto index_to_atom_name = atom_index_info( tnode);
  auto index_to_res_name = residue_index_info( tnode);   
  rb_names = RB_Name_Info{ index_to_res_name, index_to_atom_name};
}

//##########################################################################################
auto CD_Nonbonded_Parameter_Info::atom_index_info( const Node_Ptr& tnode) -> Index_Atom_Map{
  
  auto atnode = tnode->child("atoms");
  auto anodes = atnode->children_of_tag( "type");
  
  std::map< Basic_Atom_Type_Index, std::string> index_to_atom_name;
  for( auto& anode: anodes){
    auto name = checked_value_from_node< std::string>( anode, "name");
    auto index = checked_value_from_node< Basic_Atom_Type_Index>( anode, "index");
    index_to_atom_name.insert( make_pair( index, name));
  }
  
  return index_to_atom_name;
}

//###############################################################################################
auto CD_Nonbonded_Parameter_Info::residue_index_info( const Node_Ptr& tnode) -> Index_Residue_Map{
  
  auto resnode = tnode->child( "residues"); 
  auto rnodes = resnode->children_of_tag( "type");
  
  std::map< Residue_Type_Index, std::string> index_to_res_name;
  for( auto& rnode: rnodes){
    auto name = checked_value_from_node< std::string>( rnode, "name");
    auto index = checked_value_from_node< Residue_Type_Index>( rnode, "index");
    index_to_res_name.insert( make_pair( index, name));
  }
  
  return index_to_res_name;
}

//##############################################################################################################
void CD_Nonbonded_Parameter_Info::complete_dict( const RB_Name_Info& lrb_names, const Spline_Info& spline_info){
  
  auto& rb_to_a = spline_info.rb_to_a;
  
  auto& index_to_res_name = lrb_names.reses;
  auto& index_to_atom_name = lrb_names.atoms;
  
  for( auto& item: rb_to_a){
    auto ia = item.second;
    auto ar = item.first;
    auto ir = ar.first;
    auto ib = ar.second;
    auto riter = index_to_res_name.find( ir);
    if (riter != index_to_res_name.end()){
      auto& rname = riter->second;
      auto aiter = index_to_atom_name.find( ib);
      if (aiter != index_to_atom_name.end()){
        auto aname = aiter->second;
        auto key = make_pair( rname, aname);
        dict.insert( make_pair( key, ia));
      }
      else
        error( "CD_Nonbonded_Parameter_Info: atom name not found from index ", ib);        
    }
    else
      error( "CD_Nonbonded_Parameter_Info: residue name not found from index ", ir);
  }
}


//############################################################
void CD_Nonbonded_Parameter_Info::
  put_spline_info_into_array( const Spline_Info& spline_info){
     
  auto na = max_atom_type_index( spline_info);
  aa_to_spline.resize( na);
  
  auto& aa_to_spline_map = spline_info.aa_to_spline;
  auto da1 = Atom_Type_Index(1) - Atom_Type_Index(0);
  for( auto ia: range( na)){
    Atom_Type_Index iap1 = ia + da1;
    auto& a_to_spline = aa_to_spline[ia];
    a_to_spline.resize( iap1);
    for( auto& is: a_to_spline)
      is = Spline_Index( maxi);
    
    for( auto ja: range( iap1)){
      auto key = make_array( ja,ia);
      auto itr = aa_to_spline_map.find( key);
      if (itr != aa_to_spline_map.end()){
        auto is = itr->second;
        a_to_spline[ja] = is;
      }
    }
  }
}

//######################################################
Atom_Type_Index
CD_Nonbonded_Parameter_Info::
  max_atom_type_index( const Spline_Info& spline_info){
    
  auto& aa_to_spline_map = spline_info.aa_to_spline;
    
  Atom_Type_Index na{0};
  for( auto& entry: aa_to_spline_map){
    auto aa = entry.first;
    auto amax = max( aa[0],aa[1]);
    na = max( na, amax);
  }
  
  return na;
}

//###################################################################
auto CD_Nonbonded_Parameter_Info::types_from_node( const JP::Node_Ptr& node) -> Types_Info{
  Types_Info tinfo;
  tinfo.rtypes = checked_array_from_node< Residue_Type_Index, 2>( node, "residues");
  tinfo.btypes = checked_array_from_node< Basic_Atom_Type_Index, 2>( node, "atoms");
  return tinfo;
}

//######################################################################
void CD_Nonbonded_Parameter_Info::put_types_into_rb_to_a( const Types_Info& tinfo,
                                                           RB_To_A& rb_to_a){
  for( auto i: range(2)){
    auto ri = tinfo.rtypes[i];
    auto bi = tinfo.btypes[i];
    auto key = make_pair( ri, bi);
    Atom_Type_Index nat( rb_to_a.size());
    rb_to_a.insert( make_pair( key, nat));
  }
}

//########################################################################
void CD_Nonbonded_Parameter_Info::add_spline_types( const RB_To_A& rb_to_a,
                                                    const Types_Info& tinfo,
                                                    AA_To_Spline& aa_to_spline
                                                  ){
  auto& rtypes = tinfo.rtypes;
  auto& btypes = tinfo.btypes;
  
  auto at0 = rb_to_a.find( make_pair( rtypes[0], btypes[0]))->second;
  auto at1 = rb_to_a.find( make_pair( rtypes[1], btypes[1]))->second;
  auto at0o = min( at0, at1);
  auto at1o = max( at0, at1);
  Spline_Index isp( aa_to_spline.size());
  aa_to_spline.insert( make_pair( make_array( at0o, at1o), isp));
}

//##############################################################################
auto CD_Nonbonded_Parameter_Info::spline_info_from_parser( JP::Parser& parser,
                                                           const Node_Ptr& node) const -> Spline_Info{
  
  Spline_Info spline_info;
  auto& rb_to_a = spline_info.rb_to_a;
  auto& laa_to_spline = spline_info.aa_to_spline;
  
  auto get_poten = [&]( const JP::Node_Ptr& node){
    
    node->complete();
    auto orders = checked_array_from_node< int, 2>( node, "orders");
        
    if (orders[0] == 0 && orders[1] == 0){
      auto tinfo = types_from_node( node);
      put_types_into_rb_to_a( tinfo, rb_to_a);
      add_spline_types( rb_to_a, tinfo, laa_to_spline);
      
      auto rdata = checked_vector_from_node< Energy>( node, "data");
      auto data = converted_energy( rdata);
      spline_info.fs.emplace_back( distances[0], distances[1], data);
    }
  };
  
  parser.apply_to_nodes_of_tag( node, "potential", get_poten);
  
  return with_wildcards_expanded( std::move( spline_info));
}

//#######################################################################################################
auto CD_Nonbonded_Parameter_Info::residues( const RB_To_A& rb_to_a) -> set< Residue_Type_Index>{
  set< Residue_Type_Index> reses;
  const Residue_Type_Index wild(0); 
  for( auto item: rb_to_a){
    auto key = item.first;
    auto ir = key.first;
    if (ir != wild){
      reses.insert( ir);
    }
  }
  return reses;
}

//#####################################################
template< class Key, class T>
bool mcontains( const map< Key, T>& mp, const Key& key){
  auto itr = mp.find( key);
  return itr != mp.end();
}

//############################################################
auto CD_Nonbonded_Parameter_Info::
  with_wildcard_info( Spline_Info&& old_info) -> Spline_Info{
  
  auto info = std::move( old_info);
  auto& rb_to_a = info.rb_to_a;
  auto& wild_to_as = info.wild_to_as;
  
  auto reses = residues( rb_to_a);
  RB_To_A new_rb_to_a;
   
  const Residue_Type_Index wild(0); 
  for( auto& entry: rb_to_a){
    auto [rb,ai] = entry;
    auto [ri,bi] = rb;
    if (ri == wild){
      for( auto rj: reses){
        auto key = make_pair( rj, bi);
        auto itr = rb_to_a.find( key);
        if (itr == rb_to_a.end()){
          Atom_Type_Index na{ rb_to_a.size() + new_rb_to_a.size()};
          new_rb_to_a.insert( make_pair( key, na));
          wild_to_as[ ai].insert( na);
        }
        else{
          auto aj = itr->second;
          wild_to_as[ ai].insert( aj);
        }
      }
    }
  }
  rb_to_a.insert( new_rb_to_a.begin(), new_rb_to_a.end());
  
  return info;
}

//#######################################################################################################
auto CD_Nonbonded_Parameter_Info::with_wildcards_expanded( Spline_Info&& old_info) -> Spline_Info{
  
  auto info = with_wildcard_info( std::move(old_info));
  auto& wild_to_as = info.wild_to_as;
  auto& aa_to_spline = info.aa_to_spline;
  
  AA_To_Spline new_aa_to_spline;
  for( auto& entry: aa_to_spline){
    auto [aa,si] = entry;
    auto a0 = aa[0];
    auto a1 = aa[1];
    
    auto wild0 = mcontains( wild_to_as, a0); 
    auto wild1 = mcontains( wild_to_as, a1);
    
    auto put_in_wilds = [&, lsi=si]( auto aw, auto aj){
      for( auto ai: wild_to_as.at( aw)){
        auto alo = min( aj,ai);
        auto ahi = max( aj,ai);
        auto key = make_array( alo, ahi);
        new_aa_to_spline.insert( make_pair( key, lsi));
      }
    };
    
    if (wild0 && !wild1){
      put_in_wilds( a0,a1);
    }
    else if ( wild1 && !wild0){
      put_in_wilds( a1,a0);
    }
    
    else if (wild0 && wild1){
      for( auto ai0: wild_to_as.at( a0)){
        for( auto ai1: wild_to_as.at( a1)){
          auto alo = min( ai0, ai1);
          auto ahi = max( ai0, ai1);
          auto key = make_array( alo, ahi);
          new_aa_to_spline.insert( make_pair( key, si));
        }
      }
    }
  }
    
  aa_to_spline.insert( new_aa_to_spline.begin(), new_aa_to_spline.end());
  return info;
}

//######################################################################
void CD_Nonbonded_Parameter_Info::get_energy_units([[maybe_unused]] Parser& parser,
                                                   const Node_Ptr& node){
  
  node->complete();
  set_energy_units( node);
}


//######################################################################################################
void CD_Nonbonded_Parameter_Info::establish_interaction_radii( Vector< Atom, Atom_Index>& atoms) const{
  
  auto max_r = distances[1];
  for( auto& atom: atoms){
    atom.interaction_radius = max_r;
  }
}

//##############################################################
Vec3< Force> CD_Nonbonded_Parameter_Info::
  force( Vec3< Length> r01, Length r, const Atom& atom0, const Atom& atom1) const{
    
  if (r >= distances[1]){
    const Force F0( 0.0);  
    return Vec3< Force>{ F0, F0, F0};
  }
  else {
    auto it0 = atom0.type;
    auto it1 = atom1.type;
    Atom_Type_Index it0o = max( it0, it1);
    Atom_Type_Index it1o = min( it0, it1);
    auto is = aa_to_spline[it0o][it1o];
    auto& spl = Vs[is];
    
    auto F = -spl.first_deriv( r);
    return (F/r)*r01;
  }
}

//##############################################################
Energy CD_Nonbonded_Parameter_Info::potential( Length r,
                          const Atom& atom0, const Atom& atom1) const{
  
  if (r >= distances[1]){  
     return Energy( 0.0);
  }
  else {
    auto it0 = atom0.type;
    auto it1 = atom1.type;
    Atom_Type_Index it0o = max( it0, it1);
    Atom_Type_Index it1o = min( it0, it1);
    auto is = aa_to_spline[it0o][it1o];
    auto& spl = Vs[is];
    return spl.value( r);
  }
  
}


}

/*
int main(){
  std::string file( "bull.xml");
  std::ifstream input( file);
  JP::Parser parser( input, file);
  
  CD_Nonbonded_Parameter_Info info( parser);
  std::out << info.Vs.size() << "\n";
}
*/
