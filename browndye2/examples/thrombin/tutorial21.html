<html>
<body>
<h1>BrownDye Tutorial</h1>

<p>
This tutorial will walk you through two different types of BD simulations on 
the Thrombin-Thrombomodulin system, which is an important component of the
blood-clotting cascade.  Thrombin consists of 295 amino acids, and
thrombomodulin consists of 117 amino acids.  
(My thanks to Adam Van Wynsberghe
for the necessary data on these two molecules.)
Here we treat each molecule as a rigid body, or "core". Also, the
actual reaction rate is much smaller that what it calculated in this
tutorial. However, we want something that runs on one processor
for just a few minutes in order to give you a taste of the software,
so I have increased the reaction distance (described below) from 5 Angstroms to 15 Angstroms so you can actually obtain a "rate" from only 1000 trajectories.

<p>
BrownDye can be easily installed on Linux or Mac OS X.  
If you have Windows, you can
install <a href="http://cygwin.com/install.html"> Cygwin </a>,
which provides a free Linux emulator, and then to install BrownDye using the
Cygwin terminal.
Go <a href="#windows"> here </a> for more details.
Other software required will be <a href="http://www.poissonboltzmann.org/apbs/">
    APBS </a>, <a href="http://caml.inria.fr/pub/docs/manual-ocaml/index.html">Ocaml</a>, and <a href="http://www.ks.uiuc.edu/Research/vmd/"> VMD </a> for the visualization.
<p>
Also, be sure to check out the website for more up-to-date installation instructions.

<h2> Running on a Linux Computer </h2>
Other necessary software is the Ocaml compiler, which is free and can be obtained from <a href="http://caml.inria.fr/"> here </a>. If you are using Ubuntu or a similar Linux based on Debian, you can just type

<pre>
sudo apt install ocaml 
</pre>

You will also need APBS; this can also quickly be installed on Ubuntu by typing

<pre>
sudo apt install apbs 
</pre>

The following steps will also work on the Mac terminal and Windows Cygwin
terminal.
You can install BrownDye on your machine by going to the 
<a href="http://browndye.ucsd.edu"> website </a>, downloading 
the file <code>browndye.tar.gz</code>, moving the file to a directory of your choice, 
and typing

<pre>
tar xvfz browndye.tar.gz
cd browndye
make all
</pre>

All of the executables are now in the directory <code>browndye/bin</code>; you need
to add this directory to your path.  If you use a bash shell, this can
be done by typing

<pre>
PATH=$PATH:fullpath/browndye/bin
</pre>

where <code>fullpath</code> is the full path name of the directory containing the
browndye distribution.
If you now want to run the thrombin-thrombomodulin tutorial, go to
<code>browndye/examples/thrombin</code>.

<p>
The atomic coordinates for thrombin and thrombomodulin are in
files <code>t.pqr</code> and <code>m.pqr</code>.  Because BrownDye works primarily with
XML files, you must convert these two files to an equivalent XML format:

<pre>
pqr2xml < t.pqr > t_atoms.xml
pqr2xml < m.pqr > m_atoms.xml
</pre>

Next, you must generate the electrostatic grids, in dx format, using
APBS:

<pre>
apbs t.in
apbs m.in
</pre>
where the input files <code>t.in</code> and <code>m.in</code> are provided.  The grids are
output to <code>t.dx</code> and <code>m.dx</code>.  
Be sure to take note of the Debye length
in the APBS output if you don't feel like calculating it by hand; it
will be needed later.  (Some older versions of APBS will name the output files 
<code>t-PE0.dx</code> and <code> m-PE0.dx</code>; if that happens, be sure to 
rename the files.)

<p>  In addition to atomic coordinates and grids, the third key input
is the set of reaction criteria.  This can be generated from the 
two coordinate files <code>t_atoms.xml</code> and <code>m_atoms.xml</code>, and a file,
<code>protein_protein_contacts.xml</code>, which describes which pairs of atom
types can define a contact 
(It is actually stored as <code>protein_protein_contacts.xml.bak</code>; you
need to copy from this file.)

The program <code>make_rxn_pairs</code> takes these
three files and a search distance to generate a file of reaction
pairs.  This assumes that the coordinates of the two molecules
are consistent with the bound state.
<pre>
make_rxn_pairs -mol0 t_atoms.xml -mol1 m_atoms.xml -ctypes protein_protein_contacts.xml -dist 15 > reaction_pairs.xml
</pre>
 The resulting file still is not suitable for input into the simulation
programs, however.  I&#39;ve made the program general enough to 
have more than one reaction in a simulation, so one could envision having
several such reaction pair files that would need to combined into a final
reaction description file.  For now, you can use the program <code>make_rxn_file</code>,
which generates an input file for the case of one reaction:
<pre>
  make_rxn_file -pairs reaction_pairs.xml -state_from before -state_to after -rxn association -mol0 thrombin thrombin -mol1 tmodulin tmodulin -distance 15.0 -nneeded 3 > reactions.xml
</pre>
This generates a reaction description file which tells the simulation
programs that if any 3 of the atom pairs approach within 15 Angstroms,
a reaction occurs.

<p> A note: if you don&#39;t feel like typing the above commands in, especially
if you make changes and need to do it repeatedly, I have included a
Makefile in the example directory. Typing
<pre>
make all
</pre>
should run the above commands.

<p>
The remaining pieces of information are contained in the file <code>input.xml</code>.
(This must be copied from the file <code>input.xml.bak</code>.)
It contains, among other things, information on the solvent,
information on each molecule, and parameters governing the simulation
itself.
For the sake of efficiency, the larger molecule
should be contained in the first group if there is a large size difference.  
Also, each molecule is assigned a prefix from the name of core; these
are used in naming the intermediate files that are generated in the
next step:
<pre>
bd_top input.xml
</pre>
The bd_top program is written in Ocaml using a Unix "make"-like utility
that I wrote to help orchestrate the creation of the files.
Like "make", if an intermediate file is changed or replaced, 
running bd_top (which is analogous to a Makefile) will run only 
those commands necessary to re-generate files that depend on the
updated file.  Unlike "make", this utility, which I call the
"Orchestrator", can also read information from xml files and 
have chains of dependent calculations (eventually I want to 
write a version in Python so it will look more familiar to most people).
So, when the command is executed, the following files are generated
for thrombin:

<ul>
<li><b>thrombin_charges.xml</b> - a set of effective charges


</li><li><b>thrombin_hydro_radius.xml</b> - hydrodynamic parameters  

</li><li><b>thrombin_charges_squared.xml</b> - effective charges squared; used for the desolvation forces 

</li><li><b>thrombin_squared_cheby.xml</b> - Chebyshev interpolation data structure for
effective charges squared; used for desolvation forces

</li><li><b>thrombin_surface.xml</b> - a list of atoms on the surface found by
rolling a probe sphere. 

</li><li><b>thrombin_surface_atoms.xml</b> - actual list of surface atoms to be used
for collision detection 

</li><li><b>thrombin_mpole.xml</b> - multipole expansion of effective charges

</li> <li> <b> thrombin_inside.xml </b> - grid of points denoting inside (1) or outside (0) 

</li><li><b> thrombin_born.dx </b> - grid of values used to estimate the desolvation energy; uses
    a Born model.

</li>

</ul>

The corresponding files with prefix "tmodulin" are also generated for
thrombomodulin.  In addition, the following file is generated:
<ul>
</li><li><b>tmodulin_cheby.xml</b> - Chebyshev interpolation data structure for
effective charges; used for Coulombic forces
</ul>

<p>
The following files are generated for both molecules:

<ul>

</li><li><b>thrombin_tmodulin_solvent.xml</b> - solvent information 
</li><li><b>thrombin_tmodulin_simulation.xml</b> - contains additional information generated by the auxilliary programs; is read directly into the simulation programs. 

</ul> 
The nice thing about these intermediate files is that any of them can
be replaced or changed, and then <code>bd_top</code> run again to update everything.
For example, right now I&#39;m using a simple test-charge approximation by
default, but one could easily generate effective charges using another
program such as SDA, convert the output into the appropriate XML format,
replace <code>thrombin_charges.xml</code> and <code>tmodulin_charges.xml</code>, and run <code>bd_top</code> again.

<p> Another useability note:  if you want to clean things up, you
can delete all *.dx and *.xml files; the two xml files that you
need to get started are also available as <code>input.xml.bak</code> and
<code>protein_protein_contacts.xml.bak</code>.

<p>
At this point, you can choose to do a simulation of one trajectory at a time,
or you can do a weighted-ensemble simulation.  In general, the weighted-ensemble
method is not as efficient at the single-trajectory method
unless the probability of a reaction event is
very low.

<h3>One trajectory at a time</h3>
<p>
To perform the single-trajectory simulation, the following is executed:
<pre>
nam_simulation thrombin_tmodulin_simulation.xml
</pre>
The results end up in <code>results.xml</code>, as designated in <code>input.xml</code>.
As the simulation proceeds, you can look at <code>results.xml</code> to 
see the simulation progress.  (This is called <code>nam_simulation</code> after
Northrup, Allison, and McCammon, who came up with the first algorithm
of this type.  This code uses a fancy variation on the orginal 
algorithm.).

<p>
At any point, you can use <code>compute_rate_constant</code> to 
analyze <code>results.xml</code> and obtain an estimate of and 95% confidence bounds
on
the reaction rate constant in units of M/s:
<pre>
compute_rate_constant < results.xml 
</pre>
This will put the rate constant results to standard output.

<h3> Visualization </h3>
<p> 
To visualize a trajectory leading to a reaction, you must include
the line
<pre>
&lttrajectory_file&gt trajectory &lt/trajectory_file&gt
</pre>
in the input file, run <code>bd_top</code> and <code> nam_simulation </code>
again.  The value <code>trajectory</code> gives its names to the resulting trajectory
files, and can be what you want.
Because the files can get quite large, you might want
to also include a line such as
<pre>
&lt n_steps_per_output &gt 10 &lt /n_steps_per_output &gt
</pre>
which causes only every 10th configuration to be output.
At the end, the files <code> trajectory0.xml </code> and
<code> trajectory0.index.xml</code>
will be generated. The first file contains the actual trajectories in a 
compressed format, and the second file functions as an index to the
first file.
If you are running with more than one thread, more files
like this will also be generated, with names <code> trajectory1.xm1 </code>,
<code> trajectory1.index.xml </code>, etc., with one pair of files per thread.

After the initial trajectory files are generated, they must be processed further.  For example, if you wish to view a trajectory that results in a reaction, you
 must find such a trajectory by running 
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -srxn association
</pre>
which prints out the numbers of reactive trajectories.  Then, pick one of
the numbers, say 8, and run
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -n 8 > trajectory.xml
</pre>
This uncompresses the contents of <code>trajectory0.xml</code> and outputs 
the desired trajectory into <code>trajectory.xml</code>.
At this point, you can cut down even further on the number of configurations by
adding to the above to get
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -n 8 -nstride 10 > trajectory.xml
</pre>
which outputs only every tenth configuration in <code>trajectory0.xml</code>.

The final step is to generate an vtf-format file for visualization by
VMD, which is obtained by running
<pre>
vtf_trajectory -traj trajectory.xml > trajectory.vtf
</pre>
This file can be very large, since all of the atom positions for every configuration are output to <code> traj.vtf</code>, so it can be important to 
reduce the number of trajectories upstream.  This file then can be passed
to VMD for visualization.  If you are worried about the size of the file,
you can run 
<pre>
vtf_trajectory -mol0 t_atoms.xml -mol1 m_atoms.xml -trajf trajectory.xml -trial
</pre>
which will output nothing except an estimate of the file size. If it looks like
it might be too large, you can go back and increase the stride argument to
<code> process_trajectories </code>.

At last, you can start up VMD, load in the vtf file, and watch the animation.

<h3> Weighted Ensemble Method </h3>
<p>
To perform the weighted-ensemble method, you must first generate the
bins for the system copies:
<pre>
build_bins thrombin_tmodulin_simulation.xml
</pre>
The  number of system copies used in the bin-building process are
given in <code>input.xml</code> in the <code>n_copies</code> tag.
As it runs, reaction coordinate numbers will go scrolling past; they should keep
getting smaller and eventually stop.  If that does not happen, i.e.,
the numbers keep on going, you might need to increase the number of system 
copies, or it might be that your reaction criterion is unattainable.
Assuming it converges, the bin information is placed in <code>t_m_bins.xml</code>.
The actual weighted-ensemble simulation is then run:
<pre>
we_simulation thrombin_tmodulin_simulation.xml
</pre>
As before, the results are output to <code>results.xml</code>.  In each row of
output numbers, the right-most number is the flux of system copies that
escaped without reaction, while the other ones are reactive fluxes.
So, even for a rare reaction event, you should at least see small numbers for
the reactive fluxes after the system has reacted steady-state.
This can be visually examined at any point, and can also be analyzed as
above, but with a different program:
<pre>
compute_rate_constant_we < results.xml 
</pre>
Because the streams of numbers are autocorrelated, a more sophisticated
approach for computing confidence intervals is used, and if there are
not enough data points, the program <code>compute_rate_constant_we</code> will
simply refuse to provide an answer.

<h3> Ideas for fun </h3>

<p> You can change the random number generator seed, under the <code>seed</code> tag.
Good to do if you&#39;re bored but don&#39;t have the energy to do anything else.

<p>
One parameter to play with is the reaction criterion distance, which is the
<code>-distance</code> input to the program <code>make_rxn_file</code>.  The number given in 
the tutorial and the Makefile is 15.0, but you can change that by re-running
<code>make_rxn_file</code> or by changing it in the Makefile.

<p>
You can also change the ionic strength in files <code>t.in</code> and <code>m.in</code> and
generate new APBS grids.  Note: <i> you must take note of the new Debye length</i>
and put that value in the file <code>input.xml</code>.  So far, it is not possible to
automatically get the Debye length from the output DX file of APBS.

<p>
If your machine has several processors, you
can change the value under the tag <code>n_threads</code> in file <code>input.xml</code> and
see it run under several processors.  So far, it runs only on shared-memory
machines using <code>pthreads</code>.
Even on a single-processor
machine, you can still run several threads, but it does not make the
programs go any faster.

<p>
A final useability note:  Most of the programs will output a description of 
themselves and their options if you type in
<pre>
program -help
</pre>

<h2><a name="windows"> Notes on Running BrownDye on Windows and Macintosh</a> </h2>
The Cygwin package is easy to install; you go to the website and download
and run the <code>setup.exe</code> file. The only part that requires some
effort is finding the necessary packages to install.  If you do nothing,
Cygwin installs a minimal subset, but to run Browndye you need gcc,
make, autoconf, and ocaml.  You could install all optional packages, but
it makes for a huge and lengthy download.  Once Cygwin is installed,
you can pop open a terminal window, giving you access to a directory
tree just like any other Unix system.  If you are missing packages,
you can run <code>setup.exe</code> again, which will again present you 
the choice of packages.

<p>
In order to obtain gcc for OS X on the Mac, you need to go to Apple&#39;s website
and register as a developer. If you are doing your work in a university, this
is free.  Then, you install the XCode Developer tools, which include gcc.
Ocaml is available for the Mac from the Ocaml website. 

<h2> References </h2>
<p>
  Huber, GA and McCammon, JA. <a href="https://doi.org/10.1016/j.trechm.2019.07.008"> Brownian Dynamics Simulations of Biological Molecules </a>
  Trends in Chemistry 1, 727-738 (2019)

<p>
Huber, GA and McCammon, JA. <a href="http://dx.doi.org/10.1016/j.cpc.2010.07.022"> Browndye: A Software Package for Brownian
Dynamics.</a>  Computer Physics
Communications 181, 1896-1905 (2010)

<p>
Ermak, DL and McCammon, JA. Brownian Dynamics with Hydrodynamic Interactions,
J. Chem. Phys. 69, 1352-1360 (1978)

<p>
Northrup SH, Allison SA and McCammon JA.
Brownian Dynamics Simulation of Diffusion-Influenced Bimolecular Reactions
J. Chem. Phys. 80, 1517-1526

<p>
Luty BA, McCammon JA and Zhou HX. Diffusive Reaction-Rates From Brownian Dynamics Simulations - Replacing the Outer Cutoff Surface by an Analytical Treatment,
J. Chem. Phys. 97, 5682-5686 (1992)

<p>
Huber GA and Kim S. Weighted-ensemble Brownian dynamics simulations for protein association reactions, Biophys. J 70, 97-110 (1996)

<p>
Gabdoulline RR and Wade RC. Effective charges for macromolecules in solvent,
J. Phys. Chem 100, 3868-3878 (1996)


</body>
</html>

