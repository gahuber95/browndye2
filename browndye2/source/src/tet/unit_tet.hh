#pragma once

#include "v4.hh"


namespace Browndye{
  // length of side is 2
  inline
  V4< Vec3< double> > unit_tetrahedron(){
    const double z0{ 1.0/sqrt(2.0)};
    return V4< Vec3< double> >{{1.0,0.0,-z0},{-1.0,0.0,-z0},
                                {0.0,1.0,z0},{0.0,-1.0,z0}};
  }
}