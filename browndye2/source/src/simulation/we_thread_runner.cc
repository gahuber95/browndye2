#include "we_thread_runner.hh"
#include "we_simulator.hh"

namespace Browndye{

const Thread_Index t0( 0), t1( 1);

//##################################################
// constructor
WE_Thread_Runner::WE_Thread_Runner( Sim& _sim):
  sim(_sim), barrier( _sim.n_threads()/Thread_Index(1)){
}

//#################################################
void WE_Thread_Runner::run_threads(){
  
  auto nt = sim.n_threads();
  task_refs.resize( nt);
  for( auto ith: range(nt)){
    tasks.emplace_back();
    task_refs[ith] = &tasks.back();
  }
  
  Vector< std::thread> threads;
  Vector< std::packaged_task< void (Thread_Index)>, Thread_Index> ptasks;
  
  auto lrun = [&]( Thread_Index ith){
    run_steps( ith);
  };
    
  for( auto ith: range( t1, nt)){
    (void)ith;
    ptasks.emplace_back( lrun);
  }

  for( auto ith: range( t1, nt)){
    threads.emplace_back( std::move( ptasks[ith - (t1-t0)]), ith);
  }
  lrun( t0);
  
  for( auto& thread: threads){
    thread.join();
  }
  
  if (has_error){
    error( error_msg.value());
  }
}

//#################################################################
void WE_Thread_Runner::run_steps( Thread_Index ith){
  
  while( true){
    run_step( ith);
    
    if (ith == t0){
      ++sim.istep;
    }
      
    barrier.wait();
    
    if (!has_error){
      if (sim.istep == sim.n_steps){
        break;
      }
    }
    else{
      break;
    }
  }
}

//##############################################
double
WE_Thread_Runner::frac_remaining(const WE_Task& task,
                                         Thread_Index nt,
                                          Sys_Copy_Index nc){
  auto n = nc/(nt/t1);
  auto nrem = n - task.n_completed;
  auto inrem = (nrem + Sys_Copy_Index(0))/Sys_Copy_Index(1);
  auto ni = n/Sys_Copy_Index(1);
  return (double)(inrem)/ni;
}

//###############################################
template< class Idx, class Itr>
void WE_Thread_Runner::advance_iterator( Idx n, Itr& itr){
  
  for( auto i: range( n)){
    (void)i;
    ++itr;
    if (itr == sim.states.end()){
      break;
    }
  }
}

//##################################################
void WE_Thread_Runner::setup_needy( WE_Task& task){
  
  auto ineedy = needy.top();
  auto& tasko = *task_refs[ ineedy];
  
  auto next_itr = task.itr; 
  advance_iterator( task.stride, next_itr);
       
  signaled = false;
  needy.pop();
  tasko.itr = next_itr;
  task.stride = task.stride*2;
  tasko.stride = task.stride;
  tasko.being_processed.clear();
}

//###############################################
void WE_Thread_Runner::advance_task( WE_Task& task, Thread_Index it){
  
  auto& state = *( task.itr->second);
  assert( state.mob_info);
  
  step_state( state, it); 
  ++(task.n_completed);
  
  advance_iterator( task.stride, task.itr);
}

//###############################################
WE_Task& WE_Thread_Runner::setup_task( Thread_Index ith){
  
  WE_Task& task = *task_refs[ ith];

  auto nt = sim.n_threads();
  task.stride = WE_Task::SC_Diff( nt/t1);
  
  task.itr = sim.states.begin();
  advance_iterator( ith, task.itr);
  
  task.n_completed = Sys_Copy_Index( 0);
  task.almost_done = false;
  task.being_processed.clear();
  
  return task;
}

//##############################################################
inline
void pspx( Thread_Index ith){
  if (ith == t0){
    std::cout << "         ";
  }
}

//##################################################
void WE_Thread_Runner::finish_if_last( int arg){
  
  ++tdone;
  if (tdone == task_refs.size()){
    ndone = Sys_Copy_Index(0);
    tdone = t0;
    
    // debug
    /*
    auto print_tot_wt = [&]{
      double tot_wt = 0.0;
      auto f = [&]( const System_State& state){
        tot_wt += state.wt;
      };
      
      sim.apply_to_states( f);
      std::cout << "total wt " << tot_wt << std::endl;
    };
    */
    
    //std::cout << "before ";
    //print_tot_wt();
    //std::cout << "update all " << ' ' << n_updates << std::endl;
    //++n_updates;
    sim.update_all();
    //std::cout << "end update all " << std::endl;
    //std::cout << "after ";
    //print_tot_wt();
  }
}
//#######################################################
void WE_Thread_Runner::run_step( Thread_Index ith){
   
  auto& task = setup_task( ith); 
  auto nc = sim.n_states();  
     
  try{
    while(true){
      if (has_error){
        return;
      }
      
      {
        Lock lock( mutex);
        if (ndone == nc){
          signaled = false;
          finish_if_last( 0);
          return;
        }
      }
      
      if (!task.being_processed.test_and_set()){
      
        Sys_Copy_Index ic{ 0};
        
        while( task.itr != sim.states.end()){
          
          if (has_error){
            return;    
          }
          
          if (signaled && !task.almost_done){
            Lock lock( mutex);
            
            if (!needy.empty()){
              setup_needy( task);
            }
          }
          
          else{ // step
            advance_task( task, ith);
            ++ic;
            task.almost_done =  (task.itr == sim.states.end());
          }
        } // end share of copies
        
        {
          Lock lock( mutex);
          
          ndone += (ic - Sys_Copy_Index(0));
          if (ndone == nc){
            while( !needy.empty()){
              auto ind = needy.top();
              needy.pop();
              task_refs[ind]->being_processed.clear();
            }
            
            signaled = false;
            finish_if_last( 1);
            return;
          }
          else{
            needy.push( ith);
            signaled = true;
          }
        }
      } // inner loop
    } // outer loop
  }
  catch( const std::exception& exc){
    Lock lock( mutex);
    error_msg = exc.what();
    has_error = true;
  }
}

//#######################################################
void WE_Thread_Runner::step_state( State& state, Thread_Index it){
            
  state.rethread( sim, it);          
  state.do_large_step( it);
  
  if (state.fate == System_Fate::Escaped){
    if (!sim.building_bins){
      state.set_initial_state();
      state.fate = System_Fate::Escaped;
    }
  }
  else if (state.fate == System_Fate::Final_Rxn){
    if (!sim.building_bins){
      state.set_initial_state();
    }       
  }
}


}
