#pragma once

/*
 * solvent.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#include <fstream>
#include "../lib/units.hh"
#include "../global/physical_constants.hh"
#include "../global/defaults.hh"
#include "../xml/jam_xml_pull_parser.hh"


namespace Browndye{

struct Solvent{
  Solvent() = default;

  Length debye_length = Default::debye_length;
  double dielectric = Default::solvent_dielectric;
  double relative_viscosity = Default::relative_viscosity;
  Energy kT = Default::kT;
  double desolve_fudge = Default::solvation_parameter;
  Length srad = Default::solvent_radius;

  Solvent( const std::string& fname);
  Solvent( const JAM_XML_Pull_Parser::Node_Ptr&);
  
  void output( std::ofstream&) const;
  
  decltype( Length3()/Time()) diffusivity_factor() const;
  Viscosity viscosity() const;
  
private:
  void initialize( const JAM_XML_Pull_Parser::Node_Ptr&);    
};

//#####################################################
inline
Viscosity Solvent::viscosity() const{
  
  return water_viscosity*relative_viscosity;
}

inline 
decltype( Length3()/Time()) Solvent::diffusivity_factor() const{
  
  auto mu = viscosity();
  return kT/mu;
}

}

