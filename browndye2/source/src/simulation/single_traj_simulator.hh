#pragma once

#include <mutex>
#include "simulator.hh"
#include "nam_multi_interface.hh"

namespace Browndye{

class Trajectory_Outputter;

class Single_Traj_Simulator: public Simulator{
public:
  explicit Single_Traj_Simulator( JAM_XML_Pull_Parser::Node_Ptr);
  Single_Traj_Simulator() = delete;
  Single_Traj_Simulator( const Single_Traj_Simulator&) = delete;
  Single_Traj_Simulator( Single_Traj_Simulator&&) = delete;
  Single_Traj_Simulator& operator=( const Single_Traj_Simulator&) = delete;
  Single_Traj_Simulator& operator=( Single_Traj_Simulator&&) = delete;

  //void initialize( const std::string& file);

  //void set_number_of_trajectories( size_t);
  virtual void run() = 0;
  virtual ~Single_Traj_Simulator()= default;
  
protected:
  //friend class Single_NAM_Interface;
  typedef std::unique_ptr< Trajectory_Outputter> Traj_Out_Ptr;

  //virtual void run_individual( System_State&) = 0;
  //virtual void run_trajectory( System_State&, const Traj_Out_Ptr&) = 0;
  
  Traj_Out_Ptr new_trajectory_outputter( const System_State&);
  //void output_results( const System_State&);
  static void start_trajectory_output( const System_State& state, const Traj_Out_Ptr&) ;
  static void end_trajectory_output( const System_State& state, const Traj_Out_Ptr&) ;
  //void print_state( const System_State&,
  //                std::ofstream&, bool state_change ) const;
  //virtual void run_thread( System_State* state) = 0;
 
  size_t n_trajs = 0;
  //size_t n_stuck = 0;
  //size_t n_escaped = 0;
  size_t n_steps_per_output = 1;
  size_t n_output_states_per_block = 1000;
  size_t max_n_steps = maxi;
  size_t n_trajs_per_output = 1;
  std::string traj_file_name;
  //bool do_output_rng_state = false;
  std::mutex mutex;
  bool has_error = false;
  std::exception_ptr except;

  size_t n_trajs_completed = 0;
  size_t n_trajs_started = 0;
 
  size_t n_bd_steps = 0;
};

std::string uitoa( size_t i);

}
