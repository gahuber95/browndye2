#pragma once

#include "../../lib/vector.hh"
#include "../../global/indices.hh"
#include "bonded_parameter_info.hh"
#include "../../structures/bonded_structures.hh"

namespace Browndye{

struct MM_Bonded_Parameter_Info: public Bonded_Parameter_Info{
  
  explicit MM_Bonded_Parameter_Info( Force_Field_Type);
  explicit MM_Bonded_Parameter_Info( Force_Field_Type,  Vector< JAM_XML_Pull_Parser::Node_Ptr>&);

  Force_Field_Type fft = Force_Field_Type::None;
  Vector< MM::Bond_Type, Bond_Type_Index> bond_types;
  Vector< MM::Angle_Type, Angle_Type_Index> angle_types;
  Vector< MM::Dihedral_Type, Dihedral_Type_Index> dihedral_types;
  
  [[nodiscard]] Force_Field_Type ff_type() const override{
    return fft;
  }
  
  ~MM_Bonded_Parameter_Info() override = default;
  
  typedef MM Structures;
};
}


