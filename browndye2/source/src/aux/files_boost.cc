#include "./files.hh"
#include "./files_macros.hh"

#ifdef USING_BOOST

namespace Browndye{
namespace Orchestrator{

using std::string;

//############################
File_Time current_time(){

  return std::time( nullptr);
}

//############################
File_Time earliest_time(){

  return File_Time( 0);
}

//############################
File_Time file_time( const string& name){
  
  auto path = FS::current_path();
  path /= name;
  
  return FS::last_write_time( path) - earliest_time();
}

//############################
bool file_exists( const string& name){
  auto path = FS::current_path();
  path /= name;
  return FS::exists( path);
}

//######################################
void remove_file( const std::string& name){
  auto path = FS::current_path();
  path /= name;
  FS::remove( path);
}

}}

#endif