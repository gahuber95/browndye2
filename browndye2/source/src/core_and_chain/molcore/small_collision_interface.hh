#pragma once

#include "../../lib/vector.hh"
#include "../../structures/atom.hh"
#include "../../transforms/transform.hh"
#include "partition_contents_ext.hh"
#include "../collision_interface.hh"
#include "core_common.hh"
#include "pre_core_thread.hh"

namespace Browndye{

class Small_Col_Interface{
public:
  
  static constexpr bool is_changeable = true;
  static constexpr bool is_chain = false;

  typedef Pos Position;
  typedef Ind_Atom_Index Index;
  typedef ::Length Length;

  typedef Pre_Core_Thread Container;

  static
  void does_have_near_interaction( Container&){};

  typedef Atom_Index Ref;
  typedef Vector< Atom_Index, Ind_Atom_Index> Refs;
  typedef Browndye::Transform Transform;
  typedef Ind_Atom_Index size_type;

  static
  void copy_references( Container& cont, Refs& arefs){
    auto& atoms = cont.common.atoms;
    auto n = converted< Ind_Atom_Index >( atoms.size());
    arefs.resize( n);
    for( auto i: range( n)) {
      arefs[i] = converted< Atom_Index>( i);
    }
  }

  static
  void copy_references([[maybe_unused]] const Container& cont, const Refs& arefs0,
                        Refs& arefs1){
    
    auto n = arefs0.size();
    arefs1.resize( n);
    for( auto i: range( n)) {
      arefs1[i] = arefs0[i];
    }
  }

  static
  size_type size( const Container& cont){
    return converted< Ind_Atom_Index>( cont.common.atoms.size());
  }

  static
  size_type size( const Container&, const Refs& refs){
    return refs.size();
  }

  static
  Pos position( const Container& cont, const Refs& refs, size_type i){
    auto& atoms = cont.common.atoms;
    return atoms[refs[i]].pos;
  }

  static
  Pos transformed_position( Pos pos, const Transform& tform){
    return tform.transformed( pos);
  }

  static
  Length radius( const Container& cont, const Refs& refs, size_type i){
    auto& atoms = cont.common.atoms;
    return atoms[refs[i]].interaction_radius;
  }
 
  template< class F>
  static
  void partition_contents( const Container& cont, const Refs& refs, const F& f,
                           Refs& refs0, Refs& refs1){
    partition_contents_ext< Small_Col_Interface>( cont, refs, f, refs0, refs1);
  }

  static
  void empty_container( Container&, Refs& refs){
    refs.clear();
  }

  static
  void swap( Container&, Refs& refs, size_type i, size_type j){
    auto ref = refs[i];
    refs[i] = refs[j];
    refs[j] = ref;
  }
  
  static
  void clear_transform( Container& cont, Refs& refs){
    auto& tposes = cont.atom_tposes;
    for( auto i: refs)
      tposes[i].transformed = false;
  }
  
  // not needed for Collision_Detector, but for Browndye force calculations
  static
  Atom_Type_Index atom_type( const Container& cont, Ref i){
    auto& atoms = cont.common.atoms;
    return atoms[i].type;
  }

  static
  Atom_Index atom_number( const Container&, Ref i){
    return i;
  }

  static
  Pos transformed_position( Container& cont, const Transform& tform, Atom_Index i){
    auto& ttposes = cont.atom_tposes;
    auto& ttpos = ttposes[i];
    auto& tpos = ttpos.pos;
    auto& is_transformed = ttpos.transformed;
    if (!is_transformed){
      auto& atoms = cont.common.atoms;
      auto& atom = atoms[i];
      tpos = tform.transformed( atom.pos);
      is_transformed = true;
    }
    return tpos;
  }
  
  static
  Length physical_radius( const Container& cont, Atom_Index i){
    auto& atoms = cont.common.atoms;
    return atoms[i].radius;
  }
  
  static
  Length2 sasa( const Container& cont, Atom_Index i){
    auto& atoms = cont.common.atoms;
    return atoms[i].sasa();
  }

  static
  Pos position( const Container& cont, Atom_Index i){
    auto& atoms = cont.common.atoms;
    return atoms[i].pos;
  }
  
  static
  Charge charge([[maybe_unused]] const Container& cont, [[maybe_unused]] Atom_Index i){
    error( __FILE__, __LINE__, "should not get here");
    return Charge( 0.0);
  }

};
}
