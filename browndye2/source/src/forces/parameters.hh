#pragma once

#include "../structures/solvent.hh"
#include "../lib/vector.hh"
#include "../global/indices.hh"

namespace Browndye{

template< class PInfo, class BPInfo>
struct Parameters{
  const Solvent& solvent;
  const PInfo& pinfo;
  const BPInfo& bpinfo;
  
  Parameters( const Solvent&, const PInfo&, const BPInfo&);
};

template< class PInfo, class BPInfo>
Parameters< PInfo, BPInfo>::Parameters( const Solvent& _solvent, const PInfo& _pinfo, const BPInfo& _bpinfo):
  solvent(_solvent), pinfo(_pinfo), bpinfo(_bpinfo){}
  

}
