#pragma once

/*
 * pathways_state.hh
 *
 *  Created on: Oct 15, 2015
 *      Author: root
 */

#include "../lib/vector.hh"
#include "../global/limits.hh"

namespace Browndye{
namespace Pathways{

  template< class I>
  class State{
  public:
    typedef typename I::State_Index State_Index;
    typedef typename I::Rxn_Index Rxn_Index;
    
    State_Index n{ maxi};
    std::string name;
    Vector< Rxn_Index> rxns_to, rxns_from;
  };

}
}

