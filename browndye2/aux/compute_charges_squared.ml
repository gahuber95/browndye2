(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* Generates charge squared file for desolvation forces.  Takes pqrxml file
in standard input and outputs pqrxml file to standard output *)


open Vec3;;

type particle = {
  pos: Vec3.t;
  q: float;
}
;;


Arg.parse [] 
  (fun fname -> Printf.fprintf stderr "no default arguments") 
  "Generates charge squared file for born_integral program"
;;


let particles =
  let fp = Scanf.Scanning.stdin in
  let parts = ref [] in
    Point_parser.apply_to_points ~qtag: "charge" ~buffer: fp
      ~f: (fun ~x:x ~y:y ~z:z ~radius:r ~charge:q ~ptype:ptype ->
	let part = {
	  pos = v3 x y z;
	  q = q;
	}
	in
	  parts := part :: (!parts)
      );
    !parts
;;


Printf.printf "<!-- \"charge\" is actual charge squared -->\n";
Printf.printf "<roottag>\n";

List.iter
  (fun sph ->
    
    let pos = sph.pos in
      
      Printf.printf "  <point>\n";
      Printf.printf "    <x> %g </x> <y> %g </y> <z> %g </z>\n" 
	pos.x pos.y pos.z; 
      
      Printf.printf "    <charge> %g </charge>\n" (sph.q *. sph.q);
      Printf.printf "    <actual_charge> %g </actual_charge>\n" sph.q;
      Printf.printf "  </point>\n";
  )
  particles
;;

Printf.printf "</roottag>\n";;

