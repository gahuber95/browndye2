#pragma once

#include <string>
#include "../lib/vector.hh"
#include "../global/pos.hh"
#include "../global/indices.hh"
#include "../xml/jam_xml_pull_parser.hh"

namespace Browndye{

class Residue_Index_Tag{};
typedef Index< Residue_Index_Tag> Residue_Index;

struct CAtom{
  std::string name;
  Pos pos;
  Atom_Index number;
  Charge charge;

  typedef std::tuple< const std::string&,
		      Pos, Atom_Index, Charge> Rep;

  Rep rep() const{
    return std::make_tuple( name, pos, number, charge);
  }

  bool operator==( const CAtom& other) const{
    return rep() == other.rep();
  }
};

struct Residue{
  std::string name;
  Residue_Index number;
  Vector< CAtom> atoms;
  std::string core;
};

struct XML_Info{
  std::ifstream input;
  JAM_XML_Pull_Parser::Parser parser;
  JAM_XML_Pull_Parser::Node_Ptr top;
  
  XML_Info( const std::string& file): input( file), parser( input, file){
    top = parser.top();
    top->complete();
  }
};

Vector< Residue, Residue_Index> residues_of_file( const std::string& atom_file);

}


