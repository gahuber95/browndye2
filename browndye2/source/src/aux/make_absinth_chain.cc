#include <set>
#include "../xml/node_info.hh"
#include "../global/pos.hh"
//#include "../input_output/get_args.hh"
#include "../global/indices.hh"
#include "ntemplate.hh"

// debug
#include "spqrxml.hh"

// consistency checks
// all and only all of amber atoms are included in residues
// each atom is in at least 2 length constraints
// no repeated constraints
// degrees of freedoms

namespace{
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::cout;
  using std::endl;
  using std::string;
  using std::map;
  using std::optional;
  using std::pair;
  using std::make_pair;

  template< class T>
  using rw = std::reference_wrapper< T>;
    
  template< class T>
  using pair2 = pair< T,T>;  
    
  struct AString{
    string data;
    bool is_prev = false;
    
    bool operator<( const AString& other) const{
      return cmp() < other.cmp();
    }
    
    [[nodiscard]]
    std::tuple< size_t, bool> cmp() const {
      return std::make_tuple( std::hash< string>{}( data), is_prev);
    }
    
  };
  
  struct RTemplate{
    string residue;
    map< string, Vector< AString> > nebors;
    Vector< Vector< AString> > flats;
    Vector< Array< string, 2> > redundants; 
  };
  
  //###########################################################################
  template< class T>
  Vector< T> vector_of_list( const std::list< T>& lst){
    
    Vector< T> res;
    for( auto& t: lst){
      res.push_back( t);
    }
    return res;
  }
  
  //########################################################################
  map< string, RTemplate> new_nebor_template(){
    
    std::stringstream input( template_nebs_text);
    JP::Parser parser( input);
    auto top = parser.top();
    top->complete();
    
    map< string, RTemplate> nebor_template;
    
    auto do_residue = [&]( JP::Node_Ptr rnode){
      RTemplate temp;
      temp.residue = checked_value_from_node< string>( rnode, "name");
      
      auto astring_of = [&]( const string& atom){
        auto na = atom.size();
        return (atom.back() == '-') ? AString{ atom.substr( 0, na-1), true} : AString{ atom};
      };
      
      auto anodes = rnode->children_of_tag( "atom");
      for( auto& anode: anodes){
        auto aname = checked_value_from_node< string>( anode, "name");
        auto nebs = checked_vector_from_node< string>( anode, "nebs");
        for( auto& neb: nebs){
          auto aneb = astring_of( neb);
          temp.nebors[ aname].push_back( aneb);
          if (!aneb.is_prev){
            AString aaname{ aname};
            temp.nebors[ aneb.data].push_back( aaname);
          }
        }
      }
      
      auto fnodes = rnode->children_of_tag( "flat");
      for( auto& fnode: fnodes){
        auto anames = vector_from_node< string>( fnode);
        auto aanames = anames.mapped( astring_of);
        temp.flats.push_back( std::move( aanames));
      }
      
      auto rdnode = rnode->child( "redundant");
      if (rdnode){
        auto lpnodes = rdnode->children_of_tag( "pair");
        auto pnodes = vector_of_list( lpnodes);
        temp.redundants = pnodes.mapped( [&]( JP::Node_Ptr pnode){
          auto atoms = array_from_node< string, 2>( pnode);
          std::sort( atoms.begin(), atoms.end());
          return atoms;
        });
       }
        
      nebor_template[ temp.residue] = std::move( temp);
    };
    
    parser.apply_to_nodes_of_tag( top, "residue", do_residue);
    
    return nebor_template;
  }
  
  //#####################################
  class Residue_Itag{};
  typedef Index< Residue_Itag> Res_Index;
  
  struct Atom{
    Atom_Index num;
    string name;
    Res_Index res_num{ maxi};
    Pos pos;
    Charge charge;
    Length radius;
  };
  
  typedef rw< const Atom> Atom_Ref;
  typedef map< Atom_Index, Atom_Ref> Atom_Map;
  
  //#####################################
  struct Residue{
    string name;
    // residue number is stored in associated map
    Vector< Atom> atoms;
  };

  typedef map< Res_Index, Residue> Residues;
  
  //#####################################
  struct LConstraint{
    std::array< Atom_Ref, 2> atoms;
    Length length;
    
    [[nodiscard]] pair2< Atom_Index> rep() const{
      return make_pair( atoms[0].get().num, atoms[1].get().num);
    }
    
    LConstraint( const Atom& a0, const Atom& a1, Length r): atoms{ std::cref(a0), std::cref(a1)}{
      
      if (a0.num > a1.num){
        std::swap( atoms[0], atoms[1]);
      }
      length = r;
    }
    
    bool operator<( const LConstraint& other) const{
      return rep() < other.rep();
    }
    
    bool operator==( const LConstraint& other) const{
      return rep() == other.rep();
    }
  };
  
  typedef Vector< LConstraint, Constraint_Index> LConstraints;
  
  //#####################################
  struct CConstraint{
    Array< Atom_Index, 4> atoms;
  };
  
  typedef Vector< CConstraint, Constraint_Index> CConstraints;
  
  //#####################################
  Residues new_residues( const string& old_mfile_name){
    
    Residues residues; 
    
    auto do_residue = [&]( JP::Node_Ptr rnode){
      rnode->complete();
      auto rname = checked_value_from_node< string>( rnode, "name");
      auto ir = checked_value_from_node< Res_Index>( rnode, "number");
      auto anodes = vector_of_list( rnode->children_of_tag( "atom"));
      
      Residue residue;
      residue.name = rname;
      residue.atoms = anodes.mapped( [&]( JP::Node_Ptr anode){ 
        auto aname = checked_value_from_node< string>( anode, "name");
        auto num = checked_value_from_node< Atom_Index>( anode, "number");
        auto x = checked_value_from_node< Length>( anode, "x");
        auto y = checked_value_from_node< Length>( anode, "y");
        auto z = checked_value_from_node< Length>( anode, "z");
        auto q = checked_value_from_node< Charge>( anode, "charge");
        auto a = checked_value_from_node< Length>( anode, "radius");
        Pos pos{ x,y,z};
        return Atom{ num, aname, ir, pos, q, a};
      });
      
      residues[ir] = std::move( residue);
    };
    
    std::ifstream input( old_mfile_name);
    JP::Parser parser( input, old_mfile_name);
    auto top = parser.top();
    parser.apply_to_nodes_of_tag( top, "residue", do_residue);
    return residues;
  }
  
  //#####################################################
  // Set up first-order length constraints
  LConstraints
    new_lconstraints( const map<string, RTemplate> &nebor_template, const Residues &residues, Res_Index irb, Res_Index ire) {
    
    LConstraints lconstraints;
       
    auto rbg = residues.begin();
    auto ren = residues.end();
    for( auto itr = rbg; itr != ren; ++itr) {
      auto &entry = *itr;
      auto ir = entry.first;
      if (irb <= ir && ir <= ire) {
        auto &residue = entry.second;
        auto &rname = residue.name;

        auto &templ = [&]() -> const RTemplate & {
          auto jtr = nebor_template.find(rname);
          if (jtr == nebor_template.end()) {
            error("residue name", rname, "is not found in the built-in template");
          }
          return jtr->second;
        }();

        //auto& templ = nebor_template.at( rname);
        auto &atoms = residue.atoms;

        auto prev_itr = [&] {
          if (itr == rbg) {
            return ren;
          }
          else {
            auto jtr = itr;
            --jtr;
            return jtr;
          }
        }();

        auto prev_residue = prev_itr == ren ? nullptr : &(prev_itr->second);

        const Vector<Atom> *prev_atoms = nullptr;
        if (prev_residue) {
          prev_atoms = &(prev_residue->atoms);
        }

        auto &nebors = templ.nebors;
        for (auto &atom0: atoms) {
          auto &aname = atom0.name;
          auto &nebs = [&]() -> const Vector<AString> & {
            auto jtr = nebors.find(aname);
            if (jtr == nebors.end()) {
              error("atom name", aname, "is not found in template for", residue.name);
            }
            return jtr->second;
          }();

          for (auto &neb: nebs) {
            auto &patoms = neb.is_prev ? *prev_atoms : atoms;
            auto ir1 = neb.is_prev ? prev_itr->first : ir;
            auto jtr = std::find_if(patoms.begin(), patoms.end(), [&](const Atom &atom) {
              return atom.name == neb.data;
            });
            if (jtr == patoms.end()) {
              std::cerr << "is neb prev " << neb.is_prev << endl;
              std::cerr << "res " << rname << endl;
              std::cerr << "prev res " << prev_residue->name << endl;
              error("atom", neb.data, "not found in residue", ir1);
            }
            auto &atom1 = *jtr;
            auto r = distance(atom0.pos, atom1.pos);
            LConstraint lcons(atom0, atom1, r);

            lconstraints.push_back(lcons);
          }
        }
      }
    }
    
    return lconstraints;
  }
  
  //###################################################################
  bool operator<( Atom_Ref a0, Atom_Ref a1){
    return &(a0.get()) < &(a1.get());
  }
  
  // Set up second-order length constraints
  void add_2nd_order_constraints( const Atom_Map& all_atoms, LConstraints& lconstraints){

    map< Atom_Index, std::set< Atom_Ref > > all_nebs;
    for( auto& lcons: lconstraints){
      auto& atom0 = lcons.atoms[0].get();
      auto& atom1 = lcons.atoms[1].get();
      auto ia0 = atom0.num;
      auto ia1 = atom1.num;
      all_nebs[ia0].insert( atom1);
      all_nebs[ia1].insert( atom0);
    }
     
    for( auto& entry0: all_nebs){
      auto ia0 = entry0.first;
      auto& atom0 = all_atoms.at( ia0).get();
      auto pos0 = atom0.pos;
      auto& iams = entry0.second;
      for( auto atomm: iams){
        auto iam = atomm.get().num;
        auto& nebsm = all_nebs.at( iam);
        for( auto& atom1r: nebsm){
          auto& atom1 = atom1r.get();
          if (&atom1 != &atom0){
            auto pos1 = atom1.pos;
            auto r = distance( pos0, pos1);
            LConstraint lcons( atom0, atom1, r);
            lconstraints.push_back( lcons);
          }
        }
      }
    }
    
    auto lce = lconstraints.end();
    std::sort( lconstraints.begin(), lce);
    auto new_end = std::unique( lconstraints.begin(), lce);
    lconstraints.erase( new_end, lce);
  }
  
  //###############################################################
  // remove redundant constraints
  // assumes that redundant constraints don't cross residue boundaries
  LConstraints nonredundant_constraints( const map< string, RTemplate>& nebor_template,
                                                 const Residues& residues, const LConstraints& lconstraints){
    LConstraints discards;
    for( auto& lcons: lconstraints){
      auto at0 = lcons.atoms[0].get();
      auto at1 = lcons.atoms[1].get();
      auto ir0 = at0.res_num;
      auto ir1 = at1.res_num;
      auto& aname0 = at0.name;
      auto& aname1 = at1.name;
       
      if (ir0 > ir1){
        error( "residues", ir0, ir1, "out of order");
      }

      auto& rname = residues.at( ir1).name;
      auto& templ = nebor_template.at( rname);
      for( auto& red: templ.redundants){
        auto& ar0 = red[0];
        auto& ar1 = red[1];
        
        if (((ar0 == aname0) && (ar1 == aname1)) ||
            ((ar0 == aname1) && (ar1 == aname0))){
          discards.push_back( lcons);
        }
      }
    }
    std::sort( discards.begin(), discards.end());
  
    
    std::vector< LConstraint> nord_lconstraints;
    std::set_difference( lconstraints.begin(), lconstraints.end(),
                        discards.begin(), discards.end(),
                        std::inserter( nord_lconstraints, nord_lconstraints.begin()));
    return nord_lconstraints;
  }
  
  //##############################################################
  CConstraints
  new_coplanar_constraints( const map<string, RTemplate> &nebor_template, const Residues &residues, Res_Index irb, Res_Index ire) {

    CConstraints cconstraints;
    auto rbg = residues.begin();
    for( auto itr = rbg; itr != residues.end(); itr++) {
      auto &entry = *itr;
      auto ir = entry.first;
      if (irb <= ir && ir <= ire) {
        auto &residue = entry.second;
        auto &rname = residue.name;
        auto &templ = nebor_template.at(rname);
        for (auto &flat: templ.flats) {
          auto nf = flat.size();
          for (auto ifl: range(3, nf)) {

            Array<AString, 4> lflat;
            for (auto k: range(4)) {
              lflat[k] = flat[(ifl - 3) + k];
            }

            auto anum_of = [&](const AString &aatom) {

              auto prev_residue = [&]() {
                if (itr == rbg) {
                  return (const Residue *) nullptr;
                } else {
                  auto jtr = itr;
                  --jtr;
                  return &(jtr->second);
                }
              }();

              auto &res = aatom.is_prev ? *prev_residue : residue;
              auto &aname = aatom.data;
              auto jtr = std::find_if(res.atoms.begin(), res.atoms.end(),
                                      [&](const Atom &atom) {
                                        return atom.name == aname;
                                      });
              if (jtr == res.atoms.end()) {
                error("atom", aname, "not found in residue", ir);
              }
              return jtr->num;
            };

            CConstraint cconstraint{lflat.mapped(anum_of)};
            cconstraints.push_back(cconstraint);
          }
        }
      }
    }
    return cconstraints;
  }
  
  //#############################################
  void output_residues( const string& fname, const map< Res_Index, rw< const Residue> >& new_residues);


  void output_chain( const string &cname, const Atom_Map &core_atoms,
                   const map< Res_Index, rw< const Residue> >& chain_residues, const string &mname,
                  const LConstraints &lconstraints, const CConstraints &cconstraints,
                  const map<Atom_Index, std::set<Atom_Index>> &excluded,
                  const map<Atom_Index, std::set<Atom_Index>> &core_excluded) {
    
    auto is_core = [&]( Atom_Index ia){
      return core_atoms.find( ia) != core_atoms.end();
    };

    auto catoms_file = cname + "_chain_atoms.xml";
    std::ofstream outp{ cname + "_chain.xml"};
    output_residues( catoms_file, chain_residues);

    // Output chain file
    outp << "<top>\n";
    // more info here
    outp << "  <atoms> " << catoms_file << " </atoms>\n";
    outp << "  <name> " << cname << " </name>\n";
    outp << "  <length_constraints>\n";
    for( auto& lcons: lconstraints){
      outp << "    <length_constraint>\n";
      auto ia0 = lcons.atoms[0].get().num;
      auto ia1 = lcons.atoms[1].get().num;
      outp << "      <atoms> " << ia0 << ' ' << ia1 << " </atoms>\n";
      outp << "      <length> " << lcons.length << " </length>\n";
      
      auto m0 = is_core( ia0);
      auto m1 = is_core( ia1);
      if (m0 || m1){
        outp << "      <cores> ";
        outp << (m0 ? mname : "*");
        outp << ' ';
        outp << (m1 ? mname : "*");
        outp << " </cores>\n";
      }
      
      outp << "    </length_constraint>\n";
    }
    outp << "  </length_constraints>\n";
  
    outp << "  <coplanar_constraints>\n";
    for( auto& ccons: cconstraints){
      outp << "    <coplanar_constraint>\n";
      outp << "      <atoms> ";
      for( auto ia: ccons.atoms){
        outp << ia << ' ';
      }
      outp << "</atoms>\n";

      auto mis = ccons.atoms.mapped( is_core);
      auto misany = mis.folded_left( [&]( bool res, bool mi){return res | mi;}, false);
      if (misany){
        outp << "      <cores> ";
        for( auto mi: mis){
          outp << (mi ? mname : "*");
          outp << ' ';
        }
        outp << "</cores>\n";
      }
      
      outp << "    </coplanar_constraint>\n";
    }
    outp << "  </coplanar_constraints>\n";
    
    outp << "  <excluded_atoms>\n";
    for( auto& entry: excluded){
      outp << "    <chain_atom>\n";
      outp << "      <atom> " << entry.first << " </atom>\n";
      outp << "      <ex_atoms> ";
      auto& exs = entry.second;
      for( auto ea: exs){
        outp << ea << ' ';
      }
      outp << "</ex_atoms>\n";
      outp << "    </chain_atom>\n";
    }
    if (!core_excluded.empty()) {
      outp << "    <core>\n";
      outp << "      <name> " << mname << " </name>\n";
      for( auto& item: core_excluded){
        auto ia = item.first;
        outp << "      <core_atom>\n";
        outp << "        <atom> " << ia << " </atom>\n";
        outp << "        <ex_atoms> ";
        auto& exs = item.second;
        for( auto ea: exs){
          outp << ea << ' ';
        }
        outp << "</ex_atoms>\n";
        outp << "      </core_atom>\n";
      }
      outp << "    </core>\n";
    }

    outp << "  </excluded_atoms>\n";
    outp << "</top>\n";
  }
  
  //#####################################################
  /*
  // debug
  struct Residue_Info{
    string residue;
    Vector< Vector< std::pair< string, size_t> > > variants; 
  };
  
  map< string, Residue_Info> new_residue_info(){
    
    std::stringstream input( rdata);
    JP::Parser parser( input);
    auto top = parser.top();
    top->complete();
    
    map< string, Residue_Info> res;
    
    auto proc_res = [&]( JP::Node_Ptr rnode){
      auto vnodes = rnode->children_of_tag( "variant");
      Residue_Info rinfo;
      rinfo.residue = checked_value_from_node< string>( rnode, "name");
      for( auto vnode: vnodes){
        auto& variant = rinfo.variants.emplace_back();
        auto snodes = vnode->children_of_tag( "sub");
        for( auto snode: snodes){
          auto atom = snode->data()[0];
          size_t ior = atoi( snode->data()[1].c_str());
          variant.emplace_back( atom, ior);
        }
      }
      res[ rinfo.residue] = std::move( rinfo); 
    };
    
    auto osnode = top->child( "others");
    auto rnodes = osnode->children_of_tag( "residue");
    for( auto rnode: rnodes){
      proc_res( rnode);
    }
    
    return res;
  }
  */
  
  //######################################################
  map< string, Vector< string> > new_amber_info(){
    
    std::stringstream input( rdata);
    JP::Parser parser( input);
    auto top = parser.top();
    top->complete();
    
    map< string, Vector< string> > res;
    
    auto proc_res = [&]( JP::Node_Ptr rnode){
      auto rname = checked_value_from_node< string>( rnode, "name");
      res[ rname] = checked_vector_from_node< string>( rnode, "atoms"); 
    };
    
    auto anode = top->child( "amber");
    auto rnodes = anode->children_of_tag( "residue");
    for( auto rnode: rnodes){
      proc_res( rnode);
    }
    
    return res;
  }
  
  //########################################################
  [[maybe_unused]]
  void check_consistency( const Residues& residues, const Vector< LConstraint>& lconstraints){
   
    for( auto& lcons: lconstraints){
      auto ia0 = lcons.atoms[0].get().num;
      auto ia1 = lcons.atoms[1].get().num;
      if (ia1 <= ia0){
        error( "wrong order", ia0, ia1);
      }
    }
    
    std::set< Array< Atom_Index, 2> > alt_lcons;
    for( auto& lcons: lconstraints){
      Array< Atom_Index, 2> iats;
      iats[0] = lcons.atoms[0].get().num;
      iats[1] = lcons.atoms[1].get().num;
      auto res = alt_lcons.insert( iats);
      if (!res.second){
        error( "duplicate", iats);
      }
    }
    
    auto amber_info = new_amber_info();
    
    for( auto& entry: residues){
      auto& res = entry.second;
      auto& rname = res.name;
      auto ir = entry.first;
      
      auto is_term = rname.size() == 4;
      
      auto trname = is_term ? rname.substr( 1,3) : rname;
      
      auto ainfo = amber_info.at( trname).copy();
      
      if (is_term){
        if (rname[0] == 'N'){
          auto hitr = std::find( ainfo.begin(), ainfo.end(), "H");
          if (hitr == ainfo.end()){
            error( __LINE__, "not get here");
          }
          ainfo.erase( hitr);
          ainfo.push_back( "H1");
          ainfo.push_back( "H2");
          ainfo.push_back( "H3");          
        }
        else if (rname[0] == 'C'){
          ainfo.push_back( "OXT");
        }
        else{
          error( __LINE__, "not get here");
        }
      }
      
      auto anames = res.atoms.mapped( [&]( const Atom& atom){ return atom.name;});
      
      std::sort( ainfo.begin(), ainfo.end());
      std::sort( anames.begin(), anames.end());
      
      if (anames.size() != ainfo.size()){
        std::cerr << "residue " << rname << ' ' << ir << endl;
        error( "different number of atoms", anames.size(), ainfo.size());
      }
      
      if (ainfo != anames){
        error( "atom lists are different");
      }
    }
    
  }

  //#####################################################################################################################
  [[maybe_unused]]
  void dof_check( const Residues& residues, const Vector< LConstraint>& lconstraints, const Vector< CConstraint>& cconstraints){
    
    // from CA
    map< string, size_t> rdofs{ {"PRO",2},{"GLY",0},{"ALA",1},{"ARG",4},{"ASN",2},{"ASP",2},{"ASH",2},{"CYS",2},{"CYM",1},
    {"GLN",3},{"GLH",3},{"GLU",3},{"HID",2},{"HIE",2},{"HIP",2},{"ILE",4},{"LEU",4},{"LYS",5},{"MET",4},{"PHE",2},{"SER",2},{"THR",3},{"TRP",2},
    {"TYR",3},{"VAL",3}};
    // each residue has 2 additional dof (except for PRO, which is corrected for)
    
    size_t ndofs = 2*residues.size() + 6;
    size_t natoms = 0;
    for( auto& entry: residues){
      auto& res = entry.second;
      auto rname = res.name;
      auto rtname = rname.size() == 4 ? rname.substr( 1,3) : rname;
      auto idof = rdofs.at( rtname);
      ndofs += idof;
      natoms += res.atoms.size();
    }
    
    auto ncons = lconstraints.size() + cconstraints.size();
    
    cout << ndofs << ' ' << 3*natoms - ncons << endl;
  }
  
  //####################################################################
  // for one core:  core_name [-c name begin end] 1 or more  
  
  class Chain_Maker{
  public:
    explicit Chain_Maker( const string &old_mfile_name);
    void make_chain(const string &old_mfile_name, const string &mname, const string &cname,
                    Res_Index ire, Res_Index irb) const;
    
    Residues residues;
    
  private:
    map< string, RTemplate> nebor_template;

    [[nodiscard]] pair2< Atom_Map> atom_maps( const optional< string>& mname, Res_Index irb, Res_Index ire) const;
    [[nodiscard]] pair2< Res_Index> core_bounds( Res_Index, Res_Index) const;
  };

  // constructor
  Chain_Maker::Chain_Maker( const string &old_mname_file) {
    nebor_template = new_nebor_template();
    cout << "got here a0" << endl;
    residues = new_residues( old_mname_file);
  }
  
  //################################################################################
  pair2< Res_Index> Chain_Maker::core_bounds( const Res_Index irb, Res_Index ire) const{
  
    auto has = [&]( Res_Index i){
      return residues.find( i) != residues.end();
    };
    
    const Res_Index R0{ 0}, R1{ 1}, R2{ 2};
    const auto D1 = R1 - R0, D2 = R2 - R0;
    
    auto ilo = [&]{
      if (irb == R0){
        return irb;
      }
      else if (irb == Res_Index{1}){
        return  has( R0) ? R0 : R1;
      }
      else{
        if (has( irb-D1)){
          return has( irb-D2) ? irb-D2 : irb-D1;
        }
        else{
          return irb;
        }
      }
    }();
    
    auto ihi = [&]{
      if (has( ire+D1)){
        return has( ire+D2) ? ire+D2 : ire+D1;
      }
      else{
        return ire;
      }
    }();
    return make_pair( ilo, ihi);
  }
  
  //##############################################################################
  pair2< Atom_Map> Chain_Maker::atom_maps( const optional< string>& mname, Res_Index irb, Res_Index ire) const{
    
    Atom_Map all_ret, core_ret;
    
    if (!mname){
      for( auto& entry: residues){
        auto& res = entry.second;
        for( auto& atom: res.atoms){
          auto ia = atom.num;
          all_ret.insert_or_assign( ia, atom);
        }
      }
    }
    else{
      auto [ilo,ihi] = core_bounds( irb, ire);
      
      for( auto& entry: residues){
        auto ir = entry.first;
        if (ilo <= ir && ir <= ihi){
          auto& res = entry.second;
          for( auto& atom: res.atoms){
            auto ia = atom.num;
            all_ret.insert_or_assign( ia, atom);          
            if (ir < irb || ire < ir){
              core_ret.insert_or_assign( ia, atom);
            }
          }     
        }
      }
    }
    
    return make_pair( all_ret, core_ret);
  }
  
  //###############################################################################################################
  void
  Chain_Maker::make_chain( const string &old_mfile_name, const string &mname, const string &cname,
                          Res_Index ire, Res_Index irb) const{
    
    Atom_Map all_atoms, core_atoms;
    
    std::tie( all_atoms, core_atoms) = atom_maps( mname, irb, ire);
    
    auto is_core = [&]( Atom_Index ia){
      return core_atoms.find( ia) != core_atoms.end();
    };

    auto irend = [&]{
      Res_Index ir_max( 0);
      for( auto& item: residues){
        auto ir = item.first;
        ir_max = std::max( ir_max, ir);
      }
      return ir_max;
    }();

    if (ire > irend){
      error( "residue number too high:", ire);
    }

    auto iree = (ire == irend) ? ire : inced( ire);
    auto lconstraints = new_lconstraints( nebor_template, residues, irb, iree);
    add_2nd_order_constraints( all_atoms, lconstraints);
    auto nord_lconstraints = nonredundant_constraints( nebor_template, residues, lconstraints);
    auto cconstraints = new_coplanar_constraints(nebor_template, residues, irb, iree);
    
    // filter constraints with too many cores
    auto nord_lconstraints1 = nord_lconstraints.filtered( [&]( const LConstraint& cons){
      auto& a0 = cons.atoms[0].get();
      auto& a1 = cons.atoms[1].get();   
      return !(is_core(a0.num) && is_core(a1.num));
    });
    
    auto cconstraints1 = cconstraints.filtered( [&]( const CConstraint& cons){
      
      auto msum = cons.atoms.folded_left( [&]( size_t sum, Atom_Index ia){
        return sum +  (is_core( ia) ? 1 : 0);
      }, 0);
      
      return msum < 3;
    });
    
    // computed excluded atoms
    auto [excluded, core_excluded] = [&](){
      map< Atom_Index, std::set< Atom_Index > > excluded, core_excluded;
      for( auto& lcons: lconstraints){
        auto ia0 = lcons.atoms[0].get().num;
        auto ia1 = lcons.atoms[1].get().num;
        auto m0 = is_core( ia0);
        auto m1 = is_core( ia1);

        if (m0){
          if (!m1){
            core_excluded[ia0].insert( ia1);
          }
        }
        else{
          if (m1){
            core_excluded[ia1].insert( ia0);
          }
          else{
            if (ia0 < ia1){
              excluded[ia1].insert( ia0);
            }
            else{
              excluded[ia0].insert( ia1);
            }
          }
        }
      }
      return std::make_pair( excluded, core_excluded);
    }();

    auto chain_residues = [&]{
      std::map< Res_Index, rw< const Residue> > reses;
      for( auto& item: residues){
        auto ir = item.first;
        if (irb <= ir && ir <= ire){
          reses.insert( std::make_pair( ir, std::cref( item.second)));
        }
      }
      return reses;
    }();

    //check_consistency( residues, nord_lconstraints);
    //dof_check( residues, nord_lconstraints, cconstraints);
    output_chain( cname, core_atoms, chain_residues, mname, nord_lconstraints1, cconstraints1, excluded, core_excluded);
  }
  
  //#################################################################################
  void output_residues( const string& fname, const map< Res_Index, rw< const Residue> >& new_residues) {

    Vector< Res_Index> keys;
    for( auto& item: new_residues){
      keys.push_back( item.first);
    }
    std::sort( keys.begin(), keys.end());

    std::ofstream outp( fname);
    outp << "<top>\n";
    for (auto ir: keys) {
      auto &residue = new_residues.at(ir).get();
      outp << "  <residue>\n";
      outp << "    <name> " << residue.name << " </name>\n";
      outp << "    <number> " << ir << " </number>\n";
      for (auto &atom: residue.atoms) {
        outp << "    <atom>\n";
        outp << "      <name> " << atom.name << " </name>\n";
        outp << "      <number> " << atom.num << " </number>\n";
        auto pos = atom.pos;
        outp << "      <x> " << pos[0] << " </x>\n";
        outp << "      <y> " << pos[1] << " </y>\n";
        outp << "      <z> " << pos[2] << " </z>\n";
        outp << "      <charge> " << atom.charge << " </charge>\n";
        outp << "      <radius> " << atom.radius << " </radius>\n";
        outp << "    </atom>\n";
      }
      outp << "  </residue>\n";
    }
    outp << "</top>\n";
  }

  //#################################################################################
  void output_new_core( const string& mname, const Vector< pair2< Res_Index> >& core_limits, const Residues& residues){
    
    map< Res_Index, rw< const Residue> > new_residues;
    
    //Vector< Res_Index> keys;
    for( auto& entry: residues){
      auto& residue = entry.second;
      auto ir = entry.first;
      auto is_in = core_limits.folded_left( [&]( bool res, pair2< Res_Index> lims){
        auto [irb,ire] = lims;
        auto in_del = irb <= ir && ir <= ire;
        return res & !in_del;
      }, true);
      
      if (is_in){
        new_residues.insert_or_assign( ir, std::cref( residue));
      }
    }

    auto fname = mname + "_core.xml";
    output_residues( fname, new_residues);
  }
  
  //#################################################################################
  void main0( int argc, char* argv[]){
    if (argc == 2 && argv[1] == string( "-help")){  
      cout <<
      "core_file new_core_name [-c chain_name beginning_residue_number end_residue_number]\n"
      "outputs new core file at new_core_name_core.xml\n"
      "and each chain file as chain_name_chain.xml\n"
      "and each chain atom file as chain_name_chain_atoms.xml\n";
      exit(0);
    }

    cout << "got here 0" << endl;

    string old_mname_file{ argv[1]};

    Chain_Maker cmaker( old_mname_file);

    cout << "got here h" << endl;

    if ((argc-3)%4 != 0){
      error( "input cannot be complete");
    }

    Vector< pair2< Res_Index> > core_limits;
    string mname{ argv[2]};
    auto nc = (argc-3)/4;

    cout << "got here 1" << endl;

    // core [-c name begin end]
    for( auto ic: range( nc)){
      auto ic4 = 4*ic;
      string flag{ argv[3 + ic4]};
      if (flag != "-c"){
        error( "need a -c flag in this position");
      }
      string cname{ argv[ 4 + ic4]};
      Res_Index irb{ (size_t)strtol( argv[ 5 + ic4], nullptr, 10)};
      Res_Index ire{ (size_t)strtol( argv[ 6 + ic4], nullptr, 10)};
      core_limits.push_back( make_pair( irb, ire));
      cout << "make chain " << mname << ' ' << cname << endl;
      cmaker.make_chain( old_mname_file, mname, cname, ire, irb);
      cout << "end make chain" << endl;
    }

    output_new_core( mname, core_limits, cmaker.residues);
  }
}

//###########################################
int main( int argc, char* argv[]){
  main0( argc, argv); 
}
