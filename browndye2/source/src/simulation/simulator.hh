#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Simulator is the base class for NAM_Simulator and WE_Simulator.
It holds the molecule data and provides code for reading in 
data from the XML file input to "nam_simulation", we_simulation",
and "build_bins".

*/

#include <set>
#include <ctime>
#include "../molsystem/system_state.hh"
#include "../molsystem/system_common.hh"
#include "../molsystem/system_thread.hh"
#include "../global/physical_constants.hh"
#include "../xml/node_info.hh"
#include "../input_output/outtag.hh"
#include "../xml/check_input_nodes.hh"

namespace Browndye{

class Simulator{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  explicit Simulator( const Node_Ptr&);

  //std::ofstream& output_stream();
  //void input_rng_states( Node_Ptr);

  //void output_rng_states( std::ofstream&, size_t n_spaces) const;
  [[nodiscard]] Thread_Index n_threads() const;
  [[nodiscard]] Sys_Copy_Index n_states() const;
  System_State& state( Sys_Copy_Index);
  System_Thread& thread( Thread_Index);
  
  //template< class F>
  //void apply_to_states( F&&);
  
  [[nodiscard]] Thread_Index thread_index( Sys_Copy_Index) const;

protected:
  //void input_rng_state( Node_Ptr, size_t);
  //void output_rng_state( std::ofstream&, size_t irng, unsigned int n_spaces) const;

  //typedef std::list< Node_Ptr> NList;

  void begin_output( std:: ofstream&);
  
  std::unique_ptr< System_Common> common;
  Vector< System_Thread, Thread_Index> mthreads;
  std::stack< Sys_Copy_Index> unused_indices;
  Sys_Copy_Index max_idx{ 0};
  std::map< Sys_Copy_Index, std::unique_ptr< System_State> > states;
  
  std::string output_name;
  
  Vector< uint32_t> extra_seed;
};

//###################################################
inline
Thread_Index Simulator::n_threads() const{
  return mthreads.size();
}

inline
Sys_Copy_Index Simulator::n_states() const{
  return Sys_Copy_Index( states.size());
}

inline
System_State& Simulator::state( Sys_Copy_Index ic){
  return *(states.at( ic));
}

inline
System_Thread& Simulator::thread( Thread_Index ith){
  return mthreads[ ith];
}

//#######################################
/*
template< class F>
void Simulator::apply_to_states( F&& f){
  
  for( auto& item: states){
    auto& state = *(item.second);
    f( state);
  }
} 
*/

}
