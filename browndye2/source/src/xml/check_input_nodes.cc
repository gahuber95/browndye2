#include "check_input_nodes.hh"

namespace Browndye{
  void check_input( JAM_XML_Pull_Parser::Node_Ptr node){
    
    Tag pattern{ "root", "n_threads", "seed", "output", "n_trajectories",
                 "n_trajectories_per_output", "max_n_steps", "trajectory_file",
                  "n_steps_per_output","min_rxn_dist_file", "n_copies",
                  "n_bin_copies", "n_steps", "n_we_steps_per_output", "bin_file",
                  "n_output_states_per_block",
                  tag("system", "solvent_file", "force_field", "parameters",
                      "start_at_site", "b_radius", "reaction_file",
                      "density_field", "atom_weights",
                      "hydrodynamic_interactions", "n_steps_between_hi_updates",
                      tag( "time_step_tolerances", "constant_dt",
                          "minimum_core_dt", "minimum_chain_dt",
                          "minimum_core_reaction_dt", "minimum_chain_reaction_dt"),
                      tag( "group", "name", "chain",
                          tag("core", "name", "atoms", "hydro_params",
                              "eff_charges", "eff_charges_squared", "areas",
                              tag("electric_field", "grid", "multipole_field"),
                              tag("desolvation_field", "grid"),
                              tag("copy", "core", "translation", "rotation")
                              ),
                          
                              tag( "dummy", "name", "atoms", "core")
                      ),
                      tag( "restraints",
                          tag("restraint", "length",
                              "group0", "core0", "chain0", "atom0",
                              "group1", "core1", "chain1", "atom1"))
                  )
          };
    
    pattern.check_node( node);
  }
}
