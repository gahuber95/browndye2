(*************************************************************)
type face_t = {
  pos: Vec3.t;
  area: float;
  potential: float;
}

type point_charge = {
  charge: float;
  cpos: float*float*float;
}

let pr = Printf.printf 
let pi = 3.1415926 
let sq x = x*.x 

module V = Vec3
module GP = Grid_parser 
module H = Hashtbl

let dx_file_name_ref = ref "" 
let charge_file_name_ref = ref "" 
let debye_ref = ref max_float 
let vperm_ref = ref 0.000142 
let solvdi_ref = ref 78.0 
let cx_ref, cy_ref, cz_ref = ref nan, ref nan, ref nan;;
 
Arg.parse
  [
    ("-dx",
     (Arg.Set_string dx_file_name_ref),
     ": grid dx file"
    );
    
    ("-charges",
     (Arg.Set_string charge_file_name_ref),
     ": charges xml file"
    );

    ("-center",
     (Arg.Tuple [Arg.Set_float cx_ref; Arg.Set_float cy_ref; 
	         Arg.Set_float cz_ref;]),
     ": center of fitting sphere. If not included, default is grid center");
    
    ("-solvdi", Arg.Set_float solvdi_ref, " solvent dielectric (default value 78");
    
    ("-vperm", Arg.Set_float vperm_ref, " vacuum permitivity (default value assuming units of Angstroms, electron charge, and kT (298 K))");
    
    ("-debye",
     (Arg.Set_float debye_ref),
     ": debye length (default infinity)"
    );
    
  ]
  (fun a -> ())
  "Generates a multipole fit from the electrostatic data"
; 

  (* let do_xml = !do_xml_ref *)
  let dx_file_name = !dx_file_name_ref in
  let charge_file_name = !charge_file_name_ref in
  let debye = !debye_ref in
  let vperm = !vperm_ref in
  let solvdi = !solvdi_ref in
  
  let eps = vperm*.solvdi in
  let e4pi = 4.0*.pi*.eps in

  let len = Array.length in

  let min3 a b c = min (min a b) c in
  
  let some_of item = 
    match item with
    | Some obj -> obj
    | None -> raise (Failure "some_of")
  in                    

  if (dx_file_name = "" && charge_file_name = "") then
    failwith "need either file from -dx or -charges";
	     
  let grid_potential,cx,cy,cz,radius,face_pts_n_vs,potential_opt = 
    if (dx_file_name = "") then
      let  pcharges =
	Array.of_list ( 
	    let module JP = Jam_xml_ml_pull_parser in
	    let module NI = Node_info in
	    let cparser = JP.new_parser (Scanf.Scanning.from_file charge_file_name) in
	    JP.fold_nodes_of_tag 
	      cparser "point"
	      (fun lst pnode ->
	       JP.complete_current_node cparser;
	       let q = NI.float_of_child pnode "charge" 
	       and x = NI.float_of_child pnode "x" 
	       and y = NI.float_of_child pnode "y"
	       and z = NI.float_of_child pnode "z"
	       in
	       let pcharge = {
		 charge = q;
		 cpos = x,y,z;
	       }
	       in
	       pcharge :: lst
	      )
	      []
	  )
      in
	    
      let poten pos = 
	let x,y,z = pos.V.x, pos.V.y, pos.V.z in
	Array.fold_left 
	  (fun v pcharge ->
	   let qx,qy,qz = pcharge.cpos in
	   let r = sqrt ((sq (x -. qx)) +. (sq (y -. qy)) +. (sq (z -. qz))) in
	   v +. (exp (-.r/.debye))/.(e4pi*.r)
	  )
	  0.0
	  pcharges
      in

      let cx,cy,cz = !cx_ref, !cy_ref, !cz_ref in

      let radius =
	let rm = 
	  Array.fold_left
	    (fun r pcharge ->
	     let qx,qy,qz = pcharge.cpos in
	     let d = 
	       sqrt ((sq (cx -. qx)) +. (sq (cy -. qy)) +. (sq (cz -. qz))) 
	     in
	     max r d
	    )
	    0.0
	    pcharges
	in
	if rm > 0.0 then
	  2.0*.rm
	else
	  1.0
      in

      let potential_opt pos = 
	 let x,y,z = pos.V.x, pos.V.y, pos.V.z in
	 let dx,dy,dz = x -. cx, y -. cy, z -. cz in
	 let r = sqrt ((sq dx) +. (sq dy) +. (sq dz)) in
	 if (r < radius) then
	   Some (poten pos)
	 else 
	   None
      in

      let verts = 
	let order = 4 in
	let center = V.v3 cx cy cz in 
	let verts_n_areas = Sphere.icoso_face_centers order center radius in
	Array.map 
	  (fun va ->
	   let vert, area = va in
	   vert, poten vert
	  )
	  verts_n_areas	  
      in

      poten, cx,cy,cz, radius, verts,potential_opt

    else	 
       let dx_buf = Scanf.Scanning.from_file dx_file_name in
       let ginfo = GP.new_grid dx_buf in
       let grid = some_of ginfo.GP.data in
       let low = ginfo.GP.low in
       let lx,ly,lz = low.V.x, low.V.y, low.V.z in
       let hx,hy,hz = ginfo.GP.h in                    
       let nx,ny,nz = ginfo.GP.n in
       let ux,uy,uz = 
	 lx +. (float (nx-1))*.hx, ly +. (float (ny-1))*.hy, lz +. (float (nz-1))*.hz
       in
       let cx,cy,cz = 
	 let is_nan = 
	   match classify_float !cx_ref with
	   | FP_nan -> true
	   | _ -> false
	 in
	 if is_nan then
	   0.5*.( lx +. ux), 0.5*.( ly +. uy), 0.5*.( lz +. uz)
	 else
	   !cx_ref, !cy_ref, !cz_ref 
       in 
       let poten pos = 
	 let x,y,z = pos.V.x, pos.V.y, pos.V.z in
	 
	 let ix = int_of_float ((x -. lx)/.hx) 
	 and iy = int_of_float ((y -. ly)/.hy) 
	 and iz = int_of_float ((z -. lz)/.hz)
	 in
	 let value kx ky kz = 
	   Bigarray.Array3.get grid (ix+kx) (iy+ky) (iz + kz) 
	 in
	 
	 let v000 = value 0 0 0
	 and v100 = value 1 0 0 
	 and v010 = value 0 1 0
	 and v110 = value 1 1 0
	 and v001 = value 0 0 1
	 and v101 = value 1 0 1 
	 and v011 = value 0 1 1
	 and v111 = value 1 1 1
	 in
	 let xl = lx +. (float ix)*.hx
	 and yl = ly +. (float iy)*.hy
	 and zl = lz +. (float iz)*.hz
	 in
	 let xh = xl +. hx
	 and yh = yl +. hy
	 and zh = zl +. hz
	 in
	 
	 let v00 = ((z -. zl)*.v001 +. (zh -. z)*.v000)/.hz
	 and v01 = ((z -. zl)*.v011 +. (zh -. z)*.v010)/.hz
	 and v10 = ((z -. zl)*.v101 +. (zh -. z)*.v100)/.hz
	 and v11 = ((z -. zl)*.v111 +. (zh -. z)*.v110)/.hz
	 in
	 
	 let v0 = ((y -. yl)*.v01 +. (yh -. y)*.v00)/.hy
	 and v1 = ((y -. yl)*.v11 +. (yh -. y)*.v10)/.hy
	 in
	 ((x -. xl)*.v1 +. (xh -. x)*.v0)/.hx
       in

       let face_pts_n_vs = 
	 let dict = H.create 0 in
	 for iy = 0 to ny-1 do
	   for iz = 0 to nz-1 do
             H.add dict (0,iy,iz) nan; 
             H.add dict (nx-1,iy,iz) nan; 
	   done;
	 done;
	 
	 for ix = 1 to nx-2 do
	   for iz = 0 to nz-1 do
             H.add dict (ix,0,iz) nan;
             H.add dict (ix,ny-1,iz) nan;
	   done;
	 done;
	 
	 for ix = 1 to nx-2 do
	   for iy = 1 to ny-2 do
             H.add dict (ix,iy,0) nan;
             H.add dict (ix,iy,nz-1) nan;
	   done;
	 done;
	 
	 Array.of_list (
             H.fold 
	       (fun idx vdum res ->
		let ix,iy,iz = idx in
		let v = Bigarray.Array3.get grid ix iy iz in
		let x = lx +. hx*.(float ix)
		and y = ly +. hy*.(float iy)
		and z = lz +. hz*.(float iz)
		in
		if not (v = v) then (
		  pr "%d %d %d\n" ix iy iz;
		  exit 0;
		);
		
		(V.v3 x y z, v) :: res
	       ) 
	       dict []
	   )
		       
       in

       let potential_opt pos = 
	 let x,y,z = pos.V.x, pos.V.y, pos.V.z in
	 let dx,dy,dz = x -. lx, y -. ly, z -. lz in
	 let ix = int_of_float (dx/.hx)
	 and iy = int_of_float (dy/.hy)
	 and iz = int_of_float (dz/.hz)
	 in
	 let inr i n = 
	   0 <= i && i < (n-1)
	 in
	 if not ((inr ix nx) && (inr iy ny) && (inr iz nz)) then (
	   None
	 )
	 else
	   Some (poten pos)
       in
       
       let r = min 
		 (min3 (cx-.lx) (cy-.ly) (cz-.lz)) 
		 (min3 (ux-.cx) (uy-.cy) (uz-.cz)) 
       in 
       poten,cx,cy,cz,r,face_pts_n_vs,potential_opt
  in
  
  let center = V.v3 cx cy cz in
       
  let faces = 
    let order = 3 in
    let r = radius in
    let verts_n_areas = Sphere.icoso_face_centers order center r in

    let verts = 
      Array.map 
	(fun va ->
	 let vert,area = va in
	 vert)
	verts_n_areas
    in

    let areas = 
      Array.map 
	(fun va ->
	 let vert,area = va in
	 area)
	verts_n_areas
    in

    let n = len verts_n_areas in
    let vs = Array.map grid_potential verts in         
    Array.init n
	       (fun i -> {
		  pos = verts.(i);
		  area = areas.(i);
		  potential = vs.(i);
	        }
	       )
  in

  let igral f =
    Array.fold_left
      (fun res face ->
       let pos = face.pos in 
       let x = pos.V.x -. cx
       and y = pos.V.y -. cy
       and z = pos.V.z -. cz
       in
       let ff = f x y z in
       res +. face.area*.ff*.face.potential
      )
      0.0
      faces
  in

  let r = radius in
  let r2 = r*.r in

  let rf2 x y z = x*.x +. y*.y +. z*.z in
  let rf x y z = sqrt (rf2 x y z) in

  let nrm1 = sqrt (4.0*.pi/.3.0) in
  let f1 x y z = 1.0/.sqrt(4.0*.pi) 
  and fx x y z = x/.(nrm1*.(rf x y z)) 
  and fy x y z = y/.(nrm1*.(rf x y z)) 
  and fz x y z = z/.(nrm1*.(rf x y z)) 
  in
  
  let nrm2 = sqrt (4.0*.pi/.15.0) in
  let fqm2 x y z = x*.y/.(nrm2*.(rf2 x y z)) 
  and fqm1 x y z = z*.y/.(nrm2*.(rf2 x y z)) 
  and fq0 x y z = (3.0*.z*.z/.(rf2 x y z) -. 1.0)/.(2.0*.(sqrt 3.0)*.nrm2) 
  and fq1 x y z = z*.x/.(nrm2*.(rf2 x y z)) 
  and fq2 x y z = (x*.x -. y*.y)/.(2.0*.(rf2 x y z)*.nrm2) 
  in  
  
  let ig1 = igral f1 
  and igx = igral fx 
  and igy = igral fy 
  and igz = igral fz 
  and igqm2 = igral fqm2
  and igqm1 = igral fqm1
  and igq0 = igral fq0
  and igq1 = igral fq1
  and igq2 = igral fq2
  in

  let k0s r = 
    1.0/.r
  in
  let k1s r = 
    (k0s r)*.(1.0/.debye +. 1.0/.r)
  in         
  
  let k2s r = 
    (k0s r)*.(1.0/.(sq debye) +. 3.0/.(r*.debye) +. 3.0/.(r*.r))
  in
  
  let k0 = k0s radius
  and k1 = k1s radius
  and k2 = k2s radius
  in               
                         
  let pol1 = e4pi*.ig1/.(r2*.k0)
                          
  and polx = e4pi*.igx/.(r2*.k1)
  and poly = e4pi*.igy/.(r2*.k1)
  and polz = e4pi*.igz/.(r2*.k1)
                          
  and polqm2 = e4pi*.igqm2/.(r2*.k2)
  and polqm1 = e4pi*.igqm1/.(r2*.k2)
  and polq0 =  e4pi*.igq0/.(r2*.k2)
  and polq1 =  e4pi*.igq1/.(r2*.k2)
  and polq2 =  e4pi*.igq2/.(r2*.k2)
  in
  
  let mpole_potential apos = 
    let pos = V.vdiff apos center in
    let r = V.vnorm pos in
    let dfactor = (exp (-.(r -. radius)/.debye))/.e4pi in
    let x,y,z = pos.V.x, pos.V.y, pos.V.z in
    let comp0 = (f1 x y z)*.pol1*.(k0s r)
    and comp1 = ((fx x y z)*.polx +. (fy x y z)*.poly +. (fz x y z)*.polz)*.(k1s r)
    and comp2 = ((fqm2 x y z)*.polqm2 +. (fqm1 x y z)*.polqm1 +. (fq0 x y z)*.polq0 +. 
		   (fq1 x y z)*.polq1 +. (fq2 x y z)*.polq2)*.(k2s r)
    in
    dfactor*.(comp0 +. comp1 +. comp2)
  in    
               
  let vnrm = 
    let sum = 
      Array.fold_left
        (fun sum posv ->
         let pos,av = posv in
         sum +. av*.av
        )
        0.0
        face_pts_n_vs
    in
    sqrt (sum/.(float (len face_pts_n_vs)))
  in
         
  let abs_error = 
    let sum = 
      Array.fold_left
        (fun sum posv ->
         let pos,av = posv in
         let v = mpole_potential pos in
         sum +. (sq (v -. av))
        )
        0.0
        face_pts_n_vs
    in
    sqrt (sum/.(float (len face_pts_n_vs)))
  in
    
  let rel_error = abs_error/.vnrm in

(* ********************************************************** *)

let full_potential pos = 
  match potential_opt pos with
    | Some v -> v
    | None -> mpole_potential pos
in

let sphere_points r = 
  let center = V.v3 cx cy cz in
  let order = 4 in
  let sphere = Sphere.made ~order: order ~radius: r ~center: center in
  Sphere.icoso_vertices sphere
in

let sphere_variation r = 
  let pts = sphere_points r in
  let n = len pts in
  let vs = Array.map full_potential pts in
  let avg = 
    let sum = Array.fold_left (+.) 0.0 vs in
    sum /. (float n)
  in
  Array.fold_left
    (fun res v -> 
      let diff = abs_float (v -. avg) in
      max diff res
    )
    0.0
    vs
in

let smooth_radius =
  let spv = sphere_variation in

  let tol = 0.01 in
  let rec bisect rout fout rin fin = 
    let rmid = 0.5*.( rout +. rin) in
    if (rout -. rin) < 0.01*.radius then
      rmid
    else
      let fmid = spv rmid in
      if fmid > tol then
	bisect rout fout rmid fmid
      else
	bisect rmid fmid rin fin

  in
  let rec found_outer r =
    if (spv r) < tol then
      r
    else
      found_outer (2.0*.r)
  in
  let rout = found_outer radius in 
   
  if rout = 0.0 then
    0.0
  else
    let rec found_inner r =  
      if (spv r) > tol || r < 1.0e-20*.radius then
        r
      else
        found_inner (0.5*.r)
    in
    let rin = found_inner radius 
    in
    bisect rout (spv rout) rin (spv rin)
in


(* ********************************************************** *)
  let ptag n tag value =
    for i = 1 to n do
      pr " "
    done;
    pr "<%s> %g </%s>\n" tag value tag
                  
  and ptago n tag = 
    for i = 1 to n do
    pr " "
    done;
    pr "<%s>\n" tag
                  
                  
  and ptagc n tag = 
    for i = 1 to n do
      pr " "
    done;
    pr "</%s>\n" tag
  in
  
  pr "<!-- Output of mpole_grid_fit -->\n";
  ptago 0 "multipole_field";
  
  ptago 2 "center";
  ptag 4 "x" cx;
  ptag 4 "y" cy;
  ptag 4 "z" cz;
  ptagc 2 "center";

  ptag 2 "charge" (pol1*.(exp (radius/.debye))/.(sqrt (4.0*.pi)));
  ptag 2 "zeroth_order_pole" pol1;

  ptago 2 "first_order_poles"; 
  ptag 4 "x" polx;
  ptag 4 "y" poly;
  ptag 4 "z" polz;
  ptagc 2 "first_order_poles"; 

  ptago 2 "second_order_poles";
  ptag 4 "qm2" polqm2;
  ptag 4 "qm1" polqm1;
  ptag 4 "q0" polq0;
  ptag 4 "q1" polq1;
  ptag 4 "q2" polq2;
  ptagc 2 "second_order_poles";

  ptag 2 "absolute_error" abs_error;
  ptag 2 "relative_error" rel_error;
  ptag 2 "rms_energy" vnrm;

  ptag 2 "debye" debye;

  ptag 2 "vacuum_permittivity" vperm;
  ptag 2 "solvent_dielectric" solvdi;
  ptag 2 "fit_radius" radius;
  
  ptag 2 "smooth_radius" smooth_radius; 

  ptagc 0 "multipole_field";
