#include "../global/error_msg.hh"

namespace Browndye{

bool bool_of_str( const std::string& tf){
  typedef std::string Str;
  if (tf == Str( "true") || 
      tf == Str( "True") || 
      tf == Str( "TRUE")){
    
    return true;
  }
  else if (tf == Str( "false") ||
           tf == Str( "False") ||
           tf == Str( "FALSE"))

    return false;
    
  else{
    error( "bool_of_str: input not in boolean format");
    return false;
  }
}
//####################################################
std::string str_of_bool( bool tf){
  
  return tf ? "true" : "false";
}


}
