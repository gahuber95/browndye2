/*
 * molecule_state.cc
 *
 *  Created on: Jul 13, 2016
 *      Author: root
 */

#include "core_state.hh"
#include "../../group/group_common.hh"
#include "../../group/group_state.hh"
//#include "../../molsystem/system_state.hh"
#include "../reset_hints.hh"

namespace Browndye{

void Core_State::reset_hints( const Vector< Group_Common, Group_Index>& gcommons){

  Browndye::reset_hints( *this, gcommons);
}

//##################################################################
// constructor
Core_State::Core_State( Core_Thread& thread, const Core_Common& common,
                         Group_State& _group,
                        const Vector< Group_Common, Group_Index>& gcommons, bool for_mob):
  force( Zeroi{}), torque( Zeroi{}), 
  group_ref( _group.common()), thread_ref( thread), common_ref( common){
  
  _transform = common.copy_tform;
  
  if (!for_mob){
    reset_hints(  gcommons);
    
      auto& bound_atoms = common.bound_atoms;
    auto nb = bound_atoms.size();
    constrained_poses.resize( nb);
    set_to_start(gcommons);
  }
}

//###########################################################
// copy constructor
Core_State::Core_State( const Core_State& other):
  group_ref( other.group_ref), thread_ref( other.thread_ref),
  common_ref( other.common_ref){
  
  copy_from( other);
}

//##########################################################
Core_State& Core_State::operator=( const Core_State& other){
  
  group_ref = other.group_ref;
  copy_from( other);
  return *this;
}

//###################################################
void Core_State::copy_from( const Core_State& other){

  force = other.force;
  torque = other.torque;
  v_hints = other.v_hints.copy();
  born_hints = other.born_hints.copy();
  _transform = other._transform;
  constrained_poses = other.constrained_poses.copy();
}

//##############################################
Group_Index Core_State::group_number() const{
  return group_ref.get().number;
}

void Core_State::set_to_start(const Vector<Group_Common, Group_Index> &gcommons) {

  auto htform = common().home_tform();
  _transform = htform;

  update_constrained_atoms();
  reset_hints( gcommons);
  update_constrained_atoms(); // needed?
}
//##############################################################
void Core_State::update_constrained_atoms(){
  
  for( auto ib: range( constrained_poses.size())){
    auto& ainfo = common().bound_atoms[ib];
    auto ia = ainfo.ia;
    auto& atom = common().atoms[ia];
    constrained_poses[ib] = _transform.transformed( atom.pos);
  }

  /*
  auto& sys = common().system;
  auto& dumids = common().dummies;
  auto& group = sys.groups[common().ig];
  for( auto iid: range( dummy_poses.size())){
    auto id = dumids[iid];
    auto& dmol = group.dummies[ id];
    auto& datoms = dmol.atoms;
    auto na = datoms.size();
    auto& ldum_poses = dummy_poses[iid];
    for( auto ia: range( na)){
      auto& datom = datoms[ia];
      ldum_poses[ia] = _transform.transformed( datom.pos);
    }
  }
   */
}

}

