#pragma once
#include <map>
#include "../lib/vector.hh"
#include "../global/indices.hh"
#include "../structures/atom.hh"
#include "../xml/jam_xml_pull_parser.hh"
#include "../forces/other/nonbonded_parameter_info.hh"
#include "../group/test_tag.hh"

namespace Browndye{

  struct Macro_Struct{
    std::string name, atoms_file;
    Group_Index ig;
    Vector< Atom, Atom_Index> atoms;
    std::map< Atom_Index, Atom_Index> atom_imap;
    
    Macro_Struct( const JAM_XML_Pull_Parser::Node_Ptr&, const Nonbonded_Parameter_Info&, Group_Index);

    // for testing
    explicit Macro_Struct( Test_Tag, const Vector<Browndye::Atom, Browndye::Atom_Index> &);

  private:
    void setup_map();
  };

}
