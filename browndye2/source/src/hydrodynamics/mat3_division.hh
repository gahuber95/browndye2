#pragma once

#include "../lib/units.hh"
#include "../lib/array.hh"
#include "../lib/cholesky.hh"
#include "../lib/solve3.hh"

namespace Browndye{

//##########################################################
template< class Bt>
class I_Mat3_Cholesky{
public:
  typedef decltype( sqrt( Bt())) Lt;
  
  typedef Mat3< Bt> Matrix;
  typedef Mat3< Lt> Dec_Matrix;
  typedef Bt Matrix_Value;
  typedef Lt Dec_Matrix_Value;
  typedef size_t Index;
  
  template< class T>
  static
  T element( Mat3< T> B, size_t i, size_t j){
    return B[i][j];
  }
  
  template< class T>
  static
  T& element_ref( Mat3< T>& B, size_t i, size_t j){
    return B[i][j];
  }
  
  template< class T>
  static
  size_t size( Mat3< T> ){
    return 3;
  }
   
  template< class T>
  static
  T transpose( T t){
    return t;
  }
  
  template< class T>
  static
  T zero(){
    return T(0.0);
  }
  
  template< class T>
  static
  bool is_negative( T t){
    return t < T(0.0);
  }
  
  template< class T>
  static
  bool is_zero( T t){
    return t == T(0.0);
  }
  
  static
  void error( const std::string& msg){
    Browndye::error( msg);
  }
};

//#################################################
template< class Bt, class Lt>
auto operator/( Vec3< Bt> _b, Mat3< Lt> _L){
  
  typedef decltype( Bt()/Lt()) Xt;
  Vec3< Xt> x;  
 
  auto b = _b;
  auto L = _L;
  Solve3::solve( L, b, x);
  
  return x;
}

// assume Lt is upper triangular
template< class Bt, class Lty>
auto operator/( Mat3< Bt> B, Mat3< Lty> Lt){

  typedef decltype( Bt()/Lty()) Xt;
  Mat3< Xt> X;
  
  for( auto ir: range(3)){
    auto& b = B[ir];
    auto& x = X[ir];
    
    x[0] = b[0]/Lt[0][0];
    x[1] = (b[1] - Lt[0][1]*x[0])/Lt[1][1];
    x[2] = (b[2] - Lt[0][2]*x[0] - Lt[1][2]*x[1])/Lt[2][2];
  }
  
  return X;
}

}

template< class Mt>
auto sqrt( Browndye::Mat3< Mt> M){
  typedef decltype( sqrt(Mt())) Lt;
  Browndye::Mat3< Lt> L;
  
  bool success; // later hook to outside
  Browndye::Cholesky::decompose< Browndye::I_Mat3_Cholesky< Mt> >( M, L, success);
  if (!success){
    Browndye::error( "Mat3 matrix sqrt failed");
  }
  Lt L0( 0.0);
  L[0][1] = L0;
  L[0][2] = L0;
  L[1][2] = L0;
  return L;
}

template< class T0, class T1, class T2, class T3, class F>
Unit< T0,T1,T2,T3,F> transpose( Unit< T0,T1,T2,T3,F> x){
  return x;
}


