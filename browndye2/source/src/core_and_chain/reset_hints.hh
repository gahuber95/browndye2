#pragma once

#include "../group/group_common.hh"

namespace Browndye{

template< class Object>
void reset_hints( Object& obj, const Vector< Group_Common, Group_Index>& gcommons){
  auto ng = gcommons.size();
  auto& v_hints = obj.v_hints;
  auto& born_hints = obj.born_hints;
  v_hints.resize( ng);
  born_hints.resize( ng);
  for( auto gi: range( ng)){
    auto& gcommon = gcommons[gi];
    auto& cores = gcommon.cores;
    auto nc = cores.size();
    auto& vhs = v_hints[gi];
    auto& bhs = born_hints[gi];
    vhs.resize( nc);
    bhs.resize( nc);
    for( auto ci: range( nc)){
      vhs[ci] = cores[ci].v_hint0;
      bhs[ci] = cores[ci].born_hint0;
    } 
  }
}
}

