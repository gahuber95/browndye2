#pragma once
#include <array>
#include <algorithm>
#include "../global/error_msg.hh"

namespace Browndye{

  enum class Force_Field_Type {None, Molecular_Mechanics, Molecular_Mechanics_HP, Absinth, Spline};
  
  inline 
  const std::array< std::string, 5> ff_reps{ "None", "molecular_mechanics", "molecular_mechanics_hp", "ABSINTH", "spline"};
  
  //####################################################
  inline
  std::string force_field_rep( Force_Field_Type ff) {
    
    if (ff == Force_Field_Type::None){
      error( "force field not defined");
      return "";
    }
    else{
      return ff_reps[ (int)ff];
    }
  }
  
  //####################################################
  inline
  Force_Field_Type force_field_of_rep( const std::string& rep){
    
    auto ffb = ff_reps.begin();
    auto ffe = ff_reps.end();
    auto itr = std::find( ffb, ffe, rep);
    if (itr == ffe){
      error( "force field", rep, "is not defined");
      return Force_Field_Type::None;
    }
    else{
      auto i = itr - ffb;
      return (Force_Field_Type)i;
    }
  }
  
}
