#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/* 
Generic code for finding the smallest bounding sphere of a group
of points.  This implements the algorithm of E. Welzl
("Smallest Enclosing Disks (Balls And Ellipsoids)", 
Lect. Notes Comput. Sci. 555 (1991), 359-370.)

Also used to find the smallest bounding sphere of a collection of
smaller spheres.
 
Equivalent to the Ocaml code circumspheres.ml.

The I class has the following types and functions:

typedef Length
typedef Position
typedef Points;
typedef Point_Refs;
typedef Index;

static
void initialize( const Points& pts, Point_Refs& refs);

static
Index size( const Points&);

static
void get_position( const Points& pts, const Point_Refs& refs,
                   Index i, Vec3< Length>& pos);

static
void swap( Points& pts, Point_Refs& refs, Index i, Index j);

template< typename ... Args>
static
void error( Args ... args);


// needed by get_smallest_enclosing_sphere_of_spheres
static
Length radius( const Points& pts, const Point_Refs& refs, Index i);

*/

#include <cstdlib>
#include <cmath>
#include <stack>

#include "solve3.hh"
#include "array.hh"



namespace Browndye::Circumspheres{


template< class I>
void get_smallest_enclosing_sphere( typename I::Points& pts_in,
                                    typename I::Position& center, Length& radius);


template< class I>
void 
get_smallest_enclosing_sphere_of_spheres( typename I::Points& pts_in,
                                          typename I::Position& center,
                                           typename I::Length& radius);


//################################################################

template< class I>
class Doer{
public:
  typedef typename I::Length Length;
  typedef typename I::Points Points;
  typedef typename I::Position Position;
  typedef typename I::Point_Refs Point_Refs;
  typedef typename I::Index Index;
  
  typedef Array< Length, 3> Pos;
  
  static
  void get_smallest_triangle_sphere( Pos p0, Pos p1, Pos p2,
                                     Pos& center, 
                                     Length& radius);

  static
  void get_smallest_tetrahedron_sphere( Pos p0, Pos p1, Pos p2, Pos p3,
                                         Pos& center, 
                                         Length& radius);
  
  static
  void get_smallest_enclosing_sphere_inner( Points& pts_in, Point_Refs& pts,
                                       Pos& center, Length& radius);
  
  static
  void get_smallest_enclosing_sphere( Points& pts_in,
                                      Position& center, Length& radius);

  static  
  void get_smallest_enclosing_sphere_of_spheres( Points& pts_in,
                                                Position& center,
                                                Length& radius);  
  static
  void get_circumcircle( Pos p0, Pos p1, Pos p2,
                          Pos& center, Length& radius);
  
  static
  void get_circumsphere( Pos p0, Pos p1, Pos p2, Pos p3,
                          Pos& center, Length& radius);
  
  static
  bool inside_tetrahedron( Pos p0, Pos p1, Pos p2, Pos p3, Pos p);
    
  static  
  bool stest( Pos origin, Pos perp, Pos vert, Pos p);

private:
  static
  std::pair< Pos, Length> welzl( Points& pts_in, Point_Refs& pts);

  static
  std::pair< Pos, Length> sphere_of_pair( Pos, Pos);

  static
  std::pair< Pos, Length> sphere_of_triangle( Pos, Pos, Pos);

  static
  std::pair< Pos, Length> sphere_of_tet( Pos, Pos, Pos, Pos);

  static
  bool is_inside( std::pair< Pos, Length> sph, Pos);
};


template< class I>
void get_smallest_enclosing_sphere( typename I::Points& pts_in,
                                    typename I::Position& center, Length& radius){
  
  Doer<I>::get_smallest_enclosing_sphere( pts_in, center, radius);
}


template< class I>
void 
get_smallest_enclosing_sphere_of_spheres( typename I::Points& pts_in,
                                          typename I::Position& center,
                                           typename I::Length& radius){
  
  Doer<I>::get_smallest_enclosing_sphere_of_spheres( pts_in, center, radius);
}


//********************************************************************
using std::max;

template< class I>
void Doer<I>::
get_smallest_enclosing_sphere_inner( Points& pts_in,
                                     Point_Refs& pts,
                                     Pos& center, Length& radius){
  
  const auto n = I::size( pts_in);

  auto rmod = []( auto n, auto i){
     return (n + (i - Index(0))) % n;
   };

  Index s0(0), s1(1), s2(2), s3(3), s4(4);

  if (n == s0)
    I::error( "get_smallest_enclosing_sphere: no points");
  else if (n == s1){
    I::get_position( pts_in, pts, s0, center);
    radius = Length( 0.0);
  }
  else if (n == s2){
    Vec3< Length> p0, p1;
    I::get_position( pts_in, pts, s0, p0);
    I::get_position( pts_in, pts, s1, p1);
    center = 0.5*(p0 + p1);
    radius = max( distance( p0, center), distance( p1, center));
  }
  else if (n == s3){
    Vec3< Length> p0, p1, p2;
    I::get_position( pts_in, pts, s0, p0);
    I::get_position( pts_in, pts, s1, p1);
    I::get_position( pts_in, pts, s2, p2);
    get_smallest_triangle_sphere( p0, p1, p2, center, radius); 
  }
  else if (n == s4){
    Vec3< Length> p0, p1, p2, p3;
    I::get_position( pts_in, pts, s0, p0);
    I::get_position( pts_in, pts, s1, p1);
    I::get_position( pts_in, pts, s2, p2);
    I::get_position( pts_in, pts, s3, p3);
    get_smallest_tetrahedron_sphere( p0, p1, p2, p3, center, radius); 

  }
  else{
    auto i0 = s0;
    Index i{};

  outer:
    //cout << "outer\n";
    auto d1 = s1 - s0;
    auto d2 = s2 - s0;
    auto d3 = s3 - s0;
    auto d4 = s4 - s0;

    i = rmod( n, i0 + d4);
    //cout << "i0 " << i0 << endl;
    //cout << "i " << i << endl;

    Vec3< Length> p0, p1, p2, p3;
    I::get_position( pts_in, pts, rmod( n, i0   ), p0);
    I::get_position( pts_in, pts, rmod( n, i0+d1), p1);
    I::get_position( pts_in, pts, rmod( n, i0+d2), p2);
    I::get_position( pts_in, pts, rmod( n, i0+d3), p3);
    
    get_smallest_tetrahedron_sphere( p0,p1,p2,p3, center, radius);
    //cout << "radius " << radius << endl;

  inner:
    //cout << "inner\n";
    Vec3< Length> p;
    I::get_position( pts_in, pts, i, p);
    if (distance( center, p) > radius){
      //cout << "distance " << distance( center, p) << endl;
      i0 = rmod( n, i0-d1);
      //cout << "i0 " << i0 << endl;
      I::swap( pts_in, pts, i0, i);
      //cout << "swap " << i << ' ' << i0 << endl;
      goto outer;
    }
    else{
      i = rmod( n, i+d1);
      //cout << "i " << i << endl;
      if (!(i == i0))
        goto inner;
    }

  }

  // consistency check
  for( auto i: range( n)){
  //for (auto i = s0; i < n; i++){
    Vec3< Length> pos;
    I::get_position( pts_in, pts, i, pos);
    Length r = distance( pos, center);
    if (r > 1.0001*radius){
      I::error( "outsider", radius, r);
    }
  }
}

//##################################################################
template< class I>
auto Doer<I>::welzl( Points& pts_in, Point_Refs& pts) -> std::pair< Pos, Length>{

  const auto n = I::size( pts_in);
  Array< Index, 4> bound_pts;
  std::pair< Pos, Length> res;

  auto sph_of = [&]( int ib){
    if (ib == 1){
      Pos p;
      I::get_position( pts_in, pts, bound_pts[0], p);
      return std::make_pair( p, Length(0));
    }
    else if (ib == 2){
      return sphere_of_pair( bound_pts[0], bound_pts[1]);
    }
    else if (ib == 3){
      return sphere_of_triangle( bound_pts[0], bound_pts[1], bound_pts[2]);
    }
    else{
      return sphere_of_tet( bound_pts[0], bound_pts[1], bound_pts[2], bound_pts[3]);
    }
  };

  struct Frame{
    Index ip;
    bool extra_bound = false;
    std::pair< Pos, Length> res;
  };

  int ib = 0;
  Index ic{ 0};
  std::stack< Frame> lstack;
  Vector< bool, Index> chosen( n, false);

  size_t ip = 0;
  Frame frame0;
  frame0.ip = 0;
  lstack.push( frame0);
  bound_pts[0] = 0;
  chosen[0] = true;
  ++ib;
  while( !lstack.empty()){
    auto frame = lstack.top();
    lstack.pop();
    if (ib == 4 || ic == n){
      auto lres = sph_of();
      if (lstack.empty()){
        res = frame.res;
        break;
      }
      else{
        lstack.top().res = lres;
      }
    }
    else{
      while( chosen[ip]){
        ++ip;
        if (ip == n){
          ip = 0;
        }
      }
      Pos p;
      I::get_position( pts_in, pts, ip, p);


    }

    chosen[ frame.ip] = false;
    if (frame.extra_bound){
      --ib;
    }



  }

  return std::make_pair( Pos(), Length());
}


//********************************************************************
template< class I>
void Doer<I>::get_smallest_enclosing_sphere( Points& pts_in,
                                          Position& center, Length& radius){

  typename I::Point_Refs pts{};
  I::initialize( pts_in, pts);
  Pos _center;
  get_smallest_enclosing_sphere_inner( pts_in, pts, _center, radius);
  center[0] = _center[0];
  center[1] = _center[1];
  center[2] = _center[2];  
}



//********************************************************************
template< class I>
void 
Doer<I>::get_smallest_enclosing_sphere_of_spheres( Points& pts_in,
                                                    Position& center, Length& radius){
  Point_Refs pts{};
  I::initialize( pts_in, pts);

  //cout << "smallest sphere\n";
  {
    auto n = I::size( pts_in);
    for( auto i: range( n)) {
      Pos p;
      I::get_position(pts_in, pts, i, p);
      //cout << r << ' ' << p << endl;
    }
  }

  Pos _center;
  get_smallest_enclosing_sphere_inner( pts_in,
                                           pts, _center, radius);
  auto n = I::size( pts_in);

  Length max_d = Length( 0.0);
  for( auto i: range( n)){
    Length r = I::radius( pts_in, pts, i);
    Pos p;
    I::get_position( pts_in, pts, i, p);
    Length d = r + distance( _center, p);
    if (d > max_d){
      max_d = d;
    }
  }
  
  center[0] = _center[0];
  center[1] = _center[1];
  center[2] = _center[2];    
  radius = max_d;
}

//********************************************************************
template< class I>
void Doer<I>::get_circumcircle( Pos p0, Pos p1, Pos p2,
                                 Pos& center, Length& radius){
 
  auto d1 = p1 - p0;
  auto d2 = p2 - p0;
  auto prp = cross( d1, d2);
  auto nrm = norm( prp);
  typedef decltype( Length()*Length()) Length2;
  
  const auto linf = Length( large);
  if (nrm == Length2(0.0)){ // collinear
    center[0] = linf;
    center[1] = linf;
    center[2] = linf;
    radius = linf;
  }
  else{
    auto perp = prp/sqrt( nrm);
    auto h1 = 0.5*d1;
    auto h2 = 0.5*d2;

    Mat3< Length> mat;
    mat[0][0] = d1[0];
    mat[0][1] = d1[1];
    mat[0][2] = d1[2];

    mat[1][0] = d2[0];
    mat[1][1] = d2[1];
    mat[1][2] = d2[2];

    mat[2][0] = perp[0];
    mat[2][1] = perp[1];
    mat[2][2] = perp[2];

    Vec3< Length2> b;
    Vec3< Length> relpos;
    b[0] = dot( h1, d1);
    b[1] = dot( h2, d2);
    b[2] = Length2( 0.0);

    Solve3::solve( mat, b, relpos);

    center = p0 + relpos;
    radius = distance( center, p0); 
  }
  
}



//********************************************************************
template< class I>
void Doer<I>::get_circumsphere( Pos p0, Pos p1, Pos p2, Pos p3,
                                 Pos& center, Length& radius){
  
  typedef decltype( Length()*Length()*Length()) Length3;
  typedef decltype( Length()*Length()) Length2;
  
  auto d1 = p1 - p0;
  auto d2 = p2 - p0;
  auto d3 = p3 - p0;
  
  auto h1 = 0.5*d1;
  auto h2 = 0.5*d2;
  auto h3 = 0.5*d3;
  
  auto h2ch3 = cross( h2,h3);
  const Length linf( large);
  if (dot( h1, h2ch3) == Length3( 0.0)){ // coplanar
    center[0] = linf;
    center[1] = linf;
    center[2] = linf;
    radius = linf;
  }
  else{
    Mat3< Length> mat;
    mat[0][0] = d1[0];
    mat[0][1] = d1[1];
    mat[0][2] = d1[2];

    mat[1][0] = d2[0];
    mat[1][1] = d2[1];
    mat[1][2] = d2[2];

    mat[2][0] = d3[0];
    mat[2][1] = d3[1];
    mat[2][2] = d3[2];

    Vec3< Length2> b; 
    Vec3< Length> relpos;
    b[0] = dot( h1, d1);
    b[1] = dot( h2, d2);
    b[2] = dot( h3, d3);

    Solve3::solve( mat, b, relpos);
    
    center = p0 + relpos;
    radius = distance( center, p0);
  }
}    

//********************************************************************
template< class T>
T avg( T t0, T t1){
  return 0.5*(t0 + t1);
}

template< class I>
void Doer<I>::get_smallest_triangle_sphere( Pos p0, Pos p1, Pos p2,
                                   Pos& center, Length& radius){

  auto d01 = p1 - p0;
  auto d12 = p2 - p1;
  auto d20 = p0 - p2;

  Length r01 = norm( d01);
  Length r12 = norm( d12);
  Length r20 = norm( d20);

  Length maxr = max( max( r01, r12), r20);
  
  Vec3< Length> trial_center;
  const Vec3< Length> *rem_pt_ptr = nullptr;
  if (r01 == maxr){
    trial_center = avg( p0,p1);
    rem_pt_ptr = &p2;
  }
  else if (r12 == maxr){
    trial_center = avg( p1,p2);
    rem_pt_ptr = &p0;
  }
  else{
    trial_center = avg( p2,p0);
    rem_pt_ptr = &p1;
  }
  const Vec3< Length>& rem_pt = *rem_pt_ptr;

  Length trial_radius = 0.5*maxr;
  
  if (distance( rem_pt, trial_center) <= trial_radius){
    center = trial_center;
    radius = trial_radius;
  }
  else{
    get_circumcircle( p0, p1, p2, center, radius);
  }

  {
    // adjust in case round-off error keeps points outside 
    Length d0 = distance( center, p0);
    Length d1 = distance( center, p1);
    Length d2 = distance( center, p2);
    radius = max( max( d0, d1), d2); 
  }
}

//********************************************************************
template< class I>
void Doer<I>::get_smallest_tetrahedron_sphere( 
                                     Pos p0, Pos p1, Pos p2, Pos p3,
                                     Pos& center, Length& radius){

  Array< Pos, 4> ps;
  ps[0] = p0;
  ps[1] = p1;
  ps[2] = p2;
  ps[3] = p3;

  int pair_ij[2] = {1,0};
  Length max_pd = distance( p0, p1);
  for( int i = 0; i<=3; i++){
    const Vec3< Length>& pi = ps[i];
    for( int j = 0; j <= i-1; j++){
      const Vec3< Length>& pj = ps[j];
      Length d = distance( pi, pj);
      if (d > max_pd){
        max_pd = d;
        pair_ij[0] = i;
        pair_ij[1] = j;
      }
    }
  }
  int i = pair_ij[0];
  int j = pair_ij[1];
  
  auto trial_center = avg( ps[i], ps[j]);
  Length trial_radius = max( distance( ps[i], trial_center),
                             distance( ps[j], trial_center));

  int rem_i, rem_j;
  if((i == 1) && (j == 0))
    {rem_i = 2; rem_j = 3;}
  else if ((i == 2) && (j == 0))
    {rem_i = 1; rem_j = 3;}
  else if ((i == 3) && (j == 0))
    {rem_i = 1; rem_j = 2;}
  else if ((i == 2) && (j == 1))
    {rem_i = 0; rem_j = 3;}
  else if ((i == 3) && (j == 1))
    {rem_i = 0; rem_j = 2;}
  else if ((i == 3) && (j == 2))
    {rem_i = 0; rem_j = 1;}
  else{
    rem_i = -1;
    rem_j = -1;
    I::error( "get_smallest_tetrahedron_sphere: internal error");
  }  

  if (distance( ps[ rem_i], trial_center) <= trial_radius &&
      distance( ps[ rem_j], trial_center) <= trial_radius){
    center = trial_center;
    //L::copy( trial_center, center);
    radius = trial_radius;
  }
  else{
    // try triangles now
    Vec3< Length> c0, c1, c2, c3;
    Length r0, r1, r2, r3;

    get_smallest_triangle_sphere( p1,p2,p3, c0,r0);
    get_smallest_triangle_sphere( p0,p2,p3, c1,r1);
    get_smallest_triangle_sphere( p0,p1,p3, c2,r2);
    get_smallest_triangle_sphere( p0,p1,p2, c3,r3);

    Length maxr = max (max (max( r0,r1), r2), r3);

    const Vec3< Length> *rem_pt_ptr = nullptr;
    Vec3< Length> *trial_center_ptr = nullptr;

    if (maxr == r0){
      trial_center_ptr = &c0;
      rem_pt_ptr = &p0;
    }
    else if (maxr == r1){
      trial_center_ptr = &c1;
      rem_pt_ptr = &p1;
    }
    else if (maxr == r2){
      trial_center_ptr = &c2;
      rem_pt_ptr = &p2;
    }
    else if (maxr == r3){
      trial_center_ptr = &c3;
      rem_pt_ptr = &p3;
    }
    else{
      I::error( "get_smallest_tetrahedron_sphere: internal error 2"); 
    }
    const auto& rem_pt = *rem_pt_ptr;
    const auto& ltrial_center = *trial_center_ptr;

    if (distance( rem_pt, ltrial_center) <= maxr){
      center = ltrial_center;
      //L::copy( trial_center, center);
      radius = maxr;
    }
    else{
      get_circumsphere( p0,p1,p2,p3, center, radius);
    }
  }

  {
    // adjust in case round-off error keeps points outside 
    auto d0 = distance( center, p0);
    auto d1 = distance( center, p1);
    auto d2 = distance( center, p2);
    auto d3 = distance( center, p3);
    radius = max( max( max( d0,d1),d2),d3);
  }
}
  
template< class I>
bool Doer<I>::stest( Pos origin, Pos perp, Pos vert, Pos p){

  auto pmorigin = p - origin;
  auto vertmorigin = vert - origin;
  return dot( perp, pmorigin)*dot( perp, vertmorigin) > Length6( 0.0);
}

//********************************************************************
template< class I>
bool Doer<I>::inside_tetrahedron( Pos p0, Pos p1, Pos p2, Pos p3, Pos p){
  
  auto d01 = p1 - p0;
  auto d02 = p2 - p0;
  auto d03 = p3 - p0;
  auto d12 = p2 - p1;
  auto d13 = p3 - p1;
  
  auto perp3 = cross( d01, d02);
  auto perp2 = cross( d01, d03);
  auto perp1 = cross( d02, d03);
  auto perp0 = cross( d12, d13);

  return 
    stest( p1, perp0, p0, p) && stest( p0, perp1, p1, p) && 
    stest( p0, perp2, p2, p) && stest( p0, perp3, p3, p);
}

}


