/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that is called for we_simulation

#include "../input_output/release_info.hh"
#include "we_simulator.hh"
#include "simulation.hh"

namespace B = Browndye;

int main( int argc, char* argv[]){

  if (argc < 2)
    B::error( "we_simulation: need input file name");

  std::string file = argv[1];

  B::print_release_info();

  B::Simulation simulation( file);

  B::WE_Simulator wes( simulation.top_node, false);
  wes.run();
}
