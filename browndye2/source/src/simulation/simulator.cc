#include "simulator.hh"

namespace Browndye{
  
//######################################
typedef std::string String;
namespace JP = JAM_XML_Pull_Parser;

// constructor
Simulator::Simulator( const JP::Node_Ptr& node){

  check_input( node);

  auto tres = value_from_node< Thread_Index>( node, "n_threads");
  auto nthreads = tres ? *tres : Thread_Index( 1u);

  auto sres = value_from_node< uint32_t>( node, "seed");
  auto iseed =
    sres ? *sres : clock();

  Node_Ptr mp_node = checked_child( node, "system");
  common = std::make_unique< System_Common>( mp_node);
    
  constexpr size_t ns = 624;
  
  std::seed_seq sseq = {iseed};
  std::mt19937_64 gen( sseq);
  std::uniform_int_distribution< uint32_t> unif;

  std::set< Vector< uint32_t> > seeds;
  mthreads.reserve( nthreads);
  for( auto i: range( nthreads)){
    Vector< uint32_t> seed( ns);
    do {
      for( auto& subseed: seed){
    	subseed = unif( gen);
      }
    }
    while( seeds.find( seed) != seeds.end());
    mthreads.emplace_back( *common, i, seed);
  }

  extra_seed.resize( ns);
  do {
    for( auto& subseed: extra_seed){
      subseed = unif( gen);
    }
  }
  while( seeds.find( extra_seed) != seeds.end());
      
  output_name = checked_value_from_node< std::string>( node, "output");
}

//#############################################################
void Simulator::begin_output( std::ofstream& outp){
  
  outp << "<rates>\n";
  common->output( outp);
}
  
 //##############################################################
Thread_Index Simulator::thread_index( Sys_Copy_Index icp) const{
  
  auto nth = n_threads();
  auto kcp = icp/Sys_Copy_Index(1);
  auto kth = nth/Thread_Index(1);
  return Thread_Index{ kcp % kth};
}
 
}