(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*

  Generates grid of 1's or 0's denoting the inside and outside of the molecule. If the lower corner and spacing are not 
  specified, reasonable default values are computed.

  The following arguments with flags are used:

  -spheres : name of xml file with sphere data
  -surface : name of file output from surface_spheres; has surface sphere information
  -corner" : sets lower corner of exclusion grid
  -ngrid"  : set number of grid points in each direction (default 100 100 100)
  -spacing : set grid point spacing
  -exclusion_distance : set size of exclusion distance from spheres (default: probe radius)
  -egrid : uses dx file as template for grid size and spacing 

  This algorithm constructs a cage of triangles connecting the surface sphere centers. If a point
  is not overlapped by a sphere, it is tested to see whether it is inside the cage by counting the number
  of intersections with triangles with a line segment connecting the point with an outside point.
  A tree-like data structure is used to reduce the number of triangles that must be checked for
  intersections with the line segment.
*)


let pr = Printf.printf;;
let len = Array.length;;

module H = Hashtbl;;

module Int = 
struct
  type t = int
  let compare x y = compare x y
end

module Indices = Set.Make( Int);;

open Vec3;;

module GP = Grid_parser;;

let error msg = 
  raise (Failure msg)
;;

let sq2 = sqrt 2.0;;

let sq x = x*.x;;

let print3 a = 
  Printf.printf "%f %f %f\n" a.x a.y a.z
;;

let some x = 
  match x with
    | Some y -> y
    | None -> error "some: is nothing"


let normed_diff a b = 
  let diff = vdiff a b in
  let nrm = vnorm diff in
  vscaled (1.0/.nrm) diff
;;

let normed a = 
  let nrm = vnorm a in
  vscaled (1.0/.nrm) a
;;

let contactable a2pos a2r cr pr = 
  let x,y,z = a2pos in
  let r = sqrt (x*.x +. y*.y) in
  ((sq( r -. cr)) +. z*.z) < (sq (a2r +. pr))

;;    

open Is_inside_spheres;;

let rec tree_size tree = 
  match tree with
    | Empty -> 0
    | Leaf lf -> 1
    | Branch br ->
      let c0,c1 = br.children in
      (tree_size c0) + (tree_size c1)
;;


let array_fold12 f g items = 
  let res = ref (f items.(0)) in
  let n = Array.length items in
  for i = 1 to n-1 do
    let item = items.(i) in
    res := g !res (f item)
  done;
  !res
;;

module Vec3 = 
struct
  type t = Vec3.t
  type reff = int
  type container = Vec3.t array
  let v3 = v3
  let xyz v = v.x, v.y, v.z
  let sum = vsum
  let diff = vdiff
  let scaled = vscaled
  let dot = dot
  let cross = cross
  let distance = distance
  let position arr i = arr.(i)
  let to_array arr = Array.init (Array.length arr) (fun i -> i)
end

module Spheres = Circumspheres.M( Vec3);;

let rec search_tree coords radius items = 

  let n = Array.length items in

  if n == 1 then
    let item = items.(0) in
    Leaf {
      child = item;
      lpos = coords item;
      lradius = radius item;
    }
  else (
    let pts = Array.map coords items in
    let center, max_rp = 
      Spheres.smallest_enclosing_sphere pts
    in

    let max_item, max_r = 
      array_fold12
	(fun item -> item, (distance center (coords item)) +. (radius item))
	(fun res0 res1 ->
	  let item0, r0 = res0
	  and item1, r1 = res1
	  in
	  if r0 > r1 then
	    res0
	  else
	    res1
	)
	items
    in
    let axis = normed_diff (coords max_item) center in
    
    let sitems = 
      Array.init n (fun i -> items.(i)) 
    in
    
    Array.sort
      (fun item0 item1 ->
	let proj item = 
	  dot (vdiff (coords item) center) axis
	in
	compare (proj item0) (proj item1)
      )
      sitems;
    
    let nleft, nright = 
      let hn = n/2 in
      if (n mod 2) = 0 then
	hn,hn
      else
	hn+1, hn
    in
    
    let low_items = 
      Array.init 
	nleft 
	(fun i -> sitems.(i))
    and high_items = 
      Array.init
	nright
	(fun i -> sitems.(i+nleft))
    in

    let low_tree = search_tree coords radius low_items
    and high_tree = search_tree coords radius high_items
    in
    Branch {
      bpos = center;
      bradius = max_r;
      children = low_tree, high_tree;
    }
  )
;;

let two_pi = 2.0*.3.1415926;;

let fold12 f g items = 
  match items with
    | head :: tail ->
      List.fold_left
	(fun res item ->
	  g res (f item)
	)
	(f head)
	tail
    | [] -> 
      error "fold12: nothing in list"
;;

let fold12_lol f g lol = 
  fold12 
    (fold12 f g)
    g
    lol
;;

let ordered (i:int) (j:int) = 
  if i < j then
    i,j
  else
    j,i
;;

(* a0,a1,a2 are clockwise as viewed from outside *)
let fmin (a:float) (b:float) = 
  if a < b then a else b
;;

type  'a problem = {
  ubound: float;
  ptree: 'a tree;
}
;;

let closest_item_t idistance tre pos =

  let module H = Heap in 

  let bounds tre = 
    match tre with
      | Empty -> raise (Failure "closest_item_t: should not get here")
      | Leaf leaf ->
	let d = idistance leaf.child pos in
	d, d
      | Branch branch ->
	let d = distance pos branch.bpos in
	d -. branch.bradius, d +. branch.bradius
  in
  let ucompare t0 t1 = 
    -(compare t0.ubound t1.ubound) 
  in

  let queue = H.created ucompare 10 in
  
  let add_to_queue ubound tre =
    let lb, ub = bounds tre in
    if lb < ubound then (
      let pr = {
	ptree = tre;
	ubound = ub;
      }
      in
      H.add queue pr
    )
  in

  let rec loop ubound best_item =
    if H.is_empty queue then (
      best_item
    )
    else
      let pr = H.popped_max queue in
      match pr.ptree with
	| Empty -> raise (Failure "closest_item_t loop: should not get here")
	| Leaf leaf ->
	  let rdist = (distance leaf.lpos pos) -. leaf.lradius in
	  if rdist < ubound then
	    let item = leaf.child in
	    let dist = idistance item pos in
	    if dist < ubound then
	      loop dist (Some item) 
	    else
	      loop ubound best_item 
	  else
	    loop ubound best_item

	| Branch branch ->
	  let distc = distance pos branch.bpos 
	  and br = branch.bradius in
	  if distc -. br > ubound then
	    loop ubound best_item
	  else (
	    let new_ubound = fmin ubound (distc +. br) in
	    let child0, child1 = branch.children in
	    add_to_queue ubound child0;
	    add_to_queue ubound child1;
	    loop new_ubound best_item 
	  )
  in
  add_to_queue infinity tre;
  some (loop infinity None)
;;

(* p0 to left, p1 to right, q0 straight above p0-p1, q1 below.  
   Returns 1 if q0-q1 passes above p0-p1, -1 if to left, and 0 if
   through *)
let line_passing p0 p1 q0 q1 = 
  let d0 = vdiff p0 q0
  and d1 = vdiff p1 q0 in
  let cp = cross d1 d0 in
  let dd = vdiff q1 q0 in
  compare (dot dd cp) 0.0  
;;

let triangle_center spheres tri = 
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).spos
  and p1 = spheres.(i1).spos
  and p2 = spheres.(i2).spos
  in
  let center,r = Spheres.circumcircle p0 p1 p2 in
  center
;;

let triangle_size spheres tri = 
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).spos
  and p1 = spheres.(i1).spos
  and p2 = spheres.(i2).spos in
  let center,r = Spheres.circumcircle p0 p1 p2 in
  r
;;

type tri_contact = 
  | Plane
  | Vertex of int
  | Edge of int*int
;;

let triangle_contact spheres tri pos =
  let i0,i1,i2 = tri in
  let p0 = spheres.(i0).spos
  and p1 = spheres.(i1).spos
  and p2 = spheres.(i2).spos
  in
  
  let d01 = vdiff p1 p0
  and d12 = vdiff p2 p1
  and d20 = vdiff p0 p2
  in
  let perp = cross d01 d12 in
  let p01 = cross perp d01  
  and p12 = cross perp d12
  and p20 = cross perp d20 
  in
  if 
    (dot (vdiff pos p0) p01) > 0.0 &&
      (dot (vdiff pos p1) p12) > 0.0 &&
      (dot (vdiff pos p2) p20) > 0.0 
  then ( (* on triangle *)
    let nperp = normed perp in

    (abs_float (dot (vdiff pos p0) nperp)), Plane
  )
  else
    
    let dist_to_line p0 p1 = 
      let d01 = vdiff p1 p0 in
      let nd01 = vnorm d01 in
      let axis = vscaled (1.0/.nd01) d01 in
      let d0 = vdiff pos p0 in
      let proj = dot d0 axis in

      if proj > nd01 || proj < 0.0 then
	None
      else
	Some (distance d0 (vscaled proj axis))
    in
    let min_line_dist = 
      List.fold_left
	(fun res ldistij ->
	  let ldist, ij = ldistij in
	  match ldist with
	    | None -> res
	    | Some d -> 
	      let old_d, old_ij = res in
	      match old_d with
		| None -> (Some d), ij
		| Some old_d ->
		  if d < old_d then
		    (Some d), ij
		  else
		    res
	)
	(None,(-1,-1))
	[ (dist_to_line p0 p1), ordered i0 i1; 
	  (dist_to_line p1 p2), ordered i1 i2; 
	  (dist_to_line p2 p0), ordered i2 i0;] 
    in

    let min_vert_dist = 
      let d0 = distance p0 pos
      and d1 = distance p1 pos
      and d2 = distance p2 pos
      in
      let min_d = fmin (fmin d0 d1) d2 in
      if min_d = d0 then
	d0, i0
      else if min_d = d1 then
	d1, i1
      else
	d2, i2
    in
    
    let med, edge = min_line_dist 
    and mvd, vertex = min_vert_dist in

    match med with
      | None ->
	mvd, (Vertex vertex);

      | Some d -> 
	if d < mvd then
	  let j0,j1 = edge in
	  d, (Edge (j0, j1))
	else
	  mvd, (Vertex vertex)

;;

let triangle_distance spheres tri pos =
  let d, contact = triangle_contact spheres tri pos in
  d
;;

let is_in items item = 
  if (len items) = 0 then
    false
  else
    let rec recurse il ih = 
      if ih - il = 0 then
	il
      else
	let im = (ih + il)/2 in
	if item = items.(im) then
	  im
	else if item < items.(im) then
	  recurse il im
	else
	  recurse (im+1) ih
    in
    let im = recurse 0 ((len items)-1) in
    item = items.(im) 
	
;;

let new_tri_search_tree triangles spheres = 

  search_tree
    (fun tri ->
      triangle_center spheres tri
    )
    (fun tri ->
      triangle_size spheres tri
    )
    triangles
;;

(* generates well-separated points *)
let coord_generator nx ny nz = 
  let next_pow2 n = 
    let rec loop ibt = 
      if ibt < n then
	loop (ibt lsl 1)
      else
	ibt
    in
    loop 1
  in
  let n = max (max (next_pow2 nx) (next_pow2 ny)) (next_pow2 nz) in

  let log2 = 
    let rec loop i = 
      if (1 lsl i) < n then
	loop (i+1)
      else
	i
    in
    loop 1
  in
  let tlog2m1 = 3*log2 - 1 in
  let with_rev_bits n = 
    let rec loop res ibt = 
      if ibt > tlog2m1 then
	res
      else (
	let new_res = 
	  if ((1 lsl (tlog2m1 - ibt)) land n) > 0 then
	    res lor (1 lsl ibt)
	  else
	    res
	in
	loop new_res (ibt+1)
      )
    in
    loop 0 0
  in
  let pulled_apart n =
    let rec loop res ibt =
      if ibt <= tlog2m1 then
	let jbt = ibt/3 in
	let oand a b = if (a land b) > 0 then 1 else 0 in
	let mask0 = 1 lsl ibt
	and mask1 = 1 lsl (ibt+1)
	and mask2 = 1 lsl (ibt+2)
	in
	let new_res res mask = 
	  res lor ((oand mask n) lsl jbt)
	in
	let res0,res1,res2 = res in
	let nres0 = new_res res0 mask0
	and nres1 = new_res res1 mask1
	and nres2 = new_res res2 mask2
	in
	loop (nres0,nres1,nres2) (ibt + 3)
      else
	res
    in
    loop (0,0,0) 0
  in
  let i = ref 0 in
  (fun () -> 
    let rec loop () = 
      let ix,iy,iz = pulled_apart (with_rev_bits !i) in
      i := !i + 1;
      if ix < nx && iy < ny && iz < nz then
	ix,iy,iz
      else
	loop ()
    in
    loop()
  )
;;

let rot_to_min i0 i1 i2 = 
  if i0 < i1 && i0 < i2 then
    i0,i1,i2
  else if i1 < i0 && i1 < i2 then
    i1,i2,i0
  else
    i2,i0,i1
;;

let array_3d nx ny nz a = 
  Array.init nx
    (fun i ->
      Array.init ny 
	(fun i ->
	  Array.init nz
	    (fun i -> a)))
;;

type grid_type = 
  | Known of bool
  | Unknown
;;

(* later, fix to use method from reduced_surface for finding center *)
let center_and_radius low_corner widthx widthy widthz nx ny nz = 
  let domain_radius =
    let fx,fy,fz = widthx*.(float nx), widthy*.(float ny), widthz*.(float nz) in
    0.55*.(sqrt (fx*.fx +. fy*.fy +. fz*.fz));
  in
  let center = 
    let offset = {
      x = 0.5*.(float nx)*.widthx;
      y = 0.5*.(float ny)*.widthy;
      z = 0.5*.(float nz)*.widthz;
    }
    in vsum low_corner offset
  in
  center, domain_radius
;;

let interior_grid_points 
    spheres surface_indices triangles probe_radius ex_radius
    low_corner widthx widthy widthz nx ny nz = 

  let has_triangles = (len triangles) > 0 in

  let center, domain_radius = center_and_radius low_corner widthx widthy widthz nx ny nz in 

  let tri_search_tree = 
    if has_triangles then
      Some (new_tri_search_tree triangles spheres)
    else
      None
  in
  let surface_spheres = 
    Array.map
      (fun i -> spheres.(i))
      surface_indices
  in
  let closest_triangle = 
    match tri_search_tree with
      | Some tst -> 
	(fun pos ->
	  closest_item_t
	    (fun tri x ->
	      triangle_distance spheres tri x 
	    )
	    tst
	    pos
	)
      | None ->
	(fun pos -> raise (Failure "closest_triangle: no triangles"))
	  
  in
  let coords = coord_generator nx ny nz in

  let sphere_part = partitioned_spheres surface_spheres in
  let sphere_search_tree = sphere_part.tree in
  let grid_map = array_3d nx ny nz Unknown in
  let lx,ly,lz = low_corner.x, low_corner.y, low_corner.z in
  let pt = v3 0.0 0.0 0.0 in

  for iix = 0 to nx-1 do
    for iiy = 0 to ny-1 do
      for iiz = 0 to nz-1 do
	let ix,iy,iz = coords () in  
	let x = lx +. (float ix)*.widthx in 
	let y = ly +. (float iy)*.widthy in
	let z = lz +. (float iz)*.widthz in
	match grid_map.(ix).(iy).(iz) with
	  | Known tf -> ()
	    
	  | Unknown -> (
	    pt.x <- x;
	    pt.y <- y;
	    pt.z <- z;
	    let is_completely_inside =
	      if has_triangles then
		let test_pt =
		  let d = vdiff pt center in
		  let dnrm = vnorm d in
		  vsum center (vscaled (domain_radius/.dnrm) d)
		in 
		is_inside_spheres (some tri_search_tree) spheres test_pt pt 
	      else
		false
	    in
	    
	    let dist = 
	      if is_completely_inside then		      	       
		let tri = closest_triangle pt in
		let dist, contact = triangle_contact spheres tri pt in
		dist
	      else
		let sphere = 
		  closest_item_t
		    (fun sph pt -> (distance pt sph.spos) -. sph.radius)
		    sphere_search_tree
		    pt
		in
		(distance sphere.spos pt) -. (ex_radius +. sphere.radius)
	    in
	    
	    let is_inside = is_completely_inside || (dist < 0.0) in
	    
	    let bounded n m = 
	      if m < 0 then 
		0 
	      else if m >= n then
		n-1
	      else
		m
	    in
	    let bounded2 n m0 m1 = 
	      bounded n m0, bounded n m1
	    in	    

	    let nr width = 
	      max 0 (int_of_float (dist/.width)) 
	    in
	    let nrx,nry,nrz = nr widthx, nr widthy, nr widthz in

	    let mx0,mx1 = bounded2 nx (ix - nrx) (ix + nrx)
	    and my0,my1 = bounded2 ny (iy - nry) (iy + nry)
	    and mz0,mz1 = bounded2 nz (iz - nrz) (iz + nrz) in
	    for jx = mx0 to mx1 do
	      let xx = lx +. (float jx)*.widthx in
	      for jy = my0 to my1 do
		let yy = ly +. (float jy)*.widthy in
		for jz = mz0 to mz1 do
		  let zz = lz +. (float jz)*.widthz in
		  let r = sqrt 
		    ((sq (xx -. x)) +. 
			(sq (yy -. y)) +. 
			(sq (zz -. z)))
		  in
		  if r < dist then
		    grid_map.(jx).(jy).(jz) <- Known is_inside;
		done;
	      done;
	    done;
	    grid_map.(ix).(iy).(iz) <- Known is_inside;
	  );
	    
      done;
    done;
  done;
  grid_map
;;

let spheres_file = ref "";;
let red_file = ref "";;
let ex_radius_ref = ref nan;;

let compute_grid = ref false;;
let cornerx = ref nan;;
let cornery = ref nan;;
let cornerz = ref nan;;
let vsizex = ref nan;;
let vsizey = ref nan;;
let vsizez = ref nan;;
let ngridx = ref 100;;
let ngridy = ref 100;;
let ngridz = ref 100;;
let egrid_ref = ref "";;


Arg.parse 
  [("-spheres", (Arg.Set_string spheres_file), ": name of xml file with sphere data");
   ("-surface", (Arg.Set_string red_file), ": name of file output from surface_spheres; has surface sphere information");

   ("-corner", 
    (Arg.Tuple 
       [(Arg.Set_float cornerx); 
	(Arg.Set_float cornery); 
	(Arg.Set_float cornerz);]),
    ": sets lower corner of exclusion grid"
   );
   ("-ngrid",
    (Arg.Tuple 
       [(Arg.Set_int ngridx); 
	(Arg.Set_int ngridy); 
	(Arg.Set_int ngridz);]),
    ": set number of grid points in each direction (default 100 100 100)"
   );
   ("-spacing", 
    (Arg.Tuple 
       [(Arg.Set_float vsizex); 
	(Arg.Set_float vsizey); 
	(Arg.Set_float vsizez);]),
    ": set grid point spacing"
   );

   ("-exclusion_distance", (Arg.Set_float ex_radius_ref), 
    ": set size of exclusion distance from spheres (default: probe radius)"
   );

   ("-egrid", (Arg.Set_string egrid_ref), ": uses dx file as template for grid size and spacing"); 

  ] 
  (fun a -> 
    Printf.printf "needs args\n";
  )
  "Generates grid of 1's or 0's denoting the inside and outside of the molecule\n. If the lower corner and spacing are not specified, reasonable default values are computed.\n"
;;

let is_nan x = 
  match classify_float x with
    | FP_nan -> true
    | _ -> false
;;


let appended_arrays a b = 
  let na, nb = len a, len b in
  Array.init (na + nb)
    (fun i ->
      if i < na then 
	a.(i)
      else
	b.(i-na)
    )
;;

module JP = Jam_xml_ml_pull_parser;;

let checked_node_of_tag parser tag = 
  match JP.node_of_tag parser tag with
    | None -> error ("no tag of "^tag);
    | Some node -> node
;;

let checked_child node tag = 
  match JP.child node tag with
    | None -> error ("no tag of "^tag);
    | Some cnode -> cnode
;;

let do_xyzr () =
  let sphere_list = ref [] in
  let i = ref 0 in

  let low_x, low_y, low_z = ref infinity, ref infinity, ref infinity 
  and high_x, high_y, high_z = 
    ref neg_infinity, ref neg_infinity, ref neg_infinity
  in

  let put_sphere ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
      ~x:x ~y:y ~z:z ~radius:r ~charge:q = 
    low_x := min (x -. r) !low_x;
    low_y := min (y -. r) !low_y;
    low_z := min (z -. r) !low_z;

    high_x := max (x +. r) !high_x;
    high_y := max (y +. r) !high_y;
    high_z := max (z +. r) !high_z;

    let sphere = {
      spos = v3 x y z;
      radius = r;
      number = !i;
      active = false;
      actual_number = anum;
    }
    in
    i := !i + 1;
    sphere_list :=  sphere :: (!sphere_list);
  in
  (
    let fp = Scanf.Scanning.from_file !spheres_file in
    Atom_parser.apply_to_atoms ~buffer: fp ~f: put_sphere;
  );

  let spheres = Array.of_list (List.rev !sphere_list) in
  
  let parser = JP.new_parser (Scanf.Scanning.from_file !red_file) in
  
  let float_of_tag tag = 
    let node = checked_node_of_tag parser tag in
    JP.complete_current_node parser;
    let stm = JP.node_stream node in
    JP.float_from_stream stm
      
  and int_of_tag node tag = 
    let cnode = checked_child node tag in
    let stm = JP.node_stream cnode in
    JP.int_from_stream stm
  in        
  let probe_radius = float_of_tag "probe_radius" in 
  
  let ex_radius = 
    if is_nan !ex_radius_ref then
      probe_radius
    else
      !ex_radius_ref 
  in 
  let actual_indices = 
    let tbl = H.create 0 in
    Array.iter
      (fun sph -> 
	if H.mem tbl sph.actual_number then
	  let msg =
	    Printf.sprintf "Duplicate sphere number %d" sph.actual_number in
	  error msg
	else
	  H.add tbl sph.actual_number sph.number
      )
      spheres;
    tbl
  in
  let trios =       
    ignore (JP.node_of_tag parser "trios");
    let unnormed_trios = 
      let trio_list = ref [] in
      JP.apply_to_nodes_of_tag parser "trio"
      (fun node ->
	JP.complete_current_node parser;
	let stm = JP.node_stream node in
	let i0 = JP.int_from_stream stm in
	let i1 = JP.int_from_stream stm in
	let i2 = JP.int_from_stream stm in
	trio_list := (i0,i1,i2) :: (!trio_list)
      );
      Array.of_list (!trio_list)
    in
    
    Array.map
      (fun trio -> 
	let ai = H.find actual_indices in
	let i0,i1,i2 = trio in
	(ai i0), (ai i1), (ai i2) 
      )
      unnormed_trios
  in
  let spnode = checked_node_of_tag parser "spheres" in
  JP.complete_current_node parser;
  let indices tag = 
    let snode = checked_child spnode tag in
    
    let n = int_of_tag snode "n" in
    let sarray = Array.init n (fun i -> 0) in
    if n > 0 then (
      let inode = checked_child snode "indices" in
      let stm = JP.node_stream inode in
      for i = 0 to n-1 do
	sarray.(i) <- H.find actual_indices (JP.int_from_stream stm)
      done;
    );
    sarray
  in  
  let ssindices = indices "surface" in
  let dsindices = indices "danglers" in

  let egrid = !egrid_ref in
  if not (egrid = "") then (
    let buf = Scanf.Scanning.from_file egrid in
    let ginfo = GP.grid_info buf in
    let nx,ny,nz = ginfo.GP.n in
    ngridx := nx;
    ngridy := ny;
    ngridz := nz;
    let c = ginfo.GP.low in
    let hx,hy,hz = ginfo.GP.h in
    cornerx := c.x;
    cornery := c.y;
    cornerz := c.z;
    vsizex := hx;
    vsizey := hy;
    vsizez := hz;
  )	    
  else	   
    (* only ngridx, ngridy, ngridz are known *)
    if is_nan !cornerx then (
      let max_radius = Array.fold_left (fun res sph -> max res sph.radius) 0.0 spheres in

      let skin = 1.1 in
      let er = skin*.(ex_radius +. max_radius) in 
      cornerx := !low_x -. er;
      cornery := !low_y -. er;
      cornerz := !low_z -. er;
      
      let hcornerx = !high_x +. er 
      and hcornery = !high_y +. er
      and hcornerz = !high_z +. er
      in
      let hx = (hcornerx -. !cornerx)/.(float_of_int (!ngridx-1))
      and hy = (hcornery -. !cornery)/.(float_of_int (!ngridy-1))
      and hz = (hcornerz -. !cornerz)/.(float_of_int (!ngridz-1))
      in
      vsizex := hx;
      vsizey := hy;
      vsizez := hz;
    );

  let corner = v3 !cornerx !cornery !cornerz in	    
  (* check bounds *)
  (
    let hx,hy,hz = !vsizex, !vsizey, !vsizez in
    let nx,ny,nz = !ngridx,!ngridy,!ngridz in
    let lowx,lowy,lowz = !cornerx, !cornery, !cornerz in
    let highx = lowx +. (float (nx-1))*.hx
    and highy = lowy +. (float (ny-1))*.hy
    and highz = lowz +. (float (nz-1))*.hz
    in
    Array.iter 
      (fun sphere -> 
	let r = sphere.radius +. ex_radius in
	let lx = sphere.spos.x -. r
	and ly = sphere.spos.y -. r
	and lz = sphere.spos.z -. r
	in
	let hx = sphere.spos.x +. r
	and hy = sphere.spos.y +. r
	and hz = sphere.spos.z +. r
	in
	if lx < lowx || ly < lowy || lz < lowz ||
	     hx > highx || hy > highy || hz > highz then (

          Printf.fprintf stderr "full radius %f\n" r;
          Printf.fprintf stderr "pos %f %f %f\n" sphere.spos.x sphere.spos.y sphere.spos.z;
          
	  Printf.fprintf stderr 
	    "sphere %d is outside the grid bounds\n" sphere.actual_number;
	  error "out of bounds";
	)
      )
      spheres;
  );

  Printf.printf "<reduced_surface>\n";
  Printf.printf "  <probe_radius> %f </probe_radius>\n" probe_radius;
  Printf.printf "  <exclusion_radius> %f </exclusion_radius>\n" ex_radius;
  
  let all_ssindices = 
    appended_arrays ssindices dsindices
  in	      
  let grid = interior_grid_points spheres all_ssindices trios
    probe_radius ex_radius corner !vsizex !vsizey !vsizez !ngridx !ngridy !ngridz
    
  in
  Printf.printf "  <grid>\n";
  Printf.printf "    <corner> %f %f %f </corner>\n" !cornerx !cornery !cornerz;
  Printf.printf "    <npts> %d %d %d </npts>\n" !ngridx !ngridy !ngridz;
  Printf.printf "    <spacing> %f %f %f </spacing>\n" !vsizex !vsizey !vsizez;
  Printf.printf "    <data>\n";
  
  for ix = 0 to !ngridx-1 do
    Printf.printf "      <plane>\n";
    for iy = 0 to !ngridy-1 do
      Printf.printf "        <row> ";
      for iz = 0 to !ngridz-1 do
	let c = 
	  match grid.(ix).(iy).(iz) with
	    | Unknown -> error "undefined grid point"
	    | Known is_inside ->
	      if is_inside then 1 else 0 
		    (* if is_inside then '#' else ' ' *) 
	in
	Printf.printf "%d " c; 
		  (* Printf.printf "%c" c;  *)
      done; 
      Printf.printf "</row>\n";	      
    done;
    Printf.printf "      </plane>\n\n";
  done;	  
  Printf.printf "    </data>\n";
  Printf.printf "  </grid>\n";	      
  Printf.printf "</reduced_surface>\n";
;;

do_xyzr();;  



