#pragma once
/*
 * sfield_interface.hh
 *
 *  Created on: Oct 14, 2015
 *      Author: root
 */



#include "single_grid.hh"

namespace Browndye{

template< class I>
class SField_Interface{
public:
  typedef typename I::Potential Potential;
  typedef Single_Grid< Potential> SGrid;
  typedef typename I::Potential_Gradient Potential_Gradient;
  typedef decltype( Potential_Gradient()*Potential_Gradient()) EE;
  typedef decltype( EE()/Length()) EE_Grad;

  typedef SGrid Contained;
  typedef Vec3< Length> Point;

  static
  bool ioverlap( Length a, Length b, Length x, Length y);
  
  static
  bool overlap( const SGrid& grida, const SGrid& gridb);
  
  static
  bool iencloses( Length a, Length b, Length x, Length y);

  static
  bool encloses( const SGrid& grid0, const SGrid& grid1);
 
  static
  bool between( Length a, Length x, Length b);

  static
  bool is_inside( Vec3< Potential_Gradient>& grad,
                  const Point& pos, const SGrid& grid);
    
  static
  bool is_inside( Vec3< EE_Grad>& grad, const Point& pos, const SGrid& grid);  
    
  static
  bool is_inside( Potential& v,
                  const Point& pos, const SGrid& grid);
  
  static
  void error( const std::string& msg1);
};

//#########################################################################
template< class I>
bool SField_Interface<I>::ioverlap( Length a, Length b, Length x, Length y){
  bool res =  (b > x) && (a < y);
  return res;
}

//#########################################################
template< class I>
bool SField_Interface<I>::overlap( const SGrid& grida, const SGrid& gridb){
  
  auto lowa = grida.low_corner();
  auto higha = grida.high_corner();
  auto lowb = gridb.low_corner();
  auto highb = gridb.high_corner();
  
  return
    ioverlap( lowa[0], higha[0], lowb[0], highb[0]) &&
    ioverlap( lowa[1], higha[1], lowb[1], highb[1]) &&
    ioverlap( lowa[2], higha[2], lowb[2], highb[2]);
}

//#########################################################
template< class I>
bool SField_Interface<I>::iencloses( Length a, Length b, Length x, Length y){
  return (a <= x) && (y <= b);
}

//#########################################################
template< class I>
bool SField_Interface<I>::encloses( const SGrid& grid0, const SGrid& grid1){

  auto low0 = grid0.low_corner();
  auto high0 = grid0.high_corner();
  auto low1 = grid1.low_corner();
  auto high1 = grid1.high_corner();

  bool in0 =  iencloses( low0[0], high0[0], low1[0], high1[0]);
  bool in1 =  iencloses( low0[1], high0[1], low1[1], high1[1]);
  bool in2 =  iencloses( low0[2], high0[2], low1[2], high1[2]);

  return in0 && in1 && in2;
}

//#########################################################
template< class I>
bool SField_Interface<I>::between( Length a, Length x, Length b){
  return a <= x && x < b;
}

//#########################################################
template< class I>
bool SField_Interface<I>::is_inside( Vec3< Potential_Gradient>& grad,
                const Point& pos, const SGrid& grid){
  
  auto gopt = grid.gradient( pos);
  if (gopt){
    grad = gopt.value();
    return true;
  }
  else{
    return false;
  }
}

//#########################################################
template< class I>
bool SField_Interface<I>::is_inside( Vec3< EE_Grad>& grad,
                const Point& pos, const SGrid& grid){
  
  auto gopt = grid.EE_gradient( pos);
  if (gopt){
    grad = gopt.value();
    return true;
  }
  else{
    return false;
  }
}

//#########################################################
template< class I>
bool SField_Interface<I>::is_inside( Potential& v,
                const Point& pos, const SGrid& grid){

  auto Vo = grid.potential( pos);
  if (Vo){
    v = Vo.value();
    return true;
  }
  else{
    return false;
  }
}

//#########################################################
template< class I>
void SField_Interface<I>::error( const std::string& msg1){
  const std::string msg0 = "Field: ";
  Browndye::error( msg0, msg1);
}


}

