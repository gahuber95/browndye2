/*
 * 
 * group_state.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: root
 */

#include "../lib/range.hh"
#include "group_common.hh"
#include "group_thread.hh"
#include "group_state.hh"
#include "../motion/constraint_tolerance.hh"
#include "../generic_algs/remove_duplicates.hh"
#include "group_constraint_interface.hh"
#include "../molsystem/system_thread.hh"
#include "bead_constrainer_setupper.hh"

namespace Browndye{

using std::map;
using std::move;
using std::make_pair;
using std::pair;

//##########################################################################
// constructor
Group_State::Group_State( const Group_Common& gcommon, Group_Thread& gthread, bool for_mob):
  
  thread_ref( gthread), common_ref{ gcommon}{
    
  auto& common = this->common();
  auto& scommon = gcommon.system;
  
  auto nm = common.cores.size();
  core_states.reserve( nm);
  for( auto im: range( nm)){
    core_states.emplace_back( gthread.core_threads[im], common.cores[im],
                             *this, scommon.groups, for_mob);
  }
  
  auto nc = common.chains.size();
  chain_states.reserve( nc);
  for( auto i: range( nc)){
    chain_states.emplace_back( common.chains[i], scommon.groups);
  }

  if (!for_mob){
    setup_beads_and_constraints( gthread);
    setup_cons_info_and_doer( gthread);
  
    auto& constrainer = cons_info_and_doer->doer;
    save( thread().current);
    
    auto& cinfo = cons_info_and_doer->cinfo;
    cinfo.initial_satisfaction = true;
    constrainer->satisfy_no_init( cinfo, constraint_solve_tol);
    cinfo.initial_satisfaction = false;
  }
}

//################################################
void Group_State::setup_cons_info_and_doer( Group_Thread& thread){
  auto& constrainers = thread.constrainers;
  auto frindex = constraint_index();
  auto itr = constrainers.find( frindex);
  if (itr != constrainers.end()){
    cons_info_and_doer = &(itr->second);
    cons_info_and_doer->cinfo.state = this;
  }
  else{
   Bead_Constrainer_Setupper bcs( thread, *this); 
   bcs.setup_new_cons_info();
  }
}

//############################################################
void Bead_Constrainer_Setupper::remeasure_tet_tet_cons(){
  
  auto& cinfo = state.cons_info_and_doer->cinfo;
  auto& tt_cons = cinfo.tet_tet_constraints;
  auto& lcomm = state.common();
  auto& tet_beads = lcomm.all_tet_beads;
  auto tdiff = cinfo.tdiff;
  
  for( auto& con: tt_cons){
    auto ib0 = con.beads[0];
    auto ib1 = con.beads[1];
    auto lb0 = tet_beads[ ib0 - tdiff];
    auto lb1 = tet_beads[ ib1 - tdiff];
    auto it0 = lb0.is;
    auto it1 = lb1.is;
    if (it0 != it1){
      error( __FILE__, __LINE__, "not get here");
    }
    auto& tet = state.tets[ it0];
    con.length = tet.fixed->length;
  }
}

//#############################################################
void Bead_Constrainer_Setupper::remeasure_tm_cons(){
   
  auto& cinfo = state.cons_info_and_doer->cinfo;
  auto& lcomm = state.common();
  auto& tet_beads = lcomm.all_tet_beads;
  auto tdiff = cinfo.tdiff;
  
  auto mdiff = cinfo.mdiff;
  auto& tm_cons = cinfo.tet_core_constraints;
  auto& lcore_beads = lcomm.all_core_beads;
  for( auto& con: tm_cons){
    auto ib0 = con.beads[0];
    auto ib1 = con.beads[1];
    auto lb0 = tet_beads[ ib0 - tdiff];
    
    auto it0 = lb0.is;
    auto& tet = state.tets[it0];
    auto ja = lb0.ia;
    auto tpos = tet.fixed->poses0[ja];
    
    auto lb1 = lcore_beads[ ib1 - mdiff];
    auto im1 = lb1.is;
    
    auto iia = lb1.ia;
    auto& core_state = core_states[im1];
    auto& core_comm = core_state.common();
    auto ia = core_comm.bound_atoms[ iia].ia;
    
    auto pos1 = core_comm.atoms[ia].pos;
    auto mpos = core_state.transformed( pos1);
    
    auto r = distance( tpos, mpos);
    con.length = r;
  }
}

//###########################################################
void Bead_Constrainer_Setupper::remeasure_tet_and_core_cons(){
  
  remeasure_tet_tet_cons();
  remeasure_tm_cons();
}

//########################################################
void Bead_Constrainer_Setupper::doit(){
  
  setup_coreless_chain();
  setup_related_chains();
  setup_free_cores();
  setup_tets();
  
  auto& constrainers = thread.constrainers;
  auto frindex = state.constraint_index();
  auto itr = constrainers.find( frindex);
  if (itr != constrainers.end()){
    state.cons_info_and_doer = &(itr->second);
    state.cons_info_and_doer->cinfo.state = &state;    
    remeasure_tet_and_core_cons();
  }
  else{
   setup_new_cons_info();
  }
}

//###############################################
void Bead_Constrainer_Setupper::setup_new_cons_info(){
  
  setup_min_core_of_tet();
  setup_min_chain_of_tet();
  
  Constraints_Info cons_info;
  setup_divs( cons_info);
  setup_beads( cons_info);
  setup_constraints( cons_info);
  
  // link it up
  auto frindex = state.constraint_index();
  auto& constrainers = thread.constrainers;
  Group_Thread::CID cid;
  cid.cinfo = std::move( cons_info);
  auto [itr, success] =
    constrainers.insert( make_pair( frindex, std::move( cid)));
  
  if (!success){
    error( "not get here", __FILE__, __LINE__);
  }
  
  auto* cons_info_and_doer = &(itr->second);
  
  state.cons_info_and_doer = cons_info_and_doer;
  
  auto& cidoer = itr->second;
  cidoer.cinfo.state = &state;
  cidoer.cinfo.thread = &thread;
  
  //typedef Group_Constraint_Interface GC_Interface;
  //typedef Chain_Constraint_Satisfier::Doer< GC_Interface> CDoer;
  cidoer.doer = std::make_unique< Constrainer>( cidoer.cinfo);
}

//#########################################################
void Group_State::setup_beads_and_constraints( Group_Thread& gthread){
  
  Bead_Constrainer_Setupper bcs( gthread, *this);
  bcs.doit();
}

 
//###################################################################
// may need to change algorithm if ntet != 4
Array< Atom_Index, 3>
Group_State::best_links( const Array< Pos, ntet, Atom_Index>& verts,
                          Pos pos){

  typedef Atom_Index Idx;
  auto volume = [&]( Idx i0, Idx i1, Idx i2){
    auto d0 = pos - verts[i0];
    auto d1 = pos - verts[i1];
    auto d2 = pos - verts[i2];
    return fabs( dot( d0, cross(d1,d2)));
  };

  Length3 best_vol{0.0};
  Array< Idx, 3> best_ids{};
  for( auto i: range(4u)){
    auto i0 = i;
    auto i1 = (i+1) % 4;
    auto i2 = (i1+1) % 4;
    auto ii0 = Idx(i0);
    auto ii1 = Idx(i1);
    auto ii2 = Idx(i2);
    auto vol = volume( ii0,ii1,ii2);
    if (vol > best_vol){
      best_ids[0] = ii0;
      best_ids[1] = ii1;
      best_ids[2] = ii2;
      best_vol = vol;
    }
  }

  return best_ids;
} 

//#####################################
std::vector< bool> Group_State::constraint_index() const{
  
  const Chain_Index c1(1);
  auto nc = chain_states.size();
  std::vector< bool> idx( nc/c1);
  for( auto ic: range(nc)){
    idx[ic/c1] = chain_states[ic].frozen;
  }
  return idx;
}

//#########################################
Group_Index Group_State::number() const{
  return common().number;
}

//###############################
bool Group_State::has_hi() const{
  return common().system.has_hi;
}

//###############################
void Group_State::shift( Pos d){
  
  for( auto& chain: chain_states){
    chain.shift( d);
  }
  
  for( auto& tet: tets){
    
    for( auto ia: range( Atom_Index( ntet))){
      auto pos = tet.poses[ ia];
      auto tpos = pos + d;
      tet.poses[ia] = tpos;
    }
    
    tet.push_transforms_down();
  }  
}

//##################################################################
void Group_State::reset_to_start( const System_Common& sys_common){
  
  auto& gcommons = sys_common.groups;
  
  for( auto& core: core_states){
    core.set_to_start(gcommons);
  }
  
  for( auto& chain: chain_states){
    chain.reset_to_start( gcommons);
  }

  setup_beads_and_constraints( thread());
}

//######################################################
void Group_State::copy_from( const Group_State& other){
  
  core_states = other.core_states.copy();
  chain_states = other.chain_states.copy();
  tets = other.tets.copy();
  for( auto& tet: tets){
    tet.set_group_state( *this);
  }
  
  cons_info_and_doer = other.cons_info_and_doer;
}

//##################################################
// copy constructor
Group_State::Group_State( const Group_State& other):
  thread_ref( other.thread_ref), common_ref{ other.common_ref}{
  copy_from( other);
}

//#############################################################
Group_State& Group_State::operator=( const Group_State& other){
  if (&other != this) {
    copy_from(other);
    common_ref = other.common_ref;
    thread_ref = other.thread_ref;
  }
  return *this;
}
//##############################
void Group_State::save( Saved_Group_State& saved) const{
  
  
  saved.has_state = true;
  
  saved.chain_states.clear();
  Chain_Index ncs(0);
  for( auto& chain: chain_states){
    if (!chain.frozen){
      ++ncs;
    }
  }
  saved.chain_states.reserve( ncs);  
  
  for( auto& chain: chain_states){
    if (!chain.frozen){ 
      saved.chain_states.push_back( chain);
    }
  }  
   
  saved.tet_states.clear();
  saved.tet_states.reserve( tets.size()); 
  for( auto& tet: tets){ 
    saved.tet_states.push_back( tet.small_tet());
  }
  
  saved.core_states.clear();
  saved.core_states.reserve( core_states.size());
  for( auto& core: core_states){    
    Small_Core smcore{ core.constrained_poses.copy(), core.force,
                        core.torque};
    saved.core_states.push_back( std::move( smcore));
  }
}

//##############################
void Group_State::swap( Saved_Group_State& saved){
  
  Saved_Group_State temp;
  save( temp);
  restore( saved);
  saved = std::move( temp);
}

//#####################################
void Group_State::restore( const Saved_Group_State& saved){
       
  if (!saved.has_state){
    error( __FILE__, __LINE__, "no state");
  }
    
  Chain_Index ic(0);
  for( auto& chain: chain_states){
    if (!chain.frozen){ 
      chain = saved.chain_states[ic];
      ++ic;
    }
  }
  
  auto nt = tets.size();
  for( auto it: range(nt)){
    auto& tet = tets[it];
    tet.get_from_small_tet( saved.tet_states[it]);
    tet.push_transforms_down();
  }
  
  auto nm = core_states.size();
  for( auto im: range( nm)){
    auto& score = saved.core_states[im];
    auto& core = core_states[im]; 
    core.constrained_poses = score.constrained_poses.copy();
    core.force = score.force;
    core.torque = score.torque;
  }
}

//#################################################
// approximate, not using hydrodynamic interactions
Pos Group_State::center() const{
  
  Vec3< Length2> sum( Zeroi{});
  Length rsum{ 0.0};
  for( auto& tet: tets){
    auto tcen0 = tet.fixed->com0;
    auto tform = tet.transform_of();
    auto tcen = tform.transformed( tcen0);
    auto radius = tet.fixed->radius;
    auto r4 = 4.0*radius;
    sum += r4*tcen;
    rsum += r4;
  }
  
  for( auto& chain: chain_states){
    if (!chain.frozen){
      auto& common = chain.common();
      auto& atoms = chain.atoms;
      auto& common_atoms = common.atoms;
      auto na = atoms.size();
      for( auto ia: range(na)){
        auto pos = atoms[ia].pos;
        auto radius = common_atoms[ia].radius;
        sum += radius*pos;
        rsum += radius;
      }
    }
  }
  
  return (1.0/rsum)*sum;
}

//#####################################################################
/*
Time Group_State::tet_natural_time_step( const Solvent& solvent) const{
  
  Time min_dt{ large};
  for( auto& tet: tets){ 
    auto a = tet.fixed->radius;
    auto dfactor = solvent.diffusivity_factor();
    auto L = tet.fixed->length;
    auto D = dfactor/(pi6*a);
    const double factor = constraint_backstep_tol;
    auto dt = 0.25*sq( factor*L)/D;
    min_dt = std::min( min_dt, dt); 
  }
  return min_dt;
}
 */
//################################################################
/*
// should be negative
Length3 tet_triple_product( const V4< Pos>& poses){
  
  auto pa = [&]( size_t i){
    return poses[ Atom_Index(i)];
  };
  
  auto p0 = pa(0);
  auto v1 = pa(1) - p0;
  auto v2 = pa(2) - p0;
  auto v3 = pa(3) - p0;
  
  return dot( cross(v1,v2),v3);
}
*/
//###############################################################
void Group_State::correct_position( const Vector< Pos, Bead_Index>& xs0){
  
  if (xs0.empty()){
    return;
  }
  
  typedef Group_Constraint_Interface GCI;
  auto& cinfo = cons_info_and_doer->cinfo;
  auto np = cinfo.beads.size();
  
  auto ress = initialized_vector( np, [&]( Bead_Index ip){
    if (is_core_bead( ip)){
      typedef decltype( Energy()/Diffusivity()) Resistance;
      return Resistance{ 0.0};
    }
    else{
      auto D = GCI::mobility( cinfo, ip);
      return 1.0/D;
    }
  });
    
  auto xs = initialized_vector( np, [&]( Bead_Index ip){
    return bead_position( ip);
  });     
         
  auto tform = weighted_alignment_tform( xs, xs0, ress);
       
  for( auto ip: range( np)){
    auto& x = bead_position( ip);
    x = tform.transformed( x);
  }      
}

//###################################################################
void Group_State::satisfy_constraints(){
  
  if (has_unfrozen_chain()){
    auto& constrainer = cons_info_and_doer->doer;
        
    size_t nsteps;
    auto& cinfo = cons_info_and_doer->cinfo;
    
    //auto np = cinfo.beads.size();
    //auto xs0 = initialized_vector( np, [&]( Bead_Index ip){
    //  return bead_position( ip);
    //});
    
    constrainer->satisfy( cinfo, constraint_solve_tol, nsteps);
    //correct_position( xs0);
    push_tets_down();
  }
}

//#######################################
void Group_State::push_forces_to_tets(){
  for( auto& tet:tets){
    tet.compute_vertex_forces();
  }  
}

//##############################################
bool Group_State::has_unfrozen_chain() const{
  
  if (chain_states.empty()){
    return false;
  }
  else{
    bool all_frozen = true;
    for( auto& chain: chain_states){
      if (!chain.frozen){
        all_frozen = false;
        break;
      }
    }
    return !all_frozen;
  }
}

//#####################################################
Pos Group_State::bead_position( Bead_Index ib) const{
  
  auto mut_this = const_cast< Group_State*>( this);
  return mut_this->bead_position( ib);
}

//##############################################
const Bead0_Index b0( 0);

Pos& Group_State::bead_position( Bead_Index ib){
  
  auto& cinfo  = cons_info_and_doer->cinfo;
  auto iib = cinfo.beads[ib];
  
  //const Bead0_Index lb0( 0);
  auto& comm = common();
  
  auto mdiff = cinfo.mdiff;
  auto tdiff = cinfo.tdiff;
  
  if (iib < b0 + mdiff){
    auto bead = comm.all_chain_beads[iib];
    auto ic = bead.is;
    auto ia = bead.ia;
    auto& chain_state = chain_states[ic];
    auto& atom = chain_state.atoms[ia];
    return atom.pos;
  }
  else if (iib < b0 + tdiff){
    auto bead = comm.all_core_beads[ iib - mdiff];
    auto im = bead.is;
    auto iia = bead.ia;
    auto& core_state = core_states[im];
    return core_state.constrained_poses[iia];
  }
  else{
    auto bead = comm.all_tet_beads[ iib - tdiff];
    auto it = bead.is;
    auto ia = bead.ia;
    auto& tet = tets[it];
    return tet.poses[ia];
  }
}

//##############################################
Pos Group_State::saved_bead_position( Bead_Index ib) const{
  
  auto& cinfo  = cons_info_and_doer->cinfo;
  auto iib = cinfo.beads[ib];
  
  //const Bead0_Index lb0( 0);
  auto& comm = common();
  
  auto mdiff = cinfo.mdiff;
  auto tdiff = cinfo.tdiff;
  
  auto& saved = thread().current;
  
  if (iib < b0 + mdiff){
    auto bead = comm.all_chain_beads[iib];
    auto ic = bead.is;
    auto ia = bead.ia;
    
    return saved.chain_states[ic].atoms[ia].pos;
  }
  else if (iib < b0 + tdiff){
    auto bead = comm.all_core_beads[ iib - mdiff];
    auto im = bead.is;
    auto iia = bead.ia;    
    return saved.core_states[im].constrained_poses[iia];
  }
  else{
    auto bead = comm.all_tet_beads[ iib - tdiff];
    auto it = bead.is;
    auto ia = bead.ia;
    return saved.tet_states[it].poses[ia];
  }
}


//#####################################################
Pos Group_State::old_bead_position( const Group_Thread& gthread, Bead_Index ib) const{
  
  auto& cinfo  = cons_info_and_doer->cinfo;
  auto iib = cinfo.beads[ib];
  
  //const Bead0_Index lb0( 0);
  auto& comm = common();
  
  auto mdiff = cinfo.mdiff;
  auto tdiff = cinfo.tdiff;
  
  auto& saved = gthread.current;
  if (!saved.has_state){
    error( __FILE__, __LINE__, "no state");
  }
  
  if (iib < b0 + mdiff){
    auto bead = comm.all_chain_beads[ iib];
    auto ic = bead.is;
    auto ia = bead.ia;
    auto& chain_state = saved.chain_states[ic];
    auto& atom = chain_state.atoms[ia];
    return atom.pos;
  }
  else if (iib < b0 + tdiff){
    auto bead = comm.all_core_beads[ iib - mdiff];
    auto im = bead.is;
    auto iia = bead.ia;
    auto& core_state = saved.core_states[im];
    return core_state.constrained_poses[iia]; 
  }
  else{
    auto bead = comm.all_tet_beads[ iib - tdiff];
    auto it = bead.is;
    auto ia = bead.ia;
    auto& tet = saved.tet_states[it];
    return tet.poses[ia];
  }
}

//#####################################################
bool Group_State::is_core_bead( Bead_Index ib) const{
  
  auto& cinfo  = cons_info_and_doer->cinfo;
  auto iib = cinfo.beads[ib];
  
  //const Bead0_Index lb0( 0);
  
  auto mdiff = cinfo.mdiff;
  auto tdiff = cinfo.tdiff;
  
  return (iib >= b0 + mdiff) && (iib < b0 + tdiff);
}

//######################################################
Length Group_State::bead_radius( Bead_Index ib) const{
  
  auto& cinfo  = cons_info_and_doer->cinfo;
  auto iib = cinfo.beads[ib];
  
  //const Bead0_Index lb0( 0);
  auto& comm = common();
  
  auto mdiff = cinfo.mdiff;
  auto tdiff = cinfo.tdiff;
  
  if (iib < b0 + mdiff){
    auto bead = comm.all_chain_beads[iib];
    auto ic = bead.is;
    auto ia = bead.ia;
    auto& chain = comm.chains[ic];
    auto& atom = chain.atoms[ia];
    return atom.radius;
  }
  else if (iib < b0 + tdiff){
    auto bead = comm.all_core_beads[ iib - mdiff];
    auto im = bead.is;
    auto& core = comm.cores[im];
    return 0.01*core.h_radius;
  }
  else{
    auto bead = comm.all_tet_beads[ iib - tdiff];
    auto it = bead.is;
    auto& tet = tets[it];
    return tet.fixed->radius;
  }
}

//##########################################################
const Constraint0_Index c0( 0);

const Length_Constraint& Group_State::lconstraint( Constraint_Index ic) const{
  
  auto& cinfo = cons_info_and_doer->cinfo;
  auto iic = cinfo.constraints[ic];
  
  auto cop_diff = cinfo.cop_diff;
  auto tm_diff = cinfo.tm_diff;
  auto tt_diff = cinfo.tt_diff;
  
  //Constraint0_Index lc0( 0);
  
  auto& comm = common();
 
  if (iic < c0 + cop_diff){
    return comm.all_length_constraints[ iic];
  }
  else if (iic < c0 + tm_diff){
    error( __FILE__, __LINE__, "not get here");
    return comm.all_length_constraints.back(); // dummy
  }
  else if (iic < c0 + tt_diff){
    return cinfo.tet_core_constraints[ iic - tm_diff];
  }
  else {
    return cinfo.tet_tet_constraints[ iic - tt_diff];
  }
}

//#########################################################
std::pair< Bead_Index, Bead_Index>
Group_State::lconstraint_beads( Constraint_Index ic) const{

  auto& con = lconstraint( ic);
  auto& cinfo = cons_info_and_doer->cinfo;
  
  auto iib0 = con.beads[0];
  auto iib1 = con.beads[1];
  auto ib0 = cinfo.bead_indices[iib0];
  auto ib1 = cinfo.bead_indices[iib1];
  return std::make_pair( ib0, ib1);
}

//#######################################################
std::array< Bead_Index, 4>
Group_State::cconstraint_beads( Constraint_Index ic) const{
  
  auto& cinfo = cons_info_and_doer->cinfo;
  auto iic = cinfo.constraints[ic];
  
  auto cop_diff = cinfo.cop_diff;
  
  auto& comm = common();
   
  if (is_coplanar_constraint( ic)){ 
    auto& con = comm.all_coplanar_constraints[ iic - cop_diff];
    return con.beads.mapped( [&]( Bead0_Index iib){
      return cinfo.bead_indices[ iib];
    }); 
  }
  else{
    error( __FILE__, __LINE__, "not get here");
    return {};
  }
}
//###################################################
Length Group_State::constraint_length( Constraint_Index ic) const{
  
  auto& con = lconstraint( ic);
  return con.length;
}

//##########################################################
bool Group_State::is_coplanar_constraint( Constraint_Index ic) const{
  
  auto& cinfo = cons_info_and_doer->cinfo;
  auto iic = cinfo.constraints[ic];
  
  auto cop_diff = cinfo.cop_diff;
  auto tm_diff = cinfo.tm_diff;
  
  //Constraint0_Index lc0( 0);
  return  (c0 + cop_diff <= iic && iic < c0 + tm_diff);
}

//#########################################################
Constraint_Type Group_State::constraint_type( Constraint0_Index iic) const{
  
  auto& cinfo = cons_info_and_doer->cinfo;
  
  //Constraint0_Index lc0(0);
  if (iic < c0 + cinfo.cop_diff){
    return Constraint_Type::Chain_Core;
  }
  else if (iic < c0 + cinfo.tm_diff){
    return Constraint_Type::Coplanar;
  }
  else if (iic < c0 + cinfo.tt_diff){
    return Constraint_Type::Tet_Core;
  }
  else{
    return Constraint_Type::Tet_Tet;
  }
  
}

//#####################################################
void Group_State::rethread( Group_Thread &gthread){
  
  thread_ref = gthread;
  auto nm = gthread.core_threads.size();
  for( auto im: range( nm)){
    core_states[ im].rethread( gthread.core_threads[ im]);
  }
  
  setup_cons_info_and_doer( gthread);
}

//#####################################################
void Group_State::push_tets_down(){
  for( auto& tet: tets){
    tet.push_transforms_down();
  }
}

//#####################################################
/*
void Group_State::push_tet_core_tforms_down(){
  for( auto& tet: tets){
    tet.push_core_tforms_down();
  }
}
*/
//#####################################################
// debug
double Group_State::constraint_error() const{
  auto& cinfo = cons_info_and_doer->cinfo;
  cinfo.initial_satisfaction = true;
  auto res = cons_info_and_doer->doer->sys_rms_violation( cinfo);
  cinfo.initial_satisfaction = false;
  return res;
}

/*
double Group_State::saved_constraint_error() const{
  auto& cinfo = cons_info_and_doer->cinfo;
  auto res = cons_info_and_doer->doer->sys_rms_violation( cinfo);
  return res;
}
*/
//############################################################
// from common, first conformation; used in staged_simulation
Diffusivity Group_State::diffusivity() const{
  
  auto& sys = common().system;
  auto& solvent = sys.solvent;
  auto D_factor = solvent.diffusivity_factor();
  auto& mob_info0 = common().mobility_info;
  return D_factor*mob_info0.trans_mobility;
}

//############################################################
/*
Inv_Time Group_State::rotational_diffusivity() const{
  
  auto& sys = common().system;
  auto& solvent = sys.solvent;
  auto D_factor = solvent.diffusivity_factor();
  auto& mob_info0 = common().mobility_info;
  return D_factor*mob_info0.rot_mobility;
}
*/
//############################################################
Vec3< Inv_Length> Group_State::scaled_force() const{
    
  F3 f( Zeroi{});
  for( auto& tet: tets){
    for( auto& ft: tet.forces){
      f += ft;
    }
  }
  
  for( auto& chain: chain_states){
    if (!chain.frozen){
      for( auto& atom: chain.atoms){
        f += atom.force;
      }
    }
  }
  
  auto kT = common().system.solvent.kT;
  return f/kT;
}

//############################################################
/*
Vec3< double> Group_State::scaled_torque() const{
    
  auto cen = center();  
    
  T3 tq( Zeroi{});
  for( auto& tet: tets){
    for( auto i: range( Atom_Index( ntet))){
      auto f = tet.forces[i];
      auto pos = tet.poses[i] - cen;
      tq += cross( pos, f);
    }
  }
  
  for( auto& chain: chain_states){
    if (!chain.frozen){
      for( auto& atom: chain.atoms){
        auto f = atom.force;
        auto pos = atom.pos;
        tq += cross( pos, f);
      }
    }
  }
  
  auto kT = common().system.solvent.kT;
  return tq/kT;
}
*/
}

/*
int main(){ turn into unit test

  Group_State< Group_State_States::Neutral> state;
  Group_Common common;
  Vector< Core_Index> coris; 
  Vector< Chain_Index> chainis;

  Chain_State cs0, cs1;

  Vector< Chain_State_Atom> as(8);
  Length o( 5.0), z( 0.0);
  as[0].pos = Pos{o,o,o};
  as[1].pos = Pos{o,o,z};
  as[2].pos = Pos{o,z,o};
  as[3].pos = Pos{o,z,z};
  as[4].pos = Pos{z,o,o};
  as[5].pos = Pos{z,o,z};
  as[6].pos = Pos{z,z,o};
  as[7].pos = Pos{z,z,z};
  for( auto i: range( 8))
    cs0.atoms.push_back( as[i]);

  state.chain_states.push_back( cs0);

  for( auto& atom0: cs0.atoms){
    auto atom = atom0;
    atom.pos[2] += Length(10.0);
    cs1.atoms.push_back( atom);
  }
  state.chain_states.push_back( cs1);

  chainis.push_back( Chain_Index(0));
  chainis.push_back( Chain_Index(1));

  Chain chain;
  for( auto i: range(8)){
    Atom atom;
    atom.radius = Length(rad);
    chain.atoms.push_back( atom);
  }

  common.chains.push_back( chain);
  common.chains.push_back( chain);

  auto tet = equivalent_tetrahedron( state, common, coris, chainis);

  for( auto pos: tet.poses)
    out << pos[0] << " " << pos[1] << " " << pos[2] << "\n";
  out << "\n";

  for( auto& row: tet.lengths){
    for( auto r: row)
      out << r << " ";
    out << "\n";
  }

  out << "radius " << tet.radius << "\n";

  {
    Group_State< Group_State_States::Neutral> state;
    Group_Common common;
    Vector< Chain_Index> chainis;

    Chain_State cs;
    for( auto i: range(4)){
      Chain_State_Atom atom;
      atom.pos = tet.poses[i];
      cs.atoms.push_back( atom);
    }
    
    state.chain_states.push_back( cs);
    chainis.push_back( Chain_Index(0));
    
    Chain chain;
    for( auto i: range(4)){
      Atom atom;
      atom.radius = Length( tet.radius);
      chain.atoms.push_back( atom);
    }
    
    common.chains.push_back( chain);
    
    auto tet = equivalent_tetrahedron( state, common, coris, chainis);

    
  }

}
*/
