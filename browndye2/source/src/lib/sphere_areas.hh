#pragma once

#include <cassert>
#include <random>
#include "geomalg.hh"
#include "vector.hh"
#include "array.hh"
#include "../global/pi.hh"
#include "sq.hh"

namespace Sphere_Area{
  

using namespace Browndye;

using std::cout;
using std::endl;

typedef Array< double, 2> V2;
typedef Array< double, 3> V3;

constexpr int X = 0;
constexpr int Y = 1;
constexpr int Z = 2;

//class Sph_ITag{};
//typedef Index< Sph_ITag> Sph_Index;

class LSeg_ITag{};
typedef Index< LSeg_ITag> LSeg_Index;

class Arc_ITag{};
typedef Index< Arc_ITag> Arc_Index;

class Pt_ITag{};
typedef Index< Pt_ITag> Pt_Index;

using std::pair;
using std::optional;
using std::make_pair;

//################################
/*
struct Sphere{
  Pos pos;
  Length radius;
  
  Pos position() const{
    return pos;
  }
  
  typedef Length LType;
};
*/

//##################################
namespace GA = Geom_Alg;

using GA::element;
constexpr GA::None gnone;

template< class GPos>
V3 pos_of_pt( GPos pt){
  
  auto x = element< 1>( pt);
  auto y = element< 2>( pt);
  auto z = element< 3>( pt);
  
  auto pos_x = []( auto x){
    if constexpr (std::is_same_v< GA::None, decltype( x)>){
      return 0.0;
    }
    else{
      return x;
    }
  };
  
  return make_array( pos_x( x), pos_x( y), pos_x( z));
}

//######################################
template< class GPos>
struct GSphere{
  template< uint... n>
  using E = GA::E< n...>;
  
  template< int n>
  using IC = GA::IC< n>;
  
  GPos pos;
  double radius;
  
  V3 position() const{
    return pos_of_pt( pos);
  }
  
  typedef double LType;
};

template< class GPos>
auto new_gsphere( GPos _pos, double radius){
  return GSphere< GPos>{ _pos, radius};
}

//###########################
template< class T>
struct Circle{
  Vec3< T> pos;
  T radius;
  V3 perp;
};

//################################
using GA::E;
using GA::IC;
typedef decltype( GA::new_sparse_vector( E<1,2>{}, 0.0, E<2,4>{}, 0.0, E<1,4>{}, 0.0)) GLine;

template< class Sph_Index>
struct Line_Seg{
  GLine line;
  Pt_Index ipt0, ipt1;
  bool blocked = false;
  Sph_Index isph;
  bool buries = false;
};

//#############################################
typedef decltype( GA::new_sparse_vector( E<1>{}, 0.0, E<2>{}, 0.0, E<4>{}, IC<1>{})) Pt2;

struct Circle_Pt{
  Pt2 pt;
  bool blocked = false;
  
  Circle_Pt( Pt2 _pt): pt(_pt){}
  Circle_Pt( GA::None){}
};

//#######################################
// counter-clockwise from ipt0 to ipt1
struct Arc{
  Pt_Index ipt0, ipt1;
  bool blocked = false;
  bool newly_blocked = true;
};



//################################################
inline
auto pt_of_pos( V3 pos){
  return GA::point( pos[X], pos[Y], pos[Z]);
}

inline
auto pt_of_pos( V2 pos){
  return GA::point( pos[X], pos[Y], gnone);
}

inline
auto line_of_perp( V3 perp){
  auto ux = perp[X];
  auto uy = perp[Y];
  auto uz = perp[Z]; 
  return GA::new_sparse_vector( E<1,4>{}, ux, E<2,4>{}, uy, E<3,4>{}, uz);
}

// plane points toward perp
inline
auto plane_of_pos_n_perp( V3 pos, V3 perp){
  
  auto line = line_of_perp( perp);
  auto pt = pt_of_pos( pos);
  return plane_with_point_and_perp_to_line( pt, line);
}

typedef decltype( GA::point( double(), double(), double())) Pt3;
typedef decltype( plane_of_pos_n_perp( V3{}, V3{})) Plane;

//########################################################
constexpr IC<0> i0;
constexpr IC<1> i1;

const auto z_axis = GA::new_sparse_vector( E<3>{}, i1);
const auto xy_plane = GA::new_sparse_vector( E<1,2,4>{}, IC<1>{});
const auto origin = GA::point( i0,i0, gnone);

//#######################################################
inline
Pt2 line_line_intersection( GLine linea, GLine lineb){
   
  auto vert_planea = ext_prod( z_axis, linea);
  auto vert_planeb = ext_prod( z_axis, lineb);  
  auto vert_line = ext_anti_prod( vert_planea, vert_planeb); 
  auto ptt = ext_anti_prod( vert_line, xy_plane); 
 
  auto x = element< 1>( ptt);
  auto y = element< 2>( ptt);
  auto w = element< 4>( ptt);
  return GA::point( x/w, y/w, i0);  
}

//#######################################
template< class Pt>
bool is_pt_blocked( GLine line, Pt pt){
  
  auto plane = ext_prod( line, pt);
  auto ori = element< 1,2,4>( plane);
  return ori > 0.0; // okay
}

//#################################################################
auto tformed_0sphere( double h0, double a0t){
  auto pos = GA::point( i0, i0, h0);
  return new_gsphere( pos, a0t);
}

typedef decltype( GA::home_transform( Plane{}, Pt3{})) Home_Tform;

// given spheres 0 and 1, process each sphere 2
template< class I>
struct Sphere_Processor{
  typedef typename I::Sphere_Index Index;
  typedef typename I::LType LType;
  typedef typename I::Sphere Sphere;
  typedef typename I::System System;
  typedef Array< LType, 3> Pos;
  
  Vector< Line_Seg< Index>, LSeg_Index> lsegs;
  Vector< Arc, Arc_Index> arcs;
  Vector< Circle_Pt, Pt_Index> pts;
  
  typedef decltype( tformed_0sphere( double(), double())) Sphere0;
  Sphere0 s0t; 
  Home_Tform htform;
  typedef decltype( 1.0/LType()) Inv_LType;
  Inv_LType scale;
  //bool blocked = false;
  
  Sphere_Processor( const System&, const Sphere& s0, const Sphere& s1);
  
  // false if circle of s0-s1 is occluded
  bool process( const Sphere& s2);
  
  typedef decltype( LType()*LType()) LType2;
  pair< LType2, bool> occluded_area() const;
  void check_consistency() const;
  
private:
  const System& sys;
  
  LType rad( const Sphere&) const;
  Pos pos( const Sphere&) const;
  void add_initial_pts();
  void add_initial_arcs();
  void add_home_transform( Circle< LType>&);
  pair< GLine, bool> sphere2_line( const Sphere& s2) const;
  void block_pts( GLine);
  void check_against_segs( unsigned int& n_new_pts, GLine, bool buries);
  void check_against_arcs( unsigned int& n_new_pts,
                            Pt2 pta, Pt2 ptb, GLine line, bool buries);
  
  static
  std::pair< Pt2, Pt2> arc_points( GLine line);
  
  void find_sliced_arc( unsigned int&, GLine line, Pt2 pta, Pt2 ptb,
                        bool buries);
  bool not_all_are_blocked() const;
  
  void truncate_arc( bool bl0, Pt2 pta, Pt2 ptb, Arc& arc);
  
  void add_line_segment( bool last_is_zero, GLine, bool buries);
  void compute_arc_sum( double& sum) const;
  void compute_segment_pair_sum( const Vector< Plane, LSeg_Index>&,
                                         double& sum) const;
  
  void compute_arc_lseg_sum( const Vector< Plane, LSeg_Index>&,
                                    double& sum) const;
  bool is_arc_found( Pt_Index ipt) const;
  
  Vector< Plane, LSeg_Index> segment_planes() const;
  void block_seg_pts();
  void block_arc_pts();
  
  void add_segment_point( GLine line, Line_Seg< Index>& lseg,
                          bool bl0, bool& last_pt_is_zero);
  
  void change_arcs( Arc& arc, Pt_Index ipa, Pt_Index ipb);
  
  static
  auto ctformed( Home_Tform htform, Inv_LType scale, Pos p);
  
  auto tformed_sphere( Home_Tform htform, Inv_LType scale, Sphere s) const;
  
  template< class I0, class I1>
  auto ss_inters_circle( const typename I0::Sphere& s0, const typename I1::Sphere& s1) const;
};



template< class I>
auto Sphere_Processor<I>::rad( const Sphere& sph) const -> LType{
  return I::radius( sys, sph);
}

template< class I>
auto Sphere_Processor<I>::pos( const Sphere& sph) const -> Pos{
  return I::position( sys, sph);
}

//###############################################################
// perp points from s0 to s1
template< class I>
template< class I0, class I1>
auto Sphere_Processor<I>::ss_inters_circle( const typename I0::Sphere& s0, const typename I1::Sphere& s1) const{ 
  
  auto p0 = I0::position( sys, s0); //s0.position();
  auto p1 = I1::position( sys, s1); //s1.position();
  auto a0 = I0::radius( sys, s0); //s0.radius;
  auto a1 = I1::radius( sys, s1); //s1.radius;
  auto r = distance( p0, p1);
  
  auto a02 = a0*a0;
  auto a12 = a1*a1;
  auto a2diff = a12 - a02;
  auto rdb = 2.0*r;
  auto r0 = (r*r - a2diff)/rdb;
  auto u = normed( p1 - p0);
  
  typedef typename I0::LType LType;
  
  Circle< LType> circle;
  circle.perp = u;
  circle.radius = sqrt( a02 - r0*r0);
  circle.pos = p0 + r0*u;
  
  typedef decltype( r0) R0;
  auto center_buried = (r0 < R0(0.0));
  return make_pair( circle, center_buried);
}

//#########################################################
template< class I>
void Sphere_Processor<I>::compute_arc_sum( double& sum) const{

  auto h0 = element< 3>( s0t.pos);
  auto r0 = s0t.radius;

  auto c_theta = -h0/r0;
    
  for( auto arc: arcs){
    if (!arc.blocked){
      auto ip0 = arc.ipt0;
      auto ip1 = arc.ipt1;
      auto pt0 = pts[ip0].pt;
      auto pt1 = pts[ip1].pt;
         
      // inner product
      auto c_phi_v = dot_prod( pt0, pt1);
      auto c_phi = element<>( c_phi_v);
      auto phi = acos( c_phi);
      sum += c_theta*phi;
    }
  }
}

//########################################################
// put in angles between planes
template< class I>
void Sphere_Processor<I>::
  compute_segment_pair_sum( const Vector< Plane, LSeg_Index>& planes,
                             double& sum) const{
  
  auto h0 = element< 3>( s0t.pos);
  for( auto ipt: range( pts.size())){
    auto& pt = pts[ipt];
    if (!pt.blocked){
      uint n_lsegs = 0;
      LSeg_Index il0{ maxi}, il1{ maxi};
      
      for( auto isg: range( lsegs.size())){
        auto& lseg = lsegs[isg];
        if (lseg.ipt0 == ipt || lseg.ipt1 == ipt){
          ++n_lsegs;
          if (n_lsegs == 1){
            il0 = isg;
          }
          else{
            il1 = isg;
            auto& plane0 = planes[il0];
            auto& plane1 = planes[il1];
            auto bur0 = lsegs[il0].buries;
            auto bur1 = lsegs[il1].buries;
            
            auto omega = angle_between_planes( plane0, plane1);
            
            if (h0 > 0.0 && (bur0 ^ bur1)){
              sum += omega;
            }
            else if (h0 < 0.0){
              sum += omega;
            }
            else{
              sum -= omega; // ad hoc
            }
            break;
          }
        }
      }
    }
  }
}

//###################################################
template< class I>
bool Sphere_Processor<I>::is_arc_found( Pt_Index ipt) const{      
  bool arc_found = false;  
  for( auto iarc: range( arcs.size())){
    auto& arc = arcs[iarc];
    if (arc.ipt0 == ipt || arc.ipt1 == ipt){
      arc_found = true;
      break;
    }
  }
  return arc_found;
}

//###########################################################
typedef decltype( GA::point( i0,i0, double())) VPt;
           
inline           
auto segarc_planes( Pt2 ept, VPt cen0, Plane plane){
   
  auto aplane = [&]{
    auto line = line_of_points( origin, ept);
    return plane_of_line_and_perp_plane( line, xy_plane);
  }();
  
  // seg plane
  auto splane = [&]{
    auto line = line_of_points( ept, cen0); 
    return plane_of_line_and_perp_plane( line, plane);
  }();
  
  return make_pair( aplane, splane);
}

//############################################################
// put in angles between plane and arc
template< class I>
void Sphere_Processor<I>::compute_arc_lseg_sum( const Vector< Plane, LSeg_Index>& planes,
                                     double& sum) const{
  
  auto h0 = element< 3>( s0t.pos);
  auto cen0 = GA::point( i0, i0, h0);
  
  for( auto ipt: range( pts.size())){
    auto& pt = pts[ipt];
    if (!pt.blocked){
      if (is_arc_found( ipt)){
         
        for( auto isg: range( lsegs.size())){
          auto& lseg = lsegs[isg];
          bool is0 = lseg.ipt0 == ipt;
          bool is1 = lseg.ipt1 == ipt;
          if (is0 || is1){      
            auto ept = pts[ipt].pt;            
            auto [aplane,splane] = segarc_planes( ept, cen0, planes[isg]); 
            auto omega = angle_between_planes( aplane, splane);
              
            auto h0 = element< 3>( s0t.pos);
            if (h0 < 0.0 || (h0 > 0.0 && lseg.buries)){  
              sum += omega;
            }
            else{
              sum -= omega; 
            }
            break;
  }}}}}}
     
//########################################################
template< class I>
Vector< Plane, LSeg_Index> Sphere_Processor<I>::segment_planes() const{
  
  auto h0 = element< 3>( s0t.pos);
  auto cen0 = GA::point( i0, i0, h0);
  
  // lseg.line points ccw
  // plane normal points outward for cen0[Z] < 0
  return lsegs.mapped(
    [&]( const Line_Seg< Index>& lseg){
      auto plane = ext_prod( lseg.line, cen0);
      
      auto h0 = GA::element< 3>( s0t.pos);
      if (lseg.buries && h0 > 0.0){
        return IC<-1>{}*plane;
      }
      else{
        return plane;
      }
    }
  );  
}

//##############################################################
// true if blocked
template< class I>
auto Sphere_Processor<I>::occluded_area() const -> std::pair< LType2, bool>{
  
  double sum = 0.0;
  compute_arc_sum( sum);
   
  auto planes = segment_planes();
  compute_segment_pair_sum( planes, sum);
  compute_arc_lseg_sum( planes, sum);
  
  bool blocked = (sum == 0.0);
  
  auto radius0 = s0t.radius/scale;
  return make_pair( sq( radius0)*( pi2 - sum), blocked);
}

//#########################################################
template< class I>
auto Sphere_Processor<I>::ctformed( Home_Tform htform, Inv_LType scale, Pos p){
  auto sp = scale*p;
  auto spt = pt_of_pos( sp);
  return GA::transformed( htform, spt);
}

template< class I>
auto Sphere_Processor<I>::tformed_sphere( Home_Tform htform, Inv_LType scale, Sphere s) const{
  return new_gsphere( ctformed( htform, scale, pos( s)), scale*rad( s));
}

//###################################################################
// constructor
template< class I>
Sphere_Processor<I>::Sphere_Processor( const System& _sys, const Sphere& s0, const Sphere& s1): sys(_sys){
  
  auto a0 = rad( s0);
  //auto a1 = s1.radius;
  
  auto c1 = ss_inters_circle<I,I>( s0, s1).first;
  scale = 1.0/c1.radius;
  add_home_transform( c1);
      
  auto h0 = element< 3>(ctformed( htform, scale, pos( s0)));
  //auto h1 = element< 3>(ctformed( htform, scale, s1.pos));
  
  auto a0t = scale*a0;
  //auto a1t = scale*a1;
  
  s0t = tformed_0sphere( h0, a0t);
  add_initial_pts();
  add_initial_arcs();
}

//############################################################
template< class I>
void Sphere_Processor<I>::add_home_transform( Circle< LType>& c1){
  
  V3 cpos = scale*c1.pos;
  auto cpt = pt_of_pos( cpos);
  auto plane = plane_of_pos_n_perp( cpos, c1.perp);
  
  htform = GA::home_transform( plane, cpt);  
}

//##############################################
template< class I>
void Sphere_Processor<I>::add_initial_pts(){
  
  auto add_circle_pt = [&]( V2 pos){
    Circle_Pt cpt( pt_of_pos( pos));
    pts.push_back( cpt);
  };
  
  add_circle_pt( V2{ 1.0, 0.0});
  add_circle_pt( V2{ -1.0, 0.0});
}

//##########################################
template< class I>
void Sphere_Processor<I>::add_initial_arcs(){
  
  const Pt_Index ip0( 0), ip1( 1);
  
  Arc arc0;
  arc0.ipt0 = ip0;
  arc0.ipt1 = ip1;
  arcs.push_back( arc0);
  
  Arc arc1;
  arc1.ipt0 = ip1;
  arc1.ipt1 = ip0;
  arcs.push_back( arc1);
}

//####################################################
template< class System, class Sph>  
class SIface{
public:
  //typedef decltype( s0t) Sphere;
  typedef Sph Sphere;
  typedef double LType;
  
  static
  Array< double, 3> position( const System&, const Sphere& sph){
    return sph.position();
  }
  
  static
  double radius( const System&, const Sphere& sph){
    return sph.radius;
  }
};

// line points ccwise
template< class I>
pair< GLine, bool> Sphere_Processor<I>::sphere2_line( const Sphere& s2) const{
  
  auto s2t = tformed_sphere( htform, scale, s2);
 
  typedef SIface< System, decltype( s0t)> I0;
  typedef SIface< System, decltype( s2t)> I2;
  
  auto [c02,is_buried] = ss_inters_circle< I0,I2>( s0t, s2t);
  auto plane02 = plane_of_pos_n_perp( c02.pos, c02.perp);
  auto line = GA::unitized( line_of_planes( xy_plane, plane02));
  return make_pair( line, is_buried);
}

//#################################################
inline
auto is_circle_unblocked( GLine line){
  if (is_pt_blocked( line, origin)){ 
    return false; // whole circle is blocked
  }
  else{
    return true;    
  }
}

//##############################################
template< class I>
void Sphere_Processor<I>::block_seg_pts(){
  
  for( auto& lseg: lsegs){
    auto ip0 = lseg.ipt0;
    auto ip1 = lseg.ipt1;
    auto& pt0 = pts[ ip0];
    auto& pt1 = pts[ ip1];
    if (pt0.blocked && pt1.blocked){
      lseg.blocked = true;
    }
  }
}

//############################################
template< class I>
void Sphere_Processor<I>::block_arc_pts(){
  
  for( auto& arc: arcs){
    auto ip0 = arc.ipt0;
    auto ip1 = arc.ipt1;
    auto& pt0 = pts[ ip0];
    auto& pt1 = pts[ ip1];
    if (pt0.blocked && pt1.blocked){
      arc.blocked = true;
    }
  }
}

//############################################
template< class I>
void Sphere_Processor<I>::block_pts( GLine line){
  
  for( auto& cpt: pts){
    if (is_pt_blocked( line, cpt.pt)){
      cpt.blocked = true;
    }
  }
  
  block_seg_pts();
  block_arc_pts();
}

//###################################################################
template< class I>
void Sphere_Processor<I>::add_line_segment( bool last_pt_is_zero, GLine line, bool buries){
  
  Line_Seg< Index>& lseg = lsegs.emplace_back();
  lseg.line = line;
  lseg.buries = buries;
  auto ipa = deced( pts.size());
  auto ipb = deced( ipa);
  if (last_pt_is_zero){
    lseg.ipt0 = ipa;
    lseg.ipt1 = ipb;
  }
  else{
    lseg.ipt0 = ipb;
    lseg.ipt1 = ipa;    
  }
}

//################################################################
template< class I>
void Sphere_Processor<I>::add_segment_point( GLine line, Line_Seg< Index>& lseg,
                                bool blocked0, bool& last_pt_is_zero){
  
  Circle_Pt cpt( line_line_intersection( line, lseg.line));
  auto ipt_new = pts.size();
  pts.push_back( cpt);
   
  if (blocked0){
    lseg.ipt0 = ipt_new;
    last_pt_is_zero = false;
  }
  else{
    lseg.ipt1 = ipt_new;
    last_pt_is_zero = true;
  }
}

//###############################################################
template< class I>
void Sphere_Processor<I>::check_against_segs( unsigned int& n_new_pts,
                                              GLine line, bool buries){
  
  for( auto& lseg: lsegs){
    
    bool last_pt_is_zero = false;
    if (!lseg.blocked){
      auto& pt0 = pts[ lseg.ipt0];
      auto& pt1 = pts[ lseg.ipt1];
      auto bl0 = pt0.blocked;
      auto bl1 = pt1.blocked;
      if (bl0 && bl1){
        lseg.blocked = true;
      }
      else if (bl0 ^ bl1){
        ++n_new_pts;  
        add_segment_point( line, lseg, bl0, last_pt_is_zero);
      }
    }
    if (n_new_pts == 2){
      add_line_segment( last_pt_is_zero, line, buries);
      break;
    }
  } 
}
//########################################################
template< class I>
void Sphere_Processor<I>::truncate_arc( bool bl0, Pt2 pta, Pt2 ptb, Arc& arc){
  
  auto ipt_new = pts.size();
  Circle_Pt cpt( gnone);
  if (bl0){
    cpt.pt = ptb;
    arc.ipt0 = ipt_new;
  }
  else{
    cpt.pt = pta;
    arc.ipt1 = ipt_new;
  }
  
  pts.push_back( cpt);
}

//#################################################################
template< class I>
void Sphere_Processor<I>::check_against_arcs( unsigned int& n_new_pts,
                                     Pt2 pta, Pt2 ptb, GLine line,
                                          bool buries){   
    
  for( auto& arc: arcs){
    
    bool last_pt_is_zero = false; 
    if (!arc.blocked){
      auto& pt0 = pts[ arc.ipt0];
      auto& pt1 = pts[ arc.ipt1];
      auto bl0 = pt0.blocked;
      auto bl1 = pt1.blocked;
            
      if (bl0 && bl1){ // superfluous?
        arc.blocked = true;
      }
      else if (bl0 ^ bl1){ // arc is intersected at one point
        ++n_new_pts;
        truncate_arc( bl0, pta, ptb, arc);
        last_pt_is_zero = bl1;
      }
    }
    
    if (n_new_pts == 2){
      add_line_segment( last_pt_is_zero, line, buries);
      break;
    }
  } 
}

//#####################################################
inline
auto arc_points_directions( GLine line){
  
  auto vplane = ext_prod( z_axis, line);
  auto tline = ext_prod( lcomp( weight( vplane)), origin);
  
  auto tpt = ext_anti_prod( tline, vplane);
  
  V2 t{ element< 1>( tpt), element< 2>( tpt)};
  V2 u{ element< 1,4>( line), element< 2,4>( line)};
  return make_pair( t,u);
}

//###########################################################
template< class I>
std::pair< Pt2, Pt2> Sphere_Processor<I>::arc_points( GLine line){
  
  auto [t,u] = arc_points_directions( line);
  
  auto r = element< 1,2>( line);
  auto rp = sqrt( 1.0 - r*r);
  auto ru = rp*u;
  
  auto pa = t - ru; 
  Pt2 pta = point( pa[X], pa[Y], gnone);
  
  auto pb = t + ru;
  Pt2 ptb = point( pb[X], pb[Y], gnone);
  
  return std::make_pair( pta, ptb);
}

//#####################################################
template< class I>
void Sphere_Processor<I>::change_arcs( Arc& arc, Pt_Index ipa, Pt_Index ipb){    

  if (arc.blocked){ // only arc left in disk
    arc.ipt0 = ipb;
    arc.ipt1 = ipa;
    arc.blocked = false;
  }
  else{ // split arc
    auto old_ipt1 = arc.ipt1;
    arc.ipt1 = ipa;
    Arc new_arc;
    new_arc.ipt0 = ipb;
    new_arc.ipt1 = old_ipt1;
    arcs.push_back( new_arc);
  }
}

//#########################################################
// an arc sliced by line, or line is hidden by other lines
template< class I>
void Sphere_Processor<I>::find_sliced_arc( unsigned int& n_new_pts,
                                         GLine line, Pt2 pta, Pt2 ptb,
                                       bool buries){
  
  for( auto& arc: arcs){    
    if (!arc.blocked || (arc.blocked && arc.newly_blocked)){
    
      auto pt0 = pts[ arc.ipt0].pt;
      auto pt1 = pts[ arc.ipt1].pt;
      auto arc_line = line_of_points( pt0, pt1);
      
      auto ablo = is_pt_blocked( arc_line, pta); 
      auto bblo = is_pt_blocked( arc_line, ptb);
      if (ablo && bblo){  
        Circle_Pt cpta( pta), cptb( ptb);
        auto ipa = pts.size();
        auto ipb = inced( ipa);
        pts.push_back( cpta);
        pts.push_back( cptb);
        n_new_pts += 2; 
               
        change_arcs( arc, ipa, ipb);
        add_line_segment( false, line, buries);
        break;
      }
    }
  }
}
     
//###################################################
template< class I>
bool Sphere_Processor<I>::not_all_are_blocked() const{
       
  assert( !(lsegs.empty() && arcs.empty()));
  
  for( auto& lseg: lsegs){
    if (!lseg.blocked){
      return true;
    }
  }
  
  for( auto& arc: arcs){
    if (!arc.blocked){
      return true;
    }
  }
  
  return false;
}

//##################################################
template< class I>
void Sphere_Processor<I>::check_consistency() const{
  
  auto print = [&](){
    cout << "all arcs at error\n";
    for( auto& ac: arcs){
       cout << "  arc " << ac.blocked << ' ' << ac.newly_blocked << endl;
       auto pt0 = pts[ ac.ipt0].pt;
       auto pt1 = pts[ ac.ipt1].pt;
       cout << "  " << pos_of_pt( pt0) << endl;
       cout << "  " << pos_of_pt( pt1) << endl;
     }
     cout << "all segs at error\n";
     for( auto& lseg: lsegs){
       cout << "  seg " << lseg.blocked << endl;
       auto pt0 = pts[ lseg.ipt0].pt;
       auto pt1 = pts[ lseg.ipt1].pt;
       cout << "  " << pos_of_pt( pt0) << endl;
       cout << "  " << pos_of_pt( pt1) << endl;
     }
  };
  
  auto find_pt1 = [&]( auto p0){
    
    int nr = 0;
    for( auto& lseg: lsegs){
      if (!lseg.blocked){
        auto ip1 = lseg.ipt1;
        auto p1 = pos_of_pt( pts[ ip1].pt);
        if (p0 == p1){
          ++nr;
          if (nr > 1){
            print();
            error( "not consistent");
          }
        }
      }
    }
    
    for( auto& arc: arcs){
      if (!arc.blocked){
        auto ip1 = arc.ipt1;
        auto p1 = pos_of_pt( pts[ ip1].pt);
        if (p0 == p1){
          ++nr;
          if (nr > 1){
            print();
            error( "not consistent");
          }
        }
      }
    }
    
    if (nr != 1){
      print();
      error( "not consistent");
    }
  };
  
  for( auto& lseg: lsegs){
    if (!lseg.blocked){
      auto ip0 = lseg.ipt0;
      auto p0 = pos_of_pt( pts[ ip0].pt);
      find_pt1( p0);
    }
  }
  
  for( auto& arc: arcs){
    if (!arc.blocked){
      auto ip0 = arc.ipt0;
      auto p0 = pos_of_pt( pts[ ip0].pt);
      find_pt1( p0);
    }
  }
}

//##################################################
template< class I>
bool Sphere_Processor<I>::process( const Sphere& s2){
  
  for( auto& arc: arcs){ 
    if (arc.blocked){
      arc.newly_blocked = false;
    }
  }
   
  auto [line, buries] = sphere2_line( s2);
  auto bnorm = GA::bulk_norm( line); // distance from origin
  if (bnorm > 1.0){ // outside
    return is_circle_unblocked( line);
  }
  else{
    block_pts( line);
    
    unsigned int n_new_pts = 0;
    check_against_segs( n_new_pts, line, buries);
    
    if (n_new_pts != 2){     
      auto [pta,ptb] = arc_points( line);      
      check_against_arcs( n_new_pts, pta, ptb, line, buries);
      
      if (n_new_pts == 0){    
        find_sliced_arc( n_new_pts, line, pta, ptb, buries);
      }
      if (n_new_pts == 0){
        return not_all_are_blocked();
      }
    }
    
    return true;
  }
}

//###############################################################
template< class I>
auto weighted_area( const typename I::System& sys,
                     const Vector< double, typename I::Sphere_Index>& weights,
                     const Vector< typename I::Sphere, typename I::Sphere_Index>& spheres){
  
  
  typedef typename I::LType LType;
  typedef decltype( LType()*LType()) LType2;
  typedef typename I::Sphere_Index Index;
  typedef typename I::Sphere Sphere;
  
  LType2 w_occluded_area{ 0.0};
  
  auto pos = [&]( const Sphere& sph){
    return I::position( sys, sph);
  };
  
  auto rad = [&]( const Sphere& sph){
    return I::radius( sys, sph);
  };
  
  
  auto n = spheres.size();
  for( auto is0: range( n)){

    auto& s0 = spheres[is0];
    auto a0 = rad( s0);
    auto p0 = pos( s0);
    auto wt0 = weights[ is0];
    
    //Length2 barea0{ 0.0};
    Vector< Index> s0_nebors;
    for( auto is1: range( n)){
      if (is1 != is0){
        auto& s1 = spheres[is1];
        auto a1 = rad( s1);
        auto p1 = pos( s1);
        if (distance( p0, p1) < a0 + a1){
          s0_nebors.push_back( is1);
        }
      }
    }
    
    bool all_blocked = !s0_nebors.empty();
    for( auto is1: s0_nebors){
      auto& s1 = spheres[is1];
      auto a1 = rad( s1);
      
      Sphere_Processor<I> sproc( sys, s0, s1);
      
      bool circle_blocked = false;
      for( auto is2: s0_nebors){
        if (is2 != is1){
          auto s2 = spheres[is2];
          
          auto d12 = distance( pos( s2), pos( s1));     
          if (d12 < a1 + rad( s2)){
            circle_blocked = !sproc.process( s2);
            if (circle_blocked){
              break;
            }
          }
        }
        //sproc.check_consistency();
      }
      
      if (!circle_blocked){
        // compute occluded
        auto [oa,blocked] = sproc.occluded_area();
        if (blocked){
          circle_blocked = true;
        }
        else{
          if (oa < LType2( 0.0)){
            error( "negative area");
          }          
          all_blocked = false;
          w_occluded_area += wt0*oa;
        }
      }
    }
    if (all_blocked){
      w_occluded_area += wt0*pi4*sq( rad( s0));
    }
  }
  
  LType2 w_unoccluded_area{ 0.0};
  for( auto i: range( n)){
    auto r = rad( spheres[i]);
    auto area = pi4*r*r;
    auto w = weights[i];
    w_unoccluded_area += w*area;
  }
   
  return w_unoccluded_area - w_occluded_area;
}

//###############################################################
template< class I>
auto mc_weighted_area( const typename I::System& sys,
                      const Vector< double, typename I::Sphere_Index>& weights,
                    const Vector< typename I::Sphere, typename I::Sphere_Index>& spheres){
  
  typedef typename I::Sphere Sphere;
  
  std::mt19937_64 gen;
  std::normal_distribution< double> gauss;
  
  auto pos = [&]( const Sphere& sph){
    return I::position( sys, sph);
  };
  
  auto rad = [&]( const Sphere& sph){
    return I::radius( sys, sph);
  };
  
  const size_t nsamp = 1e7;
  typedef typename I::LType LType;
  typedef decltype( LType()*LType()) LType2;
  LType2 area{ 0.0};
  
  auto nsp = spheres.size();
  for( auto i0: range( nsp)){
    
    auto& sph0 = spheres[i0];
    size_t nsurf = 0;
    for( auto ism: range( nsamp)){
      (void)ism;
      
      Vec3< double> u{ gauss( gen), gauss( gen), gauss( gen)};
      auto unrm = norm( u);
      auto pt = pos( sph0) + (rad( sph0)/unrm)*u;
      
      bool coll = false;
      for( auto i1: range( nsp)){
        if (i1 != i0){
          auto& sph1 = spheres[i1];
          auto d = distance( pt, sph1.pos);
          if (d < rad( sph1)){
            coll = true;
            break;
          }
        }
      }
      
      if (!coll){
        ++nsurf;
      }
    }
    
    auto frac = ((double)nsurf)/nsamp;
    area += weights[i0]*pi4*sq( rad( sph0))*frac;
  }
  return area;
}

//######################################
/*
const Length L1( 1.0);
namespace GA = Geom_Alg;

// move GA tests to GA library
pair< uint, int> cwedge( pair< uint, int> a, pair< uint, int> b){
  
  auto [av,as] = a;
  auto [bv,bs] = b;
  auto [wv,ss] = GA::wedge( av, bv);
  return make_pair( wv, ss*as*bs);
}

pair< uint, int> cawedge( pair< uint, int> a, pair< uint, int> b){
  
  auto [av,as] = a;
  auto [bv,bs] = b;
  auto [wv,ss] = GA::antiwedge( av, bv);
  return make_pair( wv, ss*as*bs);
}


pair< uint, int> crcomp( pair< uint, int> a){
  
  auto [av,as] = a;
  auto [wv,ss] = GA::right_complement( av);
  return make_pair( wv, as*ss);
}

pair< uint, int> clcomp( pair< uint, int> a){
  
  auto [av,as] = a;
  auto [wv,ss] = GA::left_complement( av);
  return make_pair( wv, as*ss);
}

void ga_tests(){
  
  // exterior product symmetries
  for( uint a = 0; a < 16; a++){
    for( uint b = 0; b < 16; b++){
      
      auto [val,sgn] = GA::wedge( a,b);
      auto [rval, rsgn] = GA::wedge( b,a);
      auto ga = GA::grade( a);
      auto gb = GA::grade( b);
      auto gv = GA::grade( val);
      
      if (sgn != 0){
        if (gv != ga + gb){
          error( "grades do not match");
        }
        
        if (ga > 0 && gb > 0){
          if (gv % 2 == 0 && !(ga == 2 && gb == 2)){ // even
            auto det = sgn*(int)val + rsgn*(int)rval;
            if (det != 0){
              cout << det << endl;
              cout << a << ' ' << b << ' ' << val << ' ' << sgn << ' ' << rsgn << endl;
            }
          }
          else{ // odd
            auto det = sgn*(int)val - rsgn*(int)rval;
            if (det > 0){
              cout << det << endl;
              cout << a << ' ' << b << ' ' << val << ' ' << sgn << ' ' << rsgn << endl;
            }
          }
        }
      }
      
      {
        auto [abv,abs] = GA::wedge( a, b);
        auto [bav,bas] = GA::wedge( b, a);
        auto gra = GA::grade( a);
        auto grb = GA::grade( b);
        auto sg = GA::power( gra*grb, -1);
        
        if (abv != bav || abs != sg*bas){
          error( "not work");
        }
      }
      
      for( uint c = 0; c < 16; c++){
        
        auto ac = make_pair( a, 1);
        auto cc = make_pair( c, 1);
        auto left = cwedge( ac, GA::wedge( b, c));
        auto right = cwedge( GA::wedge( a, b), cc);
        if (left != right){
          auto [leftv,lefts] = left;
          auto [rightv,rights] = right;
          if (!(lefts == 0) || !(rights == 0)){
            error( "not associative");
          }
        }
      }
      
    }
  }
  
  // left and right complements
  for( uint a = 0; a < 16; a++){
    
    auto [rv,rs] = GA::right_complement( a);    
    auto [gv,gs] = GA::geometric( a, rv);
    
    if (gv != 15 || rs*gs != 1){
      error( "right comp wrong");
    }
  }

  for( uint a = 0; a < 16; a++){
    for( uint b = 0; b < 16; b++){
      {
        auto left = crcomp( GA::wedge( a,b));  
        auto right = cawedge( GA::right_complement( a),
                               GA::right_complement( b));
      
        if (left != right){
          error( "right comp wrong 1");
        }
      }
      {
        auto left = crcomp( GA::antiwedge( a,b));  
        auto right = cwedge( GA::right_complement( a),
                               GA::right_complement( b));

        if (left != right){
          error( "right comp wrong 2");
        }
        
      }
      {
        auto left = clcomp( GA::wedge( a,b));  
        auto right = cawedge( GA::left_complement( a),
                               GA::left_complement( b));
      
        if (left != right){
          error( "left comp wrong 1");
        }
      }
      {
        auto left = clcomp( GA::antiwedge( a,b));  
        auto right = cwedge( GA::left_complement( a),
                               GA::left_complement( b));
      
        if (left != right){
          error( "left comp wrong 2");
        }
        
      }
      
    }
  }
}
//####################################################
void test_spheres( std::mt19937_64& gen, int isamp){
  
  std::uniform_real_distribution< double> uni( -1.0, 1.0), unir( 0.5, 1.0);
  
  Sph_Index nsp( 10);
  
  Vector< Sphere, Sph_Index> spheres;
  Vector< double, Sph_Index> weights;
  
  while(true){
    
    for( auto isp: range( nsp)){
      (void)isp;
      auto& sph0 = spheres.emplace_back();
      sph0.radius = L1*unir( gen);
      sph0.pos = Pos{ L1*uni( gen), L1*uni( gen), L1*uni( gen)};
      weights.emplace_back( 1.0); 
    }
    
    bool do_over = false;
    for( auto isp: range( nsp)){
      auto& sphi = spheres[isp];
      for( auto jsp: range( isp)){
        auto& sphj = spheres[jsp];
        auto d = distance( sphi.pos, sphj.pos);
        auto ai = sphi.radius;
        auto aj = sphj.radius;
        if (d + ai < aj || d + aj < ai){
          do_over = true;
        }
      }
    }
    if (!do_over){
      break;
    }
    else{
      spheres.clear();
      weights.clear();
    }
  }
  
  //auto remove = [&]( int i){
  //  Sph_Index isp(i);
  //  spheres[isp] = spheres.back();
  //  spheres.pop_back();
  //};
     
     
  auto area = weighted_area( weights, spheres);
  auto aarea = mc_weighted_area( weights, spheres);
  auto rdiff = fabs( area - aarea)/aarea;
  
  cout << isamp << ' ' << area << ' ' << aarea << ' ' << rdiff << endl;
  if (rdiff > 1.0e-3){
    cout << "#####################################\n";
  }
}

*/

}

/*
//###################################################
int main(){ 
  std::mt19937_64 gen;
  
  for( auto i: range( 50)){
    test_spheres( gen, i);
  }
}
*/
