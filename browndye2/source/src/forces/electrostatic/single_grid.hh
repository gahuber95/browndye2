#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Single_Grid::Grid holds grid data. It can be initialized from a DX file or
and XML file (such as output by aux/grid_distances).  It uses trilinear
interpolation to get values at arbitrary points.

 */


#include <cstdio>
#include <cstdarg>
#include <string>
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../xml/node_info.hh"
#include "../../lib/units.hh"
#include "../../lib/vector.hh"
#include "../../lib/array.hh"
#include "../../global/pos.hh"

namespace Browndye{

class Single_Grid_Defs{
public:
  typedef JAM_XML_Pull_Parser::Parser Parser;
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
  
  typedef std::pair< std::tuple< long, long, long>, std::tuple< double, double, double> > Locator;
  template< class T> using optional = std::optional<T>;
  typedef std::string string;
  
  template< class T>
  using Cube = Array< Array< Array< T, 2>, 2>, 2>;
  
  static constexpr std::nullopt_t nullopt = std::nullopt;
};


//###############################################
class Single_Grid_Defs2: public Single_Grid_Defs{};

template< class Potential>
class Single_Grid_Nograd: public Single_Grid_Defs{
public:
 
  optional< Potential> potential( const Pos& pos) const;
 
  Single_Grid_Nograd( const string& filename, Pos offset, bool include_data = true);
  Single_Grid_Nograd() = delete;
  Single_Grid_Nograd( const Single_Grid_Nograd&) = delete;
  Single_Grid_Nograd& operator=( const Single_Grid_Nograd&) = delete;
  Single_Grid_Nograd( Single_Grid_Nograd&&) = default;
  Single_Grid_Nograd& operator=( Single_Grid_Nograd&&) = default;
  ~Single_Grid_Nograd() = default;
  
  // xml input
  explicit Single_Grid_Nograd( Parser& parser);
  
  [[nodiscard]] Pos low_corner() const;
  [[nodiscard]] Pos high_corner() const;
  [[nodiscard]] Vec3< size_t> dimensions() const;
  [[nodiscard]] Pos spacings() const;
  
  Potential value( long, long, long) const;
  void set_value( long, long, long, Potential);
  void add_to_value( long, long, long, Potential);
  [[nodiscard]] Pos position( long, long, long) const;
  void set_to_zero();
  
protected:
  static constexpr int X = 0, Y = 1, Z = 2;
  
  typedef typename Units::Short_Version< Potential>::Res SPotential;
  typedef typename Vector< SPotential>::size_type size_type;

  Potential potential( Locator) const;  
  
  void shift_position( Pos);

  size_t nx,ny,nz, nyz;
  size_t nxm1,nym1,nzm1;
  Pos low_corn;
  Length hx,hy,hz;

  Vector< SPotential> data;
  
  static
  void check_scanf( const string& filename, FILE* fp,
                   unsigned int status0, unsigned int status, unsigned int line);
  
  static
  bool was_comment( const string& filename, FILE* fp);
   
  template< class T>
  T potential_of_cube( Cube<T>, double ax, double ay, double az) const;
  
  Cube< Potential> cube_of_data( long ix, long iy, long iz) const;
  [[nodiscard]] Locator locator( Pos) const;
   
  [[nodiscard]] bool in_range1( long ix, long iy, long iz) const;
};

//##############################################################
template< class Potential>
class Single_Grid: public Single_Grid_Nograd< Potential>, Single_Grid_Defs2{
public:
  
  Single_Grid( const string& filename, Pos offset, bool include_data = true);
  Single_Grid() = delete;
  Single_Grid( const Single_Grid&) = delete;
  Single_Grid& operator=( const Single_Grid&) = delete;
  Single_Grid( Single_Grid&&) = default;
  Single_Grid& operator=( Single_Grid&&) = default;
  ~Single_Grid() = default;
  
  // xml input
  explicit Single_Grid( Parser& parser);
  
  typedef decltype( Potential()/Length()) Pot_Grad;
  optional< Vec3< Pot_Grad> > gradient( const Pos& pos) const;
  
  typedef decltype( ( Pot_Grad()*Pot_Grad())) EE;
  //optional< EE> EE_potential( Pos pos) const;
  
  typedef Vec3< decltype( EE()/Length())> EE_Grad;
  optional< EE_Grad> EE_gradient( Pos pos) const;
  
private:
  size_t nxm2,nym2,nzm2;
  
  Vec3< Pot_Grad> gradient( Locator) const;
  Cube< EE> EE_cube( long ix, long iy, long iz) const;
  
  template< class T>
  Array< decltype( T()/Length()), 3> gradient_of_cube( Cube<T>, double ax, double ay, double az) const;
  
  [[nodiscard]] bool in_range2( long ix, long iy, long iz) const;
};

//###########################################################################
// constructor
template< class V>
Single_Grid< V>::Single_Grid( const string& filename, Pos offset, bool include_data):
  Single_Grid_Nograd< V>( filename, offset, include_data){
  
  nxm2 = this->nxm1 - 1;
  nym2 = this->nym1 - 1;
  nzm2 = this->nzm1 - 1;
}

// constructor
template< class V>
Single_Grid< V>::Single_Grid( Parser& parser): Single_Grid_Nograd< V>( parser){
  
  nxm2 = this->nxm1 - 1;
  nym2 = this->nym1 - 1;
  nzm2 = this->nzm1 - 1;
}

//##############################################################
template< class V>
auto Single_Grid_Nograd< V>::locator( Pos pos) const -> Locator{
  
  Length rx = pos[0] - low_corn[0];
  Length ry = pos[1] - low_corn[1];
  Length rz = pos[2] - low_corn[2];
  
  auto ix = (long)(rx/hx);
  auto iy = (long)(ry/hy);
  auto iz = (long)(rz/hz);
  
  double ax = (rx - ((double)ix)*hx)/hx;
  double ay = (ry - ((double)iy)*hy)/hy;
  double az = (rz - ((double)iz)*hz)/hz;
  
  return std::make_pair( std::make_tuple( ix,iy,iz), std::make_tuple( ax,ay,az));
}

//##############################################################
template< class V>
Pos Single_Grid_Nograd< V>::low_corner() const{
  
  return low_corn;
}

//##############################################################
template< class V>
Vec3< size_t> Single_Grid_Nograd<V>::dimensions() const{
  
  return Vec3< size_t>{ nx,ny,nz};
}

template< class V>
Pos Single_Grid_Nograd<V>::spacings() const{
  
  return Pos{ hx,hy,hz};
}

//##############################################################
template< class V>
Pos Single_Grid_Nograd< V>::high_corner() const{
  
  Pos c;
  c[X] = low_corn[X] + hx*(double)(nx-1);
  c[Y] = low_corn[Y] + hy*(double)(ny-1);
  c[Z] = low_corn[Z] + hz*(double)(nz-1);
  return c;
}

//##############################################################
/*
template< class Potential>
Single_Grid_Nograd< Potential> with_shifted_position( Single_Grid_Nograd< Potential>&& grid,
					const Pos& offset){
 
  grid.low_corn[0] += offset[0];
  grid.low_corn[1] += offset[1];
  grid.low_corn[2] += offset[2];
  
  return std::move( grid);
}
*/

//##############################################################
template< class V>
inline
void Single_Grid_Nograd<V>::check_scanf( const string& filename, FILE* fp,
                   unsigned int status0, unsigned int status, unsigned int line){
  if(ferror( fp) || (status0 != status)) {
    fclose( fp);
    error( "input error in file ", filename, " at line ", line);
  }
}
 
//############################################################## 
template< class V>
inline
bool Single_Grid_Nograd<V>::was_comment( const string& filename, FILE* fp){
  char c;
  int status = fscanf( fp, "%c", &c);
  check_scanf( filename, fp, 1, status, __LINE__);

  if (c == '#'){
    do{
      int lstatus = fscanf( fp, "%c", &c);
      check_scanf( filename, fp, 1, lstatus, __LINE__);
    }
    while (c != '\n');
    return true;
  }
  else
    return false;
}

//##############################################################
// constructor
template< class Potential>
Single_Grid_Nograd< Potential>::Single_Grid_Nograd( const string& filename, Pos offset, bool include_data){

  FILE *fp = fopen( filename.c_str(), "r");
  if (!fp){
    error( "cannot open file ", filename, " at line ", __LINE__);
  }
  
  bool comments_done;
  do{
    comments_done = !was_comment( filename, fp);
  }
  while (!comments_done);
  
  size_t nx_in, ny_in, nz_in;
  int status; // ugly hack
  status = fscanf( fp, "bject 1 class gridpositions counts %zu %zu %zu\n", 
                   &nx_in, &ny_in, &nz_in);
  
  check_scanf( filename, fp, 3, status, __LINE__);

  nx = nx_in;
  ny = ny_in;
  nz = nz_in;

  nyz = ny*nz;
  
  nxm1 = nx-1;
  nym1 = ny-1;
  nzm1 = nz-1;
  
  double val0, val1, val2;
  status = fscanf( fp, "origin %lf %lf %lf\n", 
                   &val0, &val1, &val2);
  set_fvalue( low_corn[0], val0);
  set_fvalue( low_corn[1], val1);
  set_fvalue( low_corn[2], val2);

  check_scanf( filename, fp, 3, status, __LINE__);  

  double val;
  double duma, dumb;
  status = fscanf( fp, "delta %lf %lf %lf\n", &val, &duma, &dumb);
  check_scanf( filename, fp, 3, status, __LINE__);
  set_fvalue( hx, val); 

  status = fscanf( fp, "delta %lf %lf %lf\n", &duma, &val, &duma);
  check_scanf( filename, fp, 3, status, __LINE__);
  set_fvalue( hy, val); 

  status = fscanf( fp, "delta %lf %lf %lf\n", &duma, &dumb, &val);
  check_scanf( filename, fp, 3, status, __LINE__);
  set_fvalue( hz, val); 

  unsigned int iduma, idumb, idumc;
  status = fscanf( fp, "object 2 class gridconnections counts %u %u %u\n", 
          &iduma, &idumb, &idumc);
  check_scanf( filename, fp, 3, status, __LINE__);

  status = 
    fscanf( fp, 
            "object 3 class array type double rank 0 items %u data follows", 
            &iduma);
  check_scanf( filename, fp, 1, status, __LINE__);

  if (include_data){
    auto n = nx*ny*nz;
    data.resize( n); 

    for( auto i: range( n)){
      float lval;
      status = fscanf( fp, "%f ", &lval);
      set_fvalue( data[i], lval);
      check_scanf( filename, fp, 1, status, __LINE__);
    }
  }

  fclose( fp);
  shift_position( offset);
}

//##############################################################
template< class Potential>
class Plane_Getter{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
  
  void operator()( const Node_Ptr& node){
    
    node->complete();
    auto rnodes = node->children_of_tag( "row");
    size_t iy = 0;
    for( auto& rnode: rnodes){
      auto& ndata = rnode->data();

      for (size_t iz = 0; iz < nz; iz++){
        sources.set_value( ix,iy,iz, stoul( ndata[iz]));
      }
      ++iy;
    }
    ++ix;
  }
  
  explicit Plane_Getter( Single_Grid_Nograd< Potential>& _sources):
    sources( _sources){
    
    ix = 0;
    auto ns = sources.dimensions();
    //nx = ns[0];
    //ny = ns[1];
    nz = ns[2];
  }
  
  Single_Grid_Nograd< Potential>& sources;
  //size_t nx,ny,nz;
  size_t nz;
  size_t ix;
};

//################################################
// constructor
// works only for int types
template< class Potential>
Single_Grid_Nograd< Potential>::Single_Grid_Nograd( Parser& parser){
  
  Array< std::string, 4> tags{ "corner", "npts", "spacing", "data"};
  
  auto lcorner = [&]( Node_Ptr& cnode){
    cnode->complete();
    low_corn = array_from_node< Length, 3>( cnode);
  };
  
  auto lnpts = [&]( Node_Ptr& cnode){
    cnode->complete();
    auto ns = array_from_node< size_t, 3>( cnode);
    nx = ns[X];
    ny = ns[Y];
    nz = ns[Z];
    nxm1 = nx-1;
    nym1 = ny-1;
    nzm1 = nz-1;  
    nyz = ns[Y]*ns[Z];
  };
  
  auto lspacing = [&]( Node_Ptr& cnode){
    cnode->complete();
    auto hs = array_from_node< Length, 3>( cnode);
    hx = hs[X];
    hy = hs[Y];
    hz = hs[Z];
  };
  
  
  auto ldata = [&]( Node_Ptr& cnode){
    size_t nxyz = nyz*nz;
    data.resize( nxyz);
    
    Plane_Getter pg( *this);
    parser.apply_to_nodes_of_tag( cnode, "plane", pg);
  };
  
  auto tnode = parser.top();
  auto gnode = parser.next_child_with_tag( tnode, "grid");
  
  auto success = apply_to_each_child_with_tag( gnode, tags,
                                   lcorner, lnpts, lspacing, ldata);
  
  if (!success){
    gnode->perror( "missing a tag");
  }  
}

//################################################
template< class Potential>
Potential
Single_Grid_Nograd< Potential>::value( long ix, long iy, long iz) const{
  
  size_type i = ix*nyz + iy*nz + iz;
  return data[i];
}

template< class Potential>
void
Single_Grid_Nograd< Potential>::set_value( long ix, long iy, long iz, Potential v){
  
  size_type i = ix*nyz + iy*nz + iz;
  data[i] = v;
}

template< class Potential>
void Single_Grid_Nograd< Potential>::add_to_value( long ix, long iy, long iz, Potential v){
  
  size_type i = ix*nyz + iy*nz + iz;
  data[i] += v;
}

template< class Potential>
Pos Single_Grid_Nograd< Potential>::position( long ix, long iy, long iz) const{
  
  Pos offset{ hx*(double)ix, hy*(double)iy, hz*(double)iz};
  return low_corn + offset;
}

//########################################################
template< class Potential>
auto Single_Grid_Nograd< Potential>::cube_of_data( long ix, long iy, long iz) const -> Cube< Potential>{
  
  Cube< Potential> cube;
  
  size_type i = ix*nyz + iy*nz + iz;
   
  cube[0][0][0] = data[i];
  cube[0][0][1] = data[i+1];
  cube[0][1][0] = data[i+nz];
  cube[0][1][1] = data[i+nz+1];
  
  cube[1][0][0] = data[i+nyz];
  cube[1][0][1] = data[i+nyz+1];
  cube[1][1][0] = data[i+nyz+nz];
  cube[1][1][1] = data[i+nyz+nz+1];
  
  return cube;
}

//########################################################################################
template< class Potential>
auto Single_Grid< Potential>::gradient( Locator loc) const -> Vec3< Pot_Grad>{

  auto [is,as] = loc;
  auto [ix,iy,iz] = is;
  auto [ax,ay,az] = as;
 
  auto cube = this->cube_of_data( ix,iy,iz);
  return gradient_of_cube( cube, ax,ay,az);  
}

//#####################################################################################################
template< class Potential>
template< class T>
Array< decltype( T()/Length()), 3>
Single_Grid< Potential>::gradient_of_cube( Cube<T> cube, double ax, double ay, double az) const{
   
  auto vmmm = cube[0][0][0];
  auto vmmp = cube[0][0][1];
  auto vmpm = cube[0][1][0];
  auto vmpp = cube[0][1][1];
  
  auto vpmm = cube[1][0][0];
  auto vpmp = cube[1][0][1];
  auto vppm = cube[1][1][0];
  auto vppp = cube[1][1][1];
  
  double apx = 1.0 - ax;
  double apy = 1.0 - ay;
  double apz = 1.0 - az;
  
  auto hx = this->hx;
  auto hy = this->hy;
  auto hz = this->hz;
  
  // z component
  auto gzmm = (vmmp - vmmm)/hz;
  auto gzmp = (vmpp - vmpm)/hz;
  auto gzpm = (vpmp - vpmm)/hz;
  auto gzpp = (vppp - vppm)/hz;
  
  auto gzm = apy*gzmm + ay*gzmp;
  auto gzp = apy*gzpm + ay*gzpp;
  
  Array< decltype(T()/Length()), 3> g;
  
  g[2] = apx*gzm + ax*gzp;
  
  // y component
  auto gymm = (vmpm - vmmm)/hy;
  auto gymp = (vmpp - vmmp)/hy;
  auto gypm = (vppm - vpmm)/hy;
  auto gypp = (vppp - vpmp)/hy;
  
  auto gym = apz*gymm + az*gymp;
  auto gyp = apz*gypm + az*gypp;
  
  g[1] = apx*gym + ax*gyp;
  
  // x component
  auto gxmm = (vpmm - vmmm)/hx;
  auto gxmp = (vpmp - vmmp)/hx;
  auto gxpm = (vppm - vmpm)/hx;
  auto gxpp = (vppp - vmpp)/hx;
  
  auto gxm = apz*gxmm + az*gxmp;
  auto gxp = apz*gxpm + az*gxpp;
  
  g[0] = apy*gxm + ay*gxp;
   
  return g; 
}

//##############################################################
template< class Potential>
auto Single_Grid< Potential>::gradient( const Pos& pos) const ->  optional< Vec3< Pot_Grad> > {

  auto loc = this->locator( pos);
  auto [ix,iy,iz] = loc.first;

  if (this->in_range1( ix,iy,iz)){
    return gradient( loc);
  }
  else{
    return nullopt;
  }
}

//##############################################################
template< class Potential>
Potential Single_Grid_Nograd< Potential>::potential( Locator loc) const{

  auto [is,as] = loc;
  auto [ix,iy,iz] = is;
  auto [ax,ay,az] = as;

  auto cube = cube_of_data( ix,iy,iz); 
  return potential_of_cube( cube, ax,ay,az); 
}

//##################################################################################################
template< class Potential>
template< class T>
T Single_Grid_Nograd< Potential>::potential_of_cube( Cube<T> cube, double ax, double ay, double az) const{
   
  auto vmmm = cube[0][0][0];
  auto vmmp = cube[0][0][1];
  auto vmpm = cube[0][1][0];
  auto vmpp = cube[0][1][1];
  
  auto vpmm = cube[1][0][0];
  auto vpmp = cube[1][0][1];
  auto vppm = cube[1][1][0];
  auto vppp = cube[1][1][1];
   
  double apx = 1.0 - ax;
  double apy = 1.0 - ay;
  double apz = 1.0 - az;
  
  auto vmm = apz*vmmm + az*vmmp;
  auto vmp = apz*vmpm + az*vmpp;
  auto vpm = apz*vpmm + az*vpmp;
  auto vpp = apz*vppm + az*vppp;
  
  auto vm = apy*vmm + ay*vmp;
  auto vp = apy*vpm + ay*vpp;
  
  return apx*vm + ax*vp;
}


//##############################################################
template< class Potential>
auto Single_Grid_Nograd< Potential>::potential( const Pos& pos) const -> optional< Potential>{
  
  auto loc = locator( pos);
  auto [ix,iy,iz] = loc.first;
   
  if (in_range1( ix,iy,iz)){
    return potential( loc);
  }
  else{
    return nullopt;
  }
}

//##############################################################
template< class Potential>
auto Single_Grid< Potential>::EE_cube( long ix, long iy, long iz) const -> Cube< EE>{
        
  auto nyz = this->nyz;
  auto nz = this->nz;
  
  auto pot = [&]( long jx, long jy, long jz){
    auto i = jx*nyz + jy*nz + jz;
    if constexpr (debug){
      if (i >= this->data.size()){
        error( "out of bounds ", jx, jy, jz);
      }
    }
    return this->data[i];
  };
  
  auto wx = 2.0*this->hx;
  auto wy = 2.0*this->hy;
  auto wz = 2.0*this->hz;
  Cube< EE> EEs;
  for( auto kx: range(2)){
    for( auto ky: range(2)){
      for( auto kz: range(2)){
        auto jx = ix + kx;
        auto jy = iy + ky;
        auto jz = iz + kz;
        
        auto Ex = (pot( jx+1,jy,jz) - pot( jx-1,jy,jz))/wx;
        auto Ey = (pot( jx,jy+1,jz) - pot( jx,jy-1,jz))/wy;
        auto Ez = (pot( jx,jy,jz+1) - pot( jx,jy,jz-1))/wz;
        
        EEs[kx][ky][kz] = Ex*Ex + Ey*Ey + Ez*Ez;
      }
    }
  }
  return EEs;
}

//############################################################################
template< class Potential>
bool Single_Grid< Potential>::in_range2( long ix, long iy, long iz) const{
  
  return ! ((ix >= (long)(nxm2)) || (iy >= (long)(nym2)) || (iz >= (long)(nzm2)) || 
                 (ix < 1) || (iy < 1) || (iz < 1));
}

//############################################################################
template< class Potential>
bool Single_Grid_Nograd< Potential>::in_range1( long ix, long iy, long iz) const{
  
  return ! ((ix >= (long)(nxm1)) || (iy >= (long)(nym1)) || (iz >= (long)(nzm1)) || 
                 (ix < 0) || (iy < 0) || (iz < 0));
}

//##########################################################################
/*
template< class Potential>
auto Single_Grid< Potential>::EE_potential( Pos pos) const -> optional< EE>{
  
  auto loc = this->locator( pos);
  auto [is,as] = loc;
  auto [ix,iy,iz] = is;
   
  if (in_range2( ix,iy,iz)){
    auto EEs = EE_cube( ix,iy,iz);
    auto [ax,ay,az] = as;
    return potential_of_cube( EEs, ax,ay,az);   
  }
  else{
    return nullopt;
  }
}
*/
//##############################################################
template< class Potential>
auto Single_Grid< Potential>::EE_gradient( Pos pos) const -> optional< EE_Grad> {
  
  auto loc = this->locator( pos);
  auto [is,as] = loc;
  auto [ix,iy,iz] = is;
   
  if (in_range2( ix,iy,iz)){
    auto EEs = EE_cube( ix,iy,iz);
    auto [ax,ay,az] = as;
    return gradient_of_cube( EEs, ax,ay,az);   
  }
  else{
    return nullopt;
  } 
}

//##############################################################
template< class Potential>
void Single_Grid_Nograd< Potential>::shift_position( Pos offset){
  
  low_corn += offset;
}

template< class V>
void Single_Grid_Nograd< V>::set_to_zero(){
  data.fill( V{ 0.0});
}

}

//********************************************************
// test code
/*
class GInterface{
public:
  typedef ::Potential Potential;
  typedef ::Pot_Grad Pot_Grad;

};

namespace JP = JAM_XML_Pull_Parser;

int main(){
  std::ifstream input( "bull");
  JP::Parser parser( input);

  Single_Grid_Nograd::Grid< GInterface> grid( parser);

  Length x[3];
  Pot_Grad f[3];
  bool in_range;
  grid.get_gradient( x, f, in_range);
  Potential v;
  grid.get_potential( x, v, in_range);

  grid.invert_data();

  return 0;
}
*/

