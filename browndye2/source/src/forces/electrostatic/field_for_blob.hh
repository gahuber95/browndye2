#pragma once

#include "field.hh"
#include "../../generic_algs/collision_detector.hh"

namespace Browndye{

template< class CInterface_In, class FInterface_In>
struct Field_For_Blob{
  typedef CInterface_In CInterface;
  typedef FInterface_In FInterface;

  typedef Field::Field< FInterface> Field;
  typedef Collision_Detector::Structure< CInterface> Collision_Structure;
  typedef typename Field::Hint Hint;

  const Field& field;
  Collision_Structure& collision_structure;
  Hint& hint;

  Field_For_Blob( const Field& _field, Collision_Structure& cs, Hint& _hint):
    field(_field), collision_structure( cs), hint( _hint){}
};


template< class CInterface, class FInterface>
Field_For_Blob< CInterface, FInterface>
new_field_for_blob( const Field::Field< FInterface>& field, Collision_Detector::Structure< CInterface>& cs, typename Field::Field< FInterface>::Hint& hint){
  return Field_For_Blob< CInterface, FInterface>( field, cs, hint);
}

}

