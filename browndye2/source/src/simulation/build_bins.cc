/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that is called for build_bins

#include "we_simulator.hh"
#include "simulation.hh"

int main( int argc, char* argv[]){

  if (argc < 2){
    Browndye::error( "build_bins: need input file name");
  }
  const char* input = argv[1];
  Browndye::Simulation simulation( input);

  Browndye::WE_Simulator wes( simulation.top_node, true);

  wes.generate_bins();
}
