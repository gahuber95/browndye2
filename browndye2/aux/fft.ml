(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* Computes the autocorrelation of the input using the
Fast Fourier Transform.  The output autocorrelation is
1/10 the length of the input data *)

let bit_reverse nn xin =
  let x = ref xin in
  let n = ref 0 in
    for i = 0 to nn-1 do (
      n := !n lsl 1;
      n := !n lor (!x land 1);
      x := !x lsr 1;
    )
    done;
    !n


module I = Int64

let log2n iin = 
  let n = 64 in
  let i = I.of_int iin in
  let j = ref (n-1) in
    while (I.logand i (I.shift_left I.one !j)) = I.zero do
      j := !j - 1
    done;
    !j



let pi = 3.1415926535897

module C = Complex

let new_c r i = 
  {
    C.im = i;
    C.re = r;
  }


let copy c = {
  C.re = c.C.re;
  C.im = c.C.im;
}


let f is_inv a aa = 
  let sgn = if is_inv then -1.0 else 1.0 in
  let log2_n = log2n (Array.length a) in
  let nn = 1 lsl log2_n in
    for i = 0 to nn-1 do
      aa.( (bit_reverse log2_n i)) <- a.(i);
    done;
    
    for s = 1 to log2_n do
      let m = 1 lsl s in
      let w = ref (new_c 1.0 0.0) in
      let fm = 2.0*.pi/.(float m) in
      let wm = new_c (cos fm) (sgn*.(sin fm)) in
	for j = 0 to (m/2)-1 do
	  let k = ref j in
	    while !k < nn do 
	      let t = C.mul !w  aa.(!k + m/2)
	      and u = copy aa.(!k) in
		aa.(!k) <- C.add u t;
		aa.(!k + m/2) <- C.sub u t;
		k:= !k + m	    
	    done;
	    w := C.mul !w wm;
	done;
    done;

    if is_inv then
      let fact = new_c (1.0/.(float nn)) 0.0 in
	for i = 0 to nn-1 do
	  aa.(i) <- C.mul fact aa.(i)
	done

let sq x = x*.x

(* computes autocorrelation for 1/10 of the data length *)
let autocorrelation a =
  let n = Array.length a in
  let fn = float n in
  let mean = 
    let sum = Array.fold_left (+.) 0.0 a in
      sum/.fn
  in
  let s2 = 
    Array.fold_left 
      (fun sum x -> sum +. (sq (x -. mean))) 0.0 a 
  in
 
  let pn = n + n/10 in
  let l2n = log2n pn in
  let dn = 1 lsl (l2n+1) in
  let pa = Array.init dn 
    (fun i -> 
      if i < n 
      then 
	new_c (a.(i) -. mean) 0.0 
      else 
	new_c 0.0 0.0
    ) 
  in
  let aa = Array.init dn (fun i -> new_c 0.0 0.0) in
    f false pa aa;
    for i = 0 to dn-1 do
      aa.(i) <- C.mul aa.(i) (C.conj aa.(i));
    done;
    let fauto = Array.init dn (fun i -> new_c 0.0 0.0) in
      f true aa fauto;
      
      Array.init (n/10) (fun i -> fauto.(i).C.re/.s2)




(**********************************************)
(* Test code *)
(*
let fp = Scanf.Scanning.from_file "bull2.dat";;

let ldata = ref [];;

try
  while true do
    Scanf.bscanf fp "%g\n" (fun x -> ldata := x :: (!ldata));
  done;

with End_of_file -> ()

let data = Array.of_list (List.rev !ldata);;

let autoc = autocorrelation data;;

Array.iter (fun x -> Printf.printf "%g\n" x) autoc;;
*)

(*


let z = new_c 0.0 0.0;;
let one = new_c 1.0 0.0;;

let a = [| one; z; z; z; z; z; z; one |];;

let aa = Array.init 8 (fun i -> (new_c nan nan));;

f false a aa;;

Array.iter
  (fun x -> Printf.printf "%f %f\n" x.C.re x.C.im)
  aa
;;

f true aa a;;

Array.iter
  (fun x -> Printf.printf "%f %f\n" x.C.re x.C.im)
  a
;;

*)
