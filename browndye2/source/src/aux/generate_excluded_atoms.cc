#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
//#include "../global/indices.hh"

namespace {
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
  
  struct AtomD{
    size_t i;
    std::optional< string> core;
    
    std::tuple< size_t, const std::optional< string>&> rep() const{
      return std::make_tuple( i, std::ref( core));
    }
    
    bool operator==( const AtomD& a1) const{
      auto r0 = rep();
      auto r1 = a1.rep();
      return r0 == r1;
    }
    
    bool operator<( const AtomD& a1) const{
      auto r0 = rep();
      auto r1 = a1.rep();
      return r0 < r1;
    }
    
  };
  
  typedef Array< AtomD, 2> APair;
  typedef Vector< APair> APairs;
  typedef Vector< AtomD> AtomDs;
  
  //###################################################
  APairs
  bonds_of_node( JP::Node_Ptr node, const string& tag){
    
    auto ptag = tag + "s";
    auto bonds_node = node->child( ptag);
    auto bond_nodes = bonds_node->children_of_tag( tag);
    
    APairs bonds;
    
    for( auto& bnode: bond_nodes){
      auto atomis = checked_array_from_node< size_t, 2>( bnode, "atoms");
      auto cores_opt = array_from_node< string, 2>( bnode, "cores");
      APair bond;
      bond[0].i = atomis[0];
      bond[1].i = atomis[1];
      if (cores_opt){
        auto& cores = cores_opt.value();
        if (cores[0] != "*"){
          bond[0].core = cores[0];
        }
        if (cores[1] != "*"){
          bond[1].core = cores[1];
        }
      }
      bonds.push_back( bond);
    }
    
    return bonds;
  }
  
  //#####################################################
  APairs pairs13_of( const APairs& bonds){
    
    APairs pairs13;
    auto nb = bonds.size();
    for( auto i: range( nb)){
      auto& bondi = bonds[i];
      auto& atomi0 = bondi[0];
      auto& atomi1 = bondi[1];
      //cout << "bond " << atomi0.i << ' ' << atomi1.i << endl;
      for( auto j: range(i)){
        auto& bondj = bonds[j];
        auto& atomj0 = bondj[0];
        auto& atomj1 = bondj[1];
        
        auto pushp = [&]( const AtomD& a0, const AtomD& a1){
          
          auto pair = [&]{
            if (a0 < a1){
              return APair{ a0, a1};
            }
            else{
              return APair{ a1, a0};
            }
          }();
          
          pairs13.push_back( pair);
        };
        
        if (atomi0 == atomj0){
          pushp( atomi1, atomj1);
        }
        else if (atomi0 == atomj1){
          pushp( atomi1, atomj0);
        }
        else if (atomi1 == atomj0){
          pushp( atomi0, atomj1);
        }
        else if (atomi1 == atomj1){
          pushp( atomi0, atomj0);
        }
      }
    }
    return pairs13;
  }  
  
  //#####################################################
  APairs pairs14_of( const APairs& bonds, const APairs& pairs13){
    
    APairs pairs14;
  
    for( auto& bondi: bonds){
      auto& atomi0 = bondi[0];
      auto& atomi1 = bondi[1];
      for( auto& bondj: pairs13){
        auto& atomj0 = bondj[0];
        auto& atomj1 = bondj[1];
        
        auto pushp = [&]( const AtomD& a0, const AtomD& a1){
          Array< AtomD, 2> pair{ a0, a1};
          pairs14.push_back( pair);
        };
        
        if (atomi0 == atomj0){
          pushp( atomi1, atomj1);
        }
        else if (atomi0 == atomj1){
          pushp( atomi1, atomj0);
        }
        else if (atomi1 == atomj0){
          pushp( atomi0, atomj1);
        }
        else if (atomi1 == atomj1){
          pushp( atomi0, atomj0);
        }
      }
    }
    return pairs14;
  }
  
  //#########################################################
  void main0( const string& file){
    std::ifstream input( file);
    JP::Parser parser( input, file);
    
    auto top = parser.top();
    top->complete();
    
    auto bonds = bonds_of_node( top, "bond");
    auto bondsb = bonds_of_node( top, "length_constraint");
    auto bondsr = bonds_of_node( top, "length_constraintr");
    
    bonds.insert( bonds.end(), bondsb.begin(), bondsb.end());
    bonds.insert( bonds.end(), bondsr.begin(), bondsr.end());
    
    auto remove_dups = []( APairs& pairs){
      auto b = pairs.begin();
      auto e = pairs.end();
      std::sort( b, e);
      auto new_end = std::unique( b, e);
      pairs.erase( new_end, e);
    };
    
    auto pairs13 = pairs13_of( bonds);
    remove_dups( pairs13);
    
    auto pairs14 = pairs14_of( bonds, pairs13);
    remove_dups( pairs14);
    
    std::map< AtomD, AtomDs > ex_atoms; 
    
    auto add_to_dict = []( std::map< AtomD, AtomDs>& ex_atoms,
                            const APairs& bonds){
      for( auto& pair: bonds){
        auto& a0 = pair[0];
        auto& a1 = pair[1];
        auto& atoms0 = ex_atoms[a0];
        atoms0.push_back( a1);
        auto& atoms1 = ex_atoms[a1];
        atoms1.push_back( a0);
      }
    };
    
    add_to_dict( ex_atoms, bonds);
    //add_to_dict( ex_atoms, pairs13);
        
    std::map< AtomD, AtomDs> core_ex_atoms;
    for( auto& entry: ex_atoms){
      auto& key = entry.first;
      if (key.core){
        auto& atoms = entry.second;
        auto new_entry = std::make_pair( key, atoms.copy());
        core_ex_atoms.insert( std::move( new_entry));
      }
    }
    
    std::map< AtomD, AtomDs> chain_ex_atoms;
    for( auto& entry: ex_atoms){
      auto& key = entry.first;
      if (!key.core){
        auto ia = key.i;
        AtomDs new_atoms;
        auto& atoms = entry.second;
        for( auto& atom: atoms){
          if (!atom.core && atom.i < ia){
            new_atoms.push_back( atom);
          }
        }
        chain_ex_atoms.insert( std::make_pair( key, std::move( new_atoms)));
      }
    }
    
    cout << "<excluded>\n";
    for( auto& entry: chain_ex_atoms){
      auto& key = entry.first;
      auto& atoms = entry.second;
      cout << "  <chain_atom>\n";
      cout << "    <atom> "<< key.i << " </atom>\n";
      cout << "    <ex_atoms> ";
      for( auto& atom: atoms){
        cout << atom.i << ' ';
      }
      cout << "</ex_atoms>\n";
      cout << "  </chain_atom>\n";
    }
    
    cout << "  <core>\n";
    auto name = core_ex_atoms.begin()->first.core.value();
    cout << "    <name> " << name << " </name>\n";
    for( auto& entry: core_ex_atoms){
      auto& key = entry.first;
      auto& atoms = entry.second;
     
      cout << "    <core_atom>\n";
      cout << "      <atom> "<< key.i << " </atom>\n";
      cout << "      <ex_atoms> ";
      for( auto& atom: atoms){
        cout << atom.i << ' ';
      }
      cout << "</ex_atoms>\n";
      cout << "    </core_atom>\n";
    }
    cout << "  </core>\n";
    
    cout << "</excluded>\n";
    
  }
  
  
}

//##########################
int main(){
  main0( "tchain.xml");
}
