#include "born_grid.hh"
#include "../forces/electrostatic/single_grid.hh"
#include "../xml/node_info.hh"
#include "../forces/other/blank_nonbonded_parameter_info.hh"

namespace{

using namespace Browndye;

enum Coord {X,Y,Z};
const Vec3< size_t> ones{ 1, 1, 1};
//constexpr Length L1{ 1.0};

template< class T>
auto inv3( Vec3< T> v){
  
  return v.mapped( [&]( T t){ return 1.0/t;});
}

template< class T>
auto tprod( Vec3< T> v){
  return v[X]*v[Y]*v[Z];
}

template< class T>
T flrep( T t){
  return t;
}

//double flrep( size_t t){
//  return (double)t;
//}

using std::max;

template< class Ta, class Tb>
auto kprod( Vec3< Ta> a, Vec3< Tb> b){
  
  auto fa = a.mapped( []( Ta t){ return flrep(t);});
  auto fb = b.mapped( []( Tb t){ return flrep(t);});
  
  typedef std::decay_t< decltype( fa[0])> Tfa;
  typedef std::decay_t< decltype( fb[0])> Tfb;
  
  typedef decltype( Tfa()*Tfb()) Tab;
  
  return Vec3< Tab>{ fa[X]*fb[X], fa[Y]*fb[Y], fa[Z]*fb[Z]}; 
}

}

//#################################################################
namespace Browndye{
  
  // constructor
  Born_Grid::Born_Grid( const std::string& dfile, const std::string& afile): rgrid( dfile, Pos( Zeroi{})){
    
    rgrid.set_to_zero();
    debye_length = Length{ large};
    Blank_Nonbonded_Parameter_Info pinfo;
    atoms = pinfo.atoms_from_file( afile);
  }
  
  // constructor
  Born_Grid::Born_Grid( const std::string& dfile, Parser& parser): rgrid( dfile, Pos( Zeroi{})){
    
    rgrid.set_to_zero();
    debye_length = Length{ large};
    sgrid = std::make_unique< Single_Grid_Nograd< unsigned int> >( parser);
    svolume = tprod( sgrid->spacings());
  }
  
  //######################################################################
  /*
  uint Grid::input( size_t ix, size_t iy, size_t iz) const{
    return data[ iz + ns[Z]*iy + nyz*ix];
  }
  
  uint& Grid::input( size_t ix, size_t iy, size_t iz){
    return data[ iz + ns[Z]*iy + nyz*ix];
  }
  
  void Grid::add_to_result( size_t ix, size_t iy, size_t iz, Inv_Length V){
    auto nrz = receivers.ns[Z];
    auto nryz = receivers.nyz;
    receivers.data[ iz + nrz*iy + nryz*ix] += V;
  }
  
  void Grid::set_result( size_t ix, size_t iy, size_t iz, Inv_Length V){
    auto nrz = receivers.ns[Z];
    auto nryz = receivers.nyz;
    receivers.data[ iz + nrz*iy + nryz*ix] = V;
  }
  
  Inv_Length Grid::result( size_t ix, size_t iy, size_t iz) const{
    auto nrz = receivers.ns[Z];
    auto nryz = receivers.nyz;
    return receivers.data[ iz + nrz*iy + nryz*ix];
  }
 */   
  
}
