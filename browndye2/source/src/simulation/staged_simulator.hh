#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
NAM_Simulator reads in the data, carries out a one-trajectory-at-a-time
simulation, and outputs data.  Also takes care of the multithreading. 
(NAM stands for Northrup-Allison-McCammon, even though their
original algorithm is not used here.
*/



#include <mutex>
#include "single_traj_simulator.hh"
#include "nam_multi_interface.hh"

namespace Browndye{

class Stager;

class Staged_Simulator: public Single_Traj_Simulator{
public:
  explicit Staged_Simulator( JAM_XML_Pull_Parser::Node_Ptr);
  Staged_Simulator() = delete;
  Staged_Simulator( const Staged_Simulator&) = delete;
  Staged_Simulator( Staged_Simulator&&) = delete;
  Staged_Simulator& operator=( const Staged_Simulator&) = delete;
  Staged_Simulator& operator=( Staged_Simulator&&) = delete;

  //void initialize( const std::string& file);

  //void set_number_of_trajectories( size_t);
  void run() override;
  
private:
  friend class Staged_Multi_Interface;

  Stager run_individual( System_State&);
  void run_trajectory( System_State&, Stager&);
  //void output_results( const System_State&);

  //void print_state( const System_State&,
  //                std::ofstream&, bool state_change ) const;
  Stager run_thread( System_State* state);
  //void check_stuck( System_State& state, bool& finished);
  //void check_done( System_State& state, bool& finished);
 
  //size_t n_trajs = 0;
  //size_t n_stuck = 0;
  //size_t n_escaped = 0;
  //size_t n_steps_per_output = 1;
  //size_t n_output_states_per_block = 1000;
  //size_t max_n_steps = maxi;
  //size_t n_trajs_per_output = 1;
  //std::string traj_file_name;
  //bool do_output_rng_state = false;
  //std::mutex mutex;
  //bool has_error = false;
  //std::exception_ptr except;

  //size_t n_trajs_completed = 0;
  //size_t n_trajs_started = 0;
  //size_t n_bd_steps = 0;
};

//**************************************************
/*
template< class Func>
void NAM_Multi_Interface::apply_to_object_refs( Func& f, NAM_Simulator* sim){
  
  for( auto& system: sim->systems)
    f( &system);  
} 

inline
size_t NAM_Multi_Interface::size( NAM_Simulator* sim){
  return sim->systems.size();
}
*/
}

/* 

Input:

information for Molecule_Pair

number of trajectories
number of steps per output
maximum number of steps
output file
optional random number seed

Output:

number reacted vs. number of trajectories

*/

