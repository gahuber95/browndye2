#pragma once
#include "../lib/vector.hh"

namespace Browndye{

template< class T, class Index>
class Symmetric_Skyline_Matrix;

//######################################################3
template< class T, class Indexx>
class SSM_Interface{
public:
  typedef Indexx Index;
  typedef Symmetric_Skyline_Matrix< T, Index> Matrix;
  typedef T Matrix_Value;
  typedef decltype( sqrt(T(1.0))) Sqrt_T;
  typedef Sqrt_T Dec_Matrix_Value;
  typedef Symmetric_Skyline_Matrix< Sqrt_T, Index> Dec_Matrix;
  
  template< class U>
  static
  Index size( const Symmetric_Skyline_Matrix< U, Index>& mat){
    return mat.size();
  }

  template< class U>
  static
  Index bandwidth( const Symmetric_Skyline_Matrix< U, Index>& mat, 
                   Index i){
    return mat.bandwidth( i);
  }

  template< class U>
  static
  U element( const Symmetric_Skyline_Matrix< U, Index>& mat, 
             Index i, Index j){
    return mat( i,j);
  }

  template< class U>
  static
  U& element_ref( Symmetric_Skyline_Matrix< U, Index>& mat, 
                  Index i, Index j){
    return mat( i,j);
  }

  template< class U>
  static
  U element( const Vector< U, Index>& vec, Index i){
    return vec[i];
  }
  
  template< class U>
  static
  U& element_ref( Vector< U, Index>& vec, Index i){
    return vec[i];
  }
  
  template< class Vec>
  class Value_Type{
  public:
    typedef typename Vec::value_type Result;
  };

  template< class U>
  class Vector_Type{
  public:
    typedef Vector< U, Index> Result;
  };

  template< class U>
  static
  void resize( Vector< U, Index>& v, Index n){
    v.resize( n);
  }

  template< class U>
  static
  U transpose( U u){
    return u;
  }

  template< class U>
  static
  bool is_negative( U u){
    return u <= U( 0.0);
  }

  template< class U>
  static
  bool is_zero( U u){
    return u <= U( 0.0);
  }

  static
  void error( const char* msg){
    Browndye::error( msg);
  }

};

}
