#include "node_checker.hh"
#include "../lib/slist.hh"

namespace Browndye{
   
  //###################################################
  std::pair< SList< std::string>, SList< std::string> >
  Tag::spurious_n_used( std::list< Node_Ptr>& nchildren) const{
    
    SList< string> nnames;
    for( auto& child: nchildren){
      nnames = appended( child->tag(), nnames);
    }
    nnames = with_no_dups( sorted( nnames));   
       
    SList< string> tnames;
    for( auto& tptr: children){
      tnames = appended( tptr->name, tnames);
    }
     
    auto spurious = set_difference( nnames, tnames);
    
    auto used_tags = set_intersection( nnames, tnames);
    return std::make_pair( spurious, used_tags);
  }
  
  //#########################################################
  const JAM_XML_Pull_Parser::Node_Ptr&
  Tag::child_node_of_tag( const std::list< Node_Ptr>& nchildren,
                         const string& utag) const{
    
    auto itr = std::find_if( nchildren.begin(), nchildren.end(),
                [&]( const Node_Ptr& child){
        return child->tag() == utag;
      });
    return *itr; 
  }
  
  //##################################################################
  const Tag& Tag::child_tag_of_tag( const string& utag) const{
    
    auto itr = std::find_if( children.begin(), children.end(),
                       [&]( const std::shared_ptr< Tag>& tagp){
            
      return tagp->name == utag;
    });
    
    return **itr;
  }
  
  //######################################################
  void Tag::check_node( Node_Ptr node) const{

    auto& nname = node->tag();      
    auto nchildren = node->all_children();
      
    auto [spurious, used_tags] = spurious_n_used( nchildren);  
         
    for( auto& stag: spurious){
      std::cerr << "Warning: Node " << stag << " in " << nname << " not used\n";
    }
      
    for( auto& utag: used_tags){
      auto& child = child_node_of_tag( nchildren, utag);
      auto& ctag = child_tag_of_tag( utag);
      ctag.check_node( child);
    }
  }
}
