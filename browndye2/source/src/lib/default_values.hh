#pragma once

#include <vector>
#include <array>
#include <limits>
#include <complex>

//*********************************************************************
namespace Browndye{
namespace JAM_Vector{

template< class T, bool has_quiet_nan, bool has_inf, bool is_bounded>
class DVV{
public:
  static
  T value(){
    return T();
  }
  
};


template< class T, bool has_inf, bool is_bounded>
class DVV< T, true, has_inf, is_bounded>{
public:
  static
  T value(){
    return T( std::numeric_limits< T>::quiet_NaN());
  }  
};

template< class T, bool is_bounded>
class DVV< T, false, true, is_bounded>{
public:
  static
  T value(){
    return T( std::numeric_limits< T>::infinity());
  }  
};

template< class T>
class DVV< T, false, false, true>{
public:
  static
  T value(){
    return T( std::numeric_limits<T>::max());
  }

};

/*
template< class T, size_t n>
class Array;

template< class T, size_t n>
class DVV< Array< T, n>, false, false, false>{
public:
  static
  Array< T,n> value(){
    return Array< T,n>();
  }

};
*/


template< class T>
class Default_Value{
public:
  
  static const bool has_quiet_nan = std::numeric_limits< T>::has_quiet_NaN;
  static const bool has_infinity = std::numeric_limits< T>::has_infinity;
  static const bool is_bounded = std::numeric_limits< T>::is_bounded;

  typedef DVV< T, has_quiet_nan, has_infinity, is_bounded> DVVA;
 
  static
  T value(){
    return DVVA::value();
  }

};

template< class T>
class DVV< std::complex<T>, false, false, false>{
public:
  static
  std::complex<T> value(){
    typedef Default_Value< T> DT;
    return std::complex<T>{ DT::value(), DT::value()};
  }

};

}
}

