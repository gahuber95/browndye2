#pragma once
#include <map>
//#include <z3.h>

#include "../global/pos.hh"
#include "../global/physical_constants.hh"
#include "../lib/range.hh"
#include "../transforms/transform.hh"
#include "../transforms/blank_transform.hh"
#include "../generic_algs/collision_detector.hh"
#include "../structures/solvent.hh"
#include "../generic_algs/compute_bond_distance_gen.hh"
#include "../generic_algs/compute_bond_angle_gen.hh"
#include "../generic_algs/compute_torsion_gen.hh"
#include "other/bonded_interactions_interfaces.hh"
#include "../lib/class_selector.hh"
#include "electrostatic/screened_coulomb.hh"
//#include "absinth/absinth_iface.hh"
#include "parameters.hh"
#include "excluded_flags.hh"
#include "share_tet.h"

/* Cases to consider

Mol-Chain:
  immobile - frozen
  immobile - free
  mobile - frozen
  mobile - free

Mol-Mol:
  immobile - mobile
  mobile - mobile

Chain-Chain
  frozen - frozen
  free - free
  frozen - free

Idea:
  Transform everything to Core00's frame, compute forces and torques,
  and rotate them all back at the end.
  
  For chain, compute forces on atoms whether frozen or not
  For core, compute torques about hydro center
 */

namespace Browndye{

  void alt_test_force_balance(const System_State &state, int flag);


//#define force_and_torque0( force1, torque1, r01) make_tuple( -force1, -cross( r01, force1) - torque1)

template< class I, bool ce>
class Force_Computer;

template< class I, bool ce>
void compute_all_forces( const typename I::PInfo& pinfo, const typename I::BPInfo& bpinfo, const Solvent& solvent, typename I::System_State& state){
  Force_Computer< I, ce> fc( solvent, pinfo, bpinfo, state);
  fc.doit();
}

//######################################################
template< class I, bool compute_energy>
class Force_Computer{
public:
  typedef typename I::PInfo PInfo;
  typedef typename I::BPInfo BPInfo;
  typedef typename I::System_State System_State;
  
  Force_Computer( const Solvent&, const PInfo&, const BPInfo&, System_State&);
  void doit();
  
  Energy V{ 0.0}; // for now, just intrachain COFFDROP
  
private:
  // data
  System_State& sys;
  typedef Parameters< PInfo, BPInfo> Params;
  Params params;

  static constexpr Core_Index m0{0}, m1{1};
  static constexpr Group_Index g0{0}, g1{1};
  //static constexpr Atom_Index maxats{ maxi};
  
  // class definitions
  
  class System_State_Transformer{
  public:
    System_State& state;
    Transform tform0;
    
    explicit System_State_Transformer( System_State&);
    ~System_State_Transformer();
    
    System_State_Transformer( const System_State_Transformer&) = delete;
    System_State_Transformer& operator=( const System_State_Transformer&) = delete;
    System_State_Transformer( System_State_Transformer&&) = default;
    System_State_Transformer& operator=( System_State_Transformer&&) = default;
  };
  
  class Local_Force_Computer_Base{
  public:
    typedef std::map< Atom_Index, Vector< Atom_Index> > Excluded_Atoms;
    
    Local_Force_Computer_Base( System_State&, const PInfo&, const Solvent&,
                               Pos, const Excluded_Atoms&, const Excluded_Atoms&);
    
    System_State& state;
    const PInfo& pinfo;
    const Solvent& solvent;
    const Pos center1;
    F3 force1 = F3( Zeroi{});
    T3 torque1 = T3( Zeroi{});
    
    // maps atom0 to atoms 1
    const Excluded_Atoms &excluded_atoms, &one_four_atoms;
  };  
  
  template< class I0, class I1>
  class Local_Force_Computer: public Local_Force_Computer_Base{
  public:
    typedef std::map< Atom_Index, Vector< Atom_Index> > Excluded_Atoms;
    typedef typename I0::Container Container0;
    typedef typename I1::Container Container1;
    typedef typename I0::Refs Refs0;
    typedef typename I1::Refs Refs1;
    typedef typename I0::Ref Ref0;
    typedef typename I1::Ref Ref1;
    typedef typename I0::Transform Transform0;
    typedef typename I1::Transform Transform1;
    static constexpr bool compute_exclusions = !I0::is_chain & I1::is_chain;
  
    Local_Force_Computer( System_State&, const PInfo&, const Solvent&, Pos,
                          const Excluded_Atoms&, const Excluded_Atoms&);
  
    void operator()( const Transform0& tform0, Container0& cont0, Refs0& refs0, Pos, Length,
                     const Transform1& tform1, Container1& cont1, Refs1& refs1, Pos, Length);
    
    void get_direct_exclusions( Container0& cont0, Ref0 i0, Refs1& refs1,
                               Vector< bool, Atom_Index>&) const;
    void get_one_four_exclusions( Container0& cont0, Ref0 i0, Refs1& refs1,
                                 Vector< bool, Atom_Index>&) const;
    
    double final_exclusion_factor( const Vector< bool, Atom_Index>& excluded,
                                    const Vector< bool, Atom_Index>& one_four, Ref1 i1);
    
    void add_forces( Container0& cont0, Container1& cont1, Ref0 i0, Ref1 i1, Pos tpos1, F3 F);
  };  
  
  template< class C>
  using Col_Struct = typename Collision_Detector::Structure< C>;
  
  // private functions
  typedef typename I::Group_State Group_State;
  typedef typename I::Chain_State Chain_State;
  typedef typename I::Chain_Common Chain_Common;
  typedef typename I::Core_State Core_State;
  //typedef typename I::Core_Thread Core_Thread;
  //typedef typename I::Group_Thread Group_Thread;
  
  void setup_chain_collision_infos();
  void compute_all_intragroup_forces();
  void compute_all_intergroup_forces();
  void compute_intragroup_forces( bool is_group0, Group_State&);
  
  template< bool is_mobile0>
  void add_core_forces( Core_State&, Core_State&);
  
  template< bool is_mobile0>
  void add_core_chain_forces( Core_State& state0, Chain_State& cstate1);
  
  template< bool is_mobile0>
  void add_core_chain_electrostatic_forces( 
                               Core_State& state0, Chain_State& cstate1);
  
  template< bool is_mobile0>
  std::tuple< F3, T3> core_chain_nonelectrostatic_forces( 
                               Core_State& state0,
                               Chain_State& cstate1);
  
  typedef std::map< Atom_Index, Vector< Atom_Index> > Ex_Atom_Map;
  
  std::tuple< const Ex_Atom_Map&, const Ex_Atom_Map&>
  excluded_maps_of_core( const Ex_Atom_Map&, const Chain_Common& chain, Core_Index mi) const;
     
  template< bool is_mobile0, bool is_mobile1>
  std::tuple< F3, T3>
  core_desolvation_force_on_1( Core_State&, Core_State&);
  
  template< bool is_mobile0>
  std::tuple< F3, T3> core_electrostatic_force_on_1( Core_State&, Core_State&);
  
  template< bool is_mobile0>
  std::tuple< F3, T3>
  core_local_force_on_1( Core_State&, Core_State&);
  
  void add_chain_coulombic_forces( Chain_State& cstate0, Chain_State& cstate1);
  
  template< class CI0, class CI1>
  std::tuple< F3, T3 > local_interactions( Pos center1,
                                          Col_Struct< CI0>& cs0,
                                           typename CI0::Transform tform0,
                                          Col_Struct< CI1>& cs1,
                                           typename CI1::Transform tform1);
  
  template< class CI0, class CI1>
  std::tuple< F3, T3 > local_interactions_with_exclusions( Pos center1,
                                                Col_Struct< CI0>& cs0,
                                                const typename CI0::Transform& tform0,
                                                Col_Struct< CI1>& cs1,
                                               const typename CI1::Transform& tform1,
                                        const std::map< Atom_Index, Vector< Atom_Index> >& excluded,
                                        const std::map< Atom_Index, Vector< Atom_Index> >& one_four);
  
  void add_local_chain_forces( 
                              Chain_State& cstate0, Chain_State& cstate1);
  
  void add_intergroup_forces( bool is_group0, Group_State&, Group_State&);
  void add_intergroup_core_core_forces( bool is_group0,
                                       Group_State&, Group_State&);
  void add_intergroup_core_chain_forces( bool is_group0,
                                         Group_State&, Group_State&);
  void add_intergroup_chain_chain_forces( Group_State&, Group_State&);
  
  void compute_density_grid_forces();

  static
  std::tuple< F3, T3> force_and_torque0( F3 force1, T3 torque1, Pos r);

  template< bool is_mobile>
  static
  Pos center( const Core_State& state);
  
  template< bool is_mobile0>
  static
  decltype( auto) transform0( const Core_State& state0);
  
  template< class C0, class FI>
  using Field_For_Blob = typename I:: template Field_For_Blob< C0, FI>;
  
  template< class FI>
  using Blob_Interface = typename I:: template Blob_Interface< FI>;
  
  template< class FI>
  using Blob = typename I:: template Blob< 4, Blob_Interface< FI> >;
  
  template< class C0, class FI, class TForm0, class TForm1>
  static
  std::tuple< F3, T3>
  cheby_and_grid_forces1( const Field_For_Blob< C0, FI>& field0, 
                         const Blob< FI>& blob1,
                         const TForm0& tform0, const TForm1& tform1,
                         double factor);
  
  class Group_Force_Computer{
  public:
    Group_Force_Computer( Force_Computer& fc, bool _is_group0,
                         Group_State& _state):
      is_group0(_is_group0), force_computer( fc), 
      state(_state){}
    
    void doit();
    
  private:
    const bool is_group0;
    Force_Computer& force_computer;
    Group_State& state;
        
    // private functions
    void clear_group_forces();
    void add_intragroup_core_forces();
    void add_intragroup_interchain_forces();
    void add_intrachain_forces( Chain_State& cstate);
    void add_intragroup_core_core_forces();
    void add_intragroup_core_chain_forces();
    void add_intrachain_nonbonded_forces( Chain_State& cstate);
    void add_intrachain_bonded_forces( Chain_State& cstate);
    void add_force_to_core_atom( F3 F, Core_Index mi, Atom_Index ai);
    void add_forces_from_bonds( Chain_State& cstate);
    void add_forces_from_angles( Chain_State& cstate);
    void add_forces_from_dihedrals( Chain_State& cstate);
  };

};

//###################################################
/*
template< class I, bool ce>
constexpr Group_Index Force_Computer< I,ce>::g0;

template< class I, bool ce>
constexpr Group_Index Force_Computer< I,ce>::g1;

template< class I, bool ce>
constexpr Core_Index Force_Computer< I,ce>::m0;

template< class I, bool ce>
constexpr Core_Index Force_Computer< I,ce>::m1;
*/

//template< class I, bool ce>
//constexpr Atom_Index Force_Computer< I,ce>::maxats;

//################################################################
// constructor
template< class I, bool ce>
template< class I0, class I1>
Force_Computer< I,ce>::Local_Force_Computer< I0, I1>::
Local_Force_Computer( System_State& state, const PInfo& pinfo, const Solvent& solvent, Pos center1,
                    const Excluded_Atoms& excluded, const Excluded_Atoms& one_four):
Local_Force_Computer_Base( state, pinfo, solvent, center1, excluded, one_four){}

//################################################################
// constructor
template< class I, bool ce>
Force_Computer< I,ce>::
Local_Force_Computer_Base::
  Local_Force_Computer_Base( System_State& _state, const PInfo& _pinfo, const Solvent& _solvent,
                         Pos _center1, const Excluded_Atoms& _excluded,
                            const Excluded_Atoms& _one_four):
    state(_state), pinfo( _pinfo), solvent(_solvent), center1(_center1),
    excluded_atoms(_excluded), one_four_atoms(_one_four){}

//#################################################################
// constructor
template< class I, bool ce>
Force_Computer< I,ce>::Force_Computer( const Solvent& solvent,
                                               const PInfo& pinfo,
                                               const BPInfo& bpinfo,
                                               System_State& _sys):
  sys( _sys), params( solvent, pinfo, bpinfo){}

//#############################################################
void compute_absinth_forces( System_State& state);

 // assumes that core 0 of group 0 is stationary
template< class I, bool ce>
void Force_Computer< I,ce>::doit(){

  V = Energy{ 0.0};
  setup_chain_collision_infos();

  for( auto& group: sys.groups){
    for( auto& chain: group.chain_states){
      chain.has_near_interaction = false;
    }
  }

  {
    System_State_Transformer sys_state_transformer( sys);

    compute_all_intragroup_forces();
    compute_all_intergroup_forces();
    //alt_test_force_balance(sys, 2);
    if constexpr (I::is_absinth()){
      compute_absinth_forces( sys);
    }
  }

  compute_density_grid_forces();
}

//##################################################################
template< class I, bool ce>
template< bool is_mobile0>
decltype( auto)
Force_Computer<I,ce>::transform0( const Core_State& state0){
  
  return choice< is_mobile0>( state0.transform(), Blank_Transform()); 
}

//################################################################
template< class I, bool ce>
template< bool is_mobile>
Pos Force_Computer<I,ce>::center( const Core_State& state){
  auto tform = transform0< is_mobile>( state);
  return tform.translation();
}

//################################################
template< class Chain_State>
auto& collision_structure( Chain_State& chain){
  return chain.collision_info->col_struct;
}

//#######################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::compute_all_intragroup_forces(){
  
  auto& groups = sys.groups;
  auto& group0 = groups[ g0];
  
  compute_intragroup_forces( true, group0);

  const auto ng = groups.size();  
  for( auto gi: range( g1, ng)){
    compute_intragroup_forces( false, groups[gi]);
  }
}

//########################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::
  compute_intragroup_forces( bool is_group0, Group_State& state){
 
  Group_Force_Computer gfc( *this, is_group0, state);
  gfc.doit();
}

//######################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::doit(){
  
  clear_group_forces();
  
  add_intragroup_core_forces(); 
  add_intragroup_interchain_forces();  
    
  for( auto& cstate: state.chain_states){
    if (!cstate.frozen){
      add_intrachain_forces( cstate);
    }
  }
}

//############################################################################################################  
template< class I, bool ce>
void Force_Computer< I,ce>::compute_all_intergroup_forces(){
   
  auto& groups = sys.groups;
  const auto ng = groups.size();
  auto& group0 = groups[ g0];
 
  for( auto gi: range( g1, ng)){
    add_intergroup_forces( true, group0, groups[gi]);
     
    for( auto gj: range( g1, gi)){
      add_intergroup_forces( false, groups[gj], groups[gi]);
    }
  }
}  
  
//##########################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::setup_chain_collision_infos(){
  
  for( auto& group: sys.groups){
    for( Chain_State& chain: group.chain_states){
      if (!chain.atoms.empty()){
        chain.collision_info = std::make_unique< typename Chain_State::Chain_Collision_Info>( chain);
      }
    }
  }
}

//###########################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::clear_group_forces(){
  
  for( auto& core: state.core_states){
    core.force = F3( Zeroi{});
    core.torque = T3( Zeroi{});
  }
  
  for( auto& cstate: state.chain_states){
    for( auto& catom: cstate.atoms){
      catom.force = F3( Zeroi{});
    }
  } 
}

//#####################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intragroup_core_forces(){
   
  auto& mstates = state.core_states;
  
  if (mstates.size() > m0){
    add_intragroup_core_core_forces();
    add_intragroup_core_chain_forces();
  }
}

//####################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intragroup_core_core_forces(){
  
  auto& mstates = state.core_states;
  const auto nm = mstates.size();
  auto& core0 = mstates[m0];
  
  // core-core  
  if (is_group0){
    for( auto mi: range( m1, nm)){
      auto& corei = mstates[mi];
      if (!share_tet( core0, corei)){
        force_computer.add_core_forces<false>( core0, corei);
      }
    }
  }
    
  auto mb = is_group0 ? m1 : m0;
  for( auto mi: range( mb, nm)){    
    auto& corei = mstates[mi];
    for( auto mj: range( mb, mi)){
      auto& corej = mstates[mj];
      if (!share_tet( corei, corej)){
        force_computer.add_core_forces<true>( corej, corei);
      }
    }
  }
}

//####################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intragroup_core_chain_forces(){
  
  auto& mstates = state.core_states;
  auto& core0 = mstates[ m0];
  auto& cstates = state.chain_states;
  
  if (is_group0){
    for( auto ic: range( cstates.size())){
      auto& chaini = cstates[ic];
      if (!share_tet( core0, chaini)){
        force_computer.add_core_chain_forces< false>( core0, chaini);
      }
    }
  }
  auto mb = is_group0 ? m1 : m0;
  const auto nm = mstates.size();
  for( auto mi: range( mb, nm)){
    auto& core = mstates[mi];
    for( auto ic: range( cstates.size())){
      auto& chain = cstates[ic];
      if (!share_tet( core, chain)){
        force_computer.add_core_chain_forces< true>( core, chain); 
      }
    }
  }
}

//###################################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intragroup_interchain_forces(){
  
  auto& chain_states = state.chain_states;
    
  // chain-chain
  for( auto ci: range( chain_states.size())){
    auto& cstatei = chain_states[ci];
    for( auto cj: range( ci)){
      auto& cstatej = chain_states[cj];
      if (!share_tet( cstatei, cstatej)) {
        force_computer.add_local_chain_forces(cstatei, cstatej);
        cstatei.has_near_interaction = true;
        cstatej.has_near_interaction = true;
        if constexpr (!I::is_absinth()) {
          force_computer.add_chain_coulombic_forces(cstatei, cstatej);
        }
      }
    }
  }
}

//#########################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intrachain_forces( Chain_State& cstate){
  
  add_intrachain_nonbonded_forces( cstate);
  add_intrachain_bonded_forces( cstate);
}

//#####################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intrachain_bonded_forces( Chain_State& cstate){
  
  add_forces_from_bonds( cstate);
  add_forces_from_angles( cstate);
  add_forces_from_dihedrals( cstate);
}

//###################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_forces_from_dihedrals( Chain_State& cstate){
  
  auto& lparams = force_computer.params;
  auto& bpinfo = lparams.bpinfo;
  
  auto& core_states = state.core_states;
  auto& catoms = cstate.atoms;
  
  auto& chain = cstate.common();
  auto& dihedrals = chain.dihedrals;
  Energy& lV = force_computer.V;
  
  typedef Bonded_Structure_Interface< typename Chain_Common::Dihedral, Core_State, Chain_State> Dihedral_Interface;
  typename Dihedral_Interface::Chain_Ref chain_ref( core_states, cstate);
  
  for( auto ani: range( dihedrals.size())){
    double phi;
    auto& dihedral = dihedrals[ani];
    compute_torsion_angle_and_derivatives< Dihedral_Interface>( chain_ref, ani, phi);
    
    auto& ttype = bpinfo.dihedral_types[ dihedral.type];
    
    if constexpr (ce){
      lV += ttype.V( phi);
    }
    
    auto mdVdph = ttype.mdVdph( phi);
    for( auto k: range(4)){
      auto ai = dihedral.atomis[k];
      auto F = mdVdph*chain_ref.derivs[k];
      if (!dihedral.on_core( k)){
        catoms[ai].force += F;
      }
      else{
        auto mi = std::get< Core_Index>( dihedral.coris[k]);
        add_force_to_core_atom( F, mi, ai);
      }
    }
  }
}

//##################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_forces_from_angles( Chain_State& cstate){
  
  auto& lparams = force_computer.params;
  auto& bpinfo = lparams.bpinfo;
  
  auto& core_states = state.core_states;
  auto& catoms = cstate.atoms;

  Energy& lV = force_computer.V;
  auto& chain = cstate.common();
  auto& angles = chain.angles;
  
  typedef Bonded_Structure_Interface< typename Chain_Common::Angle, Core_State, Chain_State> Angle_Interface;
  typename Angle_Interface::Chain_Ref chain_ref( core_states, cstate);
  
  for( auto ani: range( angles.size())){
    double theta;
    auto& angle = angles[ani];
    compute_bond_angle_and_derivatives< Angle_Interface>( chain_ref, ani, theta);
    auto& atype = bpinfo.angle_types[ angle.type];
    auto mdVdth = atype.mdVdth( theta);
    
    if constexpr (ce){
      lV += atype.V( theta);
    }
    
    for( auto k: range(3)){
      auto ai = angle.atomis[k];
      auto F = mdVdth*chain_ref.derivs[k];
      if (!angle.on_core( k)){
        catoms[ai].force += F;
      }
      else{
        auto mi = std::get< Core_Index>( angle.coris[k]);
        add_force_to_core_atom( F, mi, ai);
      }
    }
  }
}

//################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_forces_from_bonds( Chain_State& cstate){
  
  auto& lparams = force_computer.params;
  auto& bpinfo = lparams.bpinfo;
  
  auto& chain = cstate.common();
  auto& core_states = state.core_states;
  auto& catoms = cstate.atoms;
  
  typedef Bonded_Structure_Interface< typename Chain_Common::Bond, Core_State, Chain_State> Bond_Interface;
  typename Bond_Interface::Chain_Ref chain_ref( core_states, cstate);

  Energy& lV = force_computer.V;
  auto& bonds = chain.bonds;  
  for( auto bi: range( bonds.size())){
    Length r;
    auto& bond = bonds[bi];
    
    compute_bond_distance_and_derivatives< Bond_Interface>( chain_ref, bi, r);
    auto& btype = bpinfo.bond_types[ bond.type];
    Force mdVdr = btype.mdVdr( r);
    
    if constexpr (ce){
      lV += btype.V( r);
    }
    
    for( auto k: range(2)){
      auto ai = bond.atomis[k];
      auto F = mdVdr*chain_ref.derivs[k];
      if (!bond.on_core( k)){
        catoms[ai].force += F;
      }
      else{
        auto mi = std::get< Core_Index>( bond.coris[k]);
        add_force_to_core_atom( F, mi, ai);
      }
    }
  }
  
}

//############################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_force_to_core_atom( F3 F, Core_Index mi, Atom_Index ai){
  
  auto& core = state.core_states[ mi];

  Pos cen;
  if (mi == m0 && state.number() == g0){
    cen = center< false>( core);
  }
  else{
    cen = center< true>( core);
  }
  core.force += F;
  auto& atoms = core.atoms();
  auto pos = core.transformed( atoms[ ai].pos);
  core.torque += cross( pos - cen, F);
}

//######################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::Group_Force_Computer::add_intrachain_nonbonded_forces( Chain_State& cstate){

  auto& lparams = force_computer.params;
  auto& pinfo = lparams.pinfo;
  auto& solvent = lparams.solvent;
  cstate.has_near_interaction = true;

  auto& chain = cstate.common();
  auto& catoms = cstate.atoms;
  auto& atoms = chain.atoms;
  const auto na = atoms.size();
  
  auto& thread = force_computer.sys.thread();
  auto& excluded = thread.excluded;
  auto& one_four = thread.one_four;
  //auto& coulomb_excluded = thread.coulomb_excluded;
  
  if (na > excluded.size()){
    one_four.resize( na);
    excluded.resize( na);
  }
    
  const Charge Q0{ 0.0};
  
  for( auto ia: range( na)){
    
    fill_in_excluded_flags( chain.excluded_atoms[ia], excluded);
    fill_in_excluded_flags( chain.one_four_atoms[ia], one_four);
      
    auto& catomi = catoms[ia];
    auto& atomi = atoms[ia];
    auto posi = catomi.pos;
    auto qi = atomi.charge;
    
    bool charged = (qi != Q0);
    
    for( auto aj: range( ia)){
      if (!excluded[aj]){
        auto& catomj = catoms[aj];
        auto& atomj = atoms[aj];
        auto posj = catomj.pos;
        auto qj = atomj.charge;
         
        auto of_factor = one_four[aj] ? pinfo.one_four_factor : 1.0;
     
        auto rij = posj - posi;
        auto r = norm( rij);
        auto F = of_factor*pinfo.force( rij, r, atomi, atomj);
        
        auto& lV = force_computer.V;
        if constexpr (ce){
          lV += of_factor*pinfo.potential( r, atomi, atomj);
        }
        
        if constexpr (!I::is_absinth()){
          if (charged && (qj != Q0)){
            auto Fc = screened_coulomb( solvent, rij, r, qi, qj);
            F += Fc;
          }
        }
        
        catomj.force += F;
        catomi.force -= F;
      }
    }
  }
}

//###################################################################################
template< class I, bool ce>
template< bool is_mobile0>
void Force_Computer< I,ce>::add_core_chain_electrostatic_forces( Core_State& mstate0, Chain_State& cstate1){
      
  auto tform0 = transform0< is_mobile0>( mstate0);
  auto center0 = center< is_mobile0>( mstate0);
 
  const double chain_dielectric = 4.0; // later, make it an input
  const double solvent_dielectric = params.solvent.dielectric;
  auto fudge = params.solvent.desolve_fudge;
  const auto born_factor = fudge*(1.0/chain_dielectric - 1.0/solvent_dielectric)*0.5*vacuum_permittivity;

  if (mstate0.v_field()){
    auto mi = mstate0.number();
    auto gi = mstate0.group_number();
  
    auto rel_tform = tform0.inverse();
    auto& field0 = *(mstate0.v_field());
    auto& bfield_ptr = mstate0.born_field();
  
    auto& chain = cstate1.common();
    auto& atoms = chain.atoms;
    auto& v_hint = cstate1.v_hints[gi][mi];
    auto& b_hint = cstate1.born_hints[gi][mi];
    for( auto ia: range( atoms.size())){
      auto& catom = cstate1.atoms[ia];
      auto& atom = atoms[ia];
      auto pos = rel_tform.transformed( catom.pos);
      auto q = atom.charge;
      auto grad = field0.gradient( pos, v_hint);
      //auto F = q*tform0.rotated( grad);
      auto F_loc = q*grad;
      
      if (bfield_ptr){
        auto& bfield0 = *bfield_ptr;
        auto q2 = q*q;
        auto bgrad = bfield0.gradient( pos, b_hint);
        auto F0 = -q2*bgrad;
        
        auto vol = atom.volume;
        auto EE_grad = field0.EE_gradient( pos, v_hint);
        auto F1 = -born_factor*vol*EE_grad;
        
        F_loc += F0 + F1;
      }
      
      auto F = tform0.rotated( F_loc);
      
      catom.force += F;
      mstate0.force -= F;
      mstate0.torque -= cross( catom.pos - center0, F);     
    } 
  }
}
//#################################################################
// from force and torque at r to force and torque at origin
template< class I, bool ce>
inline
std::tuple< F3, T3> Force_Computer< I,ce>::force_and_torque0( F3 force1, T3 torque1, Pos r01){

  auto force0 = -force1;
  auto torque0 = -cross( r01, force1) - torque1;
  return make_tuple( force0, torque0);
}
//######################################################################################
template< class I, bool ce>
template< bool is_mobile0>
void Force_Computer< I,ce>::add_core_chain_forces( Core_State& mstate0,
                                               Chain_State& cstate1){

  if (!cstate1.atoms.empty()){
    Pos center0 = center< is_mobile0>( mstate0);
    Pos center1 = cstate1.hydro_center();
    
    auto [lj_force1, lj_torque1] = core_chain_nonelectrostatic_forces< is_mobile0>( mstate0, cstate1);
      
    add_core_chain_electrostatic_forces< is_mobile0>( mstate0, cstate1);
      
    auto r01 = center1 - center0;
    auto [lj_force0, lj_torque0] = force_and_torque0( lj_force1, lj_torque1, r01);
    
    mstate0.force += lj_force0;
    mstate0.torque += lj_torque0;
  }
}

//##################################################################################
template< class I, bool ce>
template< bool is_mobile0>
std::tuple< F3, T3> Force_Computer< I,ce>::core_chain_nonelectrostatic_forces( 
                             Core_State& state0, Chain_State& cstate1){
 
  auto& cols0 = state0. template collision_structure< is_mobile0>(); 
  auto& cols1 = collision_structure( cstate1);
  
  auto tform0 = transform0< is_mobile0>( state0);
  auto tform1 = Blank_Transform();
  
  Pos center1 = cstate1.hydro_center();
  
  typedef decltype( cols0) Col_Struct0;
  typedef decltype( cols1) Col_Struct1;
  typedef typename std::decay_t< Col_Struct0>::Col_I Col_Iface0;
  typedef typename std::decay_t< Col_Struct1>::Col_I Col_Iface1;
  
  auto& chain = cstate1.common();
  auto mi = state0.number();
  
  Ex_Atom_Map blank;
  auto ex_maps = excluded_maps_of_core( blank, chain, mi);
  auto& excluded = std::get<0>( ex_maps);
  auto& one_four = std::get<1>( ex_maps);
  
  auto [lj_force1, lj_torque1] =
    local_interactions_with_exclusions< Col_Iface0, Col_Iface1>( center1,
                                                cols0, tform0, cols1, tform1,
                                                excluded, one_four);
  
  return std::make_tuple( lj_force1, lj_torque1);
}

//################################################################################################################3
template< class I, bool ce>
auto
Force_Computer< I,ce>::excluded_maps_of_core( const Ex_Atom_Map& blank,
                    const Chain_Common& chain, Core_Index mi) const ->
    std::tuple< const Ex_Atom_Map&, const Ex_Atom_Map&>{
  
  auto& excluded_atoms = chain.excluded_core_atoms;
  auto itrex = excluded_atoms.find( mi);
  auto& excluded = (itrex == excluded_atoms.end()) ? blank : itrex->second;  
  
  auto& one_four_atoms = chain.one_four_core_atoms;
  auto itrof = one_four_atoms.find( mi);
  auto& one_four = (itrof == one_four_atoms.end()) ? blank : itrof->second;
  
  return std::forward_as_tuple( excluded, one_four);
}

//########################################################################################
template< class I, bool ce>
template< class CI0, class CI1>
std::tuple< F3, T3 > Force_Computer< I,ce>::local_interactions_with_exclusions(
    const Pos center1,
    Col_Struct< CI0>& cs0, const typename CI0::Transform& tform0,
    Col_Struct< CI1>& cs1, const typename CI1::Transform& tform1,
    const std::map< Atom_Index, Vector< Atom_Index> >& excluded,
    const std::map< Atom_Index, Vector< Atom_Index> >& one_four){

  auto& pinfo = params.pinfo;
  auto& solvent = params.solvent;

  Local_Force_Computer< CI0, CI1> lfc( sys, pinfo, solvent, center1, excluded, one_four);
  Collision_Detector::compute_interaction< CI0, CI1>( lfc, cs0, tform0, cs1, tform1);
  return std::make_tuple( lfc.force1, lfc.torque1);
}

//############################################################################################################
// mol0 has vgrid; forces and torques about hydro centers
// assume molecules are transformed to home position (if is_mobile0 == true ?)
template< class I, bool ce>
template< bool is_mobile0>
void Force_Computer< I,ce>::
  add_core_forces( Core_State& state0, Core_State& state1){ 
     
  auto [ lj_force1, lj_torque1] = core_local_force_on_1< is_mobile0>( state0, state1);
  auto [e_force1, e_torque1] = core_electrostatic_force_on_1< is_mobile0>( state0, state1);
  auto [b_force10, b_torque10] =
    core_desolvation_force_on_1< is_mobile0, true>( state0, state1);
  auto [b_force00, b_torque00] =
    core_desolvation_force_on_1< true, is_mobile0>( state1, state0);

  auto r01 = center< true>( state1) - center< is_mobile0>( state0);
  auto [b_force01, b_torque01] = force_and_torque0( b_force10, b_torque10, r01);
  auto [b_force11, b_torque11] = force_and_torque0( b_force00, b_torque00, -r01);
  
  F3 force1_nb = lj_force1 + e_force1;
  T3 torque1_nb = lj_torque1 + e_torque1;
  auto [force0_nb, torque0_nb] = force_and_torque0( force1_nb, torque1_nb, r01);
   
  auto force0 = b_force00 + b_force01 + force0_nb;
  auto force1 = b_force10 + b_force11 + force1_nb;
  auto torque0 = b_torque00 + b_torque01 + torque0_nb;
  auto torque1 = b_torque10 + b_torque11 + torque1_nb;  
                    
  state0.force += force0;
  state0.torque += torque0;
  state1.force += force1;
  state1.torque += torque1;
}

//#################################################################################################
template< class I, bool ce>
template< bool is_mobile0>
std::tuple< F3, T3>
Force_Computer< I,ce>::core_local_force_on_1( Core_State& state0,
                                          Core_State& state1){
    
  auto tform0 = transform0< is_mobile0>( state0); 
  auto tform1 = state1.transform();
   
   auto cols = I:: template collision_structures< is_mobile0, true>( state0, state1);
   auto& cols0 = cols.first;
   auto& cols1 = cols.second;
   
  typedef decltype( cols0) Col_Struct0;
  typedef decltype( cols1) Col_Struct1;
  
  typedef typename std::decay< Col_Struct0>::type::Col_I Col_Iface0;
  typedef typename std::decay< Col_Struct1>::type::Col_I Col_Iface1;
  
  Pos center1 = center< true>( state1);
  
  auto [force1, torque1] =
    local_interactions< Col_Iface0, Col_Iface1>( center1, cols0, tform0, cols1, tform1);

  return std::make_tuple( force1, torque1);
}

//#########################################################################################################
template< class I, bool ce>
template< bool is_mobile0>
std::tuple< F3, T3>
Force_Computer< I,ce>::core_electrostatic_force_on_1( Core_State& state0, Core_State& state1){
  
  auto tform0 = state0.transform();
  auto tform1 = state1.transform();
  auto& cols0 = state0. template collision_structure< is_mobile0>();
 
  auto gi0 = state0.group_number();  
  auto mi0 = state0.number();
  auto& v_hint0 = state1.v_hints[gi0][mi0];
   
  auto [force1, torque1] = [&](){
    if( state0.v_field() && state1.q_blob()){
      auto v_field_for_blob =
        I::new_field_for_blob( *(state0.v_field()), cols0, v_hint0);
      
      return
        cheby_and_grid_forces1( v_field_for_blob, *(state1.q_blob()),
                               tform0, tform1, 1.0);
    }
    else{
      return make_tuple( F3( Zeroi{}), T3( Zeroi{}));
    }
  }();
  
  return std::make_tuple( force1, torque1);
}

//###############################################################################################################
template< class I, bool ce>
template< bool is_mobile0, bool is_mobile1>
std::tuple< F3, T3>
Force_Computer< I,ce>::
  core_desolvation_force_on_1( Core_State& state0, Core_State& state1){
 
  if (state0.born_field() && state1.born_field()){
  
    auto fudge = params.solvent.desolve_fudge;
    
    auto gi0 = state0.group_number();
    auto mi0 = state0.number();
    
    auto& born_hint0 = state1.born_hints[gi0][mi0];
    
    auto cols =
      I:: template collision_structures< is_mobile0, is_mobile1>( state0, state1);
    auto& cols0 = cols.first;
        
    auto tform0 = transform0< is_mobile0>( state0);
    auto tform1 = transform0< is_mobile1>( state1);
    
    auto born_field_for_blob0 =
        I::new_field_for_blob( *(state0.born_field()), cols0, born_hint0);
    
    return 
    cheby_and_grid_forces1( born_field_for_blob0, *(state1.q2_blob()),
                             tform0, tform1, fudge); 
  }
  else{
    return std::make_tuple( F3( Zeroi{}), T3( Zeroi{}));
  }
}

//###############################################################################################
template< class I, bool ce>
template< class CI0, class CI1>
std::tuple< F3, T3 >
Force_Computer< I,ce>::local_interactions( const Pos center1,
    typename Collision_Detector::Structure< CI0>& cs0, typename CI0::Transform tform0,
    typename Collision_Detector::Structure< CI1>& cs1, typename CI1::Transform tform1){

  auto& pinfo = params.pinfo;
  auto& solvent = params.solvent;
 
  std::map< Atom_Index, Vector< Atom_Index> > blank;
  Local_Force_Computer< CI0, CI1> lfc( sys, pinfo, solvent, center1, blank, blank);
  Collision_Detector::compute_interaction< CI0, CI1>( lfc, cs0, tform0, cs1, tform1);
  return std::make_tuple( lfc.force1, lfc.torque1);
}

//########################################################################################3
// remember origin of molecule 0 sits at global origin, cheby torques are about origin,
// and rotation is of blob relative to field
template< class I, bool ce>
template< class C0, class FI, class TForm0, class TForm1>
std::tuple< F3, T3>
Force_Computer< I,ce>::
cheby_and_grid_forces1( const Field_For_Blob< C0, FI>& field0, 
                       const Blob< FI>& blob1,
                       const TForm0& tform0, const TForm1& tform1,
                       double factor){
    
  auto inv_tform0 = tform0.inverse();
  auto rel_tform = inv_tform0*tform1;

  // torque about blob1 origin transformed by rel_tform
  F3 force1_fm0;
  T3 torque1_fm0;
  blob1.get_force_and_torque( rel_tform, field0, force1_fm0, torque1_fm0);

  auto force1 = tform0.rotated( force1_fm0);
  auto torque1 = tform0.rotated( torque1_fm0);
   
  auto sforce1 = factor*force1;
  auto storque1 = factor*torque1;

  return std::make_tuple( sforce1, storque1);
}

//####################################################################
template< class I, bool ce>
template< class I0, class I1>
void
Force_Computer< I,ce>::Local_Force_Computer< I0, I1>::operator()(
           const Transform0& tform0, Container0& cont0, Refs0& refs0, Pos cen0, Length r0,
           const Transform1& tform1, Container1& cont1, Refs1& refs1, Pos cen1, Length r1){
        
  auto& thread = this->state.thread();
  auto& excluded = thread.excluded;
  auto& one_four = thread.one_four;

  I0::does_have_near_interaction( cont0);
  I1::does_have_near_interaction( cont1);

  for( auto i0: refs0){
    auto tpos0 = I0::transformed_position( cont0, tform0, i0);
    auto itype0 = I0::atom_type( cont0, i0);
    auto rad0 = I0::physical_radius( cont0, i0);
    auto area0 = I0::sasa( cont0, i0);
    
    Atom atom0;
    atom0.radius = rad0;
    atom0.type = itype0;
    atom0.set_sasa( area0);
    
    get_direct_exclusions( cont0, i0, refs1, excluded);
    get_one_four_exclusions( cont0, i0, refs1, one_four);
    
    /* for future
    if constexpr (I0::is_chain && I1::is_chain){
      atom0.volume = I0::volume( cont0, i0);
      atom0.charge = I0::charge( cont0, i0);
    }
    */
    
    for( auto i1: refs1){
      auto tpos1 = I1::transformed_position( cont1, tform1, i1);
      auto itype1 = I1::atom_type( cont1, i1);
      auto rad1 = I1::physical_radius( cont1, i1);
      auto area1 = I1::sasa( cont1, i1);
      
      Atom atom1;
      atom1.radius = rad1;
      atom1.type = itype1;
      atom1.set_sasa( area1);  
      auto excl_factor = final_exclusion_factor( excluded, one_four, i1);
      
      auto r01 = tpos1 - tpos0;
      auto r2 = norm2( r01);
      auto r = sqrt( r2);
      
      F3 F( Zeroi{});
      if (excl_factor > 0.0){
        F = excl_factor*this->pinfo.force( r01, r, atom0, atom1);
      }
      
      /* for future
      if constexpr (I0::is_chain && I1::is_chain){
        if (this->solvent.desolvation_factor > 0.0){
          atom1.volume = I1::volume( cont1, i1);
          atom1.charge = I0::charge( cont1, i1);
          
          auto qs0 = sq( atom0.charge);
          auto qs1 = sq( atom1.charge);
          auto dL = this->solvent.debye_length; // how to include ionic screening?
          auto b0 = I0::born_factor();
          auto b1 = I1::born_factor();
          
          auto Fval = qs0*b1*sph_born( atom1.volume, r) + qs1*b0*sph_born( atom0.volume, r);
          F += Fval*r01;
        }
      }
      */
      
      add_forces( cont0, cont1, i0, i1, tpos1, F);
    }    
  }  
}

//###############################################################################
template< class I, bool ce>
template< class I0, class I1>
void
Force_Computer< I,ce>::Local_Force_Computer< I0, I1>::
  add_forces( Container0& cont0, Container1& cont1, Ref0 i0, Ref1 i1, Pos tpos1, F3 F){
  
  if constexpr (I0::is_chain){
    I0::add_force( cont0, i0, -F);
  }
  if constexpr (I1::is_chain){
    I1::add_force( cont1, i1,  F);
  }
  if constexpr (!(I0::is_chain && I1::is_chain)){
    this->force1 += F;
    auto rpos1 = tpos1 - this->center1;
    auto tq = cross( rpos1, F);
    this->torque1 += tq;
  }
}


//####################################################################################
template< class I, bool ce>
template< class I0, class I1>
double
Force_Computer< I,ce>::Local_Force_Computer< I0, I1>::
  final_exclusion_factor( const Vector< bool, Atom_Index>& excluded,
                           const Vector< bool, Atom_Index>& one_four, Ref1 i1){
  
  double excl_factor = 1.0;
  if constexpr (compute_exclusions){
    if (excluded[i1]){
      excl_factor = 0.0;
    }
    else if (one_four[i1]){
      excl_factor = this->pinfo.one_four_factor;
    }
  }
  return excl_factor;
}

//#######################################################################
template< class I, bool ce>
template< class I0, class I1>
void
Force_Computer< I,ce>::Local_Force_Computer< I0,I1>::
  get_direct_exclusions( Container0& cont0, Ref0 i0, Refs1& refs1, Vector< bool, Atom_Index>& excluded) const{

  if constexpr (compute_exclusions){
    auto na = refs1.size();
    if (excluded.size() < na){
      excluded.resize( na);
    }
    for( auto ia: range(na)){
      excluded[ia] = false;
    }
      
    auto ai = I0::atom_number( cont0, i0);
    
    auto itr = this->excluded_atoms.find( ai);
    if (itr != this->excluded_atoms.end()){
      auto& these_ex_atoms = itr->second;  
    
      for( auto aj: these_ex_atoms){
        excluded[aj] = true;
      }
    }
  }
}

//##########################################################################
template< class I, bool ce>
template< class I0, class I1>
void
Force_Computer< I,ce>::Local_Force_Computer< I0,I1>::
  get_one_four_exclusions( Container0& cont0, Ref0 i0, Refs1& refs1,
                          Vector< bool, Atom_Index>& one_four) const{
      
  if constexpr (compute_exclusions){

    auto na = refs1.size();
    if (one_four.size() < na){
      one_four.resize( na);
    }
    one_four.fill( false);
    
    auto ai = I0::atom_number( cont0, i0);
    auto itr = this->one_four_atoms.find( ai);
    if (itr != this->one_four_atoms.end()){
      auto& these_ex_atoms = itr->second;  
    
      for( auto aj: these_ex_atoms){
        one_four[aj] = true;
      }
    }
  }
}
//################################################################################################3
template< class I, bool ce>
void Force_Computer< I,ce>::add_local_chain_forces( 
                            Chain_State& cstate0, Chain_State& cstate1){
          
  if (!(cstate0.atoms.empty() || cstate1.atoms.empty())){        
    auto tform = Blank_Transform(); 
    typedef typename I::Chain_Col_Interface Col_Iface;
    auto center1 = cstate1.hydro_center();
    auto& cols0 = collision_structure( cstate0);
    auto& cols1 = collision_structure( cstate1);
    local_interactions< Col_Iface, Col_Iface>( center1, cols0, tform, cols1, tform);
  }
}


//##################################################################################################
// assumes that core 0 of group 0 is stationary
// inter-group
template< class I, bool ce>
void Force_Computer< I,ce>::add_intergroup_forces( 
                            bool is_group0,
                            Group_State& state0, Group_State& state1){
           
  add_intergroup_core_core_forces( is_group0, state0, state1);
  add_intergroup_core_chain_forces( is_group0, state0, state1);
  add_intergroup_chain_chain_forces( state0, state1);   
}

//###################################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::add_intergroup_chain_chain_forces( Group_State& state0, Group_State& state1){
  
  auto& chain_states0 = state0.chain_states;
  auto& chain_states1 = state1.chain_states;

  for( auto& chain0: chain_states0){
    for( auto& chain1: chain_states1){
      add_local_chain_forces( chain0, chain1);
      if constexpr (!I::is_absinth()) {
        add_chain_coulombic_forces(chain0, chain1);
      }
    }
  }
}

//#############################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::add_intergroup_core_chain_forces( 
                            bool is_group0,
                            Group_State& state0, Group_State& state1){
  
  bool is_group0_wc = is_group0 && (state0.core_states.size() > m0);

  auto& mstates0 = state0.core_states;
  auto& mstates1 = state1.core_states;
  auto& cstates0 = state0.chain_states;
  auto& cstates1 = state1.chain_states;

  // core-chain  
  if (is_group0_wc){
    auto& core00 = mstates0[ m0];
    for( auto& chain1: cstates1){
      add_core_chain_forces< false>( core00, chain1);
    }
  }
    
  auto nm0 = mstates0.size();
  const auto mb = is_group0 ? m1 : m0;
    
  for( auto mi: range( mb, nm0)){
    auto& core0 = mstates0[mi];
    for( auto& chain1: cstates1){
      add_core_chain_forces< true>( core0, chain1); 
    }
  }
  
  auto nm1 = mstates1.size();
  for( auto mi1: range( nm1)){
    auto& core1 = mstates1[ mi1];
    for( auto& chain0: cstates0){
      add_core_chain_forces< true>( core1, chain0);
    }
  }
}


//###########################################################################################
template< class I, bool ce>
void Force_Computer< I,ce>::add_intergroup_core_core_forces( 
                            bool is_group0,
                            Group_State& state0, Group_State& state1){
  
  bool is_group0_wc = is_group0 && (state0.core_states.size() > m0);
  auto& core_states0 = state0.core_states;
  auto& core_states1 = state1.core_states;
  
  auto nm0 = core_states0.size();
  auto nm1 = core_states1.size();
 
  const auto mb = is_group0 ? m1 : m0;

  // core-core
  if (is_group0_wc){
    auto& core00 = core_states0[ m0];
    for( auto mi: range( m0, nm1)){
      add_core_forces< false>( core00, core_states1[mi]);
    }
  }
  
  for( auto mi0: range( mb, nm0)){
    auto& mstate0 = core_states0[mi0];
    for( auto mi1: range( m0, nm1)){
      auto& mstate1 = core_states1[mi1];
      add_core_forces<true>( mstate0, mstate1);
    }
  }
}

//######################################################################################################
// different chains
template< class I, bool ce>
void Force_Computer< I,ce>::add_chain_coulombic_forces( Chain_State& cstate0, Chain_State& cstate1){

  error( "should not be called", __FILE__, __LINE__);

  auto& chain0 = cstate0.common();
  auto& chain1 = cstate1.common();
  auto& satoms0 = cstate0.atoms;
  auto& satoms1 = cstate1.atoms;
  auto& atoms0 = chain0.atoms;
  auto& atoms1 = chain1.atoms;
  
  for( auto ci0: range( satoms0.size())){
    auto& satom0 = satoms0[ci0];
    auto& atom0 = atoms0[ci0];
    auto q0 = atom0.charge;
    if (q0 != Charge( 0.0)){

      for( auto ci1: range( satoms1.size())){
        auto& satom1 = satoms1[ci1];
        auto& atom1 = atoms1[ci1];
        auto q1 = atom1.charge;
        if (q1 != Charge( 0.0)){
          auto r01 = satom1.pos - satom0.pos;
          auto r = norm( r01);
          auto F = screened_coulomb( params.solvent, r01, r, q0,q1);
          satom1.force += F;
          satom0.force -= F;
        }
      }
    }
  }
}
    
//#################################################################################################
// constructor
template< class System_State, bool ce>
Force_Computer< System_State, ce>::System_State_Transformer::System_State_Transformer( System_State& _state):  
   state(_state){
  
  auto& group0 = state.groups[ g0];
  auto& core_states = group0.core_states;
  if (core_states.size() > m0){
    tform0 = core_states[m0].transform();
    auto inv_tform0 = tform0.inverse(); 
    for( auto& group: state.groups){
      
      for( auto& core: group.core_states){
        core.transform_transform( inv_tform0);
      }
      for( auto& chain: group.chain_states){
        if( !chain.atoms.empty()){
          for( auto& catom: chain.atoms){
            catom.pos = inv_tform0.transformed( catom.pos);
          }
          auto& cols = collision_structure( chain);
          auto ccenter = cols.position();
          auto ccenter0 = inv_tform0.transformed( ccenter);
          cols.shift( ccenter0 - ccenter);
        }
      }
    }
  }
}

//#################################################################################################
template< class I, bool ce>
Force_Computer< I,ce>::System_State_Transformer::~System_State_Transformer(){
  
  auto& group0 = state.groups[ g0];
  auto& core_states = group0.core_states;
    
  if (core_states.size() > m0){
    for( auto& group: state.groups){
      
      for( auto& core: group.core_states){
        core.transform_transform( tform0);
        core.force = tform0.rotated( core.force);
        core.torque = tform0.rotated( core.torque);
      }
        
      for( auto& chain: group.chain_states){
        if (!chain.atoms.empty()){
          for( auto& catom: chain.atoms){
            catom.pos = tform0.transformed( catom.pos);
            catom.force = tform0.rotated( catom.force);
          }
          
          auto& cols = collision_structure( chain);
          auto ccenter = cols.position();
          auto ccenter0 = tform0.transformed( ccenter);
          cols.shift( ccenter0 - ccenter);
        }
      }   
    }
  }
}

//#################################################################
// density grid force
template< class I, bool ce>
void Force_Computer< I,ce>::compute_density_grid_forces(){
  
  auto& field = sys.common().density_field;
  auto& groups = sys.groups;
  
  if (field){
    for( auto& group: groups){
      for( auto& core: group.core_states){
        F3 force( Zeroi{});
        T3 torque( Zeroi{});
        
        auto& common = core.common();
        auto center = core.translation();
        for( auto& atom: common.atoms){
          auto pos = core.transformed( atom.pos);
          auto gopt = field->gradient( pos);
          if (gopt){
            auto g = gopt.value();
            auto w = atom.mass_weight;
            force += w*g;
            auto rpos = pos - center;
            torque += w*cross( rpos, g); 
          }
        }
        core.force += force;
        core.torque += torque;
      }
    }
  }
}


}
