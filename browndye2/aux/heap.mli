(* used internally. Implements a mutable heap data structure. *)

type 'a t

val is_empty: 'a t -> bool

(* Creates empty heap. First argument is a comparison function
with same behavior as Ocaml "compare" function. *)
val created: ('a -> 'a -> int)->int -> 'a t

val add: 'a t -> 'a -> unit

val popped_max: 'a t -> 'a

val size: 'a t -> int
