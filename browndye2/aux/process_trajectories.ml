(*
Processes trajectory files as described in the User's Manual,
outputting requested information in XML format. It takes the
following arguments with flags:

-traj : trajectory file from nam_simulation
-index : index file
-n : trajectory number (starting at 0)
-sn : subtrajectory number (starting at 0). Default is 0.
-nstride : only every nstride^th state is output; the default of 1 means that every state is output
-srxn : find subtrajectories resulting from completion of the given reaction
-uncomp : find subtrajectories starting from given state but failing to complete
*)

let pr = Printf.printf;;

let lmap f lst = List.rev (List.rev_map f lst);;

let index_file_name_ref = ref "";;
let traj_file_name_ref = ref "";;
let itraj_ref = ref 0;;
let istraj_ref = ref 0;;
let nstride_ref = ref 1;;
let selected_rxn_ref = ref "";;
let selected_state_ref = ref "";;

Arg.parse
  [
    ("-traj", Arg.Set_string traj_file_name_ref, "trajectory file (from nam_simulation");
    ("-index", Arg.Set_string index_file_name_ref, "index file; default is traj.index.xml if trajectory file is traj.xml");
    ("-n", Arg.Set_int itraj_ref, "trajectory number (starting at 0, default is 0)");
    ("-sn", Arg.Set_int istraj_ref, "subtrajectory number (starting at 0). Default is 0");
    ("-nstride", Arg.Set_int nstride_ref, "only every nstride^th state is output; the default of 1 means that every state is output");
    ("-srxn", Arg.Set_string selected_rxn_ref, "find subtrajectories resulting from completion of the given reaction");
    ("-uncomp", Arg.Set_string selected_state_ref, "find subtrajectories starting from given state but failing to complete");
  ]
 (fun arg -> raise (Failure "no anonymous arguments"))
  "Outputs trajectory info in xml format"
;;

let traj_file_name = !traj_file_name_ref;;
(* let index_file_name = !index_file_name_ref;; *)
let itraj = !itraj_ref;;
let istraj = !istraj_ref;;
let nstride = !nstride_ref;;
let selected_rxn = !selected_rxn_ref;;
let selected_state = !selected_state_ref;;

let index_file_name =
  let iref = !index_file_name_ref in 
  if iref = "" then
    let n = String.length traj_file_name in
    let base = String.sub traj_file_name 0 (n-4) in
    base ^ ".index.xml"
  else
    iref
;;
                              
if (itraj = (-1))&& (selected_rxn = "") && (selected_state = "") then
  raise (Failure "need trajectory number (-n)")
;;

(* let red_file_name, red_file = Filename.open_temp_file "BD" ".xml";; *)
let traj_file = open_in traj_file_name;;

module SS = Scanf.Scanning;;
module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;
module H = Hashtbl;;

let traj_buf = SS.from_channel traj_file;;
let tparser = JP.new_parser traj_buf;;

let node_of_tag parser tag = 
  let opt = JP.node_of_tag parser tag in
    match opt with
      | None -> raise (Failure ("no tag of "^tag))
      | Some node -> 
	  JP.complete_current_node parser;
	  node
;;

let force_type =
  let node = node_of_tag tparser "force_model_type" in
  JP.string_from_stream (JP.node_stream node)
;;
  
let n_states_per_record = 
  let node = node_of_tag tparser "n_states_per_record" in
    JP.int_from_stream (JP.node_stream node)
;;

let centers =
  let node = node_of_tag tparser "core_centers" in
  let cnodes = JP.children node "center" in
  Array.of_list @@
    lmap
      (fun cnode ->
        JP.float3_from_stream (JP.node_stream cnode)
      )
      cnodes
;;

type atoms_type = Core | Chain | Time;;
  
let files =
  let node = node_of_tag tparser "atom_files" in
  let gnodes = JP.children node "group" in
  let lst0 = 
    List.fold_left
      (fun files gnode ->
        let mnodes = JP.children gnode "core"
        and cnodes = JP.children gnode "chain" in
        
        let filesof atype nodes =
          List.map
            (fun node ->
              atype, JP.string_from_stream (JP.node_stream node)
            )
            nodes
        in
        
        let mfiles = filesof Core mnodes
        and cfiles = filesof Chain cnodes in
        files @ mfiles @ cfiles
      )
      []
      gnodes
    in
    let lst = (Time,"") :: lst0 in
    Array.of_list lst;
;;
    

  
let fields = 
  let node = node_of_tag tparser "fields" in
  let fnodes = JP.children node "field" in
  Array.of_list @@      
    lmap
      (fun fnode -> 
	let stm = JP.node_stream fnode in
	let name = JP.string_from_stream stm in
	let nbytes = JP.int_from_stream stm in
	name, nbytes 
      )
      fnodes      
;;

type fate = Escaped | Reacted | Stuck | In_Progress | Undetermined;;

type straj_record = {
  snumber: int;
  sstart: Int64.t;
  reaction: string option;
  state: string;
  state_end: string option;
  sfate: fate;
}
;;

type traj_record = {
  tnumber: int;
  tstart: Int64.t;
  subtrajectories: straj_record array;
};;

let traj_index = 
  let trajs_lst = 
    let iparser = 
      let index_buf = SS.from_file index_file_name in
      JP.new_parser index_buf 
    in
    let res = ref [] in
    while not (JP.finished iparser) do
      JP.next iparser;
      if JP.is_current_node_tag iparser "trajectory" then (
	JP.complete_current_node iparser;
	let tnode = JP.current_node iparser in 
	let snodes = JP.children tnode "subtrajectory" in
	let strajs_lst = 	       
	  lmap
	    (fun snode ->
	      let cstring tag = 
		match JP.child snode tag with
		| None -> None
		| Some cnode -> 
		   Some (JP.string_from_stream (JP.node_stream cnode))
	      in {
		  snumber = NI.int_of_child snode "n_subtraj";
		  sstart = NI.int64_of_child snode "start";
		  state = NI.string_of_child snode "rxn_state";
		  reaction = cstring "rxn";
		  state_end = cstring "rxn_state_end";
		  sfate = 
		    let fnode_opt = JP.child snode "fate" in
		    match fnode_opt with
		    | None -> Undetermined
		    | Some fnode ->
		       let fatestr = JP.string_from_stream (JP.node_stream fnode) in
		       if fatestr = "escaped" then
			 Escaped
		       else if fatestr = "reacted" then
			 Reacted
		       else if fatestr = "stuck" then
			 Stuck
		       else 
			 In_Progress;
	        }
	    )
	    snodes
	in
	let strajs = Array.of_list strajs_lst in
	Array.sort
          (fun straj0 straj1 -> compare straj0.snumber straj1.snumber) strajs;
	let traj_rec = {
	    subtrajectories = strajs;
	    tstart = NI.int64_of_child tnode "start";
	    tnumber = NI.int_of_child tnode "n_traj";
	  }
	in
	res := traj_rec :: (!res);
      )
    done;
    List.rev !res
  in
  let trajs = Array.of_list trajs_lst in
  Array.sort
    (fun traj0 traj1 -> compare traj0.tnumber traj1.tnumber) trajs;
  trajs
;;
  
let sel_rxn = not (selected_rxn = "");;
let sel_state = not (selected_state = "");;
  
if sel_rxn || sel_state then (
  pr "<trajectories>\n";
  if sel_rxn then
    pr "  <rxn> %s </rxn>\n" selected_rxn
  else
    pr "  <state> %s </state>\n" selected_state;
  
  Array.iter (fun traj -> 
      let rec loop i first_done = 
        if i < Array.length traj.subtrajectories then
	  let straj = traj.subtrajectories.(i) in
	  if sel_rxn then (	    
	    match straj.reaction with
	    | None -> ()
	    | Some rxn ->
	       if rxn = selected_rxn then (
		 if not first_done then (
		   pr "  <trajectory>\n";
		   pr "    <number> %d </number>\n" traj.tnumber;
		 );
		 pr "    <subtrajectory> %d </subtrajectory>\n" straj.snumber;
		 loop (i+1) true;
	       )
	       else 
		 loop (i+1) first_done;
	  )
	                    
	  else ( (* selected state *)
	    if (straj.state = selected_state) && 
	         ((straj.sfate = Escaped) || (straj.sfate = Stuck)) then (
	      if not first_done then (
		pr "  <trajectory>\n";
		pr "    <number> %d </number>\n" traj.tnumber;
	      );
	      pr "    <subtrajectory> %d </subtrajectory>\n" straj.snumber;
	      let fate_rep = 
		match straj.sfate with
		| Escaped -> "escaped"
		| Stuck -> "stuck"
		| _ -> failwith "should not get here: 228"
	      in
	      pr "    <fate> %s </fate>\n" fate_rep;
	      
	      loop (i+1) true;
	    )
	    else 
	      loop (i+1) first_done;
	  )  
        else (
	  if first_done then
	    pr "  </trajectory>\n";
        )
      in
      loop 0 false;
    )
             traj_index;
    
  pr "</trajectories>\n";
)
                                 
else(
  let itraj_file = open_in traj_file_name in
  let tindex = traj_index.(itraj).subtrajectories.(istraj).sstart in
  LargeFile.seek_in itraj_file tindex;
  let iparser = JP.new_parser_of_channel itraj_file "<trajectories> <trajectory> " in
  let n_num_per_state = 
    Array.fold_left 
      (fun sum info ->
	let name, nbytes = info in
	sum + nbytes
      )
      0 fields
  in
  ignore (JP.node_of_tag iparser "subtrajectory");
  pr "<trajectory>\n";
  pr "  <force_model_type> %s </force_model_type>\n" force_type;
  pr "  <core_centers>\n";
  Array.iter
    (fun center ->
      let x,y,z = center in
      pr "    <center> %g %g %g </center>\n" x y z;
    )
    centers;
  pr "  </core_centers>\n";

  pr "  <pattern> ";
  Array.iter
    (fun file ->
      let atype,name = file in
      let aname = 
        match atype with
        | Core -> "core"
        | Chain -> "chain"
        | Time -> "dt"
      in
      pr "%s " aname
    )
    files;
  pr "</pattern>\n";
  
  
  pr "  <atom_files> ";
  Array.iter
    (fun file ->
      let atype,name = file in
      pr "%s " name
    )
    files;
  pr "</atom_files>\n";
  
  (*

  (let x,y,z = center0 in
   pr "  <center_molecule0> %g %g %g </center_molecule0>\n" x y z;
  );
  (let x,y,z = center1 in	
   pr "  <center_molecule1> %g %g %g </center_molecule1>\n" x y z;
  );

   *)  

  let last_state = ref (Array.init 0 (fun i -> Array.init 0 (fun i -> 0.0))) in
  let istate = ref 0 in
  JP.apply_to_nodes_of_tag
    iparser "s" 
    (fun node ->
      JP.complete_current_node iparser;
      let mime_data, nstates = 
	let nnode_opt = JP.child node "n" in
	match nnode_opt with
	| None ->
	   let stm = JP.node_stream node in
	   JP.string_from_stream stm, n_states_per_record
	| Some nnode ->
	   let nstates = 
	     JP.int_from_stream (JP.node_stream nnode)
	   in
	   let mime_data = NI.string_of_child node "data" in
	   mime_data, nstates
      in
      let data = Decode_binary.floats_of_string mime_data in
      let dt = ref 0.0 in
      for jstate = 0 to nstates-1 do
        
        let statep = jstate*n_num_per_state in
	let state = 
	  Array.init n_num_per_state 
	    (fun i -> data.(statep + i))
	in

        let idata = ref 0 in	
	Array.iter
	  (fun field -> 
	    let name, nbytes = field in
	    for ib = 0 to nbytes-1 do
	      if (name = "dt") then (
                let ddt = state.(!idata+ib) in
                dt := !dt +. ddt;
              );
            done;
              
	    idata := !idata + nbytes;
	  )
	  fields;
        
	if (!istate mod nstride) = 0 || (jstate = nstates-1) then ( 

          if (!istate mod nstride) = 0 then (
	    let idata = ref 0 in
	    pr "  <state>\n";		
	    Array.iter
	      (fun field -> 
	        let name, nbytes = field in
	        pr "    <%s> " name;
	        for ib = 0 to nbytes-1 do
                  if (name = "dt") then (
                    pr "%g " !dt;
                    dt := 0.0;
                  )
                  else 
		    pr "%g " state.(!idata+ib);
	        done;
	        pr " </%s>\n" name;
	        idata := !idata + nbytes;
	      )
	      fields;
	    pr "  </state>\n";
          );

          if jstate = nstates-1 then(
            let idata = ref 0 in
            let ldata =
              let nf = (Array.length fields) in
              Array.init nf (fun i ->
                  let field = fields.(i) in
                  let name, nbytes = field in
                  let oidata = !idata in
                  idata := !idata + nbytes;
                  Array.init nbytes (fun ib -> state.(oidata + ib))
                )             
            in
            last_state := ldata;
          );
          
	);
	istate := !istate + 1;
      done;
    );

  pr "  <state>\n";
  let idata = ref 0 in
  Array.iter
    (fun field ->
      let name, nbytes = field in
      pr "    <%s> " name;
      for ib = 0 to nbytes-1 do
        pr "%g " (!last_state).(!idata).(ib);
      done;
      pr " </%s>\n" name;
      idata := !idata + 1;
    )
    fields;
  pr "  </state>\n";
  
  pr "</trajectory>\n";      
)
;;
  
   
