(* Used internally. Used to parse the output grid of "inside_points". *)

type data3d =
    (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout)
    Bigarray.Array3.t


type t = {
  data: data3d;
  spacing: float*float*float;
  corner: float*float*float;
  exclusion_radius: float;
}
  
(* (grid xml_format buffer) - the grid can be
in XML format (the usual) or plain format *)  
val grid: bool -> Scanf.Scanning.scanbuf -> t


  
