#include "files.hh"
#include "../xml/node_info.hh"

namespace {
  using namespace Browndye;
  using namespace JAM_XML_Pull_Parser;
  using std::pair;
  using std::make_pair;
  namespace OC = Orchestrator;
  
  //###########################################################  
  pair< std::unique_ptr< Parser>, Node_Ptr>
    xml_from_file( const string& file){
    
    std::ifstream input( file);
    auto parser = std::make_unique< Parser>( input, file);
    auto top = parser->top();
    top->complete();
    return make_pair( move( parser), top);
  }
  
  //######################################################
  void fix_PE0( const std::string& stem){
    
    auto file = ("./" + stem) + ".dx";
    if (!OC::file_exists( file)){
      auto alt_file = ("./" + stem) + "-PE0.dx";
      OC::rename_file( alt_file, file); 
    }
  }
  
  //######################################################
  void main0( int argc, char* argv[]){
    if (argc < 2){
      error( "need input file name\n");
    }
       
    std::string file( argv[1]);   
         
    auto top = xml_from_file( file).second;  
    top->complete();
    auto snode = top->child( "system");
    if (!snode){
      top->perror( "system not found");
    }   
    
    auto gnodes = snode->children_of_tag( "group");
    for( auto& gnode: gnodes){
      auto cnodes = gnode->children_of_tag( "core");
      for( auto& cnode: cnodes){
        auto enode = cnode->child( "electric_field");
        if (enode){
          auto grnodes = enode->children_of_tag( "grid");
          for( auto& grnode: grnodes){
            auto& attrs = grnode->attributes();
            auto oval = attrs.value( "source");
            if (oval){
              auto val = oval.value();
              if (val == "make_apbs_inputs"){
                auto dxfile = value_from_node< string>( grnode);
                auto ndx = dxfile.size();
                auto stem = dxfile.substr( 0, ndx-3);
                auto infile = stem + ".in";
                string command0( "apbs ");
                auto command = command0 + infile;
                auto status = system( command.c_str());
                if (!WIFEXITED(status)){
                  error( command, "failed");
                }
                
                fix_PE0( stem);
              }
            }
          }
        }
      }
    }
  }  
}

//############################################
int main( int argc, char* argv[]){
  main0( argc, argv);  
}


