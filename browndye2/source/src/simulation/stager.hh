#pragma once
#include "../lib/vector.hh"
#include "../global/pos.hh"
#include "../transforms/transform.hh"
#include "../global/indices.hh"

namespace Browndye{
  class System_Common;
  class System_State;

  class Stager{
  public:
    explicit Stager( const System_Common&);
    std::pair< bool, Bin_Index> add_data_reject( const System_State&);
    
    // data
    //Pos center;
    //Transform tform;
    
    Vector< Length, Bin_Index> distances; // by partition; include zero, outmost
    Vector< size_t, Bin_Index> counts; // by bin
    Vector< Time, Bin_Index> times; // by bin
    Vector< Array< size_t,2>, Bin_Index> n_crosses; // by partition
    Vector< Array< Inv_Length,2>, Bin_Index> flux_sums; // by partition
    std::optional< Length> last_rho;
    Inv_Length last_F{ NaN};
    //Diffusivity last_D{ NaN};
    
    Stager copy() const;
    Diffusivity site_diffusivity( const System_State& state) const;
    Inv_Length site_force( const System_State& state, Vec3< double> u) const;
    std::optional< Bin_Index> bin_of( Length r) const;
    
  private:
    class Init_Tag{};
    explicit Stager( Init_Tag){};
  };
 
  Stager added( const Stager& s0, const Stager& s1);
    
 
}
