(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Convenience program.  
Converts ASCII UHBD grid file to DX file.
Uses standard input and output.
*)


Arg.parse
[]
(fun arg -> raise (Failure "no anonymous arguments"))
"Converts ASCII UHBD grid file to DX file"
;;

let stdib = Scanf.Scanning.stdin;;

Scanf.bscanf stdib "%s@\n" (fun s -> ());;

Scanf.bscanf stdib " %g %g %d %d %d %d %d" 
  (fun dum0 dum1 dum2 dum3 dum4 dum5 dum6 -> ())
;;

let nx_ref, ny_ref, nz_ref = ref 0, ref 0, ref 0;;
let h_ref = ref nan;;
let xmin_ref, ymin_ref, zmin_ref = ref 0.0, ref 0.0, ref 0.0;;

Scanf.bscanf stdib " %d %d %d %g %g %g %g" 
  (fun nx ny nz h xmin ymin zmin ->
    nx_ref := nx; ny_ref := ny; nz_ref := nz; h_ref := h;
    xmin_ref := xmin; ymin_ref := ymin; zmin_ref := zmin; 
  )
;;

let nx,ny,nz = !nx_ref, !ny_ref, !nz_ref;;
let h = !h_ref;;
let xmin, ymin, zmin = !xmin_ref +. h, !ymin_ref +. h, !zmin_ref +. h;;

Scanf.bscanf stdib " %g %g %g %g\n" (fun dum0 dum1 dum2 dum3 -> ());;

Scanf.bscanf stdib " %g %g %d %d\n" (fun dum0 dum1 dum2 dum3 -> ());;

(*
Printf.printf "n %d %d %d\n" nx ny nz;;
Printf.printf "h %g\n" h;;
Printf.printf "min %g %g %g\n" xmin ymin zmin;;
*)


Printf.printf "object 1 class gridpositions counts %d %d %d\n" nx ny nz;;
Printf.printf "origin %g %g %g\n" xmin ymin zmin;; 
Printf.printf "delta %g 0.0 0.0\n" h;;
Printf.printf "delta 0.0 %g 0.0\n" h;;
Printf.printf "delta 0.0 0.0 %g\n" h;;
Printf.printf "object 2 class gridconnections counts %d %d %d\n" nx ny nz;;
Printf.printf "object 3 class array type double rank 0 items %d data follows\n" (nx*ny*nz);;


let phi = 
  Array.init nx (fun ix -> 
    Array.init ny (fun iy -> 
      Array.init nz (fun iz -> nan)
    )
  )
;;

let nblines = nx*ny/6;;
let ix, iy, iz = ref 0, ref 0, ref 0;;

let inc i = 
  i := !i + 1;
;;

for i = 1 to nz do
  Scanf.bscanf stdib " %d %d %d\n" (fun dum0 dum1 dum2 -> ());

  let v = [| nan; nan; nan; nan; nan; nan; |] in
  let get_line n = 
    for k = 0 to n-1 do
      phi.(!ix).(!iy).(!iz) <- v.(k);
      inc ix;
      if !ix = nx then (
	ix := 0;
	inc iy;
	if !iy = ny then (
	  iy := 0;
	  inc iz;
	  if !iz = nz then
	    iz := 0;
	)
      )
	
    done;
  in
    for j = 1 to nblines do        
      Scanf.bscanf stdib " %g %g %g %g %g %g\n" 
	(fun v0 v1 v2 v3 v4 v5 ->
	  v.(0) <- v0;
	  v.(1) <- v1;
	  v.(2) <- v2;
	  v.(3) <- v3;
	  v.(4) <- v4;
	  v.(5) <- v5;
	);
      
      get_line 6;
    done;
    let nrem = nx*ny - 6*nblines in
      for k = 0 to nrem-1 do
	Scanf.bscanf stdib " %g" (fun v0 -> v.(k) <- v0;)
      done;
      get_line nrem;
	
done;; 

let iline = ref 0;;
for ix = 0 to nx-1 do
  for iy = 0 to ny-1 do
    for iz = 0 to nz-1 do
      Printf.printf "%g " phi.(ix).(iy).(iz);
      inc iline;
      if !iline = 3 then (
	iline := 0;
	Printf.printf "\n";
      )
    done;
  done;
done
;;

Printf.printf 
"attribute \"dep\" string \"positions\"
object \"regular positions regular connections\" class field
component \"positions\" value 1
component \"connections\" value 2
component \"data\" value 3\n"
;;
