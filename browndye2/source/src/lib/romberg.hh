#pragma once

/* Approximate the definite integral of f from a to b by Romberg's method.
    eps is the desired accuracy.
*/

#include <functional>
#include "vector.hh"

namespace Browndye{

namespace Romberg{

template< class F>
auto isum( F& f, size_t nb, size_t ne){
  decltype( f(0)) sum( 0.0);
  for( auto i = nb; i <= ne; i++)
    sum += f(i);
  return sum;
}

inline
size_t pow( size_t x, size_t n){
  if (n == 0)
    return 1;
  else{
    if (n % 2 == 0){
      auto xnh = pow( x, n/2);
      return xnh*xnh;
    }
    else
      return x*pow( x, n-1);
  }
}

template< class F, class X>
auto integral( F& f, X a, X b, double eps){

  typedef decltype( f(a)) Y;
  typedef decltype( X()*Y()) Ig;

  std::function< Ig (size_t, const Vector<Ig>&)> loop = 
    [&]( size_t n, const Vector< Ig>& rnm) -> Ig{

    auto h = (b - a)/((double)pow( 2,n));
    Vector< Ig> rn( n+1);

    auto lfun = [&]( size_t k) -> Y {
      return f( a + ((double)(2*k-1))*h);
    };

    rn[0] = 0.5*rnm[0] + h*isum( lfun, 1, pow( 2, n-1));
    for( auto m = 1u; m <= n; m++)
      rn[m] = rn[m-1] + (rn[m-1] - rnm[m-1])/((double)(pow( 4, m) - 1));
      
    if (fabs( (rnm[n-1] - rn[n])/rn[n]) < eps)
      return rn[n];
    else
      return loop( n+1, rn);
  };


  Vector< Ig> r0(1);
  r0[0] = 0.5*(b - a)*(f(a) + f(b)); 

  return loop( 1, r0);
}

}}

