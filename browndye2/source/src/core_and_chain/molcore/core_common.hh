#pragma once

#include <memory>
#include <string>
#include <map>
#include "../../lib/units.hh"
#include "../../xml/jam_xml_pull_parser.hh"
#include "../../forces/other/nonbonded_parameter_info.hh"
#include "../../forces/electrostatic/field.hh"
#include "../../forces/electrostatic/born_field_interface.hh"
#include "../../structures/atom.hh"
#include "../../forces/electrostatic/blob_interface.hh"
#include "../../generic_algs/charged_blob_simple.hh"
#include "large_collision_interface.hh"
#include "../../forces/electrostatic/v_field_interface.hh"
#include "../chain/chain_common.hh"

namespace Browndye{

class System_Common;

// normalize atom positions so that hydrodynamic center is at origin!
class Core_Common: public Macro_Struct{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
  
  Core_Common( const Nonbonded_Parameter_Info&, const Node_Ptr&, const System_Common&, Group_Index, Core_Index);
  Core_Common() = delete;
  ~Core_Common() = default;
  Core_Common( const Core_Common&) = delete;
  Core_Common( Core_Common&&) = default;
  Core_Common& operator=( const Core_Common&) = delete;
  Core_Common& operator=( Core_Common&&) = delete;
  Core_Common( Test_Tag,  System_Common &_sys,  const Vector<Atom, Atom_Index> &atoms, Core_Index num); // for testing


  [[nodiscard]] bool are_same( const Core_Common&) const;
  [[nodiscard]] bool has_copy_children() const;
  
  //[[nodiscard]] Vec3< Length> transformed_position( Blank_Transform, Vector< Atom, Atom_Index>::iterator) const;
  [[nodiscard]] Transform home_tform() const;
  [[nodiscard]] Charge charge() const;
  
  // Data
  const Core_Index number;
  bool is_stationary = false;
  Transform copy_tform;

  typedef Collision_Detector::Structure< Large_Col_Interface< false> > Collision_Structure;
  std::unique_ptr< Collision_Structure> collision_structure;
  
  typedef Blob_Interface< V_Field_Interface> Q_Blob_Interface;
  std::shared_ptr< Charged_Blob::Blob< 4, Q_Blob_Interface> > q_blob;
  
  std::shared_ptr< Field::Field< V_Field_Interface> > v_field;
  Field::Field< V_Field_Interface>::Hint v_hint0;
  
  std::shared_ptr< Field::Field< Born_Field_Interface> >  born_field;
  Field::Field< Born_Field_Interface>::Hint born_hint0;
  
  typedef Blob_Interface< Born_Field_Interface> Q2_Blob_Interface;
  std::shared_ptr< Charged_Blob::Blob< 4, Q2_Blob_Interface> > q2_blob;
  
  Length h_radius = Length( NAN);
      
  const System_Common& system;
  const Core_Common* copy_parent = nullptr;

  Pos hydro_center; // hydrodynamic center before initial centering
    
  struct Atom_Info{
    Atom_Index ia;
    
    struct Chain_Info{
      Chain_Index ic;
      bool is_constrained;
    };
    Vector< Chain_Info> chain_infos;
  };
  
  Vector< Atom_Info, Ind_Atom_Index> bound_atoms;

private:
  // private functions
  [[nodiscard]] std::pair< Length, Pos> hydro_params( const std::string& file) const;
  void set_desolvation_chebybox_from_node(const Node_Ptr &node);
  void set_charge_chebybox_from_node(const Node_Ptr &node);
  void check_mpole_consistency() const;
  //void get_springs( Node_Ptr node);
  void setup_from_parent( Node_Ptr);
  void find_copy_parent( const Node_Ptr&);
  void setup_fields( const Node_Ptr&, bool& pre_checked);
  void setup_atoms( const Nonbonded_Parameter_Info&);
  void setup_eta_max();
  void hollow_out_grids();
};


}

