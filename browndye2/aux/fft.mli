(* Computes the autocorrelation of the input using the
Fast Fourier Transform.  The output autocorrelation is
1/10 the length of the input data *)

val autocorrelation : float array -> float array
