#pragma once
#include <cmath>

/* 
Given positions of two points, computes 
the bond length in r and the corresponding derivatives.
Points are numbered 0 through 1.

Interface class must have the following types and static functions:

types: Chain, Group_Index, Length

functions:

Pos position( const Chain& chain, const Group_Index& iq, size_t ip) -
get the position of the ip^th point (0,1,2). The user-defined
index iq can be used to select the group of three points. Pos
 must be indexible like an array and have consistent units
 

void set_derivative( Chain& chain, const Group_Index& iq, size_t ip,
          size_t k, Deriv& a)
sets the k^th component (0,1,2) of the derivative with respect to the 
ip^th point (0,1,2) to a. The user-defined
index iq can be used to select the group of three points.
Deriv must have consistent units.

*/
namespace Browndye{

template< class I>
void compute_bond_distance_and_derivatives( typename I::Chain& chain, typename I::Group_Index ig, typename I::Length& r){

  typedef typename I::Length Length;

  auto p1 = I::position( chain, ig, 0);
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];

  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];

  // r21 = x2 - x1
  Length a7 = x2 - x1;
  Length a8 = y2 - y1;
  Length a9 = z2 - z1;

  // r
  r = sqrt( a7*a7 + a8*a8 + a9*a9);
  
  // r
  double b7 = a7/r;
  double b8 = a8/r;
  double b9 = a9/r;

  // r21
  I::set_derivative( chain,ig,1,0, b7);
  I::set_derivative( chain,ig,1,1, b8);
  I::set_derivative( chain,ig,1,2, b9);

  I::set_derivative( chain,ig,0,0, -b7);
  I::set_derivative( chain,ig,0,1, -b8);
  I::set_derivative( chain,ig,0,2, -b9);
}

template< class I>
[[maybe_unused]]
auto bond_distance( typename I::Chain& chain, typename I::Group_Index ig) -> typename I::Length{

  typedef typename I::Length Length;

  
  auto p1 = I::position( chain, ig, 0);
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];
  
  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];

  // r21 = x2 - x1
  Length a7 = x2 - x1;
  Length a8 = y2 - y1;
  Length a9 = z2 - z1;

  // r
  return sqrt( a7*a7 + a8*a8 + a9*a9);
}
}

