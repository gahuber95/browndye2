#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
WE_Simulator reads in the data, carries out a weighted-ensemble
simulation, and outputs data.  Also takes care of the multithreading. 
*/


#include "../molsystem/system_common.hh"
#include "../molsystem/system_thread.hh"
#include "we_simulator_interface.hh"
#include "../global/browndye_rng.hh"
#include "we_thread_runner.hh"
#include "../generic_algs/we.hh"

namespace Browndye{


class WE_Simulator: public Simulator{
public:
  WE_Simulator( JAM_XML_Pull_Parser::Node_Ptr, bool bb);

  void run();
  void generate_bins();

  bool building_bins = false;
  
private:
  //typedef System_State State;
  friend class WE_Interface;
  friend class WE_Thread_Runner;

  Sys_Copy_Index n_copies{ 0}, n_bin_copies{ 0};

  size_t n_steps = 0;
  size_t n_steps_per_output = 0;
  Rxn_Index n_final_rxns{ 1};

  std::ofstream output;
  std::ofstream bin_ofstream;

  Vector< double> bin_partitions; // make sure 0 has 0.0

  size_t istep = 0;
  std::streampos stream_position;
  Vector< double, Rxn_Index> next_fluxes;

  typedef Weighted_Ensemble_Dynamics::Sampler< WE_Interface> Sampler;
  std::unique_ptr< Sampler> sampler;
  Browndye_RNG rng;
  std::unique_ptr< WE_Thread_Runner> thread_runner;
  
  void get_bins( JAM_XML_Pull_Parser::Node_Ptr);
  
  void update_all();
  size_t number_of_fluxes() const;
  Sys_Copy_Index number_of_objects() const; 
};

//#########################################################
template< class Function> 
void WE_Interface::apply_to_bin_neighbors( WE_Simulator& sim, 
                                           size_t bin, 
                                           Function& f){
  const size_t n = sim.bin_partitions.size();
  
  if (bin == 0){
    f( 1);
  }
  else if (bin == (n-1)){
    f( n-2);
  }
  else{
    f( bin-1);
    f( bin+1);
  } 
}
//####################################################
inline
void WE_Simulator::update_all(){
  sampler->update_fluxes();
  sampler->renormalize_objects_1D();
}
}


/*

Input:

information for Molecule_Pair

Bins
  # copies per bin
  number
  locations 

total number of steps
number of steps per output
output file

Output:

probability fluxes
*/


