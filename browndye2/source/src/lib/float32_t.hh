#pragma once

/*
This is used to define a floating point number that is 32 bits.
It selects either "float" or "double", depending on the sizeof
function, which is used to select a template.  Needed for 
encode_binary.cc.
*/

namespace Browndye {
    static_assert( sizeof( float) == 4);
    typedef float float32_t;
}
