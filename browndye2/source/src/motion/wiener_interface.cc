#include "wiener_interface.hh"

namespace Browndye{

//#######################################################################################
void Wiener_Interface::set_difference( const W_Vector& a, const W_Vector& b, W_Vector& c){

  for( auto i: range( a.size())){
    auto& ai = a[i];
    auto& bi = b[i];
    auto& ci = c[i];
    for( auto j: range( ai.size())){
      auto& aij = ai[j];
      auto& bij = bi[j];
      auto& cij = ci[j];
      for( auto k: range( aij.fine_dW.size())){
        cij.fine_dW[k] = aij.fine_dW[k] - bij.fine_dW[k];
      }
      cij.coarse_dW = aij.coarse_dW - bij.coarse_dW;
    }
  }
}

//###############################################################
void Wiener_Interface::set_half( const W_Vector& a, W_Vector& c){
  
  for( auto i: range( a.size())){
    auto& ai = a[i];
    auto& ci = c[i];
    for( auto j: range( ai.size())){
      auto& aij = ai[j];
      auto& cij = ci[j];
      for( auto k: range( aij.fine_dW.size())){
        cij.fine_dW[k] = 0.5*aij.fine_dW[k];
      }
      cij.coarse_dW = 0.5*cij.coarse_dW;
    }
  }
}

//############################################
void Wiener_Interface::set_zero( W_Vector& w){
  
  for( auto& wi: w){
    for( auto& wij: wi){
      for( auto& wijk: wij.fine_dW){
        wijk = Vec3< Sqrt_Time>( Zeroi{});
      }
      wij.coarse_dW = Vec3< Sqrt_Time>( Zeroi{});
    }
  }  
}

//################################################################################
void Wiener_Interface::set_gaussian( System& state, Sqrt_Time sdev, W_Vector& w){
  
  auto& gen = gaussian( state);
  for( auto& wi: w){
    for( auto& wij: wi){
      for( auto& wijk: wij.fine_dW){
        for( auto kk: range(3)){
          wijk[kk] = sdev*gen.gaussian();  
        }
      }
      for( auto kk: range( 3)){
        wij.coarse_dW[kk] = sdev*gen.gaussian();
      }
    }
  }
}

//#################################################################################
void Wiener_Interface::add_gaussian( System& state, Sqrt_Time sdev, W_Vector& w){
  
  auto& gen = gaussian( state);
  for( auto& wi: w){
    for( auto& wij: wi){
      for( auto& wijk: wij.fine_dW){
        for( auto kk: range(3)){
          wijk[kk] += sdev*gen.gaussian();  
        }
      }
      for( auto kk: range( 3)){
        wij.coarse_dW[kk] += sdev*gen.gaussian();
      }
    }
  }
} 
  
//######################################################################
auto Wiener_Interface::wiener_vector( System_State& state) -> W_Vector{

  assert( state.mob_info);
  
  auto& mob_info = state.mob_info;
  Mob_Info_And_State mob_info_state{ state, *mob_info};
   
  auto ng = state.groups.size(); 
  W_Vector w( ng);
  
  Sys_Mob_Div_Index idiv( 0);
  
  Vector< Mob_Div_Index, Group_Index> n_group_divs( ng, Mob_Div_Index(0));
  for( auto& div: mob_info->divisions){
    auto ig = div.first;
    ++n_group_divs[ig];
  }
  
  //typedef Vector< Vec3< Sqrt_Time>, Atom_Index> Res2;
  typedef Coarse_And_Fine Res2;
  typedef Vector< Res2, Mob_Div_Index> Res1;
  for( auto ig: range( ng)){
    auto gdiv = n_group_divs[ig];
    
    Res1 w1( gdiv);
    for( auto jdiv: range( gdiv)){
      auto na = System_Mobility_Interface::n_beads( mob_info_state, idiv);
      Res2 w2;
      w2.fine_dW.resize( na);
      w1[ jdiv] = std::move( w2);
      ++idiv;
    }
      
    w[ig] = std::move( w1);
  }
  
  return w;
}
 
//#########################################################
auto Wiener_Interface::gaussian( System& state) -> Gaussian&{
  return state.thread().rng;
} 
 
//####################################################### 
std::pair< bool,bool>
Wiener_Interface::advance( System& system, [[maybe_unused]] Time t, Time dt,
                                          const W_Vector& ws){
  System_State::BD_Step_Result bd_res;
  system.do_bd_step( dt, ws, bd_res);
  return std::make_pair( bd_res.is_done, bd_res.must_backstep);
}
  
void Wiener_Interface::step_back( System& state, [[maybe_unused]] Time t, [[maybe_unused]] Time dt){
  state.restore();
} 
 
}


