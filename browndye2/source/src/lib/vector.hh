#pragma once

#include <type_traits>
#include <vector>
#include <algorithm>
#include <optional>
#include <functional>
#include "../global/error_msg.hh"
#include "default_values.hh"
#include "index.hh"
#include "index_selector.hh"
#include "range.hh"
#include "../global/debug.hh"
//*******************************************************************

namespace Browndye{

template< class T, class Idx>
class Vector;

//##########################################################
template< class T, class Idx = size_t>
class Vector: public std::vector<T>{
public:
  typedef std::vector<T> Parent;
  typedef const std::vector<T> CParent;
  typedef typename Parent::reference reference;
  typedef typename Parent::const_reference const_reference;
  typedef Idx index_type;
  typedef T value_type;
  
  Vector(): Parent(){}

  std::vector< T>& parent(){
    return *this;
  }

  const std::vector< T>& parent() const{
    return *this;
  }
  
  explicit Vector( Idx n): Parent( value(n)){
    if constexpr (debug){
      typedef JAM_Vector::Default_Value< T> DV;
      for( auto i = Idx(0); i < n; i++)
        (*this)[i] = DV::value();
    }
  }

  Vector( Idx n, const T& t): Parent( value(n), t){}

  template< class InputIt>
  Vector( InputIt first, InputIt last): Parent( first, last){}

  //  Vector( const Vector& other): Parent( other){}
  Vector( const Vector& other) = delete;
  
  Vector( Vector&& other) noexcept: Parent( std::move(other)){}

  Vector( Parent&& other) noexcept: Parent( std::move( other)){}
  
  Vector( std::initializer_list< T> init): Parent( init){}

  Vector copy() const{
    if constexpr( Index_Selector::has_copy_method< T>::value){
      return copy_vector();
    }
    else{
      return copy_basic();
    }
  }
  
  reference operator[]( Idx i){
    check_bounds( i);
    return Parent::operator[](value(i));
  }

  const_reference operator[]( Idx i) const{
    check_bounds( i);
    return Parent::operator[](value(i));
  }

  reference front(){
    if constexpr (bounds_check){
      if (Parent::size() == 0){
        error( "Vector::front: no members");
      }
    }
    return Parent::front();
  }

  const_reference front() const{
    if constexpr (bounds_check){
      if (Parent::size() == 0){
        error( "Vector::front: no members");
      }
    }  
    return Parent::front();
  }

  reference back(){
    if constexpr (bounds_check){
      if (Parent::size() == 0){
        error( "Vector::back: no members");
      }
    }
    return Parent::back();    
  }

  const_reference back() const{
    if constexpr (bounds_check){
      if (Parent::size() == 0){
        error( "Vector::back no members");
      }
    }
    return Parent::back();
  }

  Idx size() const{
    return Idx( Parent::size());
  }

  void reserve( Idx n){
    Parent::reserve( value(n));
  }
  
  Vector& operator=( const Vector& other) = delete;

  Vector& operator=( Vector&& other) noexcept {
    Parent::operator=( std::move(other));
    return *this;
  }

  Vector& operator=( Parent&& other){
    Parent::operator=( std::move(other));
    return *this;
  }
  
  void assign( Idx i, const T& t){
    assign( value(i), t);
  }

  reference at( Idx i){
    check_bounds( i);
    return Parent::at(value(i));
  }

  const_reference at( Idx i) const{
    check_bounds( i);
    return Parent::at(value(i));
  }

  void resize( Idx i){
    Parent::resize( value(i));
  }

  void resize( Idx i, const T& t){
    Parent::resize( value(i), t);
  }
  
  bool operator==( const Vector& other) const{
    auto* me = static_cast< CParent*>(this);
    auto* you = static_cast< CParent*>(&other);
    return (*me) == (*you);
  }

  // add other index-dependent functions later
  
  template< class F>
  auto mapped( F&& f) const{
    auto n = size();
    typedef std::result_of_t<F( T)>  Res;
    Vector< Res, Idx> res;
    res.reserve( n);
    for( auto& x: *this){
      res.emplace_back( f( x));
    }
    return res;
  }
    
  // works for zero-length
  template< class F, class A>
  auto folded_left( F&& f, A&& a) const{
    auto r = std::forward< A>( a);  
    for( auto i: range( size())){
      r = f( r, (*this)[i]);
    }  
    return r;
  }
   
  // must have at least two elements
  template< class F>
  auto folded_left( F&& f) const{
    auto r = f( (*this)[ Idx(0)], (*this)[ Idx(1)]);
    for( auto i: range( Idx(2), size())){
      r = f( r, (*this)[i]);
    }
    return r;
  }
  
  // works for zero-length
  template< class F, class A>
  auto foldedi_left( F&& f, A&& a) const{
    auto r = a;  
    for( auto i: range( size())){
      r = f( r, (*this)[i], i);
    }
    return r;
  }
  
  // must have at least two elements
  template< class F>
  auto foldedi_left( F&& f) const{
    auto r = f( (*this)[ Idx(0)], (*this)[ Idx(1)], Idx(1));
    for( auto i: range( Idx(2), size())){
      r = f( r, (*this)[i],i);
    }
    return r;
  }
    
  template< class F>
  void iterate( F&& f) const{
    for( auto& x: *this){
      f( x);
    }
  }
  
  template< class F>
  void iterate( F&& f){
    for( auto& x: *this){
      f( x);
    }
  }

  template< class F>
  Vector filtered( F&&f) const;

  template< class Comp>
  std::pair< Vector< T, Idx>, Vector< T, Idx> >
  partitioned( Comp f) const;
  
  template<class F>
  std::optional< std::pair< size_t, std::reference_wrapper< const T> > >
  found( F&& f) const;

  void fill( const T& t){
    std::fill( this->begin(), this->end(), t);
  }

  void copy_from( const Vector& other){
    auto n = other.size();
    this->resize( n);
    std::copy( other.begin(), other.end(), this->begin());
  }
  
  #ifdef DEBUG
  ~Vector(){
    this->clear();
    //this->shrink_to_fit();
  }
  #endif

  // make sure size_t versions are not visible when index is not an integer type
  static constexpr bool is_int = std::is_integral< Idx>::value;
  typedef typename Index_Selector::IBlocker< is_int>::type CType;

  reference operator[]( CType) = delete;
  const_reference operator[]( CType) const = delete;

  reference at( CType) = delete;
  const_reference at( CType) const = delete;

  explicit Vector( CType) = delete;
  Vector( CType, T& t) = delete;

  void assign( CType, const T&) = delete;

  void resize( CType) = delete;
  void resize( CType, const T&) = delete;


  private:
  Vector copy_basic() const{
    Vector res;
    res.reserve( size());
    for( const auto& x: *this){
      res.push_back( x);
    }
    return res;
  }
  
  Vector copy_vector() const{
    Vector res;
    res.reserve( size());
    for( const auto& x: *this){
      res.push_back( x.copy());
    }
    return res;
  }
  
  void check_bounds( Idx i) const{
    if constexpr (bounds_check){
      if (i >= size()){
        error( "Vector: out of bounds: ", i, " ", this->size());
      }
    }
  }

  static
  size_t value( Idx i){
    //return Index_NS::value( i);
    return i/Idx{1};
  }

};

//########################################
namespace Vectors{
  
  template< class T>
  class Is_Vector{
  public:
    static constexpr bool value = false;
  };
  
  template< class T, class Idx>
  class Is_Vector< Vector< T, Idx> >{
  public:
    static constexpr bool value = true;
  }; 

  template< class X>
  static
  X copy_gen( const X& x){
    if constexpr( Is_Vector< X>::value){
      return x.copy();
    }
    else{
      return x;
    }
  }

}

//##########################################
template< class F, class Idx>
auto initialized_vector( Idx n, F f){
  typedef std::result_of_t< F(Idx)> T;
  Vector< T, Idx> res;
  res.reserve( n);
  for( auto i: range( n))
    res.push_back( f(i));
  return res;
}

//######################################################
template< class T, class Idx>
Vector< T, Idx> concated( const Vector< T, Idx>& a,
                           const Vector< T, Idx>& b){
  
  auto na = a.size();
  auto nb = b.size();
  
  Vector< T, Idx> res( na+nb);
  for( auto i: range( na)){
    res[i] = Vectors::copy_gen( a[i]);
  }
  for( auto i: range(nb)){
    res[i + (na - Idx(0))] = Vectors::copy_gen( b[i]);
  }
  return res;
}

//######################################################
template< class T, class Idx>
Vector< T, Idx> subvector( const Vector< T, Idx>& v, Idx n0, Idx n1){
  
  Vector< T, Idx> res( n1 - (n0 - Idx(0)));
  for( auto i: range( n0, n1)){
    res[ i - (n0 - Idx(0))] = Vectors::copy_gen( v[i]);
  }
  return res;
}

  //#################################################
  template< class T, class Idx>
  template< class F>
  Vector< T, Idx> Vector< T, Idx>::filtered( F&& f) const{

    Vector res;
    std::copy_if( this->begin(), this->end(),
		  std::insert_iterator( res, res.end()),
		  std::forward< F>( f)); 
    return res;
  }

  //#################################################
  template< class T, class Idx>
  template< class F>
  std::pair< Vector< T, Idx>, Vector< T, Idx> >
  Vector< T, Idx>::partitioned( F f) const{

    Vector yes, no;
    for( auto& x: *this){
      if (f(x)){
    	yes.push_back( x);
      }
      else{
    	no.push_back( x);
      }
    }

    return std::make_pair( std::move( yes), std::move( no));
  }
  
//######################################################
  template< class T, class Idx>
  template<class F>
  std::optional< std::pair< size_t, std::reference_wrapper< const T> > >
  Vector< T, Idx>::found( F&& f) const{

    auto n = size();
    for( auto i: range( n)){
      T& t = (*this)[i];
      if (f(t)){
	return std::make_pair( i, std::cref(t)); 
      }
    }
    return std::nullopt; 
  }

  //##################################################
  template< class T, class Idx>
  Vector< T, Idx> appended( const Vector< T, Idx>& a,
			    const Vector< T, Idx>& b){

    auto na = a.size();
    auto nb = b.size();
    auto n = na + (nb - Idx(0));
    return initialized_vector( n, [&]( Idx i){
      if (i < na){
        return a[i];
      }
      else {
        return b[ Idx(0) + (i - na)];
      }
    });
  }
  //#################################################
  template< class Ta, class Tb, class Idx>
  auto dot( const Vector< Ta, Idx>& a, const Vector< Tb, Idx>& b){
    
    auto na = a.size();
    if (bounds_check){
      auto nb = b.size();
      if (nb != na){
        error( "Vector dot: mismatched sizes", na,nb);
      }
    }
    decltype( Ta()*Tb()) sum{ 0.0};
    for( auto i: range( na)){
      sum += a[i]*b[i];
    }
    return sum;
  }
  
  template< class T, class Idx>
  auto norm2( const Vector< T, Idx>& a){
    return dot( a,a);
  }

  template< class T, class Idx>
  T norm( const Vector< T, Idx>& a){
    return sqrt( norm2(a));
  }

  
  
}

