#include <cassert>
#include "other/mm_nonbonded_parameter_info.hh"
#include "other/mm_bonded_parameter_info.hh"
#include "other/cd_nonbonded_parameter_info.hh"
#include "other/cd_bonded_parameter_info.hh"
#include "../molsystem/system_common.hh"
#include "forces.hh"
#include "forces_impl.hh"
#include "absinth/absinth_interface.hh"
#include "lj_interface.hh"

namespace {

using namespace Browndye;

class FIface{
public:
  FIface() = default;

  typedef Browndye::System_State System_State;
  typedef Browndye::Group_State Group_State;
  typedef Browndye::Group_Thread Group_Thread;
  typedef Browndye::Chain_State Chain_State;
  typedef Browndye::Chain_Common Chain_Common;
  typedef Browndye::Core_State Core_State;
  typedef Browndye::Core_Thread Core_Thread;

  template< class C0, class FI>
  using Field_For_Blob = Browndye::Field_For_Blob< C0, FI>;
  
  template< class FI>
  using Blob_Interface = Browndye::Blob_Interface< FI>;
  
  template< size_t n, class BI>
  using Blob = Browndye::Charged_Blob::Blob< n, BI>;
  
  typedef Browndye::Chain_Col_Interface Chain_Col_Interface;
  
  //template< bool im0, bool im1, class Thread, class State0, class State1>
  template< bool im0, bool im1>
  static
  auto collision_structures( Core_State& state0, Core_State& state1){
    return Browndye::collision_structures< im0, im1>( state0, state1);
  }
  
  template< class CInterface, class FInterface>
  static
  Field_For_Blob< CInterface, FInterface>
  new_field_for_blob( const Field::Field< FInterface>& field,
                       Collision_Detector::Structure< CInterface>& cs,
                       typename Field::Field< FInterface>::Hint& hint){
    return Browndye::new_field_for_blob( field, cs, hint);
  }
};

//########################################################
/*
void push_forces_to_tets( System_State& state){
   
  for( auto& group: state.groups){
    group.push_forces_to_tets();
  } 
}
*/
}

//#########################################################################
namespace Browndye{

void compute_forces( System_State& state){

  //cout << "compute forces" << endl;
  assert( state.constraints_consistent());

  auto& common = state.common();
  auto& solvent = common.solvent;
  
  typedef Force_Field_Type FF;
  auto fft = common.force_field;
  if (fft == FF::Molecular_Mechanics){
    auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics>&>( *(common.pinfo));
    auto& bpinfo = dynamic_cast< const MM_Bonded_Parameter_Info&>( *(common.bpinfo));
    
    class I: public FIface{
    public:
      typedef MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics> PInfo;
      typedef MM_Bonded_Parameter_Info BPInfo;
      
      static constexpr bool is_absinth(){
        return false;
      }
    };
    
    compute_all_forces< I, compute_energy>( pinfo, bpinfo, solvent, state);
  }
  else if (fft == FF::Molecular_Mechanics_HP){
    auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics_HP>&>( *(common.pinfo));
    auto& bpinfo = dynamic_cast< const MM_Bonded_Parameter_Info&>( *(common.bpinfo));

    class I: public FIface{
    public:
      typedef MM_Nonbonded_Parameter_Info< FF::Molecular_Mechanics_HP> PInfo;
      typedef MM_Bonded_Parameter_Info BPInfo;

      static constexpr bool is_absinth(){
        return false;
      }
    };

    compute_all_forces< I, compute_energy>( pinfo, bpinfo, solvent, state);
  }

  else if (fft == FF::Spline){
    auto& pinfo = dynamic_cast< const CD_Nonbonded_Parameter_Info&>( *(common.pinfo));
    auto& bpinfo = dynamic_cast< const CD_Bonded_Parameter_Info&>( *(common.bpinfo));
    
    class I: public FIface{
     public:
       typedef CD_Nonbonded_Parameter_Info PInfo;
       typedef CD_Bonded_Parameter_Info BPInfo;
       
       static constexpr bool is_absinth(){
         return false;
       }
     };
    
    compute_all_forces<I, compute_energy>( pinfo, bpinfo, solvent, state);
  }
  else if (fft == FF::Absinth){
    auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< FF::Absinth>&>( *(common.pinfo));
    auto& bpinfo = dynamic_cast< const MM_Bonded_Parameter_Info&>( *(common.bpinfo));

    class I: public FIface{
    public:
      typedef MM_Nonbonded_Parameter_Info< FF::Absinth> PInfo;
      typedef MM_Bonded_Parameter_Info BPInfo;
      
      static constexpr bool is_absinth(){
        return true;
      }
    };

    compute_all_forces< I, compute_energy>( pinfo, bpinfo, solvent, state);
  }
  
  else{
    error( __FILE__, __LINE__, "force field undefined");
  }
   
  state.compute_tet_forces(); 
  state.test_force_balance( 0);
  state.set_force_consistency_true();
}

//##################################################################################################
void fill_in_excluded_flags( const Vector< Atom_Index>& excluded, Vector< bool, Atom_Index>& flags){
  
  auto mna = excluded.folded_left( []( Atom_Index ia, Atom_Index ja){ return max( ia,ja);}, Atom_Index(0));
  auto mnap = inced( mna);
  if (flags.size() < mnap){
    flags.resize( mnap);
  }
  flags.fill( false);
  for( auto iex: excluded){
    flags[iex] = true;
  }
}

// new, add later
/*
  void compute_lj_forces( System_State& state){

    auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< Force_Field_Type::Molecular_Mechanics>&>( *(state.common().pinfo));
    LJ::Sys sys( state, pinfo);

    LJ_Force_Computer::Computer< LJ_Iface>::add_forces( sys);
  }
*/

  void compute_absinth_forces( System_State& state){

    auto& common = state.common();
    auto& pinfo = dynamic_cast< const MM_Nonbonded_Parameter_Info< Force_Field_Type::Absinth>&>( *(common.pinfo));
    auto& bpinfo = dynamic_cast< const MM_Bonded_Parameter_Info&>( *(common.bpinfo));

    Parameters< MM_Nonbonded_Parameter_Info< Force_Field_Type::Absinth>, MM_Bonded_Parameter_Info> params( common.solvent, pinfo, bpinfo);

    //Absinth_Iface_In::Sys sys( state, params);
    auto& g0_threads = state.thread().groups.front().core_threads;
    typedef Absinth_Iface_In::Sys ASys;
    auto sys = g0_threads.empty() ? ASys( state, params) : ASys( state, params, g0_threads.front().ab_atoms);
    //alt_test_force_balance(state, 0);
    Absinth::Computer< Absinth_Iface< true> >::add_forces( sys);
    //alt_test_force_balance(state, 1);
  }

  //########################################################
  void alt_test_force_balance(const System_State &state, int flag) {
    F3 total_f( Zeroi{});
    T3 total_t( Zeroi{});
    for( auto& group: state.groups){
      for( auto& core: group.core_states){
        total_f += core.force;
        total_t += core.torque + cross( core.translation(), core.force);
      }
      for( auto& chain: group.chain_states){
        for( auto& atom: chain.atoms){
          total_f += atom.force;
          total_t += cross( atom.pos, atom.force);
        }
      }
    }

    if( norm( total_f) > Force{1.0e-3} || norm( total_t) > Torque{1.0e-3}){
      std::cerr << "failed\n";
      for( auto& group: state.groups){
        for( auto& core: group.core_states){
          std::cerr << "core force " << core.force << std::endl;
        }
        for( auto& chain: group.chain_states) {
          for (auto &atom: chain.atoms) {
            std::cerr << "atom force " << atom.force << std::endl;
          }
        }
      }
      error( "alt test force balance failed", flag, total_f, total_t);
    }
  }

}


