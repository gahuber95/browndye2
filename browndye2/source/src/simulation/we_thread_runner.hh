#pragma once

#include <future>
#include <variant>
#include <stack>
#include <list>
#include "../multithread/barrier.hh"
#include "../global/indices.hh"
#include "../lib/vector.hh"
#include "../molsystem/system_state.hh"

namespace Browndye{

class WE_Simulator;

struct WE_Task{
  std::future< std::optional< std::string> > result;
  std::variant< Sys_Copy_Index, std::string> message;
  
  std::map< Sys_Copy_Index, std::unique_ptr< System_State> >::iterator itr;
  
  Sys_Copy_Index next_icpx, n_completed{ 0};
  
  typedef decltype( Sys_Copy_Index() - Sys_Copy_Index()) SC_Diff;
  SC_Diff stride;
  bool almost_done = false;
  
  std::atomic_flag being_processed;
};

//#####################################################
class WE_Thread_Runner{
public:
  typedef WE_Simulator Sim;
  
  explicit WE_Thread_Runner( Sim&);
  void run_threads();
  
private:
  typedef std::lock_guard< std::mutex> Lock;
  friend class WE_Interface;
  typedef System_State State;
  
  Sim& sim;
  
  std::mutex mutex; 
  std::list< WE_Task> tasks;
  Vector< WE_Task*, Thread_Index> task_refs;
  Sys_Copy_Index ndone{ 0};
  Thread_Index tdone{ 0};

  Barrier barrier;
  volatile bool signaled = false;
  std::stack< Thread_Index> needy;
  volatile bool has_error = false;
  std::optional< std::string> error_msg;
  
  void run_step( Thread_Index ith);
  void run_steps( Thread_Index ith);
  void step_state( State&, Thread_Index);
  void setup_needy( WE_Task& task);
  void advance_task( WE_Task& task, Thread_Index);
  WE_Task& setup_task( Thread_Index ith);
  void finish_if_last( int);
  
  template< class Idx, class Itr>
  void advance_iterator( Idx n, Itr& itr);
  
  
  static
  double frac_remaining( const WE_Task& task, Thread_Index nt,
                          Sys_Copy_Index nc);
};

}