#include "nonbonded_parameter_info.hh"

namespace Browndye{

namespace JP = JAM_XML_Pull_Parser;

// class is either Parser or Node_Ptr
auto Nonbonded_Parameter_Info::atoms_from_xml_gen( JP::Parser& cont) const -> Atoms {
    
  Atoms atoms;
  auto process_residue = [&]( const JP::Node_Ptr& rnode) -> void{
    
    rnode->complete();
    auto residue_name = checked_value_from_node< std::string>( rnode, "name");
    auto anodes = rnode->children_of_tag( "atom");
    for( const auto& anode: anodes){
      Atom atom{};
      auto& pos = atom.pos;
      pos[0] = checked_value_from_node< Length>( anode, "x");
      pos[1] = checked_value_from_node< Length>( anode, "y");
      pos[2] = checked_value_from_node< Length>( anode, "z");
      auto atom_name = checked_value_from_node< std::string>( anode, "name");
            
      atom.radius = checked_value_from_node< Length>( anode, "radius");
      atom.charge = checked_value_from_node< Charge>( anode, "charge");
      atom.number = checked_value_from_node< Atom_Index>( anode, "number");
      auto sasa_opt = value_from_node< Length2>( anode, "sasa");
      atom.set_sasa( sasa_opt ? sasa_opt.value() : Length2( 0.0));

      auto vol_opt = value_from_node< Length3>( anode, "volume");
      atom.volume = vol_opt ? vol_opt.value() : Length3( 0.0);

      auto it_opt = index( residue_name, atom_name);
      
      atom.type = it_opt ? it_opt.value() : Atom_Type_Index( maxi);

      // for ABSINTH
      if (!surface_energies.empty()){
        SE_Key keyn{ residue_name, atom_name}, keyi{ residue_name, atom.number};
        auto itrn = surface_energies.find( keyn);
        auto see = surface_energies.end();
        if (itrn != see) {
          auto energy = itrn->second;
          atom.set_surface_energy(energy);
        }
        else{
          auto itri = surface_energies.find( keyi);
          if (itri != see){
            auto energy = itri->second;
            atom.set_surface_energy( energy);
          }
          else{
            atom.set_surface_energy( Energy{0.0});
          }
        }

      }
      
      atoms.push_back( atom);
    }
  };
  
  cont.apply_to_nodes_of_tag( cont.top(), "residue", process_residue);
  
  establish_interaction_radii( atoms); 
  return atoms;
}

//################################################################
auto Nonbonded_Parameter_Info::atoms_from_file( const std::string& file) const -> Atoms{

  std::ifstream input( file);
  if (!input.is_open()){
    error( "atoms file ", file, " could not be opened");
  }
  JAM_XML_Pull_Parser::Parser parser( input, file);
  return atoms_from_xml_gen( parser);
}

//################################################################
std::optional< Atom_Type_Index>
Nonbonded_Parameter_Info::index( const std::string& residue, const std::string& atom) const{
 
  auto itr = dict.find( make_pair( residue, atom));
  return (itr == dict.end()) ?
    std::nullopt : std::make_optional( itr->second);
}

Nonbonded_Parameter_Info::~Nonbonded_Parameter_Info()= default;

}
