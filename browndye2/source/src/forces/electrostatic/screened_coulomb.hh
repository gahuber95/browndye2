#pragma once
#include "../../global/pos.hh"
#include "../../structures/solvent.hh"
#include "../../global/pi.hh"
#include "../../global/physical_constants.hh"


namespace Browndye{

inline
F3 screened_coulomb( const Solvent& solvent, Pos r01, Length r, Charge q0, Charge q1){
  
  auto eps = solvent.dielectric*vacuum_permittivity;
  auto dL = solvent.debye_length;
  return (q0*q1*(r/dL + 1.0)*exp(-r/dL)/(r*r*r*pi4*eps))*r01;
}
  
}
 