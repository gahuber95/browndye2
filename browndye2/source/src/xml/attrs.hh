#pragma once

/*
Defines class Attr for containing the attributes of an XML element.
*/

#include <map>
#include <optional>
#include "str.hh"

namespace Browndye{
namespace JAM_XML_Parser{

  class Attrs{  
  public:
    Attrs() = default;
    Attrs( const Attrs& attrs) = default;
    Attrs( Attrs&& attrs) = default;
    Attrs& operator=( const Attrs& attrs) = default;
    Attrs& operator=( Attrs&& attrs) = default;
    
    std::optional< String> value( const String& key) const{
      auto itr = dict.find( key);
      if (!(itr == dict.end())){
        return itr->second;
      }
      else{
        return std::nullopt;
      }
    }
    
    void insert( const String& key, const String& res){
      dict.insert( std::make_pair( key, res));
    }

    std::map< String, String> dict;
  };
}}

