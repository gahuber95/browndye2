#include "files_macros.hh"

#ifdef USING_POSIX

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctime>
#include <string>
#include "../global/error_msg.hh"
#include "files.hh"

namespace Browndye{
namespace Orchestrator{
  
  //#################################################################
  typedef struct stat Stat;
  
  template< class S>
  time_t ftime( const S& fstat){
    
    return fstat.st_ctime;
  }
  
  //############################################
  time_t file_time( const std::string& name){
    
    Stat fstat;
    auto res = stat( name.c_str(), &fstat);
    if (res != 0){
      error( "file_time failed for", name, res);
    }
    
    return ftime( fstat);
  }
  
  //############################################
  bool file_exists( const std::string& name){
    return access( name.c_str(), F_OK ) == 0;
  }
  
  //######################################
  void remove_file( const std::string& name){
    auto res = unlink( name.c_str());
    if (res != 0){
      error( "unlink failed for ", name, res);
    }
  }
  
  //###############################################################
  void rename_file( const std::string& from, const std::string& to){
    
    constexpr size_t nb = 1000;
    char buffer[nb];
    auto path = getcwd( buffer, nb);
    if (path){
      auto dfrom = path + ("/" + from);
      auto dto = path + ("/" + to);
      auto res = rename( dfrom.c_str(), dto.c_str());
      if (res != 0){
        error( "rename from", from, " to ", to, " did not succeed");
      }
    }
    else{
      error( "directory could not be found");
    }
  }
}}

#endif
