#pragma once
//
// Created by ghuber on 9/26/23.
//

#include "../lib/array.hh"
#include "../lib/units.hh"

namespace Browndye {

  struct Absinth_Atom {

    Array<double, 5> other;

    static constexpr size_t Eta = 0;
    static constexpr size_t DEdeta = 1;
    static constexpr size_t Polar_V = 2;
    static constexpr size_t Polar_dVdeta = 3;
    static constexpr size_t Eta0 = 4;

    static constexpr Energy E1{1.0};

#ifdef DEBUG
    Absinth_Atom(){
      std::fill( other.begin(), other.end(), NAN);
    }
#endif

    void set_eta(double eta) {
      other[Eta] = eta;
    }

    void set_eta0(double eta) {
      other[Eta0] = eta;
    }

    void add_to_dEdeta(Energy dE) {
      other[DEdeta] += dE / E1;
    }

    [[nodiscard]] double eta() const {
      return other[Eta];
    }

    [[nodiscard]] double eta0() const {
      return other[Eta0];
    }

    void set_polar_v(double v) {
      other[Polar_V] = v;
    }

    [[nodiscard]] double polar_v() const {
      return other[Polar_V];
    }

    void set_polar_dvdeta(double dvde) {
      other[Polar_dVdeta] = dvde;
    }

    [[nodiscard]] double polar_dvdeta() const {
      return other[Polar_dVdeta];
    }

    [[nodiscard]] Energy dEdeta() const {
      return other[DEdeta] * E1;
    }

    void set_dEdeta(Energy dEde) {
      other[DEdeta] = dEde / E1;
    }

  };

}