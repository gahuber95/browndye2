#include <utility>
#include "../molsystem/system_state.hh"
#include "wiener_interface.hh"
#include "../forces/forces.hh"
#include "qb_factor.hh"
namespace {
  using namespace Browndye;
  using std::get;
  using std::min;
  
  typedef decltype( Length3()/Time()) DFactor;

  std::pair< DFactor, bool> D_factor_hi( const System_Common& common);
  Time pair_dt( bool has_hi, DFactor D_factor,
                  const Tet< Group_State>& tet0,
                   const Tet< Group_State>& tet1);

  constexpr Group_Index g1( 1);  
}

//################################################
namespace Browndye{

void System_State::step_const_dt( Time dt){
  
  typedef Wiener_Interface WI;
   
  auto ws = WI::wiener_vector( *this);
  WI::set_gaussian( *this, sqrt( dt), ws);
  BD_Step_Result bd_res;
  do_bd_step( dt, ws, bd_res);
  last_dt = dt;
}

//#######################################################
void System_State::step_variable_dt(){
   
  const double dt_growth_factor{ 1.1};
  auto dt = (last_dt == Time( large)) ?
    time_step_guess() :
    min( last_dt*dt_growth_factor, max_time_step());
  
  typedef Wiener_Interface WI;
  
  if (!wprocess || wprocess->at_end()){
    wprocess =
      std::make_unique< WProc>(
         Wiener_Loop::starting_wprocess< WI>( *this, dt));
  }
    
  last_dt = Wiener_Loop::do_one_bd_step< WI>( *this, *wprocess);
}

//#########################################################
// assume positions and forces are in sync, going in and out
// only takes one step, but takes the largest possible
void System_State::do_large_step( Thread_Index){
       
  assert( consistency.all());
   
  auto old_rxn_state = current_rxn_state;
  
  auto cdto = constant_dt();
  if (cdto){
    auto dt = cdto.value();
    step_const_dt( dt);
  }
  else{
    step_variable_dt();
  }
    
  if (fate != System_Fate::Final_Rxn){
    if (!common().density_field) {
      recenter();
    }
    
    if (!(common().no_nam)){
      do_bimolecular_case();
    }
  }

  if (!cdto && wprocess->at_end()){
    bool frozen_changed;
    update_geometry( frozen_changed);
    if (frozen_changed){
      update_mobility_geometry();
    }
    update_mobility_if_needed( frozen_changed);
  }
  else{
    update_mobility_if_needed( false);
  }
    
  time += last_dt;
  t_since_output += last_dt;
  ++n_full_steps;
  had_reaction = (current_rxn_state != old_rxn_state);
}
//########################################################
Time System_State::max_time_step() const{
  
  Time dt{ large}; 
  auto ng = groups.size();
  for( auto ig: range(ng)){
    dt = min( dt, max_time_within_group( ig));
    dt = min( dt, max_time_between_groups_below( ig));
  }
  return dt;
}

//##################################################
Time System_State::max_time_within_group( Group_Index ig) const{
  
  //auto [D_factor,has_hi] = D_factor_hi( common());
  auto dres = D_factor_hi( common());
  auto D_factor = dres.first;
  auto has_hi = dres.second;
  
  
  auto& group = groups[ ig];
  auto& tets = group.tets;
  
  //Time dt{ large};
  
  // refactor
  auto dt = [&]{
    Time dt0{ large};
    for( auto& tet: tets){
      auto R = tet.fixed->radius;
      Time tscale = 4.0*R*R*R/D_factor;
      dt0 = min( dt0, tscale);
    }
    return dt0;
  }();
  
  auto nt = tets.size();
  for( auto it: range( nt)){
    auto& teti = tets[it];
    for( auto jt: range( it)){
      auto& tetj = tets[jt];
      dt = min( dt, pair_dt( has_hi, D_factor, teti, tetj));
    }
  }
  return dt;
}

//##########################################################
Time System_State::max_time_between_groups_below( Group_Index ig) const{
  
  auto [D_factor,has_hi] = D_factor_hi( common());
  
  auto& groupi = groups[ig];
  Time dt{ large};
  auto& tetsi = groupi.tets;
  for( auto jg: range( ig)){
    auto& groupj = groups[jg];
    auto& tetsj = groupj.tets;
    for( auto& teti: tetsi){
      for( auto& tetj: tetsj){
        dt = min( dt, pair_dt( has_hi, D_factor, teti, tetj));
      }
    }
  }
  return dt;
}

//######################################################################
void System_State::update_mobility_if_needed( bool new_hi_geom){
  
  if (has_hi()){
    auto nsphu = common().n_steps_per_hi_update;
    if ((n_full_steps % nsphu) == 0 || new_hi_geom){
      update_mobility_quantities();
    }
  }
}

//#####################################################  
void System_State::do_bimolecular_case(){
  
  auto& com = common();
  auto cen1 = groups[ g1].center();
  auto r = norm( cen1);
  if (r > qb_factor*com.b_rad){
    //cout << "r " << r << '\n';
    last_dt = Time( large);
    
    if (com.building_bins){
      do_building_bins_case( cen1);
    }
    else{
      do_nam_case( cen1);   
    }
  }
}
  
//#####################################################################  
void System_State::do_nam_case( Pos pos1){
  
  auto& com = common();

  Transform tform1( id_matrix<3>(), pos1);
  auto res = com.outer_propagator->new_state( thread().rng, tform1);
  auto survived = res.first;
   
  if (survived){
    auto new_tform1 = res.second;
    auto dtform1 = new_tform1*tform1.inverse();
    transform_group1( dtform1);
    fate = System_Fate::In_Action;
  }
  else{
    fate = System_Fate::Escaped;
  }
}
  
//###############################################################  
void System_State::do_building_bins_case( Pos pos1){
  
  auto new_pos1 = common().b_rad*normed( pos1);
  auto d = new_pos1 - pos1;
  transform_group1( Transform( id_matrix<3>(), d));
}
 
//#############################################################
void System_State::transform_group1( Transform tform){

  auto& group1 = groups[ g1];
  group1.transform( tform);
  
  bool frozen_changed;
  update_geometry( frozen_changed);
  compute_forces(*this);

  if (has_hi()){
    if (frozen_changed){
      update_mobility_geometry();
    }
    update_mobility_quantities();
  }
}
}

namespace {
  //##########################################################
  Time pair_dt( bool has_hi, DFactor D_factor,
               const Tet< Group_State>& tet0,
               const Tet< Group_State>& tet1){
     
    auto cen0 = centroid( tet0.poses);
    auto cen1 = centroid( tet1.poses);    
    auto r = distance( cen0, cen1);
    auto a0 = tet0.hi_radius( has_hi);
    auto a1 = tet1.hi_radius( has_hi);
      
    auto D = D_factor*(1.0/a0 + 1.0/a1);
    // mean change in separation less than frac of separation
    constexpr double frac = 0.1;
    return (frac*frac/2)*r*r/D;
  }
  
  //###############################################
  std::pair< DFactor, bool> D_factor_hi( const System_Common& common){
    
    auto& solvent = common.solvent; 
    auto dfactor = solvent.diffusivity_factor(); 
    return std::make_pair( dfactor/pi6, common.has_hi);
  }

}
