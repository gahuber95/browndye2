#pragma once
#include "core_common.hh"
#include "../absinth_atom.hh"

namespace Browndye {

  struct TPosition {
    bool transformed = false;
    Pos pos;
  };

  class Core_Common;

  class Pre_Core_Thread {
  public:

    explicit Pre_Core_Thread( const Core_Common &_common) : common(_common) {}

    const Core_Common &common;
    Vector< TPosition, Atom_Index> atom_tposes, atom_tposes_alt;
    Vector< Absinth_Atom, Atom_Index> ab_atoms;
  };

}