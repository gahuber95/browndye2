#pragma once
#include "../lib/units.hh"

namespace Browndye{

  Length3 sphere_overlap( Length Ra, Length Rb, Length r);

}
