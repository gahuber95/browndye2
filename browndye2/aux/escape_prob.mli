(* Used internally.  Computes integral used in determining escape
probabilities.. 

integral has_hi a0 a1 q dL, where a0 and a1 are particle radii, q is
sgn( q0*q1)*sqrt( q0*q1), and dL is Debye length.
*)

val integral: bool->float->float->float->float -> float
