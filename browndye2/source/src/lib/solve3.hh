#pragma once

/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
template< class Um, class Ub, class Ux>
void Solve3::solve( Mat3< Um>& m, Vec3< Ub>& b, Vec3< Ux>& x)

Solves a generic 3x3 system using LU decomposition and back-substitution.
*/


#include <stdio.h>
#include <cmath>
#include <stdlib.h>
#include "units.hh"
#include "array.hh"

namespace Browndye{

namespace Solve3{

template< class U>
void swap( Vec3< U>& a, unsigned int i, Vec3< U>& b, unsigned int j){
  U temp = a[i];
  a[i] = b[j];
  b[j] = temp;
}

template< class U>
void swapm( Mat3< U>& a, unsigned int i, unsigned int j, 
            Mat3< U>& b, unsigned int m, unsigned int n){

  U temp = a[i][j];
  a[i][j] = b[m][n];
  b[m][n] = temp;
}

template< class Um, class Ub, class Ux>
void solve( Mat3< Um>& m, Vec3< Ub>& b, Vec3< Ux>& x){

  if 
    ((fabs( m[1][0]) > fabs( m[0][0])) && 
     (fabs( m[1][0]) >= fabs( m[2][0]))) 
    {
    for (unsigned int  k=0; k<=2; k++){ 
      swapm( m, 0, k, m, 1, k);
    }
    swap( b, 0, b, 1);
    }
  else if 
    ((fabs( m[2][0]) > fabs( m[0][0])) && 
     (fabs( m[2][0]) >= fabs( m[1][0])))
    {
      for (unsigned int  k = 0; k<=2; k++){ 
        swapm( m, 0, k, m, 2, k);
      }
      swap( b, 0, b, 2);
    }

  double a01 = m[1][0]/m[0][0] ;
  
  for (unsigned int k=0; k<=2; k++){ 
    m[1][k] = m[1][k] - a01*m[0][k];
  }
  b[1] = b[1] -  a01*b[0];
  
  double a02 = m[2][0]/m[0][0] ;

  for (unsigned int k=0; k<=2; k++){ 
    m[2][k] = m[2][k] - a02*m[0][k];
  }
  
  b[2] = b[2] - a02*b[0];
  
  if 
    (fabs( m[2][1]) > fabs( m[1][1])) 
    {
      for (unsigned int  k=1; k<=2; k++){ 
        swapm( m, 2, k, m, 1, k);
      }
      swap( b, 2, b, 1);
    }

  double a12 = m[2][1]/m[1][1] ;
  
  for (unsigned int k=1; k<=2; k++){
    m[2][k] = m[2][k] - a12*m[1][k];
  }
  b[2] = b[2] - a12*b[1];
  
  // backsubstitution 
  x[2] = b[2]/m[2][2];
  x[1] = (b[1] - m[1][2]*x[2])/m[1][1];
  x[0] = (b[0] - m[0][1]*x[1] - m[0][2]*x[2])/m[0][0];
}

}}

  

