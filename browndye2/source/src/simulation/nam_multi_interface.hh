/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements interface to Multithread::Mover class. Included by
"nam_simulator.hh".
*/

#include "../multithread/multithread_mover.hh"

namespace Browndye{

class NAM_Simulator;
class System_State;

/*
class NAM_Multi_Interface{
public:
  typedef Jam_Exception Exception;
  typedef NAM_Simulator* Objects_Ref;
  typedef System_State* Object_Ref;

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects_Ref objects);
 
  static size_t size( Objects_Ref objects);
  static void do_move( Multithread::Mover< NAM_Multi_Interface>&,
                      Objects_Ref objects, size_t, Object_Ref obj);
  static void set_null( Objects_Ref& objects);
  static bool is_null( Objects_Ref objects);

};
                                                
inline
void NAM_Multi_Interface::set_null( NAM_Simulator*& sim){
  sim = nullptr;
}

inline
bool NAM_Multi_Interface::is_null( NAM_Simulator* sim){
  return (sim == nullptr);
}
*/
}
