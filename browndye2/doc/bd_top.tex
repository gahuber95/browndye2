\documentclass[12pt]{article}
\usepackage{amssymb, amsmath, graphicx, url, listings, tikz}
\usepackage{enumitem}
\usetikzlibrary{matrix,shapes,arrows,positioning,chains}

\setlistdepth{9}

\setlist[itemize,1]{label=$\bullet$}
\setlist[itemize,2]{label=$\bullet$}
\setlist[itemize,3]{label=$\bullet$}
\setlist[itemize,4]{label=$\bullet$}
\setlist[itemize,5]{label=$\bullet$}
\setlist[itemize,6]{label=$\bullet$}
\setlist[itemize,7]{label=$\bullet$}
\setlist[itemize,8]{label=$\bullet$}
\setlist[itemize,9]{label=$\bullet$}

\renewlist{itemize}{itemize}{9}


\title{bd\_top User's Manual}
\author{Gary Huber}
\begin{document}

% Define block styles
\tikzset{
block/.style={
    rectangle,
    draw,
%    text width=10em,
    text centered,
    rounded corners
},
cloud/.style={
    draw,
    ellipse,
    minimum height=2em
},
descr/.style={
    fill=white,
    inner sep=2.5pt
},
connector/.style={
    -latex,
    font=\scriptsize
},
rectangle connector/.style={
    connector,
    to path={(\tikztostart) -- ++(#1,0pt) \tikztonodes |- (\tikztotarget) },
    pos=0.5
},
rectangle connector/.default=-2cm,
straight connector/.style={
    connector,
    to path=--(\tikztotarget) \tikztonodes
}
}

\maketitle

\section{Introduction}
As in Browndye 1.0, {\bf bd\_top} automates some of the preparation for the actual Browndye simulation.  It does this by running some of the auxilliary programs and
keep track of the intermediate files. For now, it works for two general cases.

First, it can handle an arbitrary number of rigid cores when the molecular mechanics model is being used.  In other words, it has the functionality of Browndye 1.0 but with more than two cores possible.  

Second, if the COFFDROP model is being used, it can handle an arbitrary number
of rigid cores and chains connected into one or two groups.  A chain
can be unattached to a core, attached at one end, or attached to 
by two ends to the same core or to two different cores.  For now, the
simulator cannot handle more than two groups. A {\it group} is a collection
of cores and chains that are all connected and cannot drift apart.

The inputs include, for each core, the XML output of {\it pqr2xml} and
zero or more APBS grids.  For each chain, the input includes
the XML output of {\it generate\_coffdrop\_beads}, and some optional lines that
describe how it is attached to the cores.

When {\it bd\_top} runs, it generates several intermediate files for
the solvent and each core and chain, and the simulation file 
that is later given to {\it nam\_simulation} or {\it we\_simulation}.  
It is possible to make changes to any of the intermediate files,
and if {\it bd\_top} is run again, it updates only those files
that depend on the one that was changed.

The simulation file that is generated has the form 
{\it name1\_simulation.xml} or {\it name1\_name2\_simulation.xml}, where
{\it name1} and {\it name2} are the names of the groups. 

\section{File Structure}

\begin{lstlisting}
<root>
  <n_threads> integer </n_threads> # optional
  <seed> integer </seed>
  <output> filename </output>

  # for nam_simulation
  <n_trajectories> integer </n_trajectories>
  <n_trajectories_per_output> integer </n_trajectories_per_output> # optional
  <max_n_steps> integer </max_n_steps>
  <trajectory_file> filename </trajectory_file> # optional
  <n_steps_per_output> integer </n_steps_per_output> # optional
  <min_rxn_dist_file> filename </min_rxn_dist_file> # optional

  # for we_simulation
  <n_copies> integer </n_copies>
  <n_bin_copies> integer </n_bin_copies>
  <n_steps> integer </n_steps>
  <n_we_steps_per_output> integer </n_we_steps_per_output>
  <bin_file> filename </bin_file>

  # system description
  <system>
    <force_field> name </force_field>
    <parameters> filename </parameters>
    <connectivity> filename </connectivity> # needed for COFFDROP
    <start_at_site> true/false </start_at_site> # optional
    <b_radius> real </b_radius> # optional 
    <reaction_file> filename </reaction_file> # optional
    <hydrodynamic_interactions> true/false </hydrodynamic_interactions> # optional
    <n_steps_between_hi_updates> integer <n_steps_between_hi_updates> # optional
    
    <density_field> filename </density_field> # optional
    <atom_weights> filename </atom_weights> # optional

    <solvent>
      <debye_length> real </debye_length>
      <dielectric> real </dielectric>
      <relative_viscosity> real </relative_viscosity>
      <kT> real </kT>
      <desolvation_parameter> real </desolvation_parameter>
      <solvent_radius> real </solvent_radius>
    </solvent>

    <time_step_tolerances> # optional
      <force> real </force>
      <reaction> real </reaction>
      <minimum_core_dt> real </minimum_core_dt>
      <minimum_chain_dt> real </minimum_chain_dt>
      <minimum_core_reaction_dt> real </minimum_core_reaction_dt>
      <minimum_chain_reaction_dt> real </minimum_chain_reaction_dt>
    </time_step_tolerances>

    <group>
      <name> name </name>
      <core> 
        <name> name </name>
        <atoms> filename </atoms>
        <all_in_surface> true/false </all_in_surface> # optional

        <electric_field> # optional
          <grid> filename </grid> # one or more
        </electric_field>

        <dielectric> real </dielectric>

      </core>  
      # zero or more

      <chain> 
        <name> name </name>
        <atoms> file </atoms>
        <constant_dt> real </constant_dt> # optional
        <constraints> true/false </constraints> # optional
        
        <link>
          <core_name> name </core_name>
          <core_residue> integer </core_residue>
          <chain_residue> integer </chain_residue>
        </link>
        # 0, 1, or 2

      </chain> # zero or more

    </group>
    # 1 or 2

  </system>

</root>
\end{lstlisting}
Most of the XML tags are identical to those in the
simulation file, and are simply copied over.  The exceptions are noted below.
\begin{itemize}
\item{\bf solvent} - The contents are simply copied over to a newly generated solvent file,
  which is an input to the simulation program. One addition is the
  {\bf solvent\_radius} tag, which represents the probe radius and
  exclusion radius in various auxilliary programs.  If left out, the
  default value is 1.5 \AA.
\item{\bf connectivity} - this file describes how the beads in the chain model are connected.  For COFFDROP, the file {\it connectivity.xml} from the website can be used.
\item{\bf link} - This sits within the {\bf chain} tag. Each instance of this item
describes a connection between the chain and a core.  The name of the core, the
number of the linking residue, and the number of the chain's linking residue are
specified.
\item{\bf all\_in\_surface} - If this is true, then all of the atoms of the 
core are included, not just a shell made of the surface atoms.  Default is false.
\item{\bf is\_protein} - If this is true, then it affects how the effective
charges are generated.  Default is false. 
\item{\bf atoms} - When using the COFFDROP model, the atoms files for cores
and chains must contain the coarse-grained ``beads''. This will usually be 
the output of {\it generate\_coffdrop\_beads}.
\item{\bf constraints} - If true, all of the bonds are treated as rigid constraints. Default is false.
\end{itemize} 

\section{Programs and Files}
\subsection{For Each Core}
This is very similar to the flow in Browndye 1.
The files are denoted by ovals, and it is assumed that the core name is ``x''.  The file names that
result are shown without the .xml file extension.  Several of the programs take solvent parameters
from the input file.  Although those dependencies are not shown in the diagram, if a solvent parameter
changes, the programs taking it as an input are rerun upon rerunning {\it bd\_top}.

\begin{tikzpicture}
  \matrix (m)[matrix of nodes, column  sep=1.5cm,row  sep=8mm, align=center, nodes={rectangle,draw, anchor=center} ]{
  |[cloud]| {Atoms} & |[block]| {surface\_spheres} & |[cloud]| {x\_surface}\\
  |[cloud]|{Reaction File} & |[block]|{make\_surface\_sphere\_list} & |[cloud]| {x\_surface\_atoms} \\
  |[cloud]|{APBS Grids} & |[block]|{Largest} \\
  |[block]|{Smallest} & |[block]|{mpole\_grid\_fit} & |[cloud]|{x\_mpole}\\
  |[cloud]|{x\_surface} & |[block]|{inside\_points} & |[cloud]|{x\_born}\\
  |[cloud]|{Atoms} & |[cloud]|{x\_inside} & |[block]|{born\_integral}\\
  |[block]|{test\_charges}  & |[block]|{hydro\_params} & |[cloud]|{x\_hydro\_params}\\
  |[cloud]|{x\_charges} & |[block]|{compute\_charges\_squared} & |[cloud]|{x\_charges\_squared}\\
  |[block]|{lumped\_charges} & |[cloud]|{x\_cheby} & |[block]|{lumped\_charges}\\
  & & |[cloud]|{x\_squared\_cheby} \\
};
\path [>=latex,->] (m-1-1) edge (m-1-2);
\path [>=latex,->] (m-1-2) edge (m-1-3);
\path [>=latex,->] (m-2-1) edge (m-2-2);
\path [>=latex,->] (m-1-1) edge (m-2-2);
\path [>=latex,->] (m-1-3) edge (m-2-2);
\path [>=latex,->] (m-2-2) edge (m-2-3);
\path [>=latex,->] (m-3-1) edge (m-3-2);
\path [>=latex,->] (m-4-2) edge (m-4-3);
\path [>=latex,->] (m-3-2) edge (m-4-2);
\path [>=latex,->] (m-3-1) edge (m-4-1);
\path [>=latex,->] (m-4-1) edge (m-5-2);
\path [>=latex,->] (m-5-1) edge (m-5-2);
\path [>=latex,->] (m-6-1) edge (m-5-2);
\path [>=latex,->] (m-5-2) edge (m-6-2);
\path [>=latex,->] (m-6-2) edge (m-6-3);
\path [>=latex,->] (m-6-3) edge (m-5-3);
\path [>=latex,->] (m-6-2) edge (m-7-2);
\path [>=latex,->] (m-7-2) edge (m-7-3);
\path [>=latex,->] (m-7-1) edge (m-8-1);
\path [>=latex,->] (m-6-1) edge (m-7-1);
\path [>=latex,->] (m-8-1) edge (m-8-2);
\path [>=latex,->] (m-8-2) edge (m-8-3);
\path [>=latex,->] (m-8-1) edge (m-9-1);
\path [>=latex,->] (m-9-1) edge (m-9-2);
\path [>=latex,->] (m-8-3) edge (m-9-3);
\path [>=latex,->] (m-9-3) edge (m-10-3);
\end{tikzpicture}

\subsection{For Each Chain}
Here, we assume that the name of the chain is ``x''.

\begin{tikzpicture}
  \matrix (m)[matrix of nodes, column  sep=1.5cm,row  sep=8mm, align=center, nodes={rectangle,draw, anchor=center} ]{
|[cloud]|{Beads} & |[cloud]|{Parameters} & |[cloud]|{Connectivity} \\
& |[block]|{coffdrop\_chain} & |[cloud]|{x\_chain} \\
};

\path [>=latex,->] (m-1-1) edge (m-2-2);
\path [>=latex,->] (m-1-2) edge (m-2-2);
\path [>=latex,->] (m-1-3) edge (m-2-2);
\path [>=latex,->] (m-2-2) edge (m-2-3);
\end{tikzpicture}

\section{Final Notes}
When setting up a COFFDROP simulation with rigid cores, the APBS files
must be generated from the output of {\it generate\_coffdrop\_beads},
and the dielectric of the molecule must be the same as the dielectric
of the solvent.  This is because the dielectric boundary effects are
presumed to be accounted for in the COFFDROP potential.

There is also a program, {\bf scale\_interactions}, that
can be used to scale the nonbonded attractive COFFDROP interactions
per the examples in the original COFFDROP papers.

\end{document}
