#pragma once
#include <memory>
#include "../group/group_constraint_interface.hh"

namespace Browndye{

//class Group_Constraint_Interface;
//struct Constraints_Info;

namespace Chain_Constraint_Satisfier{
  template< class I>
  class Doer;
}
  
class Constrainer{
public:
  typedef Constraints_Info System;
  
  explicit Constrainer( const System& sys);
  void satisfy_no_init( System&, double tol);
  void satisfy( System&, double tol, size_t& nsteps);

  [[maybe_unused]] void satisfy_simple( System&, double tol, size_t& nsteps);
  double sys_rms_violation( const System&);
  
  ~Constrainer();
  Constrainer( const Constrainer&) = delete;
  Constrainer& operator=( const Constrainer&) = delete;
  Constrainer( Constrainer&&);
  Constrainer& operator=( Constrainer&&);
  
  
private:
  typedef Chain_Constraint_Satisfier::Doer< Group_Constraint_Interface> Doer;
  //std::unique_ptr< Doer> doer;
  Doer* doer = nullptr;
};

}
