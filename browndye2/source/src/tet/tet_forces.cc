#include "tet_forces.hh"
#include "centroid.hh"


namespace{
  using namespace Browndye;
  
  template< class T, class U>
  Vec3< decltype(T()/U())> ldl_solution( Mat3< U> A, Vec3< T> b);

}

namespace Browndye{

//#############################################################
// can optimize later if needed
V4< F3> tet_vertex_forces( const V4< Pos>& rs_in, F3 F, T3 T){

  constexpr Atom_Index a1(1), a4( 4);

  auto com = centroid( rs_in);
  V4< Pos> rs;
  for( auto i: range(a4)){
    rs[i] = rs_in[i] - com;
  }
  Array< Mat3< Length>, 4> Bts;
  Length zL{0.0};
  for( auto i: range(a4)){
    auto& Bt = Bts[i/a1];
    auto& r = rs[i];
    Bt[0][0] = zL;
    Bt[0][1] =  r[2];
    Bt[0][2] = -r[1];
    Bt[1][0] = -r[2];
    Bt[1][1] = zL;
    Bt[1][2] =  r[0];
    Bt[2][0] =  r[1];
    Bt[2][1] = -r[0];
    Bt[2][2] = zL;
  }

  Mat3< Length> sumBt( Zeroi{});
  for( auto k: range( 4)){
    sumBt += Bts[k];
  }
  
  auto sumB = transpose( sumBt);

  constexpr double frac = 1.0/4;
  auto& z2 = F;
  auto z3 = T - frac*(sumB*z2);
  F3 y2 = -frac*z2;

  auto bb = [&]( size_t i) -> auto{
    return transpose(Bts[i])*Bts[i];
  };
  
  Mat3< Length2> bbsum( Zeroi{});
  for( auto k: range( 4)){
    bbsum += bb(k);
  }
  
  auto D33 = frac*(sumB*sumBt) - bbsum;
  auto y3 = ldl_solution( D33, z3);
  
  auto& mu = y3;
  auto lambda = y2 - frac*(sumBt*mu);
  
  V4< F3> fs;
  for( auto i: range( a4)){
    fs[i] = -lambda - Bts[i/a1]*mu;
  }
  return fs;
} 
}


//################################################################
namespace {

template< class T, class U>
Vec3< decltype(T()/U())> ldl_solution( Mat3< U> A, Vec3< T> b){
  
  Vec3< U> D{};
  Mat3< double> L;

  for( auto i: range(3)){
    L[i][i] = 1.0;

    for( auto j: range(i)){
      auto sum = A[i][j];
      for( auto k: range( j))
        sum -= D[k]*L[i][k]*L[j][k];
      L[i][j] = sum/D[j];
    }

    auto sum = A[i][i];
    for( auto k: range(i)){
      auto ll = L[i][k];
      sum -= ll*ll*D[k];
    }
    D[i] = sum;
  }


  Vec3<T> z{};
  for( auto i: range(3)){
    auto sum = b[i];
    for( auto k: range(i))
      sum -= L[i][k]*z[k];
    z[i] = sum/L[i][i];
  }

  typedef decltype(T()/U()) R;

  Vec3< R> y{};
  for( auto i: range(3))
    y[i] = z[i]/D[i];

  Vec3< R> x{};
  for( int i = 2; i >=0; i--){
    auto sum = y[i];
    for( int k = 2; k > i; k--)
      sum -= L[k][i]*x[k];
    x[i] = sum/L[i][i];
  }

  return x;
}


}

/*
// turn into unit test
int main(){
  Length L0{0.0}, L1{1.0}, Lo3{1.0/3.0};

  Array< Vec3< Length>,4> poses{{L0,L0,L0},{L1,L0,L0},{L0,L1,L0},{L0,L0,L1}};
  Vec3< Length> com{ Lo3, Lo3, Lo3};

  for( auto i: range(4))
    poses[i] -= com;

  Vec3< Force> F;
  F[0] = Force( 1.1);
  F[1] = Force( 2.3);
  F[2] = Force( -4.5);

  Vec3< Torque> T;
  T[0] = Torque( -9.1);
  T[1] = Torque( 8.3);
  T[2] = Torque( -7.5);
  
  auto fs = tet_forces( poses, F, T);

  for( auto i: range(4))
    out << fs[i][0] << " " << fs[i][1] << " " << fs[i][2] << "\n";

  auto Ft = fs[0] + fs[1] + fs[2] + fs[3];
  out << "total F\n";
  out << Ft[0] << " " << Ft[1] << " " << Ft[2] << "\n";

  auto Tt = 
    cross( poses[0], fs[0]) + cross( poses[1], fs[1]) + 
    cross( poses[2], fs[2]) + cross( poses[3], fs[3]);

  out << "total T\n";
  out << Tt[0] << " " << Tt[1] << " " << Tt[2] << "\n";

}
*/
