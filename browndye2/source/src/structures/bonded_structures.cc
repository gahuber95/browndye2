#include "../xml/node_info.hh"
#include "bonded_structures.hh"
#include "../forces/other/cd_bonded_parameter_info.hh"
#include "../forces/other/mm_bonded_parameter_info.hh"

namespace Browndye{

namespace PP = JAM_XML_Pull_Parser;

using std::string;

template< class Type_Index>
void insert_name( std::map< string, Type_Index>& dict,
                 const string& name){
 
  auto nt = Type_Index( dict.size());
  auto res = dict.insert( std::make_pair( name, nt));
  if (!res.second)
    error( __FILE__, __LINE__, "duplicate structure name", name);    
}

// constructor
MM::Bond_Type::Bond_Type( MM_Bonded_Parameter_Info& bpinfo, PP::Node_Ptr node){
  
  node->complete();
  auto krr = checked_value_from_node< Spring_Constant>( node, "kr");
  kr = bpinfo.converted_energy( krr);
  req = checked_value_from_node< Length>( node, "req");
  auto name = checked_value_from_node< string>( node, "name");
  insert_name( bpinfo.bond_dict, name);
}

// constructor
MM::Angle_Type::Angle_Type( MM_Bonded_Parameter_Info& bpinfo, PP::Node_Ptr node){
  
  node->complete();
  auto kthr = checked_value_from_node< Energy>( node, "kth");
  kth = bpinfo.converted_energy( kthr);
  theq = checked_value_from_node< double>( node, "theq");
  auto name = checked_value_from_node< string>( node, "name");
  insert_name( bpinfo.angle_dict, name);
}

// constructor
MM::Dihedral_Type::Dihedral_Type( MM_Bonded_Parameter_Info& bpinfo,
                             PP::Node_Ptr node){
  
  node->complete();
  auto mnodes = node->children_of_tag( "mode");
  nc = mnodes.size();
  if (nc > maxnc)
    node->perror( "Dihedral_Type: too many modes in dihedral", nc);
  
  size_t k = 0;
  for( auto mnode: mnodes){
    ns[k] = checked_value_from_node< size_t>( mnode, "n");
    gammas[k] = checked_value_from_node< double>( mnode, "gamma");
    auto vr = checked_value_from_node< Energy>( mnode, "V");
    Vs[k] = bpinfo.converted_energy( vr);
    ++k;
  }
  
  auto name = checked_value_from_node< string>( node, "name");
  index = Dihedral_Type_Index( stoi( name));
  insert_name( bpinfo.dihedral_dict, name);
}

//###########################################################################
Vector< Energy> potentials_of( const Parameter_Info& pinfo, PP::Node_Ptr node){
  
  auto rpotentials = checked_vector_from_node< Energy>( node, "data");
  return pinfo.converted_energy( rpotentials); 
}

// constructor
CD::Bond_Type::Bond_Type( CD_Bonded_Parameter_Info& bpinfo, PP::Node_Ptr node){
  
  node->complete();
  auto potentials = potentials_of( bpinfo, node);  
  auto& dists = bpinfo.distances;
  auto x0 = dists[0];
  auto xf = dists[1];
  Vs = std::make_unique< Even_Spline< Length, Energy, false> >( x0, xf, potentials);
  
  auto istr = checked_value_from_node< string>( node, "index");
  insert_name( bpinfo.bond_dict, istr);
}

// constructor
CD::Angle_Type::Angle_Type( CD_Bonded_Parameter_Info& bpinfo, PP::Node_Ptr node){
  
  node->complete();
  auto potentials = potentials_of( bpinfo, node);  
  auto& bavs = bpinfo.bond_angle_values;
  auto th0 = bavs[0];
  auto thf = bavs[1];
  Vs = std::make_unique< Even_Spline< double, Energy, false> >( th0, thf, potentials);
  
  auto istr = checked_value_from_node< string>( node, "index");
  insert_name( bpinfo.angle_dict, istr);
}

// constructor
CD::Dihedral_Type::Dihedral_Type( CD_Bonded_Parameter_Info& bpinfo, PP::Node_Ptr node){
  
  node->complete();
  auto potentials = potentials_of( bpinfo, node);  
  auto& davs = bpinfo.dihedral_angle_values;
  auto phi0 = davs[0];
  auto phif = davs[1];
  Vs = std::make_unique< Even_Spline< double, Energy, false> >( phi0, phif, potentials);
  
  auto istr = checked_value_from_node< string>( node, "index");
  index = Dihedral_Type_Index( stoi( istr));
  insert_name( bpinfo.dihedral_dict, istr);
}
}
