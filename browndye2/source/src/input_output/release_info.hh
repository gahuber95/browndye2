#pragma once

#include <iostream>

namespace Browndye{

inline
void print_release_info(){
  std::cout << "BrownDye 2.0: Version of 29 Dec 2023\n";
}

}

