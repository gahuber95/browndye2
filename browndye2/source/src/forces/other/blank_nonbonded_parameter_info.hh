#pragma once

#include "nonbonded_parameter_info.hh"

namespace Browndye{

  class Blank_Nonbonded_Parameter_Info: public Nonbonded_Parameter_Info{
    void establish_interaction_radii( Vector< Atom, Atom_Index>&) const override;
  };

  
}

