#pragma once

#include "../lib/vector.hh"
#include "../core_and_chain/molcore/core_state.hh"
#include "../core_and_chain/chain/chain_state_atom.hh"
#include "../tet/tet.hh"

namespace Browndye{

struct Saved_Group_State{
  bool has_state = false; // debug?
  Vector< Chain_State, Chain_Index> chain_states;
  Vector< Tet< Group_State>::Small_Tet_With_Forces, Tet_Index> tet_states;
  Vector< Small_Core, Core_Index> core_states;
};

}

