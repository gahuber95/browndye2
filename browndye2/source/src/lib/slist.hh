#pragma once

#include <variant>
#include <memory>
#include <optional>
#include <forward_list>
#include "vector.hh"
#include "array.hh"
#include "combinators.hh"

// come up with more dest_ functions - those that destroy the contents
// of the argument

namespace Browndye{

//#######################################
struct Empty_SList{};

//#######################################
template< class T>
struct Nonempty_SList_Innards{
  
  // SList, std::variant< Empty_SList, Nonempty_SList<T> >
  typedef std::variant< Empty_SList, std::shared_ptr< Nonempty_SList_Innards<T> > > Tail_t;
  
  template< class U>
  Nonempty_SList_Innards( U&& _head, Tail_t _tail);
  
  T head;
  Tail_t tail;
};

//###########################################
// constructor
template< class T> template< class U>
inline
Nonempty_SList_Innards< T>::Nonempty_SList_Innards( U&& _head, Tail_t _tail):
  head( std::forward<U>( _head)),
  tail( _tail){}

//#######################################
template< class T>
using Nonempty_SList = std::shared_ptr< Nonempty_SList_Innards<T> >;

//#######################################
template< class T>
using SList = std::variant< Empty_SList, Nonempty_SList< T> >;

//#######################################
template< class T>
const T& head( SList<T> slist){
  return std::get< Nonempty_SList< T> >( slist)->head;
}

//##########################################
// moves the innards of the head
template< class T>
T dest_head( SList<T> slist){
  return std::move( std::get< Nonempty_SList< T> >( slist)->head);
}

//#######################################
template< class T>
SList<T> tail( SList< T> slist){
  return std::get< Nonempty_SList< T> >( slist)->tail;
}

//#######################################
template< class T, class U>
SList< U> appended( T&& t, SList< U> slist){
  return Nonempty_SList< U>( std::make_shared< Nonempty_SList_Innards< U> >( std::forward<T>(t), slist));
}

//#######################################
template< class T>
bool is_empty( SList<T> slist){
  return std::holds_alternative< Empty_SList>( slist);
}

//#######################################
template< class T>
SList<T> slist(){
  return SList<T>( Empty_SList());
}

//#######################################
template< class T>
auto slist( T&& t){
  typedef std::remove_reference_t< T> RNT;
  typedef std::remove_const_t< RNT> RT;
  return SList< RT>( std::make_shared< Nonempty_SList_Innards< RT> >( std::forward<T>(t), SList<RT>()));
}

//#######################################
template< class T0, class... Tr>
auto slist( T0&& t0, Tr&&... tr){
  
  return appended( std::forward<T0>(t0), slist( std::forward<T0>(tr)...)); 
}

//#######################################
template< class T, class F>
auto rev_mapped( F&& f, SList< T> slist){
  
  typedef decltype( f( head( slist))) FT;
  
  SList< FT> result;
  auto remaining = slist;
  while( !is_empty( remaining)){
    result = appended( f( head( remaining)), result);
    remaining = tail( remaining);
  }
  return result;
}

//#######################################
template< class T>
SList<T> reversed( SList< T> slist){
  
  SList< T> result;
  auto remaining = slist;
  while( !is_empty( remaining)){
    result = appended( head( remaining), result);
    remaining = tail( remaining);
  }
  return result;
}

//#######################################
// moves from and destroys contents of argument
template< class T>
SList<T> dest_reversed( SList< T> slist){
  SList< T> result;
  auto remaining = slist;
  while( !is_empty( remaining)){
    result = appended( std::move( dest_head( remaining)),
                                 result);
    remaining = tail( remaining);
  }
  return result;
}

//#######################################
template< class T, class F>
auto mapped( F&& f, SList< T> slist){
  return reversed( rev_mapped( f, slist));
}

//#######################################
template< class T, class F>
void iterate( F f, SList< T> slist){
  
  auto fif = [&]( SList<T> slist){
    return is_empty( slist);
  };
  
  auto felse = [&]( SList<T> slist){
    f( head( slist));
    return tail( slist);
  };
  
  primrec0( fif, felse, slist);  
}

//#######################################
template< class T, class F, class G>
auto folded( F&& f, SList<T> lst, G&& g){
  
  auto res = std::forward<G>(g);
  auto remaining = lst;
  while( !is_empty( remaining)){
    res = f( res, head( remaining));
    remaining = tail( remaining);
  }
  
  return res;
}

//#######################################
template< class T>
SList< T> concatenated( SList<T> a, SList<T> b){
  
  auto ra = reversed( a);
  
  auto app = []( SList<T> c, const T& t){
    return appended( t, c);
  };
  
  return folded( app, ra, b);
}

//#######################################
template< class T, class Idx>
SList<T> slist_from_vector( const Vector<T,Idx>& v){
  
  auto rappended = []( SList<T> lst, const T& t){
    return appended( t, lst);
  };
  
  return reversed( v.folded_left( rappended, slist<T>()));
}

//#######################################
template< class T, class Idx>
SList<T> slist_from_vector( Vector<T,Idx>&& v){
    
  SList<T> res;
  for( auto& t: v)
    res = appended( std::move(t), res);
  
  v.clear();
  return dest_reversed( res);
}

//#######################################
template< class T, class Idx = size_t>
Vector< T, Idx> vector_from_slist( SList<T> lst){
  Vector< T, Idx> res;
  for( auto& t: lst)
    res.push_back( t);
  return res;
}

template< class T>
SList< T> end( SList<T> lst){
  return Empty_SList();
}

template< class T>
SList<T> begin( SList< T> lst){
  return lst;
}

//#######################################
template< class T>
size_t size( SList<T> lst){
  
  size_t n = 0;
  for( auto itr = begin( lst); !std::holds_alternative< Empty_SList>( itr); ++itr){
    ++n;
  }
  return n;
}


//#######################################
template< class T, size_t n>
Array< T,n> array_from_slist( SList<T> lst){
  
  if constexpr (debug){
    auto m = size( lst);
    if (m != n)
      error( "slist is wrong size ", m,  " for array size ", n);
  }
  
  Array< T,n> res;
  auto remaining = lst;
  for( auto k: range(n)){
    res[k] = head( remaining);
    remaining = tail( remaining);
  }
  return res;
}

//#######################################
template< class T, size_t n>
SList<T> slist_from_array( const Array<T,n>& ar){
  
  SList<T> res;
  for( int k = n-1; k >= 0; k--)
    res = appended( ar[k], res);
  return res;
}

//#######################################
template< class F, class T>
std::optional< std::reference_wrapper< const T> >
first_of( F&& f, SList<T> lst){
  
  auto rem = lst;
  while( !is_empty( rem)){
    const T& t = head( rem);
    if (f(t)){
      return std::optional(std::cref(t));
    }
    rem = tail( rem);
  }
  
  return std::optional< std::reference_wrapper< const T> >();
}
//#######################################
template< class T>
bool has_member( SList<T> lst, const T& t){
  
  for( auto& tt: lst){
    if (tt == t)
      return true;
  }
  return false;
}


//#######################################
// genericize to arbitrary n (fun puzzle)
template< class T0, class T1>
SList< std::tuple< T0, T1> > zipped( SList<T0> lst0, SList<T1> lst1){
  
  auto lerr = [&](){
    error( "zipped: mismatched sizes ", size(lst0), size( lst1));
  };
  
  SList< std::tuple< T0,T1> > res;
  auto rem0 = lst0;
  auto rem1 = lst1;
  while( !is_empty(rem0)){
    if (is_empty(rem1)){
      lerr();
    }
    auto tt = std::make_tuple( head(rem0), head(rem1));
    res = appended( tt, res);
    rem0 = tail( rem0);
    rem1 = tail( rem1);
  }
  
  if (!is_empty(rem1)){
    lerr();
  }
  
  return reversed( res);
}

//###########################################
template< class T0, class T1>
std::tuple< SList< T0>, SList< T1> > unzipped( SList< std::tuple< T0,T1> > lst){
  
  auto lst0 = mapped( [](auto tt){return std::get<0>(tt);}, lst);
  auto lst1 = mapped( [](auto tt){return std::get<1>(tt);}, lst);
  return std::make_tuple( lst0, lst1);
}

//#######################################
template< class T, class F>
SList< T> filtered( F&& f, SList< T> lst){
  
  SList< T> res;
  auto rem = lst;
  while( !is_empty( rem)){
    auto& x = head( rem);
    if (f(x))
      res = appended( x, res);
    rem = tail( rem);
  }
  return reversed( res);
}

//#######################################
template< class T>
std::tuple< SList<T>, SList<T> > split( SList< T> lst){
  
  int n = size( lst);
  int hn = n/2;

  SList< T> rspl0, spl1 = lst;
  for( int i = 0; i < hn; i++){
    rspl0 = appended( head( spl1), rspl0);
    spl1 = tail( spl1);
  }

  return std::make_tuple( reversed( rspl0), spl1);

}

//#######################################
template< class T, class F = std::less<T> >
SList< T> merged( SList< T> lst0, SList<T> lst1, F&& comp = std::less<T>()){
  
  auto rem0 = lst0;
  auto rem1 = lst1;
  SList< T> res;
  while( !(is_empty( rem0) || is_empty( rem1))){
    auto t0 = head( rem0);
    auto t1 = head( rem1);
    if (comp( t0,t1)){
      res = appended( t0, res);
      rem0 = tail( rem0);
    }
    else{
      res = appended( t1, res);
      rem1 = tail( rem1);
    }
  }
  
  if (!is_empty(rem0))
    res = concatenated( reversed( rem0), res);
  else if (!is_empty(rem1))
    res = concatenated( reversed( rem1), res);
  
  return reversed( res);
}

//#######################################
template< class T, class F = std::less<T> >
SList< T> sorted( SList< T> lst, F&& comp = std::less<T>()){
  
  if (is_empty( lst) || is_empty( tail( lst)))
    return lst;
  else{
     auto [lst0,lst1] = split( lst);
     auto slst0 = sorted( lst0, comp);
     auto slst1 = sorted( lst1, comp);
     return merged( slst0, slst1, comp);
  }
}

//#######################################
// assume is sorted
template< class T, class F = std::equal_to<T>>
SList< T> with_no_dups( SList< T> lst, F&& eq = std::equal_to<T>()){
    
  auto f = [&]( SList< T> res, T t) -> SList< T>{
    if (is_empty( res)){
      return slist( t);
    }
    else{
      if (eq( t, head(res))){
        return res;
      }
      else{
        return appended( t, res);
      }
    }
  };  
  
  return reversed( folded( f, lst, slist<T>())); 
}

//#######################################
template< class T, class F>
bool contains( F&& pred, SList< T> lst){
  
  auto fif = [&]( auto lst){
    if (is_empty( lst)){
      return std::make_tuple( true, false);
    }
    else{
      return std::make_tuple( pred( head(lst)), true);
    }
  };
  
  auto felse = [&]( SList<T> lst, bool found){
    return tail( lst);
  };
  
  return primrec( fif, felse, lst);
}

//#######################################
template< class T>
bool contains_object( SList< T> lst, const T& t0){
  
  auto has = [&]( const T& t1){
    return t1 == t0;
  };
  
  return contains( has, lst);
}

//###########################################
inline
SList< int> iota( int n){
  
  auto fif = []( int i, SList<int> lst){
    return i == 0;
  };
  
  auto felse = []( int i, SList<int> lst){
    auto im1 = i-1;
    return std::make_tuple( im1, appended( im1, lst));
  };
  
  return std::get<1>( primrec0( fif, felse, n, SList<int>()));
}

//#########################################
template< class F, class T>
auto accumulated( F&& f, SList<T> lst){
  
  auto facc = [&]( SList<T> res, const T& t){
    if (is_empty( res))
      return slist( t);
    else
      return appended( f( head(res), t), res);
  };
  
  return reversed( folded( facc, lst, slist<T>()));
}

//#######################################
// Stuff necessary for for-range-loop
template< class T>
void operator++( SList<T>& lst){
  lst = std::get< Nonempty_SList< T> >( lst)->tail;
}

template< class T>
const T& operator*( SList<T> lst){
  return std::get< Nonempty_SList< T> >( lst)->head;
}

inline
bool operator!=( Empty_SList, Empty_SList){
  return false;
}

template< class T>
bool operator!=( Empty_SList, Nonempty_SList< T>){
  return true;
}

template< class T>
bool operator!=( Nonempty_SList< T>, Empty_SList){
  return true;
}

//##########################################
  // Set operations
   
template< class T, class F>
SList< T> gen_set_operator( F f, SList< T> lst0, SList< T> lst1){
  auto slst0 = sorted( lst0);
  auto slst1 = sorted( lst1);
  std::forward_list< T> fres;
  f( begin( slst0), end( slst0), begin( slst1), end( slst1),
     front_inserter( fres));
  
  SList< T> res;
  for( auto& t: fres)
    res = appended( t, res);
  return res;
}
  
template< class T>
SList< T> set_union( SList< T> lst0, SList< T> lst1){
  
  auto f = []( auto b0, auto e0, auto b1, auto e1, auto out){
    std::set_union( b0,e0,b1,e1,out);
  };
  return gen_set_operator( f, lst0, lst1);
}
  
template< class T>
SList< T> set_intersection( SList< T> lst0, SList< T> lst1){
  
  auto f = []( auto b0, auto e0, auto b1, auto e1, auto out){
    std::set_intersection( b0,e0,b1,e1,out);
  };
  return gen_set_operator( f, lst0, lst1);
}
  
template< class T>
SList< T> set_difference( SList< T> lst0, SList< T> lst1){
  
  auto f = []( auto b0, auto e0, auto b1, auto e1, auto out){
    std::set_difference( b0,e0,b1,e1,out);
  };
  return gen_set_operator( f, lst0, lst1);
}
  
template< class T>
SList< T> set_symmetric_difference( SList< T> lst0, SList< T> lst1){
  
  auto f = []( auto b0, auto e0, auto b1, auto e1, auto out){
    std::set_symmetric_difference( b0,e0,b1,e1,out);
  };
  return gen_set_operator( f, lst0, lst1);
}

//#############################################
template< class T>
bool operator==( SList< T> a, SList< T> b){
  auto na = size( a);
  auto nb = size( b);
  if (na != nb)
    return false;
  
  auto ab = zipped( a,b);
  
  auto are_same = []( bool same, std::tuple< T,T> pair){
    return same && (std::get<0>( pair) == std::get<1>( pair));
  };
  
  return folded( are_same, ab, true);
}

}

//#############################################
namespace std{
  template< class T>
  struct iterator_traits< Browndye::SList<T> >{
    typedef int difference_type;
    typedef T value_type;
    typedef T* pointer;
    typedef T& reference;
    typedef std::forward_iterator_tag iterator_category;
  };
}
