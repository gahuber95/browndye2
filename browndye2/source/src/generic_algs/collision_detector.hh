#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Generic code for finding collisions between two rigid collections of spheres,
and for computing pairwise interactions among spheres within a certain
cut-off.


Interface class has following types and static functions:

typedef Container; // of spheres
typedef Refs; // references to spheres in Container
typedef Transform; // translation and rotation
typedef Index; // integer index to spheres
typedef Ref; // single reference to sphere
typedef Position; // position; equivalent to std::array< Length, 3>

void copy_references( const Container&, Refs&);
void copy_references( const Container&, const Refs&, Refs&);
Index size( const Container&);
Index size( const Container&, const Refs&);
Position position( const Container&, const Refs&, Index i);
Position transformed_position( const Pos&, const Transform&);
Length radius( const Container&, const Refs&, Index i);
void swap( const Container&, Refs&, Index i, Index j);

f has signature bool f( const Pos&). The references of all
spheres whose position gives "false" are placed in "refs0", and those
who give "true" are placed in "refs1". 

template< class F>
void partition_contents( const Container&, const Refs& refs, const F& f, 
                         Refs& refs0, Refs& refs1); 

void empty_container( const Container&, Refs&);

Ref reference( const Container&, const Refs&, Index i);

// called to tell all spheres that previous transforms are now forgotten.
void clear_transform( Container&, Refs&);

constexpr bool is_changeable; 

End of Interface description

// contains references to all spheres
template< class Interface>
class Structure{
public:
  typedef typename Interface::Container Container;
  typedef typename Interface::Ref Ref;
  
  Structure( Container& conr, Index max_n_per_cell);

  Ref nearest_object( const Pos& r);

  Distance_Finder object has overloaded 
  bool ()( const Container& container, const Refs& refs, Length r, 
           const Pos& pos) operator;
  returns "true" if any of the spheres in "refs" is within "r" of
  position "pos".

  template< class Distance_Finder>
  bool is_within( const Distance_Finder&, Length r, const Pos& pos);

  makes no attempt to resolve distances > max_distance; will
  simply return infinity
  Distance_Fun object has overloaded 
  double operator()( const Pos& pos), which returns
  the distance of position "pos" from the field.

  template< class Distance_Fun>
  Length distance_from_field( const Distance_Fun&, Length max_distance);

  void clear_transform();
  
  bool is_changeable;
};

class I has types Interface0 and Interface1 defined, which are 
the interface types, as described above, for Molecule 0 and Molecule 1.
The user-defined Local_Collision object has overloaded operator
bool operator()( const Interface0::Transform&, 
                 const Interface0::Container&,
                 const Interface0::Refs&,
                 Interface0::Ref&,

                 const Interface1::Transform&, 
                 const Interface1::Container&,
                 const Interface1::Refs&,
                 Interface1::Refs&,

                 Length& violation)
which returns "true" if any of the spheres in the inputs collide,
and also returns a reference to each of the two colliding spheres
and the extent of violation.

template< class I, class Local_Collision_Detector>
bool has_collision( 
                   const Local_Collision_Detector& cd,
                   
                   Structure< typename I0>& str0,
                   const typename I0::Transform& trans0,
                   typename I0::Ref& ref0,
                   
                   Structure< typename I1>& str1,
                   const typename I1::Transform& trans1,
                   typename I1::Ref& ref1,
                   Length& violation
                   );

Uses user-defined class Interaction_Computer to compute local 
interactions that are within the radius as defined by the interface
function.  Interaction_Computer has the operator

bool operator()( const Interface0::Transform&, 
                 const Interface0::Container&,
                 const Interface0::Refs&,
                 const Interface0::Ref&,

                 const Interface1::Transform&, 
                 const Interface1::Container&,
                 const Interface1::Refs&,
                 const Interface1::Ref&,

                 bool& has_collision,
                 Interface0:Ref& ref0, Interface1:Ref& ref1,
                 Length& violation)
and works very much like the Local_Collision_Detector above,
except that it can accumulate information from the pair-wise
interactions. 

template< class I, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Structure< typename I0>& str0,
                         const typename I0::Transform& trans0,
                         Structure< typename I1>& str1,
                         const typename I1::Transform& trans1,
                         bool& has_collision,
                         typename I0::Ref& ref0,
                         typename I1::Ref& ref1,
                         Length& violation
                         );

// Version for no collisions, just interactions

template< class I, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Structure< typename I0>& str0,
                         const typename I0::Transform& trans0,
                         Structure< typename I1>& str1,
                         const typename I1::Transform& trans1);

which requires

void operator()(
  const Interface0::Transform&, 
  const Interface0::Container&,
  const Interface0::Refs&,
  
  const Interface1::Transform&, 
  const Interface1::Container&,
  const Interface1::Refs&,
  )
*/

//#include <stdlib.h>
#include <memory>
#include <cfloat>
#include <cmath>
#include <algorithm>
#include <list>
#include <queue>

#include "../lib/circumspheres.hh"
#include "../lib/vector.hh"
#include "../lib/sq.hh"

//***********************************************************************
namespace Browndye::Collision_Detector{

  //constexpr unsigned int X{ 0};
  //constexpr unsigned int Y{ 1};
  //constexpr unsigned int Z{ 2};

//*************************************************************
template< class I>
class Partitioner{
public:
  typedef typename I::Position Position;
  typedef typename I::Length Length;
  
  Partitioner( const Position& _com, const Vec3< double>& _axis,
               Length _split){

    for( unsigned int k=0; k<3; k++){
      com[k] = _com[k];
      axis[k] = _axis[k];
    }
    split = _split;
  }

  bool operator()( const Position& r) const{
    Length proj = Length( 0.0);
    for( unsigned int k=0; k<3; k++){
      proj += (r[k] - com[k])*axis[k];
    }
    return proj > split;    
  }

  Position com;
  Vec3< double> axis;
  Length split;
};

//*************************************************************
template< class I, bool is_changeable>
class Changeable_Part;

template< class I>
class Changeable_Part< I, true>{
public:
  typedef typename I::Position Position;
  
  Changeable_Part() = default;

  void clear_transform_super(){
    if (is_transformed)
      is_transformed = false;
  }

  const Position& transformed_center([[maybe_unused]] const Position& center){
    return tcenter;
  }

  void transform_super( const typename I::Transform& tform,
                        const Position& center){
    
    if (!is_transformed){
      tcenter = I::transformed_position( center, tform);
      is_transformed = true;
    }
  }

  Changeable_Part copy() const{
   Changeable_Part res; 
   res.tcenter = tcenter;
   res.is_transformed = is_transformed;
   return res;
  }

  void copy_from( const Changeable_Part& other){
    tcenter = other.tcenter;
    is_transformed = other.is_transformed;
  }

  Position tcenter;
  bool is_transformed = false;
};

template< class I>
class Changeable_Part< I, false>{
public:
  typedef typename I::Position Pos;

  void clear_transform_super(){}
  
  void transform_super([[maybe_unused]] const typename I::Transform& tform,
                       [[maybe_unused]] const Pos& center){}

  const Pos& transformed_center( const Pos& center){
    return center;
  }

  Changeable_Part copy() const{
    Changeable_Part res;
    return res;
  }
  
  void copy_from([[maybe_unused]] const Changeable_Part& other){}
  
};

template< class I>
class Cell: public Changeable_Part< I, I::is_changeable>{
public:
  typedef typename I::Container Container;
  typedef typename I::Refs Refs;
  //typedef typename I::Ref Ref;
  typedef typename I::Index Index;
  typedef typename I::Position Position;
  typedef typename I::Length Length;
  typedef typename I::Transform Transform;
  static const bool is_changeable = I::is_changeable;
  typedef Changeable_Part< I, is_changeable> Super;

  Cell() = default;
  
  Cell( const Container& cont, const Cell& other){
    copy( cont, other);
  }
 
  Cell( const Cell&) = delete;
  Cell& operator=( const Cell&) = delete;  

  Cell( Cell&& other) noexcept {
    copy_from( other);
    movet( std::move( other));
  }

  Cell& operator=( Cell&& other) noexcept {
    copy_from( other);
    movet( std::move( other));
    return *this;
  }

  ~Cell() = default;
  
  void clear_transform( Container&);

  void load_objects( Container& conr, Index max_n){
    child0.release();
    child1.release();
    I::copy_references( conr, contents);
    split( conr, max_n);
  }

  void split( Container&, Index max_n);

  [[nodiscard]] bool is_bottom() const{
    return (!child0) && (!child1);
  }

  void transform( const Transform&);

  void shift( Position);
  void perm_transform( const Transform&);
  
  void movet( Cell&& other);
  Cell copy( const Container&) const;

    // data
  Position center;
  Length radius;
  Refs contents;
  std::unique_ptr< Cell< I> > child0, child1; 
};

template< class I>
void Cell<I>::shift( Position d){
  
  for( int k = 0; k < 3; k++)
    center[k] += d[k];
  
  if (child0)
    child0->shift( d);
  if (child1)
    child1->shift( d);
}

template< class I>
void Cell<I>::perm_transform( const Transform& tform){
  
  center = transformed_position( center, tform);
  
  if (child0)
    child0->perm_transform( tform);
  if (child1)
    child1->perm_transform( tform);
}


template< class I>
Cell<I> Cell<I>::copy( const Container& cont) const{
  
  Cell<I> res;
  res = static_cast< Super*>(this)->copy();
  
  res.center = center;
  res.radius = radius;
  I::copy_references( cont, contents, res.contents);

  if (child0){
    res.child0 = std::make_unique< Cell<I> >( cont, child0->copy()); 
  }
  
  if (child1){
    res.child1 = std::make_unique< Cell<I> >( cont, child1->copy());
  }
}


template< class I>
void Cell<I>::movet( Cell<I>&& other){
  
  child0 = std::move( other.child0);
  child1 = std::move( other.child1);
  contents = std::move( other.contents);
  center = other.center;
  radius = other.radius;
}

template< class I>
void Cell< I>::transform( const Transform& tform){
  if (is_changeable)
    Super::transform_super( tform, center);
}

template< class I>
void Cell< I>::clear_transform( Container& conr){

  if (is_changeable){
    Super::clear_transform_super();
    
    if (child0)
      child0->clear_transform( conr);

    if (child1)
      child1->clear_transform( conr);

    if (!child0 && !child1)
      I::clear_transform( conr, contents);
  }
  
}

  
//*************************************************************
template< class I0, class I1, class CD>
bool has_collision(
                   const CD& cd,
                   
                   Cell< I0>& cell0,
                   const typename I0::Transform& trans0,
                   typename I0::Container& conr0,
                   typename I0::Ref& ref0,
                   
                   Cell< I1>& cell1,
                   const typename I1::Transform& trans1,
                   typename I1::Container& conr1,
                   typename I1::Ref& ref1,
                   Length& viol
                   ){

  cell0.transform( trans0);
  cell1.transform( trans1);
  const Length dist = distance( 
                               cell0.transformed_center( cell0.center), 
                               cell1.transformed_center( cell1.center));

  if (std::isnan( dist/Length{1}))
    error( "nan encountered");

  bool res;
  if (dist > (cell0.radius + cell1.radius))
    res = false;
  else {
    if (!cell0.is_bottom() && !cell1.is_bottom()){
      if (has_collision<I0,I1,CD>( cd,
                            *(cell0.child0), trans0, conr0, ref0,
                            *(cell1.child0), trans1, conr1, ref1,
                            viol))
        res = true;
      else if (has_collision<I0,I1,CD>( cd,
                                    *(cell0.child0), trans0, conr0, ref0,
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else if (has_collision<I0,I1,CD>( cd,
                                    *(cell0.child1), trans0, conr0, ref0,
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else if (has_collision<I0,I1,CD>( cd,
                                    *(cell0.child1), trans0, conr0, ref0,
                                    *(cell1.child0), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else if (cell0.is_bottom() && !cell1.is_bottom()){
      if (has_collision<I0,I1,CD>( cd,
                               cell0, trans0, conr0, ref0, 
                               *(cell1.child0), trans1, conr1, ref1, 
                               viol))
        res = true;
      else if (has_collision<I0,I1,CD>( cd,
                                    cell0, trans0, conr0, ref0, 
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else if (cell1.is_bottom() && !cell0.is_bottom()){
      if (has_collision<I0,I1,CD>( cd,
                               *(cell0.child1), trans0, conr0, ref0, 
                               cell1, trans1, conr1, ref1, 
                               viol))
        res = true;
      else if (has_collision<I0,I1,CD>( cd,
                                    *(cell0.child0), trans0, conr0, ref0, 
                                    cell1, trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else { // both bottom

      res = cd( trans0, conr0, cell0.contents, ref0,   
                 trans1, conr1, cell1.contents, ref1, 
                 viol);

    }
  }
  return res;
}


//*************************************************************
template< class I0, class I1, class Interaction_Computer>
void compute_cell_interaction( 
                         Interaction_Computer& ic,
                         Cell< I0>& cell0,
                         const typename I0::Transform& trans0,
                         typename I0::Container& conr0,
                         Cell< I1>& cell1,
                         const typename I1::Transform& trans1,
                         typename I1::Container& conr1,
                         bool& has_collision,
                         typename I0::Ref& ref0,
                         typename I1::Ref& ref1,
                         Length& viol
                         ){

  cell0.transform( trans0);
  cell1.transform( trans1);

  const Length dist = distance( cell0.transformed_center( cell0.center), 
                                cell1.transformed_center( cell1.center));

  has_collision = false;
  if (dist <= (cell0.radius + cell1.radius)){
    if (!cell0.is_bottom() && !cell1.is_bottom()){
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child0), trans0, conr0, 
                              *(cell1.child0), trans1, conr1, 
                              has_collision, ref0, ref1, viol);
      
      if (!has_collision)
        compute_cell_interaction<I0,I1>( ic,
                                *(cell0.child0), trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
      else
        return;

      if (!has_collision)
        compute_cell_interaction<I0,I1>( ic,
                                *(cell0.child1), trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
      else
        return;
      
      if (!has_collision)
        compute_cell_interaction<I0,I1>( ic,
                                *(cell0.child1), trans0, conr0, 
                                *(cell1.child0), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }

    else if (cell0.is_bottom() && !cell1.is_bottom()){

      compute_cell_interaction<I0,I1>( ic,
                              cell0, trans0, conr0, 
                              *(cell1.child0), trans1, conr1, 
                              has_collision, ref0, ref1, viol);
      
      if (!has_collision)
        compute_cell_interaction<I0,I1>( ic,
                                cell0, trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }
    else if (cell1.is_bottom() && !cell0.is_bottom()){
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child1), trans0, conr0, 
                              cell1, trans1, conr1, 
                              has_collision, ref0, ref1, viol);

      if (!has_collision)
        compute_cell_interaction<I0,I1>( ic,
                                *(cell0.child0), trans0, conr0,
                                cell1, trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }
    else { // both bottom
      ic( trans0, conr0, cell0.contents,   
          trans1, conr1, cell1.contents, has_collision, ref0, ref1, viol);
    }
  }
}


//*************************************************************
template< class I0, class I1, class Interaction_Computer>
void compute_cell_interaction( 
                         Interaction_Computer& ic,
                         Cell< I0>& cell0,
                         const typename I0::Transform& trans0,
                         typename I0::Container& conr0,
                         Cell< I1>& cell1,
                         const typename I1::Transform& trans1,
                         typename I1::Container& conr1
                         ){

  cell0.transform( trans0);
  cell1.transform( trans1);

  const Length dist = distance( cell0.transformed_center( cell0.center), 
                                cell1.transformed_center( cell1.center));

  if (dist <= (cell0.radius + cell1.radius)){
    if (!cell0.is_bottom() && !cell1.is_bottom()){
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child0), trans0, conr0, 
                              *(cell1.child0), trans1, conr1);
      
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child0), trans0, conr0, 
                              *(cell1.child1), trans1, conr1);
    
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child1), trans0, conr0, 
                              *(cell1.child1), trans1, conr1);
    
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child1), trans0, conr0, 
                              *(cell1.child0), trans1, conr1);
    }
    else if (cell0.is_bottom() && !cell1.is_bottom()){
      compute_cell_interaction<I0,I1>( ic,
                              cell0, trans0, conr0, 
                              *(cell1.child0), trans1, conr1);
      
      compute_cell_interaction<I0,I1>( ic,
                              cell0, trans0, conr0, 
                              *(cell1.child1), trans1, conr1);
    }
    else if (cell1.is_bottom() && !cell0.is_bottom()){
      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child1), trans0, conr0, 
                              cell1, trans1, conr1);

      compute_cell_interaction<I0,I1>( ic,
                              *(cell0.child0), trans0, conr0,
                              cell1, trans1, conr1);
    }
    else { // both bottom
      ic( trans0, conr0, cell0.contents, cell0.center, cell0.radius,
          trans1, conr1, cell1.contents, cell1.center, cell1.radius);
    }
  }
}

//*************************************************************
template< class I, class IC, class ICB>
void compute_cell_self_interaction( IC&& ic, ICB&& icb, Cell< I>& cell,
                         const typename I::Transform& trans,
                         typename I::Container& conr
                         ){

  cell.transform( trans);

  if (cell.is_bottom()){
    icb( trans, conr, cell.contents);  
  }
  else{
    auto& child0 = *(cell.child0);
    auto& child1 = *(cell.child1);
    
    compute_cell_interaction< I, I>( ic, child0, trans, conr, child1, trans, conr);
    compute_cell_self_interaction< I>( ic, icb, child0, trans, conr);
    compute_cell_self_interaction< I>( ic, icb, child1, trans, conr);
  }
}
//*************************************************************
template< class Container, class Refs>
struct CPoints{
  Container& container;
  Refs& refs;
  
  CPoints( Container& _container, Refs& _refs):
    container( _container), refs( _refs){}
};

//*************************************************************
template< class I>
class Circumspheres_I{
public:
  typedef typename I::Container Container;
  typedef typename I::Index Index;
  typedef typename I::Position Position;
  typedef typename I::Length Length;

  typedef typename I::Refs Point_Refs;
  typedef CPoints< Container, Point_Refs> Points;
  
  static
  void initialize( const Points& points, Point_Refs& refs){
    I::copy_references( points.container,
                                points.refs, refs);
  }

  static
  Index size( const Points& points){
    return I::size( points.container, points.refs);
  }

  static
  void get_position( const Points& pts, const Point_Refs& refs,
                     Index i, Position& pos){
    pos = I::position( pts.container, refs, i);
  }

  static
  Length radius( const Points& pts, const Point_Refs& refs, Index i){
    return I::radius( pts.container, refs, i);
  }

 
  static
  void swap( Points& pts, Point_Refs& refs, Index i, Index j){
    I::swap( pts.container, refs, i, j);
  }
  
  template< typename ... Args>
  static
  void error( Args ... args){
    Browndye::error( args...);
  }

};


//*************************************************************
template< class I>
void Cell< I>::split( Container& conr, Index max_n){

  typedef Circumspheres_I< I> CI;
  CPoints< 
    typename I::Container,
    typename I::Refs> cpoints( conr, contents);

  Length r{};
  
  Circumspheres::
    get_smallest_enclosing_sphere_of_spheres< CI>( cpoints, center, r);

  radius = 1.1*r;

  auto n = I::size( conr, contents);
  
  if (n > max_n){

    Position com;
    com[0] = com[1] = com[2] = Length( 0.0);

    for( auto i: range( n)){
    //for( auto i = Index(0); i<n; i++){
      auto pos = I::position( conr, contents, i);
      com[0] += pos[0];
      com[1] += pos[1];
      com[2] += pos[2];
    }  

    Index s1(1);
    com[0] /= (double)(n/s1);
    com[1] /= (double)(n/s1);
    com[2] /= (double)(n/s1);
    
    const Length lnan = Length( NAN);
    Position diff;   //{{ lnan, lnan, lnan}};
    diff[0] = lnan;
    diff[1] = lnan;
    diff[2] = lnan;
    Vec3< double> axis;
    {
      Length dmax = Length( 0.0);
      for( auto i = Index(0); i<n; i++){
        auto pos = I::position( conr, contents, i);
        Length lr = I::radius( conr, contents, i);
        //Vec3< Length> cp;
        auto cp = com - pos;
        //for( int k = 0; k < 3; k++)
          //cp[k] = com[k] - pos[k];
        Length d = sqrt( sq(cp[0]) + sq(cp[1]) + sq(cp[2])) + lr;
        //distance( com, pos) + r;
        if (d > dmax){
          dmax = d;
          for( int k = 0; k < 3; k++)
            diff[k] = pos[k] - com[k];
        }
      }  

      auto normd = sqrt( sq( diff[0]) + sq( diff[1]) + sq( diff[2]));
      for( int k = 0; k < 3; k++)
        axis[k] =  diff[k]/normd;  //normed( diff);
    }
   
    Vector< Length, typename I::Index> projs( n);
    for( auto i = Index(0); i<n; i++){
      Position pos = I::position( conr, contents, i);

      Length proj = Length( 0.0);
      for( unsigned int k=0; k<3; k++){
        proj += (pos[k] - com[k])*axis[k];
      }
      projs[i] = proj;
    }

    std::sort( projs.begin(), projs.end());
    
    auto hn = n/2;
    Length split = 
      (n % 2 == 0) ? 0.5*( projs[ hn + (Index(0) - Index(1))] + projs[ hn]) : projs[ hn];
    

    Partitioner<I> pr( com, axis, split);
    
    child0 = std::make_unique< Cell<I> >();
    child1 = std::make_unique< Cell<I> >();

    I::partition_contents( conr, contents, pr,
                                   child0->contents, child1->contents);
      
    child0->split( conr, max_n);
    child1->split( conr, max_n);

    I::empty_container( conr, contents);
  }  
}

//*************************************************************
template< class I>
class Structure{

public:
  typedef typename I::Container Container;
  typedef typename I::Ref Ref;
  typedef typename I::Transform Transform;
  typedef typename I::Index Index;
  typedef I Col_I;
  typedef typename I::Position Pos;
  
  Structure() = delete;
  Structure( const Structure&);
  Structure& operator=( const Structure&) = default;
  Structure( Structure&& other) = default;
  Structure& operator=( Structure&& other) = default;

  Structure( Container& conr, Index max_n_per_cell);

  //void set_max_number_per_cell( Index n);

  [[maybe_unused]] Ref nearest_object( const Pos& r);

  template< class Distance_Finder>
  bool is_within( const Distance_Finder&,  
                  Length r, const Pos& pos) const;

  template< class Distance_Fun>
  [[maybe_unused]] Length distance_from_field( const Distance_Fun&, Length max_distance);

  [[nodiscard]] Length radius() const{
    return cell.radius;
  }

  Pos position() const{
    return cell.center;
  }
  
  // assume that underlying particles are already transformed
  void shift( Pos);
  void transform( const Transform&);

  //Cell<I>& cell_of();
  //const Cell<I>& cell_of() const;

private:
  void load_objects();

  template< class I0, class I1, class CD>
  friend
  bool has_collision( 
                     const CD& cd,
                     
                     Structure< I0>& str0,
                     const typename I0::Transform& trans0,
                     typename I0::Ref& ref0,
                     
                     Structure< I1>& str1,
                     const typename I1::Transform& trans1,
                     typename I1::Ref& ref1,
                     Length& violation             
                     );
  

  template< class I0, class I1, class Interaction_Computer>
  friend
  void compute_interaction( 
                           Interaction_Computer& ic,
                           Structure< I0>& str0,
                           const typename I0::Transform& trans0,
                           Structure< I1>& str1,
                           const typename I1::Transform& trans1,
                           bool& has_collision,
                           typename I0::Ref& ref0,
                           typename I1::Ref& ref1,
                           Length& viol
                            );
  
  template< class I0, class I1, class Interaction_Computer>
  friend
  void compute_interaction(
                           Interaction_Computer& ic,
                           Structure< I0>& str0,
                           const typename I0::Transform& trans0,
                           Structure< I1>& str1,
                           const typename I1::Transform& trans1
                            );
  
  template< class Ia, class IC, class IC_Bottom>
  friend
  void compute_self_interaction( IC&& ic, IC_Bottom&& icb, Structure< Ia>& str,
                                const typename Ia::Transform& trans);
  
  std::reference_wrapper< Container> container;
  Index max_n;
  Cell< I> cell;
};

/*
template< class I>
Cell<I>& Structure<I>::cell_of(){
  return cell;
}
*/

/*
template< class I>
const Cell<I>& Structure<I>::cell_of() const{
  return cell;
}
*/

// constructor
template< class I>
Structure<I>::Structure( Container& conr, Index max_n_per_cell): container( conr), max_n(max_n_per_cell), cell(){
  load_objects();
}

// copy constructor
template< class I>
Structure<I>::Structure( const Structure<I>& other):
  container( other.container), cell( container, other.cell)
{
  max_n = other.max_n;
}

template< class I>
inline
void Structure< I>::load_objects(){
  cell.load_objects( container, max_n);
}
  
  /*
template< class I>
inline
void Structure< I>::set_max_number_per_cell( Index n){
  max_n = n;
}
*/

template< class I>
inline
void Structure< I>::shift( Pos d){
  cell.shift( d);
}

template< class I>
inline
void Structure< I>::transform( const Transform& tform){
  cell.perm_transform( tform);
}


//*************************************************************
template< class I0, class I1, class CD>
bool has_collision( 
                   const CD& cd,
                   
                   Structure< I0>& str0,
                   const typename I0::Transform& trans0,
                   typename I0::Ref& ref0,
                   
                   Structure< I1>& str1,
                   const typename I1::Transform& trans1,
                   typename I1::Ref& ref1,
                   Length& violation               
                   ){

  auto& cont0 = str0.container.get();
  auto& cont1 = str1.container.get();

  bool res =  has_collision< I0,I1, CD>(
                                 cd, 
                                 str0.cell, trans0, cont0, ref0,
                                 str1.cell, trans1, cont1, ref1,
                                 violation);

  str0.cell.clear_transform( cont0);
  str1.cell.clear_transform( cont1);
  
  return res;
}

//*************************************************************
template< class I0, class I1, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Structure< I0>& str0,
                         const typename I0::Transform& trans0,
                         Structure< I1>& str1,
                         const typename I1::Transform& trans1,
                         bool& has_collision,
                         typename I0::Ref& ref0,
                         typename I1::Ref& ref1,
                         Length& viol
                         ){
  
  auto& cont0 = str0.container.get();
  auto& cont1 = str1.container.get();

  compute_cell_interaction<I0,I1>( ic,
                          str0.cell, trans0, cont0,
                          str1.cell, trans1, cont1,
                          has_collision, ref0, ref1, viol);

  str0.cell.clear_transform( cont0);
  str1.cell.clear_transform( cont1);
}

//*************************************************************
template< class I0, class I1, class Interaction_Computer>
void compute_interaction(
                         Interaction_Computer& ic,
                         Structure< I0>& str0,
                         const typename I0::Transform& trans0,
                         Structure< I1>& str1,
                         const typename I1::Transform& trans1
                         ){

  auto& cont0 = str0.container.get();
  auto& cont1 = str1.container.get();

  compute_cell_interaction<I0,I1>( ic,
                          str0.cell, trans0, cont0,
                          str1.cell, trans1, cont1);

  str0.cell.clear_transform( cont0);
  str1.cell.clear_transform( cont1);
}

//*************************************************************
template< class I, class IC, class IC_Bottom>
void compute_self_interaction(
                         IC&& ic, IC_Bottom&& icb,
                         Structure< I>& str,
                         const typename I::Transform& trans
                         ){

  auto& cont = str.container.get();
  auto& cell = str.cell;
  compute_cell_self_interaction( ic,icb, cell, trans, cont); 
  cell.clear_transform( cont);
}

//*************************************************************
template< class I>
struct Queue_Info{
  typedef typename I::Position Pos;
  
  Queue_Info( const Cell< I>* _cell, const Pos& pos){
    cell = _cell;
    Length dist = distance( pos, cell->center);
    high_bound = dist + cell->radius;
    low_bound = dist - cell->radius;
  }

  Length low_bound, high_bound;
  const Cell< I>* cell;

  //Queue_Info(){
    //cell = nullptr;
  // }
  Queue_Info() = delete;
};

template< class I>
struct Queue_Comparer{
  bool operator()( const Queue_Info< I>& q0,
                   const Queue_Info< I>& q1){
    return q0.high_bound > q1.high_bound;
  }
};


//*************************************************************
// pos must be in the frame of the Structure

template< class I>
template< class Distance_Finder>
bool Structure< I>::is_within( const Distance_Finder& df,
                               Length r, 
                               const Pos& pos) const{

  typedef Cell< I> Cell;
  typedef Queue_Info< I> Queue_Info;

  bool result{ false};

  if (distance( this->cell.center, pos) < r + this->cell.radius){

    std::priority_queue< Queue_Info, Vector< Queue_Info>, 
      Queue_Comparer< I> > cells{};

    cells.push( Queue_Info( &(this->cell), pos));
    
    while( !cells.empty()){
      Queue_Info qi = cells.top();
      cells.pop();
      
      const Cell* lcell = qi.cell;
      
      if (qi.high_bound < r){
        result = true;
        break;
      }
      else if (qi.low_bound < r){

        if (lcell->is_bottom()){
          bool within = df( container, lcell->contents, r, pos);
          if (within){
            result = true;
            break;
          }       
        }
        else { // not bottom
          const Cell* child0 = lcell->child0.get();
          const Cell* child1 = lcell->child1.get();

          cells.push( Queue_Info( child0, pos));
          cells.push( Queue_Info( child1, pos));
        }
      }
    }
  }

  return result;
}
  
//*************************************************************
template< class I>
    [[maybe_unused]] typename I::Ref
Structure< I>::nearest_object( const Pos& r){

  //typedef typename I::Ref Ref;
  typedef Cell< I> Cell;
  typedef Queue_Info< I> Queue_Info;

  std::priority_queue< Queue_Info, Vector< Queue_Info>, 
    Queue_Comparer< I> > cells{};

  cells.push( Queue_Info( &(this->cell), r));
  

  auto closest = Length( DBL_MAX);
  auto high_bound = Length( DBL_MAX);

  Ref result{};

  while( !cells.empty()){
    Queue_Info qi = cells.top();
    cells.pop();

    Cell* lcell = qi.cell;

    Length llow_bound = qi.low_bound;
    Length lhigh_bound = qi.high_bound;

    if (llow_bound < high_bound){

      if (lhigh_bound < high_bound)
        high_bound = lhigh_bound;
    
      if (lcell->is_bottom()){
        auto dist = Length( large);
        Ref lref{};

        auto n = I::size( container, lcell->contents);
        for( auto i: range( n)){
        //for( auto i = Index(0); i<n; i++){
          auto pos = I::position( container, lcell->contents, i);
          Length radius = I::radius( container, lcell->contents, i);
          Length d = distance( r, pos) + radius;
          if (d < dist){
            dist = d;
            lref = I::reference( container, lcell->contents, i);
          }
        }

        if (dist < closest){
          closest = dist;
          result = lref;
        }
      }
      else{
   
        cells.push( Queue_Info( lcell->child0.get(), r));
        cells.push( Queue_Info( lcell->child1.get(), r));

      }
    } 
  }
  return result;

}

//*************************************************************
template< class I>
class Small_Queue_Info{
  Small_Queue_Info( Cell< I>* _cell, const Length dist){
    cell = _cell;
    low_bound = dist - cell->radius;
    high_bound = dist + cell->radius;
  }

  Length low_bound, high_bound;
  Cell< I>* cell;
};

template< class I>
struct Small_Queue_Comparer{
  bool operator()( const Small_Queue_Info< I>& q0,
                   const Small_Queue_Info< I>& q1){
    return q0.low_bound > q1.low_bound;
  }
};


//*************************************************************
// makes no attempt to resolve distances > max_distance; will
// simply return infinity
template< class I>
template< class Distance_Fun>
Length Structure< I>::
distance_from_field( const Distance_Fun& distance_fun, Length max_distance){

  typedef Cell< I> Cell;
  typedef Small_Queue_Info< I> Small_Queue_Info;

  std::priority_queue< Small_Queue_Info, Vector< Small_Queue_Info>, 
    Small_Queue_Comparer< I> > cells{};

  cells.push( Small_Queue_Info( &(this->cell), 
                                distance_fun( this->cell->center)));

  auto high_bound = Length( DBL_MAX);
  auto min_distance = Length( DBL_MAX);

  while( !cells.empty()){
    Small_Queue_Info qi = cells.top();
    cells.pop();

    Cell* lcell = qi.cell;

    if (lcell->high_bound < high_bound)
      high_bound = lcell->high_bound;
    
    if (lcell->low_bound < high_bound && lcell->low_bound < max_distance){

      if (lcell->is_bottom()){
        Length dist = 
          I::minimum_distance( container,
                                       lcell->contents, distance_fun);
        if (dist < min_distance)
          min_distance = dist;

        if (dist < high_bound)
          high_bound = dist;

      }
      else { // not bottom
        Cell* child0 = lcell->child0.get();
        Length r0 = distance_fun( child0->center);
        cells.push( Small_Queue_Info( child0, r0));

        Cell* child1 = lcell->child1.get();
        Length r1 = distance_fun( child1->center);
        cells.push( Small_Queue_Info( child1, r1));
      }
    }
  }
  return min_distance;
}

}

