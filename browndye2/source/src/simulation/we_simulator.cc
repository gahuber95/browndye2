/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of WE_Simulator
*/

#include <fstream>
#include <string.h>
#include <vector>
#include "we_simulator.hh"
#include "../transforms/transform.hh"
#include "../lib/found.hh"
#include "../xml/node_info.hh"
#include "../molsystem/system_state.hh"
#include "../molsystem/system_common.hh"
#include "../molsystem/system_thread.hh"
#include "we_thread_runner.hh"

namespace Browndye{
  
//##############################################################
void WE_Simulator::get_bins( JAM_XML_Pull_Parser::Node_Ptr node){
  
  namespace JP = JAM_XML_Pull_Parser;
  std::string bin_file = checked_value_from_node< ::std::string>( node, "bin_file");
   
  std::ifstream input( bin_file.c_str());

  if (!input.is_open()){
    error( "bins file", bin_file.c_str(), "could not be opened");
  }

  JP::Parser parser( input, bin_file);
  auto bnode = parser.top();
  bnode->complete();
  
  auto bnodes = bnode->children_of_tag( "p");
  size_t n_bins = bnodes.size() + 1;
  bin_partitions.resize( n_bins+1);

  bin_partitions[0] = 0.0;
  auto itr = bnodes.begin();
  for( auto i: range( 1, n_bins)){
    bin_partitions[i] = value_from_node< double>( *itr);
    ++itr;
  }
  bin_partitions[ n_bins] = large;
}

const Rxn_Index r1(1);
const auto rd1 = r1 - Rxn_Index(0);

//####################################################################
// constructor
WE_Simulator::WE_Simulator( JAM_XML_Pull_Parser::Node_Ptr node, bool bb): Simulator( node){
  namespace JP = JAM_XML_Pull_Parser;
  building_bins = bb;

  rng.set_seed( extra_seed);

  n_final_rxns = common->n_reactions();
  next_fluxes.resize( n_final_rxns + rd1);
  n_copies = checked_value_from_node< Sys_Copy_Index>( node, "n_copies");
  n_bin_copies = checked_value_from_node< Sys_Copy_Index>( node, "n_bin_copies");

  n_steps = checked_value_from_node< size_t>( node, "n_steps");

  auto nws_res =  value_from_node< size_t>( node, "n_we_steps_per_output");
  if (nws_res){
    n_steps_per_output = *nws_res;
  }
  
  thread_runner = std::make_unique< WE_Thread_Runner>( *this);
  
  if (building_bins){
    auto bin_file_name = checked_value_from_node< std::string>( node, "bin_file");
    bin_ofstream.open( bin_file_name.c_str());
  }
  else{
    get_bins( node);  
  }
  
  sampler = std::make_unique< Sampler>( *this);
}

//#################################################
void WE_Simulator::run(){

  output.open( output_name.c_str());
  
  sampler->set_number_of_moves_per_output( n_steps_per_output);
  sampler->initialize_objects();
  
  begin_output( output);

  output << "  <n> " << n_final_rxns + rd1 << " </n>\n";
  
  auto rnames = common->rxn_names();
  output << "  <rxn_names> ";
  for( auto& name: rnames){
    output << name << ' ';
  }
  output << "  </rxn_names>\n";
  

  output << "  <data>\n";
  stream_position = output.tellp();
  
  thread_runner->run_threads();  
}

//###########################################################
void WE_Simulator::generate_bins(){

  building_bins = true;
  common->building_bins = true;
  
  istep = 0;

  std::list< double> partitions;
  sampler->generate_bins( partitions);

  // should print from 0 to largest 
  bin_ofstream << "<bin_partitions>\n";
  for( auto part: partitions){
    bin_ofstream << " <p> " << part << " </p>\n";
  }
  bin_ofstream << "</bin_partitions>\n";
}

//###################################################
size_t WE_Simulator::number_of_fluxes() const{
  
  if (building_bins){
    return common->n_reactions()/r1; //better check
  }
  else{
    return (n_final_rxns + rd1)/r1;
  }
}

//#####################################################
Sys_Copy_Index WE_Simulator::number_of_objects() const{
  return building_bins ? n_bin_copies : n_copies; 
}

}

