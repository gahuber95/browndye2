#pragma once

#include <string>

/*
 * system_fate.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

namespace Browndye{

enum class System_Fate {In_Action, Escaped, Final_Rxn, Stuck, Undefined};

// for debugging
template< class Stm>
inline
Stm& operator<<( Stm& stm, System_Fate fate){
  
  std::string rep;
  if (fate == System_Fate::In_Action){
    rep = "in action";
  }
  else if (fate == System_Fate::Escaped){
    rep = "escaped";
  }
  else if (fate == System_Fate::Final_Rxn){
    rep = "final rxn";
  }
  else if (fate == System_Fate::Stuck){
    rep = "stuck";
  }
  else{
    rep = "undefined";
  }
  
  stm << rep;
  return stm; 
}

}
