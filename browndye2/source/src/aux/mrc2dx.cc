#include <fstream>
#include <iostream>
#include <vector>
#include "../lib/float32_t.hh"
#include "../input_output/get_args.hh"

// need to make more general if it is included as a regular part of Browndye

bool is_big_endian(){  
  const uint32_t i = 1;
  return (*(char*)&i) == 0 ;
}

typedef Browndye::float32_t float32_t;

union Input{
  char data[4];
  int32_t result;
  float32_t fresult;
};

template< class Stm>
int32_t int_from( Stm& fp){
  Input input;
  fp.read( input.data, 4);
  return input.result;
}

template< class Stm>
float32_t float_from( Stm& fp){
  Input input;
  fp.read( input.data, 4);
  return input.fresult;
}

template< class Stm>
std::string string_from( Stm& fp){
  char data[5];
  data[4] = (char)0;
  fp.read( data, 4);
  return std::string( data);
}

namespace BD = Browndye;

int main( int argc, char* argv[]){
  using std::cout;
  using std::cin;
  //std::string fname( "S_reduced_table_170427_cc0p035.mrc");
  
  std::string help( "sends to standard output the density map in dx format; receives mrc file from standard input \n -factor: multiplicative factor, default 1 \n -exp: output factor*exp( density), default false");
  BD::print_help( argc, argv, help);

  double factor = 1.0;
  auto new_factor = BD::double_arg( argc, argv, "-factor");
  if (new_factor == new_factor)
    factor = new_factor;

  bool boltz = BD::has_flag( argc, argv, "-exp");

  int nx = -1,ny = -1,nz = -1;
  double lx = NAN,ly = NAN,lz = NAN;
  int nxt = -1;
  double dens_min, dens_max;
  
  for( int i = 1; i <= 56; i++){
    if ((11 <= i && i <= 16) || (20 <= i && i <= 22)){
      auto res = float_from( cin);
      if (i == 11)
    	lx = res;
      else if (i == 12)
    	ly = res;
      else if (i == 13)
    	lz = res;
      else if (i == 20)
    	dens_min = res;
      else if (i == 21)
    	dens_max = res;
    }
    /*
    else if (i == 27 || i == 53)
      std::cout << i << " " << string_from( cin) << "\n";
    else if (i == 54){
      auto bytes = string_from( cin);
      for( int k = 0; k < 4; k++)
    	std::cout << std::hex << (unsigned int)bytes.c_str()[k] << " ";
      std::cout << std::dec << "\n";
    }
    */
    else{
      int res = int_from( cin);
      if (i == 1)
    	nx = res;
      else if (i == 2)
    	ny = res;
      else if (i == 3)
    	nz = res;
      else if (i == 24)
    	nxt = res;
      
      //std::cout << i << " " << res << "\n";
    }
  }
  (void)dens_min;
  (void)dens_max;
  
  char label[81];
  label[80] = 0;
  for( int i = 0; i < 10; i++){
    cin.read( label, 80);
    //std::cout << label << "\n";
  }

  //std::cout << "ns " << nx << " " << ny << " " << nz << "\n";
  //std::cout << "lens " << lx << " " << ly << " " << lz << "\n";
  //std::cout << "dens " << dens_min << " " << dens_max << "\n";
  //std::cout << "nxt " << nxt << "\n";

  std::vector< char> ext_head( nxt);
  cin.read( ext_head.data(), nxt);

  std::vector< std::vector< std::vector<float32_t> > > density( nz);
  for( int iz = 0; iz < nz; iz++){
    auto& slice = density[iz];
    slice.resize( ny);
    for( int iy = 0; iy < ny; iy++){
      auto& line = slice[iy];
      line.resize( nx);
      cin.read( (char*)(line.data()), 4*nx);
    }
  }

  double hx = lx/nx;
  double hy = ly/ny;
  double hz = lz/nz;
  
  cout << "# From MRC file\n";
  cout << "object 1 class gridpositions counts "
       <<  nx << " " << ny << " " << nz << "\n";
  cout << "origin 0.0 0.0 0.0\n";
  cout << "delta " << hx << " 0.0 0.0\n";
  cout << "delta 0.0 " << hy << " 0.0\n";
  cout << "delta 0.0 0.0 " << hz << "\n";
  cout << "object 2 class gridconnections counts "
       << nx << " " << ny << " " << nz << "\n";
  cout << "object 3 class array type double rank 0 items "
       <<  nx*ny*nz << " data follows\n";

  size_t id = 0;
  for( int ix = 0; ix < nx; ix++)
      for( int iy = 0; iy < ny; iy++)
    	for( int iz = 0; iz < nz; iz++){
    	  auto rho = density[iz][iy][ix];
    	  if (boltz)
    	    cout << factor*exp( rho);
    	  else
    	    cout << factor*rho;
	  
    	  ++id;
    	  if (id == 3){
    	    cout << "\n";
    	    id = 0;
    	  }
    	  else
    	    cout << " ";
    	}
    	
  if (id > 0)
    cout << "\n";

  cout << "attribute \"dep\" string \"positions\"\n";
  cout << "object \"regular positions regular connections\" class field\n";
  cout << "component \"positions\" value 1\n";
  cout << "component \"connections\" value 2\n";
  cout << "component \"data\" value 3\n";
}

