#pragma once
#include <variant>
#include "../lib/array.hh"
#include "../lib/units.hh"
#include "../global/indices.hh"

namespace Browndye{
   
class Bead0_Index_Tag{};
typedef Index< Bead0_Index_Tag> Bead0_Index;

class Constraint0_Index_Tag{};
typedef Index< Constraint0_Index_Tag> Constraint0_Index;   
      
template< class Idx, class Idxa> 
struct BRG{ // Bead Ref Generic
  Idx is; // can refer to chain, core, tet
  Idxa ia;
  
  BRG( Idx _is, Idxa _ia): is(_is),ia(_ia){}
  
  bool operator<( BRG other) const{
    std::pair< Idx, Idxa> p{ is,ia}, po{ other.is, other.ia};
    return p < po;
  }
  
  bool operator==( BRG other) const{
    std::pair< Idx, Idxa> p{ is,ia}, po{ other.is, other.ia};
    return p == po;
  }
};
  
typedef BRG< Chain_Index, Atom_Index> Chain_Bead;
typedef BRG< Core_Index, Ind_Atom_Index> Core_Bead;
typedef BRG< Tet_Index, Atom_Index> Tet_Bead;

typedef std::variant< Chain_Bead, Core_Bead, Tet_Bead> Bead;


//#################  
struct Length_Constraint{
  Array< Bead0_Index, 2> beads;
  Length length;
};

struct Coplanar_Constraint{
  Array< Bead0_Index, 4> beads;
};

}
