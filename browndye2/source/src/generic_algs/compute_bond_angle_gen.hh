#pragma once

#include <cmath>

/* 
Given positions of three points, computes 
the bond angle in theta and the corresponding derivatives.
Points are numbered 0 through 2, with Point 1 being the
middle one.

I class must have the following types and static functions:

types: Chain, Group_Index, Value

functions:

Pos position( const Chain& chain, const Group_Index& iq, size_t ip) -
get the position of the ip^th point (0,1,2). The user-defined
index iq can be used to select the group of three points. Pos
 must be indexible like an array and have consistent units
 

void set_derivative( Chain& chain, const Group_Index& iq, size_t ip,
          size_t k, Deriv& a)
sets the k^th component (0,1,2) of the derivative with respect to the 
ip^th point (0,1,2) to a. The user-defined
index iq can be used to select the group of three points.
Deriv must have consistent units.

*/


//#########################################################
namespace Browndye{

template< class I>
void compute_bond_angle_and_derivatives( typename I::Chain& chain, typename I::Group_Index ig, typename I::Value &theta){

  auto p1 = I::position( chain, ig, 0);
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];
  
  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];

  auto p3 = I::position( chain, ig, 2);
  auto x3 = p3[0];
  auto y3 = p3[1];
  auto z3 = p3[2];

  // r12
  auto a10 = x1 - x2;
  auto a11 = y1 - y2;
  auto a12 = z1 - z2;
  
  // r32
  auto a13 = x3 - x2;
  auto a14 = y3 - y2;
  auto a15 = z3 - z2;

  // |r12|
  auto a16 = sqrt( a10*a10 + a11*a11 + a12*a12);

  // |r32|
  auto a17 = sqrt( a13*a13 + a14*a14 + a15*a15);

  // u1
  auto a18 = a10/a16;
  auto a19 = a11/a16;
  auto a20 = a12/a16;
  
  // u2
  auto a21 = a13/a17;
  auto a22 = a14/a17;
  auto a23 = a15/a17;
  
  // cos(theta)
  auto a24 = a18*a21 + a19*a22 + a20*a23;

  // theta
  theta = acos( a24);

  const auto b25 = 1.0;

  // theta
  auto b24 = -b25/sqrt( 1.0 - a24*a24);

  // cos(theta)
  auto b18 = b24*a21;
  auto b21 = b24*a18;

  auto b19 = b24*a22;
  auto b22 = b24*a19;

  auto b20 = b24*a23;
  auto b23 = b24*a20;

  // u2
  auto b15 = b23/a17;
  auto b14 = b22/a17;
  auto b13 = b21/a17;
  auto b17 = -(b23*a15 + b22*a14 + b21*a13)/(a17*a17);
  
  // u1
  auto b12 = b20/a16;
  auto b11 = b19/a16;
  auto b10 = b18/a16;
  auto b16 = -(b20*a12 + b19*a11 + b18*a10)/(a16*a16);
  
  // |r32|
  b13 += b17*a13/a17;
  b14 += b17*a14/a17;
  b15 += b17*a15/a17;

  // |r12|
  b10 += b16*a10/a16;
  b11 += b16*a11/a16;
  b12 += b16*a12/a16;

  // r32
  I::set_derivative( chain,ig,2,2,b15);
  I::set_derivative( chain,ig,2,1,b14);
  I::set_derivative( chain,ig,2,0,b13);

  I::set_derivative( chain,ig,1,2, -b15 - b12);
  I::set_derivative( chain,ig,1,1, -b14 - b11);
  I::set_derivative( chain,ig,1,0, -b13 - b10);

  // r12
  I::set_derivative( chain,ig,0,2,b12);
  I::set_derivative( chain,ig,0,1,b11);
  I::set_derivative( chain,ig,0,0,b10);
}

//#########################################################
template< class I>
[[maybe_unused]] auto bond_angle( typename I::Chain& chain, typename I::Group_Index ig) -> typename I::Value{
  
  auto p1 = I::position( chain, ig, 0);
  auto x1 = p1[0];
  auto y1 = p1[1];
  auto z1 = p1[2];
  
  auto p2 = I::position( chain, ig, 1);
  auto x2 = p2[0];
  auto y2 = p2[1];
  auto z2 = p2[2];
  
  auto p3 = I::position( chain, ig, 2);
  auto x3 = p3[0];
  auto y3 = p3[1];
  auto z3 = p3[2];

  // r12
  auto a10 = x1 - x2;
  auto a11 = y1 - y2;
  auto a12 = z1 - z2;
  
  // r32
  auto a13 = x3 - x2;
  auto a14 = y3 - y2;
  auto a15 = z3 - z2;


  // |r12|
  auto a16 = sqrt( a10*a10 + a11*a11 + a12*a12);

  // |r32|
  auto a17 = sqrt( a13*a13 + a14*a14 + a15*a15);

  // u1
  auto a18 = a10/a16;
  auto a19 = a11/a16;
  auto a20 = a12/a16;
  
  // u2
  auto a21 = a13/a17;
  auto a22 = a14/a17;
  auto a23 = a15/a17;
  
  // cos(theta)
  auto a24 = a18*a21 + a19*a22 + a20*a23;

  // theta
  auto theta = acos( a24);
  return theta;
}
}

