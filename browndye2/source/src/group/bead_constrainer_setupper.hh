#include "../lib/vector.hh"
#include "constraint.hh"

namespace Browndye {
class Group_Common;
class Group_Thread;
class Group_State;
class Chain_State;
class Core_State;
template< class G> class Tet;
struct Constraints_Info;

//##########################################################
    class Bead_Constrainer_Setupper {
    public:
        void doit();

        Bead_Constrainer_Setupper( Group_Thread &, Group_State &);

        void setup_new_cons_info();

    private:
        Group_State &state;
        Vector< Chain_State, Chain_Index> &chain_states;
        Vector< Core_State, Core_Index> &core_states;
        Vector< Tet < Group_State>, Tet_Index>& tets;

        Group_Thread &thread;
        const Group_Common &comm;
        std::optional<Chain_Index> coreless_chain;
        Chain_Index nc;
        Core_Index nm;
        Tet_Index nt;
        Vector<Vector<Chain_Index, Ind_Chain_Index>, Chain_Index> related_chains;
        Vector<Core_Index> free_cores;
        Vector<Vector<Core_Index, Ind_Core_Index>, Tet_Index> tet_cores;
        Vector<Vector<Chain_Index, Ind_Chain_Index>, Tet_Index> tet_chains;
        Vector<Core_Index, Tet_Index> min_core_of_tet;
        Vector<Chain_Index, Tet_Index> min_chain_of_tet;
        Vector<Bead0_Index, Bead_Index> core_beads;

        // private functions
        [[nodiscard]] bool is_frozen(Chain_Index) const;

        void setup_coreless_chain();

        void setup_related_chains();

        void setup_free_cores();

        void setup_tets();

        void label_with_tets();

        void setup_min_core_of_tet();

        void setup_min_chain_of_tet();

        void setup_divs(Constraints_Info &);

        void setup_beads(Constraints_Info &);

        void setup_constraints(Constraints_Info &);

        void add_chains_of_cores();

        void add_to_related_chains();

        void remove_dups_of_related_chains();

        void setup_bead_diffs(Constraints_Info &);

        void setup_chain_beads(Constraints_Info &);

        void setup_core_beads(Constraints_Info &);

        void setup_tet_beads(Constraints_Info &);

        void setup_bead_indices(Constraints_Info &);

        void setup_tet_core_constraints(Constraints_Info &);

        void setup_tet_tet_constraints(Constraints_Info &);

        void setup_length_constraints(Constraints_Info &);

        void setup_coplanar_constraints(Constraints_Info &);

        static void settle_tet_constraints(Constraints_Info &);

        void sort_constraints(Constraints_Info &cons_info);

        void remove_empty_related_chains();

        void remeasure_tet_and_core_cons();

        void remeasure_tet_tet_cons();

        void remeasure_tm_cons();

        static
        Bead0_Index tet_bead_index_of(Constraints_Info &cons_info, Tet_Index it, Atom_Index ia);

        template<class Con>
        bool has_unfrozen_chain(const Constraints_Info &, const Con &, int);


        [[nodiscard]] Cor_Cha_Index order_index(const Constraints_Info &, Bead0_Index ib) const;
    };
}