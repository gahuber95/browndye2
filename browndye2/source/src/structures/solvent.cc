/*
 * solvent.cc
 *
 *  Created on: Oct 20, 2016
 *      Author: root
 */

#include "solvent.hh"
#include "../xml/node_info.hh"
#include "../input_output/outtag.hh"

namespace{
  namespace JP = Browndye::JAM_XML_Pull_Parser;
}

namespace Browndye{

void Solvent::initialize( const JP::Node_Ptr& node){
  
  auto get = [&]( const std::string& tag, auto& x) -> void{
    typedef std::remove_reference_t< decltype(x)> X;
    auto xo = value_from_node< X>( node, tag);
        
    if (xo){
      x = *xo;
    }
  };
  
  get( "debye_length", debye_length);
  get( "dielectric", dielectric);
  get( "relative_viscosity", relative_viscosity);
  get( "kT", kT);
  get( "desolvation_parameter", desolve_fudge);
}

//###########################################
Solvent::Solvent( const JP::Node_Ptr& node){
  initialize( node);  
}

//###########################################
Solvent::Solvent( const std::string& fname){

  std::ifstream stm( fname);
  if (!stm.is_open()){
    error( "solvent file", fname, "could not be opened");
  }
  JAM_XML_Pull_Parser::Parser parser( stm, fname);
  auto node = parser.top();
  node->complete();
  initialize( node);
}

//####################################################
void Solvent::output( std::ofstream& outp) const{
  
  auto ot = [&]( const std::string& tag, auto t){
    outtag( outp, tag, t, 4);
  };
  
  outp << "  <solvent>\n";
  ot( "kT", kT);
  ot( "debye_length", debye_length);
  ot( "dielectric", dielectric);
  ot( "vacuum_permittivity", vacuum_permittivity);
  ot( "water_viscosity", water_viscosity);
  ot( "relative_viscosity", relative_viscosity);
  outp << "  </solvent>\n";
  
}

}