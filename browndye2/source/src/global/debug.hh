#pragma once

/*
 * debug.hh
 *
 *  Created on: Oct 18, 2016
 *      Author: root
 */




#ifdef DEBUG
  #include <iostream>
  #include <cassert>

  namespace Browndye{
    constexpr bool debug{ true};
    constexpr bool bounds_check{ true};
    using std::cerr;
    using std::cout;
    using std::endl;
    
    inline
    void mabort(){
      abort();
    }
    
    //inline void crap(){}
  }
#else

#ifdef BOUNDS
  namespace Browndye{
    constexpr bool bounds_check{ true};
  }
#else
  namespace Browndye{
    constexpr bool bounds_check{ false};
  }
#endif

  #include <iostream>
  // debug but with optimizations
  using std::cout;
  using std::endl;
  //using std::cerr;
  
  namespace Browndye{
    constexpr bool debug{ false};
  }
#endif

