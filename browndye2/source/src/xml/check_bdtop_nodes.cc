#include "check_bdtop_nodes.hh"

namespace Browndye{
  void check_bdtop_input( JAM_XML_Pull_Parser::Node_Ptr node){
    
    Tag pattern{ "root", "n_threads", "seed", "output", "n_trajectories",
            "n_trajectories_per_output", "max_n_steps", "trajectory_file",
            "n_steps_per_output","min_rxn_dist_file", "n_copies",
            "n_bin_copies", "n_steps", "n_we_steps_per_output", "bin_file",
            "n_output_states_per_block",
            tag("system",
                tag("solvent", "debye_length", "dielectric",
                    "relative_viscosity", "kT","desolvation_parameter",
                    "solvent_radius",
                    tag("ions", tag("ion", "charge", "radius", "conc"))),
                "force_field", "parameters", "connectivity",
                "start_at_site", "b_radius", "reaction_file",
                "density_field", "atom_weights",
                "hydrodynamic_interactions", "n_steps_between_hi_updates",
                tag( "time_step_tolerances", "force", "reaction", "constant_dt",
                    "minimum_core_dt", "minimum_chain_dt",
                    "minimum_core_reaction_dt", "minimum_chain_reaction_dt"),
                tag( "restraints",
                    tag( "restraint", "length",
                        "group0", "core0", "chain0", "atom0",
                        "group1", "core1", "chain1", "atom1")),
                tag( "group", "name", 
                    tag("core", "name", "atoms", "all_in_surface",
                        "is_protein", "atoms", "dielectric",
                        "grid_spacing", "areas",
                        tag("electric_field", "grid"),
                        tag("copy", "core", "translation", "rotation")
                    ),
                    tag("chain", "name", "atoms", "constraints", "constant_dt",
                        tag("link","core_name", "core_residue",
                              "chain_residue")
                    )
                )
            )
          };
    
    pattern.check_node( node);
  }
}
