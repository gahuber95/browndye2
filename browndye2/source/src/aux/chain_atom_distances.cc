#include "../xml/jam_xml_pull_parser.hh"
#include "../xml/node_info.hh"
#include "../transforms/transform.hh"
#include "../transforms/pure_translation.hh"
#include "../lib/rotations.hh"

namespace{
  
  using namespace Browndye;
  namespace JP = JAM_XML_Pull_Parser;
  using std::string;
  using std::max;
  using std::make_pair;
  typedef Vector< double> Vec;
  
  template< class T>
  Vector< T> vector_of_list( const std::list< T>& lst){
    Vector<T> res;
    for( auto x: lst){
      res.push_back( x);
    }
    return res;
  }
  
  //####################################################
  /*
  Pos pos_from( const Vector< Length>& poses, size_t i){
    
    auto i3 = 3*i;
    
    if (i3 + 2 >= poses.size()){
      error( "not enough positions in trajectory record");
    }
    
    return Pos{ poses[i3], poses[i3+1], poses[i3+2]};
  }
  */
  //###############################################################
  std::pair< Transform, Transform> tforms_of_state( JP::Node_Ptr node, std::pair< Pos, Pos> centers){
    
    auto mnodes = vector_of_list( node->children_of_tag( "core"));
    auto [cen0,cen1] = centers;
    
    auto tform_of = [&]( JP::Node_Ptr mnode, Pos cen){
      auto cdata = array_from_node< double, 7>( mnode);
      Pos trans{ Length{ cdata[0]}, Length{ cdata[1]}, Length{ cdata[2]}};
      Array< double, 4> quat{ cdata[3],cdata[4],cdata[5],cdata[6]};
      auto rot = mat_of_quat( quat);
      Transform tform1( rot, trans);
      Pure_Translation tform0( -cen);
      return tform1*tform0;
    };
    
    auto tform0 = tform_of( mnodes[0], cen0);
    auto tform1 = tform_of( mnodes[1], cen1);
    return make_pair( tform0, tform1);
  }
  
  //##########################################################
  std::pair< Length, double>
  dists_n_times( JP::Node_Ptr node, std::pair< Pos, Pos> centers, Pos posa0, Pos posa1, Pos posa2, Pos posb0, Pos posb1, Pos posb2){
    
    auto dt = checked_value_from_node< double>( node, "dt");
    
    auto [tforma, tformb] = tforms_of_state( node, centers);
    
    auto tposa0 = tforma.transformed( posa0);
    auto tposa1 = tforma.transformed( posa1);
    auto tposa2 = tforma.transformed( posa2);
    auto tposb0 = tformb.transformed( posb0);
    auto tposb1 = tformb.transformed( posb1);
    auto tposb2 = tformb.transformed( posb2);

    auto r0 = distance( tposa0, tposb0);
    auto r1 = distance( tposa1, tposb1);
    auto r2 = distance( tposa2, tposb2);
    
    auto r = max( max( r0,r1),r2);
    return make_pair( r, dt); 
  }
  
  //########################################################
  /*
  Vector< size_t> atom_numbers_of_res( JP::Node_Ptr rnode){
    
    rnode->complete();
  
    Vector< size_t> res;
    auto anodes = rnode->children_of_tag( "atom");
    for( auto anode: anodes){
      auto i = checked_value_from_node< size_t>( anode, "number");
      res.push_back( i);
    }
    return res;
  }
  
  //########################################################
  std::map< size_t, size_t> atom_orders( const string& at_file){
    
    std::ifstream input( at_file);
    JP::Parser parser( input, at_file);
    
    Vector< Vector< size_t> > all_atom_numbers;
    
    auto get_an = [&]( JP::Node_Ptr rnode){
      auto atom_nums = atom_numbers_of_res( rnode);
      all_atom_numbers.push_back( std::move( atom_nums));
    };
    
    auto top = parser.top();
    parser.apply_to_nodes_of_tag( top, "residue", get_an);
    
    std::map< size_t, size_t> imap;
    size_t count = 0;
    for( auto& atom_nums: all_atom_numbers){
      for( auto i: atom_nums){
        imap.insert( std::make_pair( i, count));
        ++count;
      }
    }
    
    return imap;
  }
  */
  
  //#######################
  Pos init_pos( const string& at_file, size_t ia){
    std::ifstream input( at_file);
    JP::Parser parser( input, at_file);
    auto top = parser.top();
    
    bool found = false;
    Pos res;
    
    auto pres = [&]( JP::Node_Ptr rnode){
      if (!found){
        rnode->complete();
        auto anodes = rnode->children_of_tag( "atom");
        for( auto anode: anodes){
          auto ja = checked_value_from_node< size_t>( anode, "number");
          if (ja == ia){
            auto xyz = [&]( const string& tag){
             return checked_value_from_node< Length>( anode, tag);
            };
            res[0] = xyz( "x");
            res[1] = xyz( "y");
            res[2] = xyz( "z");
            found = true;
            break;
          }
        }
      }
    };
    
    parser.apply_to_nodes_of_tag( top, "residue", pres);
    
    if (!found){
      error( "atom number", ia, "not found in", at_file);
    }
    
    return res;
  }
  
  //######################################################
  void main0( const string& at_filea, const string& at_fileb,
               size_t ia0, size_t ia1, size_t ia2,
               size_t ib0, size_t ib1, size_t ib2){
    
    JP::Parser parser( std::cin);
      
    auto posa0 = init_pos( at_filea, ia0);
    auto posa1 = init_pos( at_filea, ia1);
    auto posa2 = init_pos( at_filea, ia2);
    auto posb0 = init_pos( at_fileb, ib0);
    auto posb1 = init_pos( at_fileb, ib1);
    auto posb2 = init_pos( at_fileb, ib2);
    
    auto top = parser.top();
    
    auto centers = [&]{
      auto csnode = parser.next_child_with_tag( top, "core_centers");
      auto cnodes = vector_of_list( csnode->children_of_tag( "center"));
      auto cen0 = array_from_node< Length,3>( cnodes[0]);
      auto cen1 = array_from_node< Length,3>( cnodes[1]);
      return make_pair( cen0,cen1);
    }();

    size_t istep = 0;
    auto get_rt = [&]( JP::Node_Ptr node){
      node->complete();
      auto [r,dt] = dists_n_times( node, centers, posa0,posa1,posa2, posb0,posb1,posb2);
      std::cout << r << ' ' << dt << ' ' << istep << std::endl;
      ++istep;
    };     
        
    parser.apply_to_nodes_of_tag( top, "state", get_rt);
  }
}

//##########################################################
int main( int argc, char* argv[]){
  
  if (argc == 2){
    string arg = argv[1];
    if (arg == "-help" || arg == "--help"){
      std::cout <<
        "outputs max of atom-atom distances, and time steps\n"
        "input molecule 0 transforms with first core of simulated molecule 0\n"
        "input molecule 1 transforms with first core of simulated molecule 1\n"
        "arguments: atom file names (2) (xml from pqr), "
        "atom numbers(3)\n"
        "atom numbers(3)\n"
        "receives output of process_trajectory through standard input\n";
      return 0;
    }
  }  
  
  if (argc != 9){
    error( "wrong number of args", argc);
  }
  
  string at_file0( argv[1]), at_file1( argv[2]);
  size_t i0 = atoi( argv[3]), i1 = atoi( argv[4]), i2 = atoi( argv[5]);
  size_t j0 = atoi( argv[6]), j1 = atoi( argv[7]), j2 = atoi( argv[8]);
  
  main0( at_file0, at_file1, i0, i1,i2, j0,j1,j2);
}
