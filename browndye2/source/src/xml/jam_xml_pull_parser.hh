#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Top-level parsing classes used in the C++ code.
Implements a parser using the same model as the Ocaml version in
jam_xml_ml_pull_parser.ml. Implements two classes: the Parser and
the Node.  It wraps the Parser_1F class.
*/

#include <map>
#include <list>
#include <memory>
#include <queue>
#include "jam_xml_1f_parser.hh"
#include "../global/error_msg.hh"
#include "../lib/slist.hh"
#include "../lib/string_rep.hh"

namespace Browndye::JAM_XML_Pull_Parser{

/*************************************************************/
namespace JP = JAM_XML_Parser;
using std::string;

typedef JP::Attrs Attrs;

class Parser;

class Node{
public:
  typedef std::shared_ptr< Node> Node_Ptr;
  
  [[nodiscard]] const string& tag() const;
  [[nodiscard]] const Attrs& attributes() const;
  //[[nodiscard]] bool is_complete() const;

  Node_Ptr child( const string& tag);  
  std::list< Node_Ptr> children_of_tag( const string& tag);
  [[nodiscard]] const std::list< Node_Ptr>& all_children() const;
  [[nodiscard]] Node_Ptr with_child_removed( const string& tag) const;
  
  template< class T>
  [[maybe_unused]] Node_Ptr with_child_added( const string& tag, T&& data) const;
  
  template< class T>
  [[maybe_unused]] Node_Ptr with_child_replaced( const string& ntag,
                                 const string& otag, T&& data) const;

    [[maybe_unused]] [[nodiscard]] Node_Ptr with_child_replaced( const string& tag, const Node_Ptr&) const;

    [[maybe_unused]] [[nodiscard]] Node_Ptr with_child_added( const Node_Ptr&) const;

    [[maybe_unused]] void add_attribute( const string& key, const string& val);
  
  Node() = default;
  Node( const string& tag, const JP::Attrs&, Parser&);
  
  class Initially_Complete{};
  Node( const string& tag, Parser&, Initially_Complete);
  
  class From_File{};
  Node( const string& file, From_File);
  
  Node( const Node&) = delete;
  Node& operator=( const Node&) = delete;
  Node& operator=( Node&&) = default;
  Node( Node&&) = default;
  
  [[nodiscard]] const Vector< string>& data() const;
  
  [[nodiscard]] int line_number() const;
  [[nodiscard]] const string& file_name() const;
  
  template< typename ... Args>
  void perror( Args ... args) const;
  
  void complete();
  Parser& parser();
  
  template< class Stream>
  void output( Stream&) const;
  
  void check_parser() const;  
  [[nodiscard]] Node copy() const;
//###########################################################

private:
  void copy_to( Node&) const;
  
  template< class Stream>
  void output( Stream&, size_t n_indent) const;
    
  [[nodiscard]] Node_Ptr with_child_replaced_str( const string& ntag,
                        const string& otag, const string& data) const;
    
  [[nodiscard]] Node_Ptr with_child_added_str( const string& tag, const string& data) const;
    
  void check( const Node_Ptr& ) const;  
        
  friend 
  void begin_tag( Parser&, const string&, const JP::Attrs&);

  friend 
  void end_tag( Parser&, [[maybe_unused]] const string&, Vector< string>&&);

  friend
  class Parser;

  string ttag;
  JP::Attrs attrs;

  Vector< string> sdata;
  std::list< Node_Ptr> children;
 
  Parser* _parser = nullptr;
  int line_num = 0;
  
  bool completed = false;
  std::unique_ptr< Parser> owned;
  
  std::ifstream input;
};

std::shared_ptr< Node> node_from_file( const std::string&);

typedef typename Node::Node_Ptr Node_Ptr;

//#########################################################
enum class Tag_Needed {True [[maybe_unused]], False [[maybe_unused]]
};
enum class Apply_Type {Completed [[maybe_unused]], All_Of [[maybe_unused]], Continue [[maybe_unused]]
};

class Parser{
public:
  explicit Parser( std::istream&);
  Parser( std::ifstream&, const string&);
  Parser( std::stringstream&);
  Parser() = delete;
  Parser( const Parser&) = delete;
  Parser( Parser&&) = default;
  Parser& operator=( const Parser&) = delete;
  Parser& operator=( Parser&&) = delete;
  
  Node_Ptr top();
  
  template< class F>
  [[maybe_unused]] void apply_to_nodes_of_tag( const Node_Ptr&, const string& tag, F&& f);
  
  [[nodiscard]] bool done() const;
    
  [[nodiscard]] int current_line_number() const;
  [[nodiscard]] const string& file_name() const;
  
  template< size_t n, class Done, class...F>
  [[maybe_unused]] bool apply_to_next_children_with_tags( const Node_Ptr&,
                        const Array< string,n>& tags,
                        Done&&, F&&...f);

  Node_Ptr next_child_with_tag( Node_Ptr, const string&);

  std::pair< Node_Ptr, size_t>
  next_child_with_tag( Node_Ptr, const Vector< string>&);

  ~Parser();

private:
  friend Node;

  friend
  void begin_tag( Parser&, const string& ctag, const JP::Attrs& attrs);

  friend 
  void end_tag( Parser&, [[maybe_unused]] const string&, Vector< string>&&);
  
  std::istream& stream;

  typedef JP::Parser_1F< Parser> JParser;
  std::unique_ptr< JParser> jparser;
  
  std::stack< std::shared_ptr< Node> > leading;
  bool parsing_done = false; // needed?
  bool traversal_done = false;
  Node_Ptr top_node;

  void parse_some();  
  void complete_node( Node*);
};
  
//############################################################
inline
Parser& Node::parser(){
  return *_parser;
}

inline
void Node::complete(){
  parser().complete_node( this);
}

inline
int Node::line_number() const{
  return line_num;
}

inline
const string& Node::file_name() const{
  return _parser->file_name();
}

/*
inline
bool Node::is_complete() const{
  return completed;
}
*/

//#################################################
template< class T>
    [[maybe_unused]] Node_Ptr Node::with_child_added( const string& tag, T&& data) const{
  
  return with_child_added_str( tag, string_rep( std::forward<T>( data)));
}

//#################################################
template< class T>
    [[maybe_unused]] Node_Ptr Node::with_child_replaced( const string& ntag,
                                     const string& otag, T&& data) const{
  
  return with_child_replaced_str( ntag, otag,
                               string_rep( std::forward<T>( data)));
}

//##################################################################
  // applies f to children nodes, node not necessarily completed
template< class F>
    [[maybe_unused]] void Parser::apply_to_nodes_of_tag( const Node_Ptr& node,
                                     const string& tag, F&& f){

  auto& children = node->children;
  while( true){
    for( auto& cnode: children){
      if (cnode->tag() == tag){
        f( cnode);
        cnode->complete();
      }
    }
    
    node->children.clear();
   
    if (node->completed){
      break;
    }
    else{
      parse_some();      
    }
  }  
}

//####################################################
template< class Stream>
void Node::output( Stream& stm, size_t n_indent) const{
  
  const size_t offset = 2;
  auto ps = [&](){
    for( auto i: range( offset*n_indent)){
      (void)i;
      stm << ' ';
    }
  };
  
  ps();
  stm << "<" << ttag;
  for( auto& item: attrs.dict){
    auto [key,val] = item;
    stm << ' ' << key << "=\"" << val << "\"";  
  }
  stm << ">";
  if (!children.empty()){
    ps();
  }
  else{
    stm << ' ';
  }
  
  for( auto s: sdata){
    stm << s << ' ';
  }
  
  if (!children.empty()){
    stm << '\n';
    for( auto& child: children){
      child->output( stm, n_indent + 1);
    }
    ps();
  }
  stm << "</" << ttag << ">\n";
}

//######################################
template< class Stream>
void Node::output( Stream& stm) const{
  
  output( stm, 0);
}

//####################################################
// constructor
inline
Node::Node( const string& tag, Parser& parser, Initially_Complete):
  Node( tag, Attrs(), parser){
  completed = true;
}

inline
Node_Ptr Parser::top(){
  return top_node;
}

inline
const string& Node::tag() const{
  return ttag;
}

inline
const JP::Attrs& Node::attributes() const{
  return attrs;
}

inline
const Vector< string>& Node::data() const{
  return sdata;
}

inline
int Parser::current_line_number() const{
  return jparser->current_line_number();
}  

inline
const string& Parser::file_name() const{
  return jparser->file_name();
}  

template< typename ... Args>
void Node::perror( Args ... args) const{
  
  std::cout << "perror\n";
  std::cout << line_number() << std::endl;
  std::cout << tag() << std::endl;
  //std::cout << _parser << std::endl;
  std::cout << file_name() << std::endl;
  
  error( "In file", file_name(), "in tag", tag(), "at line", line_number(), args...);
}
   
//####################################   
template< class F0, class ... Fs>
struct GList{
  
  typedef GList< Fs...> Tail;
  
  const string tag;
  F0& f;
  Tail gtail;
  
  size_t n = 0;
  static constexpr size_t nsize = Tail::nsize + 1;

    [[maybe_unused]] GList( const SList< string>& tags, F0& f0, Fs&...fs):
    tag( head( tags)),
    f( f0), 
    gtail{ tail( tags), fs...}{
    
    gtail.inc_n();
  }
  
  void inc_n(){
    ++n;
    gtail.inc_n();
  }
  
  std::optional< size_t> apply( Node_Ptr cnode){
    if (cnode->tag() == tag){
      f( cnode);
      cnode->complete();
      return n;
    }
    else{
      return gtail.apply( cnode);
    }
  }  

};

template< class F0>
struct GList< F0>{
  
  const string tag;
  F0& f;
  size_t n = 0;  
  static constexpr size_t nsize = 1;

    [[maybe_unused]] [[maybe_unused]] GList( const SList< string>& tags, F0& f0);
  
  std::optional< size_t> apply( Node_Ptr cnode){
    if (cnode->tag() == tag){
      f( cnode);
      return n;
    }
    else{
      return std::nullopt;
    }
  }
  
  void inc_n(){
    ++n;
  }
  
};
    template<class F0>
    [[maybe_unused]] GList<F0>::GList(const SList<std::string> &tags, F0 &f0):
            tag( head( tags)), f(f0){}

//##############################################################
// Check each child node and apply function to one with corresponding tag
// After each application, call "done" with counts array.  If
// done returns true, quit and don't go on to further child nodes.

template< size_t nt, class Done, class...F>
[[maybe_unused]] bool Parser::apply_to_next_children_with_tags( const Node_Ptr& node,
                      const Array< string,nt>& tags_in,
                      Done&& done, F&&...f){
  
  Array< size_t, nt> counts( Zeroi{});
  
  auto tags = slist_from_array( tags_in);
  GList glist( tags, f...);
  
  auto& cnodes = node->children;
  while( true){
    for( auto itr = cnodes.begin(); itr != cnodes.end(); ++itr){
      auto& cnode = *itr;
      auto itopt = glist.apply( cnode);
      if (itopt){
        auto it = itopt.value();
        ++counts[it];
        if (done( counts)){
          cnodes.erase( cnodes.begin(), ++itr);
          return true;
        }
      }
    }
    
    if (node->completed){
      return false;
    }
    else{
      cnodes.clear();
      parse_some();
    }
  }
  
}

}

