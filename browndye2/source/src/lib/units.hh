#pragma once
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of physical units. This is used to ensure, at compile time,
that one is not attempting inconsistent math operations with numbers that
have physical units.  The main class is 

template< class M, class L, class T, class Q, class VType = double>
class Unit;

where the first four template parameters denote mass, length, time, 
and charge.  The parameters are instances of the Rational class, 
which represents rational numbers:

template< int Num, unsigned int Den> class Rational;

The operators and many of the math functions are overloaded.

Two commonly used classes include

template< class U0, class U1> class UProd;
template< class U0, class U1> class UQuot;

which are used to get the product and quotient of two units.

These classes are used only when UNITS is #define'd; otherwise,
they are typedef'd to the bare numerical type.

There are several pre-defined units listed near the end of this file.

 */
 

#include <cmath>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include "../global/debug.hh"
#include "../global/error_msg.hh"


namespace Units{
  using namespace Browndye;

  template< int Num, unsigned int Den>
  class Rational{
  public:
    static const int num = Num;
    static const unsigned int den = Den;
  };
  
  typedef Rational< 0, 1> RZero;
  typedef Rational< 1, 1> ROne;
}

template< class M, class L, class T, class Q, class VType>
class Unit;

/*
namespace Units {

    constexpr bool debug = Browndye::debug;



    inline
    void check_nan( double u) {

      if (debug) {
        if (std::isnan( u)) {
          error("Units: is nan");
        }
      }
    }

    inline
    void check_nan( float u) {

      if (debug) {
        if (std::isnan( u)) {
          error("Units: is nan");
        }
      }
    }
}
 */

template< class M, class L, class T, class Q, class VType = double>
class Unit{
public:
  explicit constexpr Unit( VType _value): value( _value){};
  
  Unit(): value( NAN){};

  template< class VT>
  Unit( const Unit< M,L,T,Q, VT>& other){
    check_nan( other);
    value = other.value;
  } 

  template< class VT>
  Unit( Unit< M,L,T,Q, VT>&& other){
    check_nan( other);
    value = other.value;
  } 

  template< class VT>
  Unit& operator=( const Unit< M,L,T,Q, VT>& other){
    check_nan( other);
    value = other.value;
    return *this;
  } 

  template< class VT>
  Unit& operator=( Unit< M,L,T,Q, VT>&& other){
    check_nan( other);
    value = other.value;
    return *this;
  } 

  template< class VT1>
  void operator+=( const Unit< M,L,T,Q,VT1>& u){
    check_nan( u);
    check_nan( value);
    this->value += u.value;
  }

  template< class VT1>
  void operator-=( const Unit< M,L,T,Q,VT1>& u){
    check_nan( u);
    check_nan( value);
    this->value -= u.value;
  }


  template< class VT1>
  void operator*=( const VT1& u){
    check_nan( u);
    check_nan( value);
    this->value *= u;
  }

  template< class VT1>
  void operator/=( const VT1& u){
    check_nan( u);
    check_nan( value);
    this->value /= u;
    check_nan( this->value);
  }

  typedef M Mass;
  typedef L Length;
  typedef T Time;
  typedef Q Charge;
  typedef VType Value_Type;

  VType value;

  void check_nan() const{
    if (Browndye::debug) {
      if (std::isnan( value)) {
        Browndye::error("Units: is nan");
      }
    }
  }

  template< class M1, class L1, class T1, class Q1, class VType1>
  static
  void check_nan( const Unit< M1,L1,T1,Q1,VType1>& u){
    u.check_nan();
  }

  static
  void check_nan( double u){
    if (Browndye::debug) {
      if (std::isnan( u)) {
        Browndye::error("Units: is nan");
      }
    }
  }

  static
  void check_nan( float u){
    if (Browndye::debug) {
      if (std::isnan( u)) {
        Browndye::error("Units: is nan");
      }
    }
  }

};


template< class VType>
class Unit< Units::RZero, Units::RZero, Units::RZero, Units::RZero, VType>
{
public:
  Unit( VType _value): value( _value){};
  Unit(): value( NAN){};
  operator VType(){
    return value;
  }

  VType value;
};

namespace Units{

  template< unsigned int a, unsigned int b>
  class GCD{
  public:
    static const unsigned int res = GCD< b, a % b >::res; 
  };
  
  template< unsigned int a>
  class GCD< a, 0>{
  public:
    static const unsigned int res = a;
  };
  
  template< unsigned int a, unsigned int b>
  class LCM{
  public:
    static const unsigned int res = a*b/GCD< a, b>::res;
  };
  
  // Implements Euclid algorithm for LCM
  template< int a, unsigned int b>
  class Simplest_Fraction{
  public:
    static const unsigned int absa = a > 0 ? a : -a;
    static const unsigned int gcd = GCD< absa, b>::res;
    static const unsigned int den = b/gcd;
    static const int num = a/(int)gcd;
  };
  
  template< class R0, class R1>
  class RSum{
  public:
    static const int n0 = R0::num;
    static const unsigned int d0 = R0::den;
  
    static const int n1 = R1::num;
    static const unsigned int d1 = R1::den;
  
    static const unsigned int pden = LCM< d0, d1>::res;
    static const int pnum = n0*(int)pden/(int)d0 + n1*(int)pden/(int)d1;
  
    typedef Simplest_Fraction< pnum, pden> SF;
    static const int num = SF::num; 
    static const unsigned int den = SF::den;
  
    typedef Rational< num, den> Res;
  };
  
  template< class R0, class R1>
  class RDiff{
  public:
    static const int n0 = R0::num;
    static const unsigned int d0 = R0::den;
  
    static const int n1 = R1::num;
    static const unsigned int d1 = R1::den;
  
    static const unsigned int pden = LCM< d0, d1>::res;
    static const int pnum = n0*(int)pden/(int)d0 - (int)n1*pden/(int)d1;
  
    typedef Simplest_Fraction< pnum, pden> SF;
    static const int num = SF::num; 
    static const unsigned int den = SF::den;
  
    typedef Rational< num, den> Res;
  };

  template< class T0, class T1>
  class BiType;
  
  template<>
  class BiType< double, double>{
  public:
    typedef double Res;
  };
  
  template<>
  class BiType< double, float>{
  public:
    typedef double Res;
  };
  
  template<>
  class BiType< float, double>{
  public:
    typedef double Res;
  };
  
  template<>
  class BiType< float, float>{
  public:
    typedef float Res;
  };

}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
auto operator+( const Unit< M,L,T,Q,VT0>& u0, 
                 const Unit< M,L,T,Q,VT1>& u1){
  //u0.check_nan();
  //u1.check_nan();
  u0.check_nan();
  u1.check_nan();
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u0.value + u1.value);
} 

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
auto operator-( const Unit< M,L,T,Q,VT0>& u0, 
                 const Unit< M,L,T,Q,VT1>& u1){
  //u0.check_nan();
  //u1.check_nan();
  u0.check_nan();
  u1.check_nan();
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u0.value - u1.value);
} 

template< class M, class L, class T, class Q, class VT>
auto operator-( const Unit< M,L,T,Q,VT>& u){
  //u.check_nan();
  u.check_nan();
  return Unit< M,L,T,Q,VT>( -u.value);
}

namespace Units{

  template< class R>
  class RNeg{
  public:
    static const int num = -R::num;
    static const unsigned int den = R::den;
  
    typedef Rational< num, den> Res;
  };
  
  template< class R>
  class RHalf{
  public:
    static const int pnum = R::num;
    static const unsigned int pden = R::den*2;
  
    typedef Simplest_Fraction< pnum, pden> SF;
  
    static const int num = SF::num; 
    static const unsigned int den = SF::den;
  
    typedef Rational< num, den> Res;
  };
  
  template< class R>
  class RThird{
  public:
    static const int pnum = R::num;
    static const unsigned int pden = R::den*3;
  
    typedef Simplest_Fraction< pnum, pden> SF;
  
    static const int num = SF::num; 
    static const unsigned int den = SF::den;
  
    typedef Rational< num, den> Res;
  };
  
  
  template< class R>
  class RDouble{
  public:
    static const int pnum = R::num*2;
    static const unsigned int pden = R::den;
  
    typedef Simplest_Fraction< pnum, pden> SF;
  
    static const int num = SF::num; 
    static const unsigned int den = SF::den;
  
    typedef Rational< num, den> Res;
  };
  
  template< class U>
  class USqrt{
  public:
    typedef typename U::Mass M;
    typedef typename U::Length L;
    typedef typename U::Time T;
    typedef typename U::Charge Q;
    typedef typename U::Value_Type VT;  
  
    typedef Unit< typename RHalf< M>::Res, typename RHalf< L>::Res, 
                  typename RHalf< T>::Res, typename RHalf< Q>::Res, VT>
    Res;
  };
  
  template< class U>
  class USq{
  public:
    typedef typename U::Mass M;
    typedef typename U::Length L;
    typedef typename U::Time T;
    typedef typename U::Charge Q;
    typedef typename U::Value_Type VT;
    
    typedef Unit< typename RDouble< M>::Res, typename RDouble< L>::Res, 
                  typename RDouble< T>::Res, typename RDouble< Q>::Res, VT>
    Res;
  };


template< class M, class L, class T, class Q, class VT>
auto sqrt( const Unit< M,L,T,Q,VT>& u){

  u.check_nan();
  //u.check_nan();

  if (debug){
    if ((u.value) < 0.0){
      error( "units::sqrt: negative argument", u.value);
    }
  }

  return 
    Unit< typename Units::RHalf< M>::Res, typename Units::RHalf< L>::Res, 
    typename Units::RHalf< T>::Res, typename Units::RHalf< Q>::Res, VT>( std::sqrt( u.value));
}

template< class M, class L, class T, class Q, class VT>
auto cbrt( const Unit< M,L,T,Q,VT>& u){
  u.check_nan();
  return 
    Unit< typename Units::RThird< M>::Res, typename Units::RThird< L>::Res, 
    typename Units::RThird< T>::Res, typename Units::RThird< Q>::Res, VT>( std::cbrt( u.value));
}

template< class M, class L, class T, class Q, class VT>
Unit< M, L, T, Q, VT>
fabs( const Unit< M,L,T,Q,VT>& u){
  u.check_nan();
  return Unit< M,L,T,Q, VT>( std::fabs( u.value));
}

}

namespace Units{

  template< class M, class L, class T, class Q, class VT>
  class Dimless_Filter{
  public:
    typedef Unit< M,L,T,Q,VT> Res;
  };
  
  template< class VT>
  class Dimless_Filter< RZero, RZero, RZero, RZero, VT>{
  public:
    typedef VT Res;
  };
  
  
  template< class U0, class U1>
  class UProd{
  public:
    typedef typename U0::Mass M0;
    typedef typename U0::Length L0;
    typedef typename U0::Time T0;
    typedef typename U0::Charge Q0;
    typedef typename U0::Value_Type VT0;
  
    typedef typename U1::Mass M1;
    typedef typename U1::Length L1;
    typedef typename U1::Time T1;
    typedef typename U1::Charge Q1;
    typedef typename U1::Value_Type VT1;
  
    typedef typename BiType< VT0, VT1>::Res VT;
  
    typedef typename Units::Dimless_Filter< typename RSum< M0,M1>::Res, 
                                           typename RSum< L0,L1>::Res, 
                                           typename RSum< T0,T1>::Res, 
                                           typename RSum< Q0,Q1>::Res, VT >::Res Res; 
    
  };
  
  template< class U>
  class UProd< U, double>{
  public:
    typedef U Res;
  };
  
  template< class U>
  class UProd< double, U>{
  public:
    typedef U Res;
  };

}

template< class M0, class L0, class T0, class Q0, class VT0,
          class M1, class L1, class T1, class Q1, class VT1>
inline
auto operator*( const Unit< M0,L0,T0,Q0,VT0>& u0, 
           const Unit< M1,L1,T1,Q1,VT1>& u1){
  //u0.check_nan();
  //u1.check_nan();
  u0.check_nan();
  u1.check_nan();
  typedef typename Units::UProd< 
    Unit< M0,L0,T0,Q0,VT0>, 
    Unit< M1,L1,T1,Q1,VT1> >::Res Res;
  return Res( u0.value * u1.value);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M,L,T,Q,typename Units::BiType< VT0, VT1>::Res > 
operator*( const Unit< M,L,T,Q,VT0> u, const VT1& v){
  u.check_nan();
  Unit< M,L,T,Q,VT0>::check_nan( v);
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u.value*v);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
Unit< M,L,T,Q,typename Units::BiType< VT0, VT1>::Res > 
operator*( const VT0& v, const Unit< M,L,T,Q,VT1> u){
  u.check_nan();
  Unit< M,L,T,Q,VT1>::check_nan( v);
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u.value*v);
}

namespace Units{
  
  template< class U>
  class URecip{
  public:
    typedef typename U::Mass M;
    typedef typename U::Length L;
    typedef typename U::Time T;
    typedef typename U::Charge Q;
    typedef typename U::Value_Type VT;
  
    typedef Unit< typename RNeg< M>::Res, 
                  typename RNeg< L>::Res, 
                  typename RNeg< T>::Res, 
                  typename RNeg< Q>::Res, VT> Res;
  };
  
  template< class U0, class U1>
  class UQuot{
  public:
    typedef typename Units::UProd< U0, typename URecip< U1>::Res>::Res Res;
  };
  
  template< class U>
  class UQuot< U, double>{
  public:
    typedef U Res;
  };
  
  template< class U>
  class UQuot< double, U>{
  public:
    typedef typename URecip< U>::Res Res;
  };
}

template< class M0, class L0, class T0, class Q0, class VT0,
          class M1, class L1, class T1, class Q1, class VT1>
inline
auto operator/( const Unit< M0,L0,T0,Q0,VT0>& u0,
               const Unit< M1,L1,T1,Q1,VT1>& u1){
  //u0.check_nan();
  //u1.check_nan();
  u0.check_nan();
  u1.check_nan();
  typedef typename Units::UQuot< Unit< M0,L0,T0,Q0>, Unit< M1,L1,T1,Q1> >::Res Res;
  auto res = u0.value / u1.value;
  //res.check_nan();
  Unit< M0,L0,T0,Q0,VT0>::check_nan( res);
  return Res( res);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< typename Units::RNeg< M>::Res, typename Units::RNeg< L>::Res, 
      typename Units::RNeg< T>::Res, typename Units::RNeg< Q>::Res, 
      typename Units::BiType< VT0, VT1>::Res >
operator/( const VT1& vt, const Unit< M,L,T,Q,VT0>& u){
  
  Unit< M,L,T,Q,VT0>::check_nan( vt);
  u.check_nan();
  auto res = vt/u.value;
  Unit< M,L,T,Q,VT0>::check_nan( res);
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  return Unit< typename Units::RNeg< M>::Res, typename Units::RNeg< L>::Res, 
               typename Units::RNeg< T>::Res, typename Units::RNeg< Q>::Res, VT >( res);    
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M, L, T, Q, typename Units::BiType< VT0, VT1>::Res >
operator/( const Unit< M,L,T,Q,VT0>& u, const VT1& vt){
  u.check_nan();
  Unit< M,L,T,Q,VT0>::check_nan( vt);
  typedef typename Units::BiType< VT0, VT1>::Res VT;
  auto res = u.value/vt;
  Unit< M,L,T,Q,VT0>::check_nan( res);
  return Unit< M, L, T, Q, VT>( res);
}


template< class M, class L, class T, class Q, class VT>
inline
bool operator==( const Unit< M,L,T,Q,VT>& u0, 
                 const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  return u0.value == u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator>( const Unit< M,L,T,Q,VT>& u0, 
                const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  return u0.value > u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator<( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  auto v0 = u0.value;
  auto v1 = u1.value;
  return v0 < v1;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator>=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  return u0.value >= u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator<=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  return u0.value <= u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator!=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  u0.check_nan();
  u1.check_nan();
  return u0.value != u1.value;
}

namespace Units{

template< class U>
class Short_Version{
public:
  typedef typename U::Mass M;
  typedef typename U::Length L;
  typedef typename U::Time T;
  typedef typename U::Charge Q;

  typedef Unit< M,L,T,Q,float> Res;
};

}

template< class M, class L, class T, class Q, class VT>
inline
VT fvalue( const Unit< M,L,T,Q,VT>& u){
  return u.value;
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
void set_fvalue( Unit< M,L,T,Q,VT0>& u, const VT1& vt){
  u.value = vt;
}

typedef Unit< Units::ROne, Units::RZero, Units::RZero, Units::RZero> Mass;
typedef Unit< Units::RZero, Units::ROne, Units::RZero, Units::RZero> Length;
typedef Unit< Units::RZero, Units::RZero, Units::ROne, Units::RZero> Time;
typedef Unit< Units::RZero, Units::RZero, Units::RZero, Units::ROne> Charge;

template< class S, class M, class L, class T, class Q, class VT>
S& operator<<( S& s, const Unit< M,L,T,Q,VT>& u){
  s << u.value;
  return s;
}

template< class S, class M, class L, class T, class Q, class VT>
S& operator>>( S& s, Unit< M,L,T,Q,VT>& u){
  s >> u.value;
  return s;
}


//###########################################

namespace Units{
  
  template<>
  class Short_Version< double>{
  public:
    typedef float Res;
  };
  
  template<>
  class Short_Version< unsigned int>{
  public:
    typedef unsigned short int Res;
  };
}

inline
double fvalue( const double a){
  return a;
}

inline
float fvalue( const float a){
  return a;
}

inline
void set_fvalue( double& a, double x){
  a = x;
}

inline
void set_fvalue( float& a, float x){
  a = x;
}

typedef decltype( Length()/Time()) Velocity;
typedef decltype( Velocity()/Time()) Acceleration;
typedef decltype( Mass()*Acceleration()) Force;
typedef decltype( Force()*Length()) Torque;
typedef decltype( Force()*Length()) Energy;
typedef decltype( Energy()/Charge()) EPotential;
typedef decltype( Force()/Charge()) EPotential_Gradient;

typedef decltype( Charge()*Charge()/(Length()*Energy())) Permittivity;

typedef decltype( Length()*Length()) Length2;
typedef decltype( Length()*Length2()) Length3;
typedef decltype( Length()*Length3()) Length4;
typedef decltype( Length()*Length4()) Length5;
typedef decltype( Length()*Length5()) Length6;
typedef decltype( Length()*sqrt( Length())) Length1p5;

typedef Length3 Volume;

typedef decltype( Time()*Time()) Time2;

typedef decltype( 1.0/Length()) Inv_Length;
typedef decltype( 1.0/Length2()) Inv_Length2;
typedef decltype( 1.0/Length3()) Inv_Length3;
typedef decltype( 1.0/Length4()) Inv_Length4;
typedef decltype( 1.0/Length5()) Inv_Length5;
typedef decltype( 1.0/Length6()) Inv_Length6;

typedef decltype( sqrt( Time())) Sqrt_Time;
typedef decltype(1.0/Time()) Inv_Time;

typedef decltype( Force()/(Velocity()*Length())) Viscosity;
typedef decltype( Length2()/Time()) Diffusivity;
typedef decltype( Length()*Charge()) Dipole;
typedef decltype( Length2()*Charge()) Quadrupole;

typedef decltype( Force()/Length()) Spring_Constant;
typedef decltype( Velocity()/Force()) Mobility;
typedef decltype( Energy()/Length2()) Surface_Tension;
typedef Inv_Length3 Rot_Mobility;

namespace Units {
  template<class Num>
  inline
  void check_nan_always(Num x, const std::string &msg = "") {
    if (std::isnan( x / Num{ 1.0})) {
      error("Units: is nan", msg);
    }
  }
}


