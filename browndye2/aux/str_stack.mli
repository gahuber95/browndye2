(* 
Used internally by XML parser.

This is a "stack of strings" which is used to concatenate many
small strings together while avoiding quadratic scaling of effort.
As strings are added to the stack, they are gradually coalesced
into one big string.
*)

(* add string to stack *)
val add_str_to_stack: string -> string Stack.t -> unit

(* Finish coalescing the strings together, empty the
stack, and return one big string *)
val str_from_stack: string Stack.t -> string

